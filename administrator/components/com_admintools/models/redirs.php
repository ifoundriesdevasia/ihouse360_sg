<?php
/**
 *  @package AdminTools
 *  @copyright Copyright (c)2010-2011 Nicholas K. Dionysopoulos
 *  @license GNU General Public License version 3, or later
 *  @version $Id$
 */

// Protect from unauthorized access
defined('_JEXEC') or die('Restricted Access');

jimport('joomla.application.component.model');

require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'models'.DS.'base.php';

class AdmintoolsModelRedirs extends AdmintoolsModelBase
{
	public function setRedirectionState($newState)
	{
		$params = JModel::getInstance('Storage','AdmintoolsModel');
		$params->setValue('urlredirection',$newState ? 1 : 0);
		$params->save();
	}
	
	public function getRedirectionState()
	{
		$params = JModel::getInstance('Storage','AdmintoolsModel');
		return $params->getValue('urlredirection', 1);
	}
	
	public function buildQuery($overrideLimits = false)
	{
		$where = array();

		$fltSource		= $this->getState('source', null, 'string');
		$fltDest		= $this->getState('dest', null, 'string');
		$fltPublished	= $this->getState('published', null, 'cmd');

		$db = $this->getDBO();
		if($fltSource) {
			$where[] = '`source` LIKE "%'.$db->getEscaped($fltSource).'%"';
		}
		if($fltDest) {
			$where[] = '`dest` LIKE "%'.$db->getEscaped($fltDest).'%"';
		}
		if($fltPublished != '') {
			$where[] = '`published` = '.$db->Quote((int)$fltPublished);
		}

		$query = 'SELECT * FROM `#__admintools_redirects`';

		if(count($where) && !$overrideLimits)
		{
			$query .= ' WHERE (' . implode(') AND (',$where) . ')';
		}

		if(!$overrideLimits) {
			$order = $this->getState('order',null,'cmd');
			if($order === 'Array') $order = null;
			$dir = $this->getState('dir',null,'cmd');

			$app = JFactory::getApplication();
			$hash = $this->getHash();
			if(empty($order)) {
				$order = $app->getUserStateFromRequest($hash.'filter_order', 'filter_order', 'id');
			}
			if(empty($dir)) {
				$dir = $app->getUserStateFromRequest($hash.'filter_order_Dir', 'filter_order_Dir', 'DESC');
				$dir = in_array(strtoupper($dir),array('DESC','ASC')) ? strtoupper($dir) : "ASC";
			}

			$query .= ' ORDER BY '.$db->nameQuote($order).' '.$dir;
		}

		return $query;
	}
}