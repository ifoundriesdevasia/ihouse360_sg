<?php
/**
 *  @package AdminTools
 *  @copyright Copyright (c)2010-2011 Nicholas K. Dionysopoulos
 *  @license GNU General Public License version 3, or later
 *  @version $Id$
 */

// Protect from unauthorized access
defined('_JEXEC') or die('Restricted Access');

jimport('joomla.application.component.model');

require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'models'.DS.'base.php';

class AdmintoolsModelWafexceptions extends AdmintoolsModelBase
{
	public function buildQuery($overrideLimits = false)
	{
		$where = array();

		$fltOption			= $this->getState('foption', null, 'string');
		$fltView			= $this->getState('fview', null, 'string');
		$fltQuery			= $this->getState('fquery', null, 'string');

		$db = $this->getDBO();
		if($fltOption) {
			$where[] = '`option` LIKE "%'.$db->getEscaped($fltOption).'%"';
		}
		if($fltView) {
			$where[] = '`view` LIKE "%'.$db->getEscaped($fltView).'%"';
		}
		if($fltQuery) {
			$where[] = '`query` LIKE "%'.$db->getEscaped($fltQuery).'%"';
		}
		
		$query = 'SELECT * FROM `#__admintools_wafexceptions`';

		if(count($where) && !$overrideLimits)
		{
			$query .= ' WHERE (' . implode(') AND (',$where) . ')';
		}

		if(!$overrideLimits) {
			$order = $this->getState('order',null,'cmd');
			if($order === 'Array') $order = null;
			$dir = $this->getState('dir',null,'cmd');

			$app = JFactory::getApplication();
			$hash = $this->getHash();
			if(empty($order)) {
				$order = $app->getUserStateFromRequest($hash.'filter_order', 'filter_order', 'id');
			}
			if(empty($dir)) {
				$dir = $app->getUserStateFromRequest($hash.'filter_order_Dir', 'filter_order_Dir', 'DESC');
				$dir = in_array(strtoupper($dir),array('DESC','ASC')) ? strtoupper($dir) : "ASC";
			}

			$query .= ' ORDER BY '.$db->nameQuote($order).' '.$dir;
		}

		return $query;
	}
}