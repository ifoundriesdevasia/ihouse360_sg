<?php
/**
 *  @package AdminTools
 *  @copyright Copyright (c)2010-2011 Nicholas K. Dionysopoulos
 *  @license GNU General Public License version 3, or later
 *  @version $Id$
 */

// Protect from unauthorized access
defined('_JEXEC') or die();

$lang = JFactory::getLanguage();
$option = JRequest::getCmd('option','com_admintools');
?>

<form name="adminForm" id="adminForm" action="index.php" method="post">
	<input type="hidden" name="option" value="com_admintools" />
	<input type="hidden" name="view" value="wafconfig" />
	<input type="hidden" name="task" value="save" />
	<input type="hidden" name="<?php echo JUtility::getToken();?>" value="1" />

	<fieldset>
		<legend><?php echo JText::_('ATOOLS_LBL_WAF_CUSTOMMESSAGE_HEADER')?></legend>
		<div class="editform-row">
			<label for="custom403msg" title="<?php echo JText::_('ATOOLS_LBL_WAF_CUSTOMMESSAGE_DESC') ?>">
				<?php echo JText::_('ATOOLS_LBL_WAF_CUSTOMMESSAGE_LABEL'); ?>
			</label>
			<input type="text" size="50" name="custom403msg" value="<?php echo $this->config['custom403msg'] ?>" title="<?php echo JText::_('ATOOLS_LBL_WAF_CUSTOMMESSAGE_DESC') ?>" />
		</div>
	</fieldset>
	
	<fieldset>
		<legend><?php echo JText::_('ATOOLS_LBL_WAF_OPTGROUP_BASIC') ?></legend>
		
		<div class="editform-row">
			<label for="ipwl"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_IPWL'); ?></label>
			<?php echo AdmintoolsHelperSelect::booleanlist('ipwl', array(), $this->config['ipwl']) ?>
		</div>
		<div class="editform-row">
			<label for="ipbl"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_IPBL'); ?></label>
			<?php echo AdmintoolsHelperSelect::booleanlist('ipbl', array(), $this->config['ipbl']) ?>
		</div>
		<div class="editform-row">
			<label for="adminpw" title="<?php echo JText::_('ATOOLS_LBL_WAF_OPT_ADMINPW_TIP'); ?>"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_ADMINPW'); ?></label>
			<input type="text" size="20" name="adminpw" value="<?php echo $this->config['adminpw'] ?>" title="<?php echo JText::_('ATOOLS_LBL_WAF_OPT_ADMINPW_TIP') ?>" />
		</div>
		<div class="editform-row">
			<label for="emailonadminlogin" title="<?php echo JText::_('ATOOLS_LBL_WAF_OPT_EMAILADMINLOGIN_TIP'); ?>"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_EMAILADMINLOGIN'); ?></label>
			<input type="text" size="20" name="emailonadminlogin" value="<?php echo $this->config['emailonadminlogin'] ?>" title="<?php echo JText::_('ATOOLS_LBL_WAF_OPT_EMAILADMINLOGIN_TIP') ?>" />
		</div>		
		<div class="editform-row">
			<label for="emailonfailedadminlogin" title="<?php echo JText::_('ATOOLS_LBL_WAF_OPT_EMAILADMINFAILEDLOGIN_TIP'); ?>"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_EMAILADMINFAILEDLOGIN'); ?></label>
			<input type="text" size="20" name="emailonfailedadminlogin" value="<?php echo $this->config['emailonfailedadminlogin'] ?>" title="<?php echo JText::_('ATOOLS_LBL_WAF_OPT_EMAILADMINFAILEDLOGIN_TIP') ?>" />
		</div>		
		<div class="editform-row">
			<label for="nofesalogin"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_NOFESALOGIN'); ?></label>
			<?php echo AdmintoolsHelperSelect::booleanlist('nofesalogin', array(), $this->config['nofesalogin']) ?>
		</div>		
		<div class="editform-row">
			<label for="sqlishield"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_SQLISHIELD'); ?></label>
			<?php echo AdmintoolsHelperSelect::booleanlist('sqlishield', array(), $this->config['sqlishield']) ?>
		</div>
		<div class="editform-row">
			<label for="xssshield"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_XSSSHIELD'); ?></label>
			<?php echo AdmintoolsHelperSelect::booleanlist('xssshield', array(), $this->config['xssshield']) ?>
		</div>
		<div class="editform-row">
			<label for="muashield"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_MUASHIELD'); ?></label>
			<?php echo AdmintoolsHelperSelect::booleanlist('muashield', array(), $this->config['muashield']) ?>
		</div>
		<div class="editform-row">
			<label for="csrfshield"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_CSRFSHIELD'); ?></label>
			<?php echo AdmintoolsHelperSelect::csrflist('csrfshield', array(), $this->config['csrfshield']) ?>
		</div>
		<div class="editform-row">
			<label for="rfishield"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_RFISHIELD'); ?></label>
			<?php echo AdmintoolsHelperSelect::booleanlist('rfishield', array(), $this->config['rfishield']) ?>
		</div>
		<div class="editform-row">
			<label for="dfishield"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_DFISHIELD'); ?></label>
			<?php echo AdmintoolsHelperSelect::booleanlist('dfishield', array(), $this->config['dfishield']) ?>
		</div>
		<div class="editform-row">
			<label for="dfishield"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_UPLOADSHIELD'); ?></label>
			<?php echo AdmintoolsHelperSelect::booleanlist('uploadshield', array(), $this->config['uploadshield']) ?>
		</div>
		<div class="editform-row">
			<label for="antispam"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_ANTISPAM'); ?></label>
			<?php echo AdmintoolsHelperSelect::booleanlist('antispam', array(), $this->config['antispam']) ?>
		</div>
		<div class="editform-row">
			<label for="custgenerator"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_CUSTGENERATOR'); ?></label>
			<?php echo AdmintoolsHelperSelect::booleanlist('custgenerator', array(), $this->config['custgenerator']) ?>
		</div>
		<div class="editform-row">
			<label for="generator"><?php echo JText::_('ATOOLS_LBL_WAF_GENERATOR'); ?></label>
			<input type="text" size="45" name="generator" value="<?php echo $this->config['generator'] ?>" title="<?php echo JText::_('ATOOLS_LBL_WAF_GENERATOR_TIP') ?>" />
		</div>
		<div class="editform-row">
			<label for="logbreaches"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_LOGBREACHES'); ?></label>
			<?php echo AdmintoolsHelperSelect::booleanlist('logbreaches', array(), $this->config['logbreaches']) ?>
		</div>
		<div class="editform-row">
			<label for="emailbreaches" title="<?php echo JText::_('ATOOLS_LBL_WAF_OPT_EMAILBREACHES_TIP'); ?>"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_EMAILBREACHES'); ?></label>
			<input type="text" size="20" name="emailbreaches" value="<?php echo $this->config['emailbreaches'] ?>" title="<?php echo JText::_('ATOOLS_LBL_WAF_OPT_EMAILBREACHES_TIP') ?>" />
		</div>		
		<div class="editform-row">
			<label for="blockinstall"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_BLOCKINSTALL'); ?></label>
			<?php echo AdmintoolsHelperSelect::blockinstallopts('blockinstall', array(), $this->config['blockinstall']) ?>
		</div>
		<div class="editform-row">
			<label for="nonewadmins"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_NONEWADMINS'); ?></label>
			<?php echo AdmintoolsHelperSelect::booleanlist('nonewadmins', array(), $this->config['nonewadmins']) ?>
		</div>
		<div class="editform-row">
			<label for="encodedby"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_ENCODEDBY'); ?></label>
			<input type="text" size="45" name="encodedby" value="<?php echo $this->config['encodedby'] ?>" />
		</div>
		<div class="editform-row">
			<label for="poweredby"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_POWEREDBY'); ?></label>
			<input type="text" size="45" name="poweredby" value="<?php echo $this->config['poweredby'] ?>" />
		</div>
		<div class="editform-row">
			<label for="nojoomla"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_NOJOOMLA'); ?></label>
			<?php echo AdmintoolsHelperSelect::booleanlist('nojoomla', array(), $this->config['nojoomla']) ?>
		</div>

	</fieldset>

	<fieldset>
		<legend><?php echo JText::_('ATOOLS_LBL_WAF_OPTGROUP_FINGERPRINTING') ?></legend>

		<div class="editform-row">
			<label for="tpone"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_TPONE'); ?></label>
			<?php echo AdmintoolsHelperSelect::booleanlist('tpone', array(), $this->config['tpone']) ?>
		</div>
		<div class="editform-row">
			<label for="tmpl"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_TMPL'); ?></label>
			<?php echo AdmintoolsHelperSelect::booleanlist('tmpl', array(), $this->config['tmpl']) ?>
		</div>
		<div class="editform-row">
			<label for="tmplwhitelist" title="<?php echo JText::_('ATOOLS_LBL_WAF_OPT_TMPLWHITELIST_TIP'); ?>"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_TMPLWHITELIST'); ?></label>
			<input type="text" size="45" name="tmplwhitelist" value="<?php echo $this->config['tmplwhitelist'] ?>" />
		</div>
		<div class="editform-row">
			<label for="template"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_TEMPLATE'); ?></label>
			<?php echo AdmintoolsHelperSelect::booleanlist('template', array(), $this->config['template']) ?>
		</div>
	</fieldset>

	<fieldset>
		<legend><?php echo JText::_('ATOOLS_LBL_WAF_LBL_PROJECTHONEYPOT')?></legend>
		<div class="editform-row">
			<label for="httpblenable"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_HTTPBLENABLE'); ?></label>
			<?php echo AdmintoolsHelperSelect::booleanlist('httpblenable', array(), $this->config['httpblenable']) ?>
		</div>
		<div class="editform-row">
			<label for="bbhttpblkey"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_bbhttpblkey'); ?></label>
			<input type="text" size="45" name="bbhttpblkey" value="<?php echo $this->config['bbhttpblkey'] ?>" />
		</div>
		<div class="editform-row">
			<label for="httpblthreshold"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_HTTPBLTHRESHOLD'); ?></label>
			<input type="text" size="5" name="httpblthreshold" value="<?php echo $this->config['httpblthreshold'] ?>" />
		</div>
		<div class="editform-row">
			<label for="httpblmaxage"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_HTTPBLMAXAGE'); ?></label>
			<input type="text" size="5" name="httpblmaxage" value="<?php echo $this->config['httpblmaxage'] ?>" />
		</div>
		<div class="editform-row">
			<label for="httpblblocksuspicious"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_HTTPBLBLOCKSUSPICIOUS'); ?></label>
			<?php echo AdmintoolsHelperSelect::booleanlist('httpblblocksuspicious', array(), $this->config['httpblblocksuspicious']) ?>
		</div>
	</fieldset>
	
	<fieldset>
		<legend><?php echo JText::_('ATOOLS_LBL_WAF_LBL_BADBEHAVIOUR')?></legend>
		<div class="editform-row">
			<label for="badbehaviour"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_BADBEHAVIOUR'); ?></label>
			<?php echo AdmintoolsHelperSelect::booleanlist('badbehaviour', array(), $this->config['badbehaviour']) ?>
		</div>
		<div class="editform-row">
			<label for="bbstrict"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_BBSTRICT'); ?></label>
			<?php echo AdmintoolsHelperSelect::booleanlist('bbstrict', array(), $this->config['bbstrict']) ?>
		</div>
		<div class="editform-row">
			<label for="bbwhitelistip"><?php echo JText::_('ATOOLS_LBL_WAF_OPT_bbwhitelistip'); ?></label>
			<input type="text" size="45" name="bbwhitelistip" value="<?php echo $this->config['bbwhitelistip'] ?>" />
		</div>
	</fieldset>
	
	<fieldset>
		<legend><?php echo JText::_('ATOOLS_LBL_WAF_LBL_TSR') ?></legend>
		<div class="editform-row">
			<label for="tsrenable"><?php echo JText::_('ATOOLS_LBL_WAF_LBL_TSRENABLE'); ?></label>
			<?php echo AdmintoolsHelperSelect::booleanlist('tsrenable', array(), $this->config['tsrenable']) ?>
		</div>
		<div class="editform-row">
			<label for="neverblockips"><?php echo JText::_('ATOOLS_LBL_WAF_LBL_NEVERBLOCKIPS'); ?></label>
			<input class="narrow" type="text" size="50" name="neverblockips" value="<?php echo $this->config['neverblockips'] ?>" />
		</div>
		<div class="editform-row">
			<label for="emailafteripautoban"><?php echo JText::_('ATOOLS_LBL_WAF_LBL_EMAILAFTERIPAUTOBAN'); ?></label>
			<input class="narrow" type="text" size="50" name="emailafteripautoban" value="<?php echo $this->config['emailafteripautoban'] ?>" />
		</div>
		<div class="editform-row">
			<label for="tsrstrikes"><?php echo JText::_('ATOOLS_LBL_WAF_LBL_TSRSTRIKES'); ?></label>
			<input class="narrow" type="text" size="5" name="tsrstrikes" value="<?php echo $this->config['tsrstrikes'] ?>" />
			<span><?php echo JText::_('ATOOLS_LBL_WAF_LBL_TSRNUMFREQ') ?></span>
			<input class="narrow" type="text" size="5" name="tsrnumfreq" value="<?php echo $this->config['tsrnumfreq'] ?>" />
			&nbsp;
			<?php echo AdmintoolsHelperSelect::trsfreqlist('tsrfrequency', array(), $this->config['tsrfrequency']) ?>
		</div>
		<div class="editform-row">
			<label for="tsrbannum"><?php echo JText::_('ATOOLS_LBL_WAF_LBL_TSRBANNUM'); ?></label>
			<input class="narrow" type="text" size="5" name="tsrbannum" value="<?php echo $this->config['tsrbannum'] ?>" />
			&nbsp;
			<?php echo AdmintoolsHelperSelect::trsfreqlist('tsrbanfrequency', array(), $this->config['tsrbanfrequency']) ?>
		</div>
		<div class="editform-row">
			<label for="spammermessage"><?php echo JText::_('ATOOLS_LBL_WAF_LBL_SPAMMERMESSAGE'); ?></label>
			<input type="text" size="45" name="spammermessage" value="<?php echo $this->config['spammermessage'] ?>" />
		</div>
	</fieldset>
</form>