<?php
/**
 *  @package AdminTools
 *  @copyright Copyright (c)2010-2011 Nicholas K. Dionysopoulos
 *  @license GNU General Public License version 3, or later
 *  @version $Id$
 */

// Protect from unauthorized access
defined('_JEXEC') or die('Restricted Access');

require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'views'.DS.'base.view.html.php';

class AdmintoolsViewWafexceptions extends AdmintoolsViewBase
{
	protected function onDisplay()
	{
		$app = JFactory::getApplication();
		$hash = $this->getHash();

		// ...filter states
		$this->lists->set('fltOption',		$app->getUserStateFromRequest($hash.'filter_option',
			'foption', null, 'string'));
		$this->lists->set('fltView',		$app->getUserStateFromRequest($hash.'filter_view',
			'fview', null, 'string'));
		$this->lists->set('fltQuery',		$app->getUserStateFromRequest($hash.'filter_query',
			'fquery', null, 'string'));

		// Add toolbar buttons
		JToolBarHelper::deleteList();
		JToolBarHelper::editListX();
		JToolBarHelper::addNewX();
		JToolBarHelper::divider();
		JToolBarHelper::back((ADMINTOOLS_JVERSION == '15') ? 'Back' : 'JTOOLBAR_BACK', 'index.php?option=com_admintools&view=waf');
		
		$document = JFactory::getDocument();
		$document->addStyleSheet('../media/com_admintools/css/backend.css');
		$document->addScript('../media/com_admintools/js/backend.js');

		// Run the parent method
		parent::onDisplay();
	}
}