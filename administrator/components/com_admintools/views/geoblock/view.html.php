<?php
/**
 *  @package AdminTools
 *  @copyright Copyright (c)2010-2011 Nicholas K. Dionysopoulos
 *  @license GNU General Public License version 3, or later
 *  @version $Id$
 */

// Protect from unauthorized access
defined('_JEXEC') or die('Restricted Access');

require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'views'.DS.'base.view.html.php';

class AdmintoolsViewGeoblock extends AdmintoolsViewBase
{
	protected function onDisplay()
	{
		$app = JFactory::getApplication();

		JToolBarHelper::save();
		JToolBarHelper::cancel();
		
		$model = $this->getModel();
		$model->getConfig();
		
		$this->assign('countries',		$model->getCountries());
		$this->assign('continents',		$model->getContinents());

		$subtitle_key = 'ADMINTOOLS_TITLE_'.strtoupper(JRequest::getCmd('view','cpanel'));
		JToolBarHelper::title(JText::_('ADMINTOOLS_TITLE_DASHBOARD').' &ndash; <small>'.JText::_($subtitle_key).'</small>','admintools');
	}
}