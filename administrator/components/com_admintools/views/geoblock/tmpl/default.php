<?php
/**
 *  @package AdminTools
 *  @copyright Copyright (c)2010-2011 Nicholas K. Dionysopoulos
 *  @license GNU General Public License version 3, or later
 *  @version $Id$
 */

// Protect from unauthorized access
defined('_JEXEC') or die('Restricted Access');
?>

<div id="disclaimer">
	<h3><?php echo JText::_('ATOOLS_LBL_GEOBLOCK_INFOHEAD'); ?></h3>
	<p><?php echo JText::_('ATOOLS_LBL_GEOBLOCK_INFO'); ?></p>
	<p class="small"><?php echo JText::_('ATOOLS_LBL_GEOBLOCK_MAXMIND');?></p>
</div>

<form action="index.php" method="post" name="adminForm">
	<input type="hidden" name="option" value="com_admintools" />
	<input type="hidden" name="view" value="geoblock" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="<?php echo JUtility::getToken();?>" value="1" />
	
	<fieldset id="waf-continents">
		<legend><?php echo JText::_('ATOOLS_LBL_GEOBLOCK_CONTINENTS')?></legend>
		
		<?php echo $this->continents; ?>
	</fieldset>

	<fieldset>
		<legend><?php echo JText::_('ATOOLS_LBL_GEOBLOCK_COUNTRIES')?></legend>
		
		<table class="adminform">
		<thead>
			<tr>
				<th colspan="3">
					<button onclick="$$('.country').setProperty('checked','checked');return false;"><?php echo JText::_('ATOOLS_LBL_GEOBLOCK_ALL') ?></button>
					&nbsp;
					<button onclick="$$('.country').setProperty('checked','');return false;"><?php echo JText::_('ATOOLS_LBL_GEOBLOCK_NONE') ?></button>
				</th>
			</tr>
		</thead>
		<tbody>
			<?php echo $this->countries; ?>
		</tbody>
		</table>
	</fieldset>
</form>