<?php
/**
 *  @package AdminTools
 *  @copyright Copyright (c)2010-2011 Nicholas K. Dionysopoulos
 *  @license GNU General Public License version 3, or later
 *  @version $Id$
 */

// Protect from unauthorized access
defined('_JEXEC') or die('Restricted Access');

// Load framework base classes
jimport('joomla.application.component.view');

class AdmintoolsViewHtmaker extends JView
{
	function display()
	{
		JToolBarHelper::title(JText::_('ADMINTOOLS_TITLE_HTMAKER'),'admintools');
		JToolBarHelper::save('save','ATOOLS_LBL_HTMAKER_SAVE');
		JToolBarHelper::apply('apply','ATOOLS_LBL_HTMAKER_APPLY');
		JToolBarHelper::divider();
		JToolBarHelper::preview('index.php?option=com_admintools&view=htmaker&format=raw');
		JToolBarHelper::divider();
		JToolBarHelper::back((ADMINTOOLS_JVERSION == '15') ? 'Back' : 'JTOOLBAR_BACK','index.php?option=com_admintools');

		$model = $this->getModel();
		$config = $model->loadConfiguration();

		$this->assign('config', $config);

		$this->setLayout('default');

		// Load CSS
		$document = JFactory::getDocument();
		$document->addStyleSheet('../media/com_admintools/css/backend.css');
		
		JHTML::_('behavior.mootools');

		require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'helpers'.DS.'select.php';

		parent::display();
	}
}