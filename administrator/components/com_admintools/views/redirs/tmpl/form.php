<?php
/**
 * @package AkeebaReleaseSystem
 * @copyright Copyright (c)2010-2011 Nicholas K. Dionysopoulos
 * @license GNU General Public License version 3, or later
 * @version $Id$
 */

defined('_JEXEC') or die('Restricted Access');

$editor =& JFactory::getEditor();
?>

<form name="adminForm" id="adminForm" action="index.php" method="post">
	<input type="hidden" name="option" value="<?php echo JRequest::getCmd('option') ?>" />
	<input type="hidden" name="view" value="<?php echo JRequest::getCmd('view') ?>" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="id" value="<?php echo $this->item->id ?>" />
	<input type="hidden" name="<?php echo JUtility::getToken();?>" value="1" />

	<fieldset>
		<div class="editform-row">
			<label for="title"><?php echo JText::_('ATOOLS_LBL_REDIRS_SOURCE'); ?></label>
			<input type="text" name="source" id="source" value="<?php echo $this->item->source ?>">
		</div>
		<div class="editform-row">
			<label for="alias"><?php echo JText::_('ATOOLS_LBL_REDIRS_DEST'); ?></label>
			<input type="text" name="dest" id="dest" value="<?php echo $this->item->dest ?>">
		</div>
		<div class="editform-row">
			<label for="published"><?php echo JText::_('PUBLISHED'); ?></label>
			<div>
				<?php echo JHTML::_('select.booleanlist', 'published', null, $this->item->published); ?>
			</div>
		</div>
		<div style="clear:left"></div>
	</fieldset>
</form>