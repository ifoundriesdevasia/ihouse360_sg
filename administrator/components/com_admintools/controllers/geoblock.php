<?php
/**
 *  @package AdminTools
 *  @copyright Copyright (c)2010-2011 Nicholas K. Dionysopoulos
 *  @license GNU General Public License version 3, or later
 *  @version $Id$
 */

// Protect from unauthorized access
defined('_JEXEC') or die('Restricted Access');

jimport('joomla.application.component.controller');

require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'controllers'.DS.'default.php';

class AdmintoolsControllerGeoblock extends AdmintoolsControllerDefault
{
	public function save()
	{
		// CSRF prevention
		if(!JRequest::getVar(JUtility::getToken(), false, 'POST')) {
			JError::raiseError('403', JText::_('Request Forbidden'));
		}
		
		$continents = JRequest::getVar('continent', array(), 'default', 'array', 2);
		if(empty($continents)) {
			$continents = '';
		} else {
			$continents = array_keys($continents);
			$continents = implode(',', $continents);
		}
		
		$countries = JRequest::getVar('country', array(), 'default', 'array', 2);
		if(empty($countries)) {
			$countries = '';
		} else {
			$countries = array_keys($countries);
			$countries = implode(',', $countries);
		}
		
		$model = $this->getThisModel();
		$config = array('countries' => $countries, 'continents' => $continents);
		$model->saveConfig($config);
		
		$option = JRequest::getCmd('option');
		$view = JRequest::getCmd('view');
		$textkey = 'ATOOLS_LBL_'.strtoupper($view).'_SAVED';
		$url = 'index.php?option='.$option.'&view=waf';
		$this->setRedirect($url, JText::_($textkey));
		$this->redirect();
	}
	
	public function cancel()
	{
		// Redirect to the display task
		$option = JRequest::getCmd('option');
		$url = 'index.php?option='.$option.'&view=waf';
		$this->setRedirect($url);
		$this->redirect();
	}
}