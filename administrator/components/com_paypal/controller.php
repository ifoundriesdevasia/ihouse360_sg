<?php
/**
 * @version		$Id: controller.php 11299 2008-11-22 01:40:44Z ian $
 * @package		Joomla
 * @subpackage	Paypal
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.controller');

/**
 * Users Component Controller
 *
 * @package		Joomla
 * @subpackage	Paypal
 * @since 1.5
 */
class PaypalController extends JController
{
	/**
	 * Constructor
	 *
	 * @params	array	Controller configuration array
	 */
	function __construct($config = array())
	{
		parent::__construct($config);
		$this->registerTask( 'edit'  , 	'edit'  );
		$this->registerTask( 'save'  , 	'save'  );

	}
	
	function display(){
		JRequest::setVar( 'edit', true );
		$model	= &$this->getModel( 'paypal' );
		$view	= &$this->getView( 'paypaldetails');
		$view->setModel( $model, true );
		$view->display();
	}
	
	function saveDetails()
	{
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$model 		= &$this->getModel('paypal');
		$id = JRequest::getCmd( 'id' );
		$response = $model->saveDetails($id);

		if($response){
		$msg = JText::_('You have successfully updated the Paypal configuration.');
		}else{
		$msg = JText::_('Error occured while updating the Paypal configuration. Please contact administrator for help.');
		}


		$this->setredirect( 'index.php?option=com_paypal',$msg);
	}
	
}
