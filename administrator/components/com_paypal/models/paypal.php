<?php

// no direct access
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.model');

/**
 * Content Component Paypal Model
 *
 * @package		Joomla
 * @subpackage	Paypal
 * @since		1.5
 */
class PaypalModelPaypal extends JModel
{
	function getDetail()
	{
		global $mainframe;			
		$db		=& $this->getDBO();
		$this->_object = $this->_getDetail($db);
	
		return $this->_object;
		
	}
	
	function _getDetail($db){
		
		$query = "SELECT * FROM #__paypal_config";
		$db->setQuery($query);

		$return = $db->loadObject();

		return $return;
	
	}
	
	function saveDetails($id){
		$db		=& $this->getDBO();

		$rowid = JRequest::getVar('id', '', 'post', 'id');
		$email = JRequest::getVar('email', '', 'post', 'email');
		$currency = JRequest::getVar('currency', '', 'post', 'currency');

		if (empty($rowid)){
			$query = "INSERT INTO #__paypal_config (id,email,currency)"
		
			. "\n VALUES ( NULL, '".$email."','".$currency."')";
			$db->setQuery( $query );
			$db->query();
			$return = true;
			
		} else {
		
		$query = "UPDATE #__paypal_config SET email = '" .$email. "', currency = '".$currency."' WHERE id = '".$rowid."'";
			$db->setQuery($query);
			$db->query();
			$return = true;
		}
		return $return;
	
	}
	
}

?>