<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php  JHTML::_('behavior.tooltip');  ?>
<?php /*?><script language="javascript" type="text/javascript">
function confirmPost(msg, val, inv) {
    if (confirm(msg)) {     
	   window.adminForm.paymentType.value = val;
	   window.adminForm.invoice.value = inv;
	   window.adminForm.submit();
    } else {
       return false;
    }
} 
</script><?php */?>

<?php

	$cid = $this->cid;
	$edit = JRequest::getVar('edit',true);
	$text = intval($edit) ? JText::_( 'Edit' ) : JText::_( 'New' );
	
	JToolBarHelper::title( JText::_( 'Paypal Config' ) . ': <small><small>[ '. $text .' ]</small></small>' , 'user.png' );
	//JToolBarHelper::apply('applyDetails');
	JToolBarHelper::save('saveDetails');
	//JToolBarHelper::cancel();

?>

<form action="index.php" method="post" name="adminForm" autocomplete="off">
	<div class="col width-45">
		<fieldset class="adminform">
		<legend><?php echo JText::_( 'Paypal Details' ); ?></legend>
			<table class="admintable" cellspacing="1">
				<tr>
					<td width="150" class="key">
						<label for="name">
							<?php echo JText::_( 'Email' ); ?>
						</label>
					</td>
					<td>
                    	<input type="text" name="email" id="email" class="inputbox" size="40" value="<?php echo $this->row->email;?>" />
					</td>
				</tr>
				<tr>
					<td class="key">
						<label for="username">
							<?php echo JText::_( 'Currency' ); ?>
						</label>
					</td>
					<td>
                    	<input type="text" name="currency" id="currency" class="inputbox" size="40" value="<?php echo $this->row->currency;?>" />
					</td>
				</tr>
			</table>
		</fieldset>
	</div>
	<div class="clr"></div>

	<input type="hidden" name="id" value="<?php echo $this->row->id ?>" />
	<input type="hidden" name="cid" value="<?php echo $this->row->id ?>" />
	<input type="hidden" name="option" value="com_paypal" />
	<input type="hidden" name="task" value="" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>