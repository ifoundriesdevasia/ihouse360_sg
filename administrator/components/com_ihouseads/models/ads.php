<?php

// no direct access
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.model');

/**
 * Content Component Paypal Model
 *
 * @package		Joomla
 * @subpackage	Transaction
 * @since		1.5
 */
class iHouseAdsModelAds extends JModel
{
	/*function getDetail()
	{
		global $mainframe;			
		$db		=& $this->getDBO();
		$this->_object = $this->_getDetail($db);
	
		return $this->_object;
		
	}
	
	
	function saveDetails($id){
		$db		=& $this->getDBO();

		$rowid = JRequest::getVar('id', '', 'post', 'id');
		$email = JRequest::getVar('email', '', 'post', 'email');
		$currency = JRequest::getVar('currency', '', 'post', 'currency');

		if (empty($rowid)){
			$query = "INSERT INTO #__paypal_config (id,email,currency)"
		
			. "\n VALUES ( NULL, '".$email."','".$currency."')";
			$db->setQuery( $query );
			$db->query();
			$return = true;
			
		} else {
		
		$query = "UPDATE #__paypal_config SET email = '" .$email. "', currency = '".$currency."' WHERE id = '".$rowid."'";
			$db->setQuery($query);
			$db->query();
			$return = true;
		}
		return $return;
	
	}*/
	
	function save($adsid = 0) {
		if(!$adsid)
			return false;
			
		$db		=& 	$this->getDBO();
		
		$ad_title	=	JRequest::getVar('ad_title'); 
		$add_info	=	JRequest::getVar('add_info');
		$publish	=	JRequest::getVar('publish');
		
		$query = " UPDATE #__ihouse_ads SET ad_title = ". $db->Quote( $db->getEscaped( $ad_title, true ), false )
					.	" , add_info = ".$db->Quote( $db->getEscaped( $add_info, true ), false )
					.	" , publish = ".$db->Quote( $db->getEscaped( $publish, true ), false )
					.	" WHERE id = '$adsid' ";
					
			$db->setQuery( $query );
			$db->query();
			
		return true;	
	}
	
	function remove($arrIds = array()) {
		if(empty($arrIds))
			return false;
		
		$db		=& 	$this->getDBO();
		
		$tmp	=	implode(',', $arrIds);
		
		$query = " DELETE FROM #__ihouse_ads WHERE id IN (".$tmp.") ";
					
			$db->setQuery( $query );
			$db->query();
			
		return true;	
	}
	function refresh($arrIds = array()) {
		if(empty($arrIds))
			return false;
		
		$db		=& 	$this->getDBO();
		
		$tmp	=	implode(',', $arrIds);
		
		$query = " UPDATE #__ihouse_ads SET posting_date = '".date("Y-m-d H:i:s", time())."' WHERE id IN (".$tmp.") ";
					
			$db->setQuery( $query );
			$db->query();
			
		return true;	
		
	}
}

?>