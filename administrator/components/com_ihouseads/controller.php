<?php
/**
 * @version		$Id: controller.php 11299 2008-11-22 01:40:44Z ian $
 * @package		Joomla
 * @subpackage	Transaction
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.controller');

/**
 * Users Component Controller
 *
 * @package		Joomla
 * @subpackage	Transaction
 * @since 1.5
 */
class iHouseAdsController extends JController
{
	/**
	 * Constructor
	 *
	 * @params	array	Controller configuration array
	 */
	function __construct($config = array())
	{
		parent::__construct($config);
		$this->registerTask( 'monthly'  , 	'monthlydetails'  );
		$this->registerTask( 'total'  , 	'totaldetails'  );

	}
	
	function display(){
		
		JRequest::setVar('view','ads');
		$model	= &$this->getModel( 'ads' );
		
		$user = &JFactory::getUser();
		$viewcache = JRequest::getVar('viewcache',1,'POST','INT');
		
		if ($user->get('id')) {
			parent::display(false);
		} else {
			parent::display(true);
		}
	}
	
	function edit() {
		JRequest::setVar('view','edit');
		$model	= &$this->getModel( 'ads' );
		
		$user = &JFactory::getUser();
		$viewcache = JRequest::getVar('viewcache',1,'POST','INT');
		
		if ($user->get('id')) {
			parent::display(false);
		} else {
			parent::display(true);
		}
	}
	
	function save() {
		JRequest::checkToken() or jexit( 'Invalid Token' );
		
		$view		= JRequest::getCmd('view');
		
		$model	= &$this->getModel( 'ads' );
		
		switch($view) {
			case 'edit' :
				$adsid 	= 	JRequest::getVar('adsid');
				if($model->save($adsid)) {
					$msg = JText::_( 'Ads has been saved' );
				} else {
					$msg = JText::_( 'Error: Ads not saved' );
				}
				
				$link = 'index.php?option=com_ihouseads';
				break;
		}

		$this->setRedirect($link, $msg);
	}
	
	function apply() {
		JRequest::checkToken() or jexit( 'Invalid Token' );
		
		$view		= JRequest::getCmd('view');
		
		$model	= &$this->getModel( 'ads' );
		
		switch($view) {
			case 'edit' :
				$adsid 	= 	JRequest::getVar('adsid');
				
				if($model->save($adsid)) {
					$msg = JText::_( 'Ads has been saved' );
				} else {
					$msg = JText::_( 'Error: Ads not saved' );
				}
			
				$link = 'index.php?option=com_ihouseads&task=edit&cid='.$adsid;
				
				break;
		}
		
		$this->setRedirect($link, $msg);
	}
	
	function remove() {
		JRequest::checkToken() or jexit( 'Invalid Token' );
		
		$model	= &$this->getModel( 'ads' );
		$arrAdsId 	= 	JRequest::getVar('cid', array());
		
		if($model->remove($arrAdsId)) {
			$msg = JText::_( 'Checked Ads have been removed' );
		} else {
			$msg = JText::_( 'Error: Ads Not Removed' );
		}
		
		$link = 'index.php?option=com_ihouseads';
		
		$this->setRedirect($link, $msg);
	}
	
	/* ifoundries */
	function refresh() {
		JRequest::checkToken() or jexit( 'Invalid Token' );
		
		$model	= &$this->getModel( 'ads' );
		$arrAdsId 	= 	JRequest::getVar('cid', array());
		
		if($model->refresh($arrAdsId)) {
			$msg = JText::_( 'Checked Ads have been refreshed' );
		} else {
			$msg = JText::_( 'Error: Ads Not Refreshed' );
		}
		
		$link = 'index.php?option=com_ihouseads';
		
		$this->setRedirect($link, $msg);
	}
	/*function getAmount()
	{
		
		
		$this->setredirect( 'index.php?option=com_transaction&view=daily');
		
	}
	*/
}
