﻿<?php defined('_JEXEC') or die('Restricted access'); ?>

<script type="text/javascript">
	var baseurl = '<?php echo JURI::base(true); ?>';
</script>

<style type="text/css">
textarea {
	width:500px;
	height:150px;
}
</style>
<?php JHTML::_('behavior.tooltip'); ?>
<?php JHTML::_('behavior.calendar'); ?>

<?php //JHTML::_('script', 'admin.ihouse.js', 'administrator/components/com_ihouse/assets/'); ?>
<?php
	
	JToolBarHelper::title( JText::_( 'iHouse360 : Edit Ads' ), 'plugin.png' );
	JToolBarHelper::save();
	JToolBarHelper::apply();
	JToolBarHelper::cancel();

	//JToolBarHelper::help( 'screen.plugins' );
	
	$ordering = ($this->lists['order'] == 'p.folder' || $this->lists['order'] == 'p.ordering');
	$rows =& $this->items;

?>
<div style="padding:5px;">
<form action="index.php" method="post" name="adminForm">

		<div class="col width-60">
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Ads : '. $this->data->ad_title ); ?></legend>
				<table class="admintable">
				<tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Ad Title' ); ?>:
						</label>
					</td>
					<td>
                    	<input class="inputbox" type="text" name="ad_title" size="60" maxlength="255" value="<?php echo $this->data->ad_title ?>" />
					</td>
				</tr>
                <tr>
                	<td class="key">
						<label for="name">
							<?php echo JText::_( ' Property Type ' ); ?>:
						</label>
					</td>
					<td>
						<?php //echo $this->propertyTypeHTML ?>
                        <?php echo $this->data->property_type ?>
					</td>
                </tr>
                <?php if($this->data->property_type == '公寓') : ?>
                <tr>
                	<td class="key">
						<label for="name">
							<?php echo JText::_( 'Property Name ' ); ?>:
						</label>
					</td>
					<td>
						<?php // echo $this->propertyNameHTML ?>
                        <?php echo $this->data->property_name ?>
					</td>
                </tr>
                <?php endif; ?>
                
                <?php if($this->data->property_type == '组屋') : ?>
                <tr>
                	<td class="key">
						<label for="name">
							<?php echo JText::_( 'HDB Town' ); ?>:
						</label>
					</td>
					<td>
                        <?php echo $this->data->hdb_town_id ?>
					</td>
                </tr>
                <?php endif; ?>
                <tr>
                	<td class="key">
						<label for="name">
							<?php echo JText::_( ' Blk # ' ); ?>:
						</label>
					</td>
					<td>
						<?php echo $this->data->block_no ?>
					</td>
                </tr>
                <tr>
                	<td class="key">
						<label for="name">
							<?php echo JText::_( 'Address ' ); ?>:
						</label>
					</td>
					<td>
						<?php echo $this->data->street_name ?>
					</td>
                </tr>
                <?php if($this->data->property_type == '公寓') : ?>
                <tr>
                	<td class="key">
						<label for="name">
							<?php echo JText::_( ' District ' ); ?>:
						</label>
					</td>
					<td>
						<?php echo $this->data->property_district_id ?>
					</td>
                </tr>
                <?php endif; ?>
                <tr>
                	<td class="key">
						<label for="name">
							<?php echo JText::_( ' Tenure ' ); ?>:
						</label>
					</td>
					<td>
						<?php echo $this->data->tenure ?>
					</td>
                </tr>
                <tr>
                	<td class="key">
						<label for="name">
							<?php echo JText::_( ' Top Year ' ); ?>:
						</label>
					</td>
					<td>
						<?php echo $this->data->top_year ?>
					</td>
                </tr>
                <tr>
                	<td class="key">
						<label for="name">
							<?php echo JText::_( ' Renovation ' ); ?>:
						</label>
					</td>
					<td>
						<?php echo $this->data->renovation ?>
					</td>
                </tr>
                <tr>
                	<td class="key">
						<label for="name">
							<?php echo JText::_( ' Status ' ); ?>:
						</label>
					</td>
					<td>
						<?php echo $this->data->status_id ?>
					</td>
                </tr>
                <tr>
                	<td class="key">
						<label for="name">
							<?php echo JText::_( ' Additional Info ' ); ?>:
						</label>
					</td>
					<td>
						<textarea name="add_info"><?php echo $this->data->add_info ?></textarea>
					</td>
                </tr>
                <tr>
                	<td class="key">
						<label for="name">
							<?php echo JText::_( 'Publish ' ); ?>:
						</label>
					</td>
					<td>
                    	<input type="radio" name="publish" value="1" <?php echo ($this->data->publish == 1)?' checked="yes"':''?> />Yes
                        <input type="radio" name="publish" value="0" <?php echo ($this->data->publish == 0)?' checked="yes"':''?> />No
					</td>
                </tr>
                 <tr>
                	<td class="key">
						<label for="name">
							<?php echo JText::_( ' Posting Date ' ); ?>:
						</label>
					</td>
					<td>
						<?php echo date("d M Y",strtotime($this->data->posting_date)) ?>
					</td>
                </tr>
				</table>
			</fieldset>

       </div>
       
       <div class="clr"></div>
       
    <input type="hidden" name="adsid" value="<?php echo $this->data->id ?>"  />   
    <input type="hidden" name="option" value="com_ihouseads" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="view" value="edit"  />
	<?php echo JHTML::_( 'form.token' ); ?>   
</form>   
</div>         