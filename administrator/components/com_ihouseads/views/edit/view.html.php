<?php
/**
* @version		$Id: view.html.php 10381 2008-06-01 03:35:53Z pasamio $
* @package		Joomla
* @subpackage	Transaction
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Process component
 *
 * @static
 * @package		Transaction
 * @subpackage	Transaction Details
 * @since 1.0
 */
class iHouseAdsViewEdit extends JView
{
	function display($tpl = null)
	{
		//$cid 	= JRequest::getVar( 'cid' );
		//$model		= &$this->getModel();
		//$row = $model->getDetail();

		global $mainframe, $option;

		$user		=& 	JFactory::getUser();
		
		$uid		=	$user->get('id');	
		
		$gid		=	$user->get('gid');
		
		$layout		= 	JRequest::getCmd('layout', 'default');
		$adsid		=	JRequest::getCmd('cid');

		$db 	  	=& 	JFactory::getDBO();
		
		$row		=	$this->getDetails($adsid);
		
		$this->assignRef('user',		JFactory::getUser());
		$this->assignRef('data', 		$row);
		
		$this->assignRef('propertyTypeHTML', $this->getPropertyTypeHTML($row));
		$this->assignRef('propertyNameHTML', $this->getPropertyNameHTML($row));
		
		parent::display($tpl);
	}
	
	function getDetails($adsid = 0) {
		
		if(!$adsid)
			return false;
		
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT * FROM #__ihouse_ads WHERE id = '$adsid' ";
			$db->setQuery( $query );
			$row	=	$db->loadObject();
			
		return $row;	
	}
	
	function getPropertyTypeHTML($obj = '') {
		$value 		=	$obj->property_type;
		
		$db 	  	=& 	JFactory::getDBO();
		
		$query 		= 	" SELECT * FROM #__ihouse_property_type ";
			$db->setQuery( $query );
			$rows	=	$db->loadObjectList();
		
		$html = '';
		$html .= '<select name="property_type">';
		foreach($rows as $r) : 
			$html .= '<option value="'.$r->name_ch.'" '.(($r->name_ch == $value)?' selected="selected" ':'').'>'.$r->name_ch.'</option>';
		endforeach;
		$html .= '</select>';
		
		return $html;
	}
	
	function getPropertyNameHTML($obj = '') {
		$value 		=	$obj->property_name;
		
		$db 	  	=& 	JFactory::getDBO();
		
		$query 		= 	" SELECT * FROM #__ihouse_property ";
			$db->setQuery( $query );
			$rows	=	$db->loadObjectList();
		
		$html = '';
		$html .= '<select name="property_name">';
		foreach($rows as $r) : 
			$html .= '<option value="'.$r->name_en.'" '.(($r->name_en == $value)?' selected="selected" ':'').'>'.$r->name_en.$r->name_ch.'</option>';
		endforeach;
		$html .= '</select>';
		
		return $html;
	}
}