<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('behavior.tooltip'); ?>

<?php
	
	JToolBarHelper::title( JText::_( 'iHouse360 Ads Management' ), 'plugin.png' );
	JToolBarHelper::custom('refresh', 'apply', 'apply', 'Refresh', false);
	JToolBarHelper::deleteList();
	//JToolBarHelper::addNewX();
	//JToolBarHelper::publishList();
	//JToolBarHelper::unpublishList();
	//JToolBarHelper::editListX();
	//JToolBarHelper::help( 'screen.plugins' );
	
	//$ordering = ($this->lists['order'] == 'p.folder' || $this->lists['order'] == 'p.ordering');
	$rows =& $this->items;

?>

<form action="index.php" method="post" name="adminForm">
<table>
	<tr>
    
		<td align="left" width="100%">
			<?php echo JText::_( 'Filter' ); ?>:
			<input type="text" name="search" id="search" value="<?php echo $this->lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />
			<button onclick="this.form.submit();"><?php echo JText::_( 'Go' ); ?></button>
			<button onclick="document.getElementById('search').value='';this.form.submit();"><?php echo JText::_( 'Reset' ); ?></button>
		</td>
		<td nowrap="nowrap">
        	<?php echo $this->propertyTypeFilterHTML ?>
       
			<?php
			//echo $this->lists['type'];
			//echo $this->lists['state']; publish or unpublish state
			?>
		</td>
       
	</tr>
</table>

<table class="adminlist">
<thead>
	<tr>
		<th width="2%">
			<?php echo JText::_( 'Num' ); ?>
		</th>
		<th width="5%">
			<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $rows );?>);" />
		</th>
        <th class="title" width="10%">
			<?php echo JHTML::_('grid.sort',   'Ads ID', 'a.id', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
		<th nowrap="nowrap" width="4%">
			<?php echo 'Ads View'; ?>
		</th>
		<th class="title" width="50%">
			<?php echo JHTML::_('grid.sort',   'Ads Title', 'a.ad_title', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
        <th nowrap="nowrap" width="8%">
			<?php echo JHTML::_('grid.sort',   'Prop. Type', 'a.property_type', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
		<th nowrap="nowrap" width="15%">
			<?php echo JHTML::_('grid.sort',   'Name', 'u.name', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
        <th nowrap="nowrap" width="10%">
			<?php echo JHTML::_('grid.sort',   'Refreshed Date', 'a.posting_date', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
        <th nowrap="nowrap" width="10%">
			<?php echo JHTML::_('grid.sort',   'Created Date', 'a.creation_date', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
	</tr>
</thead>
<tfoot>
	<tr>
		<td colspan="12">
			<?php echo $this->pagination->getListFooter(); ?>
		</td>
	</tr>
</tfoot>
<tbody>
<?php

	$k = 0;
	$n = count( $rows );
	
	for ($i=0; $i < $n; $i++) {
	$row 	= $rows[$i];

	$link = JRoute::_( 'index.php?option=com_ihouseads&task=edit&cid='. $row->id );

	//$access 	= JHTML::_('grid.access',   $row, $i );
	$checked 	= JHTML::_('grid.checkedout',   $row, $i );
	//$published 	= JHTML::_('grid.published', $row, $i );

?>
	<tr class="<?php echo "row$k"; ?>">
		<td align="right">
			<?php echo $this->pagination->getRowOffset( $i ); ?>
		</td>
		<td align="center">
			<?php echo $checked; ?>
		</td>
        <td align="center">
			<a href="<?php echo $link ?>"><?php echo $row->ads_id	?></a>
		</td>
		<td align="center">
            <!--/////////ads-view//////////-->
					<?php
							$db 	  	=& 	JFactory::getDBO();
							$query="SELECT counter FROM `#__ihouse_popular` WHERE `ads_id` = '".$row->ads_id."'";
							$db->setQuery($query);
							$counter = $db->loadResult();
							echo $counter;
					?>
		</td>
		<td align="center">
			<a href="<?php echo $link ?>"><?php echo $row->ad_title	?></a>
		</td>
         <td align="center">
			<?php echo $row->property_type ?>
		</td>
		<td align="center">
            <?php echo $row->firstname.$row->chinese_name; ?>
		</td>
        <td align="center">
            <?php echo date("d-M-Y H:i:s", strtotime($row->posting_date)); ?>
		</td>
        <td align="center">
            <?php echo date("d-M-Y", strtotime($row->creation_date)); ?>
		</td>
	</tr>
	<?php
		$k = 1 - $k;
	}
	?>
</tbody>

</table>

	<input type="hidden" name="option" value="com_ihouseads" />
	<input type="hidden" name="task" value="<?php echo $this->task ?>" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>