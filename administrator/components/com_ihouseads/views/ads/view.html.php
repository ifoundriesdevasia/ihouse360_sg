<?php
/**
* @version		$Id: view.html.php 10381 2008-06-01 03:35:53Z pasamio $
* @package		Joomla
* @subpackage	Transaction
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Process component
 *
 * @static
 * @package		Transaction
 * @subpackage	Transaction Details
 * @since 1.0
 */
class iHouseAdsViewAds extends JView
{
	function display($tpl = null)
	{
		//$cid 	= JRequest::getVar( 'cid' );
		//$model		= &$this->getModel();
		//$row = $model->getDetail();

		global $mainframe, $option;

		$user		=& 	JFactory::getUser();
		
		$uid		=	$user->get('id');	
		
		$gid		=	$user->get('gid');
		
		$layout		= 	JRequest::getCmd('layout', 'default');
		
		$task		=	JRequest::getCmd('task');
		
		$startover	=	JRequest::getCmd('start'); /* for Sorting purpose, when click on submenu at the top */

		$db 	  	=& 	JFactory::getDBO();
		
		$filter_order		= $mainframe->getUserStateFromRequest( "$option.filter_order",		'filter_order',		'u.id',	'cmd' );

		$filter_order_Dir	= $mainframe->getUserStateFromRequest( "$option.filter_order_Dir",	'filter_order_Dir',	'',		'word' );
		$filter_state		= $mainframe->getUserStateFromRequest( "$option.filter_state",		'filter_state',		'',		'word' );
		$search				= $mainframe->getUserStateFromRequest( "$option.search",			'search',			'',		'string' );
		$search				= JString::strtolower( $search );
		
		$proptype			= $mainframe->getUserStateFromRequest( "$option.proptype",			'proptype',			'',		'string' );
		
		$proptype				= JString::strtolower( $proptype );
		
		//if(!preg_match('/^p\./', $filter_order))
			//$filter_order	=	'p.id';
		
		$limit		= $mainframe->getUserStateFromRequest( 'global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int' );
		$limitstart	= $mainframe->getUserStateFromRequest( $option.'.limitstart', 'limitstart', 0, 'int' );
		
		$where = array();
		$whereor = array();
		
		if ($search)
		{
			$whereor[] = ' LOWER(a.ad_title) LIKE '.$db->Quote( '%'.$db->getEscaped( $search, true ).'%', false );
			$whereor[] = ' LOWER(a.postcode) LIKE '.$db->Quote( '%'.$db->getEscaped( $search, true ).'%', false );
			$whereor[] = ' LOWER(u.name) LIKE '.$db->Quote( '%'.$db->getEscaped( $search, true ).'%', false );
			$whereor[] = ' LOWER(u.chinese_name) LIKE '.$db->Quote( '%'.$db->getEscaped( $search, true ).'%', false );
			
			$where[] 	= ( count( $whereor ) ? implode( ' OR ', $whereor ) : '' );
			
		} 
		
		if ($proptype) {
			$where[] = ' LOWER(a.property_type) LIKE '.$db->Quote( '%'.$db->getEscaped( $proptype, true ).'%', false );
		}
		
		if($filter_order && $filter_order_Dir) {
			$orderby 	= ' ORDER BY '. $filter_order .' '. $filter_order_Dir;
		}
		
		if($startover) {
			$search 	= '';
			$limitstart = 0;
			$limit		= 20;
		}
		
		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
		
		$query	=	" SELECT a.*,a.id AS ads_id, u.name AS firstname,u.chinese_name FROM #__ihouse_ads AS a "
					.	" LEFT JOIN #__users AS u ON u.id = a.posted_by "
					.	$where
					.	$orderby ;
		
		
			$db->setQuery( $query , $limitstart, $limit);
			$rows = $db->loadObjectList();

		if ($db->getErrorNum())
		{
			echo $db->stderr();
			return false;
		}
		
		/* COUNT TOTAL */
		$query = " SELECT COUNT(a.id) AS firstname,u.chinese_name FROM #__ihouse_ads AS a "
					.	" LEFT JOIN #__users AS u ON u.id = a.posted_by "
					.	$where;
		
		$db->setQuery( $query );
		$total = $db->loadResult();
		
		jimport('joomla.html.pagination');
		$pagination = new JPagination( $total, $limitstart, $limit );
		
		// table ordering
		$lists['order_Dir']	= $filter_order_Dir;
		$lists['order']		= $filter_order;

		// search filter
		$lists['search'] = $search;
		$lists['proptype'] = $proptype;

		$this->assignRef('task', $task);
		$this->assignRef('user',		JFactory::getUser());
		$this->assignRef('lists',		$lists);
		$this->assignRef('items', $rows);
		$this->assignRef('pagination', $pagination);
		
		$this->assignRef('propertyTypeFilterHTML', $this->getPropertyTypeFilterHTML($proptype));
		parent::display($tpl);
	}
	
	function getPropertyTypeFilterHTML($value = '') {
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT * FROM #__ihouse_property_type ";
			$db->setQuery( $query );
			$rows	=	$db->loadObjectList();
		
		$html = '';
		$html .= '<select name="proptype" onchange="document.adminForm.submit();">';
		$html .= '<option value="">Property Type</option>';
		foreach($rows as $r) :
			$selected = '';
			
			if($r->name_ch == $value)
				$selected = ' selected="selected" ';
		
			$html .= '<option value="'.$r->name_ch.'" '.$selected.'>'.$r->name_ch.'</option>';
			
		endforeach;
		$html .= '</select>';
		
		return $html;
	}
}