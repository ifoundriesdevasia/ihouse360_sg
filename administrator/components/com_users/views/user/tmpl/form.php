<?php defined('_JEXEC') or die('Restricted access'); ?>
<?php JHTML::_('behavior.calendar'); ?>
<?php 
JHTML::_('behavior.tooltip'); 
require_once( JPATH_COMPONENT.DS.'image.php' );
?>

<?php
	/*if ($this->user->get('user_image') == '') {
			$this->user->get('user_image') = 'blank.png';
	}*/
	
	$cid = JRequest::getVar( 'cid', array(0) );
	$edit		= JRequest::getVar('edit',true);
	$text = intval($edit) ? JText::_( 'Edit' ) : JText::_( 'New' );

	JToolBarHelper::title( JText::_( 'User' ) . ': <small><small>[ '. $text .' ]</small></small>' , 'user.png' );
	JToolBarHelper::save();
	JToolBarHelper::apply();
	if ( $edit ) {
		// for existing items the button is renamed `close`
		JToolBarHelper::cancel( 'cancel', 'Close' );
	} else {
		JToolBarHelper::cancel();
	}
	JToolBarHelper::help( 'screen.users.edit' );
	$cparams = JComponentHelper::getParams ('com_media');
?>

<?php
	// clean item data
	JFilterOutput::objectHTMLSafe( $this->user, ENT_QUOTES, '' );

	if ($this->user->get('lastvisitDate') == "0000-00-00 00:00:00") {
		$lvisit = JText::_( 'Never' );
	} else {
		$lvisit	= JHTML::_('date', $this->user->get('lastvisitDate'), '%Y-%m-%d %H:%M:%S');
	}
?>
<script language="javascript" type="text/javascript">
	function submitbutton(pressbutton) {
		var form = document.adminForm;
		if (pressbutton == 'cancel') {
			submitform( pressbutton );
			return;
		}
		var r = new RegExp("[\<|\>|\"|\'|\%|\;|\(|\)|\&]", "i");

		// do field validation
		if (trim(form.name.value) == "") {
			alert( "<?php echo JText::_( 'You must provide a name.', true ); ?>" );
		} else if (form.username.value == "") {
			alert( "<?php echo JText::_( 'You must provide a user login name.', true ); ?>" );
		} else if (r.exec(form.username.value) || form.username.value.length < 2) {
			alert( "<?php echo JText::_( 'WARNLOGININVALID', true ); ?>" );
		} else if (trim(form.email.value) == "") {
			alert( "<?php echo JText::_( 'You must provide an email address.', true ); ?>" );
		} else if (form.gid.value == "") {
			alert( "<?php echo JText::_( 'You must assign user to a group.', true ); ?>" );
		} else if (((trim(form.password.value) != "") || (trim(form.password2.value) != "")) && (form.password.value != form.password2.value)){
			alert( "<?php echo JText::_( 'Password do not match.', true ); ?>" );
		} else if (form.gid.value == "29") {
			alert( "<?php echo JText::_( 'WARNSELECTPF', true ); ?>" );
		} else if (form.gid.value == "30") {
			alert( "<?php echo JText::_( 'WARNSELECTPB', true ); ?>" );
		} else {
			submitform( pressbutton );
		}
	}

	function gotocontact( id ) {
		var form = document.adminForm;
		form.contact_id.value = id;
		submitform( 'contact' );
	}
</script>
<form action="index.php" method="post" name="adminForm" autocomplete="off" enctype="multipart/form-data">
	<div class="col width-45">
		<fieldset class="adminform">
		<legend><?php echo JText::_( 'User Details Part 1' ); ?></legend>
			<table class="admintable" cellspacing="1">
				<tr>
					<td width="150" class="key">
						<label for="name">
							<?php echo JText::_( 'Name' ); ?>
						</label>
					</td>
					<td>
						<input type="text" name="name" id="name" class="inputbox" size="40" value="<?php echo $this->user->get('name'); ?>" />
					</td>
				</tr>
				<tr>
					<td class="key">
						<label for="username">
							<?php echo JText::_( 'Username' ); ?>
						</label>
					</td>
					<td>
						<input type="text" name="username" id="username" class="inputbox" size="40" value="<?php echo $this->user->get('username'); ?>" autocomplete="off" />
					</td>
				</tr>
				<tr>
					<td class="key">
						<label for="email">
							<?php echo JText::_( 'Email' ); ?>
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="email" id="email" size="40" value="<?php echo $this->user->get('email'); ?>" />
					</td>
				</tr>
				<tr>
					<td class="key">
						<label for="password">
							<?php echo JText::_( 'New Password' ); ?>
						</label>
					</td>
					<td>
						<?php if(!$this->user->get('password')) : ?>
							<input class="inputbox disabled" type="password" name="password" id="password" size="40" value="" disabled="disabled" />
						<?php else : ?>
							<input class="inputbox" type="password" name="password" id="password" size="40" value=""/>
						<?php endif; ?>
					</td>
				</tr>
				<tr>
					<td class="key">
						<label for="password2">
							<?php echo JText::_( 'Verify Password' ); ?>
						</label>
					</td>
					<td>
						<?php if(!$this->user->get('password')) : ?>
							<input class="inputbox disabled" type="password" name="password2" id="password2" size="40" value="" disabled="disabled" />
						<?php else : ?>
							<input class="inputbox" type="password" name="password2" id="password2" size="40" value=""/>
						<?php endif; ?>
					</td>
				</tr>
                <tr>
					<td class="key">
						<label for="email">
							<?php echo JText::_( 'User Category' ); ?>
						</label>
					</td>
					<td>
			<?php echo JHTML::_('select.genericlist',  $this->userOptions, 'user_category', 'class="inputbox" size="1"', 'value', 'text', $this->user->get('user_category') );?>
					</td>
				</tr>
				<tr>
					<td valign="top" class="key">
						<label for="gid">
							<?php echo JText::_( 'Group' ); ?>
						</label>
					</td>
					<td>
						<?php echo $this->lists['gid']; ?>
					</td>
				</tr>
				<?php if ($this->me->authorize( 'com_users', 'block user' )) { ?>
				<tr>
					<td class="key">
						<?php echo JText::_( 'Block User' ); ?>
					</td>
					<td>
						<?php echo $this->lists['block']; ?>
					</td>
				</tr>
				<?php } if ($this->me->authorize( 'com_users', 'email_events' )) { ?>
				<tr>
					<td class="key">
						<?php echo JText::_( 'Receive System Emails' ); ?>
					</td>
					<td>
						<?php echo $this->lists['sendEmail']; ?>
					</td>
				</tr>
				<?php } if( $this->user->get('id') ) { ?>
				<tr>
					<td class="key">
						<?php echo JText::_( 'Register Date' ); ?>
					</td>
					<td>
						<?php echo JHTML::_('date', $this->user->get('registerDate'), '%Y-%m-%d %H:%M:%S');?>
					</td>
				</tr>
				<tr>
					<td class="key">
						<?php echo JText::_( 'Last Visit Date' ); ?>
					</td>
					<td>
						<?php echo $lvisit; ?>
					</td>
				</tr>
				<tr>
					<td class="key">
						<?php echo JText::_( 'Profile Page View' ); ?>
					</td>
					<td>
						<?php
							$db 	  	=& 	JFactory::getDBO();
							$query="SELECT counter FROM `#__ihouse_popular2` WHERE `aid` = '".$this->user->get('id')."'";
							$db->setQuery($query);
							$counter = $db->loadResult();
							echo $counter;
						?>
					</td>
				</tr>
				<?php } ?>
			</table>
		</fieldset>
        <fieldset class="adminform">
		<legend><?php echo JText::_( 'Parameters' ); ?></legend>
			<table class="admintable">
				<tr>
					<td>
						<?php
							$params = $this->user->getParameters(true);
							echo $params->render( 'params' );
						?>
					</td>
				</tr>
			</table>
		</fieldset>
		<fieldset class="adminform">
		<legend><?php echo JText::_( 'Contact Information' ); ?></legend>
		<?php if ( !$this->contact ) { ?>
			<table class="admintable">
				<tr>
					<td>
						<br />
						<span class="note">
							<?php echo JText::_( 'No Contact details linked to this User' ); ?>:
							<br />
							<?php echo JText::_( 'SEECOMPCONTACTFORDETAILS' ); ?>.
						</span>
						<br /><br />
					</td>
				</tr>
			</table>
		<?php } else { ?>
			<table class="admintable">
				<tr>
					<td width="120" class="key">
						<?php echo JText::_( 'Name' ); ?>
					</td>
					<td>
						<strong>
							<?php echo $this->contact[0]->name;?>
						</strong>
					</td>
				</tr>
				<tr>
					<td class="key">
						<?php echo JText::_( 'Position' ); ?>
					</td>
					<td >
						<strong>
							<?php echo $this->contact[0]->con_position;?>
						</strong>
					</td>
				</tr>
				<tr>
					<td class="key">
						<?php echo JText::_( 'Telephone' ); ?>
					</td>
					<td >
						<strong>
							<?php echo $this->contact[0]->telephone;?>
						</strong>
					</td>
				</tr>
				<tr>
					<td class="key">
						<?php echo JText::_( 'Fax' ); ?>
					</td>
					<td >
						<strong>
							<?php echo $this->contact[0]->fax;?>
						</strong>
					</td>
				</tr>
				<tr>
					<td class="key">
						<?php echo JText::_( 'Misc' ); ?>
					</td>
					<td >
						<strong>
							<?php echo $this->contact[0]->misc;?>
						</strong>
					</td>
				</tr>
				<?php if ($this->contact[0]->image) { ?>
				<tr>
					<td class="key">
						<?php echo JText::_( 'Image' ); ?>
					</td>
					<td valign="top">
						<img src="<?php echo JURI::root() . $cparams->get('image_path') . '/' . $this->contact[0]->image; ?>" align="middle" alt="<?php echo JText::_( 'Contact' ); ?>" />
					</td>
				</tr>
				<?php } ?>
				<tr>
					<td class="key">&nbsp;</td>
					<td>
						<div>
							<br />
							<input class="button" type="button" value="<?php echo JText::_( 'change Contact Details' ); ?>" onclick="gotocontact( '<?php echo $this->contact[0]->id; ?>' )" />
							<i>
								<br /><br />
								'<?php echo JText::_( 'Components -> Contact -> Manage Contacts' ); ?>'
							</i>
						</div>
					</td>
				</tr>
			</table>
			<?php } ?>
		</fieldset>
	</div>
	<div class="col width-55">
		<fieldset class="adminform">
		<legend><?php echo JText::_( 'User Details Part 2' ); ?></legend>
			<table class="admintable" cellspacing="1">
				<tr>
					<td width="150" class="key">
						<label for="name">
							<?php echo JText::_( 'CEA Registration Number' ); ?>
						</label>
					</td>
					<td>
						<input type="text" name="cea_reg_no" id="cea_reg_no" class="inputbox" size="40" value="<?php echo $this->user->get('cea_reg_no'); ?>" />
					</td>
				</tr>
                <tr>
					<td class="key">
						<label for="email">
							<?php echo JText::_( 'Salute' ); ?>
						</label>
					</td>
					<td>
			<?php echo JHTML::_('select.genericlist',  $this->saluteOptions, 'salute', 'class="inputbox" size="1"', 'value', 'text', $this->user->get('salute') );?>
					</td>
				</tr>
				<tr>
					<td class="key">
						<label for="email">
							<?php echo JText::_( 'Chinese Name' ); ?>
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="chinese_name" id="chinese_name" size="40" value="<?php echo $this->user->get('chinese_name'); ?>" />
					</td>
				</tr>
                <tr>
					<td class="key">
						<label for="email">
							<?php echo JText::_( 'NRIC' ); ?>
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="nric" id="nric" size="40" value="<?php echo $this->user->get('nric'); ?>" />
					</td>
				</tr>
                <tr>
					<td class="key">
						<label for="email">
							<?php echo JText::_( 'Date of Birth' ); ?>
						</label>
					</td>
					<td>
                    <input class="inputbox" type="text" name="dob" id="dob" size="40" value="<?php echo $this->user->get('dob'); ?>" />
<input type="reset" value="..." onclick="return showCalendar('dob','%Y-%m-%d');" />
					</td>
				</tr>
                <tr>
					<td class="key">
						<label for="email">
							<?php echo JText::_( 'Country' ); ?>
						</label>
					</td>
					<td>
                    	<?php echo JHTML::_('select.genericlist',  $this->countryOptions, 'country', 'class="inputbox" size="1"', 'value', 'text', $this->user->get('country') );?>
					</td>
				</tr>
                <tr>
					<td class="key">
						<label for="email">
							<?php echo JText::_( 'Residence Status' ); ?>
						</label>
					</td>
					<td>
                    	 <?php echo JHTML::_('select.genericlist',  $this->residenceOptions, 'residence_status', 'class="inputbox" size="1"', 'value', 'text', $this->user->get('residence_status') );?>
					</td>
				</tr>
                <tr>
					<td class="key">
						<label for="email">
							<?php echo JText::_( 'Mobile Number' ); ?>
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="mobile_contact" id="mobile_contact" size="40" value="<?php echo $this->user->get('mobile_contact'); ?>" />
					</td>
				</tr>
                <tr>
					<td class="key">
						<label for="email">
							<?php echo JText::_( 'Home Number' ); ?>
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="home_contact" id="home_contact" size="40" value="<?php echo $this->user->get('home_contact'); ?>" />
					</td>
				</tr>
                <tr>
					<td class="key">
						<label for="email">
							<?php echo JText::_( 'Telephone 1' ); ?>
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="phone_1" id="phone_1" size="40" value="<?php echo $this->user->get('phone_1'); ?>" />
					</td>
				</tr>
                <tr>
					<td class="key">
						<label for="email">
							<?php echo JText::_( 'Telephone 2' ); ?>
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="phone_2" id="phone_2" size="40" value="<?php echo $this->user->get('phone_2'); ?>" />
					</td>
				</tr>
                <tr>
					<td class="key">
						<label for="email">
							<?php echo JText::_( 'Fax' ); ?>
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="fax" id="fax" size="40" value="<?php echo $this->user->get('fax'); ?>" />
					</td>
				</tr>
                <tr>
					<td class="key">
						<label for="email">
							<?php echo JText::_( 'Home Address' ); ?>
						</label>
					</td>
					<td>
                    	<textarea name="home_address" id="home_address" cols="30" rows="3"><?php echo $this->user->get('home_address'); ?></textarea>
					</td>
				</tr>
                <tr>
					<td class="key">
						<label for="email">
							<?php echo JText::_( 'Home Postal Code' ); ?>
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="home_postalcode" id="home_postalcode" size="40" value="<?php echo $this->user->get('home_postalcode'); ?>" />
					</td>
				</tr>
                <tr>
					<td class="key">
						<label for="email">
							<?php echo JText::_( 'Organization Name' ); ?>
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="organization_name" id="organization_name" size="40" value="<?php echo $this->user->get('organization_name'); ?>" />
					</td>
				</tr>
                <tr>
					<td class="key">
						<label for="email">
							<?php echo JText::_( 'Organization Address' ); ?>
						</label>
					</td>
					<td>
                    	<textarea name="organization_address" id="organization_address" cols="30" rows="3"><?php echo $this->user->get('organization_address'); ?></textarea>
					</td>
				</tr>
                <tr>
					<td class="key">
						<label for="email">
							<?php echo JText::_( 'Rental Address' ); ?>
						</label>
					</td>
					<td>
                    	<textarea name="rental_address" id="rental_address" cols="30" rows="3"><?php echo $this->user->get('rental_address'); ?></textarea>
					</td>
				</tr>
                <tr>
					<td class="key">
						<label for="email">
							<?php echo JText::_( 'Rental Postal Code' ); ?>
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="rental_postalcode" id="rental_postalcode" size="40" value="<?php echo $this->user->get('rental_postalcode'); ?>" />
					</td>
				</tr>
                <tr>
					<td class="key">
						<label for="email">
							<?php echo JText::_( 'Company Name' ); ?>
						</label>
					</td>
					<td>
                    	<textarea name="company_name" id="company_name" cols="30" rows="3"><?php echo $this->user->get('company_name'); ?></textarea>
					</td>
				</tr>
                <tr>
					<td class="key">
						<label for="email">
							<?php echo JText::_( 'Company Logo' ); ?>
						</label>
					</td>
					<td>
                        <?php
						if($this->user->get('company_logo')) {
							echo '<b>Name of Image:</b><br>'.$this->user->get('company_logo');
							}
							imageUpload($this->user->get('company_logo'),$this->user->get('company_logo'),'company_logo');
							?>
						<?php echo $this->user->get('company_logo'); ?>
					</td>
				</tr>
                <tr>
					<td class="key">
						<label for="email">
							<?php echo JText::_( 'User Image' ); ?>
						</label>
					</td>
					<td>
                       <?php
						if($this->user->get('user_image')) {
							echo '<b>Name of Image:</b><br>'.$this->user->get('user_image');
							}
							imageUpload($this->user->get('user_image'),$this->user->get('user_image'),'user_image');
							?>
						<?php echo $this->user->get('user_image'); ?>

					</td>
				</tr>
                <tr>
					<td class="key">
						<label for="email">
							<?php echo JText::_( 'Website' ); ?>
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="website" id="website" size="40" value="<?php echo $this->user->get('website'); ?>" />
					</td>
				</tr>
                <tr>
					<td class="key">
						<label for="email">
							<?php echo JText::_( 'User Interest In' ); ?>
						</label>
					</td>
					<td>
                    	<?php echo JHTML::_('select.genericlist',  $this->userintOptions, 'user_interest', 'class="inputbox" size="1"', 'value', 'text', $this->user->get('user_interest') );?>
					</td>
				</tr>
                <tr>
					<td class="key">
						<label for="email">
							<?php echo JText::_( 'Merchant Interest In' ); ?>
						</label>
					</td>
					<td>
                    	<?php echo JHTML::_('select.genericlist',  $this->merchantintOptions, 'merchant_interest', 'class="inputbox" size="1"', 'value', 'text', $this->user->get('merchant_interest') );?>
					</td>
				</tr>
                <tr>
					<td class="key">
						<label for="email">
							<?php echo JText::_( 'Agree to receive' ); ?>
						</label>
					</td>
					<td>
        				<input type="checkbox" value="ihouse360 Newsletter" id="receive[]" name="receive[]" <?php if(in_array('ihouse360 Newsletter', $this->receive)) { echo "checked"; }?> />ihouse360 Newsletter<input type="checkbox" value="3rd Party Newsletter" id="receive[]" name="receive[]" <?php if(in_array('3rd Party Newsletter', $this->receive)) { echo "checked"; }?> />3rd Party Newsletter<input type="checkbox" value="Email Alert When Customer Enquiry" id="receive[]" name="receive[]" <?php if(in_array('Email Alert When Customer Enquiry', $this->receive)) { echo "checked"; }?> />Email Alert When Customer Enquiry
                   
					</td>
				</tr>
			</table>
		</fieldset>
	</div>
	<div class="clr"></div>

	<input type="hidden" name="id" value="<?php echo $this->user->get('id'); ?>" />
	<input type="hidden" name="cid[]" value="<?php echo $this->user->get('id'); ?>" />
	<input type="hidden" name="option" value="com_users" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="contact_id" value="" />
	<?php if (!$this->me->authorize( 'com_users', 'email_events' )) { ?>
	<input type="hidden" name="sendEmail" value="0" />
	<?php } ?>
	<?php echo JHTML::_( 'form.token' ); ?>
</form>
