<?php
/**
* @version		$Id: view.html.php 14401 2010-01-26 14:10:00Z louis $
* @package		Joomla
* @subpackage	Users
* @copyright	Copyright (C) 2005 - 2010 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Users component
 *
 * @static
 * @package		Joomla
 * @subpackage	Users
 * @since 1.0
 */
class UsersViewUser extends JView
{
	function display($tpl = null)
	{
		$cid		= JRequest::getVar( 'cid', array(0), '', 'array' );
		$edit		= JRequest::getVar('edit',true);
		$me 		= JFactory::getUser();
		JArrayHelper::toInteger($cid, array(0));

		$db 		=& JFactory::getDBO();
		if($edit)
			$user 		=& JUser::getInstance( $cid[0] );
		else
			$user 		=& JUser::getInstance();

		$myuser		=& JFactory::getUser();
		$acl		=& JFactory::getACL();

		// Check for post data in the event that we are returning
		// from a unsuccessful attempt to save data
		$post = JRequest::get('post');
		if ( $post ) {
			$user->bind($post);
		}

		if ( $user->get('id'))
		{
			$query = 'SELECT *'
			. ' FROM #__contact_details'
			. ' WHERE user_id = '.(int) $cid[0]
			;
			$db->setQuery( $query );
			$contact = $db->loadObjectList();
		}
		else
		{
			$contact 	= NULL;
			// Get the default group id for a new user
			$config		= &JComponentHelper::getParams( 'com_users' );
			$newGrp		= $config->get( 'new_usertype' );
			$user->set( 'gid', $acl->get_group_id( $newGrp, null, 'ARO' ) );
		}

		$userObjectID 	= $acl->get_object_id( 'users', $user->get('id'), 'ARO' );
		$userGroups 	= $acl->get_object_groups( $userObjectID, 'ARO' );
		$userGroupName 	= strtolower( $acl->get_group_name( $userGroups[0], 'ARO' ) );

		$myObjectID 	= $acl->get_object_id( 'users', $myuser->get('id'), 'ARO' );
		$myGroups 		= $acl->get_object_groups( $myObjectID, 'ARO' );
		$myGroupName 	= strtolower( $acl->get_group_name( $myGroups[0], 'ARO' ) );;

		// ensure user can't add/edit group higher than themselves
		/* NOTE : This check doesn't work commented out for the time being
		if ( is_array( $myGroups ) && count( $myGroups ) > 0 )
		{
			$excludeGroups = (array) $acl->get_group_children( $myGroups[0], 'ARO', 'RECURSE' );
		}
		else
		{
			$excludeGroups = array();
		}

		if ( in_array( $userGroups[0], $excludeGroups ) )
		{
			echo 'not auth';
			$mainframe->redirect( 'index.php?option=com_users', JText::_('NOT_AUTH') );
		}
		*/

		/*
		if ( $userGroupName == 'super administrator' )
		{
			// super administrators can't change
	 		$lists['gid'] = '<input type="hidden" name="gid" value="'. $currentUser->gid .'" /><strong>'. JText::_( 'Super Administrator' ) .'</strong>';
		}
		else if ( $userGroupName == $myGroupName && $myGroupName == 'administrator' ) {
		*/

		if ( $userGroupName == $myGroupName && $myGroupName == 'administrator' )
		{
			// administrators can't change each other
			$lists['gid'] = '<input type="hidden" name="gid" value="'. $user->get('gid') .'" /><strong>'. JText::_( 'Administrator' ) .'</strong>';
		}
		else
		{
			$gtree = $acl->get_group_children_tree( null, 'USERS', false );

			// remove users 'above' me
			//$i = 0;
			//while ($i < count( $gtree )) {
			//	if ( in_array( $gtree[$i]->value, (array)$excludeGroups ) ) {
			//		array_splice( $gtree, $i, 1 );
			//	} else {
			//		$i++;
			//	}
			//}

			$lists['gid'] 	= JHTML::_('select.genericlist',   $gtree, 'gid', 'size="10"', 'value', 'text', $user->get('gid') );
		}

		// build the html select list
		$lists['block'] 	= JHTML::_('select.booleanlist',  'block', 'class="inputbox" size="1"', $user->get('block') );

		// build the html select list
		$lists['sendEmail'] = JHTML::_('select.booleanlist',  'sendEmail', 'class="inputbox" size="1"', $user->get('sendEmail') );
		
		//added by cleo
		$userOptions[] = JHTML::_('select.option', '', JText::_('Please Select'));
		$userOptions[] = JHTML::_('select.option', 'Individual', JText::_('Individual'));
		$userOptions[] = JHTML::_('select.option', 'Merchant', JText::_('Merchant'));
		$userOptions[] = JHTML::_('select.option', 'Owner', JText::_('Owner'));
		$userOptions[] = JHTML::_('select.option', 'Agent', JText::_('Agent'));
		
		$saluteOptions[] = JHTML::_('select.option', '', JText::_('Please Select'));
		$saluteOptions[] = JHTML::_('select.option', 'Mr', JText::_('Mr'));
		$saluteOptions[] = JHTML::_('select.option', 'Mrs', JText::_('Mrs'));
		$saluteOptions[] = JHTML::_('select.option', 'Mdm', JText::_('Mdm'));
		$saluteOptions[] = JHTML::_('select.option', 'Miss', JText::_('Miss'));
		$saluteOptions[] = JHTML::_('select.option', 'Dr', JText::_('Dr'));
		
		$residenceOptions[] = JHTML::_('select.option', '', JText::_('Please Select'));
		$residenceOptions[] = JHTML::_('select.option', 'Singapore Citizen', JText::_('Singapore Citizen'));
		$residenceOptions[] = JHTML::_('select.option', 'Singapore PR', JText::_('Singapore PR'));
		$residenceOptions[] = JHTML::_('select.option', 'Others', JText::_('Others'));
		
		$userintOptions[] = JHTML::_('select.option', '', JText::_('Please Select'));
		$userintOptions[] = JHTML::_('select.option', 'Rent', JText::_('Rent Property'));
		$userintOptions[] = JHTML::_('select.option', 'Buy', JText::_('Buy Property'));
		$userintOptions[] = JHTML::_('select.option', 'None', JText::_('None'));
		
		$merchantintOptions[] = JHTML::_('select.option', '', JText::_('Please Select'));
		$merchantintOptions[] = JHTML::_('select.option', 'Place Ads', JText::_('Place Ads'));
		$merchantintOptions[] = JHTML::_('select.option', 'EDM Services', JText::_('EDM Services'));
		$merchantintOptions[] = JHTML::_('select.option', 'Receive Free Events Invitation', JText::_('Receive Free Events Invitation'));
		
		$country_list = array(

		"Afghanistan",
	
		"Albania",
	
		"Algeria",
	
		"Andorra",
	
		"Angola",
	
		"Antigua and Barbuda",
	
		"Argentina",
	
		"Armenia",
	
		"Australia",
	
		"Austria",
	
		"Azerbaijan",
	
		"Bahamas",
	
		"Bahrain",
	
		"Bangladesh",
	
		"Barbados",
	
		"Belarus",
	
		"Belgium",
	
		"Belize",
	
		"Benin",
	
		"Bhutan",
	
		"Bolivia",
	
		"Bosnia and Herzegovina",
	
		"Botswana",
	
		"Brazil",
	
		"Brunei",
	
		"Bulgaria",
	
		"Burkina Faso",
	
		"Burundi",
	
		"Cambodia",
	
		"Cameroon",
	
		"Canada",
	
		"Cape Verde",
	
		"Central African Republic",
	
		"Chad",
	
		"Chile",
	
		"China",
	
		"Colombi",
	
		"Comoros",
	
		"Congo (Brazzaville)",
	
		"Congo",
	
		"Costa Rica",
	
		"Cote d��Ivoire",
	
		"Croatia",
	
		"Cuba",
	
		"Cyprus",
	
		"Czech Republic",
	
		"Denmark",
	
		"Djibouti",
	
		"Dominica",
	
		"Dominican Republic",
	
		"East Timor (Timor Timur)",
	
		"Ecuador",
	
		"Egypt",
	
		"El Salvador",
	
		"Equatorial Guinea",
	
		"Eritrea",
	
		"Estonia",
	
		"Ethiopia",
	
		"Fiji",
	
		"Finland",
	
		"France",
	
		"Gabon",
	
		"Gambia, The",
	
		"Georgia",
	
		"Germany",
	
		"Ghana",
	
		"Greece",
	
		"Grenada",
	
		"Guatemala",
	
		"Guinea",
	
		"Guinea-Bissau",
	
		"Guyana",
	
		"Haiti",
	
		"Honduras",
	
		"Hungary",
	
		"Iceland",
	
		"India",
	
		"Indonesia",
	
		"Iran",
	
		"Iraq",
	
		"Ireland",
	
		"Israel",
	
		"Italy",
	
		"Jamaica",
	
		"Japan",
	
		"Jordan",
	
		"Kazakhstan",
	
		"Kenya",
	
		"Kiribati",
	
		"Korea, North",
	
		"Korea, South",
	
		"Kuwait",
	
		"Kyrgyzstan",
	
		"Laos",
	
		"Latvia",
	
		"Lebanon",
	
		"Lesotho",
	
		"Liberia",
	
		"Libya",
	
		"Liechtenstein",
	
		"Lithuania",
	
		"Luxembourg",
	
		"Macedonia",
	
		"Madagascar",
	
		"Malawi",
	
		"Malaysia",
	
		"Maldives",
	
		"Mali",
	
		"Malta",
	
		"Marshall Islands",
	
		"Mauritania",
	
		"Mauritius",
	
		"Mexico",
	
		"Micronesia",
	
		"Moldova",
	
		"Monaco",
	
		"Mongolia",
	
		"Morocco",
	
		"Mozambique",
	
		"Myanmar",
	
		"Namibia",
	
		"Nauru",
	
		"Nepa",
	
		"Netherlands",
	
		"New Zealand",
	
		"Nicaragua",
	
		"Niger",
	
		"Nigeria",
	
		"Norway",
	
		"Oman",
	
		"Pakistan",
	
		"Palau",
	
		"Panama",
	
		"Papua New Guinea",
	
		"Paraguay",
	
		"Peru",
	
		"Philippines",
	
		"Poland",
	
		"Portugal",
	
		"Qatar",
	
		"Romania",
	
		"Russia",
	
		"Rwanda",
	
		"Saint Kitts and Nevis",
	
		"Saint Lucia",
	
		"Saint Vincent",
	
		"Samoa",
	
		"San Marino",
	
		"Sao Tome and Principe",
	
		"Saudi Arabia",
	
		"Senegal",
	
		"Serbia and Montenegro",
	
		"Seychelles",
	
		"Sierra Leone",
	
		"Singapore",
	
		"Slovakia",
	
		"Slovenia",
	
		"Solomon Islands",
	
		"Somalia",
	
		"South Africa",
	
		"Spain",
	
		"Sri Lanka",
	
		"Sudan",
	
		"Suriname",
	
		"Swaziland",
	
		"Sweden",
	
		"Switzerland",
	
		"Syria",
	
		"Taiwan",
	
		"Tajikistan",
	
		"Tanzania",
	
		"Thailand",
	
		"Togo",
	
		"Tonga",
	
		"Trinidad and Tobago",
	
		"Tunisia",
	
		"Turkey",
	
		"Turkmenistan",
	
		"Tuvalu",
	
		"Uganda",
	
		"Ukraine",
	
		"United Arab Emirates",
	
		"United Kingdom",
	
		"United States",
	
		"Uruguay",
	
		"Uzbekistan",
	
		"Vanuatu",
	
		"Vatican City",
	
		"Venezuela",
	
		"Vietnam",
	
		"Yemen",
	
		"Zambia",
	
		"Zimbabwe"
	
		);
	
		$countryOptions[] = JHTML::_('select.option', '', JText::_('Please Select'));
		foreach ($country_list as $country) {
		$countryOptions[] = JHTML::_('select.option', $country,$country);
		}
		
		$receive = explode(",", $user->get('agree_to_receive'));

		$this->assignRef('me', 		$me);
		$this->assignRef('lists',	$lists);
		$this->assignRef('user',	$user);
		$this->assignRef('contact',	$contact);
		$this->assignRef('userOptions', $userOptions );
		$this->assignRef('saluteOptions', $saluteOptions );
		$this->assignRef('residenceOptions', $residenceOptions );
		$this->assignRef('userintOptions', $userintOptions );
		$this->assignRef('merchantintOptions', $merchantintOptions );
		$this->assignRef('countryOptions', $countryOptions );
		$this->assignRef('receive', $receive );

		parent::display($tpl);
	}
}
