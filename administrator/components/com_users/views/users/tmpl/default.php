<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php  JHTML::_('behavior.tooltip');  ?>

<?php
	JToolBarHelper::title( JText::_( 'User Manager' ), 'user.png' );
	JToolBarHelper::custom( 'logout', 'cancel.png', 'cancel_f2.png', 'Logout' );
	JToolBarHelper::deleteList();
	JToolBarHelper::editListX();
	JToolBarHelper::addNewX();
	JToolBarHelper::help( 'screen.users' );
?>

<form action="index.php?option=com_users" method="post" name="adminForm">
	<table>
		<tr>
			<td width="100%">
				<?php echo JText::_( 'Filter' ); ?>:
				<input type="text" name="search" id="search" value="<?php echo htmlspecialchars($this->lists['search']);?>" class="text_area" onchange="document.adminForm.submit();" />
				<button onclick="this.form.submit();"><?php echo JText::_( 'Go' ); ?></button>
				<button onclick="document.getElementById('search').value='';this.form.getElementById('filter_type').value='0';this.form.getElementById('filter_logged').value='0';this.form.submit();"><?php echo JText::_( 'Reset' ); ?></button>
			</td>
			<td nowrap="nowrap">
				<?php echo $this->lists['type'];?>
				<?php echo $this->lists['logged'];?>
                <?php echo $this->lists['accts']; ?>
			</td>
		</tr>
	</table>

	<table class="adminlist" cellpadding="1">
		<thead>
			<tr>
				<th width="2%" class="title">
					<?php echo JText::_( 'NUM' ); ?>
				</th>
				<th width="3%" class="title">
					<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->items); ?>);" />
				</th>
				<th width="10% class="title">
					<?php echo JHTML::_('grid.sort',   'Name', 'a.name', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
				</th>
				<th width="15%" class="title" >
					<?php echo JHTML::_('grid.sort',   'Username', 'a.username', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
				</th>
				<th width="1%" class="title" nowrap="nowrap">
					<?php echo JHTML::_('grid.sort',   'ID', 'a.id', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
				</th>
				<th width="5%" class="title" nowrap="nowrap">
					<?php echo JText::_( 'Logged In' ); ?>
				</th>
				<th width="5%" class="title" nowrap="nowrap">
					<?php echo JHTML::_('grid.sort',   'Enabled', 'a.block', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
				</th>
				<th width="10%" class="title">
					<?php echo JHTML::_('grid.sort',   'Group', 'groupname', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
				</th>
                <th width="10%" class="title">
					<?php echo JHTML::_('grid.sort',   'User Category', 'user_category', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
				</th>
				<th width="15%" class="title">
					<?php echo JHTML::_('grid.sort',   'E-Mail', 'a.email', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
				</th>
				<th width="10%" class="title">
					<?php echo JHTML::_('grid.sort',   'Register/LastV Date', 'a.lastvisitdate', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
				</th>
				<th width="1%" class="title" nowrap="nowrap">
					<?php echo JHTML::_('grid.sort',   'Cea', 'a.cea_reg_no', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
				</th>
				<th width="10%" class="title" nowrap="nowrap">
					<?php echo JHTML::_('grid.sort',   'Mobile', 'a.mobile_contact', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
				</th>
				<th width="4%" class="title" nowrap="nowrap">
					<?php echo 'Plan'; ?>
				</th>
				<th width="4%" class="title" nowrap="nowrap">
					<?php echo 'View'; ?>
				</th>
				<th width="4%" class="title" nowrap="nowrap">
					<?php echo 'Balance'; ?>
				</th>
				<th width="4%" class="title" nowrap="nowrap">
					<?php echo 'Ads'; ?>
				</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="17">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
		<?php
			$k = 0;
			for ($i=0, $n=count( $this->items ); $i < $n; $i++)
			{
				$row 	=& $this->items[$i];

				$img 	= $row->block ? 'publish_x.png' : 'tick.png';
				$task 	= $row->block ? 'unblock' : 'block';
				$alt 	= $row->block ? JText::_( 'Enabled' ) : JText::_( 'Blocked' );
				$link 	= 'index.php?option=com_users&amp;view=user&amp;task=edit&amp;cid[]='. $row->id. '';
				$regdate	= JHTML::_('date', $row->registerDate, '%Y-%m-%d');
				
				if ($row->lastvisitDate == "0000-00-00 00:00:00") {
					$lvisit = JText::_( 'Never' );
				} else {
					$lvisit	= JHTML::_('date', $row->lastvisitDate, '%Y-%m-%d');
				}
			?>
			<tr class="<?php echo "row$k"; ?>">
				<td>
					<?php echo $i+1+$this->pagination->limitstart;?>
				</td>
				<td>
					<?php echo JHTML::_('grid.id', $i, $row->id ); ?>
				</td>
				<td>
					<a href="<?php echo $link; ?>">
						<?php echo $row->name; ?>  <?php echo $row->chinese_name; ?></a>
				</td>
				<td>
					<?php echo $row->username; ?>
				</td>
				<td>
					<a href='http://singapore.ihouse360.com/index.php?option=com_user&view=agent_profile&id=<?php echo $row->id; ?>&Itemid=136' target=_blank><?php echo $row->id; ?></a>
				</td>
				<td align="center">
					<?php echo $row->loggedin ? '<img src="images/tick.png" width="16" height="16" border="0" alt="" />': ''; ?>
				</td>
				<td align="center">
					<a href="javascript:void(0);" onclick="return listItemTask('cb<?php echo $i;?>','<?php echo $task;?>')">
						<img src="images/<?php echo $img;?>" width="16" height="16" border="0" alt="<?php echo $alt; ?>" /></a>
				</td>
				<td>
					<?php echo JText::_( $row->groupname ); ?>
				</td>
                <td>
					<?php echo JText::_( $row->user_category ); ?>
				</td>
				<td>
					<a href="mailto:<?php echo $row->email; ?>">
						<?php echo $row->email; ?></a>
				</td>
				<td nowrap="nowrap">
					<u><?php echo $regdate; ?></u>&nbsp;I&nbsp;<u><?php echo $lvisit; ?></u>
				</td>
				<td>
					<?php echo $row->cea_reg_no ?>
				</td>
				<td>
					<?php echo $row->mobile_contact; ?>
				</td>
				<td>
					<!--/////////<?php var_dump($row->plan_id); ?>//////////-->
					<?php
							$db 	  	=& 	JFactory::getDBO();
							$query="SELECT plan_id FROM `#__cbsubs_subscriptions` WHERE `user_id` = '".$row->id."' AND `status` = 'A' AND (`plan_id` = '3' OR `plan_id` = '2')";
							$db->setQuery($query);
							$plan_id = $db->loadResult();
					?>
					<?php 
						$str = "";
						switch($plan_id)
						{
						   case 2:
								$str = 'Quarterly';
							   break;
						   case 3:
								$str = 'Yearly';
							   break;
						   default:
								$str = 'PerPay';
							   break;
						}
						echo $str;
					?>
					<!--///////////////////-->
				</td>
				<td>
					<!--/////////profile-view//////////-->
					<?php
							$db 	  	=& 	JFactory::getDBO();
							$query="SELECT counter FROM `#__ihouse_popular2` WHERE `aid` = '".$row->id."'";
							$db->setQuery($query);
							$counter = $db->loadResult();
							echo $counter;
					?>
				</td>
				<td>
					<!--/////////credit//////////-->
					<?php
						$creditlink = JRoute::_('index.php?option=com_ihouse&view=credit&layout=edit&cid='.$row->id);
						$addcreditlink = JRoute::_('index.php?option=com_ihouse&view=credit&layout=add&cid='.$row->id);
						
							$db 	  	=& 	JFactory::getDBO();
							$query="SELECT credit FROM `#__ihouse_users_credit` WHERE `user_id` = '".$row->id."'";
							$db->setQuery($query); 
							$credit = $db->loadResult();
							if(($credit || $credit == 0)) {
								echo '<a href="'.$creditlink.'">';
								echo $credit;
								echo '</a>';
							} 
							
							if($credit == null) {
								echo '<a href="'.$addcreditlink.'">';
								echo 'Add Credit';
								echo '</a>';
							}
					?>
				</td>
				<td>
					<!--/////////Ads No//////////-->
					<?php
							$db 	  	=& 	JFactory::getDBO();
							$query="SELECT COUNT(*) FROM `#__ihouse_ads` WHERE `posted_by` = '".$row->id."'";
							$db->setQuery($query);
							$no = $db->loadResult();
							echo $no;
					?>
				</td>
			</tr>
			<?php
				$k = 1 - $k;
				}
			?>
		</tbody>
	</table>

	<input type="hidden" name="option" value="com_users" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>