<?php
/**
* @version		1.5.0
* @package		Transaction
* @subpackage	Transaction
* @copyright	Copyright ?2005 - 2009 iFoundries (Asia Pacific) Pte Ltd. All Rights Reserved.
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// Require the base controller
require_once (JPATH_COMPONENT.DS.'controller.php');

// Create the controller
$controller	= new iHouseCommentController( );

// Perform the Request task
$controller->execute( JRequest::getCmd('task'));
$controller->redirect();