<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('behavior.tooltip'); ?>

<?php
	
	JToolBarHelper::title( JText::_( 'iHouse360 Review / Comments Management ' ), 'plugin.png' );
	JToolBarHelper::deleteList();
	//JToolBarHelper::addNewX();
	//JToolBarHelper::publishList();
	//JToolBarHelper::unpublishList();
	//JToolBarHelper::editListX();
	//JToolBarHelper::help( 'screen.plugins' );
	
	//$ordering = ($this->lists['order'] == 'p.folder' || $this->lists['order'] == 'p.ordering');
	$rows =& $this->items;

?>

<form action="index.php" method="post" name="adminForm">
<table>
	<tr>
    
		<td align="left" width="100%">
			<?php echo JText::_( 'Filter' ); ?>:
			<input type="text" name="search" id="search" value="<?php echo $this->lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />
			<button onclick="this.form.submit();"><?php echo JText::_( 'Go' ); ?></button>
			<button onclick="document.getElementById('search').value='';this.form.submit();"><?php echo JText::_( 'Reset' ); ?></button>
		</td>
		<td nowrap="nowrap">
			<?php
			echo $this->lists['type'];
			//echo $this->lists['state']; publish or unpublish state
			?>
		</td>
       
	</tr>
</table>

<table class="adminlist">
<thead>
	<tr>
		<th width="3%">
			<?php echo JText::_( 'Num' ); ?>
		</th>
		<th width="3%">
			<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $rows );?>);" />
		</th>
		<th nowrap="nowrap" width="15%">
			<?php echo JHTML::_('grid.sort',   'Property Name', 'p.name_en', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
<!--		<th nowrap="nowrap" width="15%">
			<?php echo JText::_('Post By'); ?>
		</th>
		<th nowrap="nowrap" width="15%">
			<?php echo JText::_('Category'); ?>
		</th> -->
        <th nowrap="nowrap" width="15%">
			<?php echo JHTML::_('grid.sort',   'Title', 'r.title', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
        <th nowrap="nowrap" width="64%">
			<?php echo JHTML::_('grid.sort',   'Message', 'r.message', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
	</tr>
</thead>
<tfoot>
	<tr>
		<td colspan="12">
			<?php echo $this->pagination->getListFooter(); ?>
		</td>
	</tr>
</tfoot>
<tbody>
<?php

	$k = 0;
	$n = count( $rows );
	
	for ($i=0; $i < $n; $i++) {
	$row 	= $rows[$i];

	//$link = JRoute::_( 'index.php?option=com_ihousecomment&task=seereview&cid='. $row->id );

	//$access 	= JHTML::_('grid.access',   $row, $i );
	$checked 	= JHTML::_('grid.checkedout',   $row, $i );
	//$published 	= JHTML::_('grid.published', $row, $i );

?>
	<tr class="<?php echo "row$k"; ?>">
		<td align="right">
			<?php echo $this->pagination->getRowOffset( $i ); ?>
		</td>
		<td align="center">
			<?php echo $checked; ?>
		</td>
		<td align="center">
			<?php
							$db 	  	=& 	JFactory::getDBO();
							$query="SELECT postcode FROM `#__ihouse_property` WHERE `id` = '".$row->property_id."'";
							$db->setQuery($query);
							$postcode = $db->loadResult();
					?>
			<a href='http://singapore.ihouse360.com/index.php?option=com_ihouse&view=ihouse&layout=property_detail&id=<?php echo $row->property_id; ?>&postcode=<?php echo $postcode; ?>' target=_blank><?php echo $row->name_en.$row->name_ch; ?></a>
		</td>
		<!--
		<td align="center">
					<?php
							$db 	  	=& 	JFactory::getDBO();
							$query="SELECT name FROM `#__users` WHERE `id` = '".$this->user->get('id')."'";
							$db->setQuery($query);
							$name = $db->loadResult();
							echo $name;
					?>
		</td>
		<td align="center">
					<?php
							$db 	  	=& 	JFactory::getDBO();
							$query="SELECT user_category FROM `#__users` WHERE `id` = '".$this->user->get('id')."'";
							$db->setQuery($query);
							$category = $db->loadResult();
							echo $category;
					?>
		</td>
		-->
        <td align="center">
            <?php echo $row->rev_title; ?>
		</td>
        <td align="center">
            <?php echo $row->rev_msg; ?>
		</td>
	</tr>
	<?php
		$k = 1 - $k;
	}
	?>
</tbody>

</table>

	<input type="hidden" name="option" value="com_ihousecomment" />
	<input type="hidden" name="task" value="<?php echo $this->task ?>" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
	<?php echo JHTML::_( 'form.token' ); ?>
  
</form>