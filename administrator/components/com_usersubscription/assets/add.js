function loadCondo(section, district) {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();
	
	var url = 'index.php?option=com_usersubscription&task=ajaxLoadCondo&r=' + unixtime_ms;	
	var req = new Ajax(url, {
	   			data		: 	
								{
									'section'	:	section,
									'district' 	: 	district
								},
				method		: "post",
				onRequest	: function() {
					/*$$('.ajax-loader').setStyle('display', '');*/
				},
	    		onComplete	: function(response) {
					var obj = Json.evaluate(response);
					var condo = obj.html;

					$('ajax-load-condo').setHTML(condo);
	   			},
				evalScripts: true
		}).request();
}

function loadAds(property_id) {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();
	
	var url = 'index.php?option=com_usersubscription&task=ajaxLoadAds&r=' + unixtime_ms;	
	var req = new Ajax(url, {
	   			data		: 	
								{
									'property_id' 	: property_id
								},
				method		: "post",
				onRequest	: function() {
					/*$$('.ajax-loader').setStyle('display', '');*/
				},
	    		onComplete	: function(response) {
					var obj = Json.evaluate(response);
					var ads = obj.html;

					if(!ads) {
						ads = 'No Ads for this Condo';
					}
					$('ajax-load-ads').setHTML(ads);
	   			},
				evalScripts: true
		}).request();
}
/*function toggle(id) {
	
	var t = $(id).getStyle('display');

	if(t == '') {
		$(id).setStyle('display', 'none');
	} else {
		$(id).setStyle('display','');
	}
	
}
*/