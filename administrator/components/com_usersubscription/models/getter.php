<?php

// no direct access
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.model');

/**
 * Content Component Paypal Model
 *
 * @package		Joomla
 * @subpackage	Transaction
 * @since		1.5
 */
class UsersubscriptionModelGetter extends JModel
{
	function getPropertyListHTML($section, $district) {
		$db = $this->getDBO();
		
		$query = " SELECT * FROM #__ihouse_property WHERE district_id = '$district' ";
			$db->setQuery( $query );
			$rows = $db->loadObjectList();
		
		if($section == 'type7' || $section == 'type4' || $section == 'type5' || $section == 'type6') {
			$javascript = " javascript:loadAds(this.value) ";
		} else {
			$javascript = " javascript:void(0) ";
		}
		
		$html = '';
		$html .= '<select name="property" onchange="'.$javascript.'">';
		$html .= '<option name="">Select Property</option>';
		foreach($rows as $r) :
			$html .= '<option value="'.$r->id.'">'.$r->name_en.' - '.$r->name_ch.'</option>';
		endforeach;
		$html .= '</select>';
		
		return $html;
	}
	function getAdsForThisCondo($property_id) {
		$db = $this->getDBO();
		
		$query = " SELECT * FROM #__ihouse_ads WHERE property_id = '$property_id' ";
			$db->setQuery( $query );
			$rows = $db->loadObjectList();
		
		$html = '';
		$html .= '<select name="ads">';
		$html .= '<option name="">Select Ads</option>';
		foreach($rows as $r) :
			$html .= '<option value="'.$r->id.'">'.$r->property_name.' - '.$r->ad_title.'</option>';
		endforeach;
		$html .= '</select>';
		
		return $html;
	}
}

?>