<?php

// no direct access
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.model');

/**
 * Content Component Paypal Model
 *
 * @package		Joomla
 * @subpackage	Transaction
 * @since		1.5
 */
class UsersubscriptionModelSubscription extends JModel
{
	function edit($subsid, $planid) {
		
		if(!$planid)
			return false;
		
		$db			=& 	$this->getDBO();
		
		$query = " SELECT * FROM #__cbsubs_subscriptions WHERE id = '$subsid' ";
			$db->setQuery( $query );
			$row = $db->loadObject();
		
		$expirydate = $row->expiry_date;
		
		$query = " SELECT validity FROM #__cbsubs_plans WHERE id = '$planid' ";
	
			$db->setQuery($query);
			$validity = $db->loadResult(); //duration

		$date = explode("-", $validity);
		$year = $date[0];
		$month = $date[1];
		$day = substr($date[2], 0, 2);

		$eH		= date('H', strtotime($expirydate));
		$ei		= date('i', strtotime($expirydate)) + 1;
		$es		= date('s', strtotime($expirydate));
		$em		= date('m', strtotime($expirydate)) + $month;
		$ed		= date('d', strtotime($expirydate)) + $day;
		$eY		= date('Y', strtotime($expirydate)) + $year;
		
		$last_renew	=	date("Y-m-d H:i:s", time());
		$expirydate = date("Y-m-d H:i:s", mktime($eH,$ei,$es,$em,$ed,$eY));	
		
		if($row->status == 'A') { // subs actived
			
		} 
		// elseif($row->status == 'X') { // subs expired
			
		//}
		
		$query = " UPDATE #__cbsubs_subscriptions " 
				.	" SET expiry_date = ". $db->Quote( $db->getEscaped( $expirydate, true ), false )
				.	" , last_renewed_date = ". $db->Quote( $db->getEscaped( $last_renew, true ), false )
				.	" , plan_id = ". $db->Quote( $db->getEscaped( $planid, true ), false )
				.	" , status = 'A' "
				.	" WHERE id = '$subsid' ";
				
			$db->setQuery( $query );
			$db->query();
		
		return true;
	}
	function add($tasktype) {
		
		$db			=& 	$this->getDBO();
		$whereslot	=	array();
		
		$user		=	JFactory::getUser();
		
		$plan_id	=	JRequest::getVar('plan');
		$ads_id		=	JRequest::getVar('ads',0); // type4,5,6,7
		
		if($ads_id) { $obj	=	$this->getUserAds($ads_id); }
			
		$ip 		= 	$_SERVER['REMOTE_ADDR'];
		
		switch($tasktype) {
			case 'type1': // Guru
				$user_id			=	JRequest::getVar('userid'); // agent
				$property_id 		= 	JRequest::getVar('property');
				$ads_type_config 	= 	1;

				$whereslot[] 		= 	" property_id = '$property_id' ";
				break;
			case 'type2': // Condo Specialist
				$user_id			=	JRequest::getVar('userid'); // agent
				$property_id 		= 	JRequest::getVar('property');
				$ads_type_config 	= 	2;

				$whereslot[] 		= 	" property_id = '$property_id' ";
				break;
			
			case 'type3': // Condo Expert
				$user_id			=	JRequest::getVar('userid'); // agent
				$ads_type_config 	= 	3;

				break;
			case 'type4': // Featured
				
				$user_id			=	$obj->posted_by;
				$property_type 		= 	$obj->ad_type;				
				$ads_type_config 	= 	4;

				$whereslot[] 		= 	" property_type = '$property_type' ";
				break;
			case 'type5': // Featured
				$user_id			=	$obj->posted_by;
				$property_type 		= 	$obj->ad_type;				
				$ads_type_config 	= 	5;
				$property_id 		= 	JRequest::getVar('property');

				$whereslot[] 		= 	" property_id = '$property_id' ";
				break;
			case 'type6': // Featured
				$user_id			=	$obj->posted_by;
				$property_type 		= 	$obj->ad_type;				
				$ads_type_config 	= 	6;

				$whereslot[] 		= 	" property_type = '$property_type' ";
				break;
			case 'type7': // New Condo Sales
				$user_id			=	$obj->posted_by;
				$property_type 		= 	$obj->ad_type;				
				$ads_type_config 	= 	7;

				break;
		}
		$whereslot[] = " ads_type_config = ". $ads_type_config;
		
		
		// 1. GET VALIDITY / DURATION
		$query = " SELECT validity FROM #__cbsubs_plans WHERE id = '$plan_id' ";
	
			$db->setQuery($query);
			$validity = $db->loadResult(); //duration

		$date = explode("-", $validity);
		$year = $date[0];
		$month = $date[1];
		$day = substr($date[2], 0, 2);

		$expirydate = date("Y-m-d H:i:s", mktime(date("H"),date("i")+1,date("s"),date("m")+$month,date("d")+$day,date("Y")+$year));
		
		
		// 2. Insert SUBSCRIPTION
		$query = "INSERT INTO #__cbsubs_subscriptions (id,status,user_id,plan_id,parent_plan,parent_subscription,replaces_plan,replaces_subscription,subscription_date,last_renewed_date,expiry_date,next_event_date,next_event_id,next_event_params,autorenew_type,autorecurring_type,regular_recurrings_total,regular_recurrings_used,previous_recurrings_used,previous_expiry_date,previous_status,ip_addresses,integrations)"
	
		. "\n VALUES ( NULL, 'A', '".$user_id."', '".$plan_id."', '0', '0', NULL, NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,'".$expirydate."', NULL, '0', '', '0', '0', '0', '1', '0', NULL, 'R', '".$ip."', '')";
			
			$db->setQuery( $query );
			$db->query();
			
		$sid = $db->insertid();
		
		
		// 3. Insert into ADTYPE
		$query = "INSERT INTO #__ihouse_ads_type(id,ads_type_config,subscription_id,property_id,ads_id,user_id,property_type,duration,start_date,end_date)"
	
		. "\n VALUES ( NULL, '$ads_type_config', '$sid', '$property_id', '$ads_id', '$user_id', '$property_type', '', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
		$db->setQuery( $query );
		$db->query();
		
		
		// 4. -1 for space 
		
		$whereslot 		= 	( count( $whereslot ) ? ' WHERE ' . implode( ' AND ', $whereslot ) : '' );
		
		$sql = "UPDATE #__ihouse_ads_type_slot SET space = space - 1 "
				. $whereslot;
					$db->setQuery($sql);
					$db->query();
		
		return true;
	}
	
	function remove($subs_id = array()) {
		
		if(empty($subs_id))
			return false;
		
		$db		=& $this->getDBO();
		
		foreach($subs_id as $s) :
		
			$query = "  SELECT a.ads_type_config, a.property_type, a.property_id, cb.status "
					.	" FROM #__ihouse_ads_type AS a "
					.	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = a.subscription_id "
					.	" WHERE a.subscription_id = '$s' LIMIT 1 ";
				$db->setQuery( $query );
				$row = $db->loadObject();
			
			$ads_type_config 	= 	(int) $row->ads_type_config;
			$property_type		=	$row->property_type;
			$property_id		=	$row->property_id;
			
			$where = array();
			$where[] = " ads_type_config = '$ads_type_config' ";
			
			switch($ads_type_config) {
				case 1:
				case 2:
				case 5:
					$where[] = " property_id = '$property_id' ";
					break;
					
				case 4:
				case 6:
					$where[] = " property_type = '$property_type' ";
					break;
			
				case 3:
				case 7:
					break;	
			}
			
			$where 		= 	( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
			
			// ADD SPACE +1, when status active, else, do nothing
			if($row->status == 'A') {
				$query = " UPDATE #__ihouse_ads_type_slot SET space = space + 1 ". $where;
					$db->setQuery( $query );
					$db->query();
			}
		endforeach;

		$query = " DELETE FROM #__cbsubs_subscriptions WHERE id IN (".implode(',',$subs_id).") ";
			$db->setQuery( $query );
			$db->query();
		
		$query = " DELETE FROM #__ihouse_ads_type WHERE subscription_id IN (".implode(',',$subs_id).") ";
			$db->setQuery( $query );
			$db->query();
			
		return true;	
	}
	
	function checkType($tasktype) {
		
		$has_plan 	= JRequest::getVar('plan','');
		
		$tmp = new StdClass;
		$tmp->msg = 'Successful Checked';
		$tmp->status = true;
		
		if(empty($has_plan)) { 
			$tmp->msg = "Please complete all fields : Plan ";
			$tmp->status = false;
			return $tmp; 
		}
		
		switch($tasktype) {
			case 'type1': $ads_type_config = 1; break;
			case 'type2': $ads_type_config = 2; break;
			case 'type3': $ads_type_config = 3; break;
			case 'type4': $ads_type_config = 4; break;
			case 'type5': $ads_type_config = 5; break;
			case 'type6': $ads_type_config = 6; break;
			case 'type7': $ads_type_config = 7; break;
		}
		
		switch($tasktype) {
			case 'type1': // Guru
			case 'type2': // Condo Specialist
				$has_agent 	= 	JRequest::getVar('userid','');
				$has_condo	=	JRequest::getVar('property',''); // property_id
				
				if(empty($has_agent)) { 
					$tmp->msg = "Please complete all fields : Agent ";
					$tmp->status = false;
					return $tmp; 
				}
				if(empty($has_condo)) { 
					$tmp->msg = "Please complete all fields : Condo ";
					$tmp->status = false;
					return $tmp; 
				}
				
				$obj = new StdClass;
				$obj->property_id = $has_condo;
				$obj->property_type = '';
				$obj->ads_type_config = $ads_type_config;
				
				return $this->__checkSlot($obj);
				break;
			case 'type3': // Condo Expert
				$has_agent 	= 	JRequest::getVar('userid','');
				if(empty($has_agent)) {
					$tmp->msg = "Please complete all fields : Agent ";
					$tmp->status = false;
					return $tmp; 	
				}
				
				break;
			case 'type4': // Featured
			case 'type5': // Featured
			case 'type6': // Featured
			case 'type7': // New Condo Sales
				$has_ad 	= JRequest::getVar('ads',''); // ads_id
				if(empty($has_ad)) { 
					$tmp->msg = "Please complete all fields : Ads ";
					$tmp->status = false;
					return $tmp; 
				}
				
				$tmp	=	$this->getUserAds($has_ad);
				
				$obj = new StdClass;
				$obj->property_id = $has_condo;
				$obj->property_type = $tmp->ad_type;
				$obj->ads_type_config = $ads_type_config;
				
				return $this->__checkSlot($obj);
				break;
		}
		return $tmp;
	}
	function checkSlot($tasktype) { // single slot for type 3 and 7
		global $mainframe;
		
		$db		=& $this->getDBO();
		
		$tmp = new StdClass;
		$tmp->msg = 'Successful Checked Slot';
		$tmp->status = true;
		
		$adstype = 0;
		
		switch($tasktype) { 
			case 'type1':
			case 'type2':
			case 'type4': // sell or rent type
			case 'type5': // every condo have 8 space 
			case 'type6': // sell or rent type
				return true; // because they have many sub-category for this, set to true
				break;
			case 'type3':
				$adstype = 3;
				break;
			
			case 'type7': // New Condo Sales
				$adstype = 7;
				break;
		}
		$query = " SELECT space FROM #__ihouse_ads_type_slot WHERE ads_type_config = '$adstype' ";
			$db->setQuery( $query );
			$space = (int) $db->loadResult();
		
		if($space == 0)
			return false;
			
		return true;
	}
	function getUserAds($ads_id)
	{
		global $mainframe;			
		
		$db		=& $this->getDBO();
		
		$query = " SELECT * FROM #__ihouse_ads WHERE id = '$ads_id' LIMIT 1 ";
			$db->setQuery( $query );
			$row = $db->loadObject();
			
		return $row;
		
	}
	function __checkSlot($obj) { // many slots for type 1,2,4,5,6,7
		global $mainframe;			
		
		$tmp = new StdClass;
		$tmp->msg = 'Successful Checked';
		$tmp->status = true;
		
		$db		=& $this->getDBO();
		
		$property_id = $obj->property_id;
		$property_type = $obj->property_type;
		$ads_type_config = $obj->ads_type_config;
		
		$where = array();
		
		if($property_id) { $where[] = " property_id = '$property_id' "; }
		if($property_type) { $where[] = " property_type = '$property_type' "; }
		if($ads_type_config) { $where[] = " ads_type_config = '$ads_type_config' "; }
		
		$where 		= 	( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
		
		$query = " SELECT space FROM #__ihouse_ads_type_slot "
				.	$where;
				
			$db->setQuery( $query );
			$space = $db->loadResult();
		
		if($space == 0) {
			$tmp->msg = " Sorry. No more slots ";
			$tmp->status = false;
		}
		return $tmp;
	}
	
	
	/*function saveDetails($id){
		$db		=& $this->getDBO();

		$rowid = JRequest::getVar('id', '', 'post', 'id');
		$email = JRequest::getVar('email', '', 'post', 'email');
		$currency = JRequest::getVar('currency', '', 'post', 'currency');

		if (empty($rowid)){
			$query = "INSERT INTO #__paypal_config (id,email,currency)"
		
			. "\n VALUES ( NULL, '".$email."','".$currency."')";
			$db->setQuery( $query );
			$db->query();
			$return = true;
			
		} else {
		
		$query = "UPDATE #__paypal_config SET email = '" .$email. "', currency = '".$currency."' WHERE id = '".$rowid."'";
			$db->setQuery($query);
			$db->query();
			$return = true;
		}
		return $return;
	
	}*/
	
}

?>