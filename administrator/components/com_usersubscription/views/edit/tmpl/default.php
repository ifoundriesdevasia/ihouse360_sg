<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('behavior.tooltip'); ?>
<?php JHTML::_('behavior.calendar'); ?>
<?php JHTML::_('script', 'add.js', 'administrator/components/com_usersubscription/assets/'); ?>
<?php
	JToolBarHelper::title( JText::_( 'iHouse360 Add '. $this->tasktype ), 'plugin.png' );
	JToolBarHelper::save();
	JToolBarHelper::apply();
?>

<form action="index.php" method="post" name="adminForm">

	<div class="col">
		<fieldset class="adminform">
		<legend><?php echo JText::_( 'Info' ); ?></legend>
		<table class="admintable" cellspacing="1">
        	<?php if($this->tasktype == 'type1' ||
					 $this->tasktype == 'type2' ||
					 $this->tasktype == 'type4' ||
					 $this->tasktype == 'type5' ||
					 $this->tasktype == 'type6' ||
					 $this->tasktype == 'type7') : ?>
        	<tr>
				<td width="150" class="key">
					<label for="name">
						<?php echo JText::_( 'District' ); ?>
					</label>
				</td>
				<td>
					<?php echo $this->district->code.' - '.$this->district->district_en ?>
				</td>
			</tr>
            <tr>
				<td width="150" class="key">
					<label for="name">
						<?php echo JText::_( 'Condo' ); ?>
					</label>
				</td>
				<td>
					<?php 
						if($this->info->name_en) 
							echo $this->info->name_en;
						else
							if($this->property_name)
								echo $this->property_name;
							else
								echo 'N/A';
					?>	
				</td>
			</tr>
            <?php endif ?>
            
            
            
            <?php if($this->tasktype == 'type1' || $this->tasktype == 'type2' || $this->tasktype == 'type3') : ?>
            <tr>
				<td width="150" class="key">
					<label for="name">
						<?php echo JText::_( 'Agent' ); ?>
					</label>
				</td>
				<td>
					<?php echo $this->agent->name ?>
				</td>
			</tr>
            
        	<?php endif; ?>
        
        
        	<?php if($this->tasktype == 'type7' || 
					 $this->tasktype == 'type4' || 
					 $this->tasktype == 'type5' ||
					 $this->tasktype == 'type6' ) : ?>
            <tr>
				<td width="150" class="key">
					<label for="name">
						<?php echo JText::_( 'Ad title' ); ?>
					</label>
				</td>
				<td>
					<?php echo $this->info->ad_title ?>
				</td>
			</tr>
            <?php endif; ?>
            <tr>
				<td width="150" class="key">
					<label for="name">
						<?php echo JText::_( 'Current Plan' ); ?>
					</label>
				</td>
				<td>
                	<?php echo $this->info->subsname; ?>
				</td>
			</tr>
            <tr>
				<td width="150" class="key">
					<label for="name">
						<?php echo JText::_( 'Status Plan' ); ?>
					</label>
				</td>
				<td>
                	<?php 
						switch($this->info->status) {
							case 'X' : echo 'Expired'; break;
							case 'A' : echo 'Active'; break;
						} 
					?>
				</td>
			</tr>
            <tr>
				<td width="150" class="key">
					<label for="name">
						<?php echo JText::_( 'Start Date' ); ?>
					</label>
				</td>
				<td>
                	<?php echo $this->info->subscription_date; ?>
				</td>
			</tr>
            <tr>
				<td width="150" class="key">
					<label for="name">
						<?php echo JText::_( 'Last Renewed Date' ); ?>
					</label>
				</td>
				<td>
                	<?php echo $this->info->last_renewed_date; ?>
				</td>
			</tr>
            <tr>
				<td width="150" class="key">
					<label for="name">
						<?php echo JText::_( 'Expiry Date' ); ?>
					</label>
				</td>
				<td>
                	<?php echo $this->info->expiry_date; ?>
				</td>
			</tr>
            <tr>
				<td width="150" class="key">
					<label for="name">
						<?php echo JText::_( 'Extend Plan' ); ?>
					</label>
				</td>
				<td>
                <?php if($this->info->status == 'A') : 
                	 	echo $this->plan ;
                 		else : 
                	    echo 'Expired! Cannot Extend';
                		endif; ?>    
				</td>
			</tr>	
			</table>
		</fieldset>
	</div>

	<input type="hidden" name="option" value="com_usersubscription" />
	<input type="hidden" name="task" value="" />
    <input type="hidden" name="tasktype" value="<?php echo $this->tasktype ?>" />
    <input type="hidden" name="section" value="edit" />
    <input type="hidden" name="subsid" value="<?php echo $this->subsid ?>" />
    
	<?php echo JHTML::_( 'form.token' ); ?>
  
</form>