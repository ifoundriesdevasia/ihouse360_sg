<?php
/**
* @version		$Id: view.html.php 10381 2008-06-01 03:35:53Z pasamio $
* @package		Joomla
* @subpackage	Transaction
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Process component
 *
 * @static
 * @package		Transaction
 * @subpackage	Transaction Details
 * @since 1.0
 */
class UsersubscriptionViewEdit extends JView
{
	function display($tpl = null)
	{
		//$cid 	= JRequest::getVar( 'cid' );
		//$model		= &$this->getModel();
		//$row = $model->getDetail();

		global $mainframe, $option;

		$user		=& 	JFactory::getUser();
		
		$uid		=	$user->get('id');	
		
		$gid		=	$user->get('gid');
		
		$layout		= 	JRequest::getCmd('layout', 'default');
		
		$cid		=	JRequest::getCmd('cid');
		
		$task		=	JRequest::getCmd('task');
		
		$tasktype	=	JRequest::getCmd('tasktype');
		
		$startover	=	JRequest::getCmd('start'); /* for Sorting purpose, when click on submenu at the top */

		$db 	  	=& 	JFactory::getDBO();

		// table ordering
		$lists['order_Dir']	= $filter_order_Dir;
		$lists['order']		= $filter_order;

		// search filter
		$lists['search']= $search;
		//$this->assignRef('row', 			$row );
		$this->assignRef('subsid', $cid);
		$this->assignRef('tasktype', $tasktype);
		$this->assignRef('task', $task);
		$this->assignRef('user',		JFactory::getUser());
		$this->assignRef('lists',		$lists);
		$this->assignRef('items', $rows);
		$this->assignRef('pagination', $pagination);
		
		$info = $this->getInfoSubscription($cid);
		
		$districtid = '';
		
		if(!empty($info->property_district_id)) // ads
			$districtid = $info->property_district_id;
		
		if(!empty($info->district_id)) // property
			$districtid = $info->district_id;
			
		$this->assignRef('info', $info);
		$this->assignRef('district', $this->getDistrictInfo($districtid));
		$this->assignRef('plan', $this->getPlanHTML($tasktype));
		$this->assignRef('agent', $this->getAgents($cid));
		parent::display($tpl);
	}
	function getInfoSubscription($cid) {
		$db		=& 	JFactory::getDBO();
		
		$query = " SELECT p.*,a.*,cb.expiry_date,cb.subscription_date,cb.last_renewed_date,cb.status, cp.name AS subsname FROM #__ihouse_ads_type AS at "
				.	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = at.subscription_id "
				.	" LEFT JOIN #__cbsubs_plans AS cp ON cp.id = cb.plan_id "
				.	" LEFT JOIN #__ihouse_property AS p ON at.property_id = p.id "
				.	" LEFT JOIN #__ihouse_ads AS a ON at.ads_id = a.id "
				.	" WHERE at.subscription_id = '$cid' ";
				
			$db->setQuery( $query );
			$row = $db->loadObject();
			
		return $row;
	}
	function getDistrictInfo($districtid) {
		$db		=& 	JFactory::getDBO();
		
		$query = " SELECT * FROM #__ihouse_district WHERE code = '$districtid' ";
				
			$db->setQuery( $query );
			$row = $db->loadObject();
		
		return $row;
	}
	function getAgents($cid) {
		$db		=& 	JFactory::getDBO();
		
		$query = " SELECT u.* FROM #__ihouse_ads_type AS a "
				.	" LEFT JOIN #__users AS u ON u.id = a.user_id "
				.	" WHERE a.subscription_id = '$cid' ";
			$db->setQuery( $query );
			$row = $db->loadObject();
		
		return $row;
	}
	function getPlanHTML($tasktype) {
		$db		=& 	JFactory::getDBO();
		
		switch($tasktype) {
			case 'type1': $strids = "5"; break;
			case 'type2': $strids = "6"; break;
			case 'type3': $strids = "7"; break;
			case 'type4': $strids = "8,9,10,11"; break;
			case 'type5': $strids = "12,13,14,15"; break;
			case 'type6': $strids = "16,17,18,19"; break;
			case 'type7': $strids = "20,21,22,23"; break;
		}
		
		$query = " SELECT * FROM #__cbsubs_plans WHERE id IN (".$strids.") ";
			$db->setQuery( $query );
			$rows = $db->loadObjectList();
		
		$html = '';
		$html .= '<select name="plan">';
		$html .= '<option value="">Select Plan</option>';
		
		foreach($rows as $r):
			$html .= '<option value="'.$r->id.'">'.$r->name.'</option>';
		endforeach;
		
		$html .= '</select>';
		return $html;
	}
	
}