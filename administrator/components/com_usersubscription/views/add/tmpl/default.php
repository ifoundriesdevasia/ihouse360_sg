<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('behavior.tooltip'); ?>
<?php JHTML::_('behavior.calendar'); ?>
<?php JHTML::_('script', 'add.js', 'administrator/components/com_usersubscription/assets/'); ?>
<?php
	JToolBarHelper::title( JText::_( 'iHouse360 Add '. $this->tasktype ), 'plugin.png' );
	JToolBarHelper::save();
	JToolBarHelper::apply();
?>

<form action="index.php" method="post" name="adminForm">

	<div class="col">
		<fieldset class="adminform">
		<legend><?php echo JText::_( 'Please select' ); ?></legend>
		<table class="admintable" cellspacing="1">
        	<?php if($this->tasktype == 'type1' ||
					 $this->tasktype == 'type2' ||
					 $this->tasktype == 'type4' ||
					 $this->tasktype == 'type5' ||
					 $this->tasktype == 'type6' ||
					 $this->tasktype == 'type7') : ?>
        	<tr>
				<td width="150" class="key">
					<label for="name">
						<?php echo JText::_( 'District' ); ?>
					</label>
				</td>
				<td>
					<?php echo $this->district ?>	
				</td>
			</tr>
            <tr>
				<td width="150" class="key">
					<label for="name">
						<?php echo JText::_( 'Condo' ); ?>
					</label>
				</td>
				<td>
					<div id="ajax-load-condo"></div>	
				</td>
			</tr>
            <?php endif ?>
            
            
            
            <?php if($this->tasktype == 'type1' || $this->tasktype == 'type2' || $this->tasktype == 'type3') : ?>
            <tr>
				<td width="150" class="key">
					<label for="name">
						<?php echo JText::_( 'List Agents' ); ?>
					</label>
				</td>
				<td>
					<?php echo $this->agent ?>
				</td>
			</tr>
            
        	<?php endif; ?>
        
        
        	<?php if($this->tasktype == 'type7' || 
					 $this->tasktype == 'type4' || 
					 $this->tasktype == 'type5' ||
					 $this->tasktype == 'type6' ) : ?>
            <tr>
				<td width="150" class="key">
					<label for="name">
						<?php echo JText::_( 'List all ads of this condo' ); ?>
					</label>
				</td>
				<td>
					<div id="ajax-load-ads"></div>	
				</td>
			</tr>
            <?php endif; ?>
            
            <tr>
				<td width="150" class="key">
					<label for="name">
						<?php echo JText::_( 'Plan' ); ?>
					</label>
				</td>
				<td>
                	<?php echo $this->plan ?>
				</td>
			</tr>	
			</table>
		</fieldset>
	</div>

	<input type="hidden" name="option" value="com_usersubscription" />
	<input type="hidden" name="task" value="" />
    <input type="hidden" name="tasktype" value="<?php echo $this->tasktype ?>" />
	<input type="hidden" name="boxchecked" value="0" />
	<?php echo JHTML::_( 'form.token' ); ?>
  
</form>