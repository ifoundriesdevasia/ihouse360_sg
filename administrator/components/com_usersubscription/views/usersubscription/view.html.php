<?php
/**
* @version		$Id: view.html.php 10381 2008-06-01 03:35:53Z pasamio $
* @package		Joomla
* @subpackage	Transaction
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Process component
 *
 * @static
 * @package		Transaction
 * @subpackage	Transaction Details
 * @since 1.0
 */
class UsersubscriptionViewUserSubscription extends JView
{
	function display($tpl = null)
	{
		//$cid 	= JRequest::getVar( 'cid' );
		//$model		= &$this->getModel();
		//$row = $model->getDetail();

		global $mainframe, $option;

		$user		=& 	JFactory::getUser();
		
		$uid		=	$user->get('id');	
		
		$gid		=	$user->get('gid');
		
		$layout		= 	JRequest::getCmd('layout', 'default');
		
		$task		=	JRequest::getCmd('task');
		
		$startover	=	JRequest::getCmd('start'); /* for Sorting purpose, when click on submenu at the top */

		$db 	  	=& 	JFactory::getDBO();
		
		$filter_order		= $mainframe->getUserStateFromRequest( "$option.filter_order",		'filter_order',		'u.id',	'cmd' );

		$filter_order_Dir	= $mainframe->getUserStateFromRequest( "$option.filter_order_Dir",	'filter_order_Dir',	'',		'word' );
		$filter_state		= $mainframe->getUserStateFromRequest( "$option.filter_state",		'filter_state',		'',		'word' );
		$filter_subscription	= $mainframe->getUserStateFromRequest( "$option.filter_subscription",		'filter_subscription', 	0,			'int' );
		$search				= $mainframe->getUserStateFromRequest( "$option.search",			'search',			'',		'string' );
		$search				= JString::strtolower( $search );
		
		//if(!preg_match('/^p\./', $filter_order))
			//$filter_order	=	'p.id';
		
		$limit		= $mainframe->getUserStateFromRequest( 'global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int' );
		$limitstart	= $mainframe->getUserStateFromRequest( $option.'.limitstart', 'limitstart', 0, 'int' );
		
		$where = array();
		$whereor = array();
		
		if($startover) {
			$search 	= '';
			$limitstart = 0;
			$limit		= 20;
		}
		
		if ($search)
		{
			switch($task) {
				case 'type1':
				case 'type2':
					$whereor[] = ' LOWER(p.name_en) LIKE '.$db->Quote( '%'.$db->getEscaped( $search, true ).'%', false );
					break;
				case 'type3':
					$whereor[] = ' LOWER(u.name) LIKE '.$db->Quote( '%'.$db->getEscaped( $search, true ).'%', false );
					break;
				case 'type4':
				case 'type5':
				case 'type6':
				case 'type7':
				case 'normal':
					$whereor[] = ' LOWER(a.ad_title) LIKE '.$db->Quote( '%'.$db->getEscaped( $search, true ).'%', false );
					break;	
				case 'subsads':	
					$whereor[] = ' LOWER(u.name) LIKE '.$db->Quote( '%'.$db->getEscaped( $search, true ).'%', false );
					break;
				default:
					$whereor[] = ' LOWER(u.name) LIKE '.$db->Quote( '%'.$db->getEscaped( $search, true ).'%', false );
					break;	
			}
			//$whereor[] = ' LOWER(u.name) LIKE '.$db->Quote( '%'.$db->getEscaped( $search, true ).'%', false );
			
			$where[] = ( count( $whereor ) ?  implode( ' OR ', $whereor ) : '' );
			
			
		} 
		
		
		if($filter_subscription) { // for select box
			$where[]	= ' cb.plan_id = '.$filter_subscription;
		}
		
		$join_property_1 	= 	'';
		$join_property_2	=	'';
		$join_ads		=	'';
		
		switch($task) {
			case 'type1':
				$where[] 	= 	' cb.plan_id = 5 ';
				$property_field = ' , p.name_en ';
				$join_property_2 = ' RIGHT JOIN #__ihouse_property AS p ON p.id = t.property_id ';
				$join_property_1 = ' LEFT JOIN #__ihouse_ads_type AS t ON t.subscription_id = cb.id';
				break;
			case 'type2':
				$where[] 	= 	' cb.plan_id = 6 ';
				$property_field = ' , p.name_en ';
				$join_property_2 = ' RIGHT JOIN #__ihouse_property AS p ON p.id = t.property_id ';
				$join_property_1 = ' LEFT JOIN #__ihouse_ads_type AS t ON t.subscription_id = cb.id ';
				break;
			case 'type3':
				$where[] 	= 	' cb.plan_id = 7 ';
				$join_property_1 = ' LEFT JOIN #__ihouse_ads_type AS t ON t.subscription_id = cb.id ';
				break;
			case 'type4':
				$where[] 	= 	' cb.plan_id IN (8,9,10,11) ';
				$property_field = ' , a.ad_title, a.ad_type ';
				$join_property_2 = ' RIGHT JOIN #__ihouse_ads AS a ON a.id = t.ads_id ';
				$join_property_1 = ' LEFT JOIN #__ihouse_ads_type AS t ON t.subscription_id = cb.id ';
				break;
			case 'type5':
				$where[] 	= 	' cb.plan_id IN (12,13,14,15) ';
				$property_field = ' , a.ad_title ';
				$join_property_2 = ' RIGHT JOIN #__ihouse_ads AS a ON a.id = t.ads_id ';
				$join_property_1 = ' LEFT JOIN #__ihouse_ads_type AS t ON t.subscription_id = cb.id ';
				break;
			case 'type6':
				$where[] 	= 	' cb.plan_id IN (16,17,18,19) ';
				$property_field = ' , a.ad_title , a.ad_type';
				$join_property_2 = ' RIGHT JOIN #__ihouse_ads AS a ON a.id = t.ads_id ';
				$join_property_1 = ' LEFT JOIN #__ihouse_ads_type AS t ON t.subscription_id = cb.id ';
				break;
			case 'type7':
				$where[] 	= 	' cb.plan_id IN (20,21,22,23) ';
				$property_field = ' , a.ad_title ';
				$join_property_2 = ' RIGHT JOIN #__ihouse_ads AS a ON a.id = t.ads_id ';
				$join_property_1 = ' LEFT JOIN #__ihouse_ads_type AS t ON t.subscription_id = cb.id ';
				break;
			case 'normal':
				$where[] 	= 	' cb.plan_id = 4 ';
				$property_field = ' , a.ad_title ';
				$join_property_2 = ' RIGHT JOIN #__ihouse_ads AS a ON a.subscription_id = cb.id ';
				break;
			case 'subsads':
				$where[] 	= 	' cb.plan_id IN (2,3) ';
				$property_field = ' , a.ad_title ';
				$join_property_2 = ' RIGHT JOIN #__ihouse_ads AS a ON a.subscription_id = cb.id ';
				break;
			case 'nosubs':
				$where[]	=	" u.user_category = 'Agent' ";
				$where[]	=	" cb.plan_id IS NULL ";
				break;
			default:
				$where[] 	= 	' cb.plan_id IN (1,2,3,25) ';
				break;
		}
		
		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
		
		if($filter_order && $filter_order_Dir) {
			$orderby 	= ' ORDER BY '. $filter_order .' '. $filter_order_Dir;
		}
		
		if($startover) {
			$filter_order		= 	' cb.expiry_date '; 

			$filter_order_Dir	= 	' DESC ';
			
			$orderby 			= 	' ORDER BY '.$filter_order.' '. $filter_order_Dir;
		}

		$query = " SELECT u.id AS user_id ,u.name, cp.name as subs_name, cb.expiry_date, cb.status, cb.id ".$property_field
					.	" FROM #__users AS u "
					.	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.user_id = u.id "
					.	$join_property_1
					.	" LEFT JOIN #__cbsubs_plans AS cp ON cp.id = cb.plan_id "
					.	$join_property_2
					.	$where
					.	$orderby;		
			
			$db->setQuery( $query , $limitstart, $limit);
			$rows	=	$db->loadObjectList();
		
		$query = " SELECT COUNT(cb.id) FROM #__users AS u "
					.	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.user_id = u.id "
					.	$join_property_1
					.	" LEFT JOIN #__cbsubs_plans AS cp ON cp.id = cb.plan_id "
					.	$join_property_2
					.	$where;
					
			$db->setQuery( $query );
			$total	=	$db->loadResult();
		
		if ($db->getErrorNum())
		{
			echo $db->stderr();
			return false;
		}
		
		jimport('joomla.html.pagination');
		$pagination = new JPagination( $total, $limitstart, $limit );
		
		$subscription[] = JHTML::_('select.option',  '', '- '.JText::_( 'Select Subscription' ).' -' );
		switch($task) {
			case 'type1':
			case 'type2':
			case 'type3':
			case 'normal':
			case 'nosubs':
				break;
			case 'type4':
				$subscription[] = JHTML::_('select.option',  8, JText::_( 'Type 4: Featured Listing 1 Week' ) );
				$subscription[] = JHTML::_('select.option',  9, JText::_( 'Type 4: Featured Listing 2 Week' ) );
				$subscription[] = JHTML::_('select.option',  10, JText::_( 'Type 4: Featured Listing 3 Week' ) );
				$subscription[] = JHTML::_('select.option',  11, JText::_( 'Type 4: Featured Listing 4 Week' ) );
				break;
			case 'type5':
				$subscription[] = JHTML::_('select.option',  12, JText::_( 'Type 5: Featured Listing 1 Week' ) );
				$subscription[] = JHTML::_('select.option',  13, JText::_( 'Type 5: Featured Listing 2 Week' ) );
				$subscription[] = JHTML::_('select.option',  14, JText::_( 'Type 5: Featured Listing 3 Week' ) );
				$subscription[] = JHTML::_('select.option',  15, JText::_( 'Type 5: Featured Listing 4 Week' ) );
				break;
			case 'type6':
				$subscription[] = JHTML::_('select.option',  16, JText::_( 'Type 6: Featured Listing 1 Week' ) );
				$subscription[] = JHTML::_('select.option',  17, JText::_( 'Type 6: Featured Listing 2 Week' ) );
				$subscription[] = JHTML::_('select.option',  18, JText::_( 'Type 6: Featured Listing 3 Week' ) );
				$subscription[] = JHTML::_('select.option',  19, JText::_( 'Type 6: Featured Listing 4 Week' ) );
				break;
			case 'type7':
				$subscription[] = JHTML::_('select.option',  20, JText::_( 'Type 7: Featured Listing 1 Week' ) );
				$subscription[] = JHTML::_('select.option',  21, JText::_( 'Type 7: Featured Listing 2 Week' ) );
				$subscription[] = JHTML::_('select.option',  22, JText::_( 'Type 7: Featured Listing 3 Week' ) );
				$subscription[] = JHTML::_('select.option',  23, JText::_( 'Type 7: Featured Listing 4 Week' ) );
				break;
			case 'subsads':
				$subscription[] = JHTML::_('select.option',  2, JText::_( 'Quarterly Subscription' ) );
				$subscription[] = JHTML::_('select.option',  3, JText::_( 'Yearly Subscription' ) );
				break;
			default:
				$subscription[] = JHTML::_('select.option',  1, JText::_( 'Owner Refresh Subscription' ) );
				$subscription[] = JHTML::_('select.option',  2, JText::_( 'Quarterly Subscription' ) );
				$subscription[] = JHTML::_('select.option',  3, JText::_( 'Yearly Subscription' ) );
				$subscription[] = JHTML::_('select.option',  25, JText::_( 'Owner Standard 1 Ads' ) );
				break; 
		}
		$lists['subscription'] = JHTML::_('select.genericlist',   $subscription, 'filter_subscription', 'class="inputbox" size="1" onchange="document.adminForm.submit( );"', 'value', 'text', "$filter_subscription" );
		
		// table ordering
		$lists['order_Dir']	= $filter_order_Dir;
		$lists['order']		= $filter_order;

		// search filter
		$lists['search']= $search;
		//$this->assignRef('row', 			$row );

		$this->assignRef('task', $task);
		$this->assignRef('user',		JFactory::getUser());
		$this->assignRef('lists',		$lists);
		$this->assignRef('items', $rows);
		$this->assignRef('pagination', $pagination);
		
		parent::display($tpl);
	}
}