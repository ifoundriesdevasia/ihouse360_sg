<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('behavior.tooltip'); ?>

<?php
	
	JToolBarHelper::title( JText::_( 'iHouse360 User Subscription' ), 'plugin.png' );

	if($this->task == 'type1' ||
	   $this->task == 'type2' ||
	   $this->task == 'type3' ||
	   $this->task == 'type4' ||
	   $this->task == 'type5' ||
	   $this->task == 'type6' ||
	   $this->task == 'type7') {
		JToolBarHelper::addNewX();
		JToolBarHelper::deleteList();
	}
	//JToolBarHelper::publishList();
	//JToolBarHelper::unpublishList();
	//JToolBarHelper::editListX();
	//JToolBarHelper::help( 'screen.plugins' );
	
	//$ordering = ($this->lists['order'] == 'p.folder' || $this->lists['order'] == 'p.ordering');
	$rows =& $this->items;

?>

<form action="index.php" method="post" name="adminForm">
<table>
	<tr>
    
		<td align="left" width="100%">
			<?php echo JText::_( 'Filter' ); ?>:
			<input type="text" name="search" id="search" value="<?php echo $this->lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />
			<button onclick="this.form.submit();"><?php echo JText::_( 'Go' ); ?></button>
			<button onclick="document.getElementById('search').value='';this.form.submit();"><?php echo JText::_( 'Reset' ); ?></button>
		</td>
		<td nowrap="nowrap">
			<?php
			echo $this->lists['type'];
			echo $this->lists['subscription'];
			//echo $this->lists['state']; publish or unpublish state
			?>
		</td>
       
	</tr>
</table>

<table class="adminlist">
<thead>
	<tr>
		<th width="1%">
			<?php echo JText::_( 'Num' ); ?>
		</th>
		
        
        <?php if($this->task == 'type1' ||
	   			$this->task == 'type2' ||
	   			$this->task == 'type3' ||
	   			$this->task == 'type4' ||
	   			$this->task == 'type5' ||
	   			$this->task == 'type6' ||
	   			$this->task == 'type7') {
		?>	
        <th width="1%">
			<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $rows );?>);" />
		</th>
		<th class="title" width="6%">
			<?php echo JHTML::_('grid.sort',   'Subs Id', 'cb.id', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
        <?php } ?>
		<th nowrap="nowrap" width="10%">
			<?php echo JHTML::_('grid.sort',   'Name', 'u.name', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
        <th nowrap="nowrap" width="10%">
			<?php echo JHTML::_('grid.sort',   'Subscription Name', 'cp.name', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
        <?php if($this->task == 'type4' || $this->task == 'type6') : ?>
        <th nowrap="nowrap" width="10%">
            <?php echo JHTML::_('grid.sort',   'Ad Type', 'a.ad_type', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
        <?php endif; ?>
        <?php if($this->task == 'type1' || $this->task == 'type2') : ?>
        <th nowrap="nowrap" width="10%">
            <?php echo JHTML::_('grid.sort',   'Property Name', 'p.name_en', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
        <?php endif; ?>
        
		<?php if($this->task == 'type4' || $this->task == 'type5' || $this->task == 'type6' || $this->task == 'type7' || $this->task == 'normal' || $this->task == 'subsads') : ?>
        <th nowrap="nowrap" width="10%">
            <?php echo JHTML::_('grid.sort',   'Ad Title', 'a.ad_title', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
        <?php endif; ?>
        
        <th nowrap="nowrap" width="10%">
			<?php echo JHTML::_('grid.sort',   'Status', 'cb.status', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
        <th nowrap="nowrap" width="10%">
			<?php echo JHTML::_('grid.sort',   'Expired', 'cb.expiry_date', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
	</tr>
</thead>
<tfoot>
	<tr>
		<td colspan="12">
			<?php echo $this->pagination->getListFooter(); ?>
		</td>
	</tr>
</tfoot>
<tbody>
<?php

	$k = 0;
	$n = count( $rows );
	
	for ($i=0; $i < $n; $i++) {
	$row 	= $rows[$i];

	$link = JRoute::_( 'index.php?option=com_usersubscription&task=editform&tasktype='.$this->task.'&cid='. $row->id );

	//$access 	= JHTML::_('grid.access',   $row, $i );
	$checked 	= JHTML::_('grid.checkedout',   $row, $i );
	//$published 	= JHTML::_('grid.published', $row, $i );

?>
	<tr class="<?php echo "row$k"; ?>">
		<td align="right">
			<?php echo $this->pagination->getRowOffset( $i ); ?>
		</td>
		
        <?php if($this->task == 'type1' ||
	   			$this->task == 'type2' ||
	   			$this->task == 'type3' ||
	   			$this->task == 'type4' ||
	   			$this->task == 'type5' ||
	   			$this->task == 'type6' ||
	   			$this->task == 'type7') {
		?>	
        <td align="center">
			<?php echo $checked; ?>
		</td>
		<td align="center">
		<span class="editlinktip hasTip" title="<?php echo JText::_( 'Subscription ID' );?>::<?php echo $row->id; ?>">
			<a href="<?php echo $link; ?>">
				<?php echo $row->id; ?>
            </a>
        </span>
		</td>
        <?php } ?>
		<td align="center">
            <?php echo $row->name; ?>
		</td>
		<td class="order">
            <?php echo ($row->subs_name)?$row->subs_name:'No Subs' ?>
		</td>
        <?php if($this->task == 'type4' || $this->task == 'type6') : ?>
        <td class="order">
            <?php echo ($row->ad_type)?$row->ad_type:'No Type'; ?>
		</td>
        <?php endif; ?> 
        <?php if($this->task == 'type1' || $this->task == 'type2') : ?>
        <td class="order">
            <?php echo $row->name_en ?>
		</td>
        <?php endif; ?>
        <?php if($this->task == 'type4' || $this->task == 'type5' || $this->task == 'type6' || $this->task == 'type7' || $this->task == 'normal' || $this->task == 'subsads') : ?>
        <td class="order">
            <?php echo $row->ad_title ?>
		</td>
        <?php endif; ?>
         <td class="order">
         	<?php 	if($row->status == 'A') 
        				echo 'Active';
					else if($row->status == 'X')
						echo 'Expired';
					else
						echo 'No Subs Status';
			?>			
        </td>
        <td class="order">
        	 <?php echo ($row->expiry_date)?$row->expiry_date:'No Subs Expiry Date' ?>
        </td>
	</tr>
	<?php
		$k = 1 - $k;
	}
	?>
</tbody>

</table>

	<input type="hidden" name="option" value="com_usersubscription" />
	<input type="hidden" name="task" value="<?php echo $this->task ?>" />
    <input type="hidden" name="tasktype" value="<?php echo $this->task ?>" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
	<?php echo JHTML::_( 'form.token' ); ?>
  
</form>