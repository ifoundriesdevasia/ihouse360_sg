<?php
/**
 * @version		$Id: controller.php 11299 2008-11-22 01:40:44Z ian $
 * @package		Joomla
 * @subpackage	Transaction
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.controller');

/**
 * Users Component Controller
 *
 * @package		Joomla
 * @subpackage	Transaction
 * @since 1.5
 */
class UserSubscriptionController extends JController
{
	/**
	 * Constructor
	 *
	 * @params	array	Controller configuration array
	 */
	function __construct($config = array())
	{
		parent::__construct($config);
		$this->registerTask( 'monthly'  , 	'monthlydetails'  );
		$this->registerTask( 'total'  , 	'totaldetails'  );

	}
	
	function display(){
		
		JRequest::setVar('view','usersubscription');
		$model	= &$this->getModel( 'subscription' );
		
		$user = &JFactory::getUser();
		$viewcache = JRequest::getVar('viewcache',1,'POST','INT');
		
		if ($user->get('id')) {
			parent::display(false);
		} else {
			parent::display(true);
		}
	}
	function editform() {
		JRequest::setVar('view','edit');
		
		$user = &JFactory::getUser();
		$viewcache = JRequest::getVar('viewcache',1,'POST','INT');
		
		if ($user->get('id')) {
			parent::display(false);
		} else {
			parent::display(true);
		}
	}
	function type1() {
		JRequest::setVar('view','usersubscription');
		$user = &JFactory::getUser();
		$viewcache = JRequest::getVar('viewcache',1,'POST','INT');
		
		if ($user->get('id')) {
			parent::display(false);
		} else {
			parent::display(true);
		}
	}
	function type2() {
		JRequest::setVar('view','usersubscription');
		$user = &JFactory::getUser();
		$viewcache = JRequest::getVar('viewcache',1,'POST','INT');
		
		if ($user->get('id')) {
			parent::display(false);
		} else {
			parent::display(true);
		}
	}
	function type3() {
		JRequest::setVar('view','usersubscription');
		$user = &JFactory::getUser();
		$viewcache = JRequest::getVar('viewcache',1,'POST','INT');
		
		if ($user->get('id')) {
			parent::display(false);
		} else {
			parent::display(true);
		}
	}
	function type4() {
		JRequest::setVar('view','usersubscription');
		$user = &JFactory::getUser();
		$viewcache = JRequest::getVar('viewcache',1,'POST','INT');
		
		if ($user->get('id')) {
			parent::display(false);
		} else {
			parent::display(true);
		}
	}
	function type5() {
		JRequest::setVar('view','usersubscription');
		$user = &JFactory::getUser();
		$viewcache = JRequest::getVar('viewcache',1,'POST','INT');
		
		if ($user->get('id')) {
			parent::display(false);
		} else {
			parent::display(true);
		}
	}
	function type6() {
		JRequest::setVar('view','usersubscription');
		$user = &JFactory::getUser();
		$viewcache = JRequest::getVar('viewcache',1,'POST','INT');
		
		if ($user->get('id')) {
			parent::display(false);
		} else {
			parent::display(true);
		}
	}
	function type7() {
		JRequest::setVar('view','usersubscription');
		$user = &JFactory::getUser();
		$viewcache = JRequest::getVar('viewcache',1,'POST','INT');
		
		if ($user->get('id')) {
			parent::display(false);
		} else {
			parent::display(true);
		}
	}
	function remove() { // not really deleting, only delete the featured ads, and ads the slots up to 1
		$tasktype	=	JRequest::getVar('tasktype');
		
		$subs_id	=	JRequest::getVar('cid', array());
		
		$model		= &$this->getModel('subscription');
		
		$link 	= 	'index.php?option=com_usersubscription&task='.$tasktype.'&start=1';
		
		if($model->remove($subs_id)) {
			$msg	=	'Successful Delete';
		} else {	
			$msg	=	'Error: UnSuccessful';
		}
		$this->setRedirect($link, $msg);
	}
	function add() {
		JRequest::setVar('view','add');
		
		$tasktype	=	JRequest::getVar('tasktype');
		
		$model		= &$this->getModel('subscription');
		
		$user = &JFactory::getUser();
		
		if($model->checkSlot($tasktype)) {
					
		} else {
			$link = 'index.php?option=com_usersubscription&task='.$tasktype.'&start=1';
			$msg	=	'Slot is not available for this type : '. ucfirst($tasktype);
			$this->setRedirect($link, $msg);
		}

		if ($user->get('id')) {
			parent::display(false);
		} else {
			parent::display(true);
		}
	}
	
	function save() {

		$section	=	JRequest::getVar('section');
		$tasktype	=	JRequest::getVar('tasktype');
		$model		= &$this->getModel('subscription');
		
		$link 		= 'index.php?option=com_usersubscription&task='.$tasktype.'&start=1';
		
		if($section == 'edit') {
			$subsid	=	JRequest::getVar('subsid','');
			$planid	=	JRequest::getVar('plan','');
			
			$link 		= 'index.php?option=com_usersubscription&task=editform&tasktype='.$tasktype.'&cid='.$subsid;
			
			if($model->edit($subsid, $planid) ) {
				$msg = "Plan has been extended/applied to this subscription";
			} else {
				$msg = "Plan has not been extended/applied to this subscription";
			}
			
			$this->setRedirect($link, $msg);
		} else { // Add
		
			if($model->checkType($tasktype)) {
			
				if($model->add($tasktype)) {
					$msg = "Success Saved";
				} else {
					$msg = "Saved Unsuccessful";
				}
		
				$this->setRedirect($link, $msg);
			
			} else {
			
				$msg = "Please complete all fields";
				$this->setRedirect($link, $msg);
			
			}
		}
	}
	function apply() {

		$section		=	JRequest::getVar('section');
		$tasktype		=	JRequest::getVar('tasktype');
		
		$model		= &$this->getModel('subscription');
		
		if($section == 'edit') {
			$subsid	=	JRequest::getVar('subsid','');
			$planid	=	JRequest::getVar('plan','');
			
			$link 		= 'index.php?option=com_usersubscription&task=editform&tasktype='.$tasktype.'&cid='.$subsid;
			
			if($model->edit($subsid, $planid) ) {
				$msg = "Plan has been extended/applied to this subscription";
			} else {
				$msg = "Plan has not been extended/applied to this subscription";
			}
			$this->setRedirect($link, $msg);
		} else { // Add
			
			
			$link 		= 'index.php?option=com_usersubscription&task=add&tasktype='.$tasktype;
		
			$tmp = $model->checkType($tasktype);
		
			if($tmp->status) {
			
				if($model->add($tasktype)) {
					$msg = "Success Saved";
				} else {
					$msg = "Saved Unsuccessfull";
				}
		
				$this->setRedirect($link, $msg);
			
			} else {
			
				$msg = $tmp->msg;
				$this->setRedirect($link, $msg);
			
			}
		
		}
	}
	
	function ajaxLoadCondo() {
		global $mainframe;
		include_once('assets/json.php');

		$json = new Services_JSON();
		
		$model		= &$this->getModel('getter');
		
		header('Content-type: application/json');

		$section	=	JRequest::getVar('section');
		$district	=	JRequest::getVar('district');
		
		$tmp		=	$model->getPropertyListHTML($section, $district);

		echo $json->encode(array(	"html"	=> $tmp
							));
		
		$mainframe->close();
		exit;
	}
	function ajaxLoadAds() {
		global $mainframe;
		include_once('assets/json.php');

		$json = new Services_JSON();
		
		$model		= &$this->getModel('getter');
		
		header('Content-type: application/json');
		
		$section		=	JRequest::getVar('section');
		
		//if($section == 'type7') {
			$property_id	=	JRequest::getVar('property_id');
			$tmp			=	$model->getAdsForThisCondo($property_id);
		//} else {
		//}
		echo $json->encode(array(	"html"	=> $tmp
							));
		
		$mainframe->close();
		exit;
	}
}
