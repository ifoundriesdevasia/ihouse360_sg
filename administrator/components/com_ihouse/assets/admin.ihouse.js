
function add_row(tmp_id) {
	var foo 		= 	new Date; // Generic JS date object
	var unixtime_ms = 	foo.getTime();

	//var url = baseurl + '/components/com_insureasia/assets/addRow.php?r=' + unixtime_ms;	
	//alert(url);
	
	var tableTag = $('new_receipt_table');
	var tbodyTag = tableTag.getElementsByTagName('tbody')[0];
					
	var trTag = document.createElement('tr');
	var tdTag1 = document.createElement('td');
	var tdTag2 = document.createElement('td');
	var tdTag3 = document.createElement('td');
	
	tdTag1.innerHTML = '<input class="inputbox" type="text" name="policy_' + tmp_id + '" id="name" size="60" maxlength="255" value="" />';
	tdTag2.innerHTML = '<input class="inputbox" type="text" name="ref_' + tmp_id + '" id="name" size="60" maxlength="255" value="" />';
	tdTag3.innerHTML = '<input class="inputbox" type="text" name="amt_' + tmp_id + '" id="name" size="60" maxlength="255" value="" />';
	
	trTag.appendChild(tdTag1);
	trTag.appendChild(tdTag2);
	trTag.appendChild(tdTag3);
	
	tbodyTag.appendChild(trTag);
	
	var next_tmp_id	=	tmp_id + 1;
	
	$('add_row_receipt_tmp').setHTML('<a href="#" onclick="javascript:add_row('+ next_tmp_id +');">Add Row</a><input type="hidden" name="total" value="' + next_tmp_id + '" />');
}

function deletePolicyNo(id) {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();
	
	//alert($('total_amount_policy'));
	
	var url = 'index.php?option=com_insureasia&task=delete_policy_no&r=' + unixtime_ms;	
	var req = new Ajax(url, {
	   			data		: 	
								{
									'item_id' 	: id
								},
				method		: "post",
				onRequest	: function() {
					/*$$('.ajax-loader').setStyle('display', '');*/
				},
	    		onComplete	: function(response) {
					/*$$('.ajax-loader').setStyle('display', 'none');*/
					$('policy_' + id).setHTML('');
					$('ajax_msg').setHTML(response);
	   			},
				evalScripts: true
		}).request();
}

function toggle(id) {
	
	var t = $(id).getStyle('display');

	if(t == '') {
		$(id).setStyle('display', 'none');
	} else {
		$(id).setStyle('display','');
	}
	
}
