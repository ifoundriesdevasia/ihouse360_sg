<?php defined('_JEXEC') or die('Restricted access'); ?>

<script type="text/javascript">
	var baseurl = '<?php echo JURI::base(true); ?>';
</script>


<?php JHTML::_('behavior.tooltip'); ?>
<?php JHTML::_('behavior.calendar'); ?>

<?php JHTML::_('script', 'admin.ihouse.js', 'administrator/components/com_ihouse/assets/'); ?>
<?php
	
	JToolBarHelper::title( JText::_( 'iHouse360 District Add' ), 'plugin.png' );
	JToolBarHelper::save();
	JToolBarHelper::apply();
	JToolBarHelper::cancel();

	//JToolBarHelper::help( 'screen.plugins' );
	
	$ordering = ($this->lists['order'] == 'p.folder' || $this->lists['order'] == 'p.ordering');
	$rows =& $this->items;

?>
<div style="padding:5px;">
<form action="index.php" method="post" name="adminForm">

		<div class="col width-60">
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Property : '); ?></legend>
				<table class="admintable">
				<tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Name (English) ' ); ?>:
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="name_en" size="60" maxlength="255" value="" />
					</td>
				</tr>
                <tr>
                	<td class="key">
						<label for="name">
							<?php echo JText::_( 'Name (Chinese) ' ); ?>:
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="name_ch" size="60" maxlength="255" value="" />
					</td>
                </tr>
                <tr>
                <td class="key">
						<label for="name">
							<?php echo JText::_( 'Address ' ); ?>:
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="address" size="60" maxlength="255" value="" />
					</td>
                </tr>
                <tr>
                <td class="key">
						<label for="name">
							<?php echo JText::_( 'Postal Code ' ); ?>:
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="postcode" size="60" maxlength="255" value="" />
					</td>
                </tr>
                <tr>
                <td class="key">
						<label for="name">
							<?php echo JText::_( 'Property Type ' ); ?>:
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="property_type" size="60" maxlength="255" value="" />
					</td>
                </tr>
                <tr>
                <td class="key">
						<label for="name">
							<?php echo JText::_( 'Tenure ' ); ?>:
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="tenure" size="60" maxlength="255" value="" />
					</td>
                </tr>    
                <tr>
                <td class="key">
						<label for="name">
							<?php echo JText::_( 'Property Class ' ); ?>:
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="property_class" size="60" maxlength="255" value="" />
					</td>
                </tr>
                <tr>
                <td class="key">
						<label for="name">
							<?php echo JText::_( 'Year Built ' ); ?>:
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="year_built" size="60" maxlength="255" value="" />
					</td>
                </tr>
                <tr>
                <td class="key">
						<label for="name">
							<?php echo JText::_( 'Room Size ' ); ?>:
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="room_size" size="60" maxlength="255" value="" />
					</td>
                </tr>
                <tr>
                <td class="key">
						<label for="name">
							<?php echo JText::_( 'District ' ); ?>:
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="district_id" size="60" maxlength="255" value="" />Choose 1
					</td>
                </tr>
                <tr>
                <td class="key">
						<label for="name">
							<?php echo JText::_( 'Condo Units ' ); ?>:
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="units_on_condo" size="60" maxlength="255" value="" />
					</td>
                </tr>    
                <tr>
                <td class="key">
						<label for="name">
							<?php echo JText::_( 'Description ' ); ?>:
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="description" size="60" maxlength="255" value="" />
					</td>
                </tr>
                <tr>
                <td class="key">
						<label for="name">
							<?php echo JText::_( 'Nearby MRT ' ); ?>:
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="nearby_mrt" size="60" maxlength="255" value="" />Separated by '|'
					</td>
                </tr>
                <tr>
                <td class="key">
						<label for="name">
							<?php echo JText::_( 'Nearby School ' ); ?>:
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="nearby_school" size="60" maxlength="255" value="" />
					</td>
                </tr>  
                <tr>
                <td class="key">
						<label for="name">
							<?php echo JText::_( 'Developer ' ); ?>:
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="developer" size="60" maxlength="255" value="" />
					</td>
                </tr>
                <tr>
                <td class="key">
						<label for="name">
							<?php echo JText::_( 'Floor Plan ' ); ?>:
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="floor_plan" size="60" maxlength="255" value="" />
					</td>
                </tr>  
				</table>
			</fieldset>

       </div>
       
       <div class="clr"></div>
       
       
    <input type="hidden" name="option" value="com_ihouse" />
	<input type="hidden" name="task" value="" />
	<?php echo JHTML::_( 'form.token' ); ?>   
</form>   
</div>    