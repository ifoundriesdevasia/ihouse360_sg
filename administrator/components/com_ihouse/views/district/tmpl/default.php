<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('behavior.tooltip'); ?>

<?php
	
	JToolBarHelper::title( JText::_( 'iHouse360 District' ), 'plugin.png' );
	//JToolBarHelper::deleteList();
	//JToolBarHelper::addNewX();
	//JToolBarHelper::publishList();
	//JToolBarHelper::unpublishList();
	//JToolBarHelper::editListX();
	//JToolBarHelper::help( 'screen.plugins' );
	
	//$ordering = ($this->lists['order'] == 'p.folder' || $this->lists['order'] == 'p.ordering');
	$rows =& $this->items;

?>

<form action="index.php" method="post" name="adminForm">
<table>
<!--
	<tr>
		<td align="left" width="100%">
			<?php echo JText::_( 'Filter' ); ?>:
			<input type="text" name="search" id="search" value="<?php echo $this->lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />
			<button onclick="this.form.submit();"><?php echo JText::_( 'Go' ); ?></button>
			<button onclick="document.getElementById('search').value='';this.form.submit();"><?php echo JText::_( 'Reset' ); ?></button>
		</td>
		<td nowrap="nowrap">
			<?php
			echo $this->lists['type'];
			//echo $this->lists['state']; publish or unpublish state
			?>
		</td>
	</tr>
    -->
</table>

<table class="adminlist">
<thead>
	<tr>
		<th width="1%">
			<?php echo JText::_( 'Num' ); ?>
		</th>
		<th width="1%">
			<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $rows );?>);" />
		</th>
		<th class="title" width="4%">
			<?php echo JHTML::_('grid.sort',   'Code', 'd.id', @$this->lists['order_Dir'], @$this->lists['order'] ,'district'); ?>
		</th>
		<th nowrap="nowrap" width="10%">
			<?php echo JHTML::_('grid.sort',   'Name', 'd.district_en', @$this->lists['order_Dir'], @$this->lists['order'], 'district' ); ?>
		</th>
        <th nowrap="nowrap" width="10%">
			<?php echo JHTML::_('grid.sort',   'Name (Ch)', 'd.district_ch', @$this->lists['order_Dir'], @$this->lists['order'] ,'district'); ?>
		</th>
	</tr>
</thead>
<tfoot>
	<tr>
		<td colspan="12">
			<?php //echo $this->pagination->getListFooter(); ?>
		</td>
	</tr>
</tfoot>
<tbody>
<?php
	$k = 0;
	$n = count( $rows );
	
	for ($i=0; $i < $n; $i++) {
	$row 	= $rows[$i];

	$link = JRoute::_( 'index.php?option=com_ihouse&view=district&layout=edit&cid='. $row->id );

	//$access 	= JHTML::_('grid.access',   $row, $i );
	$checked 	= JHTML::_('grid.checkedout',   $row, $i );
	//$published 	= JHTML::_('grid.published', $row, $i );

?>

	<tr class="<?php echo "row$k"; ?>">
		<td align="right">
			<?php //echo $this->pagination->getRowOffset( $i ); ?>
		</td>
		<td align="center">
			<?php echo $checked; ?>
		</td>
		<td align="center">
				<span class="editlinktip hasTip" title="<?php echo JText::_( 'Edit Property' );?>::<?php echo $row->id; ?>">
				<a href="#<?php //echo $link; ?>">
					<?php echo $row->code; ?></a></span>
		</td>
		<td align="center">
            <?php echo $row->district_en; ?>
		</td>
		<td class="order">
            <?php echo $row->district_ch ?>
		</td>
	</tr>
	<?php
		$k = 1 - $k;
	}
	?>
</tbody>

</table>

	<input type="hidden" name="option" value="com_ihouse" />
	<input type="hidden" name="task" value="" />
	<!--<input type="hidden" name="filter_client" value="<?php echo $this->client;?>" />-->
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
	<?php echo JHTML::_( 'form.token' ); ?>
  
</form>