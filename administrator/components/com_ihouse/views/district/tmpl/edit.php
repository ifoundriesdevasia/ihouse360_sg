<?php defined('_JEXEC') or die('Restricted access'); ?>

<script type="text/javascript">
	var baseurl = '<?php echo JURI::base(true); ?>';
</script>


<?php JHTML::_('behavior.tooltip'); ?>
<?php JHTML::_('behavior.calendar'); ?>

<?php JHTML::_('script', 'admin.ihouse.js', 'administrator/components/com_ihouse/assets/'); ?>
<?php
	
	JToolBarHelper::title( JText::_( 'iHouse360 District Edit' ), 'plugin.png' );
	JToolBarHelper::save();
	JToolBarHelper::apply();
	JToolBarHelper::cancel();

	//JToolBarHelper::help( 'screen.plugins' );
	
	$ordering = ($this->lists['order'] == 'p.folder' || $this->lists['order'] == 'p.ordering');
	$rows =& $this->items;

?>

<form action="index.php" method="post" name="adminForm">

		<div class="col width-60">
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Receipt Details' ); ?></legend>
				<table class="admintable">
				<tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Receipt No' ); ?>:
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="receipt_id" id="name" size="60" maxlength="255" value="<?php echo $this->receipt_no ?>" readonly="readonly" />
					</td>
				</tr>
				<tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Date' ); ?>:
						</label>
					</td>
					<td ><!-- MUST USE CALENDAR CLICKED -->
						<input class="inputbox" type="text" name="receipt_date" id="receipt_date" size="11" maxlength="255" value="" readonly="readonly"/>
                        <a href="#" onclick="return showCalendar('receipt_date', '%Y-%m-%d');"><img class="calendar" src="images/blank.png" alt="calendar" /></a>
					</td>
				</tr>
                <tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Received From' ); ?>:
						</label>
					</td>
					<td >
						<input class="inputbox" type="text" name="receipt_from" id="name" size="60" maxlength="255" value="" />
					</td>
				</tr>
                <tr>
					<td class="key">
						<?php echo JText::_( 'Agency' ); ?>:
					</td>
					<td>
                    	<?php echo $this->agency ?>
					</td>
				</tr>
                <tr>
					<td class="key">
						<?php echo JText::_( 'Payment By' ); ?>:
					</td>
					<td>
                    	<?php echo $this->payment_by ?>
					</td>
				</tr>
                <!--
				<tr>
					<td class="key">
						<?php echo JText::_( 'Published' ); ?>:
					</td>
					<td>
						<?php echo $lists['published']; ?>
					</td>
				</tr>
                -->
				<?php
				if ($row->id) {
					?>
					<tr>
						<td class="key">
							<label>
								<?php echo JText::_( 'ID' ); ?>:
							</label>
						</td>
						<td>
							<strong><?php echo $row->id;?></strong>
						</td>
					</tr>
					<?php
				}
				?>
				</table>
			</fieldset>
            
            
      
       </div>
       
       <div class="clr"></div>
       
       <div class="col" style="width:100%">
       
       <fieldset class="adminform">
				<legend><?php echo JText::_( 'Information' ); ?></legend>
				<div id="add_row_receipt_tmp" style="float:left;">
                	<a href="javascript:void(0);" onclick="javascript:add_row(1);">Add Row</a>
                    <input type="hidden" name="total" value="1" />
                </div>
                <div class="clr"></div>
                <div>
				<table id="new_receipt_table" class="admintable" style="width:100%;vertical-align:top;">
				
                <tr>
                	<td class="key" style="width:33%;text-align:left;">
						<label for="con_position">
						<?php echo JText::_( 'Policy No' ); ?>
						</label>
					</td>
					<td class="key" style="width:33%;text-align:left;" >
						<label for="con_position">
						<?php echo JText::_( 'References' ); ?>
						</label>
					</td>
                    <td class="key" style="width:33%;text-align:left;" >
                    	<label for="con_position">
						<?php echo JText::_( 'Amount' ); ?>
						</label>
                    </td>
                </tr>
                
                <tr>
					<td>
						<input class="inputbox" type="text" name="policy_0" id="name" size="60" maxlength="255" value="" />
					</td>
					<td >
						<input class="inputbox" type="text" name="ref_0" id="name" size="60" maxlength="255" value="" />
					</td>
                    <td >
                    	<input class="inputbox" type="text" name="amt_0" id="name" size="60" maxlength="255" value="" />
                    </td>
				</tr>
                </table>
                </div>
       </fieldset>
       </div>      
    <input type="hidden" name="option" value="com_insureasia" />
	<input type="hidden" name="task" value="" />
	<?php echo JHTML::_( 'form.token' ); ?>   
</form>            