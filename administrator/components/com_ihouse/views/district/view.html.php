<?php
/**
* @version		$Id: view.html.php 10711 2008-08-21 10:09:03Z eddieajau $
* @package		Joomla
* @subpackage	Poll
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Poll component
 *
 * @static
 * @package		Joomla
 * @subpackage	Poll
 * @since 1.0
 */
class IhouseViewDistrict extends JView
{
	function display($tpl = null)
	{
		global $mainframe, $option;

		$layout		= 	JRequest::getCmd('layout', 'default');
		
		$user		=& 	JFactory::getUser();
		
		$db 	  	=& 	JFactory::getDBO();
		
		
		
		$filter_order		= $mainframe->getUserStateFromRequest( "$option.filter_order",		'filter_order',		'd.id',	'cmd' );
		$filter_order_Dir	= $mainframe->getUserStateFromRequest( "$option.filter_order_Dir",	'filter_order_Dir',	'',		'word' );
		$filter_state		= $mainframe->getUserStateFromRequest( "$option.filter_state",		'filter_state',		'',		'word' );
		$search				= $mainframe->getUserStateFromRequest( "$option.search",			'search',			'',		'string' );
		$search				= JString::strtolower( $search );
		
		if(!preg_match('/^d\./', $filter_order))
			$filter_order	=	'd.id';
		
		$limit		= $mainframe->getUserStateFromRequest( 'global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int' );
		$limitstart	= $mainframe->getUserStateFromRequest( $option.'.limitstart', 'limitstart', 0, 'int' );
		
		
		if($filter_order && $filter_order_Dir) {
			$orderby 	= ' ORDER BY '. $filter_order .' '. $filter_order_Dir;
		}
		
		
		switch($layout) {
			case 'edit' :
				$id 		= 	JRequest::getCmd('cid');
		
				//$row		= 	$this->getProperty($id);
		
				//$this->assignRef('property_no',	$id);
				//$this->assignRef('data', $row);
				
				break;
			default :
				$query = "SELECT * FROM #__ihouse_district AS d "
						. " "
						. $where
						. " "
						. $orderby;
				
					$db->setQuery( $query );
					$rows = $db->loadObjectList();
					$this->assignRef('items', $rows);
				break;
		}
		
		
		// table ordering
		$lists['order_Dir']	= $filter_order_Dir;
		$lists['order']		= $filter_order;

		// search filter
		$lists['search']= $search;
		
		$this->assignRef('lists',		$lists);
		
		
		$this->assignRef('pagination',	$pagination);
		parent::display($tpl);
		
	}
	
	/*function getListHTML($name = 'receipt_payment_by') {
		$html = '';
		$html .= '<input type="radio" name="'.$name.'" value="CS" onchange="document.getElementById(\'chq_no\').style.display=\'none\';" /> Cash';
		$html .= '<input type="radio" name="'.$name.'" value="CQ" onchange="javascript:toggle(\'chq_no\');" /> Cheque';
		
		$html .= '&nbsp;&nbsp;';
		
		$html .= '<span id="chq_no" style="display:none;">';
		
		$html .= 'Cheque # : <input type="text" id="receipt_cheque_id" name="receipt_cheque_id" value="" />';
		
		$html .= '</span>';
		return $html;
	}
	*/
	
}
?>
