<?php defined('_JEXEC') or die('Restricted access'); ?>

<script type="text/javascript">
	var baseurl = '<?php echo JURI::base(true); ?>';
</script>


<?php JHTML::_('behavior.tooltip'); ?>
<?php JHTML::_('behavior.calendar'); ?>

<?php JHTML::_('script', 'admin.insureasia.js', 'administrator/components/com_insureasia/assets/'); ?>
<?php
	
	JToolBarHelper::title( JText::_( 'iHouse360 Edit : User Credit' ), 'plugin.png' );
	JToolBarHelper::save();
	JToolBarHelper::apply();
	JToolBarHelper::cancel();

	//JToolBarHelper::help( 'screen.plugins' );
	
	$ordering = ($this->lists['order'] == 'p.folder' || $this->lists['order'] == 'p.ordering');
	$row =& $this->data;

?>

<form action="index.php" method="post" name="adminForm">

		<div class="col width-60">
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'User :' ); ?></legend>
				<table class="admintable">
                <?php
				if ($row->id) {
					?>
					<tr>
						<td class="key">
							<label>
								<?php echo JText::_( 'ID' ); ?>:
							</label>
						</td>
						<td>
							<strong><?php echo $row->id;?></strong>
						</td>
					</tr>
					<?php
				}
				?>
				<tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Username' ); ?>:
						</label>
					</td>
					<td>
						<?php echo $row->username; ?>
					</td>
				</tr>
                <tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Credits' ); ?>:
						</label>
					</td>
					<td>
						<input name="credits" type="text" value="<?php echo $row->credit; ?>" />
					</td>
				</tr>
				</table>
			</fieldset>
            
            
      
       </div>
       
       <div class="clr"></div>
    <input type="hidden" name="update" value="1" />   
    <input type="hidden" name="user_id" value="<?php echo $row->id;?>" />    
    <input type="hidden" name="option" value="com_ihouse" />
	<input type="hidden" name="task" value="" />
    <input type="hidden" name="view" value="credit" />
	<?php echo JHTML::_( 'form.token' ); ?>   
</form>            