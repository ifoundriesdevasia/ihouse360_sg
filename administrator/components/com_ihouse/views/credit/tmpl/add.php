<?php defined('_JEXEC') or die('Restricted access'); ?>

<script type="text/javascript">
	var baseurl = '<?php echo JURI::base(true); ?>';
</script>


<?php JHTML::_('behavior.tooltip'); ?>
<?php JHTML::_('behavior.calendar'); ?>

<?php //JHTML::_('script', 'admin.ihouse.js', 'administrator/components/com_ihouse/assets/'); ?>
<?php
	
	JToolBarHelper::title( JText::_( 'iHouse360 Add User + Credits' ), 'plugin.png' );
	JToolBarHelper::save();
	JToolBarHelper::apply();
	JToolBarHelper::cancel();

	//JToolBarHelper::help( 'screen.plugins' );
	
	$ordering = ($this->lists['order'] == 'p.folder' || $this->lists['order'] == 'p.ordering');
	$rows =& $this->items;

?>
<div style="padding:5px;">
<form action="index.php" method="post" name="adminForm">

		<div class="col width-60">
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'User : '); ?></legend>
				<table class="admintable">
				<tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Username ' ); ?>:
						</label>
					</td>
					<td>
						<?php echo $this->users_list ?>
					</td>
				</tr>
                <tr>
                	<td class="key">
						<label for="name">
							<?php echo JText::_( 'Credits ' ); ?>:
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="credits" size="10" maxlength="50" value="" />
					</td>
                </tr> 
				</table>
			</fieldset>

       </div>
       
       <div class="clr"></div>
       
       
    <input type="hidden" name="option" value="com_ihouse" />
    <input type="hidden" name="view" value="credit" />
	<input type="hidden" name="task" value="" />
	<?php echo JHTML::_( 'form.token' ); ?>   
</form>   
</div>    