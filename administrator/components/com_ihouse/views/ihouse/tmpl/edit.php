<?php defined('_JEXEC') or die('Restricted access'); ?>

<script type="text/javascript">
	var baseurl = '<?php echo JURI::base(true); ?>';
</script>

<?php 
	$editor =& JFactory::getEditor('jce');
	//$params = array('smilies'=> '0' ,'style'  => '1' ,  'layer'  => '1' , 'table'  => '1' ,'clear_entities'=>'1');
	$params = array();
?>
<?php JHTML::_('behavior.tooltip'); ?>
<?php JHTML::_('behavior.calendar'); ?>

<?php JHTML::_('script', 'admin.ihouse.js', 'administrator/components/com_ihouse/assets/'); ?>
<?php
	
	JToolBarHelper::title( JText::_( 'iHouse360 : Edit Property ' ), 'plugin.png' );
	JToolBarHelper::save();
	JToolBarHelper::apply();
	JToolBarHelper::cancel();

	//JToolBarHelper::help( 'screen.plugins' );
	
	$ordering = ($this->lists['order'] == 'p.folder' || $this->lists['order'] == 'p.ordering');
	$rows =& $this->items;

?>
<div style="padding:5px;">
<form action="index.php" method="post" name="adminForm">

		<div class="col width-70">
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Property : '. $this->data->name_ch . " " . $this->data->name_en ); ?></legend>
				<table class="admintable">
				<tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Name (English) ' ); ?>:
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="name_en" size="60" maxlength="255" value="<?php echo $this->data->name_en ?>" />
					</td>
				</tr>
                <tr>
                	<td class="key">
						<label for="name">
							<?php echo JText::_( 'Name (Chinese) ' ); ?>:
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="name_ch" size="60" maxlength="255" value="<?php echo $this->data->name_ch ?>" />
					</td>
                </tr>
                <tr>
                <td class="key">
						<label for="name">
							<?php echo JText::_( 'Address ' ); ?>:
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="address" size="60" maxlength="255" value="<?php echo $this->data->address ?>" />
					</td>
                </tr>
                <tr>
                <td class="key">
						<label for="name">
							<?php echo JText::_( 'Postal Code ' ); ?>:
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="postcode" size="60" maxlength="255" value="<?php echo $this->data->postcode ?>" />
					</td>
                </tr>
                <tr>
                <td class="key">
						<label for="name">
							<?php echo JText::_( 'Property Type ' ); ?>:
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="property_type" size="60" maxlength="255" value="<?php echo $this->data->property_type ?>" />
					</td>
                </tr>
                <tr>
                <td class="key">
						<label for="name">
							<?php echo JText::_( 'Tenure ' ); ?>:
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="tenure" size="60" maxlength="255" value="<?php echo $this->data->tenure ?>" />
					</td>
                </tr>    
                <tr>
                <td class="key">
						<label for="name">
							<?php echo JText::_( 'Property Class ' ); ?>:
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="property_class" size="60" maxlength="255" value="<?php echo $this->data->property_class ?>" />
					</td>
                </tr>
                <tr>
                <td class="key">
						<label for="name">
							<?php echo JText::_( 'Year Built ' ); ?>:
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="year_built" size="60" maxlength="255" value="<?php echo $this->data->year_built ?>" />
					</td>
                </tr>
                <tr>
                <td class="key">
						<label for="name">
							<?php echo JText::_( 'Room Size ' ); ?>:
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="room_size" size="60" maxlength="255" value="<?php echo $this->data->room_size ?>" />
					</td>
                </tr>
                <tr>
                <td class="key">
						<label for="name">
							<?php echo JText::_( 'District ' ); ?>:
						</label>
					</td>
					<td>
						<?php echo $this->district ?>Choose 1
					</td>
                </tr>
                <tr>
                <td class="key">
						<label for="name">
							<?php echo JText::_( 'Condo Units ' ); ?>:
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="units_on_condo" size="60" maxlength="255" value="<?php echo $this->data->units_on_condo ?>" />
					</td>
                </tr>    
                <tr>
                <td class="key">
						<label for="name">
							<?php echo JText::_( 'Description ' ); ?>:
						</label>
					</td>
					<td>
                    	<?php echo $editor->display( 'description',$this->data->description , '90%', '300', '70', '20', false, $params); ?>
						
					</td>
                </tr>
                <tr>
                <td class="key">
						<label for="name">
							<?php echo JText::_( 'Nearby MRT ' ); ?>:
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="nearby_mrt" size="60" maxlength="255" value="<?php echo $this->data->nearby_mrt ?>" />Separated by '|'
					</td>
                </tr>
                <tr>
                <td class="key">
						<label for="name">
							<?php echo JText::_( 'Nearby School ' ); ?>:
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="nearby_school" size="60" maxlength="255" value="<?php echo $this->data->nearby_school ?>" />Separated by '|'
					</td>
                </tr>  
                <tr>
                <td class="key">
						<label for="name">
							<?php echo JText::_( 'Developer ' ); ?>:
						</label>
					</td>
					<td>
						<input class="inputbox" type="text" name="developer" size="60" maxlength="255" value="<?php echo $this->data->developer ?>" />
					</td>
                </tr>
                <tr>
                <td class="key">
						<label for="name">
							<?php echo JText::_( 'Floor Plan ' ); ?>:
						</label>
					</td>
					<td>
						<?php echo $this->floorplan ?>
					</td>
                </tr>  
                <!--
				<tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Date' ); ?>:
						</label>
					</td>
					<td >
						<input class="inputbox" type="text" name="receipt_date" id="receipt_date" size="11" maxlength="255" value="<?php echo $this->data->receipt_date ?>" readonly="readonly" />
                        <a href="#" onclick="return showCalendar('receipt_date', '%Y-%m-%d');"><img class="calendar" src="images/blank.png" alt="calendar" /></a>
					</td>
				</tr>
                -->
				</table>
			</fieldset>

       </div>
       
       <div class="clr"></div>
       
       
    <input type="hidden" name="option" value="com_ihouse" />
	<input type="hidden" name="task" value="" />
    <input type="hidden" name="update" value="1" />
    <input type="hidden" name="property_id" value="<?php echo (int) $this->property_no ?>" />
	<?php echo JHTML::_( 'form.token' ); ?>   
</form>   
</div>         