<?php
/**
* @version		$Id: view.html.php 10711 2008-08-21 10:09:03Z eddieajau $
* @package		Joomla
* @subpackage	Poll
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Poll component
 *
 * @static
 * @package		Joomla
 * @subpackage	Poll
 * @since 1.0
 */
class IhouseViewIhouse extends JView
{
	function display($tpl = null)
	{
		global $mainframe, $option;

		$user		=& 	JFactory::getUser();
		
		$uid		=	$user->get('id');	
		
		$gid		=	$user->get('gid');
		
		$layout		= 	JRequest::getCmd('layout', 'default');
		
		$db 	  	=& 	JFactory::getDBO();
		
		$filter_order		= $mainframe->getUserStateFromRequest( "$option.filter_order",		'filter_order',		'p.id',	'cmd' );
		$filter_order_Dir	= $mainframe->getUserStateFromRequest( "$option.filter_order_Dir",	'filter_order_Dir',	'',		'word' );
		$filter_state		= $mainframe->getUserStateFromRequest( "$option.filter_state",		'filter_state',		'',		'word' );
		$search				= $mainframe->getUserStateFromRequest( "$option.search",			'search',			'',		'string' );
		$search				= JString::strtolower( $search );
		
		if(!preg_match('/^p\./', $filter_order))
			$filter_order	=	'p.id';
		
		$limit		= $mainframe->getUserStateFromRequest( 'global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int' );
		$limitstart	= $mainframe->getUserStateFromRequest( $option.'.limitstart', 'limitstart', 0, 'int' );
		
		
		$where = array();
		
		if ($search)
		{
			$where[] = ' LOWER(p.name_en) LIKE '.$db->Quote( '%'.$db->getEscaped( $search, true ).'%', false ).' OR LOWER(p.postcode) LIKE '.$db->Quote( '%'.$db->getEscaped( $search, true ).'%', false );
			
		} 
		

		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
		
		if($filter_order && $filter_order_Dir) {
			$orderby 	= ' ORDER BY '. $filter_order .' '. $filter_order_Dir;
		}
		
		switch($gid) {
			case 25	: /* SUPER ADMINISTRATOR */
			case 23	: 
			default : 
			
				//$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
				
				$query	=	'SELECT * ' 
							.	' FROM #__ihouse_property AS p '
							.	' '
							.	$where
							.	' '
							.	$orderby;
				
				break;			
		}
		
		$db->setQuery( $query , $limitstart, $limit);
		
		$rows = $db->loadObjectList();
		
		if ($db->getErrorNum())
		{
			echo $db->stderr();
			return false;
		}
		
		
		/* COUNT TOTAL */
		$query = 'SELECT COUNT(id) '
			. ' FROM #__ihouse_property '
			. $where
		;
		
		$db->setQuery( $query );
		$total = $db->loadResult();
		
		jimport('joomla.html.pagination');
		$pagination = new JPagination( $total, $limitstart, $limit );
		
		// state filter
		//$lists['state']	= JHTML::_('grid.state',  $filter_state );

		// table ordering
		$lists['order_Dir']	= $filter_order_Dir;
		$lists['order']		= $filter_order;

		// search filter
		$lists['search']= $search;
		
		
		$this->assignRef('user',		JFactory::getUser());
		$this->assignRef('lists',		$lists);
		$this->assignRef('items',		$rows);
		$this->assignRef('pagination',	$pagination);
		
		switch($layout) {
			case 'edit' :
				$id 		= 	JRequest::getCmd('cid');
		
				$row		= 	$this->getProperty($id);
				$district	=	$this->getDistrictHTML($row->district_id);
				$floorplan	=	$this->getFloorPlanHTML($row->floor_plan);
				
				$this->assignRef('district', $district);
				$this->assignRef('floorplan', $floorplan);
				
				$this->assignRef('property_no',	$id);
				$this->assignRef('data', $row);
				
				break;
			case 'add' :	
				$model 		= $this->getModel('ihouse');
			
				$district	=	$this->getDistrictHTML();
				$floorplan	=	$this->getFloorPlanHTML();
				$csv = $model->propertyCSVFileInsert();
				
				
				$this->assignRef('csv_info',$csv);
				
				$this->assignRef('district', $district);
				$this->assignRef('floorplan', $floorplan);
				break;
		}
		
		parent::display($tpl);
		
	}
	
	function getDistrictHTML($id = '') {
			
		$db 	  =& JFactory::getDBO();
		
		$query	=	"SELECT * FROM #__ihouse_district ";
		
			$db->setQuery( $query );	
		
			$result = $db->loadObjectList();
		
		$html = '';
		$html .= '<select name="district_id">';
		foreach($result as $r) :
			
			$selected = '';
			if($r->code == $id)
				$selected = 'selected="selected"';
		
			$html .= '<option value="'.$r->code.'" '.$selected.' >'.$r->code.'</option>';
		
		endforeach;
		$html .= '</select>';
			
		return $html;	
	}
	
	function getFloorPlanHTML($floorplan = '') {
		
		$arr = array();
		if(!empty($floorplan))
			$arr = explode('|',$floorplan);
		
		$db 	  =& JFactory::getDBO();
		
		$query	=	"SELECT * FROM #__ihouse_floorplan ";
		
			$db->setQuery( $query );	
		
			$result = $db->loadObjectList();
		
		$html = '';
		$html .= '<select name="floor_plan[]" multiple="multiple">';
		foreach($result as $r) :
			
			$selected = '';
			if(in_array($r->type_ch, $arr))
				$selected = 'selected="selected"';
		
			$html .= '<option value="'.$r->type_ch.'" '.$selected.' >'.$r->type_ch.'</option>';
		
		endforeach;
		$html .= '</select>';
			
		return $html;	
	}
	
	
	function getProperty($id = 0) {
		if(!$id)
			return false;
		
		$db 	  =& JFactory::getDBO();
		
		$query	=	"SELECT * FROM #__ihouse_property "
					. " WHERE id = '$id' ";
		
			$db->setQuery( $query );
		
		return $db->loadObject();
	}

}
?>
