<?php defined('_JEXEC') or die('Restricted access'); ?>

<script type="text/javascript">
	var baseurl = '<?php echo JURI::base(true); ?>';
</script>


<?php JHTML::_('behavior.tooltip'); ?>
<?php JHTML::_('behavior.calendar'); ?>

<?php JHTML::_('script', 'admin.insureasia.js', 'administrator/components/com_insureasia/assets/'); ?>
<?php
	
	JToolBarHelper::title( JText::_( 'Edit : Promotion' ), 'plugin.png' );
	JToolBarHelper::save();
	JToolBarHelper::apply();
	JToolBarHelper::cancel();

	//JToolBarHelper::help( 'screen.plugins' );
	
	$ordering = ($this->lists['order'] == 'p.folder' || $this->lists['order'] == 'p.ordering');
	$row =& $this->data;

?>

<form action="index.php" method="post" name="adminForm">

		<div class="col width-60">
			<fieldset class="adminform">
				<legend><?php echo JText::_( $row->name ); ?></legend>
				<table class="admintable">
                <?php
				if ($row->id) {
					?>
					<tr>
						<td class="key">
							<label>
								<?php echo JText::_( 'ID' ); ?>:
							</label>
						</td>
						<td>
							<strong><?php echo $row->id;?></strong>
						</td>
					</tr>
					<?php
				}
				?>
				<tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Name' ); ?>:
						</label>
					</td>
					<td>
						<input name="promotion" type="text" value="<?php echo $row->name; ?>" />
					</td>
				</tr>
                <tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Description' ); ?>:
						</label>
					</td>
					<td>
                    	<textarea name="description" rows="20" cols="80"><?php echo $row->description; ?></textarea>
					</td>
				</tr>
                <?php if($row->credit) : ?>
                <tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Credits ' ); ?>:
						</label>
					</td>
                    
					<td>
						<input name="credits" type="text" value="<?php echo $row->credit; ?>" />
					</td>
				</tr>
                <?php endif; ?>
                
                <?php if($row->discount) : ?>
                <tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Discount ' ); ?>:
						</label>
					</td>
                    
					<td>
						<input name="discount" type="text" value="<?php echo $row->discount; ?>" />E.g : 0.25 => 25%
					</td>
				</tr>
                <?php endif; ?>
                
                <?php if($row->status) : ?>
                <tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Status' ); ?>:
						</label>
					</td>
                    
					<td>
						<?php echo $row->status ?>
					</td>
				</tr>
                <?php endif; ?>
				</table>
			</fieldset>
            
            
      
       </div>
       
       <div class="clr"></div>
    <input type="hidden" name="update" value="1" />   
    <input type="hidden" name="cid" value="<?php echo $row->id;?>" />    
    <input type="hidden" name="option" value="com_ihouse" />
	<input type="hidden" name="task" value="" />
    <input type="hidden" name="view" value="promotion" />
	<?php echo JHTML::_( 'form.token' ); ?>   
</form>            