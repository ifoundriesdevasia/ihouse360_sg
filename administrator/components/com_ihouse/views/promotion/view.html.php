<?php
/**
* @version		$Id: view.html.php 10711 2008-08-21 10:09:03Z eddieajau $
* @package		Joomla
* @subpackage	Poll
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Poll component
 *
 * @static
 * @package		Joomla
 * @subpackage	Poll
 * @since 1.0
 */
class IhouseViewPromotion extends JView
{
	function display($tpl = null)
	{
		global $mainframe, $option;

		$layout		= 	JRequest::getCmd('layout', 'default');
		
		$user		=& 	JFactory::getUser();
		
		$this->assignRef('pagination',	$pagination);
		
		switch($layout) {
			case 'edit' :
				$id 			= 	JRequest::getCmd('cid');
		
				$row			= 	$this->getPromotion($id);
				$row->status	=	$this->getStatus($row->status); // drop down
		
				//$this->assignRef('property_no',	$id);
				$this->assignRef('data', $row);
				
				break;
			default : 
				$this->assignRef('items', $this->getPromotionList());
				break;
		}
		
		
		parent::display($tpl);
		
	}
	
	function getStatus($status) {
		if(empty($status))
			return false;
		
		$html = '';
		
		$html .= '<select name="status">';
		
		$html .= '	<option value="yes" '.(($status == "yes")?' selected="selected" ':'').'>Yes</option>';
		$html .= '	<option value="no" '.(($status == "no")?' selected="selected" ':'').'>No</option>';
		
		$html .= '</select>';
		
		return $html;
	}
	
	function getPromotion($id) {
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
		
		$query 		= 	"SELECT * FROM #__ihouse_promotion "
						.	" WHERE id = '$id'";
			$db->setQuery( $query );
			$list = $db->loadObject();
		
		if(!empty($list->params)) {
			
			$arr = explode('|', $list->params);
			foreach($arr as $t) :
				$a = explode('=', $t);
				
				$par = $a[0];
				$val = $a[1];
				
				$list->$par = $val;
			endforeach;
		}
		
		return $list;			
	}
	
	function getPromotionList() {
			
		$db 	  	=& 	JFactory::getDBO();
		
		$query 		= "SELECT * FROM #__ihouse_promotion ";
						
			$db->setQuery( $query );
			$lists = $db->loadObjectList();
		
		return $lists;
	}
	
	
	
}
?>
