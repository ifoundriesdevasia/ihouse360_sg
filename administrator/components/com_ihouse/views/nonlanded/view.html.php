<?php
/**
* @version		$Id: view.html.php 10711 2008-08-21 10:09:03Z eddieajau $
* @package		Joomla
* @subpackage	Poll
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Poll component
 *
 * @static
 * @package		Joomla
 * @subpackage	Poll
 * @since 1.0
 */
class IhouseViewNonlanded extends JView
{
	function display($tpl = null)
	{
		global $mainframe, $option;

		$layout		= 	JRequest::getCmd('layout', 'default');
		
		$user		=& 	JFactory::getUser();
		
		/*$filter_order		= $mainframe->getUserStateFromRequest( "$option.filter_order",		'filter_order',		'c.credit',	'cmd' );
		$filter_order_Dir	= $mainframe->getUserStateFromRequest( "$option.filter_order_Dir",	'filter_order_Dir',	'',		'word' );
		$filter_state		= $mainframe->getUserStateFromRequest( "$option.filter_state",		'filter_state',		'',		'word' );
		$search				= $mainframe->getUserStateFromRequest( "$option.search",			'search',			'',		'string' );
		$search				= JString::strtolower( $search );
		
		if(!preg_match('/^c\./', $filter_order))
			$filter_order	=	'c.credit';
		
		$limit		= $mainframe->getUserStateFromRequest( 'global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int' );
		$limitstart	= $mainframe->getUserStateFromRequest( $option.'.limitstart', 'limitstart', 0, 'int' );
		
		if($filter_order && $filter_order_Dir) {
			$orderby 	= ' ORDER BY '. $filter_order .' '. $filter_order_Dir;
		}
		
		$this->assignRef('pagination',	$pagination);
		*/
		
		/*
		switch($layout) {
			case 'edit' :
				$id 		= 	JRequest::getCmd('cid');
		
				$row		= 	$this->getUserCreditData($id);
		
				//$this->assignRef('property_no',	$id);
				$this->assignRef('data', $row);
				
				break;
			case 'add' : 
				$this->assignRef('users_list', $this->getUsersListHTML());
				break;
			default : 
				$db 	  	=& 	JFactory::getDBO();
		
				$query = "SELECT * FROM #__ihouse_users_credit AS c "
							. " LEFT JOIN #__users AS u ON u.id = c.user_id " 
							. $where
							. $orderby;
		
		
					$db->setQuery( $query );
					$rows = $db->loadObjectList();
					
				$this->assignRef('items', $rows);
				break;
		}
		*/
		// table ordering
		//$lists['order_Dir']	= $filter_order_Dir;
		//$lists['order']		= $filter_order;

		// search filter
		//$lists['search']= $search;
		
		$query = "SELECT * FROM #__ihouse_non_landed_price_index ";
			$db->setQuery( $query );
			$rows = $db->loadObjectList();
		echo print_r($rows);			
		$this->assignRef('items', $rows);
		
		$this->assignRef('lists',		$lists);
		
		parent::display($tpl);
		
	}
	
	function getUserCreditData($uid) {
		if(!$uid)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
		
		$query 		= "SELECT * FROM #__ihouse_users_credit AS c "
						. " LEFT JOIN #__users AS u ON u.id = c.user_id "
						. " WHERE c.user_id = '$uid'";
						
			$db->setQuery( $query );
			$list = $db->loadObject();
		
		return $list;
	}
	
}
?>
