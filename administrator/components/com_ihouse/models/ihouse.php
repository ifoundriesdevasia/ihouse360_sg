<?php
/**
* @version		$Id: poll.php 10752 2008-08-23 01:53:31Z eddieajau $
* @package		Joomla
* @subpackage	Polls
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class IhouseModelIhouse extends JModel
{
	function __construct()
	{
		global $mainframe;

		parent::__construct();

		/*$config = JFactory::getConfig();

		// Get the pagination request variables
		$this->setState('limit', $mainframe->getUserStateFromRequest('com_newsfeeds.limit', 'limit', $config->getValue('config.list_limit'), 'int'));
		$this->setState('limitstart', JRequest::getVar('limitstart', 0, '', 'int'));

		$id = JRequest::getVar('id', 0, '', 'int');
		$this->setId((int)$id);
		*/
	}
	
	function getPolicyProfile($pid) {
		$db = $this->getDBO();
		
		$query = 	"SELECT * FROM #__ia_receipt_items "
					.	" WHERE id = '$pid'; ";
		
			$db->setQuery( $query );
		
		return $db->loadObject();
	}
	
	function delete_user_credit() {
		$db = $this->getDBO();
		
		$cid			=	JRequest::getVar('cid', array());
		
		
		foreach($cid as $id) {
			/* DELETE #__ihouse_users_credit */
			$query = "DELETE FROM #__ihouse_users_credit WHERE user_id = '$id' ";
			
				$db->setQuery( $query );
				$db->query();
			
		}
		return true;
	}
	
	function delete() {
		$db = $this->getDBO();
		$cid			=	JRequest::getVar('cid', array());
		
		
		foreach($cid as $id) {
		
			/* DELETE #__ihouse_property */
			$query = "DELETE FROM #__ihouse_property WHERE id = '$id' ";
			
				$db->setQuery( $query );
				$db->query();
			
		}
		
		return true;
	}
		
	function update_promotion($id = 0) {
		if(!$id)
			return false;
		
		$db = $this->getDBO();
		
		$name				=	JRequest::getVar('promotion');
		$description		=	JRequest::getVar('description');
		
		/* Promotion 1 */
		$credits		=	JRequest::getVar('credits',0);
		
		/* Promotion 2 & 4 */
		$discount		=	JRequest::getVar('discount',0);
		
		/* Promotion 3 */ 
		$status			= 	JRequest::getVar('status', '');
		
		$params 		= array();
		
		if($credits)
			$params[] = 'credit='.$credits;
		
		if($discount)
			$params[] = 'discount='.$discount;
			
		if($status)
			$params[] = 'status='.$status;
		
		$params = implode('|', $params);
		
		$query = 	"UPDATE #__ihouse_promotion "
					. " SET name = '$name', description = '$description', params = '$params' "
					. " WHERE id = '$id'";
			$db->setQuery( $query );
			$db->query();
		
		return true;
	}
	
	function update_user_credit($uid = 0) {
		
		if(!$uid)
			return false;
		
		$db = $this->getDBO();
		
		$credits		=	JRequest::getVar('credits');
		
		$query = 	"UPDATE #__ihouse_users_credit "
					. " SET credit = ". $credits
					. " WHERE user_id = '$uid'";
			$db->setQuery( $query );
			$db->query();
		
		return true;
	}
	
	function update($id = 0) {
		
		if(!$id)
			return false;
		
		$db = $this->getDBO();

		setlocale(LC_ALL, 'en_US.UTF-8');
		
		include_once('components/com_ihouse/assets/json.php');
		
		$json = new Services_JSON();
		
		$user		=& 	JFactory::getUser();
		$uid		= 	$user->id;
		
		$property_id		=	JRequest::getVar('property_id');
		$name_en			=	JRequest::getVar('name_en', '');
		$name_en			=	str_replace("'","\'", $name_en);
		$name_ch			=	JRequest::getVar('name_ch', '');
		$address			=	JRequest::getVar('address', '');
		$address			=	str_replace("'","\'", $address);
		$postalcode			=	JRequest::getVar('postcode', '');
		$property_type 		= JRequest::getVar('property_type', '');
		$tenure				=	JRequest::getVar('tenure', '');
		$property_class		=	JRequest::getVar('property_class', '');
		$year_built			=	JRequest::getVar('year_built', '');
		$room_size			=	JRequest::getVar('room_size', '');
		$district			=	JRequest::getVar('district_id', '');
		$condo_units		=	JRequest::getVar('units_on_condo', '');
		$description		=	JRequest::getVar('description', '', 'post','string', JREQUEST_ALLOWRAW);
		$description		=	str_replace("'","\'", $description);
		$nearby_mrt			=	JRequest::getVar('nearby_mrt', '');
		$nearby_mrt			=	str_replace("'","\'", $nearby_mrt);
		$nearby_mrt			=	str_replace('"','\"', $nearby_mrt);
		$nearby_school		=	JRequest::getVar('nearby_school', '');
		$nearby_school		=	str_replace("'","\'", $nearby_school);
		$nearby_school		=	str_replace('"','\"', $nearby_school);
		$developer			=	JRequest::getVar('developer', '');
		$developer			=	str_replace("'","\'", $developer);
		$floorplan			=	implode('|',JRequest::getVar('floor_plan', array()));

		$query = 	" SELECT name_en FROM #__ihouse_property WHERE id = '$id' ";
			$db->setQuery($query);
			$projectname = $db->loadResult();

		$query = 	" UPDATE #__ihouse_property "
					. " SET name_en = '$name_en', name_ch = '$name_ch', address = '$address', postcode = '$postalcode', property_type = '$property_type' "
					. " , tenure = '$tenure', property_class = '$property_class', year_built = '$year_built',room_size = '$room_size' "
					. " , district_id = '$district', units_on_condo = '$condo_units', description = '$description' "
					. " , nearby_mrt = '$nearby_mrt', nearby_school = '$nearby_school', developer = '$developer' "
					. " , floor_plan = '$floorplan' "
					. " WHERE id = '$id' ; ";
		
			$db->setQuery( $query );
			$db->query();
		
		$slug	=	preg_replace('/(\s|\.|\')+/','-',strtolower($projectname));
		
		$query	=	" SELECT COUNT(id) FROM #__ihouse_property_slug WHERE slug LIKE '%".$slug."%' ";
			$db->setQuery( $query );
			$exists = $db->loadResult();
		
		if($exists) {
			$query = 	" UPDATE #__ihouse_property_slug "
					.	" SET project_name = '$name_en' "
					.	" , slug = '".preg_replace('/(\s|\.|\')+/','-',strtolower($name_en))."' "
					.	" WHERE slug LIKE '".$slug."' ";		
				$db->setQuery( $query );
				$db->query();
		} else {
			$query = " INSERT INTO #__ihouse_property_slug (project_name, slug) VALUES ('$name_en','$slug')";
				$db->setQuery( $query );
				$db->query();
		}
		
		$url = 'http://maps.googleapis.com/maps/api/geocode/json?address=Singapore+'.$postalcode.'+'.str_replace(' ','+',$address).'&sensor=false';
		
		$ch = curl_init();
						
		curl_setopt( $ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1" );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    	curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true); 
			
		$content = curl_exec( $ch );
		$data = $json->decode($content);
						
		$lat = $data->results[0]->geometry->location->lat;
		$lng = $data->results[0]->geometry->location->lng;
		
		$query =	" UPDATE #__ihouse_location_gps SET lat = '$lat' , lng = '$lng' WHERE postcode = '$postalcode' ";
			$db->setQuery( $query );
			$db->query();
			
		return true;
	}
	
	function save_user_credit() {
		$db = $this->getDBO();
		
		$user_id		=	JRequest::getVar('user_id', 0);
		$credit			=	JRequest::getVar('credits', 0);
		
		/* CHECK IF USER ALREADY HAVE CREDITS */
		$query = 	"SELECT id FROM #__ihouse_users_credit "
					.	" WHERE user_id ='$user_id' " ;
					
			$db->setQuery($query);
			$exist = $db->loadResult();
		
		if(!empty($exist))
			return false;
		
		$query = 	"SELECT username FROM #__users "
					.	" WHERE id ='$user_id' " ;
			$db->setQuery($query);
			$username = $db->loadResult();		
		
		$query = 	"INSERT INTO #__ihouse_users_credit "
					. " VALUES(NULL, '$user_id','$credit', 0) ";
					
			$db->setQuery($query);
			$db->query();
		
		return true;
	}
	
	function save(){
		$db = $this->getDBO();

		//$user		=& 	JFactory::getUser();
		//$uid		= $user->id;
		//$username	= $user->username;

		/* STORE, receipt_id */
		
		$property_id		=	JRequest::getVar('property_id');
		$name_en			=	JRequest::getVar('name_en', '');
		$name_ch			=	JRequest::getVar('name_ch', '');
		$address			=	JRequest::getVar('address', '');
		$postalcode			=	JRequest::getVar('postcode', '');
		$property_type 		= 	JRequest::getVar('property_type', '');
		$tenure				=	JRequest::getVar('tenure', '');
		$property_class		=	JRequest::getVar('property_class', '');
		$year_built			=	JRequest::getVar('year_built', '');
		$room_size			=	JRequest::getVar('room_size', '');
		$district			=	JRequest::getVar('district_id', '');
		$condo_units		=	JRequest::getVar('units_on_condo', '');
		$description		=	JRequest::getVar('description', '', 'post','string', JREQUEST_ALLOWRAW);
		$nearby_mrt			=	JRequest::getVar('nearby_mrt', '');
		$nearby_school		=	JRequest::getVar('nearby_school', '');
		$developer			=	JRequest::getVar('developer', '');
		$floorplan			=	implode('|', JRequest::getVar('floor_plan', array()));
		
		
		$query = 	"INSERT INTO #__ihouse_property "
					. " VALUES(NULL, '$name_en','$name_ch','$address','$postalcode','$property_type','$tenure' " 
					. " ,'$property_class','$year_built','$room_size','$district','$condo_units','$description' "
					. " ,'$nearby_mrt','$nearby_school','$developer', '$floorplan');";
		
			$db->setQuery($query);
			$db->query();

		$slug	=	preg_replace('/(\s|\.|\')+/','-',strtolower($name_en));
		
		$query	=	" SELECT COUNT(id) FROM #__ihouse_property_slug WHERE slug LIKE '%".$slug."%' ";
			$db->setQuery( $query );
			$exists = $db->loadResult();
		
		if($exists) {
			$query = 	" UPDATE #__ihouse_property_slug "
					.	" SET project_name = '$name_en' "
					.	" , slug = '".preg_replace('/(\s|\.|\')+/','-',strtolower($name_en))."' "
					.	" WHERE slug LIKE '".$slug."' ";		
				$db->setQuery( $query );
				$db->query();
		} else {
			$query = " INSERT INTO #__ihouse_property_slug (project_name, slug) VALUES('$name_en','$slug')";
				$db->setQuery( $query );
				$db->query();
		}
		return true;
	}
	
	function propertyCSVFileInsert() {
		$db = $this->getDBO();
		
		setlocale(LC_ALL, 'en_US.UTF-8');
		
		include_once('components/com_ihouse/assets/json.php');
		
		$json = new Services_JSON();
		
		jimport('joomla.filesystem.file');
		
		$file	=	JRequest::getVar('filename1', null, 'files');
		$filename = JFile::makeSafe($file['name']);
         
		//Set up the source and destination of the file
		$src = $file['tmp_name'];
		
		if(!$file) 
			return false;
		
		if(!$filename)
			return false;
		
		$query =	"SELECT DISTINCT(postcode) FROM #__ihouse_property ";
				$db->setQuery( $query );
				$lists = $db->loadObjectList(); 
		
		$tmpArr = array();
		
		foreach($lists as $l) :
			$tmpArr[] = $l->postcode;
		endforeach;
				
		
		
		$header = array();
		
		$fp = fopen($src, 'r');
		
		$line = fgetcsv($fp, 4096); // database header;
		
		foreach($line as $t) :
			$header[] = $t;
		endforeach;
		
		$text = '';
		
		while (($line = fgetcsv($fp, 1000, ",") ) !== FALSE) {
			
			if($line) { 
				
				$empty	=	false;
				
				foreach($line as $t) :
					if(!empty($t)) {
						$empty = true;
						break;
					}
				endforeach;
				
				if($empty) {
					$data = array();
					for($i = 0 ; $i < count($header); $i++) {
						$dt		=	str_replace("'","\'",$line[$i]);
						$dt		=	str_replace('"','\"',$dt);
						
						$data[] = "'".$dt."'";
						
					}
					
					$postcode 		=	str_replace('?','',$line[3]);
					$postcode		=	str_replace(' ','',$line[3]);
					
					$name_en		=	str_replace("'","\'",$line[0]);
					$name_ch		=	str_replace("'","\'",$line[1]);
					$address		=	str_replace('"',' ',$line[2]);
					$address		=	str_replace("'"," ",$address);
					$postcode 		= 	$postcode;
					$prop_type		=	str_replace("'","\'",$line[4]);
					$tenure			=	str_replace("'","\'",$line[5]);
					$prop_class		=	str_replace("'","\'",$line[6]);
					$year_built		=	str_replace("'","\'",$line[7]);
					$room_size		=	str_replace("'","\'",$line[8]);
					$district_id	=	str_replace("'","\'",$line[9]);
					$units_condo	=	str_replace("'","\'",$line[10]);
					$description	=	str_replace("'","\'",$line[11]);
					$nearby_mrt		=	str_replace("'","\'",$line[12]);
					$nearby_school	=	str_replace("'","\'",$line[13]);
					$developer		=	str_replace("'","\'",$line[14]);
					$floorplan		=	str_replace("'","\'",$line[15]);
					
					if($address)
						$tmp_addr = str_replace(' ','+',$address);
					
					$url = 'http://maps.googleapis.com/maps/api/geocode/json?address=Singapore+'.$postcode.'+'.$tmp_addr.'&sensor=false';
					
					//echo $url;
					$ch = curl_init();
						
					curl_setopt( $ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1" );
					curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    				curl_setopt( $ch, CURLOPT_URL, $url );
            		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true); 
			
					$content = curl_exec( $ch );
					$data = $json->decode($content);
						
					$lat = $data->results[0]->geometry->location->lat;
					$lng = $data->results[0]->geometry->location->lng;
					
					if(in_array($postcode, $tmpArr)) { // postcode, CSV
						$text .= "<div>Postcode : <strong>".$postcode."</strong> has been updated </div>";
							
						$query = "UPDATE #__ihouse_property SET "
									.	" name_en = '$name_en' ,"
									.	" name_ch = '$name_ch' ,"
									.	" address = '$address' ,"
									.	" property_type = '$prop_type' ,"
									.	" tenure = '$tenure' ,"
									.	" property_class = '$prop_class' ,"
									.	" year_built = '$year_built' ,"
									.	" room_size = '$room_size' ,"
									.	" district_id = '$district_id' ,"
									.	" units_on_condo = '$units_condo' ,"
									.	" description = '$description' ,"
									.	" nearby_mrt = '$nearby_mrt' ,"
									.	" nearby_school = '$nearby_school' ,"
									.	" developer = '$developer' ,"
									.	" floor_plan = '$floorplan' "
									.	" WHERE postcode = '".$postcode."' ";
									
							$db->setQuery( $query );
							$db->query(); 
						

						$query = "SELECT id FROM #__ihouse_location_gps WHERE postcode = '$postcode' ";
							$db->setQuery( $query );
							$id = $db->loadResult();
						
						if($id) { // exist
							$query = "UPDATE #__ihouse_location_gps SET lat = '$lat', lng = '$lng' WHERE postcode = '$postcode' ";
							$db->setQuery( $query );
							$db->query();
						} else {
							$query = "INSERT INTO #__ihouse_location_gps VALUES(NULL, '$postcode', '$lat','$lng')";
							$db->setQuery( $query );
							$db->query();
						}

					} else { 
						$text .= "<div>Postcode : <strong>".$postcode."</strong> has been newly added </div>";
						
						//$data = ( count ( $data ) ? implode(',', $data) : '' );
					
						$query = "INSERT INTO #__ihouse_property VALUES(NULL,'$name_en','$name_ch','$address','$postcode','$prop_type','$tenure','$prop_class','$year_built','$room_size','$district_id','$units_condo','$description','$nearby_mrt','$nearby_school','$developer','$floorplan');";
						
							$db->setQuery( $query );
							$db->query(); 
						
						$last_id = $db->insertid();
						
						/* Type 1, Featured Ads for Expert of the Condo */
						$query = "INSERT INTO #__ihouse_ads_type_slot VALUES(NULL, 1, '$last_id','','1')";
							$db->setQuery( $query );
							$db->query(); 
							
						/* Type 2, Featured Ads for Recommended Specialist for every condo */
						$query = "INSERT INTO #__ihouse_ads_type_slot VALUES(NULL, 2, '$last_id','','3')";
							$db->setQuery( $query );
							$db->query(); 	
						
						
							
						/* Type 5, Featured Ads, SellAdsListing and RentAdsListing for this Condo only, Property Highlight Tab */
						$query = "INSERT INTO #__ihouse_ads_type_slot VALUES(NULL, 5, '$last_id','','8')";
							$db->setQuery( $query );
							$db->query(); 	
						
						
						/* Type 3, Featured Ads for Condo Expert, shows at CondoDirectory1 and CondoDirectory2 - Fixed DB*/
						/* Type 4, Fixed DB same as Type 6 */
						/* Type 6, Featured Ads, SellAdsListing and RentAdsListing - Fixed DB */
						/* Type 7, New Condos for Sale - Fixed DB */
						
						/* Google Map GPS Locator */
						$query = "INSERT INTO #__ihouse_location_gps VALUES(NULL, '$postcode', '$lat','$lng')";
							$db->setQuery( $query );
							$db->query();
					}
				}
			}
			
		}
		fclose($fp);

		return $text;
	}
	
}