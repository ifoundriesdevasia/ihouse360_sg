﻿<?php
/**
* @version		$Id: poll.php 10752 2008-08-23 01:53:31Z eddieajau $
* @package		Joomla
* @subpackage	Polls
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class IhouseModelMaps extends JModel
{
	function __construct()
	{
		global $mainframe;

		parent::__construct();

		/*$config = JFactory::getConfig();

		// Get the pagination request variables
		$this->setState('limit', $mainframe->getUserStateFromRequest('com_newsfeeds.limit', 'limit', $config->getValue('config.list_limit'), 'int'));
		$this->setState('limitstart', JRequest::getVar('limitstart', 0, '', 'int'));

		$id = JRequest::getVar('id', 0, '', 'int');
		$this->setId((int)$id);
		*/
	}
	
	function createKML() {
		$db = $this->getDBO();
		
		$query = "SELECT * FROM #__ihouse_location_gps ";
			
			$db->setQuery( $query );
			$lists = $db->loadObjectList();
		
		
		
		
		$kml = array('<?xml version="1.0" encoding="UTF-8"?>');
		$kml[] = '<kml xmlns="http://www.opengis.net/kml/2.2">';
		$kml[] = '<Document>';
		
		foreach($lists as $l) :

			$kml[] = '<Placemark>';
    		$kml[] = '<name>Google Inc.</name>';
   			$kml[] = '<description></description>';
			//<![CDATA['.$this->getGoogleAdsMap($l->postcode) . ']]>;
    		$kml[] = '<Point>';
      		$kml[] = '<coordinates>'.$l->lng.','.$l->lat.'</coordinates>';
    		$kml[] = '</Point>';
  			$kml[] = '</Placemark>';
		
		endforeach;
		$kml[] = '</Document>';
		$kml[] = '</kml>';
		
		
		$myFile = JPATH_ROOT.DS."images".DS."ihouse".DS."kml".DS."test.kml";
		echo $myFile;
		$fh = fopen($myFile, 'w');
		$kmlOutput = join("\n", $kml);
		
		fwrite($fh, $kmlOutput);
		
		fclose($fh);


		return true;
	}
	
	function getGoogleAdsMap($postcode) {
		$db = $this->getDBO();
		
		$query = "SELECT * FROM #__ihouse_ads "
						.	" WHERE postcode = '".$l->postcode."'";
			$db->setQuery( $query );
			$tmp = $db->loadObjectList();
		
		$html = '';
		
		if(!empty($tmp)) :
		$html .= '<div style="width: 400px;">
					<div style="float: left; width: 400px;">
						<div style="float: left;">
							<span id="static-text">Test 1</span>
						</div>
						<div style="clear: both;"></div>
						<div style="float: left; margin: 10px 0pt;">
							<div id="gmap_btn1"><div style="margin: 5px 10px;"><a href="#">楼盘详情</a></div></div>
							<div id="gmap_btn2"><div style="margin: 5px 10px;"><a href="#">列表方式查看房源</a></div></div>
						</div> 
					</div>
					<div style="clear: both;"></div>
					<div id="gmap_bg_filter"></div>
					<div style="clear: both;"></div>
					<div>
						<table width="100%" cellspacing="0" cellpadding="1">
						<tbody>';
		foreach($tmp as $t) :				
		$html .= 		'<tr style="vertical-align: middle;">
						<td>
							<div>
								<span id="static-text">'.$t->property_name.'</span><br>'.$t->ad_title.'
							</div>
						</td>
						<td>
							<div>'.$t->no_of_room.'  '.$t->toilet.' </div>
						</td>
						<td>
							<div>'.$t->size.' 平方米 </div>
						</td>
						<td>
							<div>S$ '.$t->ask_price.'</div>
						</td>
						<td>
							<div>'.$t->tenure.' 方 </div>
						</td>
						</tr>';
		endforeach;				
		$html .=		'</tbody>
						</table>
					</div>
					<div style="clear: both;"></div>
					<div id="gmap_bg_filter"></div>
				</div>';
		else :
		
		$html	.= 'No Ads';
		
		endif;
		
		return $html;
	}
	
	function update($id = 0) {
		
		if(!$id)
			return false;
		
		$db = $this->getDBO();

		$user		=& 	JFactory::getUser();
		$uid		= 	$user->id;
		
		$property_id		=	JRequest::getVar('property_id');
		$name_en			=	JRequest::getVar('name_en', '');
		$name_ch			=	JRequest::getVar('name_ch', '');
		$address		=	JRequest::getVar('address', '');
		$postalcode	=	JRequest::getVar('postcode', '');
		$property_type = JRequest::getVar('property_type', '');
		$tenure	=	JRequest::getVar('tenure', '');
		$property_class	=	JRequest::getVar('property_class', '');
		$year_built	=	JRequest::getVar('year_built', '');
		$room_size	=	JRequest::getVar('room_size', '');
		$district	=	JRequest::getVar('district_id', '');
		$condo_units	=	JRequest::getVar('units_on_condo', '');
		$description	=	JRequest::getVar('description', '');
		$nearby_mrt	=	JRequest::getVar('nearby_mrt', '');
		$nearby_school	=	JRequest::getVar('nearby_school', '');
		$developer	=	JRequest::getVar('developer', '');
		$floorplan	=	implode('|',JRequest::getVar('floor_plan', array()));

		$query = 	"UPDATE #__ihouse_property "
					. " SET name_en = '$name_en', name_ch = '$name_ch', address = '$address', postcode = '$postalcode', property_type = '$property_type' "
					. " , tenure = '$tenure', property_class = '$property_class', year_built = '$year_built',room_size = '$room_size' "
					. " , district_id = '$district', units_on_condo = '$condo_units', description = '$description' "
					. " , nearby_mrt = '$nearby_mrt', nearby_school = '$nearby_school', developer = '$developer' "
					. " , floor_plan = '$floorplan' "
					. " WHERE id = '$id' ; ";

			$db->setQuery( $query );
			$db->query();
		
		return true;
	}
	

	
}
