<?php


// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.controller');


class iHouseController extends JController
{
	/**
	 * constructor (registers additional tasks to methods)
	 * @return void
	 */
	function __construct()
	{
		parent::__construct();

		// Register Extra tasks
		//$this->registerTask( 'add'  , 'edit' );
		
		// Set the table directory
		//JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ihouse'.DS.'tables');

	}
	
	function display()
	{
		$viewName	= JRequest::getVar('view', 'ihouse', 'default', 'cmd');
		
		JRequest::setVar('view',$viewName);
		
		$task	= JRequest::getCmd('task');
		
		$document =& JFactory::getDocument();
		
		$viewType	= $document->getType();
		
		$layout = JRequest::getVar('layout', '');
		
		// View caching logic -- simple... are we logged in?
		
		$user = &JFactory::getUser();
		$viewcache = JRequest::getVar('viewcache',1,'POST','INT');
		
		
		if ($user->get('id')) {
			parent::display(false);
		} else {
			parent::display(true);
		}
		
		
	}
	
	
	//function to generate report
	function report()
	{
		global $mainframe;
		
		$viewName	= JRequest::getVar('view', 'report', 'default', 'cmd');
		
		JRequest::setVar('view',$viewName);
		
		// View caching logic -- simple... are we logged in?
		$user = &JFactory::getUser();
		
		parent::display();
			
	}
	
	/**
	 * Reset Statistics
	 */
	function reset()
	{
		$model	=& $this->getModel( 'Report' );
		$model->reset();
		$this->setRedirect('index.php?option=com_ihouse');
	}
	
	//function to generate receipt
	function generate()
	{
		JRequest::setVar( 'view', 'generate' );
		//JRequest::setVar( 'layout', 'default_pdf' );
		parent::display();
	}
	
	/**
	 * display the edit form
	 * @return void
	 */
	 
	function edit()
	{	
		// start to differentiate user and assign correct layout
		$user		= JFactory::getUser();
		$userid		= $user->get('gid');

		$viewName	= JRequest::getVar('view', 'ihouse', 'default', 'cmd');
		
		JRequest::setVar('view',$viewName);
		JRequest::setVar( 'layout','edit');
		JRequest::setVar('hidemainmenu', 1);

		if ($user->get('id')) {
			parent::display(false);
		} else {
			parent::display(true);
		}
	
		
	}
	
	
	function add() {
		
		$viewName	= JRequest::getVar('view', 'ihouse', 'default', 'cmd');
		JRequest::setVar('view',$viewName);
		JRequest::setVar( 'layout','add'  );
		// View caching logic -- simple... are we logged in?
		$user = &JFactory::getUser();
		
		if ($user->get('id')) {
			parent::display(false);
		} else {
			parent::display(true);
		}
	}
	
	/*
		Insert CSV File for Property
	*/
	
	function insert_csv()
	{

		
		
		$view		= JRequest::getCmd('view');
		
		JRequest::setVar('view','ihouse');
		JRequest::setVar( 'layout','add'  );
		
		//if ($model->propertyCSVFileInsert()) {
			//$msg = JText::_( 'Inserting Property Data ' );
		//} else {
			//$msg = JText::_( 'Error : Operation has been aborted ' );
		//}
		
		//$link = 'index.php?option=com_ihouse&view=ihouse&layout=add';
		
		//$this->setRedirect($link, $msg);
		$user = &JFactory::getUser();
		
		if ($user->get('id')) {
			parent::display(false);
		} else {
			parent::display(true);
		}
	}

	/*
		Create a KML file, for Google Map
	*/
	
	function create_kml() {
		$model 		= $this->getModel('maps');
		
		JRequest::setVar('view','map');
		JRequest::setVar( 'layout','default');
		
		if ($model->createKML($cid)) {
			$msg = JText::_( 'User Credit Updated !' );
		} else {
			$msg = JText::_( 'Error : Update User' );
		}
		
		$user = &JFactory::getUser();
		
		if ($user->get('id')) {
			parent::display(false);
		} else {
			parent::display(true);
		}
	}

	/**
	 * save a record (and redirect to main page)
	 * @return void
	 */
	 
	 
	function save()
	{

		$model 		= $this->getModel('ihouse');
		
		$view		= JRequest::getCmd('view');
		
		switch($view) {
			case 'promotion' :
				$update 	= JRequest::getCmd('update');
				
				$cid 	= JRequest::getCmd('cid');
					
				if ($model->update_promotion($cid)) {
						$msg = JText::_( 'User Credit Updated !' );
				} else {
					$msg = JText::_( 'Error : Update User' );
				}
				
				$link = 'index.php?option=com_ihouse&view=promotion';
				
				break;
			case 'credit' :
				$update 	= JRequest::getCmd('update');
			
				if($update) {
					$uid 	= JRequest::getCmd('user_id');
					
					if ($model->update_user_credit($uid)) {
						$msg = JText::_( 'User Credit Updated !' );
					} else {
						$msg = JText::_( 'Error : Update User' );
					}
					
				}
				else {
					
					if ($model->save_user_credit()) {
						$msg = JText::_( 'User Credit Saved!' );
					} else {
						$msg = JText::_( 'Users already have credits and cannot duplicate the credits' );
					}
					
				}
				$link = 'index.php?option=com_users&task=view';
				
				break;
			case 'ihouse' :
			default :
				$update 	= JRequest::getCmd('update');
				if ($update) { // Update Receipt 
		
					$id 	= JRequest::getCmd('property_id');
			
					if ($model->update($id)) {
						$msg = JText::_( 'Property Updated!' );
					} else {
						$msg = JText::_( 'Error Updating Property' );
					}
			
				} else { // Store a new receipt
				
					if ($model->save()) {
						$msg = JText::_( 'Property Saved!' );
					} else {
						$msg = JText::_( 'Error Saving Property' );
					}
					
				}
				// Check the table in so it can be edited.... we are done with it anyway
				$link = 'index.php?option=com_ihouse';
				break;
		}
		
		
		$this->setRedirect($link, $msg);
	}
	
	function apply() {
		
		$model = $this->getModel('ihouse');
		
		$view		= JRequest::getCmd('view');
		
		switch($view) {
			case 'promotion':
				$update 	= JRequest::getCmd('update');
				
				$cid 	= JRequest::getCmd('cid');
					
				if ($model->update_promotion($cid)) {
						$msg = JText::_( 'Promotion Updated !' );
				} else {
					$msg = JText::_( 'Error : Update Promotion' );
				}
				
				$link = 'index.php?option=com_ihouse&view=promotion&layout=edit&cid='.$cid;
				
				break;
			case 'credit' :
				$update 	= JRequest::getCmd('update');
			
				if($update) {
					$uid 	= JRequest::getCmd('user_id');
					
					if ($model->update_user_credit($uid)) {
						$msg = JText::_( 'User Credit Updated!' );
					} else {
						$msg = JText::_( 'Error Updating Users' );
					}
					
					$link = 'index.php?option=com_ihouse&view=credit&layout=edit&cid='.$uid;
				}
				else {
					
					if ($model->save_user_credit()) {
						$msg = JText::_( 'User Credit Saved!' );
					} else {
						$msg = JText::_( 'Users already have credits and cannot duplicate the credits' );
					}
					$link = 'index.php?option=com_ihouse&view=credit&layout=add';
				}
				
				break;
			case 'ihouse' :
			default :
				$update 	= JRequest::getCmd('update');
		
				if ($update) { // Update Receipt 
		
					$id 	= JRequest::getCmd('property_id');
			
					if ($model->update($id)) {
						$msg = JText::_( 'Property Updated!' );
					} else {
						$msg = JText::_( 'Error Updating Property' );
					}
					
					$link = 'index.php?option=com_ihouse&layout=edit&cid='.$id;
					
				} else { // Store a new property
					
					if ($model->save()) {
						$msg = JText::_( 'Property Saved!' );
					} else {
						$msg = JText::_( 'Error Saving Property' );
					}
					
					$link = 'index.php?option=com_ihouse&layout=add';
				}
				break;
		}

		
		$this->setRedirect($link, $msg);
	}

	/**
	 * remove record(s)
	 * @return void
	 */
	function remove()
	{
		$model 	= $this->getModel('ihouse');
		
		$view 	=	JRequest::getCmd('view');
		
		switch($view) {
			case 'credit' :
				if(!$model->delete_user_credit()) {
					$msg = JText::_( 'Error: Deleting Users Credit' );
				} else {
					$msg = JText::_( 'User(s) Credit Deleted' );
				}
				$link = 'index.php?option=com_ihouse&view=credit';
				break;
				
			case 'ihouse' :
			default:
				if(!$model->delete()) {
					$msg = JText::_( 'Error: One or More Property could not be Deleted' );
				} else {
					$msg = JText::_( 'Property(s) Deleted' );
				}
				$link = 'index.php?option=com_ihouse';
				break;
		}
		
		$this->setRedirect($link, $msg );
	}
	

	/**
	 * cancel editing a record
	 * @return void
	 */
	function cancel()
	{
		$msg = JText::_( 'Operation Cancelled' );
		
		$view = JRequest::getCmd('view');
		
		switch($view) {
			case 'promotion' :
				$link = 'index.php?option=com_ihouse&view=promotion';
				break;
			case 'credit' :
				$link = 'index.php?option=com_users&task=view';
				break;
			case 'ihouse' :
			default :
				$link = 'index.php?option=com_ihouse';
				break;
		}
		$this->setRedirect($link , $msg );
	}
	
	/* task = district , for sorting*/
	function district() {
		$viewName	= JRequest::getVar('view', 'district', 'default', 'cmd');
		
		JRequest::setVar('view',$viewName);
		
		// View caching logic -- simple... are we logged in?
		
		$user = &JFactory::getUser();
		$viewcache = JRequest::getVar('viewcache',1,'POST','INT');
		
		if ($user->get('id')) {
			parent::display(false);
		} else {
			parent::display(true);
		}
	}
	
	/* task = user_credit , for sorting*/
	function user_credit() {
		$viewName	= JRequest::getVar('view', 'credit', 'default', 'cmd');
		
		JRequest::setVar('view',$viewName);
		
		// View caching logic -- simple... are we logged in?
		
		$user = &JFactory::getUser();
		$viewcache = JRequest::getVar('viewcache',1,'POST','INT');
		
		if ($user->get('id')) {
			parent::display(false);
		} else {
			parent::display(true);
		}
	}
	
}