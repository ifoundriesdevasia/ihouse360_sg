<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php  JHTML::_('behavior.tooltip');  ?>

<?php
	JToolBarHelper::title( JText::_( 'E-voucher Management' ));
/*	JToolBarHelper::publishList('publish','Publish');
	JToolBarHelper::unpublishList('unpublish','Unpublish');*/
	JToolBarHelper::deleteListX('Delete', 'deletelist');
	JToolBarHelper::editListX('edit', 'Edit');
	JToolBarHelper::addNewX('add','New');
	/*$ordering = ($this->lists['order'] == 'a.ordering');*/

?>
<?php /*?><form name="formName" id="formName" method="post" action="export.php">
<input type="submit" name="button" id="button" value="Export to CSV"/><br /><br />
    </form><?php */?>
<form action="index.php?option=com_evoucher" method="post" name="adminForm">

<input type="submit" name="button" id="button" value="Export to CSV" onclick="javascript:if(document.adminForm.boxchecked.value==0){alert('Please make a selection from the list to export');}else{ hideMainMenu(); submitbutton('export')}"/><br /><br />
	<table>
		<tr>
			<td width="100%">
				<?php /*?><?php echo JText::_( 'Filter by Package title' ); ?>:
				<input type="text" name="search" id="search" value="<?php echo htmlspecialchars($this->lists['search']);?>" class="text_area" onchange="document.adminForm.submit();" />
				<button onclick="this.form.submit();"><?php echo JText::_( 'Go' ); ?></button>
				<button onclick="document.getElementById('search').value='';this.form.getElementById('filter_type').value='0';this.form.getElementById('filter_logged').value='0';this.form.submit();"><?php echo JText::_( 'Reset' ); ?></button><?php */?>
			</td>
			<td nowrap="nowrap">
				<?php /*?><?php echo $this->lists['type'];?><?php */?>
				<?php /*?><?php echo $this->lists['logged'];?><?php */?>
			</td>
		</tr>
	</table>

	<table class="adminlist" cellpadding="1">
		<thead>
			<tr>
				<th width="2%" class="title">
					<?php echo JText::_( 'NUM' ); ?>
				</th>
				<th width="3%" class="title">
					<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->rows); ?>);" />
				</th>
				<th width="15%" class="title" >
                	<?php echo 	JText::_( 'Prefix'); ?>
				</th>
				<th width="25%" class="title" nowrap="nowrap">
                	<?php echo 	JText::_( 'Number of Vouchers'); ?>
				</th>
                <th width="25%" class="title">
                	<?php echo 	JText::_( 'Voucher key'); ?>
				</th>
                <th width="5%" nowrap="nowrap" align="center">
                    <?php echo 	JText::_( 'Discount Rate (%)'); ?>
                </th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="7">
					<?php echo $this->pageNav->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
		<?php
			$k = 0;
			for ($i=0, $n=count( $this->rows ); $i < $n; $i++)
			{
				$row 	=& $this->rows[$i];
				$link 	= 'index.php?option=com_evoucher&amp;task=edit&amp;cid='. $row->id. '';
				
				/*$checked 	= JHTML::_('grid.checkedout',   $row, $i );
				$published 	= JHTML::_('grid.published', $row, $i );*/
			?>
			<tr class="<?php echo "row$k"; ?>">
				<td>
					<?php echo $i+1+$this->pageNav->limitstart;?>
				</td>
				<td align="center">
					<?php echo JHTML::_('grid.id', $i, $row->id ); ?>
				</td>
				<td align="center">
					<a href="<?php echo $link; ?>">
						<?php echo $row->prefix; ?></a>
				</td>
				<td align="center">
					
						<?php echo $row->num_of_voucher; ?>
				</td>
				
				<td align="center">
					<?php echo $row->voucher_key;  ?>
				</td>
            	<td align="center">
					<?php echo $row->discount; ?>
				</td>
			</tr>
			<?php
				$k = 1 - $k;
				}
			?>
		</tbody>
	</table>

	<input type="hidden" name="option" value="com_evoucher" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>