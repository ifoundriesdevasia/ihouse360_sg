<?php
/**
* @version		$Id: view.html.php 10381 2008-06-01 03:35:53Z pasamio $
* @package		Joomla
* @subpackage	E-voucher List
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Process component
 *
 * @static
 * @package		iHouse360
 * @subpackage	E-voucher list
 * @since 1.0
 */
class EvoucherViewEvoucherlist extends JView
{

		
	function display($tpl = null)
	{
		
		$model		= &$this->getModel();
		$rows 		= $model->getList();
		$pageNav 	= $model->_pageNav;
		
		/*$filter 	= $this->filterThis();
		$filterStatus = $this->filterStatus();*/
		
		$this->assignRef('lists',	$model->lists );
		$this->assignRef('model'		, 	$model);
		/*$this->assignRef('filter'		, 	$filter);
		$this->assignRef('filterStatus'	, 	$filterStatus);*/
		$this->assignRef('rows'			, 	$rows );
		$this->assignRef('task'			, 	$task );
		$this->assignRef('pageNav'		,	$pageNav);
			
		parent::display($tpl);
	}
	
	//function filterThis(){
//		$model		= &$this->getModel();
//		$sources	= $model->getSource();
//		return $sources;
//	} 
//	
//	function filterStatus(){
//		$model		= &$this->getModel();
//		$status	= $model->getStatusFilter();
//		return $status;
//	}
}