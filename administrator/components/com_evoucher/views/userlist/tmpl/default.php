<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php  JHTML::_('behavior.tooltip');  ?>

<?php
	JToolBarHelper::title( JText::_( 'E-voucher User Lists' ));
	JToolBarHelper::deleteListX('Delete', 'deleteuser');
	/*JToolBarHelper::editListX('edit', 'Edit');
	JToolBarHelper::addNewX('add','New');*/

?>

<form action="index.php?option=com_evoucher" method="post" name="adminForm">
	<table>
		<tr>
			<td width="100%">
				<?php /*?><?php echo JText::_( 'Filter by Package title' ); ?>:
				<input type="text" name="search" id="search" value="<?php echo htmlspecialchars($this->lists['search']);?>" class="text_area" onchange="document.adminForm.submit();" />
				<button onclick="this.form.submit();"><?php echo JText::_( 'Go' ); ?></button>
				<button onclick="document.getElementById('search').value='';this.form.getElementById('filter_type').value='0';this.form.getElementById('filter_logged').value='0';this.form.submit();"><?php echo JText::_( 'Reset' ); ?></button><?php */?>
			</td>
			<td nowrap="nowrap">
				<?php /*?><?php echo $this->lists['type'];?><?php */?>
				<?php /*?><?php echo $this->lists['logged'];?><?php */?>
			</td>
		</tr>
	</table>

	<table class="adminlist" cellpadding="1">
		<thead>
			<tr>
				<th width="2%" class="title">
					<?php echo JText::_( 'NUM' ); ?>
				</th>
				<th width="3%" class="title">
					<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->rows); ?>);" />
				</th>
				<th class="title" align="center">
					<?php echo JHTML::_('grid.sort',   'E-voucher Number', 'a.evoucher_num', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
				</th>
				<th width="15%" class="title" >
                	<?php echo 	JText::_( 'User ID'); ?>
				</th>
                <th width="10%" class="title">
                	<?php echo 	JText::_( 'Datetime'); ?>
				</th>  
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="5">
					<?php echo $this->pageNav->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
		<?php
			$k = 0;
			for ($i=0, $n=count( $this->rows ); $i < $n; $i++)
			{
				$row 	=& $this->rows[$i];
				$link 	= 'index.php?option=com_evoucher&amp;task=userlist&amp;cid='. $row->id. '';
				
			?>
			<tr class="<?php echo "row$k"; ?>">
				<td>
					<?php echo $i+1+$this->pageNav->limitstart;?>
				</td>
				<td align="center">
					<?php echo JHTML::_('grid.id', $i, $row->id ); ?>
				</td>
				<td align="center">
					<a href="<?php echo $link; ?>">
						<?php echo $row->evoucher_num; ?></a>
				</td>
				<td align="center">
					<?php echo $row->user_id; ?>
				</td>
				<td align="center">
					<?php echo $row->datetime; ?>
				</td>
			</tr>
			<?php
				$k = 1 - $k;
				}
			?>
		</tbody>
	</table>

	<input type="hidden" name="option" value="com_evoucher" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>