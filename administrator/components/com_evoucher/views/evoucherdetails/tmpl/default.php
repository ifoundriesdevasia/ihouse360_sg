<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php  JHTML::_('behavior.tooltip');  ?>

<?php
	jimport('joomla.html.editor');
	
	//$editor = &JFactory::getEditor();
	//JFilterOutput::objectHTMLSafe( $row, ENT_QUOTES, 'text' );
	$cid = $this->cid;
	$edit = JRequest::getVar('edit',true);
	$text = intval($edit) ? JText::_( 'Edit' ) : JText::_( 'New' );
	
	JToolBarHelper::title( JText::_( 'E-voucher Details' ) . ': <small><small>[ '. $text .' ]</small></small>' , 'user.png' );
	JToolBarHelper::apply('applyDetails');
	JToolBarHelper::save('saveDetails');
	JToolBarHelper::cancel();

?>
<script language="javascript" type="text/javascript">
		<!--
		function submitbutton(pressbutton) {
			var form = document.adminForm;
			if (pressbutton == 'cancel') {
				submitform( pressbutton );
				return;
			}

			// do field validation
			if ( form.description.value == "" ) {
				alert( "<?php echo JText::_( 'You must fill in all fields.', true ); ?>" );
			} else {
				submitform( pressbutton );
			}
		}
		//-->
		</script>

<form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
<div style="width:100%; float:left;">
	<div>
		<fieldset class="adminform">
		<legend><?php echo JText::_( 'E-voucher Details' ); ?></legend>
			<table class="admintable" cellspacing="1">
				<tr>
					<td width="300" class="key">
						<label for="name">
							<?php echo JText::_( 'Prefix' ); ?> (Eg. AG)
						</label>
					</td>
					<td>
                    	<?php 
						if(!empty($this->row->prefix)){
						echo $this->row->prefix;
						} else {
						?>
                        <input type="text" id="prefix" name="prefix" value="" maxlength="2" size="71" />
                        <?php } ?>
					</td>
				</tr>
				<tr>
					<td class="key">
						<label for="email">
							<?php echo JText::_( 'Number of Vouchers' ); ?>
						</label>
					</td>
					<td>
                    	<?php 
						if(!empty($this->row->num_of_voucher)){
						echo $this->row->num_of_voucher;
						} else {
						?>
                 		<input type="text" id="num_of_voucher" name="num_of_voucher" value="" maxlength="4" size="71" />
                 		<?php } ?>
					</td>
				</tr>
                <tr>
					<td class="key">
						<label for="email">
							<?php echo JText::_( 'Voucher Key' ); ?>
						</label>
					</td>
					<td>
                    	<?php 
						if(!empty($this->row->voucher_key)){
						echo $this->row->voucher_key;
						} else {
						?>
                 		<input type="text" id="voucher_key" name="voucher_key" value="" maxlength="4" size="71" />
                 		<?php } ?>
					</td>
				</tr>
                <tr>
					<td valign="top" class="key">
						<label for="gid">
							<?php echo JText::_( 'Discount' ); ?> (%)
						</label>
					</td>
					<td>
                    	<?php 
						if(!empty($this->row->discount)){
						echo $this->row->discount;
						} else {
						?>
                        <input type="text" name="discount" id="discount" value="" size="71" />
                        <?php } ?>
					</td>
				</tr>
                <tr>
					<td valign="top" class="key">
						<label for="gid">
							<?php echo JText::_( 'Description' ); ?>
						</label>
					</td>
					<td>
                    	<textarea cols="40" rows="5" name="description" id="description"><?php echo $this->row->description;?></textarea>
					</td>
				</tr>
			</table>
		</fieldset>
</div>

    </div>

	<div class="clr"></div>

	<input type="hidden" name="id" value="<?php echo $this->row->id ?>" />
	<input type="hidden" name="cid" value="<?php echo $this->row->id; ?>" />
	<input type="hidden" name="option" value="com_evoucher" />
	<input type="hidden" name="task" value="" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>
