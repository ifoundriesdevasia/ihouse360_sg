<?php
/**
* @version		$Id: view.html.php 10381 2008-06-01 03:35:53Z pasamio $
* @package		Joomla
* @subpackage	E-voucher Details
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Process component
 *
 * @static
 * @package		iHouse360
 * @subpackage	E-voucher Details
 * @since 1.0
 */
class EvoucherViewevoucherdetails extends JView
{
	function display($tpl = null)
	{
		$db 		=& JFactory::getDBO();
		$cid 	= JRequest::getVar( 'cid', array(), '', 'array' );
		$model		= &$this->getModel();
		$row = $model->getDetail($cid[0]);
		
		//added by cleo
		$titleOptions[] = JHTML::_('select.option', '', JText::_('Please Select'));
		$titleOptions[] = JHTML::_('select.option', 'AG', 'AG');


		$this->assignRef('row', 			$row );
		$this->assignRef('titleOptions', 			$titleOptions );

		parent::display($tpl);
	}
}