<?php
/**
* @version		1.5.0
* @package		iHouse360
* @subpackage	E-voucher
* @copyright	Copyright ?2005 - 2009 iFoundries (Asia Pacific) Pte Ltd. All Rights Reserved.
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

/*
 * Make sure the user is authorized to view this page
 */
/*$user = & JFactory::getUser();
if (!$user->authorize( 'com_evoucher', 'manage' )) {
	$mainframe->redirect( 'index.php', JText::_('ALERTNOTAUTH') );
}*/

$document = & JFactory::getDocument();
?>

<?php
// Require the base controller
require_once (JPATH_COMPONENT.DS.'controller.php');

// Create the controller
$controller	= new EvoucherController( );

// Perform the Request task
$controller->execute( JRequest::getCmd('task'));
$controller->redirect();