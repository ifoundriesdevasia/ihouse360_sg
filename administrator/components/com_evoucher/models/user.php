<?php

// no direct access
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.model');

/**
 * Content Component E-voucher Model
 *
 * @package		Joomla
 * @subpackage	User List Model
 * @since		1.5
 */
class EvoucherModelUser extends JModel
{

	function getList()
	{
		global $mainframe;
		
		$context	= 'com_evoucher.user.userlist.';
		//$filter_order		= $mainframe->getUserStateFromRequest( "com_travelpackage.filter_order",		'filter_order',		'a.name',	'cmd' );
		//$filter_order_Dir	= $mainframe->getUserStateFromRequest( "com_travelpackage.filter_order_Dir",	'filter_order_Dir',	'',			'word' );
		$limit		= $mainframe->getUserStateFromRequest( 'global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int' );
		$limitstart = $mainframe->getUserStateFromRequest( $context.'limitstart', 'limitstart', 0, 'int' );
		
		$filter	= null;			
		
		$db		=& $this->getDBO();
		$this->_list = $this->_getList($db);
		
		
		
		return $this->_list;
		
	}
	
	function _getList($db){
	
	global $mainframe;
		
	$context	= 'com_evoucher.user.userlist.';
	//$filter_order		= $mainframe->getUserStateFromRequest( "com_travelpackage.filter_order",		'filter_order',		'a.name',	'cmd' );
	//$filter_order_Dir	= $mainframe->getUserStateFromRequest( "com_travelpackage.filter_order_Dir",	'filter_order_Dir',	'',			'word' );
	$limit		= $mainframe->getUserStateFromRequest( 'global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int' );
	$limitstart = $mainframe->getUserStateFromRequest( $context.'limitstart', 'limitstart', 0, 'int' );
	$search = $mainframe->getUserStateFromRequest($context.'search', 'search', '', 'string');
	//$search = JString::strtolower($search);
	/*if (strpos($search, '"') !== false) {
			$search = str_replace(array('=', '<'), '', $search);
		}
		$search = JString::strtolower($search);*/
		
	// Keyword filter
		/*if ($search) {
			$where = ' AND (LOWER( a.name ) LIKE '.$db->Quote( '%'.$db->getEscaped( $search, true ).'%', false ) .')';
				//.' OR a.id = ' . (int) $search . ')';
		}*/
	
	/*if ( $filter_order_Dir == 'ASC' ) {
			$this->lists['order_Dir'] = 'ASC';
	}	

	$this->lists['order'] = $filter_order;
		
	$orderby = ' ORDER BY '. $filter_order .' '. $filter_order_Dir;*/

	$query = "SELECT COUNT(*) FROM #__ihouse_evoucher_user";
	//$query .= $where;

	$db->setQuery($query);
	$total = $db->loadResult();

	// Create the pagination object
	jimport('joomla.html.pagination');
	$this->_pageNav = new JPagination($total, $limitstart, $limit);
	
	
	$sql = 'SELECT a.*'
			. ' FROM #__ihouse_evoucher_user AS a'
			//. ' INNER JOIN #__pkt_category AS c ON c.id = a.cat_id'
			//. $where
			. ' ORDER BY a.id'
			//. $orderby
		;

	$db->setQuery($sql,$this->_pageNav->limitstart, $this->_pageNav->limit );
	$objectList = $db->loadObjectList();
	
	$return = $objectList;
	
	return $return;
	}

	function removelist($id)
	{
	
		$conf 		=& JFactory::getConfig();
		$db			=& $this->getDBO();

		$query = "DELETE FROM #__ihouse_evoucher_user WHERE id  = '".$id."'";
		$db->setQuery($query);
		$db->query();

	}
	
}

?>