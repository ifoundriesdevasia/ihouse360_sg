<?php

// no direct access
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.model');

/**
 * Content Component E-voucher Model
 *
 * @package		Joomla
 * @subpackage	E-voucher
 * @since		1.5
 */
class EvoucherModelEvoucher extends JModel
{

	function getList()
	{
		global $mainframe;
		
		$context	= 'com_evoucher.evoucher.evoucherlist.';
		$filter_order		= $mainframe->getUserStateFromRequest( "com_evoucher.filter_order",		'filter_order',		'a.utype',	'cmd' );
		$filter_order_Dir	= $mainframe->getUserStateFromRequest( "com_evoucher.filter_order_Dir",	'filter_order_Dir',	'',			'word' );
		$limit		= $mainframe->getUserStateFromRequest( 'global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int' );
		$limitstart = $mainframe->getUserStateFromRequest( $context.'limitstart', 'limitstart', 0, 'int' );
		
		$filter	= null;			
		
		$db		=& $this->getDBO();
		$this->_list = $this->_getList($db);
		
		
		
		return $this->_list;
		
	}
	
	function _getList($db){
	
	global $mainframe;
		
	$context	= 'com_evoucher.evoucher.evoucherlist.';
	$filter_order		= $mainframe->getUserStateFromRequest( "com_evoucher.filter_order",		'filter_order',		'a.prefix',	'cmd' );
	$filter_order_Dir	= $mainframe->getUserStateFromRequest( "com_evoucher.filter_order_Dir",	'filter_order_Dir',	'',			'word' );
	$limit		= $mainframe->getUserStateFromRequest( 'global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int' );
	$limitstart = $mainframe->getUserStateFromRequest( $context.'limitstart', 'limitstart', 0, 'int' );
	$search = $mainframe->getUserStateFromRequest($context.'search', 'search', '', 'string');
	$search = JString::strtolower($search);
	if (strpos($search, '"') !== false) {
			$search = str_replace(array('=', '<'), '', $search);
		}
		$search = JString::strtolower($search);
		
	// Keyword filter
		if ($search) {
			$where = ' AND (LOWER( a.prefix ) LIKE '.$db->Quote( '%'.$db->getEscaped( $search, true ).'%', false ) .')';
				//.' OR a.id = ' . (int) $search . ')';
		}
	
	if ( $filter_order_Dir == 'ASC' ) {
			$this->lists['order_Dir'] = 'ASC';
	}	

	$this->lists['order'] = $filter_order;
		
	$orderby = ' ORDER BY '. $filter_order .' '. $filter_order_Dir;

	$query = "SELECT COUNT(*) FROM #__ihouse_evoucher_list";
	$query .= $where;

	$db->setQuery($query);
	$total = $db->loadResult();

	// Create the pagination object
	jimport('joomla.html.pagination');
	$this->_pageNav = new JPagination($total, $limitstart, $limit);
	
	
	$sql = 'SELECT a.*'
			. ' FROM #__ihouse_evoucher_list AS a'
			//. ' INNER JOIN #__pkt_category AS c ON c.id = a.cat_id'
			. $where
			. ' GROUP BY a.id'
			. $orderby
		;
		//echo $sql;
	$db->setQuery($sql,$this->_pageNav->limitstart, $this->_pageNav->limit );
	$objectList = $db->loadObjectList();
	//print_r($objectList);
	$return = $objectList;
	
	return $return;
	}
	
	function getDetail($id)
	{
		global $mainframe;
		
		$conf 		=& JFactory::getConfig();
		//$database	= $conf->getValue('config.db');
		$where[] = " a.id='".$id."'";
						
		$db		=& $this->getDBO();
		$this->_object = $this->_getDetail($db,$where);
	
		
		
		return $this->_object;
		
	}
	
	function _getDetail($db, $whereArray = array()){
		
		$query = 'SELECT a.*'
			. ' FROM #__ihouse_evoucher_list AS a'
			//. ' INNER JOIN #__pkt_category AS c ON c.id = a.cat_id'
			;
		
		
		$where =  implode('AND',$whereArray);
		$query .= " WHERE ".$where;
		$db->setQuery($query);

		$return = $db->loadObject();
		return $return;
	
	}
	
	function saveDetails($id){
		require_once( JPATH_ROOT.DS.'includes'.DS.'tools.php' );
		$db		=& $this->getDBO();
		$user 	=& JFactory::getUser();
		$user_id = $user->get('id');


		$rowid = JRequest::getVar('id', '', 'post', 'id');
		$prefixes = JRequest::getVar('prefix', '', 'post', 'prefix');
		$prefix = strtoupper($prefixes);
		$num_of_voucher = JRequest::getVar('num_of_voucher', '', 'post', 'num_of_voucher');
		$voucher_key = JRequest::getVar('voucher_key', '', 'post', 'voucher_key');
		$discount = JRequest::getVar('discount', '', 'post', 'discount');
		$description = JRequest::getVar('description', '', 'post', 'description');

		if (empty($rowid)){
		
			$query="SELECT count(*) FROM #__ihouse_evoucher_list WHERE prefix = '".$prefix."'";
			$db->setQuery($query);
			$exist_prefix = $db->loadResult();
			
			if($exist_prefix == '0'){
			$query = "INSERT INTO #__ihouse_evoucher_list (id, prefix, num_of_voucher, voucher_key, discount, description)"
		
			. "\n VALUES ( NULL, '".$prefix."','".$num_of_voucher."','".$voucher_key."','".$discount."','".$description."')";
			$db->setQuery( $query );
			$db->query();
			
			for($i = 1; $i < ($num_of_voucher + 1); $i++){
				$evoucher_num[$i] = md5($prefix.($i * $voucher_key));
				
				$query1 = "INSERT INTO #__ihouse_evoucher_user (id, evoucher_num, discount, user_used, user_id, evoucher_amt, payment_logid, estatus, datetime)"
		
			. "\n VALUES ( NULL, '".$prefix.str_pad($i, 4, "0", STR_PAD_LEFT).substr($evoucher_num[$i],0,16)."','".$discount."','no','0','0', '0', '0', CURRENT_TIMESTAMP)";
			$db->setQuery( $query1 );
			$db->query();
					
			}
			$return = true;
			
			} else {
			
			$return = false;
			
			}
			
			
			
		} else {

		
		$hquery = "UPDATE #__ihouse_evoucher_list SET description = '" .$description. "' WHERE id = '".$rowid."'";
			$db->setQuery($hquery);
			$db->query();

		
			$return = true;
		}
		return $return;
	
	}
	
	function removepkt($id)
	{
	
		$conf 		=& JFactory::getConfig();
		$db			=& $this->getDBO();
		
		$query = "SELECT * FROM #__ihouse_evoucher_list WHERE id = '".$id."'";
		$db->setQuery($query);
		$row = $db->loadObject();
		
		for($i = 1; $i < ($row->num_of_voucher + 1); $i++){
				$evoucher_num[$i] = md5($row->prefix.($i * $row->voucher_key));
				
				$query = "DELETE FROM #__ihouse_evoucher_user WHERE evoucher_num  = '".$row->prefix.str_pad($i, 4, "0", STR_PAD_LEFT).substr($evoucher_num[$i],0,16)."'";
				$db->setQuery($query);
				$db->query();
					
		}
			

		$query = "DELETE FROM #__ihouse_evoucher_list WHERE id  = '".$id."'";
		$db->setQuery($query);
		$db->query();

	}
	
}

?>