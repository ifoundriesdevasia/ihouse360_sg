<?php
/**
 * @version		$Id: controller.php 11299 2008-11-22 01:40:44Z ian $
 * @package		Joomla
 * @subpackage	E-voucher
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.controller');

/**
 * Users Component Controller
 *
 * @package		Joomla
 * @subpackage	E-voucher
 * @since 1.5
 */
class EvoucherController extends JController
{
	/**
	 * Constructor
	 *
	 * @params	array	Controller configuration array
	 */
	function __construct($config = array())
	{
		parent::__construct($config);
		$this->registerTask( 'add'  , 	'add'  );
		$this->registerTask( 'edit'  , 	'edit'  );
		$this->registerTask( 'save'  , 	'save'  );
		$this->registerTask( 'editview'  , 	'editview'  );
		$this->registerTask( 'editbooking'  , 	'editbooking'  );


	}
	
	function display(){
		$model	= &$this->getModel( 'evoucher' );
		$view	= &$this->getView( 'evoucherlist');
		$view->setModel( $model, true );
		$view->display();
	}
	
	function add() {
		JRequest::setVar( 'edit', false );
		$model	= &$this->getModel( 'evoucher' );
		$view	= &$this->getView( 'evoucherdetails');
		$view->setModel( $model, true );
		$view->display();
	}

	function edit(){
		JRequest::setVar( 'edit', true );
		$model	= &$this->getModel( 'evoucher' );
		$view	= &$this->getView( 'evoucherdetails');
		$view->setModel( $model, true );
		$view->display();
	}
	
	function deletelist(){
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$model	= &$this->getModel( 'evoucher' );
		$cid 	= JRequest::getVar( 'cid', array(), '', 'array' );
		foreach($cid as $id){
		$model->removepkt($id);
		}
		$this->setRedirect('index.php?option=com_evoucher');

	}
	
	function cancel(){
		$model	= &$this->getModel( 'evoucher' );
		$uid = JRequest::getCmd( 'id' );
		$user = $model->getDetail($uid);
		
		$msg = JText::_('Action Cancelled');
		$this->setredirect( 'index.php?option=com_evoucher',$msg);
	}
	
	function export(){
		$model	= &$this->getModel( 'export' );
		$cid 	= JRequest::getVar( 'cid', array(), '', 'array' );
		//foreach($cid as $id){
		$model->exporting($cid);
		//}

		//$this->setRedirect('index.php?option=com_evoucher');
	}


	function applyDetails()
	{
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$model 		= $this->getModel('evoucher');
		$id = JRequest::getCmd( 'id' );
		

		$response = $model->saveDetails($id);
		
		
		if($response){
		$msg = JText::_('You have successfully updated the e-voucher details.');
		}else{
		$msg = JText::_('This prefix has been used. Please try other prefix');
		}
		if(empty($id)){
		$this->setredirect( 'index.php?option=com_evoucher&task=display',$msg);
		} else {
		$this->setredirect( 'index.php?option=com_evoucher&task=edit&cid='.$id ,$msg);
		}
	}
	
	function saveDetails()
	{
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$model 		= &$this->getModel('evoucher');
		$id = JRequest::getCmd( 'id' );
		$response = $model->saveDetails($id);

		if($response){
		$msg = JText::_('You have successfully updated the evoucher details.');
		}else{
		$msg = JText::_('This prefix has been used. Please try other prefix');
		}


		$this->setredirect( 'index.php?option=com_evoucher',$msg);
	}
	
	function userlist()
	{
		$model	= &$this->getModel( 'user' );
		$view	= &$this->getView( 'userlist');
		$view->setModel( $model, true );
		$view->display();
	
	}
	
	function deleteuser(){
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$model	= &$this->getModel( 'user' );
		$cid 	= JRequest::getVar( 'cid', array(), '', 'array' );
		foreach($cid as $id){
		$model->removelist($id);
		}
		$this->setRedirect('index.php?option=com_evoucher&task=userlist');
	}
	
	/*
		
	
	
	function category()
	{
		$model	= &$this->getModel( 'category' );
		$view	= &$this->getView( 'categorylist');
		$view->setModel( $model, true );
		$view->display();
	
	}
	
	function catadd() {
		JRequest::setVar( 'edit', false );
		$model	= &$this->getModel( 'category' );
		$view	= &$this->getView( 'categorydetails');
		$view->setModel( $model, true );
		$view->display();
	}

	function catedit(){
		JRequest::setVar( 'edit', true );
		$model	= &$this->getModel( 'category' );
		$view	= &$this->getView( 'categorydetails');
		$view->setModel( $model, true );
		$view->display();
	}
	
	function deletecat(){
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$model	= &$this->getModel( 'category' );
		$cid 	= JRequest::getVar( 'cid', array(), '', 'array' );
		foreach($cid as $id){
		$model->removelist($id);
		}
		$this->setRedirect('index.php?option=com_travelpackage&task=category');
	}
	
	function cancelCatdetails(){
		$model	= &$this->getModel( 'category' );
		$uid = JRequest::getCmd( 'id' );
		$user = $model->getDetail($uid);
		
		$msg = JText::_('Action Cancelled');
		$this->setredirect( 'index.php?option=com_travelpackage&task=category',$msg);
	}

	function applyCatdetails()
	{
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$model 		= $this->getModel('category');
		$id = JRequest::getCmd( 'id' );

		$response = $model->saveDetails($id);
		
		if($response){
		$msg = JText::_('You have successfully updated the category details.');
		}else{
		$msg = JText::_('Error occured while updating the category details. Please contact administrator for help.');
		}
		if(empty($id)){
		$this->setredirect( 'index.php?option=com_travelpackage&task=category',$msg);
		} else {
		$this->setredirect( 'index.php?option=com_travelpackage&task=catedit&cid='.$id ,$msg);
		}
	}
	
	function saveCatdetails()
	{
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$model 		= &$this->getModel('category');
		$id = JRequest::getCmd( 'id' );
		$response = $model->saveDetails($id);

		if($response){
		$msg = JText::_('You have successfully updated the category details.');
		}else{
		$msg = JText::_('Error occured while updating the category details. Please contact administrator for help.');
		}


		$this->setredirect( 'index.php?option=com_travelpackage&task=category',$msg);
	}
	
	function customize()
	{
		$model	= &$this->getModel( 'customize' );
		$view	= &$this->getView( 'customizelist');
		$view->setModel( $model, true );
		$view->display();
	
	}
	
	function editview(){
		JRequest::setVar( 'edit', true );
		$model	= &$this->getModel( 'customize' );
		$view	= &$this->getView( 'customizedetails');
		$view->setModel( $model, true );
		$view->display();
	}
	
	function applycustomize()
	{
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$model 		= $this->getModel('customize');
		$id = JRequest::getCmd( 'id' );

		$response = $model->saveCustomize($id);
		
		if($response){
		$msg = JText::_('You have successfully updated the remarks.');
		}else{
		$msg = JText::_('Error occured while updating the remarks. Please contact administrator for help.');
		}
		if(empty($id)){
		$this->setredirect( 'index.php?option=com_travelpackage&task=customize',$msg);
		} else {
		$this->setredirect( 'index.php?option=com_travelpackage&task=editview&cid='.$id ,$response);
		}
	}
	
	function savecustomize()
	{
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$model 		= &$this->getModel('customize');
		$id = JRequest::getCmd( 'id' );
		$response = $model->saveCustomize($id);

		if($response){
		$msg = JText::_('You have successfully updated the remarks.');
		}else{
		$msg = JText::_('Error occured while updating the remarks. Please contact administrator for help.');
		}


		$this->setredirect( 'index.php?option=com_travelpackage&task=customize',$msg);
	}
	
	function cancelcustomize(){
		$model	= &$this->getModel( 'customize' );
		$uid = JRequest::getCmd( 'id' );
		$user = $model->getDetail($uid);
		
		$msg = JText::_('Action Cancelled');
		$this->setredirect( 'index.php?option=com_travelpackage&task=customize',$msg);
	}
	
	function deletecust(){
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$model	= &$this->getModel( 'customize' );
		$cid 	= JRequest::getVar( 'cid', array(), '', 'array' );
		foreach($cid as $id){
		$model->removecust($id);
		}
		$this->setRedirect('index.php?option=com_travelpackage&task=customize');
	}
	
	function payment_content(){
		JRequest::setVar( 'edit', true );
		$model	= &$this->getModel( 'payment' );
		$view	= &$this->getView( 'payment_content');
		$view->setModel( $model, true );
		$view->display();
	}
	
	function savecontent(){
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$model 		= &$this->getModel('payment');
		$id = JRequest::getCmd( 'id' );
		$response = $model->saveDetails($id);

		if($response){
		$msg = JText::_('You have successfully updated the Offline Payment Content.');
		}else{
		$msg = JText::_('Error occured while updating the Offline Payment Content. Please contact administrator for help.');
		}


		$this->setredirect( 'index.php?option=com_travelpackage&task=payment_content',$msg);
	
	}
	
	function video_content(){
		$model	= &$this->getModel( 'video' );
		$view	= &$this->getView( 'video_content');
		$view->setModel( $model, true );
		$view->display();
	}
	
	function addvideo() {
		JRequest::setVar( 'edit', false );
		$model	= &$this->getModel( 'video' );
		$view	= &$this->getView( 'videodetails');
		$view->setModel( $model, true );
		$view->display();
	}
	
	function editvideo(){
		JRequest::setVar( 'edit', true );
		$model	= &$this->getModel( 'video' );
		$view	= &$this->getView( 'videodetails');
		$view->setModel( $model, true );
		$view->display();
	}
	
	function applyvideo()
	{
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$model 		= $this->getModel('video');
		$id = JRequest::getCmd( 'id' );

		$response = $model->savevideo($id);
		
		if($response){
		$msg = JText::_('You have successfully updated the videos.');
		}else{
		$msg = JText::_('Error occured while updating the videos. Please contact administrator for help.');
		}
		if(empty($id)){
		$this->setredirect( 'index.php?option=com_travelpackage&task=video_content',$msg);
		} else {
		$this->setredirect( 'index.php?option=com_travelpackage&task=editvideo&cid='.$id ,$msg);
		}
	}
	
	function savevideo()
	{
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$model 		= &$this->getModel('video');
		$id = JRequest::getCmd( 'id' );
		$response = $model->savevideo($id);

		if($response){
		$msg = JText::_('You have successfully updated the videos.');
		}else{
		$msg = JText::_('Error occured while updating the videos. Please contact administrator for help.');
		}


		$this->setredirect( 'index.php?option=com_travelpackage&task=video_content',$msg);
	}
	
	function cancelvideo(){
		$model	= &$this->getModel( 'video' );
		$uid = JRequest::getCmd( 'id' );
		$user = $model->getDetail($uid);
		
		$msg = JText::_('Action Cancelled');
		$this->setredirect( 'index.php?option=com_travelpackage&task=video_content',$msg);
	}
	
	function deletevideo(){
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$model	= &$this->getModel( 'video' );
		$cid 	= JRequest::getVar( 'cid', array(), '', 'array' );
		foreach($cid as $id){
		$model->removevid($id);
		}
		$this->setRedirect('index.php?option=com_travelpackage&task=video_content');
	}
	
	function booking()
	{
		$model	= &$this->getModel( 'booking' );
		$view	= &$this->getView( 'bookinglist');
		$view->setModel( $model, true );
		$view->display();
	
	}
	function editbooking(){
		JRequest::setVar( 'edit', true );
		$model	= &$this->getModel( 'booking' );
		$view	= &$this->getView( 'bookingdetails');
		$view->setModel( $model, true );
		$view->display();
	}
	
	function applybooking()
	{
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$model 		= $this->getModel('booking');
		$id = JRequest::getCmd( 'id' );
		

		$response = $model->savebooking($id);
		
		if($response){
		$msg = JText::_('You have successfully updated the payment status.');
		}else{
		$msg = JText::_('Error occured while updating the payment status. Please contact administrator for help.');
		}
		if(empty($id)){
		$this->setredirect( 'index.php?option=com_travelpackage&task=booking',$msg);
		} else {
		$this->setredirect( 'index.php?option=com_travelpackage&task=editbooking&cid='.$id ,$msg);
		}
	}
	
	function savebooking()
	{
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$model 		= &$this->getModel('booking');
		$id = JRequest::getCmd( 'id' );
		$response = $model->savebooking($id);

		if($response){
		$msg = JText::_('You have successfully updated the payment status.');
		}else{
		$msg = JText::_('Error occured while updating the payment status. Please contact administrator for help.');
		}


		$this->setredirect( 'index.php?option=com_travelpackage&task=booking',$msg);
	}
	
	function cancelbooking(){
		$model	= &$this->getModel( 'booking' );
		$uid = JRequest::getCmd( 'id' );
		$user = $model->getDetail($uid);
		
		$msg = JText::_('Action Cancelled');
		$this->setredirect( 'index.php?option=com_travelpackage&task=booking',$msg);
	}
	
	function deletebook(){
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$model	= &$this->getModel( 'booking' );
		$cid 	= JRequest::getVar( 'cid', array(), '', 'array' );
		foreach($cid as $id){
		$model->removebook($id);
		}
		$this->setRedirect('index.php?option=com_travelpackage&task=booking');
	}*/
	
}
