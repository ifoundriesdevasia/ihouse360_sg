<?php
/**
 * @version		$Id: controller.php 11299 2008-11-22 01:40:44Z ian $
 * @package		Joomla
 * @subpackage	Transaction
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.controller');

/**
 * Users Component Controller
 *
 * @package		Joomla
 * @subpackage	Transaction
 * @since 1.5
 */
class TransactionController extends JController
{
	/**
	 * Constructor
	 *
	 * @params	array	Controller configuration array
	 */
	function __construct($config = array())
	{
		parent::__construct($config);
		$this->registerTask( 'monthly'  , 	'monthlydetails'  );
		$this->registerTask( 'total'  , 	'totaldetails'  );

	}
	
	function display(){
		JRequest::setVar( 'edit', true );
		$model	= &$this->getModel( 'transaction' );
		$view	= &$this->getView( 'dailydetails');
		$view->setModel( $model, true );
		$view->display();
	}
	
	function monthly(){
		JRequest::setVar( 'edit', true );
		$model	= &$this->getModel( 'transaction' );
		$view	= &$this->getView( 'monthlydetails');
		$view->setModel( $model, true );
		$view->display();
	}
	
	function total(){
		JRequest::setVar( 'edit', true );
		$model	= &$this->getModel( 'transaction' );
		$view	= &$this->getView( 'totaldetails');
		$view->setModel( $model, true );
		$view->display();
	}
	
	function getAmount()
	{
		JRequest::checkToken() or jexit( 'Invalid Token' );
		
		$this->setredirect( 'index.php?option=com_transaction&view=daily');
		//$model 		= $this->getModel('transaction');
		//$id = JRequest::getCmd( 'id' );
		

		//$response = $model->saveDetails($id);
		
		/*if($response){
		$msg = JText::_('You have successfully updated the package details.');
		}else{
		$msg = JText::_('Error occured while updating the package details. Please contact administrator for help.');
		}
		if(empty($id)){
		$this->setredirect( 'index.php?option=com_travelpackage&task=packagelist',$msg);
		} else {
		$this->setredirect( 'index.php?option=com_travelpackage&task=edit&cid='.$id ,$msg);
		}*/
		
	}
	
}
