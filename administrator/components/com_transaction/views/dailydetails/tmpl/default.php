<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php  JHTML::_('behavior.tooltip');  ?>
<style>
#dailydate{ width:178px;}
</style>
<script language="javascript" type="text/javascript">
		<!--
		function submitbutton(pressbutton) {
			var form = document.adminForm;

			// do field validation
			if ( form.dailydate.value == "" ) {
				alert( "<?php echo JText::_( 'You must select the date.', true ); ?>" );
			} else {
				submitform( pressbutton );
			}
		}
		//-->
</script>
<?php 
$db = & JFactory::getDBO();
$dailydate = JRequest::getVar('dailydate', '', 'post', 'dailydate');
$query = "SELECT SUM(debit) FROM #__users_transaction_log WHERE DATE(datetime) = '".$dailydate."'";
	
		$db->setQuery($query);
		$debit = $db->loadResult();
		
$query = "SELECT SUM(credit) FROM #__users_transaction_log WHERE DATE(datetime) = '".$dailydate."'";
	
		$db->setQuery($query);
		$credit = $db->loadResult();

?>
<?php

	$cid = $this->cid;
	$edit = JRequest::getVar('edit',true);
	$text = intval($edit) ? JText::_( 'Edit' ) : JText::_( 'New' );
	
	JToolBarHelper::title( JText::_( 'Daily Transaction' ) . ': <small><small>[ '. $text .' ]</small></small>' , 'user.png' );
	JToolBarHelper::apply('daily','Get transaction');

?>

<form action="index.php" method="post" name="adminForm" autocomplete="off">

	<div class="col width-45">
		<fieldset class="adminform">
		<legend><?php echo JText::_( 'Daily' ); ?></legend>
			<table class="admintable" cellspacing="1">
				<tr>
					<td width="150" class="key">
						<label for="daily">
							<?php echo JText::_( 'Date' ); ?>
						</label>
					</td>
					<td>
                    	<?php echo JHTML::_( 'calendar',$this->row->daily,'dailydate','dailydate', '%Y-%m-%d'); ?><?php /*?><br /><br /><?php */?>
                       <?php /*?> <input type="submit" name="submit" value="Submit" onclick="javascript:gettrans();" /><?php */?>
					</td>
				</tr>
			</table>
		</fieldset>
        <?php
		if(!empty($dailydate)){
		?>
        <fieldset class="adminform">
		<legend><?php echo JText::_( 'Amount' ); ?></legend>
			<table class="admintable" cellspacing="1">
				<tr>
					<td width="150" class="key">
						<label for="name">
							<?php echo JText::_( 'Debit' ); ?>
						</label>
					</td>
					<td>
                    	<?php if(!empty($debit)){ echo "$".$debit; } else { echo "$0";}?>
					</td>
				</tr>
                <tr>
					<td width="150" class="key">
						<label for="name">
							<?php echo JText::_( 'Credit' ); ?>
						</label>
					</td>
					<td>
                    	<?php if(!empty($credit)){ echo "$".$credit; } else { echo "$0";}?>
					</td>
				</tr>
			</table>
		</fieldset>
        <?php
		}
		?>
	</div>
	<div class="clr"></div>

	<input type="hidden" name="id" value="<?php echo $this->row->id ?>" />
	<input type="hidden" name="cid" value="<?php echo $this->row->id ?>" />
	<input type="hidden" name="option" value="com_transaction" />
	<input type="hidden" name="task" value="" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>