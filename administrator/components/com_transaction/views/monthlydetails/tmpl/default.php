<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php  JHTML::_('behavior.tooltip');  ?>
<?php require_once(JPATH_ROOT.DS.'includes'.DS.'tools.php');?>
<style>
#dailydate{ width:178px;}
</style>
<script language="javascript" type="text/javascript">
		<!--
		function submitbutton(pressbutton) {
			var form = document.adminForm;

			// do field validation
			if (( form.emonth.value == "" )||( form.eyear.value == "" )) {
				alert( "<?php echo JText::_( 'You must fill in all fields.', true ); ?>" );
			} else {
				submitform( pressbutton );
			}
		}
		//-->
</script>
<?php 
$db = & JFactory::getDBO();
$emonth = JRequest::getVar('emonth', '', 'post', 'emonth');
$eyear = JRequest::getVar('eyear', '', 'post', 'eyear');

$query = "SELECT SUM(debit) FROM #__users_transaction_log WHERE MONTH(datetime) = '".$emonth."' AND YEAR(datetime) = '".$eyear."'";
	
		$db->setQuery($query);
		$debit = $db->loadResult();
		
$query = "SELECT SUM(credit) FROM #__users_transaction_log WHERE MONTH(datetime) = '".$emonth."' AND YEAR(datetime) = '".$eyear."'";
	
		$db->setQuery($query);
		$credit = $db->loadResult();


// building month list
$emonthsList[] = JHTML::_('select.option', '','mm');
for ( $i = 1; $i <= 12; $i++ )
{
$emonthsList[] = JHTML::_('select.option', $i,$i);
}
$emonthList=array();
$emonthList["html"] = JHTML::_('select.genericlist',  $emonthsList, 'emonth', 'class="inputbox" size="1"', 'value', 'text', '' );

// building year list
$eyearsList[] = JHTML::_('select.option', '','yyyy');
$y = date ( "Y", extcal_get_local_time ( ) );

for ( $i = 1; $i <= 20; $i++ )
{
$eyearsList[] = JHTML::_('select.option', $y,$y);

	$y++;
}
$eyearList=array();
$eyearList["html"] = JHTML::_('select.genericlist',  $eyearsList, 'eyear', 'class="inputbox" size="1"', 'value', 'text', '' );

?>
<?php

	$cid = $this->cid;
	$edit = JRequest::getVar('edit',true);
	$text = intval($edit) ? JText::_( 'Edit' ) : JText::_( 'New' );
	
	JToolBarHelper::title( JText::_( 'Monthly Transaction' ) . ': <small><small>[ '. $text .' ]</small></small>' , 'user.png' );
	JToolBarHelper::apply('monthly','Get transaction');

?>

<form action="index.php" method="post" name="adminForm" autocomplete="off">

	<div class="col width-45">
		<fieldset class="adminform">
		<legend><?php echo JText::_( 'Monthly' ); ?></legend>
			<table class="admintable" cellspacing="1">
				<tr>
					<td width="150" class="key">
						<label for="daily">
							<?php echo JText::_( 'Month/Year' ); ?>
						</label>
					</td>
					<td>
                    	<?php echo $emonthList['html'];?>
                    	<?php echo $eyearList['html'];?>
					</td>
				</tr>
			</table>
		</fieldset>
        <?php
		if(!empty($emonth)){
		?>
        <fieldset class="adminform">
		<legend><?php echo JText::_( 'Amount' ); ?></legend>
			<table class="admintable" cellspacing="1">
				<tr>
					<td width="150" class="key">
						<label for="name">
							<?php echo JText::_( 'Debit' ); ?>
						</label>
					</td>
					<td>
                    	<?php if(!empty($debit)){ echo "$".$debit; } else { echo "$0";}?>
					</td>
				</tr>
                <tr>
					<td width="150" class="key">
						<label for="name">
							<?php echo JText::_( 'Credit' ); ?>
						</label>
					</td>
					<td>
                    	<?php if(!empty($credit)){ echo "$".$credit; } else { echo "$0";}?>
					</td>
				</tr>
			</table>
		</fieldset>
        <?php
		}
		?>
	</div>
	<div class="clr"></div>

	<input type="hidden" name="id" value="<?php echo $this->row->id ?>" />
	<input type="hidden" name="cid" value="<?php echo $this->row->id ?>" />
	<input type="hidden" name="option" value="com_transaction" />
	<input type="hidden" name="task" value="" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>