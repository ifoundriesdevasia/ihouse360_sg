<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php  JHTML::_('behavior.tooltip');  ?>
<?php 
$db = & JFactory::getDBO();

$query = "SELECT SUM(debit) FROM #__users_transaction_log";
	
		$db->setQuery($query);
		$debit = $db->loadResult();
		
$query1 = "SELECT SUM(credit) FROM #__users_transaction_log";
	
		$db->setQuery($query1);
		$credit = $db->loadResult();

?>
<?php

	$cid = $this->cid;
	$edit = JRequest::getVar('edit',true);
	$text = intval($edit) ? JText::_( 'Edit' ) : JText::_( 'New' );
	
	JToolBarHelper::title( JText::_( 'Total Transaction' ) . ': <small><small>[ '. $text .' ]</small></small>' , 'user.png' );

?>

<form action="index.php" method="post" name="adminForm" autocomplete="off">

	<div class="col width-45">
        <fieldset class="adminform">
		<legend><?php echo JText::_( 'Total Amount' ); ?></legend>
			<table class="admintable" cellspacing="1">
				<tr>
					<td width="150" class="key">
						<label for="name">
							<?php echo JText::_( 'Debit' ); ?>
						</label>
					</td>
					<td>
                    	<?php echo "$".$debit;?>
					</td>
				</tr>
                <tr>
					<td width="150" class="key">
						<label for="name">
							<?php echo JText::_( 'Credit' ); ?>
						</label>
					</td>
					<td>
                    	<?php echo "$".$credit;?>
					</td>
				</tr>
			</table>
		</fieldset>

	</div>
	<div class="clr"></div>

	<input type="hidden" name="id" value="<?php echo $this->row->id ?>" />
	<input type="hidden" name="cid" value="<?php echo $this->row->id ?>" />
	<input type="hidden" name="option" value="com_transaction" />
	<input type="hidden" name="task" value="" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>