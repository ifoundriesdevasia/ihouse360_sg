a:4:{s:5:"child";a:1:{s:0:"";a:1:{s:3:"rss";a:1:{i:0;a:6:{s:4:"data";s:3:"
	
";s:7:"attribs";a:1:{s:0:"";a:1:{s:7:"version";s:3:"2.0";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:1:{s:7:"channel";a:1:{i:0;a:6:{s:4:"data";s:56:"
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:8:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:22:"Security Announcements";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:49:"https://developer.joomla.org/security-centre.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:13:"lastBuildDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 20 Aug 2020 06:29:26 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:9:"generator";a:1:{i:0;a:5:{s:4:"data";s:40:"Joomla! - Open Source Content Management";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"language";a:1:{i:0;a:5:{s:4:"data";s:5:"en-gb";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:14:"managingEditor";a:1:{i:0;a:5:{s:4:"data";s:49:"noreply@joomla.org (Joomla! Developer Network™)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"item";a:10:{i:0;a:6:{s:4:"data";s:31:"
			
			
			
			
			
			
			
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:85:"[20200706] - Core - System Information screen could expose redis or proxy credentials";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:149:"http://feeds.joomla.org/~r/JoomlaSecurityNews/~3/xcY42y1gxIg/823-20200706-core-system-information-screen-could-expose-redis-or-proxy-credentials.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:133:"https://developer.joomla.org/security-centre/823-20200706-core-system-information-screen-could-expose-redis-or-proxy-credentials.html";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1404:"<ul>
<li><strong>Project:</strong> Joomla!</li>
<li><strong>SubProject:</strong> CMS</li>
<li><strong>Impact:</strong> Low</li>
<li><strong>Severity:</strong> <span class="label label-info">Low</span></li>
<li><strong>Versions:</strong> 3.0.0-3.9.19</li>
<li><strong>Exploit type:</strong> Information Disclosure</li>
<li><strong>Reported Date:</strong> 2020-Jun-17</li>
<li><strong>Fixed Date:</strong> 2020-July-14</li>
<li><strong>CVE Number:</strong> <a href="https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-15698" target="_blank" rel="noopener noreferrer">CVE-2020-15698</a></li>
</ul>
<h3>Description</h3>
<p>Inadequate filtering in the system information screen could expose redis or proxy credentials</p>
<h3>Affected Installs</h3>
<p>Joomla! CMS versions 3.0.0 - 3.9.19</p>
<h3>Solution</h3>
<p>Upgrade to version 3.9.20</p>
<h3>Contact</h3>
<p>The JSST at the <a title="Contact the JSST" href="https://developer.joomla.org/security-centre.html">Joomla! Security Centre</a>.</p>
<div class="alert alert-info"><strong>Reported By: </strong>Phil Taylor</div><div class="feedflare">
<a href="http://feeds.joomla.org/~ff/JoomlaSecurityNews?a=xcY42y1gxIg:iMGNJqxTeek:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/JoomlaSecurityNews?d=yIl2AUoC8zA" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/JoomlaSecurityNews/~4/xcY42y1gxIg" height="1" width="1" alt=""/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:50:"security@joomla.org (Joomla! Security Strike Team)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:15:"Security Centre";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 14 Jul 2020 13:00:06 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:133:"https://developer.joomla.org/security-centre/823-20200706-core-system-information-screen-could-expose-redis-or-proxy-credentials.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:1;a:6:{s:4:"data";s:31:"
			
			
			
			
			
			
			
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:48:"[20200705] - Core - Escape mod_random_image link";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:112:"http://feeds.joomla.org/~r/JoomlaSecurityNews/~3/u2g0mxhpOcw/822-20200705-core-escape-mod-random-image-link.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:96:"https://developer.joomla.org/security-centre/822-20200705-core-escape-mod-random-image-link.html";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1368:"<ul>
<li><strong>Project:</strong> Joomla!</li>
<li><strong>SubProject:</strong> CMS</li>
<li><strong>Impact:</strong> Low</li>
<li><strong>Severity:</strong> <span class="label label-info">Low</span></li>
<li><strong>Versions:</strong> 3.0.0-3.9.19</li>
<li><strong>Exploit type:</strong> XSS</li>
<li><strong>Reported Date:</strong> 2020-Jun-08</li>
<li><strong>Fixed Date:</strong> 2020-July-14</li>
<li><strong>CVE Number:</strong> <a href="https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-15696" target="_blank" rel="noopener noreferrer">CVE-2020-15696</a></li>
</ul>
<h3>Description</h3>
<p>Lack of input filtering and escaping allows XSS attacks in mod_random_image</p>
<h3>Affected Installs</h3>
<p>Joomla! CMS versions 3.0.0 - 3.9.19</p>
<h3>Solution</h3>
<p>Upgrade to version 3.9.20</p>
<h3>Contact</h3>
<p>The JSST at the <a title="Contact the JSST" href="https://developer.joomla.org/security-centre.html">Joomla! Security Centre</a>.</p>
<div class="alert alert-info"><strong>Reported By: </strong>Phil Taylor</div><div class="feedflare">
<a href="http://feeds.joomla.org/~ff/JoomlaSecurityNews?a=u2g0mxhpOcw:LwXYvbwZGbE:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/JoomlaSecurityNews?d=yIl2AUoC8zA" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/JoomlaSecurityNews/~4/u2g0mxhpOcw" height="1" width="1" alt=""/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:50:"security@joomla.org (Joomla! Security Strike Team)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:15:"Security Centre";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 14 Jul 2020 13:00:05 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:96:"https://developer.joomla.org/security-centre/822-20200705-core-escape-mod-random-image-link.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:2;a:6:{s:4:"data";s:31:"
			
			
			
			
			
			
			
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:59:"[20200704] - Core - Variable tampering via user table class";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:123:"http://feeds.joomla.org/~r/JoomlaSecurityNews/~3/gNyOaDMn0nk/821-20200704-core-variable-tampering-via-user-table-class.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:107:"https://developer.joomla.org/security-centre/821-20200704-core-variable-tampering-via-user-table-class.html";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1390:"<ul>
<li><strong>Project:</strong> Joomla!</li>
<li><strong>SubProject:</strong> CMS</li>
<li><strong>Impact:</strong> Low</li>
<li><strong>Severity:</strong> <span class="label label-info">Low</span></li>
<li><strong>Versions:</strong> 3.0.0-3.9.19</li>
<li><strong>Exploit type:</strong> Incorrect Access Control</li>
<li><strong>Reported Date:</strong> 2020-Jun-02</li>
<li><strong>Fixed Date:</strong> 2020-July-14</li>
<li><strong>CVE Number:</strong> <a href="https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-15697" target="_blank" rel="noopener noreferrer">CVE-2020-15697</a></li>
</ul>
<h3>Description</h3>
<p>Internal read-only fields in the User table class could be modified by users.</p>
<h3>Affected Installs</h3>
<p>Joomla! CMS versions 3.9.0 - 3.9.19</p>
<h3>Solution</h3>
<p>Upgrade to version 3.9.20</p>
<h3>Contact</h3>
<p>The JSST at the <a title="Contact the JSST" href="https://developer.joomla.org/security-centre.html">Joomla! Security Centre</a>.</p>
<div class="alert alert-info"><strong>Reported By: </strong>Phil Taylor</div><div class="feedflare">
<a href="http://feeds.joomla.org/~ff/JoomlaSecurityNews?a=gNyOaDMn0nk:7a9pgofKYsY:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/JoomlaSecurityNews?d=yIl2AUoC8zA" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/JoomlaSecurityNews/~4/gNyOaDMn0nk" height="1" width="1" alt=""/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:50:"security@joomla.org (Joomla! Security Strike Team)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:15:"Security Centre";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 14 Jul 2020 13:00:04 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:107:"https://developer.joomla.org/security-centre/821-20200704-core-variable-tampering-via-user-table-class.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:3;a:6:{s:4:"data";s:31:"
			
			
			
			
			
			
			
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:62:"[20200703] - Core - CSRF in com_privacy remove-request feature";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:126:"http://feeds.joomla.org/~r/JoomlaSecurityNews/~3/u1AquQjC25A/820-20200703-core-csrf-in-com-privacy-remove-request-feature.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:110:"https://developer.joomla.org/security-centre/820-20200703-core-csrf-in-com-privacy-remove-request-feature.html";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1421:"<ul>
<li><strong>Project:</strong> Joomla!</li>
<li><strong>SubProject:</strong> CMS</li>
<li><strong>Impact:</strong> Low</li>
<li><strong>Severity:</strong> <span class="label label-info">Low</span></li>
<li><strong>Versions:</strong> 3.9.0-3.9.19</li>
<li><strong>Exploit type:</strong> CSRF</li>
<li><strong>Reported Date:</strong> 2020-May-07</li>
<li><strong>Fixed Date:</strong> 2020-July-14</li>
<li><strong>CVE Number:</strong> <a href="https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-15695" target="_blank" rel="noopener noreferrer">CVE-2020-15695</a></li>
</ul>
<h3>Description</h3>
<p>A missing token check in the remove request section of com_privacy causes a CSRF vulnerability.</p>
<h3>Affected Installs</h3>
<p>Joomla! CMS versions 3.9.0 - 3.9.19</p>
<h3>Solution</h3>
<p>Upgrade to version 3.9.20</p>
<h3>Contact</h3>
<p>The JSST at the <a title="Contact the JSST" href="https://developer.joomla.org/security-centre.html">Joomla! Security Centre</a>.</p>
<div class="alert alert-info"><strong>Reported By: </strong>Bui Duc Anh Khoa from Viettel Cyber Security</div><div class="feedflare">
<a href="http://feeds.joomla.org/~ff/JoomlaSecurityNews?a=u1AquQjC25A:wxzS55XOUW4:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/JoomlaSecurityNews?d=yIl2AUoC8zA" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/JoomlaSecurityNews/~4/u1AquQjC25A" height="1" width="1" alt=""/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:50:"security@joomla.org (Joomla! Security Strike Team)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:15:"Security Centre";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 14 Jul 2020 13:00:03 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:110:"https://developer.joomla.org/security-centre/820-20200703-core-csrf-in-com-privacy-remove-request-feature.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:4;a:6:{s:4:"data";s:31:"
			
			
			
			
			
			
			
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:79:"[20200702] - Core - Missing checks can lead to a broken usergroups table record";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:143:"http://feeds.joomla.org/~r/JoomlaSecurityNews/~3/rbK7oHOEB5E/819-20200702-core-missing-checks-can-lead-to-a-broken-usergroups-table-record.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:127:"https://developer.joomla.org/security-centre/819-20200702-core-missing-checks-can-lead-to-a-broken-usergroups-table-record.html";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1429:"<ul>
<li><strong>Project:</strong> Joomla!</li>
<li><strong>SubProject:</strong> CMS</li>
<li><strong>Impact:</strong> Moderate</li>
<li><strong>Severity:</strong> <span class="label label-info">Low</span></li>
<li><strong>Versions:</strong> 2.5.0-3.9.19</li>
<li><strong>Exploit type:</strong> Incorrect Access Control</li>
<li><strong>Reported Date:</strong> 2020-April-04</li>
<li><strong>Fixed Date:</strong> 2020-July-14</li>
<li><strong>CVE Number:</strong> <a href="https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-15699" target="_blank" rel="noopener noreferrer">CVE-2020-15699</a></li>
</ul>
<h3>Description</h3>
<p>Missing validation checks at the usergroups table object can result into an broken site configuration.</p>
<h3>Affected Installs</h3>
<p>Joomla! CMS versions 2.5.0 - 3.9.19</p>
<h3>Solution</h3>
<p>Upgrade to version 3.9.20</p>
<h3>Contact</h3>
<p>The JSST at the <a title="Contact the JSST" href="https://developer.joomla.org/security-centre.html">Joomla! Security Centre</a>.</p>
<div class="alert alert-info"><strong>Reported By: </strong>Hoang Kien from VSEC</div><div class="feedflare">
<a href="http://feeds.joomla.org/~ff/JoomlaSecurityNews?a=rbK7oHOEB5E:jPoVa0BKoQE:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/JoomlaSecurityNews?d=yIl2AUoC8zA" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/JoomlaSecurityNews/~4/rbK7oHOEB5E" height="1" width="1" alt=""/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:50:"security@joomla.org (Joomla! Security Strike Team)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:15:"Security Centre";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 14 Jul 2020 13:00:02 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:127:"https://developer.joomla.org/security-centre/819-20200702-core-missing-checks-can-lead-to-a-broken-usergroups-table-record.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:5;a:6:{s:4:"data";s:31:"
			
			
			
			
			
			
			
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:63:"[20200701] - Core - CSRF in com_installer ajax_install endpoint";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:127:"http://feeds.joomla.org/~r/JoomlaSecurityNews/~3/QK86-YKzo9g/818-20200701-core-csrf-in-com-installer-ajax-install-endpoint.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:111:"https://developer.joomla.org/security-centre/818-20200701-core-csrf-in-com-installer-ajax-install-endpoint.html";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1417:"<ul>
<li><strong>Project:</strong> Joomla!</li>
<li><strong>SubProject:</strong> CMS</li>
<li><strong>Impact:</strong> Low</li>
<li><strong>Severity:</strong> <span class="label label-info">Low</span></li>
<li><strong>Versions:</strong> 3.7.0-3.9.19</li>
<li><strong>Exploit type:</strong> CSRF</li>
<li><strong>Reported Date:</strong> 2020-May-07</li>
<li><strong>Fixed Date:</strong> 2020-July-14</li>
<li><strong>CVE Number:</strong> <a href="https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-XXXXX" target="_blank" rel="noopener noreferrer">CVE-2020-XXXXX</a></li>
</ul>
<h3>Description</h3>
<p>A missing token check in the ajax_install endpoint com_installer causes a CSRF vulnerability.</p>
<h3>Affected Installs</h3>
<p>Joomla! CMS versions 3.7.0 - 3.9.19</p>
<h3>Solution</h3>
<p>Upgrade to version 3.9.20</p>
<h3>Contact</h3>
<p>The JSST at the <a title="Contact the JSST" href="https://developer.joomla.org/security-centre.html">Joomla! Security Centre</a>.</p>
<div class="alert alert-info"><strong>Reported By: </strong>Bui Duc Anh Khoa from Viettel Cyber Security</div><div class="feedflare">
<a href="http://feeds.joomla.org/~ff/JoomlaSecurityNews?a=QK86-YKzo9g:pab980Cu2ks:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/JoomlaSecurityNews?d=yIl2AUoC8zA" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/JoomlaSecurityNews/~4/QK86-YKzo9g" height="1" width="1" alt=""/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:50:"security@joomla.org (Joomla! Security Strike Team)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:15:"Security Centre";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 14 Jul 2020 13:00:01 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:111:"https://developer.joomla.org/security-centre/818-20200701-core-csrf-in-com-installer-ajax-install-endpoint.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:6;a:6:{s:4:"data";s:31:"
			
			
			
			
			
			
			
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:43:"[20200605] - Core - CSRF in com_postinstall";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:107:"http://feeds.joomla.org/~r/JoomlaSecurityNews/~3/AJIfHvK7WE8/817-20200605-core-csrf-in-com-postinstall.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:91:"https://developer.joomla.org/security-centre/817-20200605-core-csrf-in-com-postinstall.html";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1348:"<ul>
<li><strong>Project:</strong> Joomla!</li>
<li><strong>SubProject:</strong> CMS</li>
<li><strong>Impact:</strong> Low</li>
<li><strong>Severity:</strong> <span class="label label-info">Low</span></li>
<li><strong>Versions:</strong> 3.7.0-3.9.18</li>
<li><strong>Exploit type:</strong> CSRF</li>
<li><strong>Reported Date:</strong> 2020-May-08</li>
<li><strong>Fixed Date:</strong> 2020-June-02</li>
<li><strong>CVE Number:</strong> <a href="https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-13760">CVE-2020-13760</a></li>
</ul>
<h3>Description</h3>
<p>Missing token checks in com_postinstall cause CSRF vulnerabilities.</p>
<h3>Affected Installs</h3>
<p>Joomla! CMS versions 3.7.0 - 3.9.18</p>
<h3>Solution</h3>
<p>Upgrade to version 3.9.19</p>
<h3>Contact</h3>
<p>The JSST at the <a title="Contact the JSST" href="https://developer.joomla.org/security-centre.html">Joomla! Security Centre</a>.</p>
<div class="alert alert-info"><strong>Reported By: </strong>Bui Duc Anh Khoa from Viettel Cyber Security</div><div class="feedflare">
<a href="http://feeds.joomla.org/~ff/JoomlaSecurityNews?a=AJIfHvK7WE8:uthT5mshSlo:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/JoomlaSecurityNews?d=yIl2AUoC8zA" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/JoomlaSecurityNews/~4/AJIfHvK7WE8" height="1" width="1" alt=""/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:50:"security@joomla.org (Joomla! Security Strike Team)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:15:"Security Centre";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 02 Jun 2020 13:00:05 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:91:"https://developer.joomla.org/security-centre/817-20200605-core-csrf-in-com-postinstall.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:7;a:6:{s:4:"data";s:31:"
			
			
			
			
			
			
			
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:47:"[20200604] - Core - XSS in jQuery.htmlPrefilter";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:111:"http://feeds.joomla.org/~r/JoomlaSecurityNews/~3/ugeXSCQCG3k/816-20200604-core-xss-in-jquery-htmlprefilter.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:95:"https://developer.joomla.org/security-centre/816-20200604-core-xss-in-jquery-htmlprefilter.html";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1752:"<ul>
<li><strong>Project:</strong> Joomla!</li>
<li><strong>SubProject:</strong> CMS</li>
<li><strong>Impact:</strong> Low</li>
<li><strong>Severity:</strong> <span class="label label-info">Moderate</span></li>
<li><strong>Versions:</strong> 3.0.0-3.9.18</li>
<li><strong>Exploit type:</strong> XSS</li>
<li><strong>Reported Date:</strong> 2020-April-10</li>
<li><strong>Fixed Date:</strong> 2020-June-02</li>
<li><strong>CVE Number:</strong> <a href="https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-11022">CVE-2020-11022</a> and <a href="https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-11023">CVE-2020-11023</a></li>
</ul>
<h3>Description</h3>
<p>The jQuery project released version 3.5.0, and as part of that, disclosed two security vulnerabilities that affect all prior versions. As mentioned in the jQuery blog, both are "[...] security issues in jQuery’s DOM manipulation methods, as in .html(), .append(), and the others."</p>
<p>The Drupal project has backported the relevant fixes back to jQuery 1.x and Joomla has adopted that patch.</p>
<h3>Affected Installs</h3>
<p>Joomla! CMS versions 3.0.0 - 3.9.18</p>
<h3>Solution</h3>
<p>Upgrade to version 3.9.19</p>
<h3>Contact</h3>
<p>The JSST at the <a title="Contact the JSST" href="https://developer.joomla.org/security-centre.html">Joomla! Security Centre</a>.</p>
<div class="alert alert-info"><strong>Reported By: </strong>David Jardin, JSST</div><div class="feedflare">
<a href="http://feeds.joomla.org/~ff/JoomlaSecurityNews?a=ugeXSCQCG3k:Hklqx1aGl0o:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/JoomlaSecurityNews?d=yIl2AUoC8zA" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/JoomlaSecurityNews/~4/ugeXSCQCG3k" height="1" width="1" alt=""/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:50:"security@joomla.org (Joomla! Security Strike Team)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:15:"Security Centre";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 02 Jun 2020 13:00:04 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:95:"https://developer.joomla.org/security-centre/816-20200604-core-xss-in-jquery-htmlprefilter.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:8;a:6:{s:4:"data";s:31:"
			
			
			
			
			
			
			
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:50:"[20200603] - Core - XSS in com_modules tag options";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:114:"http://feeds.joomla.org/~r/JoomlaSecurityNews/~3/1r1g7ECDO_g/815-20200603-core-xss-in-com-modules-tag-options.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:98:"https://developer.joomla.org/security-centre/815-20200603-core-xss-in-com-modules-tag-options.html";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1370:"<ul>
<li><strong>Project:</strong> Joomla!</li>
<li><strong>SubProject:</strong> CMS</li>
<li><strong>Impact:</strong> Moderate</li>
<li><strong>Severity:</strong> <span class="label label-info">Low</span></li>
<li><strong>Versions:</strong> 3.0.0-3.9.18</li>
<li><strong>Exploit type:</strong> XSS</li>
<li><strong>Reported Date:</strong> 2020-May-06</li>
<li><strong>Fixed Date:</strong> 2020-June-02</li>
<li><strong>CVE Number:</strong> <a href="https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-13762">CVE-2020-13762</a></li>
</ul>
<h3>Description</h3>
<p>Incorrect input validation of the module tag option in com_modules allow XSS attacks.</p>
<h3>Affected Installs</h3>
<p>Joomla! CMS versions 3.0.0 - 3.9.18</p>
<h3>Solution</h3>
<p>Upgrade to version 3.9.19</p>
<h3>Contact</h3>
<p>The JSST at the <a title="Contact the JSST" href="https://developer.joomla.org/security-centre.html">Joomla! Security Centre</a>.</p>
<div class="alert alert-info"><strong>Reported By: </strong>Bui Duc Anh Khoa from Viettel Cyber Security</div><div class="feedflare">
<a href="http://feeds.joomla.org/~ff/JoomlaSecurityNews?a=1r1g7ECDO_g:vU7wz2UxFf8:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/JoomlaSecurityNews?d=yIl2AUoC8zA" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/JoomlaSecurityNews/~4/1r1g7ECDO_g" height="1" width="1" alt=""/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:50:"security@joomla.org (Joomla! Security Strike Team)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:15:"Security Centre";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 02 Jun 2020 13:00:03 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:98:"https://developer.joomla.org/security-centre/815-20200603-core-xss-in-com-modules-tag-options.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:9;a:6:{s:4:"data";s:31:"
			
			
			
			
			
			
			
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:60:"[20200602] - Core - Inconsistent default textfilter settings";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:124:"http://feeds.joomla.org/~r/JoomlaSecurityNews/~3/XfBJY540_dk/814-20200602-core-inconsistent-default-textfilter-settings.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:108:"https://developer.joomla.org/security-centre/814-20200602-core-inconsistent-default-textfilter-settings.html";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1501:"<ul>
<li><strong>Project:</strong> Joomla!</li>
<li><strong>SubProject:</strong> CMS</li>
<li><strong>Impact:</strong> Low</li>
<li><strong>Severity:</strong> <span class="label label-info">Low</span></li>
<li><strong>Versions:</strong> 2.5.0-3.9.18</li>
<li><strong>Exploit type:</strong> Insecure Permissions</li>
<li><strong>Reported Date:</strong> 2020-April-23</li>
<li><strong>Fixed Date:</strong> 2020-June-02</li>
<li><strong>CVE Number:</strong> <a href="https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-13763">CVE-2020-13763</a></li>
</ul>
<h3>Description</h3>
<p>The default settings of the global "textfilter" configuration doesn't block HTML inputs for 'Guest' users. With 3.9.19, the textfilter for new installations has been set to 'No HTML' for the groups 'Public', 'Guest' and 'Registered'.</p>
<h3>Affected Installs</h3>
<p>Joomla! CMS versions 2.5.0 - 3.9.18</p>
<h3>Solution</h3>
<p>Upgrade to version 3.9.19</p>
<h3>Contact</h3>
<p>The JSST at the <a title="Contact the JSST" href="https://developer.joomla.org/security-centre.html">Joomla! Security Centre</a>.</p>
<div class="alert alert-info"><strong>Reported By: </strong> Brian Teeman</div><div class="feedflare">
<a href="http://feeds.joomla.org/~ff/JoomlaSecurityNews?a=XfBJY540_dk:sHPQ6GrC0kc:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/JoomlaSecurityNews?d=yIl2AUoC8zA" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/JoomlaSecurityNews/~4/XfBJY540_dk" height="1" width="1" alt=""/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:50:"security@joomla.org (Joomla! Security Strike Team)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:15:"Security Centre";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 02 Jun 2020 13:00:02 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:108:"https://developer.joomla.org/security-centre/814-20200602-core-inconsistent-default-textfilter-settings.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}s:27:"http://www.w3.org/2005/Atom";a:1:{s:4:"link";a:2:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:3:"rel";s:4:"self";s:4:"type";s:19:"application/rss+xml";s:4:"href";s:42:"http://feeds.joomla.org/JoomlaSecurityNews";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:2:{s:3:"rel";s:3:"hub";s:4:"href";s:32:"http://pubsubhubbub.appspot.com/";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:3:{s:4:"info";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:1:{s:3:"uri";s:18:"joomlasecuritynews";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:14:"emailServiceId";a:1:{i:0;a:5:{s:4:"data";s:18:"JoomlaSecurityNews";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:18:"feedburnerHostname";a:1:{i:0;a:5:{s:4:"data";s:29:"https://feedburner.google.com";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}}}}}}s:4:"type";i:128;s:7:"headers";a:11:{s:12:"content-type";s:23:"text/xml; charset=UTF-8";s:4:"etag";s:27:"VdNStKo2T8EhXsbLoPrpk9ykvyM";s:13:"last-modified";s:29:"Thu, 20 Aug 2020 06:29:54 GMT";s:16:"content-encoding";s:4:"gzip";s:17:"transfer-encoding";s:7:"chunked";s:4:"date";s:29:"Thu, 20 Aug 2020 06:32:55 GMT";s:7:"expires";s:29:"Thu, 20 Aug 2020 06:32:55 GMT";s:13:"cache-control";s:18:"private, max-age=0";s:22:"x-content-type-options";s:7:"nosniff";s:16:"x-xss-protection";s:13:"1; mode=block";s:6:"server";s:3:"GSE";}s:5:"build";s:14:"20090627192103";}