
function primePropertiesTab(type) {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();			
	
	$('tab_li5_sell').removeClass('tabactive5');
	$('tab_li5_rent').removeClass('tabactive5');

	var url = 'index.php?option=com_ihouse&task=ajaxLoadPrimeProperties&r=' + unixtime_ms;		
	var req = new Ajax(url, {
	   					data		: 	
									{
										'adtype' 	: 	type
									},
						method		: "get",
		    			onSuccess	: function(data) {
							$('ajax-prime-properties').setHTML(data);
							$('tab_li5_' + type).addClass('tabactive5');
		   				},
						evalScripts : true
				}).request();				 
}

window.addEvent('domready', function() {
	primePropertiesTab('sell');
});