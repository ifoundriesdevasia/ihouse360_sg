// Browser Window Size and Position
// copyright Stephen Chapman, 3rd Jan 2005, 8th Dec 2005
// you may copy these functions but please keep the copyright notice as well
function pageWidth() 
{
	return window.innerWidth != null? window.innerWidth : document.documentElement && document.documentElement.clientWidth ?       document.documentElement.clientWidth : document.body != null ? document.body.clientWidth : null;
} 

function pageHeight() 
{
	return  window.innerHeight != null? window.innerHeight : document.documentElement && document.documentElement.clientHeight ?  document.documentElement.clientHeight : document.body != null? document.body.clientHeight : null;
} 

function posLeft() 
{
	return typeof window.pageXOffset != 'undefined' ? window.pageXOffset :document.documentElement && document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft ? document.body.scrollLeft : 0;
} 
	
function posTop() 
{
	return typeof window.pageYOffset != 'undefined' ?  window.pageYOffset : document.documentElement && document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop ? document.body.scrollTop : 0;
} 
function posRight() 
{
	return posLeft()+pageWidth();
} 
function posBottom() {
	return posTop()+pageHeight();
}
//alert(pageWidth());
//alert(pageHeight());
//alert(posBottom());

function formatNumber(value, decimal, id) {
     //decimal  - the number of decimals after the digit from 0 to 3
     //-- Returns the passed number as a string in the xxx,xxx.xx format.
	 var StartNumber = value;
	 var StartNumber = StartNumber.replace(/\,/g,'');
	 var ReplacedNumber = StartNumber.replace(/\t\r\n/g,'');
       anynum=ReplacedNumber;
	   
       divider =10;
       switch(decimal){
            case 0:
                divider =1;
                break;
            case 1:
                divider =10;
                break;
            case 2:
                divider =100;
                break;
            default:       //for 3 decimal places
                divider =1000;
        } 

       workNum=Math.abs((Math.round(anynum*divider)/divider)); 

       workStr=""+workNum 

       if (workStr.indexOf(".")==-1){workStr+="."} 

       dStr=workStr.substr(0,workStr.indexOf("."));dNum=dStr-0
       pStr=workStr.substr(workStr.indexOf(".")) 

       while (pStr.length-1< decimal){pStr+="0"} 

       if(pStr =='.') pStr =''; 

       //--- Adds a comma in the thousands place.    
       if (dNum>=1000) {
          dLen=dStr.length
          dStr=parseInt(""+(dNum/1000))+","+dStr.substring(dLen-3,dLen)
       } 

       //-- Adds a comma in the millions place.
       if (dNum>=1000000) {
          dLen=dStr.length
          dStr=parseInt(""+(dNum/1000000))+","+dStr.substring(dLen-7,dLen)
       }
	   
	   //-- Adds a comma in the billions place.
       if (dNum>=1000000000) {
          dLen=dStr.length
          dStr=parseInt(""+(dNum/1000000000))+","+dStr.substring(dLen-11,dLen)
       }
	   
	   //-- Adds a comma in the trillions place.
       if (dNum>=1000000000000) {
          dLen=dStr.length
          dStr=parseInt(""+(dNum/1000000000000))+","+dStr.substring(dLen-15,dLen)
       }
	   
       retval = dStr + pStr
	   
	  
       //-- Put numbers in parentheses if negative.
    if (anynum<0) {retval="("+retval+")";} 

	if(retval == 0) {
		   retval = '';
	}
	  
	  
	$(id).setProperty('value', retval);
}

function isNumberKey(evt)
{
	var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
       return false;
	}
		 
    return true;
}

function captureF11(evt) {
	var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode == 122) {
		/* Google Map - Full Screen */
		if(window.fullScreen) {
			$('big_map').setStyle('width', (pageWidth()-0));
			$('big_map').setStyle('height', (pageHeight()-0));
		} else {
			$('big_map').setStyle('width', (pageWidth()-0));
			$('big_map').setStyle('height', (pageHeight()-180));
		}
		
		initialize();
		deleteOverlays();
		//setTimeout('showMarkers()', 1000);
		
	}

}

window.addEvent('domready', function(){
	$('mapsearch_index_tmpl').setStyle('width', (pageWidth()-0));
	$('searchmenu2').setStyle('width', (pageWidth()-0));
	$('map_wrapper').setStyle('width',(pageWidth()-0) );
	$('map_block').setStyle('width',(pageWidth()-0) );
	$('big_map').setStyle('width', (pageWidth()-0));
	$('big_map').setStyle('height', (pageHeight()-180));
	
	/*$('big_map').addEvent('keydown', function(event){
		alert(event.keyCode);
	});*/

});