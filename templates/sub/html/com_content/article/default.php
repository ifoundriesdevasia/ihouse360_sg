<?php // no direct access
defined('_JEXEC') or die('Restricted access');

//$canEdit	= ($this->user->authorize('com_content', 'edit', 'content', 'all') || $this->user->authorize('com_content', 'edit', 'content', 'own'));
?>
<?php //echo $this->article->catid; ?>

<div style="background-color: rgb(255, 255, 255);margin-top:-16px;" id="sub-small-component-border">
	<div id="sub-small-component">
        <div id="component-headerpos">
            <div id="component-logo">
                <div id="component-title" style="width:95%"><span class="chinese_text1">快讯 News<?php //echo $this->escape($this->article->title); ?></span>&nbsp;<i></i></div>
            </div>
        </div>
    </div>
    <div style="clear:both"></div>    
    <div id="sub-small-component-content" style="padding:0 10px;">             
		<div style="color:#062284;font-size:17px;font-weight:bold;margin:20px 0;" ><?php echo $this->escape($this->article->title); ?></br><span style="font-size:14px; color:#999; margin-right:20px; float:right;">(<?php echo date('d/M/Y',strtotime($this->article->publish_up)) ?>)</span><hr size="1"></div>
<!--Google 广告 - 连接单元-->
<div style="margin-bottom:15px;">
<script type="text/javascript"><!--
google_ad_client = "ca-pub-0768787559767318";
/* Main-01 */
google_ad_slot = "6754487251";
google_ad_width = 468;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
</div>
<!--Google 广告 - 连接单元结束-->
<?php if ($this->params->get('show_page_title', 1) && $this->params->get('page_title') != $this->article->title) : ?>
	<div class="componentheading<?php echo $this->escape($this->params->get('pageclass_sfx')); ?>">
		<?php echo $this->escape($this->params->get('page_title')); ?>
	</div>
<?php endif; ?>
<?php if ($canEdit || $this->params->get('show_title') || $this->params->get('show_pdf_icon') || $this->params->get('show_print_icon') || $this->params->get('show_email_icon')) : ?>
<table class="contentpaneopen<?php echo $this->escape($this->params->get('pageclass_sfx')); ?>">
<tr>

	<?php if (!$this->print) : ?>
		<?php if ($this->params->get('show_pdf_icon')) : ?>
		<td align="right" width="100%" class="buttonheading">
		<?php echo JHTML::_('icon.pdf',  $this->article, $this->params, $this->access); ?>
		</td>
		<?php endif; ?>

		<?php if ( $this->params->get( 'show_print_icon' )) : ?>
		<td align="right" width="100%" class="buttonheading">
		<?php echo JHTML::_('icon.print_popup',  $this->article, $this->params, $this->access); ?>
		</td>
		<?php endif; ?>

		<?php if ($this->params->get('show_email_icon')) : ?>
		<td align="right" width="100%" class="buttonheading">
		<?php echo JHTML::_('icon.email',  $this->article, $this->params, $this->access); ?>
		</td>
		<?php endif; ?>
		<?php if ($canEdit) : ?>
		<!--<td align="right" width="100%" class="buttonheading">
			<?php echo JHTML::_('icon.edit', $this->article, $this->params, $this->access); ?>
		</td>-->
		<?php endif; ?>
	<?php else : ?>
		<td align="right" width="100%" class="buttonheading">
		<?php echo JHTML::_('icon.print_screen',  $this->article, $this->params, $this->access); ?>
		</td>
	<?php endif; ?>
</tr>
</table>
<?php endif; ?>

<?php  if (!$this->params->get('show_intro')) :
	echo $this->article->event->afterDisplayTitle;
endif; ?>
<?php echo $this->article->event->beforeDisplayContent; ?>
<table class="contentpaneopen<?php echo $this->escape($this->params->get('pageclass_sfx')); ?>">
<?php if (($this->params->get('show_section') && $this->article->sectionid) || ($this->params->get('show_category') && $this->article->catid)) : ?>
<tr>
	<td>
		<?php if ($this->params->get('show_section') && $this->article->sectionid && isset($this->article->section)) : ?>
		<span>
			<?php if ($this->params->get('link_section')) : ?>
				<?php echo '<a href="'.JRoute::_(ContentHelperRoute::getSectionRoute($this->article->sectionid)).'">'; ?>
			<?php endif; ?>
			<?php echo $this->escape($this->article->section); ?>
			<?php if ($this->params->get('link_section')) : ?>
				<?php echo '</a>'; ?>
			<?php endif; ?>
				<?php if ($this->params->get('show_category')) : ?>
				<?php echo ' - '; ?>
			<?php endif; ?>
		</span>
		<?php endif; ?>
		<?php if ($this->params->get('show_category') && $this->article->catid) : ?>
		<span>
			<?php if ($this->params->get('link_category')) : ?>
				<?php echo '<a href="'.JRoute::_(ContentHelperRoute::getCategoryRoute($this->article->catslug, $this->article->sectionid)).'">'; ?>
			<?php endif; ?>
			<?php echo $this->escape($this->article->category); ?>
			<?php if ($this->params->get('link_category')) : ?>
				<?php echo '</a>'; ?>
			<?php endif; ?>
		</span>
		<?php endif; ?>
	</td>
</tr>
<?php endif; ?>
<?php if (($this->params->get('show_author')) && ($this->article->author != "")) : ?>
<tr>
	<td valign="top">
		<span class="small">
			<?php JText::printf( 'Written by', ($this->escape($this->article->created_by_alias) ? $this->escape($this->article->created_by_alias) : $this->escape($this->article->author)) ); ?>
		</span>
		&nbsp;&nbsp;
	</td>
</tr>
<?php endif; ?>

<?php if ($this->params->get('show_create_date')) : ?>
<tr>
	<td valign="top" class="createdate">
		<?php echo JHTML::_('date', $this->article->created, JText::_('DATE_FORMAT_LC2')) ?>
	</td>
</tr>
<?php endif; ?>

<?php if ($this->params->get('show_url') && $this->article->urls) : ?>
<tr>
	<td valign="top">
		<a href="http://<?php echo $this->article->urls ; ?>" target="_blank">
			<?php echo $this->escape($this->article->urls); ?></a>
	</td>
</tr>
<?php endif; ?>

<tr>
<td valign="top">
<?php if (isset ($this->article->toc)) : ?>
	<?php echo $this->article->toc; ?>
<?php endif; ?>
<?php echo $this->article->text; ?>
</td>
</tr>

<?php if ( intval($this->article->modified) !=0 && $this->params->get('show_modify_date')) : ?>
<tr>
	<td class="modifydate">
		<?php echo JText::sprintf('LAST_UPDATED2', JHTML::_('date', $this->article->modified, JText::_('DATE_FORMAT_LC2'))); ?>
	</td>
</tr>
<?php endif; ?>
</table>
<hr size="1">
<span class="article_separator">&nbsp;</span>
<?php echo $this->article->event->afterDisplayContent; ?>
	{modulepos ihouse_marketnews_page}
	</div><!-- sub-small-component-content -->
</div><br />

<!--Google 广告 - news底部-->
    <div id="sub-small-component">
        <div id="component-headerpos">
            <div id="component-logo">
                <div id="component-title" style="width:95%"><span class="chinese_text1">赞助商链接</span>&nbsp;<i></i></div>
            </div>
        </div>
    </div>
    <div style="clear:both"></div>
    <div id="sub-small-component-content" style="padding:25px 30px;">
        <div style="width:300px; background-color:#fff; border:#F93 1px dashed ; padding:5px 5px; float:left; margin-bottom:25px;">
        <script type="text/javascript"><!--
        google_ad_client = "ca-pub-0768787559767318";
        /* Main-02 */
        google_ad_slot = "4640024593";
        google_ad_width = 300;
        google_ad_height = 250;
        //-->
        </script>
        <script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js"></script>
        </div>
        <div style="width:300px; background-color:#fff; border:#F93 1px dashed; padding:5px 5px; float:left; margin-left:5px; margin-bottom:25px;">
        <script type="text/javascript"><!--
        google_ad_client = "ca-pub-0768787559767318";
        /* main-03 */
        google_ad_slot = "4693443147";
        google_ad_width = 300;
        google_ad_height = 250;
        //-->
        </script>
        <script type="text/javascript"
        src="http://pagead2.googlesyndication.com/pagead/show_ads.js"></script>
        </div>
    </div>
	<!--Google 广告 End - news底部-->