<?php // no direct access
defined('_JEXEC') or die('Restricted access'); ?>
<script language="javascript" type="text/javascript">
function tableOrdering( order, dir, task )
{
	var form = document.adminForm;

	form.filter_order.value 	= order;
	form.filter_order_Dir.value	= dir;
	document.adminForm.submit( task );
}
</script>
<div id="liststyle_sector" style="padding-left:5px;">
<ul>

<?php foreach ($this->items as $item) : ?>
	<li style="padding:4px;">
		<a style="color:#062284;text-decoration:none;" href="<?php echo $item->link; ?>" target="_blank">
			<?php echo $this->escape($item->title); ?></a><span style="color:#525252">&nbsp;&nbsp;(<?php echo date('d/M/Y',strtotime($item->publish_up)) ?>)</span>
			<?php //$this->item = $item; echo JHTML::_('icon.edit', $item, $this->params, $this->access) ?>
		<?php
			/*echo $this->escape($item->title).' : ';
			$link = JRoute::_('index.php?option=com_user&view=login');
			$returnURL = JRoute::_(ContentHelperRoute::getArticleRoute($item->slug, $item->catslug, $item->sectionid), false);
			$fullURL = new JURI($link);
			$fullURL->setVar('return', base64_encode($returnURL));
			$link = $fullURL->toString();
			*/
		?>
	</li>
<?php endforeach; ?>
</ul>
</div>
<?php if ($this->params->get('show_pagination')) : ?>
<div style="text-align:right;padding:10px;">
		<?php echo $this->pagination->getPagesLinks(); ?>

		<?php echo $this->pagination->getPagesCounter(); ?>
</div>
	
<?php endif; ?>
