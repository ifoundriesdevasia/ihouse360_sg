function sendSubs(lang) {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();	
	
	$('subscribeLoader').setStyle('display','block');
	/* send the form */
	
	$('sendsubsform').send({
		onComplete: function(data) {
			var obj1 		= 	Json.evaluate(data);	
			var status		=	obj1.status;
				
			if(status) {
				$('homepage_popup_' + lang).setStyle('display', 'block');
				$('subs-err').setHTML('Subscribed!!');
			} else {
				$('subs-err').setHTML('Error Subscribing : Not in email format or it is already exist');
			}
					
			$('subscribeLoader').setStyle('display','none');	
		}
	});

}

function redirect(url) {
	window.location = url;
}

/*function closeSubsPopup(lang) {
	$('homepage_popup_' + lang).setStyle('display', 'none');
}*/


function subsLang1(lang,user_type) {
	//alert('aa');
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();	

	//$('topLoader').setStyle('display','block');
	var url = 'index.php?option=com_user&task=loadForm&r=' + unixtime_ms;		
	var req = new Ajax(url, {
	   			data		: 	
							{
								'lang' : lang,
								'user_type'	: user_type
							},
				method		: "get",
		    	onSuccess	: function(data) {
					//$('topLoader').setStyle('display','none');
					$('subs_form').setHTML(data);
		   		},
				evalScripts : true
		}).request();				 

}

function validationSubscription(langs) {
	$('salute').setStyle('color','');
	$('name').setStyle('color','');
	$('email').setStyle('color','');
	
	var salute 		= $('salute').getProperty('value');
	var name		= $('name').getProperty('value');
	var email		= $('email').getProperty('value');
	
	var how 		= true;
	
	if(salute == '') {
		$('salute').setStyle('color','red');
		how = false;
	}
	
	if(name == '') {
		$('name').setStyle('color','red');
		how = false;
	
	}
	
	if(email == '') {
		$('email').setStyle('color','red');
		how = false;
	
	} 
	
	if(how) {
		$('validInput').setProperty('value', 1);
		sendSubs(langs);
	}
	else {
		$('validInput').setProperty('value', 0);
	}
	
	submit();
}

