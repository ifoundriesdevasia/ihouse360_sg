/*function checkAcymail(value) {
	
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();	
	
	$('subscribeLoader').setStyle('display','block');
	$('subs-err').setHTML('');
	
	var url = 'index.php?option=com_subscription&task=checkEmail&r=' + unixtime_ms;		
	var req = new Ajax(url, {
	   			data		: 	
							{
								'email' : value,
							},
				method		: "get",
		    	onComplete	: function(data) {
					var obj1 		= 	Json.evaluate(data);	
					var status		=	obj1.status;

					$('subscribeLoader').setStyle('display','none');
					
					if(status) {
						$('subs-err').setHTML('Email is good');
						
					} else {
						$('subs-err').setHTML('Error: Not in email format or it is already exist');
						$('validInput').setProperty('value', 0);
					}
					
		   		},
				evalScripts : true
		}).request();				 

}
*/
function sendSubs(lang) {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();	
	
	$('subscribeLoader').setStyle('display','block');
	
	$('sendsubsform').send({
		onComplete: function(data) {
			var obj1 		= 	Json.evaluate(data);	
			var status		=	obj1.status;
				
			if(status) {
				$('homepage_popup_' + lang).setStyle('display', 'block');
				$('subs-err').setHTML('Subscribed!!');
			} else {
				$('subs-err').setHTML('Error Subscribing : Not in email format or it is already exist');
			}
					
			$('subscribeLoader').setStyle('display','none');	
		}
	});

}

function redirect(url) {
	window.location = url;
}

function closeSubsPopup(lang) {
	$('homepage_popup_' + lang).setStyle('display', 'none');
}


function subsLang(lang) {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();	

	$('topLoader').setStyle('display','');
	
	var url = 'index.php?option=com_subscription&task=loadForm&r=' + unixtime_ms;		
	var req = new Ajax(url, {
	   			data		: 	
							{
								'lang' : lang
							},
				method		: "get",
		    	onSuccess	: function(data) {
					
					$('topLoader').setStyle('display','none');
					$('subs_form').setHTML(data);
		   		},
				evalScripts : true
		}).request();				 

}

function validationSubscription(langs) {
	$('sub_salute').setStyle('color','');
	$('sub_name').setStyle('color','');
	$('sub_email').setStyle('color','');
	
	var salute 		= $('salute').getProperty('value');
	var name		= $('name').getProperty('value');
	var email		= $('email').getProperty('value');
	
	var how 		= true;
	
	if(salute == '') {
		$('sub_salute').setStyle('color','red');
		how = false;
	}
	
	if(name == '') {
		$('sub_name').setStyle('color','red');
		how = false;
	}
	
	if(email == '') {
		$('sub_email').setStyle('color','red');
		how = false;
	} 
	
	if(how) {
		$('validInput').setProperty('value', 1);
		sendSubs(langs);
	}
	else {
		$('validInput').setProperty('value', 0);
	}
}

window.addEvent('domready', function(){
	subsLang('ch');							
});
