<?php
/**
 * @version		$Id: modules.php 14401 2010-01-26 14:10:00Z louis $
 * @package		Joomla
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/*
 * none (output raw module content)
 */
function modChrome_none($module, &$params, &$attribs)
{
	echo $module->content;
}

/*
 * Module chrome that wraps the module in a table
 */
function modChrome_table($module, &$params, &$attribs)
{ ?>
	<table cellpadding="0" cellspacing="0" class="moduletable<?php echo $params->get('moduleclass_sfx'); ?>">
	<?php if ($module->showtitle != 0) : ?>
		<tr>
			<th valign="top">
				<?php echo $module->title; ?>
			</th>
		</tr>
	<?php endif; ?>
		<tr>
			<td>
				<?php echo $module->content; ?>
			</td>
		</tr>
		</table>
	<?php
}

/*
 * Module chrome that wraps the tabled module output in a <td> tag of another table
 */
function modChrome_horz($module, &$params, &$attribs)
{ ?>
	<table cellspacing="1" cellpadding="0" border="0" width="100%">
		<tr>
			<td valign="top">
				<?php modChrome_table($module, $params, $attribs); ?>
			</td>
		</tr>
	</table>
	<?php
}

/*
 * xhtml (divs and font headder tags)
 */
function modChrome_xhtml($module, &$params, &$attribs)
{
	if (!empty ($module->content)) : ?>
		<div class="moduletable<?php echo $params->get('moduleclass_sfx'); ?>">
		<?php if ($module->showtitle != 0) : ?>
			<h3><?php echo $module->title; ?></h3>
		<?php endif; ?>
			<?php echo $module->content; ?>
		</div>
	<?php endif;
}

/*
 * Module chrome that allows for rounded corners by wrapping in nested div tags
 */
function modChrome_rounded($module, &$params, &$attribs)
{ ?>
		<div class="module<?php echo $params->get('moduleclass_sfx'); ?>">
			<div>
				<div>
					<div>
						<?php if ($module->showtitle != 0) : ?>
							<h3><?php echo $module->title; ?></h3>
						<?php endif; ?>
					<?php echo $module->content; ?>
					</div>
				</div>
			</div>
		</div>
	<?php
}


/*
 * Module chrome that add preview information to the module
 */
function modChrome_outline($module, &$params, &$attribs)
{
	$doc =& JFactory::getDocument();
	$css  = ".mod-preview-info { padding: 2px 4px 2px 4px; border: 1px solid black; position: absolute; background-color: white; color: red;opacity: .80; filter: alpha(opacity=80); -moz-opactiy: .80; }";
	$css .= ".mod-preview-wrapper { background-color:#eee;  border: 1px dotted black; color:#700; opacity: .50; filter: alpha(opacity=50); -moz-opactiy: .50;}";
	$doc->addStyleDeclaration($css);

	?>
	<div class="mod-preview">
		<div class="mod-preview-info"><?php echo $module->position."[".$module->style."]"; ?></div>
		<div class="mod-preview-wrapper">
			<?php echo $module->content; ?>
		</div>
	</div>
	<?php
}

function modChrome_customhtml($module, &$params, &$attribs)
{ ?>
	<?php echo strip_tags($module->content, '<a>'); ?>
<?php
}

function modChrome_customhtml2($module, &$params, &$attribs)
{ ?>
	<?php echo strip_tags($module->content, '<a><br />'); ?>
<?php
}

function modChrome_newsletter($module, &$params, &$attribs)
{ ?>
 <div id="module-padding">
                <div id="module-pos1">
                    <div id="small-module-border">
                    
                        <div id="small-module">
                            <div id="module-headerpos">
                                <div id="module-title">Free Subscription</div>
                            </div>
                        </div>	<!--small-module-->
                        
                        <div id="small-module-content">
                        	<div id="module-content-padding">
                            
                            	<div id="small-module-content-cont">
                                    <div id="small-module-content-text">注册并接收定期简报</div>
                                    <div id="small-module-content-text1">中文 | 英文</div>
                                </div><!--small-module-content-cont-->
                        
                        <div id="small-module-content-cont"><?php echo $module->content; ?></div>
         </div><!--module-content-padding-->
                        	</div><!--small-module-content-->
                    </div><!--small-module-border-->
                </div><!--module-pos1-->
            </div><!--module-padding-->
<?php
}//end function

function modChrome_singapore($module, &$params, &$attribs)
{
?>
 <!--Start of module-->
        <div id="main-cont">
        	<div id="mid-module-border">
                <div id="mid-module">
                    <div id="module-headerpos">
                            <div id="module-title"><font style="font-size:15px;">狮城万象</font> Singapore</div>
                    </div>
                </div>
                
                <div id="mid-module-content">
                	

<?php echo $module->content; ?>
       <!--End of tabs-->
                </div>
            </div>
        </div>
        <!--End of module-->

<?php
}

function modChrome_realestate($module, &$params, &$attribs)
{
?>
 <div id="module-padding">
            <div id="module-pos4">
                <div id="sub-small-module-border">
                    <div id="sub-small-module">
                        <div id="module-headerpos">
                            <div id="module-logo">
                                <div id="module-title"><font style="font-size:15px;">最新房源</font> Latest Real Estate</div>
                            </div>
                        </div>
                    </div>

<?php echo $module->content; ?>

                </div>
            </div>
        </div>
        <!--End of module-->

<?php
}

function modChrome_leftmenu($module, &$params, &$attribs)
{
?>
<!--Start of module-->
    	<div id="module-padding">
            <div id="module-pos5">
                <div id="static-small-module-border">
                    <div id="static-small-module">
                        <div id="module-headerpos">
                                <div id="static-module-title">认识新加坡</div>
                        </div>
                    </div>

<?php echo $module->content; ?>
 
                </div>
            </div>
        </div>
        <!--End of module-->

<?php
}

function modChrome_abtleftmenu($module, &$params, &$attribs)
{
?>
<!--Start of module-->
    	<div id="module-padding">
            <div id="module-pos5">
                <div id="static-small-module-border">
                    <div id="static-small-module">
                        <div id="module-headerpos">
                                <div id="static-module-title"><i>Ihouse360</i></div>
                        </div>
                    </div>

<?php echo $module->content; ?>
 
                </div>
            </div>
        </div>
        <!--End of module-->

<?php
}
?>