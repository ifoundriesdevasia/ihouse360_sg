<?php // no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<div id="module-padding">
    	<div id="main-search-cont">
    	<div id="main-search-bg">
        	<div id="main-search-content">
            
            	<div id="main-search-header-text">新加坡公寓目录</div>
                
                <div id="main-search-pos">
               	
                    <!--LEFT-->
                    
                    <div id="main-search-left">
                        <div id="main-search-left-header">
                            <div id="main-search-header-pos">地区</div>
                        </div>
                        <div id="main-search-left-content">
                        <form name="condo_directory_form2" action="<?php echo JRoute::_('index.php?layout=condo_directory2&view=directory&option=com_ihouse') ?>" method="get">
                        
                        	<div id="main-search-padding">
                            	<div id="main-search-padding1">
                                <?php echo $districts ?>&nbsp;&nbsp;
                                <?php echo $lease ?>
                                </div>
                                <div id="main-search-padding2">
                                <?php echo $year_built	?>&nbsp;&nbsp;
                                <?php //echo $min_price ?>
                                </div>
                                <input type="button" value="搜索" class="searchbutton" onclick="document.condo_directory_form2.submit()" />
                            </div>
                        
                        <input type="hidden" name="option" value="com_ihouse" />
                        <input type="hidden" name="view" value="directory" />
                        <input type="hidden" name="layout" value="condo_directory2" />
                        <input type="hidden" name="type" value="<?php echo $type ?>" />  
                        </form>
                        </div>
                    </div>
                    
                    <!--END LEFT-->
                    
                    <!--RIGHT-->
                    
                    <div id="main-search-right">
                    	<div id="main-search-right-header">
                        	<div id="main-search-header-pos">项目名</div>
                        </div>
                        <?php 
							$type 					= JRequest::getVar('type'); 
							$lease_s				=	JRequest::getVar('lease', '');
							$district_s				=	JRequest::getVar('district_id', '');
							$year_built_s			=	JRequest::getVar('year_built', '');
							
							$urladd	= '';
							if($lease_s) 
								$urladd .= '&lease='.$lease_s;
							
							if($district_s)
								$urladd .= '&district_id='.$district_id;
								
							if($year_built_s)
								$urladd .= '&year_built='.$year_built_s;
								
						?>
                        <div id="main-search-right-content">
                        	<div id="main-search-padding3">
                            	<div id="directory-search-alpha1"><a href="<?php echo JRoute::_('index.php?option=com_ihouse&view=directory&layout=condo_directory2'.$urladd.'&Itemid=102') ?>">[All]</a></div>
                        		<div id="directory-search-alpha1"><a href="<?php echo JRoute::_('index.php?option=com_ihouse&view=directory&layout=condo_directory2&type=num'.$urladd.'&Itemid=102') ?>">[0-9]</a></div>
                                
								
								<?php foreach(range('A','Z') as $i) 
										echo '<div id="directory-search-alpha1"><a '.(($i == $type)?"class='active' ":"").' href="'.JRoute::_('index.php?option=com_ihouse&view=directory&layout=condo_directory2&type='.$i.$urladd.'&Itemid=102').'">['.$i.']</a></div>'; 
								?>

                            </div>
                        </div>
                    </div>
                    
                    <!--END RIGHT-->
                	</div><!--main-search-pos-->
                
            	</div><!--main-search-content-->
        	</div>
        </div>
        </div>
        <!--End Search Module-->