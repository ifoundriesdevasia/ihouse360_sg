<?php // no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<?php //JHTML::_('script', 'contacts.js', 'components/com_ihouse/assets/'); ?>  
<div class="component-pos">
	<div id="sub-small-component-border">
    	<div id="sub-small-component">
        	<div id="component-headerpos">
            	<div id="component-logo">
                	<div id="component-title"><span class="chinese_text1">金牌经纪</span>&nbsp;<i>Consultant</i></div>
                </div>
            </div>
        </div>
         <div id="sub-small-component-content" style="width:100%;">
                    	
                      	<div style="padding:10px;">
                        
                        <?php for($i = 0; $i < 3; $i++) : ?>
                        	<div class="list_prop_specialist">
                                <?php 	if(!empty($rows[$i])) : ?>
                            	<table width="100%" cellpadding=0 cellspacing=0>
                                    <tr>
                                    	<td width="50%">
                            			<div style="text-align:center;height:92px;">
                                        
										<?php
		  								if(empty($rows[$i]['user_image'])) {
											$user_image = JRoute::_('templates/main/images/no_avatar.jpg');
										} else {
											$user_image = 'image.php?size=92&type=1&path='.$rows[$i]['user_image'];
										}
		  								?>
                        					<a style="text-decoration:none;color:#fff;" href="<?php echo JRoute::_('index.php?option=com_user&view=agent_profile&id='.$rows[$i]['user_id'].'&Itemid=136'); ?>">
            									<img style="border:none;" src="<?php echo $user_image; ?>" />
                							</a>
                                            
                                		</div>
                                        <div style="clear:both"></div>
                                        <div style="text-align:center;">
						<!--<input type="button" onclick="javascript:emailFormPopup('<?php echo $rows[$i]['email'] ?>','');" id="agent_sub" name="agent_sub" class="contactbutton" value="Contact">-->
					</div>   
                                    	</td>
                                	    <td width="50%">         
                                		<?php 
			
				if(empty($rows[$i]['company_logo'])) {
					$comp_logo = JRoute::_('templates/main/images/thumb-nologo.jpg');
				}else{
					$comp_logo = 'image.php?size=100&type=1&path='.$rows[$i]['company_logo'];
				}
			
				?>
            
            	<?php if($rows[$i]['user_category'] == 'Agent'): ?>
            		<img src="<?php echo $comp_logo ?>" />
           	 	<?php endif; ?> 
                                	
                                		</div>
                                    	</td>
                                    </tr>              
                                </table>
                                <div class="thisspecialistprofile">
    			<div class="l">姓名</div>
        		<div class="m">:</div>
        		<div class="r">
				<?php 
				if(!empty($rows[$i]['name']) && !empty($rows[$i]['chinese_name'])) {
							$name = '';
							if (!empty($rows[$i]['name'])) {
								$name .= $rows[$i]['name'];
							}
							if (!empty($rows[$i]['chinese_name'])) {
								$name .= '</br>'.$rows[$i]['chinese_name'];
							}
							
							echo $name;
				} else { 
					  		echo 'N/A';
				}
				?>
            	</div>
            <div style="clear:both"></div>
            </div>	
            <div class="thisspecialistprofile">
    			<div class="l">公司名称</div>
        		<div class="m">:</div>
        		<div class="r"><?php echo (!empty($rows[$i]['company_name']))?wordwrap($rows[$i]['company_name'], 15, "\n", true):'N/A' ?></div>
            <div style="clear:both"></div>
        	</div>		
            <div class="thisspecialistprofile">
    			<div class="l">电话</div>
        		<div class="m">:</div>
        		<div class="r"><?php echo ($rows[$i]['mobile_contact'])?$rows[$i]['mobile_contact']:'N/A' ?></div>
            <div style="clear:both"></div>
        	</div>
            <div class="thisspecialistprofile">
    			<div class="l">网址</div>
        		<div class="m">:</div>
        		<div class="r"><?php echo (!empty($rows[$i]['website']))?wordwrap($rows[$i]['website'], 15, "\n", true):'N/A' ?></div>
            <div style="clear:both"></div>
        	</div>		
  			<div class="thisspecialistprofile">
    			<div class="l">经纪证号</div>
        		<div class="m">:</div>
        		<div class="r">
				<?php if(!empty($rows[$i]['cea_reg_no'])) : ?>
                	<?php if(preg_match('/^L/', $rows[$i]['cea_reg_no'] )): ?>
                		<a href="http://www.cea.gov.sg/cea/app/newimplpublicregister/view.jspa?licNo=<?php echo $rows[$i]['cea_reg_no'] ?>" target="_blank">
                    <?php elseif(preg_match('/^R/', $rows[$i]['cea_reg_no'] )) : ?>    
                    <a href='http://www.cea.gov.sg/cea/app/newimplpublicregister/view.jspa?licNo=""&regNo=<?php echo $rows[$i]['cea_reg_no'] ?>' target="_blank">
                    
                    <?php endif; ?>   
						<?php echo $rows[$i]['cea_reg_no'] ?>
                    </a>
                <?php else : ?>
                	N/A    
                <?php endif; ?>
                </div>
            <div style="clear:both"></div>
        	</div>  
                            <?php else : ?>
                            	<div style="text-align:center;">
                            	<img src="<?php echo JRoute::_('templates/main/images/no_avatar.jpg'); ?>" /><br />
                            	目前空缺 
                                </div>
                            <?php endif; ?>   
                           	</div>
                          <?php endfor; ?>      
                             	<div style="clear:both;"></div>
                         
                        </div>
                        
		</div>
	</div>
</div>

<!-- POPUP -->
<div id="popupForm" style="display:none;">
</div>