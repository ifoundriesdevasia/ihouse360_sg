﻿<?php
/**
 * Helper class for Slider Gallery Module
 * 
 * @package    Joomla
 * @subpackage Modules
 * @link http://www.yashvyas.in
 * @license        GNU/GPL
 * mod_galleryview is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */
class modiHouseSpecialistThisCondoHelper
{
    function getSpecialists()
    {
		$db		=& 	JFactory::getDBO();			
		
		$id 			= 	JRequest::getCmd('id');
		$postcode 		= 	JRequest::getCmd('postcode','');

		$query 	= "SELECT * FROM #__ihouse_ads_type AS t "
					. " LEFT JOIN #__users AS u ON u.id = t.user_id "
					. " LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = t.subscription_id "
					. " WHERE t.ads_type_config = 2 "
					//. " AND cb.expiry_date >= '".date("Y-m-d H:i:s", time() )."' "
					. " AND cb.status = 'A' "
					. " AND t.property_id = '$id' "
					. " ORDER BY RAND() "
					. " LIMIT 3 ";
				
			$db->setQuery( $query ) ;
			$rows = $db->loadAssocList();
			
		return $rows;
    }

}