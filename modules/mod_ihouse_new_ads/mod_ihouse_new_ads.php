<?php

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// Include the syndicate functions only once
require_once( dirname(__FILE__).DS.'helper.php' );

$layout = $params->get('layout', 'default');
$path = JModuleHelper::getLayoutPath('mod_ihouse_new_ads', $layout);

$db =& JFactory::getDBO();
	
/*$rows 			= modiHouseSearchHelper::getSearch($params);*/
$rows				= modiHouseNewAdsHelper::getNewCondosForSale();



if (file_exists($path)) {
	require($path);
}