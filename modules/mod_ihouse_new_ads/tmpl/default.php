<?php // no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<div id="module-padding9" style="width:100%;">
            <div id="module-pos1" style="width:100%;">
            	<div id="small-module-border" style="width:100%;">
                	<div id="small-module" style="width:100%;">
                    	<div id="module-headerpos">
                            <div id="module-title"><span class="chinese_text1">新公寓出售</span>&nbsp;<i>New Condo For Sales</i></div>
                        </div>
                    </div>
                   
                    <div id="small-module-content" style="width:100%;">
                    	<div id="module-content-padding" style="width:90%;padding-top:0;padding-left:10px;">
                        	<?php if(empty($rows)) : ?>
                            <div class="content-border" style="width:100%;float:left;border-bottom:1px dotted #ccc;">
                             	<div style="float:left;padding:10px 0 10px 0;width:100%;">
                            	No Ads
                                </div>
                            </div>   
                            <?php else : ?>
                        	<?php foreach($rows as $row) : ?>
                            <div style="clear:both"></div>
                            <div class="content-border" style="width:100%;float:left;border-bottom:1px dotted #ccc;">
                                <div style="float:left;padding:10px 0 10px 0;width:100%;">
                                <table width="100%" cellspacing=0 cellpadding=0 >
                                <tr style="vertical-align:top;">
                                	<td width="30%">
                                     <?php 
		$query = " SELECT ads_id, sess_id, name FROM #__ihouse_ads_image WHERE ads_id = '".$row->id."' AND is_primary = 1 ";
			$db->setQuery( $query );
			$tmps = $db->loadObject();
		
		if(empty($tmps)) {
			$query = " SELECT image FROM #__ihouse_ads_image2 WHERE ads_id = '".$row->id."' ORDER BY RAND() LIMIT 1 ";
			$db->setQuery( $query );
			$ihouse_img = $db->loadResult();
			
			if(empty($ihouse_img)) {
				$ihouse_img = JRoute::_('templates/main/images/thumb-noimage.jpg');
			}else{
				$ihouse_img = 'image.php?size=156&type=3&path='.$row->postcode.'/images/'.$ihouse_img;
			}
		}		
	?>
	<div class="adsimgbg">
      <a href="<?php echo JRoute::_('index.php?option=com_ads&view=adsdetails&id='.$row->id); ?>">
    <?php if(!empty($tmps->name)) : ?>
    
     <img src="<?php echo JRoute::_('images/ihouse/ads_images/'.$tmps->sess_id.'/med-'.$tmps->name) ?>" width="156px" height="117px" />
    
	<?php elseif(!empty($ihouse_img)) : ?>
    
    <img style="border:none;" src="<?php echo $ihouse_img ?>" />
	
	<?php else : ?>
    
    <img width="156px" height="129px" src="<?php echo JRoute::_('templates/main/images/thumb-noimage.jpg') ?>"/>
    
    <?php endif; ?>
    </a>
	</div>
                                    </td>
                                    <td width="70%"> 
                                    <div id="tabtext-cont1">
                                    	<div id="tabtext-padding1"> <a href="<?php echo JRoute::_('index.php?option=com_ads&view=adsdetails&id='.$row->id); ?>"><?php echo $row->ad_title ?></a></div>
                                       <div class="color-text1">地址: <?php echo ($row->street_name)?$row->street_name:'N/A'; ?><br />户型：<?php echo $row->no_of_room ?>房<?php echo $row->no_of_hall ?>厅<br>楼层：<?php echo ($row->floor_level)?$row->floor_level.'层':'N/A'; ?><br>价格：<?php echo ($row->ask_price)?'S$ '.number_format($row->ask_price):'N/A'; ?></div>
                                    </div>
                                	<br />
                                    
                                	</td>
                                </tr>
                                </table>
                                </div>
                            </div>
                            <?php endforeach; ?> 
                            <?php endif; ?>
                            <div style="clear:both"></div>
                            <div style="float:right;margin-top:5px;"><a href="<?php echo JRoute::_('index.php?option=com_ihouse&adtype=7&task=adtypelisting')?>" style="color:#ff8200;text-decoration:none;">显示更多 ></a></div>
                        </div>
                    </div>
                </div>
            </div>
            </div>      