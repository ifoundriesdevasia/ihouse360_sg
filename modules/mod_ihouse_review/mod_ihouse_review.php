<?php

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// Include the syndicate functions only once
require_once( dirname(__FILE__).DS.'helper.php' );

$layout = $params->get('layout', 'default');
$path = JModuleHelper::getLayoutPath('mod_ihouse_review', $layout);

//$rows 			= modiHouseSearchHelper::getSearch($params);


if (file_exists($path)) {
	require($path);
}