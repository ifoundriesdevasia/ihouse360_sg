<?php

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// Include the syndicate functions only once
require_once( dirname(__FILE__).DS.'helper.php' );

$layout = $params->get('layout', 'default');
$path = JModuleHelper::getLayoutPath('mod_ihouse_mapsearch', $layout);


$districts		= modiHouseMapSearchHelper::getDistrictListHTML();
$stations 		= modiHouseMapSearchHelper::mrt_listHTML();
$schools 		= modiHouseMapSearchHelper::school_listHTML();


//$price_range	= modiHouseMapSearchHelper::getPriceRangeListHTML();
//$room_size	= modiHouseMapSearchHelper::getRoomSizeListHTML();
//$bedroom		= modiHouseMapSearchHelper::getBedroomListHTML();

if (file_exists($path)) {
	require($path);
}