<?php // no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<?php JHTML::stylesheet('jquery-ui-1.7.1.custom.css','modules/mod_ihouse_search/css/', array('media'=>'all')); ?>
<?php JHTML::_('script', 'custom.js', 'modules/mod_ihouse_mapsearch/js/'); ?>

<?php
	$type = JRequest::getVar('type',''); 
	$inputs = '请输入华文或英文地名、邮编等等。'; 
?>
<script type="text/javascript">

function repositionDistrict() {
	$('ajax-loader-img').setStyle('display','');
	
	var district 	= 	$('district');
	var school		= 	$('school_name');
	var schooltype	=	$('school_type');
	var station		= 	$('station_name');
	var stationtype	= 	$('station_type');
	var keyword		=	$('keyword_input');
	
	schooltype.setProperty('value', '');
	stationtype.setProperty('value', '');
	
	school.setHTML('');
	station.setHTML('');
	
	val = district.getProperty('value');
	
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();
	var url = 'index.php?option=com_ihouse&task=district_reposition_map&r=' + unixtime_ms;
	
	var req = new Ajax(url, {
	   			data		: 	
							{	
								'val'		:	val
							},
				method		: "get",
		    	onSuccess	: function(data) {
					var obj1 		= 	Json.evaluate(data);
					
					var lat			=	obj1.lat;
					var lng			=	obj1.lng;
					
					var latlng = new google.maps.LatLng(lat,lng);
					map.setCenter(latlng);
					map.setZoom(14);
				},
				evalScripts: true
	}).request();	
	
	$('ajax-loader-img').setStyle('display','none');
}
/*
function repositionGoogleMap(opt) {
	
	$('ajax-loader-img').setStyle('display','');
	
	var district	=	$('district');
	var school		= 	$('school_name');
	var schooltype	=	$('school_type');
	var station		= 	$('station_name');
	var stationtype =	$('station_type');
	var keyword		=	$('keyword_input');
	
	var val			=	'';
	
	switch(opt) {
		case 'school'	:
			district.setProperty('value', '');
			station.setHTML('');
			stationtype.setProperty('value', '');
			val = school.getProperty('value');
			break;
		case 'station'	:
			school.setHTML('');
			schooltype.setProperty('value', '');
			district.setProperty('value', '');
			val = station.getProperty('value');
			break;
	}

	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();
	var url = '<?php //echo JRoute::_("index.php?option=com_ihouse&task=reposition_maps"); ?>&amp;r=' + unixtime_ms;
	
	var req = new Ajax(url, {
	   			data		: 	
							{
								'opt'		:	opt,	
								'val'		:	val
							},
				method		: "get",
		    	onSuccess	: function(data) {
					
					var obj1 		= Json.evaluate(data);
					
					var address		= obj1.address;
					var postcode 	= obj1.postcode;		
					var name_en 	= obj1.name_en;
					var name_ch 	= obj1.name_ch;
					
					if(postcode) {
						getLatLng(opt, address, postcode,name_ch,name_en);
					}
					
				},
				evalScripts: true
	}).request();	
	
	$('ajax-loader-img').setStyle('display','none');
}
*/

function submitGoogleMapForm(mode) {
	$('ajax-loader-img').setStyle('display','');
	
	var district 	= 	$('district');
	var school		= 	$('school_name');
	var station		= 	$('station_name');
	var schooltype	=	$('school_type');
	var stationtype =	$('station_type');
	var keyword		=	$('keyword_input');

	switch(mode) {
		case 'keyword' :
			station.setHTML('');
			stationtype.setProperty('value', '');
			school.setHTML('');
			schooltype.setProperty('value', '');
			district.setProperty('value', '');
			break;
		default :
			break;
	}
	
	$('mapsearchForm').send({
		onComplete	: function(data) {
			initialize();
			var obj1 			= Json.evaluate(data);
			$('ajax-loader-img').setStyle('display','none');
		}
	});
					
}

function searchString(arrayToSearch, stringToSearch) {
    //arrayToSearch.sort();

    for (var i = 0; i < arrayToSearch.length; i++) {
        // need to use a double equals sign "==" to test for equality
        if (arrayToSearch[i] == stringToSearch)
            return true;
     }
    return false;
}

function showThings(type) {
	var r = $('check_' + type).getProperty('value');
	if(r == type) {
		$('check_' + type).setProperty('value', '');
		showOverlaysCustom(type);
	} else {
		$('check_' + type).setProperty('value', type);
		clearOverlaysCustom(type);
	}
}

function mapsearchInput(value, info) {
	if(value == '') {
		$('keyword_input').setProperty('value', info);
		$('keyword_input').setStyle('color', '#ccc');
	} else if(value == info) {
		$('keyword_input').setProperty('value', '');
		$('keyword_input').setStyle('color', '#000');
	} 
}

function propertyTypeMode(value,type) {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();
	var url = 'index.php?option=com_ihouse&task=mapsearch_Mode&r=' + unixtime_ms;
	
	var req = new Ajax(url, {
	   			data		: 	
							{
								'propertytype'		:	value,
								'type'				:	type
							},
				method		: "get",
		    	onSuccess	: function(data) {
					
					var obj1 		= 	Json.evaluate(data);
					var priceHTML	=	obj1.price_html;
					var bedroomHTML =	obj1.bedroom_html;
					
					$('ajax-mapsearch-getprice').setHTML(priceHTML);
					$('ajax-mapsearch-getbedroom').setHTML(bedroomHTML);
					$('ajax-mapsearch-getflattype').setHTML('');
				},
				evalScripts: true
	}).request();	
	
}

window.addEvent('domready', function(){
	/*$('check_mrt').setProperty('checked','');*/
	$('check_school').setProperty('checked', '');
	propertyTypeMode('公寓','<?php echo JRequest::getVar('type') ?>');
});

/*********************/
/**GOOGLE MAP SEARCH */
/*********************/
function schoolLoadingGoogleMap() {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();
	
	var school = 'index.php?option=com_ihouse&task=schoolsearch&r=' + unixtime_ms;
	
	var reqs1 = new Ajax(school, {
	   					data		: 	
									{
										/*'lat1'		: lat1,
										'lat2'		: lat2,
										'lng1'		: lng1,
										'lng2'		: lng2*/
									},
						method		: "get",
		    			onSuccess	: function(data) {
							var obj1 		= Json.evaluate(data);
							var sch_postcode	= obj1.postcode;
							var sch_name_en		= obj1.name_en;
							var sch_name_ch 	= obj1.name_ch;
							var sch_lats		= obj1.lats;
							var sch_lngs		= obj1.lngs;
							var sch_address		= obj1.address;
							
							
							if(!sch_postcode) {
								
							}
							else 
							{
								total_data 			= 	sch_postcode.length;
								for(i in sch_postcode) {
									var postcode 	= sch_postcode[i];
									var name_en		= sch_name_en[i];
									var name_ch		= sch_name_ch[i];
									var lats		= sch_lats[i];
									var lngs		= sch_lngs[i];
									var address		= sch_address[i];
									customGoogleMap('school', name_en, name_ch ,address, postcode,lats, lngs, i);
								}
							}
							$('maploader').setStyle('display', 'none');
		   				},
						evalScripts : true
					}).request();
}


function panningGoogleMap(lat1, lat2, lng1, lng2) {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();
	
	var url = 'index.php?option=com_ihouse&task=mapsearch1&r=' + unixtime_ms;
	//var mrt = 'index.php?option=com_ihouse&task=mrtsearch&r=' + unixtime_ms;
	
	
	var bedroom1		=	$('bedroom');
	if(bedroom1) {
		bedroom 	= 	bedroom1.getProperty('value');
	}
	var price		= 	$('price').getProperty('value');
	var price_max	=	$('price_max').getProperty('value');
	var budget		= 	$('budget').getProperty('value');
	var amount		=	$('amount').getProperty('value');
	var keyword		=	$('keyword_input').getProperty('value');	
	var proptype	=	$('propertytype').getProperty('value');	
	
	if(keyword == '<?php echo $inputs ?>') {
		keyword = '';
	}
	
	$('maploader').setStyle('display', '');
			
	var req = new Ajax(url, {
	   	data		: 	
					{
						'lat1'		: lat1,
						'lat2'		: lat2,
						'lng1'		: lng1,
						'lng2'		: lng2,
						'type'		: '<?php echo JRequest::getVar('type') ?>',
						'bedroom'	: bedroom,
						'price'		: price,
						'price_max'	: price_max,
						'budget'	: budget,
						'amount'	: amount,
						'keyword'	: keyword,
						'proptype'	: proptype
					},
					method		: "get",
		    		onSuccess	: function(data) {
						var obj1 		= Json.evaluate(data);
						myArray 		= obj1.postcode;
						myArray2 		= obj1.title_ch;
						myArray3 		= obj1.psm_price;
						myArray4		= obj1.lats;
						myArray5		= obj1.lngs;
						myArray6		= obj1.property_id;
						myArray7 		= obj1.title_en;
						
						$('load_markers_next').setHTML('');
						$('load_markers_prev').setHTML('');
						
						if(!myArray) {
							$('totalmapproperty').setHTML('在这里找到了0个楼盘');
							
						}
						else 
						{
							total_data 		= 	myArray.length;
							var limitation	=	30;
							$('totalmapproperty').setHTML('在这里找到了'+total_data+'个楼盘');
							
							if(total_data < limitation) {
								loadMarkers(0, total_data, '');
							} else {
								loadMarkers(0,limitation, '');
							}
								
						}
						$('maploader').setStyle('display', 'none');
		   			},
					evalScripts : true
				}).request();	
					
				/*var reqs = new Ajax(mrt, {
	   					data		: 	
									{
										'lat1'		: lat1,
										'lat2'		: lat2,
										'lng1'		: lng1,
										'lng2'		: lng2
									},
						method		: "get",
		    			onSuccess	: function(data) {
							var obj1 		= Json.evaluate(data);
							
							var mrt_postcode	= obj1.postcode;
							var mrt_name_en		= obj1.name_en;
							var mrt_name_ch 	= obj1.name_ch;
							var mrt_lats		= obj1.lats;
							var mrt_lngs		= obj1.lngs;
							var mrt_address		= obj1.address;
							
							if(!mrt_postcode) {

							}
							else 
							{
								
								for(i in mrt_postcode) {
									var postcode 	= mrt_postcode[i];
									var name_en		= mrt_name_en[i];
									var name_ch		= mrt_name_ch[i];
									var lats		= mrt_lats[i];
									var lngs		= mrt_lngs[i];
									var address		= mrt_address[i];
									
									customGoogleMap('mrt', name_en, name_ch ,address, postcode,lats, lngs, i);
								}
								
							}
							
							$('maploader').setStyle('display', 'none');
							
		   				},
						evalScripts : true
					}).request();			
				*/	
				
}

function customGoogleMap(type, name_en, name_ch ,address, postcode,lats, lngs, i) {
					
					var icon_img;
					
					if(type == 'mrt') {
						icon_img = '<?php echo JURI::root().'templates/mapsearch/images/mrt.png' ?>';
					} else if(type == 'school') {
						icon_img = '<?php echo JURI::root().'templates/mapsearch/images/school.png' ?>';
					}
					
					var tt	=	name_en + ' ' + name_ch;
					
        			var marker = new google.maps.Marker({
            								map			: map, 
            								position		: new google.maps.LatLng( lats , lngs),
											title 		: tt,
											icon 			: icon_img
        			});
							
					var contentString 		= 	address + ' ' + postcode;
	
					google.maps.event.addListener(marker, 'click', function() {		
						infoWindow.close();													
						infoWindow.setContent(contentString);	
						infoWindow.setPosition(marker.getPosition());
  						infoWindow.open(map,marker);
					});
					
					
					marker.setMap(null);
					
					var tmps = $('check_' + type).getProperty('value');
					
					if(tmps == '') {
						marker.setMap(map);
					}
					
					if(type == 'mrt') { 	
						mrtArray[i] = marker;
					} else if (type == 'school') {
						schoolArray[i] = marker;
					}
					
				}



function geocodeGoogleMap(postcode, property_id, title_ch,title_en,psm_price, lats, lngs,mode, i) {
					
					var icon_img = '<?php echo JURI::root().'templates/mapsearch/images/building2.png' ?>';
					
					var texts_title = title_ch + ' ';
					
					if(title_en) {
						texts_title = texts_title + title_en
					}
					
        			var marker = new google.maps.Marker({
            								position: new google.maps.LatLng( lats , lngs),
											map: map, 
											title : texts_title,
											visible: true,
											icon : icon_img
        			});
							
					var myOptions = {
                			disableAutoPan: false
                			,maxWidth: 0
                			,pixelOffset: new google.maps.Size(0, -200)
                			,zIndex: null
                			,boxStyle: { 
                  				background: 'url(\'tipbox.gif\') no-repeat'
                  				,opacity: 1
                  				,width: '600px'
                 			}
                			,closeBoxMargin: '10px 2px 2px 2px'
                			,closeBoxURL: '<?php echo JURI::root().'templates/mapsearch/images/closetip.png' ?>'
                			,infoBoxClearance: new google.maps.Size(1, 1)
                			,isHidden: false
                			,pane: 'floatPane'
                			,enableEventPropagation: false
       				};
					
					ib = new InfoBox(myOptions);
					
					google.maps.event.addListener(marker, 'click', function() {		
						//alert(property_id);													
						var contentString = googlePostCodeAds(postcode,property_id,title_ch,title_en, psm_price);
						
						/* AJAX */
						/* maps_ads_page( page, property_id, fp_ticked, bedroom_size, price, price_max, ... ) */
						
						var bedroom 	= 	$('bedroom').getProperty('value');
						var price		= 	$('price').getProperty('value');
						var price_max	=	$('price_max').getProperty('value');
						var budget		= 	$('budget').getProperty('value');
						var amount		=	$('amount').getProperty('value');
						var keyword		=	$('keyword_input').getProperty('value');	
						var proptype	=	$('propertytype').getProperty('value');	
						
						//alert(price + ' ' + price_max);
						map_ads_page(0, property_id, 0,bedroom,price,price_max, keyword,proptype,budget,amount);
						
						ib.close();
						ib.setContent(contentString);
						ib.setOptions(myOptions);
						ib.open(map, marker);
						
					});
					markersArray[i] = marker;
				}
				
				function addCommasJavascript(nStr)
				{
					nStr += '';
					x = nStr.split('.');
					x1 = x[0];
					x2 = x.length > 1 ? '.' + x[1] : '';
					var rgx = /(\d+)(\d{3})/;
					while (rgx.test(x1)) {
						x1 = x1.replace(rgx, '$1' + ',' + '$2');
					}
					return x1 + x2;
				}

				function googlePostCodeAds(postcode,property_id,title_ch,title_en, psm_price) {
					var html 		= '';
					var total_ads 	= 0;
					
					var psm_price_1	= '-';
					var psf_price_1 = '-';
					if(psm_price) { 
						psf_price = parseInt(parseInt(psm_price.replace(/\,/g,'')) / 10.764);
						psm_price_1 = 'S$ ' + psm_price + '\/平方米'; 
						psf_price_1 = 'S$ ' + addCommasJavascript(psf_price) + '\/平方尺';
					}
					
					
					html = html + '<div class=\"infobox-tmpl\">';
					/* START - TITLE INFO */
					html = html + '	<div style=\"\"><span class=\"static-text\">'+ title_ch +'&nbsp;</span><span style=\"font-size:15px;\">'+ title_en +'</span></div>';
					html = html + '	<div style=\"\">';
					html = html + '		<div style=\"float:left;\">均价：</div>';
					html = html + '		<div style=\"float:left\">'+ psm_price_1 +'<br />'+ psf_price_1 +'</div>';
					html = html + '		<div style=\"clear:both;\"></div>';
					html = html + ' </div>';
					/* END  - TITLE INFO */
					html = html + '<div style=\"width:98%;\">';
					html = html + '<div style=\"float:left;width:98%;\">'; 
					
					/* START - BUTTONS */
	
					html = html + '	<div style=\"float:left;margin:10px 0 10px 0;\">';
	
					html = html + '	<div id=\"gmap_btn1\"><div style=\"margin:3px 5px;text-align:center;\"><a href=\"index.php?option=com_ihouse&view=ihouse&layout=property_detail&id=' + property_id + '&postcode=' + postcode + '\" target="_blank" >楼盘详情</a></div></div>';
					
					html = html + '	<div id=\"ads-mapsearch-loader-' + property_id + '\" class=\"ads-mapsearch-loader\" style=\"\"><img src=\"<?php echo JURI::root().'templates/mapsearch/images/ajax-loader.gif' ?>\" /></div>';
					
					html = html + '	</div>';
					
					/* END - BUTTONS */
					
					html = html + '</div>';
					
					html = html + '<div style=\"clear:both\"></div>';
					
					/* START - MENU - FILTERS*/ 
					
					html = html + '<div class=\"gmap_bg_filter\">';
					html = html + '<div class=\"infobox-menu1-box\">';
					html = html + '共有<span id=\"ads-total-search-filter-'+ property_id +'\">'+ total_ads +'</span>套房源&nbsp&nbsp;';
					/*html = html + '<input type=\"checkbox\" id=\"with_fp\" onchange=\"javascript:map_ads_page(0, \''+property_id+'\', this.value);\" value=\"0\">显示多图房源&nbsp&nbsp;';*/
					html = html + '</div>'
					html = html + '<div id=\"ads-price-filter-'+ property_id +'\" class=\"infobox-ads-listhtml\">';
					html = html + '</div>';
					html = html + '<div id=\"ads-bedroom-filter-'+ property_id +'\" class=\"infobox-ads-listhtml\">';
					html = html + '</div>';
					
					html = html + '&nbsp&nbsp;';
					;
					html = html + '</div>';
					
					html = html + '<div style=\"clear:both\"></div>';
					
					/* START - AD LISTING - AJAX*/
					html = html + '<div id=\"ajax-load-ads-mapsearch-' + property_id + '\" class=\"ads-list-box\">';
					html = html + '</div>';
					
					/* END - AD LISTING */
					html = html + '<div style=\"clear: both;\"></div>';
					
					/* START - MENU - FILTERS*/ 
					html = html + '<div class=\"gmap_bg_filter\">';
					html = html + '<div class=\"infobox-menu1-box\">';
					html = html + '本楼盘置业顾问：<span id="specialist-condo-mapsearch-' + property_id + '">-</span>';
					html = html + '</div>';
					html = html + '</div>';
					
					/* END   - MENU - FILTERS*/ 
					
					html = html + '<div style=\"clear: both;\"></div>';
					
					html = html + '<div class=\"infoBox-pagination\" id=\"infoBox-pagination-'+property_id+'\">';
					html = html + '</div>';
					
					html = html + '<div style=\"clear: both;\"></div>';
					
					html = html + '</div>';
					
					html = html + '</div>'; /* BOX */
					
					return html;
				}
</script>


<form name="mapsearchForm" id="mapsearchForm" action="index.php" method="post">

<div id="mod_mapsearch">
	<div style="margin-top:5px;">
		<input type="text" id="keyword_input" name="mapsearch_input" style="width:400px;color:#ccc;" onKeyDown="if (event.keyCode==13) { submitGoogleMapForm('keyword');return false; } else { return true; }" onfocus="javascript:mapsearchInput(this.value, '<?php echo $inputs; ?>');" onblur="javascript:mapsearchInput(this.value, '<?php echo $inputs; ?>');" value="<?php echo $inputs; ?>" />

		<input type="button" onclick="submitGoogleMapForm('keyword'); return false;" id="keyword_map" class="searchbutton" value="搜索">
        
        <span id="ajax-loader-img" style="display:none;">
        <img src="<?php echo JURI::root().'modules/mod_ihouse_mapsearch/images/ajax-loader.gif'; ?>" />
        </span>
        
        <span id="maploader" style="position:relative;display:none;">
    		<img src="<?php echo JURI::root().'templates/main/images/ajax-loader.gif'; ?>"  />
        	<?php /*  &nbsp;&nbsp;正在搜索，请稍候...  */ ?>
    	</span>
	</div>
	<div style="margin-top: 10px; padding-left: 10px;width:900px;">
    	<div style="float:left;">快速定位</div>
		<div style="float:left;margin-left:10px;"><?php echo $districts ?></div>
		<div style="float:left;margin-left:10px;">
			<?php echo $schools; ?><br />
			<?php echo $stations; ?>
        </div>    
        <div style="clear:both"></div>
        <div style="float:left;margin-top:-20px;">
        	<div style="float:left">
            	<input type="checkbox" id="check_school" onclick="javascript:showThings('school');" value="school" />Show Schools
            </div>
			<!--<div style="float:left;">
            	<input type="checkbox" id="check_mrt" onclick="javascript:showThings('mrt');" value="mrt" />
            </div>
            <div style="float:left;margin-top:3px;">Show MRT</div>
            -->
        </div>
        <div style="clear:both"></div>
	</div>
</div>

<div style="clear:both;"></div>

<div id="searchmenu2">
	<div id="map_wrapper">
    <div id="map_info_ajax">
    	<div style="float:left;">
        	<select id="propertytype" name="propertytype" onchange="javascript:propertyTypeMode(this.value,'<?php echo $type ?>');">
            	<!--<option value="">---</option>-->
                <!--<option value="组屋">组屋</option>-->
                <option value="公寓" selected="selected">公寓</option>
            </select>
    	</div>
        <div style="float:left;">
    
    		<span id="ajax-mapsearch-getprice"></span>
			<span id="ajax-mapsearch-getbedroom"></span>
            <span id="ajax-mapsearch-getflattype"></span>
        
    	</div>
        <div style="float:left;">
        	<input style="color:#fff;background-color:#ff8200;" type="button" value="Go" onclick="javascript:googleIdleSearch(1);" /> 
		</div>
        <div id="load_markers_prev_box" style="float:left;margin:2px 5px;">
        	<a id="load_markers_prev"></a>
        </div>
        <div id="totalmapproperty" style="float:left;margin:2px 5px">
    	</div>
    	<div id="load_markers_prev_next" style="float:left;margin:2px 5px;">
    		<a id="load_markers_next"></a>
    	</div>
    </div>
    
    
    <div style="float:right;margin-right:30px;">
    	<div style="float:left;margin-top:7px;">您的预算</div><!-- ( 新元，总价 ) -->
        <div style="float:left;margin:0 0 0 3px;"><input type="text" name="budget" id="budget" size="8" value=""  maxlength="15" onkeyup="javascript:formatNumber(this.value, 0,'budget');" onkeypress="return isNumberKey(event);" /></div>
    	<div style="float:left;margin-left:5px;">总价偏差幅度(%)&nbsp;&nbsp;</div>
        <div style="float:left;margin-left:10px;">
			<div id="slider"></div>
   			<div style="margin-top:5px;">
                0&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;2&nbsp;&nbsp;&nbsp;&nbsp;3&nbsp;&nbsp;&nbsp;&nbsp;4&nbsp;&nbsp;&nbsp;&nbsp;5&nbsp;&nbsp;&nbsp;&nbsp;6&nbsp;&nbsp;&nbsp;&nbsp;7&nbsp;&nbsp;&nbsp;&nbsp;8&nbsp;&nbsp;&nbsp;&nbsp;9&nbsp;&nbsp;&nbsp;&nbsp;10
            </div>
		</div>
      	<div style="float:left;margin-left:5px;">
        	<input style="color:#fff;background-color:#ff8200;" type="button" value="Go" onclick="javascript:googleIdleSearch(2);" /> 
		</div>
   	</div>
   	 
   
    
	</div><!-- wrapper -->
</div>

<div style="clear:both"></div>

<input type="hidden" id="type" name="type" value="<?php echo JRequest::getCmd('type','all') ?>" />
<input type="hidden" id="amount" name="amount" value="0" />
<input type="hidden" name="option" value="com_ihouse"  />
<input type="hidden" name="task" value="mapsearch"  />
</form>