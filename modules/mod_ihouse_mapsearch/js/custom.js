function propertyClsSearchNav(mode) {
	switch(mode) {
		case 'all':
			$('input_prop_cls').setProperty('value', 'all');
			break;
		case 'new':
			$('input_prop_cls').setProperty('value', 'new');
			break;
		case 'second':
			$('input_prop_cls').setProperty('value', 'second');
			break;
		case 'rent':
			$('input_prop_cls').setProperty('value', 'rent');
			break;	
	}
}

function getListMapSearch(type, value) {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();
	
	var url = 'index.php?option=com_ihouse&amp;task=mapsearchListSearch&amp;r=' + unixtime_ms;	
	var req = new Ajax(url, {
	   			data		: 	
								{
									'type' 	: type,
									'value' : value
								},
				method		: "post",
				onRequest	: function() {
					/*$$('.ajax-loader').setStyle('display', 'none');*/
				},
	    		onComplete	: function(data) {	
					var obj1 		= 	Json.evaluate(data);
					var list_html	=	obj1.list_html;
					
					//var html = '';
					//html = html + '<select onchange="repositionGoogleMap(\'' + type + '\');">';
					//html = html + list_html;
					//html = html + '</select>';
					
					$(type + '_name').setHTML(list_html);
	   			},
				evalScripts: true
		}).request();
}

function repositionGoogleMap(opt, urlss, value) {
	
	$('ajax-loader-img').setStyle('display','');
	
	var district	=	$('district');
	var school		= 	$('school_name');
	var schooltype	=	$('school_type');
	var station		= 	$('station_name');
	var stationtype =	$('station_type');
	var keyword		=	$('keyword_input');
	
	
	switch(opt) {
		case 'school'	:
			district.setProperty('value', '');
			station.setHTML('');
			stationtype.setProperty('value', '');
			break;
		case 'station'	:
			school.setHTML('');
			schooltype.setProperty('value', '');
			district.setProperty('value', '');
			break;
	}

	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();
	var url = urlss + '&r=' + unixtime_ms;
	var req = new Ajax(url, {
	   			data		: 	
							{
								'opt'		:	opt,	
								'val'		:	value
							},
				method		: "get",
		    	onSuccess	: function(data) {
					var obj1 		= Json.evaluate(data);
					
					var address		= obj1.address;
					var postcode 	= obj1.postcode;		
					var name_en 	= obj1.name_en;
					var name_ch 	= obj1.name_ch;
					
					if(postcode) {
						getLatLng(opt, address, postcode,name_ch,name_en);
					}
					
				},
				evalScripts: true
	}).request();	
	
	$('ajax-loader-img').setStyle('display','none');
}