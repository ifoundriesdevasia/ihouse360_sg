﻿<?php
/**
 * Helper class for Slider Gallery Module
 * 
 * @package    Joomla
 * @subpackage Modules
 * @link http://www.yashvyas.in
 * @license        GNU/GPL
 * mod_galleryview is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */
class modiHouseMapSearchHelper
{
    function getSearch( $params )
    {
		/* START - JRequest::getVar() */
		
		/* END - JRequest::getVar() */

		$db		=& 	JFactory::getDBO();

		$query 	= 'SELECT * FROM #__ihouse_property ';
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		return $rows;
    }
	
	function getDistrictListHTML() {
		$db		=& 	JFactory::getDBO();

		$query 	= 'SELECT * FROM #__ihouse_district ORDER BY code ASC ';
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		$html = '';
		
		$html .= '<select name="district" id="district" style="width:200px;" class="searchselect" onchange="javascript:repositionDistrict(); return false;" >';
		$html .= '	<option value="">选择区域</option>';
		foreach($rows as $r) :
			$t = JRequest::getVar('district', '');
		
			$selected = '';
			if($t == $r->code) 
				$selected = 'selected="selected"';
				
			$html .= '	<option value="'.$r->code.'" '.$selected.' >'.$r->code.' '.$r->district_ch.' '.$r->district_en.'</option>';
		endforeach;
		
		$html .= '</select>';
		
		return $html;
	}
	
	function getNearestMRTListHTML() {
		$db		=& 	JFactory::getDBO();

		$query 	= 'SELECT DISTINCT(name_ch), name_en FROM #__ihouse_station ORDER BY name_en ASC ';
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		$html = '';
		
		$html .= '<select name="nearest_station" id="nearest_station" style="width:200px;" class="searchselect" onchange="javascript:repositionGoogleMap(\'station\'); return false;">';
		$html .= '	<option value="">选择地铁</option>';
		foreach($rows as $r) :
			$t = JRequest::getVar('nearest_station', '');
		
			$selected = '';
			if($t == $r->name_ch) 
				$selected = 'selected="selected"';
			
			if($r->name_ch != '')
				$html .= '	<option value="'.$r->name_ch.'" '.$selected.' >'.$r->name_ch.' '.$r->name_en.'</option>';
				
		endforeach;
		
		$html .= '</select>';
		
		return $html;
	}
	
	function getNearestSchoolListHTML() {
		$db		=& 	JFactory::getDBO();

		$where 	= ' WHERE NOT ( type = "primary_top30" OR type = "secondary_top30") '; 
		$query 	= 'SELECT DISTINCT(name_ch), name_en, id FROM #__ihouse_school '
					. $where 
					. " ORDER BY name_en ASC ";
					
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		$html = '';
		
		$html .= '<select name="nearest_school" id="nearest_school" style="width:200px;" class="searchselect" onchange="javascript:repositionGoogleMap(\'school\'); return false;">';
		$html .= '	<option value="">选择学区</option>';
		foreach($rows as $r) :
			$t = JRequest::getVar('nearest_school', '');
		
			$selected = '';
			if($t == $r->id) 
				$selected = 'selected="selected"';
		
			$html .= '	<option value="'.$r->id.'" '.$selected.' >'.$r->name_ch.' '.$r->name_en.'</option>';
		endforeach;
		
		$html .= '</select>';
		
		return $html;
		
	}
	
	function school_listHTML() {
		$html .= '<select name="school_type" id="school_type" style="width:100px;" class="searchselect" onchange="javascript:getListMapSearch(\'school\', this.value);return false;">';
		$html .= '	<option value="">选择学区</option>';
		//$html .= '	<option value="primary">小学</option>';
		//$html .= '	<option value="secondary">中学</option>';
		$html .= '	<option value="primary_top30">小学TOP30</option>';
		$html .= '	<option value="secondary_top30">中学TOP30</option>';
		$html .= '	<option value="jc_polyc">初级学院 / 理工学院</option>';
		$html .= '	<option value="university">大学</option>';
		$html .= '</select>';
		$html .= '<span id="school_name">';
		//$html .= '<select onchange="javascript:repositionGoogleMap(\'school\');return false;"></select>';
		$html .= '</span>';
		return $html;
	}
	
	function mrt_listHTML() {
		$html .= '<select name="station_type" id="station_type" style="width:100px;" class="searchselect" onchange="javascript:getListMapSearch(\'station\',this.value); return false;">';
		$html .= '	<option value="">选择地铁</option>';
		$html .= '	<option value="CC">CC 环线</option>';
		$html .= '	<option value="CG">CG 樟宜延伸线</option>';
		$html .= '	<option value="DT">DT 市区线</option>';
		$html .= '	<option value="EW">EW 东西线</option>';
		$html .= '	<option value="NE">NE 东北线</option>';
		$html .= '	<option value="NS">NS 南北线</option>';
		$html .= '	<option value="BP">BP 武吉班让轻轨</option>';
		$html .= '	<option value="PE">PE 榜鹅东轻轨</option>';
		$html .= '	<option value="PW">PW 榜鹅西轻轨</option>';
		$html .= '	<option value="SE">SE 盛港东轻轨</option>';
		$html .= '	<option value="SW">SW 盛港西轻轨</option>';
		$html .= '</select>';
		$html .= '<span id="station_name">';
		//$html .= '<select onchange="javascript:repositionGoogleMap(\'station\');return false;" ></select>';
		$html .= '</span>';
		
		return $html;
		
	}
	
}