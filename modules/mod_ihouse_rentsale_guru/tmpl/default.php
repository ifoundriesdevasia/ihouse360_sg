<?php // no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<?php JHTML::_('script', 'contacts.js', 'components/com_ihouse/assets/'); ?>   



<div id="static-text1" style="border-bottom:2px solid #062284;float:none;">
<?php if($row->user_category == 'Agent') : ?>
	经纪&nbsp;<span style="font-size:13px;"><i><strong>Agent</strong></i></span>
<?php endif; ?>

<?php if($row->user_category == 'Owner') : ?>
	屋主&nbsp;<span style="font-size:13px;"><i><strong>Owner</strong></i></span>
<?php endif; ?>
    
</div>

<div style="clear:both"></div>

<div id="sub-small-component-content" style="width:100%;">
	<div class="agent_list_profile">
        <table width="100%" cellpadding=0 cellspacing=0>
            <tr>
                <td width="50%">
                    <div style="text-align:center;">
          				<?php
		  					if(empty($row->user_image)) {
								$user_image = JRoute::_('templates/main/images/no_avatar.jpg');
							} else {
								$user_image = 'image.php?size=90&type=1&path='.$row->user_image;
							}
		  				?>
                        <a style="text-decoration:none;color:#fff;" href="<?php echo JRoute::_('index.php?option=com_user&view=agent_profile&id='.$row->id.'&Itemid=136'); ?>">
            				<img style="border:none;" src="<?php echo $user_image; ?>" />
                		</a>
                     </div>
                     <div style="clear:both"></div>
                     <div style="text-align:center;">
						<input type="button" onclick="javascript:emailFormPopup('<?php echo $row->email ?>','<?php echo $row->ads_id ?>','<?php echo $row->id ?>');" id="agent_sub" name="agent_sub" class="contactbutton" value="联系我">
					</div>   
                 </td>
                 <td width="50%">         
              	 <?php 
			
				if(empty($row->company_logo)) {
					$comp_logo = JRoute::_('templates/main/images/thumb-nologo.jpg');
				}else{
					$comp_logo = 'image.php?size=120&type=1&path='.$row->company_logo;
				}
			
				?>
            
            	<?php if($row->user_category == 'Agent'): ?>
            		<img src="<?php echo $comp_logo ?>" />
           	 	<?php endif; ?> 
                 </td>
            </tr>              
       </table>
       <div class="thisspecialistprofile">
    			<div class="l">姓名</div>
        		<div class="m">:</div>
        		<div class="r">	
				<?php echo ($row->chinese_name)?$row->chinese_name:'N/A' ?>
            	</div>
            <div style="clear:both"></div>
       </div>
       <div class="thisspecialistprofile">
    			<div class="l">Name</div>
        		<div class="m">:</div>
        		<div class="r"><?php echo ($row->name)?$row->name:'N/A' ?>
                </div>
            <div style="clear:both">
        </div>	
        <div class="thisspecialistprofile">
    			<div class="l">电话</div>
        		<div class="m">:</div>
        		<div class="r"><?php echo ($row->mobile_contact)?$row->mobile_contact:'N/A' ?></div>
            <div style="clear:both"></div>
        </div>    
  			<?php if($row->user_category == 'Agent') : ?>
        <div class="thisspecialistprofile">
    			<div class="l">经纪证号</div>
        		<div class="m">:</div>
        		<div class="r"> 
				<?php if(!empty($row->cea_reg_no)) : ?>
                	<?php if(preg_match('/^L/', $row->cea_reg_no )): ?>
                		<a href="http://www.cea.gov.sg/cea/app/newimplpublicregister/view.jspa?licNo=<?php echo $row->cea_reg_no ?>" target="_blank">
                    <?php elseif(preg_match('/^R/', $row->cea_reg_no )) : ?>    
                    	<a href='http://www.cea.gov.sg/cea/app/newimplpublicregister/view.jspa?licNo=""&regNo=<?php echo $row->cea_reg_no ?>' target="_blank">
                    <?php endif; ?>   
						<?php echo $row->cea_reg_no ?>
                    </a>
                <?php else : ?>
                	N/A    
                <?php endif; ?></div>
            <div style="clear:both"></div>
        </div>  
            <?php endif; ?>                   
    </div>                  
</div>
<div style="clear:both;"></div> 



<!-- POPUP -->
<div id="popupForm" style="display:none;">
</div>
