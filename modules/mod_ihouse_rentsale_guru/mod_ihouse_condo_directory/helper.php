﻿<?php
/**
 * Helper class for Slider Gallery Module
 * 
 * @package    Joomla
 * @subpackage Modules
 * @link http://www.yashvyas.in
 * @license        GNU/GPL
 * mod_galleryview is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */
class modiHouseCondoDirectoryHelper
{
    function getSearch( $params )
    {
		/* START - JRequest::getVar() */
		
		/* END - JRequest::getVar() */

		$db		=& 	JFactory::getDBO();

		$query 	= 'SELECT * FROM #__ihouse_property ';
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		return $rows;
    }
	

	function getTypeYearBuiltListHTML() {

		$year_built_s			=	JRequest::getCmd('year_built', '');

		$html = '';
		
		$html .= '<select name="year_built" class="searchselect">';
		$html .= '	<option value="">选择TOP年份</option>';
		
		$selected = 'selected="selected"';
		
		$html .= '	<option value="1" '.(($year_built_s == 1)? $selected : '').' >1990年以前</option>';
		$html .= '	<option value="2" '.(($year_built_s == 2)? $selected : '').' >1990-1999年之间</option>';
		$html .= '	<option value="3" '.(($year_built_s == 3)? $selected : '').' >2000-2005年之间</option>';
		$html .= '	<option value="4" '.(($year_built_s == 4)? $selected : '').' >2006-2010年之间</option>';
		$html .= '	<option value="5" '.(($year_built_s == 5)? $selected : '').' >2010年以后</option>';
		$html .= '</select>';
		
		return $html;
		
	}
	
	function getMinPriceListHTML() {
		$min_price_s			=	JRequest::getCmd('min_price', '');

		$html = '';
		
		$html .= '<select name="min_price" class="searchselect">';
		$html .= '	<option value="">价格范围</option>';
		
		$selected = 'selected="selected"';
		
		$html .= '	<option value="1" '.(($min_price_s == 1)? $selected : '').' >每平米 SGD 8000 新元以内</option>';
		$html .= '	<option value="2" '.(($min_price_s == 2)? $selected : '').' >SGD 8001 - 12000之间</option>';
		$html .= '	<option value="3" '.(($min_price_s == 3)? $selected : '').' >SGD 12001 - 20000之间</option>';
		$html .= '	<option value="4" '.(($min_price_s == 4)? $selected : '').' >SGD 20001 - 30000之间</option>';
		$html .= '	<option value="5" '.(($min_price_s == 5)? $selected : '').' >SGD 30000以上</option>';
		$html .= '</select>';
		
		return $html;
	}
	
	function getDistrictListHTML() {
		
		$district_s				=	JRequest::getCmd('district_id', '');

		$db		=& 	JFactory::getDBO();
		
		$query 	= 'SELECT * FROM #__ihouse_district ';
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		$html = '';
		
		$html .= '<select name="district_id" class="searchselect">';
		$html .= '	<option value="">选择区域</option>';
		foreach($rows as $r):
			$selected = '';
			if($district_s == $r->code) 
				$selected = 'selected="selected"';
				
			$html .= '	<option value="'.$r->code.'" '.$selected.' >'.$r->code.' '.$r->district_ch.'</option>';
		endforeach;
		
		$html .= '</select>';
		
		return $html;
	}
	
	function getLeaseListHTML() {
		$lease_s				=	JRequest::getCmd('lease', '');
		
		$html = '';
		
		$html .= '<select name="lease" class="searchselect">';
		$html .= '	<option value="">选择地契</option>';
		$html .= '	<option value="99" '.(($lease_s == '99')?'selected="selected"':'').'>99</option>';
		$html .= '	<option value="999" '.(($lease_s == '999')?'selected="selected"':'').'>999</option>';
		$html .= '	<option value="永久" '.(($lease_s == '永久')?'selected="selected"':'').'>永久</option>';
		$html .= '</select>';
		
		return $html;
	}
	function getPropertyType($type = 'checkbox') {
		$db		=& 	JFactory::getDBO();

		$exist = array('Condo', 'HDB');
		
		$where = 
		
		$query 	= 'SELECT * FROM #__ihouse_property_type'
					. ' WHERE LOWER(name_en) = "hdb" OR LOWER(name_en) = "condo" ';
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		$html = '';
		
		
		
		foreach($rows as $r):
			$t = JRequest::getVar(strtolower($r->name_en), '');
		
			$checked = '';
			if($t == $r->name_ch) 
				$checked = 'checked="checked"';
		
			$html .= '<input type="checkbox" value="'.$r->name_ch.'" name="'.strtolower($r->name_en).'" '.$checked.' />'.$r->name_en;
		endforeach;
		
		return $html;
	}
	
}