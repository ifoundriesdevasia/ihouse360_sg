﻿<?php // no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<div id="module-padding">
    	<div id="main-search-cont">
    	<div id="main-search-bg">
        	<div id="main-search-content">
            
            	<div id="main-search-header-text">公寓目录</div>
                
                <div id="main-search-pos">
               	
                    <!--LEFT-->
                    
                    <div id="main-search-left">
                        <div id="main-search-left-header">
                            <div id="main-search-header-pos">地区</div>
                        </div>
                        <form name="condo_directory_form2" action="<?php echo JRoute::_('index.php?layout=condo_directory2&view=directory&option=com_ihouse') ?>" method="post">
                        <div id="main-search-left-content">
                        	<div id="main-search-padding">
                            	<div id="main-search-padding1">
                                <?php echo $districts ?>&nbsp;&nbsp;
                                <?php echo $lease ?>
                                </div>
                                <div id="main-search-padding2">
                                <?php echo $year_built	?>&nbsp;&nbsp;
                                <?php //echo $min_price ?>
                                </div>
                                <input type="button" value="搜索" class="searchbutton" onclick="document.condo_directory_form2.submit()" />
                            </div>
                        </div>
                        <input type="hidden" name="option" value="com_ihouse" />
                        <input type="hidden" name="view" value="directory" />
                        <input type="hidden" name="layout" value="condo_directory2" />
                        <input type="hidden" name="type" value="<?php echo $type ?>" />  
                        </form>
                    </div>
                    
                    <!--END LEFT-->
                    
                    <!--RIGHT-->
                    
                    <div id="main-search-right">
                    	<div id="main-search-right-header">
                        	<div id="main-search-header-pos">项目名</div>
                        </div>
                        
                        <div id="main-search-right-content">
                        	<div id="main-search-padding3">
                        		<div id="directory-search-alpha1"><a href="<?php echo JRoute::_('index.php?option=com_ihouse&view=directory&layout=condo_directory2&type=num') ?>">[0-9]</a></div>
                                
								
								<?php foreach(range('A','Z') as $i) 
										echo '<div id="directory-search-alpha1"><a href="'.JRoute::_('index.php?option=com_ihouse&view=directory&layout=condo_directory2&type='.$i).'">['.$i.']</a></div>'; 
								?>

                            </div>
                        </div>
                    </div>
                    
                    <!--END RIGHT-->
                	</div><!--main-search-pos-->
                
            	</div><!--main-search-content-->
        	</div>
        </div>
        </div>
        <!--End Search Module-->