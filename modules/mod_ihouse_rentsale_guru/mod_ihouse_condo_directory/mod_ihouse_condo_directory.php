<?php

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// Include the syndicate functions only once
require_once( dirname(__FILE__).DS.'helper.php' );

$layout = $params->get('layout', 'default');
$path = JModuleHelper::getLayoutPath('mod_ihouse_condo_directory', $layout);

/*$rows 			= modiHouseSearchHelper::getSearch($params);*/

$year_built				=	modiHouseCondoDirectoryHelper::getTypeYearBuiltListHTML();
$min_price				=	modiHouseCondoDirectoryHelper::getMinPriceListHTML();
$districts				=	modiHouseCondoDirectoryHelper::getDistrictListHTML();
$lease					=	modiHouseCondoDirectoryHelper::getLeaseListHTML();

$type					=	JRequest::getCmd('type', '');

if (file_exists($path)) {
	require($path);
}