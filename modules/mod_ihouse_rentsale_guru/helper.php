﻿<?php
/**
 * Helper class for Slider Gallery Module
 * 
 * @package    Joomla
 * @subpackage Modules
 * @link http://www.yashvyas.in
 * @license        GNU/GPL
 * mod_galleryview is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */
class modiHouseRentSaleGuruHelper
{
	
	function getList() {
		$db		=& 	JFactory::getDBO();
		
		$ads_id = JRequest::getCmd('id');
		
		$query 	= 	" SELECT u.*,a.id as ads_id FROM #__ihouse_ads AS a " 
					.	" LEFT JOIN #__users AS u ON u.id = a.posted_by "
					.	" WHERE a.id = '$ads_id' "
					.	' LIMIT 1 ';
			$db->setQuery( $query ) ;
			$row = $db->loadObject();
		
		return $row;
	}
}