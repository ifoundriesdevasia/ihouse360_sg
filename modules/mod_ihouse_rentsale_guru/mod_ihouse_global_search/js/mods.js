﻿
function autocomplete_globalsearch() {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();
	
	var keyword = $('global_search_keyword').getProperty('value');
	if(!keyword) {
		return false;
	} else {
		
	
		var url = 'index.php?option=com_ihouse&task=autocomplete_globalsearch&r=' + unixtime_ms;	
	
		var req = new Ajax(url, {
	   			data		: 	
								{
									'keyword' 	: keyword
								},
				method		: "get",
	    		onComplete	: function(response) {
					var obj1 		= Json.evaluate(response);
					var property 	= obj1.property;	

					if(property) {
						
						$('autocomplete_pos_search').setStyle('display','block');
						$('autocomplete_pos_search').setHTML(property);
						$('autocomplete_pos_search').setStyle('height','auto');
						
						$$('div.property_search').each(function(el) { 
							el.addEvent('click', function(e) {
								var name = el.getProperty('name');
								$('global_search_keyword').setProperty('value', name);
								$('autocomplete_pos_search').setStyle('display','none');
								
								/*document.global_search_form.submit();*/
							});
						});
					} else {
						$('autocomplete_pos_search').setStyle('display','none');
					}
	   			},
				evalScripts: true
		}).request();
	}
}

window.addEvent('domready', function() {
	
});
