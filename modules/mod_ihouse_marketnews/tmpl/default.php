﻿<?php // no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>
<script type="text/javascript">
function toggle_marketnews(id) {
	$$('.menumarket').removeClass('tabberactive');
	$('tab_marketnews' + id).addClass('tabberactive');
	
	$$('.tabbertabmarketnews').addClass('tabbertabhide');
	$('div_marketnews' + id).removeClass('tabbertabhide');
	
	if(id == 1) {
		setTimeout(drawVisualization, 200);
	}
}
</script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
google.load('visualization', '1', {'packages': ['corechart']});
//google.setOnLoadCallback(drawVisualization);
	  
function drawVisualization() {
		var data = new google.visualization.DataTable();
  		data.addColumn('string', 'x');
  		data.addColumn('number', 'All (全部)');/**/
  		data.addColumn('number', 'CCR (中央核心区域)');//
  		data.addColumn('number', 'OCR (核心区域以外)');//
		data.addColumn('number', 'RCR (其它核心区域)');//
		data.addRows(<?php echo count($rows) ?>);
		<?php $i = 0; ?>
  		<?php foreach($rows as $r) : ?>
  		data.setValue(<?php echo $i ?>,0,"<?php echo $r->timeline?>");
		data.setValue(<?php echo $i ?>,1,<?php echo $r->all ?>);
		data.setValue(<?php echo $i ?>,2,<?php echo $r->ccr ?>);
		data.setValue(<?php echo $i ?>,3,<?php echo $r->ocr ?>);
		data.setValue(<?php echo $i ?>,4,<?php echo $r->rcr ?>);
		<?php $i++; ?>
 		<?php endforeach; ?>
		var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
		chart.draw(data, {
				   width: 600, 
				   height: 360,  
				   axisFontSize: 14, 
				   titleFontSize: 16, 
				   axisColor: '#000000',
				   titleY:'Index',
				   titleX:'Timeline', 
				   legend:'bottom',
				   legendTextStyle:{fontSize:11},
				   title: 'Non-landed Price Index(新加坡非有地住宅价格指数)', 
				   axisBackgroundColor: '#EFEFEF', 
				   backgroundColor: '#FFFFFF', 
				   colors: ['#FF7400','red','green','grey'],
				   pointSize: 0, 
				   min:0, 
				   max:250,
				   chartArea: {top:30,width:500,height:160},
				   hAxis: {slantedText:true, slantedTextAngle:90}
		}); 
}
</script>
<div id="module-pos6">
                    <div id="center-module-border">
                        <div id="center-module">
                            <div id="module-headerpos">
                                    <div id="module-title"><font style="font-size:15px;">市场动态</font>&nbsp;Market News</div>
                            </div>
                        </div>
                        <div id="center-module-content" style="min-height:420px;">
                            <div id="module-content-padding8">
<div class="tabberlive">
	<ul class="tabbernav">
    	<li id="tab_marketnews2" class="menumarket tabberactive">
        	<a href="javascript:toggle_marketnews('2');" style="margin:0;">快讯</a>
        </li>
    	<li id="tab_marketnews1" class="menumarket ">
        	<a href="javascript:toggle_marketnews('1');" style="margin:0;">走势图</a>
        </li>
    </ul>                            
    <div id="div_marketnews1" class="tabbertabmarketnews tabbertabhide">
    	<div class="tabItem1">
        	 <div id="chart_div" style="width: 600px; height: 360px;">
             	<div style="text-align:center;margin-top:50px;">
                	Loading Chart....
                </div>
             </div>
        </div>
    </div>
    <div id="div_marketnews2" class="tabbertabmarketnews">
        <div class="tabItem1">
           	<div id="liststyle_sector" style="margin-top:5px;">
            <ul>
           	<?php foreach($market_rows as $r) : ?>
            
            	<li style="padding:3px;">
            	<a style="color:#062284;text-decoration:none;" href="<?php echo JRoute::_('?option=com_content&view=article&id='.$r->id.':'.$r->alias.'&catid='.$r->catid.':market-news') ?>"><?php echo $r->title ?></a> <span style="color:#525252;">(<?php echo date('d/M/Y',strtotime($r->publish_up)); ?>)</span>
            	</li>                           
            <?php endforeach; ?>
            </ul>
            </div>
            <div style="text-align:right;"><a style="text-decoration:none;color:#ff8200;" href="<?php echo JRoute::_('index.php?option=com_content&view=category&id='.$marketnews_id.'&Itemid=69') ?>">更多 ></a></div>
        </div>
    </div>     
</div>
                             </div>
                             <!--End of tabs-->
   						</div>
    				</div>
				</div>