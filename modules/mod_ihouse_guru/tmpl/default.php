<?php // no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<div style="float:left;border-bottom:2px solid #062284;width:100%;">
    <div id="static-text" style="float:left;">本楼盘置业顾问</div>
    <div style="padding-left:5px;float:left;">
        	<span style="font-size:13px;"><i><strong>The Guru</strong></i></span>
    </div>
</div>
<div style="clear:both"></div>
     
<div style="margin-top:7px;">
<?php if(empty($row)) : ?>
<table width="100%" cellspacing=0 cellpadding=0>
	<tr>
		<td>No Guru</td>
	</tr>
</table>
<?php else : ?>
<table width="100%" cellspacing=0 cellpadding=2>
<tr>
	<td colspan=3>
    <div style="text-align:center;">
    	<a style="text-decoration:none;color:#fff;" href="<?php echo JRoute::_('index.php?option=com_user&view=agent_profile&id='.$row->user_id.'&Itemid=136'); ?>">
       	<?php 
			
			if(empty($row->user_image)) {
				$user_image = JRoute::_('templates/main/images/no_avatar.jpg');
			}else{
				$user_image = 'image.php?size=90&type=1&path='.$row->user_image;
			}
			
			?>
            
            <?php if($row->user_category == 'Agent'): ?>
            	<a style="text-decoration:none;color:#fff;" href="<?php echo JRoute::_('index.php?option=com_user&view=agent_profile&id='.$row->user_id.'&Itemid=136'); ?>">
            <?php endif; ?>    
            	<img style="border:none;" src="<?php echo $user_image; ?>" />
            
            <?php if($row->user_category == 'Agent'): ?>        
                </a>
            <?php endif; ?>  
    </div>
    <div style="clear:both;"></div>
    	<div style="text-align:center;">
		<!--<input type="button" onclick="javascript:emailFormPopup('<?php echo $row->email ?>','');" id="agent_sub" name="agent_sub" class="contactbutton" value="Contact">-->
		</div>   
    </td>
    <td>
    <div style="float:left;padding-top:30px;">
   <?php 
			
				if(empty($row->company_logo)) {
					$comp_logo = JRoute::_('templates/main/images/thumb-nologo.jpg');
				}else{
					$comp_logo = 'image.php?size=120&type=1&path='.$row->company_logo;
				}
			
				?>
            
            	<?php if($row->user_category == 'Agent'): ?>
            		<img src="<?php echo $comp_logo ?>" />
           	 	<?php endif; ?> 
    </td>
</tr>
</table>
<div class="guruprofile">
	<div class="l">姓名</div>
	<div class="m">:</div>
	<div class="r">
				<?php 
				if(!empty($row->name) && !empty($row->chinese_name)) {
							$name = '';
							if (!empty($row->name)) {
								$name .= $row->name;
							}
							if (!empty($row->chinese_name)) {
								$name .= '</br>'.$row->chinese_name;
							}
							
							echo $name;
				} else { 
					  		echo 'N/A';
				}
				?>
	</div>
	<div style="clear:both"></div>
</div>	
<div class="guruprofile">
	<div class="l">公司名称</div>
	<div class="m">:</div>
	<div class="r"><?php echo (!empty($row->company_name))?wordwrap($row->company_name, 15, "\n", true):'N/A' ?></div>
	<div style="clear:both"></div>
</div>		
<div class="guruprofile">
	<div class="l">电话</div>
	<div class="m">:</div>
	<div class="r"><?php echo ($row->mobile_contact)?$row->mobile_contact:'N/A' ?></div>
	<div style="clear:both"></div>
</div>
<div class="guruprofile">
	<div class="l">网址</div>
	<div class="m">:</div>
	<div class="r"><?php echo (!empty($row->website))?wordwrap($row->website, 15, "\n", true):'N/A' ?></div>
	<div style="clear:both"></div>
</div>		
<div class="guruprofile">
	<div class="l">经纪证号</div>
	<div class="m">:</div>
	<div class="r">
		<?php if(!empty($row->cea_reg_no)) : ?>
        	<?php if(preg_match('/^L/', $row->cea_reg_no )): ?>
            	<a href="http://www.cea.gov.sg/cea/app/newimplpublicregister/view.jspa?licNo=<?php echo $row->cea_reg_no ?>" target="_blank">
            <?php elseif(preg_match('/^R/', $row->cea_reg_no )) : ?>    
                <a href='http://www.cea.gov.sg/cea/app/newimplpublicregister/view.jspa?licNo=""&regNo=<?php echo $row->cea_reg_no ?>' target="_blank">
            <?php endif; ?>   
					<?php echo $row->cea_reg_no ?>
                </a>
        <?php else : ?>
                	N/A    
        <?php endif; ?>
    </div>
	<div style="clear:both"></div> 
</div>        

<?php endif; ?>
</div>

<!-- POPUP -->
<!--<div id="popupForm" style="display:none;">
</div>-->
