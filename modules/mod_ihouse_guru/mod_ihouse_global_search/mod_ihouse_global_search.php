<?php

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// Include the syndicate functions only once
require_once( dirname(__FILE__).DS.'helper.php' );

$layout = $params->get('layout', 'default');
$path = JModuleHelper::getLayoutPath('mod_ihouse_global_search', $layout);

$doc =& JFactory::getDocument();
$doc->addScript(JURI::root() .'modules/mod_ihouse_global_search/js/mods.js');

//$rows 			= modiHouseGlobalSearchHelper::getSearch($params);

/*$bedrooms		= modiHouseSearchHelper::getBedRoomListHTML();
$districts		= modiHouseSearchHelper::getDistrictListHTML();
$stations 		= modiHouseSearchHelper::getNearestMRTListHTML();
$schools 		= modiHouseSearchHelper::getNearestSchoolListHTML();
$property_type 	= modiHouseSearchHelper::getPropertyType();

$prices			= modiHouseSearchHelper::getPriceListHTML();
*/
//$roomsizes		= modiHouseSearchHelper::getRoomSizeHTML();

if (file_exists($path)) {
	require($path);
}