﻿<?php // no direct access
defined('_JEXEC') or die('Restricted access'); 

$view = JRequest::getVar('view', '');
$task = JRequest::getVar('task', '');
$option = JRequest::getVar('option', '');
$db = JFactory::getDBO();

?>
<style type="text/css">
.breadcrumb_link {
	color: #525252;
	text-decoration:none;
}
</style>
<span class="breadcrumbs pathway">
<?php for ($i = 0; $i < $count; $i ++) :

	// If not the last item in the breadcrumbs add the separator
	if ($i < $count -1) {
		if(!empty($list[$i]->link)) {
			echo '<a href="'.$list[$i]->link.'" class="pathway">'.$list[$i]->name.'</a>';
		} else {
			echo $list[$i]->name;
		}
		echo ' '.$separator.' ';
	}  else if ($params->get('showLast', 1)) { // when $i == $count -1 and 'showLast' is true
		if($list[$i]->name == '首页') {
	    	echo '<a class="breadcrumb_link" href="'.JURI::root().'">'.$list[$i]->name.'</a>';
		} else {
			echo $list[$i]->name;
		}
	}
endfor; ?>

<?php 
if($option == 'com_ihouse'){
	if (empty($list[$i]->link)){ 

		$view 	= JRequest::getVar('view', '');
		$sector = JRequest::getVar('sector', '');
		$task	= JRequest::getVar('task', '');
		
		if ($view == 'ihouse'){
			$property_id = JRequest::getVar('id', '');
			$query = " SELECT name_ch, name_en FROM #__ihouse_property WHERE id = '$property_id' ";
				$db->setQuery( $query );
				$result = $db->loadObject();
			
			
			if(!empty($sector)) {
				echo ' '.$separator.' ';
				$query = " SELECT name_ch FROM #__ihouse_mapping_sector WHERE mapping_district = '$sector' ";
				$db->setQuery( $query );
				$result = $db->loadResult();
				
				$breadcrumb = ' 最新房源 '. $separator.' '.$result;
			}
			else if($task == 'adtypelisting') {
				echo ' '.$separator.' ';
				$adtype = JRequest::getVar('adtype','');
				$type	= JRequest::getVar('type','');
				
				if($adtype == 4) {
					if($type == 'rent')
						$type = '出租';
					else if($type == 'sell')	
						$type = '出售';
				
					$breadcrumb = '精品楼盘 '. $separator.' '.$type;
				}
				
				if($adtype == 7) {
					
					$breadcrumb = '新公寓出售 ';
				}
			}
			else {
				echo ' '.$separator.' ';
				$breadcrumb = '<a class="breadcrumb_link" href="'.JRoute::_('index.php?option=com_ihouse&view=directory&layout=condo_directory1&Itemid=150').'">公寓目录</a>'.$separator. ' '. $result->name_ch.$result->name_en;
			}
		}
		echo $breadcrumb;
	} 
}

if($option == 'com_ads') {
	if (empty($list[$i]->link)){ 
		$view = JRequest::getVar('view', '');
		
		$listinglink = '<a class="breadcrumb_link" href="'.JRoute::_('index.php').'">广告管理</a>';
				
		if ($view == 'adslisting'){
			echo ' '.$separator.' ';
			$property_id = JRequest::getVar('id', '');
			$query = " SELECT name_ch, name_en,postcode FROM #__ihouse_property WHERE id = '$property_id' ";
				$db->setQuery( $query );
				$result = $db->loadObject();
				
				$postcode = $result->postcode; 
				
			$breadcrumb = '<a class="breadcrumb_link" href="'.JRoute::_('index.php?option=com_ihouse&view=directory&layout=condo_directory1&Itemid=150').'">公寓目录</a>'.$separator. ' ';
			$breadcrumb .= '<a class="breadcrumb_link" href="'.JRoute::_('index.php?option=com_ihouse&view=ihouse&layout=property_detail&id='.$property_id.'&postcode='.$postcode).'">'.$result->name_ch.$result->name_en. '</a>';
			
			$breadcrumb .= ' '.$separator.' '. '出售房源列表';
			
		} else if($view == 'rentadslisting') {
			echo ' '.$separator.' ';
			
			$property_id = JRequest::getVar('id', '');
			$query = " SELECT name_ch, name_en,postcode FROM #__ihouse_property WHERE id = '$property_id' ";
				$db->setQuery( $query );
				$result = $db->loadObject();
				
				$postcode = $result->postcode; 
				
			$breadcrumb = '<a class="breadcrumb_link" href="'.JRoute::_('index.php?option=com_ihouse&view=directory&layout=condo_directory1&Itemid=150').'">公寓目录</a>'.$separator. ' ';
			
			$breadcrumb .= '<a class="breadcrumb_link" href="'.JRoute::_('index.php?option=com_ihouse&view=ihouse&layout=property_detail&id='.$property_id.'&postcode='.$postcode).'">'.$result->name_ch.$result->name_en. '</a>';
			
			$breadcrumb .= ' '.$separator.' '. '出租房源列表';	
			
		} else if($view == 'adsdetails') {
			echo ' '.$separator.' ';
			$ads_id = JRequest::getVar('id', '');
			$query = " SELECT property_id, ad_type , ad_title, property_type, street_name,postcode FROM #__ihouse_ads WHERE id = '$ads_id' ";
				$db->setQuery( $query );
				$result = $db->loadObject();
			
			$property_id 	= 	$result->property_id;
			$adtype			=	$result->ad_type;
			$adtitle		=	$result->ad_title;
			$property_type	=	$result->property_type;
			$street_name	=	$result->street_name;
			$postcode		=	$result->postcode;
			
			if($adtype == 'sell') {
				$adtype = '出售';
				$adurl 	= '<a class="breadcrumb_link" href="'.JRoute::_('index.php?option=com_ads&view=adslisting&id='.$property_id.'&postcode='.$postcode).'">'.$adtype.'</a>';
			}
			else if($adtype == 'rent') {
				$adtype = '出租';
				$adurl 	= '<a class="breadcrumb_link" href="'.JRoute::_('index.php?option=com_ads&view=rentadslisting&id='.$property_id.'&postcode='.$postcode).'">'.$adtype.'</a>';
			}
			
			if($property_type == '公寓') {
				$query = " SELECT name_ch, name_en FROM #__ihouse_property WHERE id = '$property_id' ";
					$db->setQuery( $query );
					$prop = $db->loadObject();
			
				$breadcrumb 	= '<a class="breadcrumb_link" href="'.JRoute::_('index.php?option=com_ihouse&view=directory&layout=condo_directory1&Itemid=150').'">公寓目录</a>';
				$breadcrumb 	.= ' '.$separator. ' <a class="breadcrumb_link" href="'.JRoute::_('index.php?option=com_ihouse&view=ihouse&layout=property_detail&id='.$property_id.'&postcode='.$postcode).'">'. $prop->name_ch.$prop->name_en. '</a>';
				$breadcrumb		.= ' '.$separator.' '.$adurl.' ';
				$breadcrumb		.= ' '.$separator. ' '.$adtitle;
				
			} else if($property_type == '组屋') {
				
				$breadcrumb = $property_type.'目录'.' '.$separator. ' '.$street_name. ' '. $separator.' '. $adtype.' '. $separator. ' '.$adtitle;
			}
		}
		else if($view == 'top_up') {
			echo ' '.$separator.' ';
			$breadcrumb = '充值';
		} else if($view == 'transaction') {
			echo ' '.$separator.' ';
			$breadcrumb = '交易记录';
		} else if($view == 'ads_management') {
			echo ' '.$separator.' ';
			$breadcrumb = $listinglink.' '.$separator.' '.'广告列表';
		} else if($view == 'the_guru') {
			echo ' '.$separator.' ';
			$breadcrumb = $listinglink.' '.$separator.' '.'The Guru';
		} else if($view == 'postads') {
			echo ' '.$separator.' ';
			$breadcrumb = $listinglink.' '.$separator.' '.'广告';
		} else if($view == 'account_info') {
			
			
		}
		
		echo $breadcrumb;
	}
}

if($option == 'com_user') {
	if (empty($list[$i]->link)){ 
		$view = JRequest::getVar('view', '');
			
		
		if($view == 'profiler') {
			echo ' '.$separator.' ';
			$breadcrumb = '会员中心 '.$separator. ' '.'个人资料';
		}
	}
}
?>
</span>
