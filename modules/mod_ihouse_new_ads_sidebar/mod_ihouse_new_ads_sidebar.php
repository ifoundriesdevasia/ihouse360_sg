<?php

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// Include the syndicate functions only once
require_once( dirname(__FILE__).DS.'helper.php' );

$layout = $params->get('layout', 'default');
$path = JModuleHelper::getLayoutPath('mod_ihouse_new_ads_sidebar', $layout);

$db =& JFactory::getDBO();
	
$rows				= modiHouseNewAdsSidebarHelper::getNewCondosForSale();



if (file_exists($path)) {
	require($path);
}