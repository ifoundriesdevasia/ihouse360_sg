﻿<?php
/**
 * Helper class for Slider Gallery Module
 * 
 * @package    Joomla
 * @subpackage Modules
 * @link http://www.yashvyas.in
 * @license        GNU/GPL
 * mod_galleryview is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */
class modiHouseNewAdsSidebarHelper
{

	function getNewCondosForSale() {
		
		$db		=& 	JFactory::getDBO();
				
		$orderby	= ' ORDER BY a.posting_date DESC '; 		
				
		$query			= 	"SELECT a.* FROM #__ihouse_ads_type AS t "
							.	" LEFT JOIN #__ihouse_ads AS a ON a.id = t.ads_id "
							.	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = t.subscription_id "
							.	" WHERE t.ads_type_config = 7 "
							//.	" AND cb.expiry_date >= '" . date('Y-m-d H:i:s', time()) . "' "
							.	" AND cb.status = 'A' "
							//.	" AND t.property_id = '$property_id' "
							.	" AND a.ad_type	= 'sell' "
							.	" AND a.publish = 1 "
							.	$orderby
							.	" LIMIT 4 ";			
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		//echo $query;
		return $rows;
	}
	
}