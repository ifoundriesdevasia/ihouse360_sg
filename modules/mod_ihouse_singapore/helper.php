﻿<?php
/**
 * Helper class for Slider Gallery Module
 * 
 * @package    Joomla
 * @subpackage Modules
 * @link http://www.yashvyas.in
 * @license        GNU/GPL
 * mod_galleryview is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */
class modiHouseSingaporeHelper
{
    function getDetails($title, $mode = 1)
    {
		$db		=& 	JFactory::getDBO();
		
		$query = " SELECT id FROM #__k2_categories WHERE name = 'Static Content-".$title."' ";
			$db->setQuery( $query );
			$catid = $db->loadResult(); 
		
		
		if($mode == 1) {
			$query = " SELECT * FROM #__k2_items WHERE catid = '$catid' LIMIT 3 ";
				$db->setQuery( $query );
				$rows = $db->loadObjectList();
		
			$html = '';
			foreach($rows as $r) :
				$html .= '<div style="margin:5px;border-bottom:1px dotted #ccc;">';
				$html .= '<div style="padding:5px;">';
				$html .= '<div><a style="color: #062284;font-weight: bold;text-decoration: none;" href="'.JRoute::_('index.php?option=com_k2&view=item&layout=item&id='.$r->id.'&Itemid=108').'">'.$r->title.'</a></div>';
			
				$content 	=	strip_tags($r->introtext);
				$html .= '<div style="padding-top:5px;">';
				
				$tmptext = '';
				if(preg_match_all('/([\w]+)|(.)/u', $content, $matches)) {
					for($i = 0; $i < 90; $i++) :
						$tmptext .= $matches[0][$i]; 
					endfor;
				}
				$html .= $tmptext;
				$html .= '</div>';
				$html .= '</div>'; 
				$html .= '<div style="text-align:right;margin-top:-19px;z-index:100;"><a style="color:#ff8200;text-decoration: none;" href="'.JRoute::_('index.php?option=com_k2&view=item&layout=item&id='.$r->id.'&Itemid=108').'">更多文章 ></a></div>';
				$html .= '</div>';
			endforeach;
		}
		else if($mode == 2) {
			$query = " SELECT * FROM #__k2_items WHERE catid = '$catid' LIMIT 1 ";
				$db->setQuery( $query );
				$row = $db->loadObject();
				
			$html .= '<div style="margin:5px;border-bottom:1px dotted #ccc;">';
			$html .= '<div style="padding:5px;">';
			$html .= '<div><a style="color: #062284;font-weight: bold;text-decoration: none;" href="'.JRoute::_('index.php?option=com_k2&view=item&layout=item&id='.$row->id.'&Itemid=108').'">'.$row->title.'</a></div>';
			
			$content 	=	$row->introtext;
			//$content 	= preg_replace('/\s\s+/', ' ', $content);
			$content 	= nl2br(strip_tags($content));
			$content 	= preg_replace("/^(<br\s*\/?>\s*)+/", "", $content);
			$content 	= preg_replace("/(<br\s*\/?>\s*)+/", "<br/><br />", $content);
			
			
			$html 		.= '<div style="padding-top:5px;">';
			
			$tmptext = '';
				if(preg_match_all('/([\w]+)|(.)/u', $content, $matches)) {
					for($i = 0; $i < 187; $i++) :
						$tmptext .= $matches[0][$i]; 
					endfor;
				}
				
			$html .= $tmptext;
			$html		.= '</div>';
			$html 		.= '</div>'; 
			$html .= '<div style="text-align:right;margin-top:-19px;z-index:100;"><a style="color:#ff8200;text-decoration: none;" href="'.JRoute::_('index.php?option=com_k2&view=item&layout=item&id='.$row->id.'&Itemid=108').'">更多文章 ></a></div>';
			$html .= '</div>';
		}
		
		return $html;
    }
}