﻿<?php

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// Include the syndicate functions only once
require_once( dirname(__FILE__).DS.'helper.php' );

$layout = $params->get('layout', 'default');
$path = JModuleHelper::getLayoutPath('mod_ihouse_singapore', $layout);

$tab1 = modiHouseSingaporeHelper::getDetails('概况',1);
$tab2 = modiHouseSingaporeHelper::getDetails('房产须知',1);
$tab3 = modiHouseSingaporeHelper::getDetails('求学导读',1);
$tab4 = modiHouseSingaporeHelper::getDetails('工作与发展',1);
$tab5 = modiHouseSingaporeHelper::getDetails('生活资讯',1);
$tab6 = modiHouseSingaporeHelper::getDetails('娱乐',1);
$tab7 = modiHouseSingaporeHelper::getDetails('投资与移民',1);
$tab8 = modiHouseSingaporeHelper::getDetails('房产问与答 ',2);

if (file_exists($path)) {
	require($path);
}