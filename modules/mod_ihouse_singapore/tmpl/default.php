﻿<?php // no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>
<script type="text/javascript">
function toggle_singapore(id) {
	$$('.menusingapore').removeClass('tabberactive');
	$('tab_singapore' + id).addClass('tabberactive');
	
	$$('.tabbertabsingapore').addClass('tabbertabhide');
	$('div_singapore' + id).removeClass('tabbertabhide');
}
</script>
<!--Start of tabs-->

<div id="module-content-padding1" style="min-height:230px;">
                    
<div class="tabberlive" style="margin-top:-5px;">
	<ul class="tabbernav">
    	<li id="tab_singapore1" class="menusingapore tabberactive">
        	<a href="javascript:toggle_singapore('1');" style="margin:0;">概况</a>
        </li>
        <li id="tab_singapore2" class="menusingapore ">
        	<a href="javascript:toggle_singapore('2');" style="margin:0;">房产须知</a>
        </li>
        <li id="tab_singapore3" class="menusingapore">
        	<a href="javascript:toggle_singapore('3');" style="margin:0;">求学导读</a>
        </li>
        <li id="tab_singapore4" class="menusingapore ">
        	<a href="javascript:toggle_singapore('4');" style="margin:0;">工作与发展</a>
        </li>
        <li id="tab_singapore5" class="menusingapore">
        	<a href="javascript:toggle_singapore('5');" style="margin:0;">生活资讯</a>
        </li>
        <li id="tab_singapore6" class="menusingapore ">
        	<a href="javascript:toggle_singapore('6');" style="margin:0;">娱乐</a>
        </li>
        <li id="tab_singapore7" class="menusingapore">
        	<a href="javascript:toggle_singapore('7');" style="margin:0;">投资与移民</a>
        </li>
        <li id="tab_singapore8" class="menusingapore ">
        	<a href="javascript:toggle_singapore('8');" style="margin:0;">房产问与答</a>
        </li>
    </ul>
                                     
    <div id="div_singapore1" class="tabbertabsingapore">
    	<div class="tabItem1">
        	
			<?php echo $tab1 ; ?>
            
        </div>
    </div>
    <div id="div_singapore2" class="tabbertabsingapore tabbertabhide">
        <div class="tabItem1">
          	
			<?php echo $tab2 ; ?>
            
        </div>
    </div>
    <div id="div_singapore3" class="tabbertabsingapore tabbertabhide">
        <div class="tabItem1">
          	
			<?php echo $tab3 ; ?>
            
        </div> 
    </div>
    <div id="div_singapore4" class="tabbertabsingapore tabbertabhide">
        <div class="tabItem1">
          	
			<?php echo $tab4 ; ?>
        
        </div>
    </div>
    <div id="div_singapore5" class="tabbertabsingapore tabbertabhide">
        <div class="tabItem1">
          	
			<?php echo $tab5 ; ?>
            
        </div>
    </div>
    <div id="div_singapore6" class="tabbertabsingapore tabbertabhide">
        <div class="tabItem1">
          	
			<?php echo $tab6 ; ?>
            
        </div>
    </div>
    <div id="div_singapore7" class="tabbertabsingapore tabbertabhide">
        <div class="tabItem1">
          	
			<?php echo $tab7 ; ?>
            
        </div>
    </div>
    <div id="div_singapore8" class="tabbertabsingapore tabbertabhide">
        <div class="tabItem1">
          	
            <?php echo $tab8; ?>
           
        </div>
    </div>
                                
</div>                   
                    	
                     </div>