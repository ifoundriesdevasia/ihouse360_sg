﻿<?php
/**
 * Helper class for Slider Gallery Module
 * 
 * @package    Joomla
 * @subpackage Modules
 * @link http://www.yashvyas.in
 * @license        GNU/GPL
 * mod_galleryview is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */
class modiHouseReviewAtDirectoryHelper
{
    function getReviews()
    {
		$db		=& 	JFactory::getDBO();

		$query = 	"SELECT r.*,p.postcode, p.name_en,p.name_ch, u.name, u.chinese_name,u.user_image FROM #__ihouse_property_review AS r "
					.	" LEFT JOIN #__users AS u ON u.id = r.user_id " 
					.	" LEFT JOIN #__ihouse_property AS p ON p.id = r.property_id " 
					.	" ORDER BY r.created DESC "
					.	" LIMIT 10 ";
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		return $rows;
    }
	
}