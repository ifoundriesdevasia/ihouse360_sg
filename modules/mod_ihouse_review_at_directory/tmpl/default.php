<?php // no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>
<script type="text/javascript">
function getReadReviewAllLess(reviewid, mod) {
	$$('.reviewclass-'+reviewid).setStyle('display','none');
	if(mod == 1) {
		$('review-'+reviewid+'l').setStyle('display','');
	} else {
		$('review-'+reviewid+'s').setStyle('display','');
	}
}

function loadUserRatingOnDirectory(page) {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();			
	
	//$('ajax-reviews-on-directory').setHTML('');
	
	var url = 'index.php?option=com_ihouse&task=loaduserratingondirectory&r=' + unixtime_ms;		
	var req = new Ajax(url, {
	   					data		: 	
									{
										'page' : page,
										'limit' : 5
									},
						method		: "get",
		    			onSuccess	: function(datas) {
							$('ajax-reviews-on-directory').setHTML(datas);
		   				},
						evalScripts : true
				}).request();				
}

loadUserRatingOnDirectory(0);
</script>
<div class="component-pos">
	<div id="sub-small-component-border" style="background-color:#fff;">
    	<div id="sub-small-component">
        	<div id="component-headerpos">
            	<div id="component-logo">
                	<div id="component-title"><span class="chinese_text1">发表评论</span>&nbsp;<i>Review</i></div>
                </div>
            </div>
        </div>
        
        <div id="sub-small-component-content" style="">
        	<div id="ajax-reviews-on-directory" style="float:left;border-bottom:1px dotted #ccc;padding:10px;width:97%;">
			<!-- ajax reviews on directory pages -->
			</div>  
		</div><!-- sub-small-component-content -->
	</div>
</div>