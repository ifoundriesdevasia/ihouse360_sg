<?php //jph removed art advert from menu html,  _VALID_MOS altered to _JEXEC
//$my not exist in 1.5, use the code in mod_transmenu to set var $myusrvalue to correct val. 47
// $database not exist in 1.5, use the code in mod_transmenu to set to db object
/**
* @version $Id: mod_ja_transmenu.php
* @copyright (C) 2005 JoomlArt.com
*/
defined('_JEXEC') or die('Restricted access');

class JAMenu{
	var $menuObj; 
	var $_params = null;
	var $_db = null;	
	var $children = null;
	var $open = null;
//jph altered object name	
	function JAMenu( &$database_x, &$params ){
		$this->_params = $params;
		$this->_db = $database_x;

		$this->loadMenu();
		$this->createmenuObj();
	}
	
	function createmenuObj (){
	//added jph
	global $mosConfig_absolute_path;
			switch ($this->_params->get( 'menutype' )){
				default:
				//jph altered
					include_once($this->_params->get( 'absPath' )."/transmenu/transmenu.php");
					$this->menuObj = new TransMenu($this);
				break;
			}
	}
	
	function  loadMenu(){
	//jph removed global legacy $my, 
	//jph get $my the 1.5 way
	$myusr	=& JFactory::getUser();
	// use this object to get user id 
	$myusrvalue = $myusr->get('aid', 0);
	//===========	
	//global $database no longer exists in 1.5, use	this-> $database_x from caller
		global $cur_template, $Itemid;
		global $mosConfig_absolute_path, $mosConfig_live_site, $mosConfig_shownoauth;

		if ($mosConfig_shownoauth) {
			$sql = "SELECT m.* FROM #__menu AS m"
			. "\nWHERE menutype='". $this->_params->get( 'menutype' ) ."' AND published='1'"
			. "\nORDER BY parent,ordering";
		} else {
			$sql = "SELECT m.* FROM #__menu AS m"
			. "\nWHERE menutype='". $this->_params->get( 'menutype' ) ."' AND published='1' AND access <= '$myusrvalue'"
			. "\nORDER BY parent,ordering";
		}
		//echo $sql;
		$this->_db->setQuery( $sql );
		$rows = $this->_db->loadObjectList( 'id' );

		// establish the hierarchy of the menu
		$this->children = array();
		// first pass - collect children
		foreach ($rows as $v ) {
			$pt = $v->parent;
			$list = @$this->children[$pt] ? $this->children[$pt] : array();
			array_push( $list, $v );
			$this->children[$pt] = $list;
		}

		// second pass - collect 'open' menus
		$this->open = array( $Itemid );
		$count = 20; // maximum levels - to prevent runaway loop
		$id = $Itemid;
		while (--$count) {
			if (isset($rows[$id]) && $rows[$id]->parent > 0) {
				$id = $rows[$id]->parent;
				$this->open[] = $id;
			} else {
				break;
			}
		}
	}
		
	function genMenu(){
		$this->beginMenu();
		$this->menuObj->beginMenu();
		$this->genMenuItems (0, 0);
		$this->menuObj->endMenu();
		$this->endMenu();

	}
	
	/*
	$pid: parent id
	$level: menu level
	$pos: position of parent
	*/
	function genMenuItems($pid, $level) {
		if (@$this->children[$pid]) {
			$i = 0;
			foreach ($this->children[$pid] as $row) {
				
				$this->menuObj->genMenuItem( $row, $level, $i);

				// show menu with menu expanded - submenus visible
				$this->genMenuItems( $row->id, $level+1 );
				$i++;
			}
		}
		
	}

	function beginMenu(){
		echo "<!-- Begin menu -->\n";
		echo '<div id="jaframeid" style="position:absolute;top:1px;left:1px;height:0px;width:0px;overflow:hidden">x
					</div>';
//removed tat jph
	}
	function endMenu(){
		echo "<!-- End menu -->\n";
	}
	function hasSubItems($id){
		if (@$this->children[$id]) return true;
		return false;
	}
}
?>