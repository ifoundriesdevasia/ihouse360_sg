<?php //jph,  _VALID_MOS altered to _JEXEC, &amp; included, 134, 
// fn. mosGetParam replaced and fn. sefRelToAbs is replaced twice, by 1.5 code from legacy file
// fn mosParameters
// global var $mosConfig_live_site replaced in 1.5 by base
defined('_JEXEC') or die('Restricted access');

class TransMenu{
	var $parent = null;
	function TransMenu(&$parent){
		$this->parent = $parent;
	}
	function getParam($paramName){
		return $this->parent->_params->get($paramName);

	}
	function beginMenu(){
		if ($this->getParam('menu_style') == 'vertical')
			$cssfile = "transmenuv.css";
		else
			$cssfile = "transmenuh.css";			
			//jph see1.5 method for css in head
			JHTML::stylesheet($cssfile,'modules/mod_transmenu/transmenu/');			
			// <link rel="stylesheet" type="text/css" href="',$this->getParam('LSPath'),'/',$cssfile,'">
		echo '<script language="javascript" src="',$this->getParam('LSPath'),'/transmenu.js"></script>
		';
			$direction = "TransMenu.direction.".$this->getParam('menu_direction');
			$position = "TransMenu.reference.".$this->getParam('menu_position');

			$top = $this->getParam('p_t');
			$left = $this->getParam('p_l');
		
			$subpad_x = $this->getParam('subpad_x');
			$subpad_y = $this->getParam('subpad_y');
		switch ($this->getParam('menu_style')){
			case 'vertical':
				echo '<div id="wrap"><div id="vertmenu">';
				echo '<table cellpadding=0 cellspacing=0 border=0>';
				foreach ($this->parent->children[0] as $v) {
					echo "<tr><td>";
					echo $this->getFirstLevelItem($v);
					echo "</td></tr>";
				}
				echo "</table></div></div>";
			break;
			
			case 'horizontal':
			default:
				$i = 0;
				echo '<div id="wrap"><div id="menu">';
				echo '<table cellpadding=0 cellspacing=0 border=0><tr>';
/*				echo '<td>{modulepos user1}</td>';
									//echo "ʨ������";
									//echo '</a></td>';
									echo "<td class='border99'>&nbsp;</td>";*/
				foreach ($this->parent->children[0] as $v) {
					echo "<td>";
					echo $this->getFirstLevelItem($v);
					echo "</td>";
					echo "<td class='border".$i."'><img src='".JURI :: base()."/templates/main/images/static-spacer.jpg' /></td>";
					$i++;
				}
				echo "</tr></table></div></div>";
//				<img src='".JURI :: base()."/templates/main/images/static-spacer.jpg' />

			break;
		}
		echo '
			<script language="javascript">
			if (TransMenu.isSupported()) {
				TransMenu.updateImgPath(\'',$this->getParam('LSPath'),'/\');
				var ms = new TransMenuSet(',$direction,', ', $left,', ',$top,', ',$position,');
				TransMenu.subpad_x = ',$subpad_x,';
				TransMenu.subpad_y = ',$subpad_y,';

			';
	}
	function endMenu(){
		echo '
				TransMenu.renderAll();
			}
			init1=function(){TransMenu.initialize();}
			if (window.attachEvent) {
				window.attachEvent("onload", init1);
			}else{
				TransMenu.initialize();			
			}
			</script>
		';
	}
	function genMenuItem(&$row, $level, $pos){
	//jph global $mosConfig_live_site not used in v 1.5 
		global $Itemid, $mainframe;
		$txt = '';

		switch ($row->type) {
			case 'separator':
			case 'component_item_link':
			break;
			case 'content_item_link':
			$temp = split("&task=view&id=", $row->link);
			$row->link .= '&Itemid='. $mainframe->getItemid($temp[1]);
			break;
			case 'url':
			if ( eregi( 'index.php\?', $row->link ) ) {
				if ( !eregi( 'Itemid=', $row->link ) ) {
					$row->link .= '&Itemid='. $row->id;
				}
			}
			break;
			case 'content_typed':
			default:
			$row->link .= '&Itemid='. $row->id;
			break;
		}

		//$row->link = ampReplace( $row->link );

		if ( strcasecmp( substr( $row->link,0,4 ), 'http' ) ) {			
			//jph not used in 1.5  $row->link = sefRelToAbs( $row->link );
			//===========now use code supplied by legacy
			$url = str_replace('&amp;', '&', $row->link);
			$uri    = JURI::getInstance();
			$prefix = $uri->toString(array('scheme', 'host', 'port'));
			$row->link = $prefix.JRoute::_($url);
			//==================			
		}

		
		//echo "$row->name $row->link $level<br>";
		if ($level){
			$pmenu = "tmenu$row->parent";
			//echo "$pmenu.addItem(\"$row->name\", \"$row->link\");\n";
			$active = 0;
			if ( in_array($row->id, $this->parent->open) ) $active = 1;

			$txt = $row->name;
			if ( $this->getParam( 'menu_images' ) ) {
				$menu_params = new stdClass();
		//========jph replace old code with 1.5 version
			//$menu_params =& new mosParameters( $row->params );
			$menu_params = new JParameter($row->params);
		//=============
			//Sets a default value if not already assigned, bug i think.. was menu_image now menu_images	
				$menu_image = $menu_params->def( 'menu_images', -1 );
				if ( ( $menu_image <> '-1' ) && $menu_image ) {
			//jph global $mosConfig_live_site not used, use 1.5 base url
					$image = '<img src="'. JURI :: base() .'/images/stories/'. $menu_image .'" border="0" alt="'. $row->name .'"/>';
					if ( $this->getParam( 'menu_images_align' ) ) {
						$txt = $txt .' '. $image;
					} else {
						$txt = $image .' '. $txt;
					}
				}
			}
			$txt = str_replace("\"", "\\\"", $txt);
//jph amp mod
			$row->link = str_replace( '&amp;', '&', $row->link );
//	
			
			echo "$pmenu.addItem(\"$txt\", \"$row->link\", $row->browserNav, $active);\n";
		}else{
			$pmenu = "ms";
		}
		$cmenu = "tmenu$row->id";
		$idmenu = "menu$row->id";
		if ($this->parent->hasSubItems($row->id)){
			if ($level == 0){
				echo "var $cmenu = ".$pmenu.".addMenu(document.getElementById(\"$idmenu\"));\n";
			}else{
				echo "var $cmenu = ".$pmenu.".addMenu(".$pmenu.".items[".$pos."]);\n";
			}
		}else{
			if ($level == 0){
				echo '
				document.getElementById("',$idmenu,'").onmouseover = function() {
					',$pmenu,'.hideCurrent();
				}
				';
			}
		}
	}

	function getFirstLevelItem( $mitem ) {
		//jph global $mosConfig_live_site not used in v 1.5 
		global $Itemid,  $mainframe;
		$txt = '';

		switch ($mitem->type) {
			case 'separator':
			case 'component_item_link':
			break;
			case 'content_item_link':
			$temp = split("&task=view&id=", $mitem->link);
			$mitem->link .= '&Itemid='. $mainframe->getItemid($temp[1]);
			break;
			case 'url':
			if ( eregi( 'index.php\?', $mitem->link ) ) {
				if ( !eregi( 'Itemid=', $mitem->link ) ) {
					$mitem->link .= '&Itemid='. $mitem->id;
				}
			}
			break;
			case 'content_typed':
			default:
			$mitem->link .= '&Itemid='. $mitem->id;
			break;
		}

		$id = 'id="menu'.$mitem->id.'"';

		//$mitem->link = ampReplace( $mitem->link );

		if ( strcasecmp( substr( $mitem->link,0,4 ), 'http' ) ) {
			//jph not used in 1.5  $mitem->link = sefRelToAbs( $mitem->link );
			//===========now use code supplied by legacy
			$url = str_replace('&amp;', '&', $mitem->link);
			$uri    = JURI::getInstance();
			$prefix = $uri->toString(array('scheme', 'host', 'port'));
			$mitem->link = $prefix.JRoute::_($url);
			//==================
		}

		$menuclass = 'mainlevel'. $this->getParam( 'class_sfx' );
		// Active Menu highlighting
		//jph, mosGetParam not used in 1.5, 1.5 uses class JArrayHelper, fn getValue
		/**
	 * Utility function to return a value from a named array or a specified default
	 *
	 * @static
	 * @param	array	$array		A named array
	 * @param	string	$name		The key to search for
	 * @param	mixed	$default	The default value to give if no key found
	 * @param	string	$type		Return type for the variable (INT, FLOAT, STRING, WORD, BOOLEAN, ARRAY)
	 * @return	mixed	The value from the source array
	 * @since	1.5
	 */
		//function getValue(&$array, $name, $default=null, $type='')
		$current_itemid = JArrayHelper::getValue( $_REQUEST, 'Itemid', 0, 'int' );		
		//$current_itemid = tr im( mosGetParam( $_REQUEST, 'Itemid', 0 ) );
		if ( in_array($mitem->id, $this->parent->open) ) {
			$menuclass = 'mainlevel_active'. $this->getParam( 'class_sfx' );
		}

		$txt = $mitem->name;
		if ( $this->getParam( 'menu_images' ) ) {
	//param menu_images = 1, i.e local checkbox images are on
			$menu_params = new stdClass();
	//========jph replace old code with 1.5 version
			//$menu_params =& new mosParameters( $mitem->params );
			$menu_params = new JParameter($mitem->params);
	//=============
			//Sets a default value if not already assigned, bug i think.. was menu_image now menu_images
			$menu_image = $menu_params->def( 'menu_images', -1 );
			//if it wasn't blank, switch pics on...
			if ( ( $menu_image <> '-1' ) && $menu_image ) {
			//jph global $mosConfig_live_site not used, use 1.5 base url
				$image = '<img src="'. JURI :: base() .'/images/stories/'. $menu_image .'" border="0" alt="'. $mitem->name .'"/>';
				if ( $this->getParam( 'menu_images_align' ) ) {
					$txt = $txt .' '. $image;
				} else {
					$txt = $image .' '. $txt;
				}
			}
		}

		switch ($mitem->browserNav) {
			// cases are slightly different
			case 1:
			// open in a new window
			$txt = '<a href="'. $mitem->link .'" target="_blank" class="'. $menuclass .'" '. $id .'>'. $txt .'</a>';
			break;

			case 2:
			// open in a popup window
			$txt = "<a href=\"#\" onclick=\"javascript: window.open('". $mitem->link ."', '', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=780,height=550'); return false\" class=\"$menuclass\" ". $id .">". $txt ."</a>\n";
			break;

			case 3:
			// don't link it
			$txt = '<span class="'. $menuclass .'" '. $id .'>'. $txt .'</span>';
			break;

			default:	// formerly case 2
			// open in parent window
			$txt = '<a href="'. $mitem->link .'" class="'. $menuclass .'" '. $id .'>'. $txt .'</a>';
			break;
		}
		return $txt;
	}
}
?>