<?php //jph modified to 1.5 form
/**
* @version $Id: mod_ja_transmenu.php
* @copyright (C) 2005 JoomlArt.com
* @license This module is licenced under the Creative Commons Attribution-NonCommercial-ShareAlike License, 2005m
*/
//defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );
defined('_JEXEC') or die('Restricted access');
//load css file 1.5 style
/**
/* Loads main class file
*/
//1.5 mode of getting these paths..
//JURI :: base() = site root url
//dirname(__FILE__) = current folder server filepath, no end slash
$params->set( 'module_name', 'JA Trans Menu' );
$params->set( 'absPath', dirname(__FILE__) );
$params->set( 'LSPath', JURI :: base() . '/modules/mod_transmenu/transmenu' );
include_once (dirname(__FILE__).DS.'transmenu'.DS.'jamenu.php');
//global $my legacy fix see jamenu.php code;
//jph global $database fix, this seems to be the same as the 1.0 $database object
$database_x	=& JFactory::getDBO();
$jamenu= new JAMenu ($database_x, $params);

$jamenu->genMenu ();
?>

