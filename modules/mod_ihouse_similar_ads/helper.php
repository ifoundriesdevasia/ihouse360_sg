﻿<?php
/**
 * Helper class for Slider Gallery Module
 * 
 * @package    Joomla
 * @subpackage Modules
 * @link http://www.yashvyas.in
 * @license        GNU/GPL
 * mod_galleryview is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */
class modiHouseSimilarAdsHelper
{
	function getPropertyType($adsid = 0) {
		if(!$adsid)
			return false;
		
		$db		=& 	JFactory::getDBO();
		
		$query 	= 	' SELECT property_type FROM #__ihouse_ads '
					.	" WHERE id = '$adsid' ";
					
			$db->setQuery( $query );
			$tmp = $db->loadResult();		
		
		return $tmp;
		
	}
	function getList() {
		$db		=& 	JFactory::getDBO();
		
		$ads_id = JRequest::getVar('id');

		$query 	= 	' SELECT ask_price, no_of_room , ad_type, property_type FROM #__ihouse_ads '
					.	" WHERE id = '$ads_id' ";
		
			$db->setQuery( $query );
			$tmp = $db->loadObject();
			
		$price 		= $tmp->ask_price;
		$no_room 	= $tmp->no_of_room; 
		$adtype		= $tmp->ad_type;
		$proptype	= $tmp->property_type;
		
		
		$where = array(); 
		
		$where[] 		= 	" property_type = '$proptype' ";
		
		if(!empty($price)) { 
			$low		=	$price - ($price * 0.1);
			$high		=	$price + ($price * 0.1);
			
			$where[] 	= 	" ask_price >= '$low' ";
			$where[]	=	" ask_price <= '$high' ";
			
		}
		
		if($no_room) {
			$where[]	=	" no_of_room = '$no_room' ";
		}
		
		
		$where[] 	=	" ad_type = '$adtype' ";
		$where[]	= 	" id != '$ads_id' ";
		$where[]	=	" publish = 1 ";
		
		$where 		= ( count( $where ) ? ' WHERE (' . implode( ' AND ', $where ).')' : '' );
		
		
		
		$query 	= 	'SELECT * FROM #__ihouse_ads '
					.	$where
					.	" ORDER BY RAND() "
					. 	" LIMIT 6 ";
					
			$db->setQuery( $query );
			$row = $db->loadObjectList();
			
		return $row;
	}
}