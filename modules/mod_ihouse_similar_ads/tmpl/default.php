﻿<?php // no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<div class="component-pos">
	<div id="sub-small-component-border">
    	<div id="sub-small-component">
        	<div id="component-headerpos">
            	<div id="component-logo">
                	<div id="component-title"><span class="chinese_text1">你可能会感兴趣的类似的房源</span>&nbsp;<i>Similiar ads you might be interested in</i></div>
                </div>
            </div>
        </div>
        <div style="clear:both;"></div>
        <div id="sub-small-component-content" >
                    	
                        <div id="">
                            <div id="">
                            <div>
                            <table class="table-border-ihouse" width="100%" cellspacing=0 cellpadding=0 >
                            <tr align="center" style="background-color:#f0f0f0;">
                            	<th width="40%" align="left" style="padding:10px;"><span style="color:#FF8200">房源名</span></th>
                                <th width="20%"><span style="color:#FF8200">户型</span></th>
                                <th width="10%"><span style="color:#FF8200">
								<?php if($proptype == '组屋') : ?>
                                	 地区
								<?php elseif($proptype == '公寓') : ?>
                                     邮区
                                <?php endif; ?>     
                                     </span>
                                </th>
                                <th width="15%"><span style="color:#FF8200">面积</span></th>
                                <th width="15%"><span style="color:#FF8200">价格(SGD)</span></th>
                            </tr>
                            <?php if(!empty($row)) : ?>
                            <?php foreach($row as $p) : ?>
                            <tr align="center">
                            	<td id="tabtext-padding1" align="left" style="padding:10px;">
                                	 <a href="<?php echo JRoute::_('index.php?option=com_ads&view=adsdetails&id='.$p->id.'&Itemid=135'); ?>">
									<?php echo $p->ad_title ?>
                                    </a>
                                </td>
                                <td>
								<?php 
								echo $p->no_of_room.'房'.$p->no_of_hall.'厅';
								?>
                                </td>
                                <td>
                                <?php if($proptype == '组屋') : ?>
                                	 <?php echo ($p->hdb_town_id)?$p->hdb_town_id:'N/A'; ?>
								<?php elseif($proptype == '公寓') : ?>
                                     <?php echo ($p->property_district_id)?$p->property_district_id:'N/A'; ?>
                                <?php endif; ?>     
									
                                </td>
                                <td><?php echo ($p->size)?number_format($p->size).' 平方尺':'N/A'; ?></td>
                                <td><?php echo ($p->ask_price)?number_format($p->ask_price):'N/A'; ?></td>
                            </tr>
                            <?php endforeach; ?>
                            <?php else : ?>
                            <tr align="center">
                            	<td id="tabtext-padding1" align="center" style="padding:10px;" colspan="5">
                                	No Similar Ads
                                </td>
                            </tr>
                            <?php endif; ?>
                            </table> 
                            </div>
                            </div>
                    	</div>
                        
                        
		</div>
	</div>
</div>