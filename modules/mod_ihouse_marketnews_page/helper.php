﻿<?php
/**
 * Helper class for Slider Gallery Module
 * 
 * @package    Joomla
 * @subpackage Modules
 * @link http://www.yashvyas.in
 * @license        GNU/GPL
 * mod_galleryview is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */
class modiHouseMarketNewsPageHelper
{
    function getPageHTML()
    {
		global $mainframe;
		
		$params		=	$mainframe;
		
		$view		= 	JRequest::getCmd('view');
		$slg		=	JRequest::getVar('id');

		$tmp		=	explode(':',$slg);
		
		$uid		=	$tmp[0];
		
		if ($view == 'article')
		{

			$html 		= '';
			$db 		= & JFactory::getDBO();
			$user		= & JFactory::getUser();
			$nullDate	= $db->getNullDate();

			$date		=& JFactory::getDate();
			$config 	= & JFactory::getConfig();
			$now 		= $date->toMySQL();

			$option 	= 'com_content';
			$canPublish = $user->authorize('com_content', 'publish', 'content', 'all');

			$row->catid =	38;
			
			$xwhere = ' AND ( a.state = 1 OR a.state = -1 )' .
			' AND ( publish_up = '.$db->Quote($nullDate).' OR publish_up <= '.$db->Quote($now).' )' .
			' AND ( publish_down = '.$db->Quote($nullDate).' OR publish_down >= '.$db->Quote($now).' )';

			// array of articles in same category correctly ordered
			$query = 'SELECT a.id,'
			. ' CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(":", a.id, a.alias) ELSE a.id END as slug,'
			. ' CASE WHEN CHAR_LENGTH(cc.alias) THEN CONCAT_WS(":", cc.id, cc.alias) ELSE cc.id END as catslug'
			. ' FROM #__content AS a'
			. ' LEFT JOIN #__categories AS cc ON cc.id = a.catid'
			. ' WHERE a.catid = ' . (int) $row->catid
			. ($canPublish ? '' : ' AND a.access <= ' .(int) $user->get('aid', 0))
			. $xwhere;
			
				$db->setQuery($query);
				$list = $db->loadObjectList('id');

			
			// this check needed if incorrect Itemid is given resulting in an incorrect result
			if ( !is_array($list) ) {
				$list = array();
			}

			reset($list);

			// location of current content item in array list
			$location = array_search($uid, array_keys($list));
			
			$rows = array_values($list);
			
			$row->prev = null;
			$row->next = null;

			if ($location -1 >= 0) 	{
				// the previous content item cannot be in the array position -1
				$row->prev = $rows[$location -1];
			}

			if (($location +1) < count($rows)) {
				// the next content item cannot be in an array position greater than the number of array postions
				$row->next = $rows[$location +1];
			}
	
			$pnSpace = "";
			if (JText::_('&lt') || JText::_('&gt')) {
				$pnSpace = " ";
			}

			if ($row->prev) {
				$row->prev = JRoute::_(ContentHelperRoute::getArticleRoute($row->prev->slug, $row->prev->catslug));
			} else {
				$row->prev = '';
			}
	
			if ($row->next) {
				$row->next = JRoute::_(ContentHelperRoute::getArticleRoute($row->next->slug, $row->next->catslug));
			} else {
				$row->next = '';
			}
	
	
			// output
			if ($row->prev || $row->next)
			{
				$html = '
				<table align="center" class="pagenav">
				<tr>'
				;
				if ($row->prev)
				{
					$html .= '
					<th class="pagenav_prev">
						<a href="'. $row->prev .'" style="color:#ff8200;text-decoration:none;">'
							. JText::_( '<上一篇>' ) . '</a>
					</th>'
					;
				}
	
				if ($row->prev && $row->next)
				{
					$html .= '
					<td width="50">&nbsp;
						
					</td>'
					;
				}
	
				if ($row->next)
				{
					$html .= '
					<th class="pagenav_next">
						<a href="'. $row->next .'" style="color:#ff8200;text-decoration:none;">'
							.  JText::_( '<下一篇>' ) .'</a>
					</th>'
					;
				}
				
				$html .= '
				</tr>
				</table>'
				;
	
				
			}
		}
	
	return $html;
    }
}