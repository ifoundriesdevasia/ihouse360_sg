<?php // no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

    	<div id="module-padding">
            <div id="module-pos4">
                <div id="sub-small-module-border">
                    <div id="sub-small-module">
                        <div id="module-headerpos">
                            <div id="module-logo">
                                <div id="module-title"><span class="chinese_text1">产业新闻</span>&nbsp;<i>News</i></div>
                            </div>
                        </div>
                    </div>
                    <!-- url index.php?option=com_k2&view=item&layout=item&id=x -->
                    <!-- from K2 -->
                    <div id="sub-small-module-content">
                    	
                        <div id="liststyle-padding">
                            <div id="liststyle1">
                            <?php if(empty($rows)) { ?>
                              <ul>
                                <li>
                                    No News
                                </li>
                               
                              </ul>
                            <?php } else {  ?>
                            <ul>
                            <?php foreach($rows as $r) : ?>
            
            	<li style="padding:3px;line-height:17px;">
            	<a style="color:#062284;text-decoration:none;" href="<?php echo JRoute::_('index.php?option=com_content&view=article&id='.$r->id.':'.$r->alias.'&catid='.$r->catid.':market-news') ?>"><?php echo $r->title ?></a> <span style="color:#525252;">(<?php echo date('d/M/Y',strtotime($r->publish_up)); ?>)</span>
            	</li>                           
            
            <?php endforeach; ?>
            				</ul>
                            <?php } ?>  
                            </div>
                            <div style="text-align:right;"><a style="text-decoration:none;color:#ff8200;" href="<?php echo JRoute::_('index.php?option=com_content&view=category&id='.$marketnews_id.'&Itemid=69') ?>">更多 ></a></div>
                    	</div>
                        
                        
                    </div>
                </div>
            </div>
        </div>