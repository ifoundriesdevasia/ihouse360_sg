﻿<?php
/**
 * Helper class for Slider Gallery Module
 * 
 * @package    Joomla
 * @subpackage Modules
 * @link http://www.yashvyas.in
 * @license        GNU/GPL
 * mod_galleryview is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */
class modiHousePopularHelper
{
	
	function getList() {
		$db		=& 	JFactory::getDBO();
		
		$query 	= 	" SELECT * "
					.	" FROM #__ihouse_popular AS po " 
					.	" LEFT JOIN #__ihouse_property AS p ON p.id = po.property_id " 
					.	" WHERE po.type = 'total' "
					.	" ORDER BY po.counter DESC LIMIT 17 ";
				
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
			
		return $rows;
	}
}