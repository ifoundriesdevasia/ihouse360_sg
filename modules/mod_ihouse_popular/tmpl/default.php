<?php // no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<div id="module-padding9" style="width:100%;">
            <div id="module-pos1" style="width:100%;">
            	<div id="small-module-border" style="width:100%;">
                	<div id="small-module" style="width:100%;">
                    	<div id="module-headerpos">
                            <div id="module-title"><span class="chinese_text1">本周热门公寓</span>&nbsp;<i>Popular Condos This Week</i></div>
                        </div>
                    </div>
                    
                    <div id="small-module-content" style="width:100%;">
                    	<div id="module-content-padding" style="width:90%;">
                            <div id="small-module-content-cont2">
                                <div id="module-content-directory1">
                                    <div id="module-content-padding4">
                                    	<div style="float: left;"><strong>楼盘名</strong></div>
                                        <div style="float: right;padding-right:20px;"><strong>浏览次数</strong>&nbsp;&nbsp;</div>
                                    </div>
                                </div>
                            </div>
                            <?php 	$i = 1; 
								if(!empty($rows)) : 
									foreach($rows as $r) : ?>
                            <div id="small-module-content-cont2" style="width:99.3%;">
                            	<div id="small-module-content-directory-border">
                                <div id="module-content-directory2">
                                    <div id="module-content-padding5">
                                    	<div>
                                    		<div id="module-content-img">
                                            	<div class="popular_img"><div style="margin:1px 0 0 <?php echo ($i >= 10)?'0':'5px'?>;color:#fff;"><?php echo $i ?></div></div>
                                            </div>
                                            <div id="small-module-content-directory-text1">
                                            <a href="<?php echo JRoute::_('index.php?option=com_ihouse&view=ihouse&layout=property_detail&id='.$r->property_id.'&postcode='.$r->postcode) ?>">
                                            	<?php echo $r->name_en ?></a><br />
												<span class="color-text1"><?php echo $r->name_ch; ?></span>
                                            </div>
                                            <div id="small-module-content-directory-text2">
                                            	<?php echo $r->counter+80; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                            
                            <?php $i++; endforeach; 
									else :
							?>
                            <div style="margin:10px;float:left;">
                            	No Popular Condo This Week
                            </div>
                            <?php		
									endif;
							?>
                            
                            
                            <div style="clear:both"></div>
                            <!--<div style="float:right;margin-top:5px;">
                            	<a href="<?php echo JRoute::_('index.php?option=com_ihouse&task=popularlisting') ?>" style="color:#ff8200;text-decoration:none;">显示更多 >
                                </a>
                            </div>
                            -->
                        </div>
                    </div>
                </div>
            </div>
            </div>      