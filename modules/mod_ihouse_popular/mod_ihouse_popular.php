<?php

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// Include the syndicate functions only once
require_once( dirname(__FILE__).DS.'helper.php' );

$layout = $params->get('layout', 'default');
$path = JModuleHelper::getLayoutPath('mod_ihouse_popular', $layout);

$rows 			= modiHousePopularHelper::getList();


$db	=	JFactory::getDBO();

if (file_exists($path)) {
	require($path);
}