﻿<?php // no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>
<script type="text/javascript">

function loadUserRatingCurrent() {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();			
	$('property_review_loader1').setStyle('display','');
	
	var url = 'index.php?option=com_ihouse&task=userratingoverall&r=' + unixtime_ms;		
	var req = new Ajax(url, {
	   					data		: 	
									{
										'id' 	:	'<?php echo JRequest::getCmd('id'); ?>'
									},
						method		: "get",
		    			onSuccess	: function(data) {
								$('userRatingMsg').setHTML('');
								$('user-rating-happening').setHTML(data);
								$('property_review_loader1').setStyle('display','none');
		   				},
						evalScripts : true
				}).request();				
}

function loadUserRatingForm() {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();			
	
	$('userRatingMsg').setHTML('');
	$('property_review_loader1').setStyle('display','');
	
	var url = 'index.php?option=com_ihouse&task=userratingform&r=' + unixtime_ms;		
	var req = new Ajax(url, {
	   					data		: 	
									{
										'id' : '<?php echo JRequest::getCmd('id'); ?>'
									},
						method		: "get",
		    			onSuccess	: function(data) {
								$('user-rating-happening').setHTML(data);
								$('property_review_loader1').setStyle('display','none');
		   				},
						evalScripts : true
				}).request();				
}

function loadUpdatedForStatisticTotal() {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();			
	
	var url = 'index.php?option=com_ihouse&task=updatestatistictotalpropertydetail&r=' + unixtime_ms;		
	var req = new Ajax(url, {
	   					data		: 	
									{
										'id' 		: '<?php echo JRequest::getCmd('id'); ?>',
										'postcode' 	: '<?php echo JRequest::getCmd('postcode'); ?>'
									},
						method		: "get",
		    			onSuccess	: function(data) {
								$$('.ajax-updated-stats').setHTML(data);
		   				},
						evalScripts : true
				}).request();				
}

function submitRatingForm() {
	$('property_review_loader1').setStyle('display','');
	$('userRatingMsg').setHTML('');
	$('userRatingForm').send({
		onComplete	:	function(data) {
			
			var obj1 	=	Json.evaluate(data);
			var status	=	obj1.status;
			
			if(!status) {
				$('userRatingMsg').setHTML('You already submit a rating');
			} else {
				loadUserRatingCurrent();
				loadUpdatedForStatisticTotal();
			}
			$('property_review_loader1').setStyle('display','none');
			
			
		}
	});	
}

loadUserRatingCurrent();
loadUpdatedForStatisticTotal();
</script>
<div id="userRatingMsg" style="float:left;color:red;"></div>

<div id="user-rating-happening">

</div>