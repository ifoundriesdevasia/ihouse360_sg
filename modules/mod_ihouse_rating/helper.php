﻿<?php
/**
 * Helper class for Slider Gallery Module
 * 
 * @package    Joomla
 * @subpackage Modules
 * @link http://www.yashvyas.in
 * @license        GNU/GPL
 * mod_galleryview is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */
class modiHouseRatingHelper
{
    function getSearch( $params )
    {
		/* START - JRequest::getVar() */
		
		/* END - JRequest::getVar() */

		$db		=& 	JFactory::getDBO();

		$query 	= 'SELECT * FROM #__ihouse_property ';
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		return $rows;
    }
	

	function getNearestSchoolListHTML() {
		$db		=& 	JFactory::getDBO();

		$where 	= ' WHERE ( type != "primary_top30" OR type != "secondary_top30") '; 
		$query 	= 'SELECT * FROM #__ihouse_school '
					. $where ;
					
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		$html = '';
		
		$html .= '<select name="nearest_school" style="width:100px;" class="searchselect">';
		$html .= '	<option value="">选择学区</option>';
		foreach($rows as $r) :
			$t = JRequest::getVar('nearest_school', '');
		
			$selected = '';
			if($t == $r->name_ch) 
				$selected = 'selected="selected"';
		
			$html .= '	<option value="'.$r->name_ch.'" '.$selected.' >'.$r->name_ch.' '.$r->name_en.'</option>';
		endforeach;
		
		$html .= '</select>';
		
		return $html;
		
	}
	
	function getPropertyType($type = 'checkbox') {
		$db		=& 	JFactory::getDBO();

		$exist = array('Condo', 'HDB');
		
		$where = 
		
		$query 	= 'SELECT * FROM #__ihouse_property_type'
					. ' WHERE LOWER(name_en) = "hdb" OR LOWER(name_en) = "condo" ';
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		$html = '';
		
		
		
		foreach($rows as $r):
			$t = JRequest::getVar(strtolower($r->name_en), '');
		
			$checked = '';
			if($t == $r->name_ch) 
				$checked = 'checked="checked"';
		
			$html .= '<input type="checkbox" value="'.$r->name_ch.'" name="'.strtolower($r->name_en).'" '.$checked.' />'.$r->name_en;
		endforeach;
		
		return $html;
	}
	
}