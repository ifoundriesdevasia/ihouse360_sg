<?php
/**
 * Helper class for Slider Gallery Module
 * 
 * @package    Joomla
 * @subpackage Modules
 * @link http://www.yashvyas.in
 * @license        GNU/GPL
 * mod_galleryview is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */
class modiHouseType4
{
    function getStart( $params )
    {
		$db		=& 	JFactory::getDBO();

		$query 	= 'SELECT * FROM #__ihouse_mapping_sector';
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		return $rows;
    }
	
	
	function getRents() {
		
		$db		=& 	JFactory::getDBO();

		$query 	= 	" SELECT t.*,a.*,i.img1 FROM #__ihouse_ads_type AS t " 
					.	" LEFT JOIN #__ihouse_ads AS a ON a.id = t.ads_id "
					.	" LEFT JOIN #__ihouse_ads_image AS i ON i.ads_id = t.ads_id "
					.	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = t.subscription_id "
					.	" WHERE t.ads_type_config = 4 " 
					.	" AND t.property_type = 'rent' "
					.	" AND cb.expiry_date >= '".date("Y-m-d H:i:s", time())."' "
					.	" LIMIT 3 " ;
					
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		return $rows;
	}
	
	function getSales() {
		
		$db		=& 	JFactory::getDBO();

		$query 	= 	" SELECT t.*,a.*,i.img1 FROM #__ihouse_ads_type AS t " 
					.	" LEFT JOIN #__ihouse_ads AS a ON a.id = t.ads_id "
					.	" LEFT JOIN #__ihouse_ads_image AS i ON i.ads_id = t.ads_id "
					.	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = t.subscription_id "
					.	" WHERE t.ads_type_config = 4 "
					.	" AND t.property_type = 'sell' " 
					.	" AND cb.expiry_date >= '".date("Y-m-d H:i:s", time())."' "
					.	" LIMIT 3 " ;
					
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		return $rows;
	}
}