﻿<?php

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// Include the syndicate functions only once
require_once( dirname(__FILE__).DS.'helper.php' );

$layout = $params->get('layout', 'default');
$path = JModuleHelper::getLayoutPath('mod_ihouse_type4', $layout);

$rents = modiHouseType4::getRents();
$sales = modiHouseType4::getSales();

$read_more_rent = '';
$read_more_sale = '';

if(count($rents) == 3)
	$read_more_rent = '<div style="float: right; margin: 5px 5px 0 0;"><a style="color: rgb(255, 130, 0); text-decoration: none;" href="#">显示更多 &gt;</a></div>';
	
if(count($sales) == 3)
	$read_more_sale = '<div style="float: right; margin: 5px 5px 0 0;"><a style="color: rgb(255, 130, 0); text-decoration: none;" href="#">显示更多 &gt;</a></div>';
	

if (file_exists($path)) {
	require($path);
}