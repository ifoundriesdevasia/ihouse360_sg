﻿<?php // no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

 <!--Start of module-->
    	<div id="module-padding">
            <div id="module-pos4">
                <div id="sub-small-module-border">
                    <div id="sub-small-module">
                        <div id="module-headerpos">
                            <div id="module-logo">
                                <div id="module-title">Featured Properties</div>
                            </div>
                        </div>
                    </div>
                    
                    <div id="sub-small-module-content">
                    	
                        <div>
                        	<!--Start of tabs-->
                            <div id="module-content-padding6">
                            	<div class="tabber">
                                     <div class="tabbertab" style="width:218px">
                                      <h2>出租</h2>
                                      <div class="tabItem1">
                                        <div style="padding-top:10px;">
                                            
                                            <!--Start-->
                                        <?php if(!empty($sales)) : ?>    
                                        <?php foreach($sales as $s) : ?>  
                                        	<?php //echo print_r($s); ?>  
                                            <div id="tabcont-sub">
                                                    <div id="tabimg">
                                                    <?php if(!empty($s->img1)) : ?>
                                                    <img width="85px" height="69px" src="<?php echo JRoute::_('images/ihouse/ads/'.$s->ads_id.'/'.$s->img1) ?>" />
                                                    <?php else : ?>
                                                    <img width="85px" height="69px" src="<?php echo JRoute::_('templates/main/images/listingimg.jpg') ?>" />
                                                    <?php endif; ?>    
                                                    </div>
                                                
                                                <div id="tabtext-cont1">
                                                    <div id="tabtext-padding1">
                                                    <a href="<?php echo JRoute::_('index.php?option=com_ads&view=saleadsdetails&id='.$s->ads_id) ?>">
													<?php echo $s->ad_title ?>
                                                    </a>
                                                    </div>
                                                    <div id="tabtext-font">户型：<?php echo $s->no_of_room ?>房<?php echo $s->toilet ?>厅<br />楼层：<?php echo date("m/d",strtotime($s->posting_date)) ?> 层<br />价格：SGD$ <?php echo $s->ask_price ?></div>
                                                </div>
                                            </div>
                                            <!--End-->
                                        <?php endforeach; ?>    
                                        
                                        <?php else : ?>
                                        	No Ads
                                        <?php endif; ?>
                                        <?php echo $read_more_sale; ?>    
                                      	</div>
                                      </div>
                                     </div>
                                
                                
                                     <div class="tabbertab" style="width:218px">
                                      <h2>出售</h2>
                                      <div class="tabItem1">
                                        <div style="padding-top:10px;">
                                                <!--Start-->
                                         <?php if(!empty($rents)) : ?>    
                                        <?php foreach($rents as $s) : ?>  
                                        	<?php //echo print_r($s); ?>  
                                            <div id="tabcont-sub">
                                                    <div id="tabimg">
                                                    <?php if(!empty($s->img1)) : ?>
                                                    	<img width="85px" height="69px" src="<?php echo JRoute::_('images/ihouse/ads/'.$s->ads_id.'/'.$s->img1) ?>" />
                                                    <?php else : ?>
                                                        <img width="85px" height="69px" src="<?php echo JRoute::_('templates/main/images/listingimg.jpg') ?>" />
                                                    <?php endif; ?>    
                                                    </div>
                                                
                                                <div id="tabtext-cont1">
                                                    <div id="tabtext-padding1">
													<a href="<?php echo JRoute::_('index.php?option=com_ads&view=rentadsdetails&id='.$s->ads_id) ?>">
													<?php echo $s->ad_title ?>
                                                    </a>
                                                    </div>
                                                    <div id="tabtext-font">户型：<?php echo $s->no_of_room ?>房<?php echo $s->toilet ?>厅<br />楼层：<?php echo date("m/d",strtotime($s->posting_date)) ?> 层<br />价格：SGD$ <?php echo $s->ask_price ?></div>
                                                </div>
                                            </div>
                                            <!--End-->
                                        <?php endforeach; ?>    
                                        
                                        <?php else : ?>
                                        	No Ads
                                        <?php endif; ?>
                                         <?php echo $read_more_rent; ?>    
                                        </div>
                                      </div>
                                     </div>
                                
                                </div>

                             </div>
                             <!--End of tabs-->
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!--End of module-->