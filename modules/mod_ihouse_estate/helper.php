﻿<?php
/**
 * Helper class for Slider Gallery Module
 * 
 * @package    Joomla
 * @subpackage Modules
 * @link http://www.yashvyas.in
 * @license        GNU/GPL
 * mod_galleryview is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */
class modiHouseEstateHelper
{
    function getNew()
    {
		$db		=& 	JFactory::getDBO();
		
		$orderby	= ' ORDER BY a.posting_date DESC '; 
		
		$query			= 	"SELECT a.* FROM #__ihouse_ads AS a "
							.	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = a.subscription_id "
							.	" WHERE cb.status = 'A' "
							.	" AND a.property_category = '新房' "
							.	" AND a.publish = 1 "
							.	$orderby
							.	" LIMIT 3 ";
							
		/*$query 	= 	" SELECT * FROM #__ihouse_ads WHERE property_category = '新房' "
					.	" LIMIT 3 "	;
					*/
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		
					
		return $rows;
    }
	
	function getSecond()
    {
		$db		=& 	JFactory::getDBO();
		
		$orderby	= ' ORDER BY a.posting_date DESC '; 
		
		/*$query 	= 	" SELECT * FROM #__ihouse_ads WHERE property_category = '其它' "
					.	" LIMIT 3 "	;*/
					
		$query			= 	"SELECT a.* FROM #__ihouse_ads AS a "
							.	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = a.subscription_id "
							.	" WHERE cb.status = 'A' "
							.	" AND a.property_category = '其它' "
							.	" AND a.ad_type = 'sell' "
							.	" AND a.publish = 1 "
							.	$orderby
							.	" LIMIT 3 ";	
							
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		return $rows;
    }
	
	function getRent()
    {
		$db		=& 	JFactory::getDBO();
		
		/*$query 	= 	" SELECT * FROM #__ihouse_ads WHERE ad_type = 'rent' "
					.	" LIMIT 3 "	;
					*/
		
		$orderby	= ' ORDER BY a.posting_date DESC '; 
					
		$query			= 	"SELECT a.* FROM #__ihouse_ads AS a "
							.	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = a.subscription_id "
							.	" WHERE cb.status = 'A' "
							.	" AND a.ad_type = 'rent' "
							.	" AND a.publish = 1 "
							.	$orderby
							.	" LIMIT 3 ";	
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		return $rows;
    }
}