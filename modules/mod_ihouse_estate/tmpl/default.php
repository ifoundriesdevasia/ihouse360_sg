<?php // no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>
<script type="text/javascript">
function toggle_estate(id) {
	$$('.menuestate').removeClass('tabberactive');
	$('tab_estate' + id).addClass('tabberactive');
	
	$$('.tabbertabestate').addClass('tabbertabhide');
	$('div_estate' + id).removeClass('tabbertabhide');
}
</script>
<!--Start of module-->
                <div id="module-pos" style="min-height:300px;">
                    <div id="small-module-border">
                    
                        <div id="small-module">
                            <div id="module-headerpos">
                                    <div id="module-title"><font style="font-size:15px;">最新房屋</font> Latest Real Estate</div>
                            </div>
                        </div>
                        
                        <div id="small-module-content" style="min-height:420px;">
                        	
                            <!--Start of tabs-->
                            <div id="module-content-padding">
<div class="tabberlive">
	<ul class="tabbernav">
    	<li id="tab_estate1" class="menuestate tabberactive">
        	<a href="javascript:toggle_estate('1');" style="margin:0;">新楼盘</a>
        </li>
        <li id="tab_estate2" class="menuestate ">
        	<a href="javascript:toggle_estate('2');" style="margin:0;">二手房</a>
        </li>
        <li id="tab_estate3" class="menuestate ">
        	<a href="javascript:toggle_estate('3');" style="margin:0;">出租</a>
        </li>
    </ul>
                                     
    <div id="div_estate1" class="tabbertabestate">
    	<div class="tabItem1">
         <?php if(empty($row1)) : ?>
                           			 <div class="content-border" style="width:100%;float:left;border-bottom:1px dotted #ccc;">
                             			<div style="float:left;padding:10px 0 10px 0;width:100%;">
                            			No Ads
                                		</div>
                            		</div>   
                            		 <?php else : ?>
                                      <?php foreach($row1 as $row) : ?>
                                      <div style="clear:both"></div>
                            			<div class="content-border" style="width:100%;float:left;border-bottom:1px dotted #ccc;">
                                			<div style="float:left;padding:10px 0 10px 0;width:100%;">
                                			<table width="100%" cellspacing=0 cellpadding=0 >
                                				<tr style="vertical-align:top;">
                                				  <td colspan="2"><div id="tabtext-padding1"> <a href="<?php echo JRoute::_('index.php?option=com_ads&view=adsdetails&id='.$row->id.'&Itemid=135'); ?>"><?php echo $row->ad_title ?></a></div></td>
                               				  </tr>
                                				<tr style="vertical-align:top;">
                                					<td width="30%">
<?php 
		$query = " SELECT ads_id, sess_id, name FROM #__ihouse_ads_image WHERE ads_id = '".$row->id."' AND is_primary = 1 ";
			$db->setQuery( $query );
			$tmps = $db->loadObject();
		
		if(empty($tmps)) {
			$query = " SELECT image FROM #__ihouse_ads_image2 WHERE ads_id = '".$row->id."' ORDER BY RAND() LIMIT 1 ";
			$db->setQuery( $query );
			$ihouse_img = $db->loadResult();
			
			if(empty($ihouse_img)) {
				$ihouse_img = JRoute::_('templates/main/images/thumb-noimage80.jpg');
			}else{
				$ihouse_img = 'image.php?size=80&type=3&path='.$row->postcode.'/images/'.$ihouse_img;
			}
		}
?>
		<div class="sidebaradsimgbg" style="height: 70px;">
      	<a href="<?php echo JRoute::_('index.php?option=com_ads&view=adsdetails&id='.$row->id.'&Itemid=135'); ?>">
    	<?php if(!empty($tmps->name)) : ?>
    
     	<img width="80px" height="60px" src="<?php echo JRoute::_('images/ihouse/ads_images/'.$tmps->sess_id.'/small-'.$tmps->name) ?>" />
    
    	<?php elseif(!empty($ihouse_img)) : ?>
    
    	<img style="border:none;" src="<?php echo $ihouse_img ?>" />
    
		<?php else : ?>
    
    <img src="<?php echo JRoute::_('templates/main/images/thumb-noimage80.jpg') ?>"/>
    
    <?php endif; ?>
    </a>
		</div>
                                    </td>
                                    <td width="70%"> 
                                    <div id="tabtext-cont1" style="width:170px;">
                                    	
                                       <div class="color-text1">地址：<?php echo ($row->street_name)?$row->street_name:'N/A'; ?><br />户型：<?php echo $row->no_of_room ?>房<?php echo $row->no_of_hall ?>厅<br>楼层：<?php echo ($row->floor_level)?$row->floor_level:'N/A'; ?><br>价格：<?php echo ($row->ask_price)?'S$ '.number_format($row->ask_price):''; ?></div>
                                    </div>
                                	<br />
                                    
                                	</td>
                                </tr>
                                </table>
                                </div>
                            </div>
                            
									  <?php endforeach; ?>
                                       <?php endif; ?>
                                       <div style="clear:both"></div>
                            <div style="float:right;margin-top:5px;margin-right:5px;"><a href="<?php echo JRoute::_('index.php?option=com_ihouse&list=new')?>" style="color:#ff8200;text-decoration:none;">显示更多 ></a></div>	
   			
   
        </div>
    </div>
    <div id="div_estate2" class="tabbertabestate tabbertabhide">
        <div class="tabItem1">
		 <?php if(empty($row2)) : ?>
                           			 <div class="content-border" style="width:100%;float:left;border-bottom:1px dotted #ccc;">
                             			<div style="float:left;padding:10px 0 10px 0;width:100%;">
                            			No Ads
                                		</div>
                            		</div>   
                            		 <?php else : ?>
                                      <?php foreach($row2 as $row) : ?>
                                      <div style="clear:both"></div>
                            			<div class="content-border" style="width:100%;float:left;border-bottom:1px dotted #ccc;">
                                			<div style="float:left;padding:10px 0 10px 0;width:100%;">
                                			<table width="100%" cellspacing=0 cellpadding=0 >
                                				<tr style="vertical-align:top;">
                                				  <td colspan="2"><div id="tabtext-padding1"> <a href="<?php echo JRoute::_('index.php?option=com_ads&view=adsdetails&id='.$row->id.'&Itemid=135'); ?>"><?php echo $row->ad_title ?></a></div></td>
                               				  </tr>
                                				<tr style="vertical-align:top;">
                                					<td width="30%">
<?php 
		$query = " SELECT ads_id, sess_id, name FROM #__ihouse_ads_image WHERE ads_id = '".$row->id."' AND is_primary = 1 ";
			$db->setQuery( $query );
			$tmps = $db->loadObject();
		
		if(empty($tmps)) {
			$query = " SELECT image FROM #__ihouse_ads_image2 WHERE ads_id = '".$row->id."' ORDER BY RAND() LIMIT 1 ";
			$db->setQuery( $query );
			$ihouse_img = $db->loadResult();
			
			if(empty($ihouse_img)) {
				$ihouse_img = JRoute::_('templates/main/images/thumb-noimage80.jpg');
			}else{
				$ihouse_img = 'image.php?size=80&type=3&path='.$row->postcode.'/images/'.$ihouse_img;
			}
		}
?>
		<div class="sidebaradsimgbg" style="height: 70px;">
      	<a href="<?php echo JRoute::_('index.php?option=com_ads&view=adsdetails&id='.$row->id.'&Itemid=135'); ?>">
    	<?php if(!empty($tmps->name)) : ?>
    
     	<img width="80px" height="60px" src="<?php echo JRoute::_('images/ihouse/ads_images/'.$tmps->sess_id.'/small-'.$tmps->name) ?>" />
    	<?php elseif(!empty($ihouse_img)) : ?>
    
    	<img style="border:none;" src="<?php echo $ihouse_img ?>" />
        
		<?php else : ?>
    
    <img width="80px" height="60px" src="<?php echo JRoute::_('templates/main/images/thumb-noimage.jpg') ?>"/>
    
    <?php endif; ?>
    </a>
		</div>
                                    </td>
                                    <td width="70%"> 
                                    <div id="tabtext-cont1" style="width:170px;">
                                    	
                                       <div class="color-text1">地址：<?php echo ($row->street_name)?$row->street_name:'N/A'; ?><br />户型：<?php echo $row->no_of_room ?>房<?php echo $row->no_of_hall ?>厅<br>楼层：<?php echo ($row->floor_level)?$row->floor_level:'N/A'; ?><br>价格：<?php echo ($row->ask_price)?'S$ '.number_format($row->ask_price):''; ?></div>
                                    </div>
                                	<br />
                                    
                                	</td>
                                </tr>
                                </table>
                                </div>
                            </div>
									  <?php endforeach; ?>
                                       <?php endif; ?>
                                       <div style="clear:both"></div>
                            <div style="float:right;margin-top:5px;margin-right:5px;"><a href="<?php echo JRoute::_('index.php?option=com_ihouse&list=second')?>" style="color:#ff8200;text-decoration:none;">显示更多 ></a></div>           

        </div>
    </div>
    
    <div id="div_estate3" class="tabbertabestate tabbertabhide">
        <div class="tabItem1">
        	<?php if(empty($row3)) : ?>
                           			 <div class="content-border" style="width:100%;float:left;border-bottom:1px dotted #ccc;">
                             			<div style="float:left;padding:10px 0 10px 0;width:100%;">
                            			No Ads
                                		</div>
                            		</div>   
                            		 <?php else : ?>
                                      <?php foreach($row3 as $row) : ?>
                                      <div style="clear:both"></div>
                            			<div class="content-border" style="width:100%;float:left;border-bottom:1px dotted #ccc;">
                                			<div style="float:left;padding:10px 0 10px 0;width:100%;">
                                			<table width="100%" cellspacing=0 cellpadding=0 >
                                				<tr style="vertical-align:top;">
                                				  <td colspan="2"><div id="tabtext-padding1"> <a href="<?php echo JRoute::_('index.php?option=com_ads&view=adsdetails&id='.$row->id.'&Itemid=135'); ?>"><?php echo $row->ad_title ?></a></div></td>
                               				  </tr>
                                				<tr style="vertical-align:top;">
                                					<td width="30%">
<?php 
		$query = " SELECT ads_id, sess_id, name FROM #__ihouse_ads_image WHERE ads_id = '".$row->id."' AND is_primary = 1 ";
			$db->setQuery( $query );
			$tmps = $db->loadObject();
		if(empty($tmps)) {
			$query = " SELECT image FROM #__ihouse_ads_image2 WHERE ads_id = '".$row->id."' ORDER BY RAND() LIMIT 1 ";
			$db->setQuery( $query );
			$ihouse_img = $db->loadResult();
			
			if(empty($ihouse_img)) {
				$ihouse_img = JRoute::_('templates/main/images/thumb-noimage80.jpg');
			}else{
				$ihouse_img = 'image.php?size=80&type=3&path='.$row->postcode.'/images/'.$ihouse_img;
			}
		}	
?>
		<div class="sidebaradsimgbg" style="height: 70px;">
      	<a href="<?php echo JRoute::_('index.php?option=com_ads&view=adsdetails&id='.$row->id.'&Itemid=135'); ?>">
    	<?php if(!empty($tmps->name)) : ?>
    
     	<img width="80px" height="60px" src="<?php echo JRoute::_('images/ihouse/ads_images/'.$tmps->sess_id.'/small-'.$tmps->name) ?>" />
    	<?php elseif(!empty($ihouse_img)) : ?>
    
    	<img style="border:none;" src="<?php echo $ihouse_img ?>" />
        
		<?php else : ?>
    
    <img width="80px" height="60px" src="<?php echo JRoute::_('templates/main/images/thumb-noimage.jpg') ?>"/>
    
    <?php endif; ?>
    </a>
		</div>
                                    </td>
                                    <td width="70%"> 
                                    <div id="tabtext-cont1" style="width:170px;">
                                    	
                                       <div class="color-text1">地址：<?php echo ($row->street_name)?$row->street_name:'N/A'; ?><br />户型：<?php echo $row->no_of_room ?>房<?php echo $row->no_of_hall ?>厅<br>楼层：<?php echo ($row->floor_level)?$row->floor_level:'N/A'; ?><br>价格：<?php echo ($row->ask_price)?'S$ '.number_format($row->ask_price):''; ?></div>
                                    </div>
                                	<br />
                                    
                                	</td>
                                </tr>
                                </table>
                                </div>
                            </div>
                            
									  <?php endforeach; ?>
                                       <?php endif; ?>
                                       <div style="clear:both"></div>
                            <div style="float:right;margin-top:5px;margin-right:5px;"><a href="<?php echo JRoute::_('index.php?option=com_ihouse&list=rent')?>" style="color:#ff8200;text-decoration:none;">显示更多 ></a></div>   

        </div>
    </div>
                                
</div>                            
                            	
                       
                             </div>
                             <!--End of tabs-->
                            
                            
                            
                        </div>
                    </div>
                </div>
                <!--End of module-->