﻿<?php // no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>
<script type="text/javascript">
function loadAdsUserRatingCurrent() {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();			
	$('property_review_loader1').setStyle('display','');
	
	var url = 'index.php?option=com_ihouse&task=adsuserratingoverall&r=' + unixtime_ms;		
	var req = new Ajax(url, {
	   					data		: 	
									{
										'ads_id' 	:	'<?php echo JRequest::getCmd('id'); ?>'
									},
						method		: "get",
		    			onSuccess	: function(data) {
								$('userRatingAdsMsg').setHTML('');	
								$('ads-user-rating-happening').setHTML(data);
								$('property_review_loader1').setStyle('display','none');
		   				},
						evalScripts : true
				}).request();				
}

function loadAdsUserRatingForm() {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();			
	
	$('property_review_loader1').setStyle('display','');
	
	var url = 'index.php?option=com_ihouse&task=adsuserratingform&r=' + unixtime_ms;		
	var req = new Ajax(url, {
	   					data		: 	
									{
										'ads_id' : '<?php echo JRequest::getCmd('id'); ?>'
									},
						method		: "get",
		    			onSuccess	: function(data) {
								$('userRatingAdsMsg').setHTML('');	
								$('ads-user-rating-happening').setHTML(data);
								$('property_review_loader1').setStyle('display','none');
		   				},
						evalScripts : true
				}).request();				
}


function submitAdsRatingForm() {
	$('property_review_loader1').setStyle('display','');
	$('userRatingAdsMsg').setHTML('');
	$('adsuserRatingForm').send({
		onComplete	:	function(data) {
			var obj1	=	Json.evaluate(data);
			var status	=	obj1.status;
			
			if(status) {
				loadAdsUserRatingCurrent();
			} else {
				$('userRatingAdsMsg').setHTML('You have already submitted a rating');
			}
			$('property_review_loader1').setStyle('display','none');
			
		}
	});	
}
loadAdsUserRatingCurrent();

</script>
<div id="userRatingAdsMsg" style="color:red;">
</div>

<div id="ads-user-rating-happening">

</div>