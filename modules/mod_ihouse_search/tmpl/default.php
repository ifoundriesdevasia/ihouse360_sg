<?php // no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<?php JHTML::stylesheet('jquery-ui-1.7.1.custom.css','modules/mod_ihouse_search/css/', array('media'=>'all')); ?>

<?php JHTML::_('script', 'custom.js', 'modules/mod_ihouse_search/js/'); ?>
<?php JHTML::_('script', 'jquery-1.3.2.js', 'modules/mod_ihouse_search/js/'); ?>
<?php JHTML::_('script', 'jquery-ui-1.7.1.custom.min.js', 'modules/mod_ihouse_search/js/'); ?>

<script type="text/javascript">
function openAdvancedSearchBox(value) {
	if(value == 0) {
		$('ads_search_box1').setStyle('display','');
		$('ads_search_box2').setStyle('display','none');
	}
	else {
		$('ads_search_box1').setStyle('display','none');
		$('ads_search_box2').setStyle('display','');
	}
}
</script>


<script type="text/javascript">
jQuery.noConflict();
jQuery(function() {			
jQuery("#slider").slider({
	value:<?php echo JRequest::getCmd('amount', 3) ?>,
	min: 0,
	max: 10,
	step: 1,
	slide: function(event, ui) {
		jQuery("#amount").val(ui.value);
	}
});

jQuery("#amount").val(jQuery("#slider").slider("value"));
});
</script>


<?php $keything = '请输入华文或英文地名、邮编等等。'; ?>
<div id="main-search-cont">
    	<div id="main-search-bg">
        	<div id="main-search-content">
            	<div id="ads_search_box1" <?php echo ($isSearchPerformed)?' style="margin-top:5px;display:none;" ':' style="margin-top:5px;" '; ?>>
                	<a class="blue_text1" href="javascript:openAdvancedSearchBox(1);">Click here to advance search ( 房屋高级搜索 )</a>
                </div>
                
                
            	<div id="ads_search_box2" <?php echo ($isSearchPerformed)?'':' style="display:none;" '; ?>>
            	<?php $propcls = JRequest::getCmd('property_cls', 'all'); ?>
				<form name="ads_adv_search_form" action="<?php echo JRoute::_("index.php?option=com_ihouse&task=adssearch") ?>" method="get">
                <div style="border-bottom:4px solid #FB9014;float:left;width:100%;">
				
                <?php //echo JRoute::_("index.php?option=com_ihouse&task=adssearch&ads_adv_search=1&property_cls=all") ?>
                <a style="text-decoration:none" onclick="javascript:propertyClassSetup('all')" href="javascript:void(0);">
                <div class="search-menu-text-ads" id="search_all" <?php echo ($propcls == 'all') ? 'style="background-color:#FB9014;color:#fff;"' : '' ?>>
                <div style="padding:3px;">全部楼盘</div>
                </div>
                </a>
                <?php //echo JRoute::_("index.php?option=com_ihouse&task=adssearch&ads_adv_search=1&property_cls=new") ?>
                <a style="text-decoration:none" onclick="javascript:propertyClassSetup('new')" href="javascript:void(0);">
                <div class="search-menu-text-ads" id="search_new" <?php echo ($propcls == 'new') ? 'style="background-color:#FB9014;color:#fff;"' : '' ?>>
                <div style="padding:3px;">新房</div>
                </div>
                </a>
                <?php //echo JRoute::_("index.php?option=com_ihouse&task=adssearch&ads_adv_search=1&property_cls=second") ?>
                <a style="text-decoration:none" onclick="javascript:propertyClassSetup('second')" href="javascript:void(0);">
                <div class="search-menu-text-ads" id="search_second" <?php echo ($propcls == 'second') ? 'style="background-color:#FB9014;color:#fff;"' : '' ?>>
                <div style="padding:3px;">二手房</div>
                </div>
                </a>
                
                <?php //echo JRoute::_("index.php?option=com_ihouse&task=adssearch&ads_adv_search=1&property_cls=rent") ?>
                <a style="text-decoration:none" onclick="javascript:propertyClassSetup('rent')" href="javascript:void(0);">
                <div class="search-menu-text-ads" id="search_rent" <?php echo ($propcls == 'rent') ? 'style="background-color:#FB9014;color:#fff;"' : '' ?>>
                <div style="padding:3px;">出租</div>
                </div>
                </a>
                
                <div style="float:right;margin-top:5px;">
                    <a class="blue_text1" href="javascript:openAdvancedSearchBox(0);">X</a>
                </div>
                
                </div>
                
                <input id="input_prop_cls" type="hidden" name="property_cls" value="<?php echo ($propcls)?$propcls:'all' ?>" />
                <div style="clear:both"></div>

				<div id="main-search-pos">
                	<?php $keyw = JRequest::getVar('keyword', ''); ?>
                	<div style="padding-top: 2px; padding-bottom: 15px;">
                    <input type="text" id="keyword_text_ads_search" name="keyword" class="searchtext" <?php echo ($keyw)?'style="color:#000;"':'style="color:#ccc;"'; ?> onfocus="javascript:searchkeywordInput(this.value, '<?php echo $keything; ?>');" onblur="javascript:searchkeywordInput(this.value, '<?php echo $keything; ?>');" value="<?php echo ($keyw)?$keyw:$keything; ?>"></div>
                    
                    <div style="padding-bottom: 15px;">
                    	<?php echo $prices ?>
                        <?php echo $bedrooms ?>
                        <?php echo $flattype_hdb ?>&nbsp;&nbsp;&nbsp;
                        <?php $tick_fp = JRequest::getCmd('with_floorplan', '') ?>
                        <input type="checkbox" value="1" name="with_floorplan" <?php echo ($tick_fp)?'checked="checked"':'' ?>  />&nbsp;<span class="blue_text1">显示多图房源</span>
                    </div>
                    <div style="padding-bottom: 15px;">
                    
                    	<div style="float:left;">总价偏差幅度(%)&nbsp;&nbsp;</div>
                   		<div style="float:left;margin-left:10px;">
							<div id="slider"></div>
   							<div style="margin-top:10px;">0&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;2&nbsp;&nbsp;&nbsp;&nbsp;3&nbsp;&nbsp;&nbsp;&nbsp;4&nbsp;&nbsp;&nbsp;&nbsp;5&nbsp;&nbsp;&nbsp;&nbsp;6&nbsp;&nbsp;&nbsp;&nbsp;7&nbsp;&nbsp;&nbsp;&nbsp;8&nbsp;&nbsp;&nbsp;&nbsp;9&nbsp;&nbsp;&nbsp;&nbsp;10</div>
							<input type="hidden" id="amount" name="amount" value="" />
                            
						</div>
                    
                    	<div style="float:right;margin-right:30px;">类型&nbsp;<img src="<?php echo JRoute::_('templates/main/images/arrow.png') ?>">
                        
                        	 <?php $prop_type = JRequest::getCmd('prop_type', '') ;?>
                          
							<input type="radio" value="3" name="prop_type" onclick="javascript:dummy1('other');" <?php echo ($prop_type == 3) ? ' checked="checked" ':''; ?> id="prop_type2">&nbsp;<span class="blue_text1">商业/有地</span>&nbsp;&nbsp;
                    		<input type="radio" value="2" name="prop_type" onclick="javascript:dummy1('hdb');" <?php echo ($prop_type == 2) ? ' checked="checked" ':''; ?> id="prop_type2">&nbsp;<span class="blue_text1">组屋</span>&nbsp;&nbsp;
                            <input type="radio" value="1" name="prop_type" onclick="javascript:dummy1('condo')"<?php echo ($prop_type == 1) ? ' checked="checked" ':''; ?> <?php echo ($prop_type == 1)?' checked="checked" ':'' ?> id="prop_type1">&nbsp;<span class="blue_text1">公寓</span>
                            <input type="radio" value="0" name="prop_type" onclick="javascript:dummy1('condo')"<?php echo ($prop_type == 0) ? ' checked="checked" ':''; ?> <?php echo ($prop_type == 0)?' checked="checked" ':'' ?> id="prop_type0" >&nbsp;<span class="blue_text1">All</span>
                        </div>    
                    </div>

                    <div style="float:left;width: 419px; background-color: rgb(242, 242, 242); border: 1px solid rgb(206, 205, 204);">
                    	
                    	<div style="">
                        	<table>
                            	<tr style="vertical-align:middle;">
                                <td>快速定位</td>
                            	<td><?php echo $districts ?><br />Note : Ctrl + Click to select multiple</td>
							<?php //echo $schools; ?>
							<?php //echo $stations; ?>
                            	</tr>
                            </table>    
                        </div>
                  	</div>
                    
                    <div style="float:right;margin-top:80px;">
 						<input type="image" src="<?php echo JRoute::_('templates/main/images/btn-search.png'); ?>" HEIGHT="31" WIDTH="67" BORDER="0" ALT="Submit Form" onclick="javascript:ihseSearch('<?php echo $keything; ?>');return false;" >
    				</div>
                    <div style="clear:both;"></div>
                    
                </div><!--main-search-pos-->
    <input type="hidden" name="ads_adv_search" value="1" />   
    <input type="hidden" name="sessionsave" value="0"  />   
    <input type="hidden" name="option" value="com_ihouse" />
	<input type="hidden" name="task" value="adssearch" />
</form>
			</div><!-- ads_search_box -->
		</div><!--main-search-content-->
	</div>
</div>

