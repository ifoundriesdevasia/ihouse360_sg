<?php

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// Include the syndicate functions only once
require_once( dirname(__FILE__).DS.'helper.php' );

$layout = $params->get('layout', 'default');
$path = JModuleHelper::getLayoutPath('mod_ihouse_search', $layout);

//$rows 			= modiHouseSearchHelper::getSearch($params);

$bedrooms		= modiHouseSearchHelper::getBedRoomListHTML();
$districts		= modiHouseSearchHelper::getDistrictListHTML();
$stations 		= modiHouseSearchHelper::getNearestMRTListHTML();
$schools 		= modiHouseSearchHelper::getNearestSchoolListHTML();
$property_type 	= modiHouseSearchHelper::getPropertyType();

$flattype_hdb	= modiHouseSearchHelper::getFlatTypeListHTML();

$prices			= modiHouseSearchHelper::getPriceListHTML();

$isSearchPerformed = modiHouseSearchHelper::isSearchPerformed();

$session = JFactory::getSession();
		
if (file_exists($path)) {
	require($path);
}