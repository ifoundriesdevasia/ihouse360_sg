﻿<?php
/**
 * Helper class for Slider Gallery Module
 * 
 * @package    Joomla
 * @subpackage Modules
 * @link http://www.yashvyas.in
 * @license        GNU/GPL
 * mod_galleryview is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */
class modiHouseSearchHelper
{
 	function getPropertyCls() {
		$pri		=	JRequest::getCmd('property_cls','all');
		$selected 	=	' selected="selected" ';
		
		
	}
 
 	function isSearchPerformed() {
		$pri		=	JRequest::getCmd('ads_adv_search','0');
		
		if($pri)
			return true;
		
		return false;
	}
 	function getFlatTypeListHTML() {
		
		$pr 		= 	JRequest::getVar('prop_type', 0);
		$row		=	JRequest::getVar('flattype','');
		
		$html = '<select name="flattype" class="searchselect2" id="flattype_hdb" '.(($pr == '2')?'':' style="display:none;" ') .'>';
		$html .= '<option value="">户型不限</option> ';
        $html .= '<option '.(($row == '1房式')?'selected="selected"':'').'value="1房式" >1房式</option>';
        $html .= '<option '.(($row == '2房式')?'selected="selected"':'').'value="2房式" >2房式</option>';
        $html .= '<option '.(($row == '3房式')?'selected="selected"':'').'value="3房式" >3房式</option>';
		$html .= '<option '.(($row == '4房式')?'selected="selected"':'').'value="4房式" >4房式</option>';
        $html .= '<option '.(($row == '5房式')?'selected="selected"':'').'value="5房式" >5房式</option>';
		$html .= '<option '.(($row == '公寓式')?'selected="selected"':'').'value="公寓式" >公寓式</option>';
        $html .= '</select>';
		
		return $html;
	}
 
	function getPriceListHTML() {
		
		$cls		=	JRequest::getVar('property_cls','all');
		
		$pr = JRequest::getVar('prop_type', '0');
		
		$pri		=	JRequest::getVar('prices','');
		$pri_max	=	JRequest::getVar('prices_max','');
		
		$pri1		=	JRequest::getVar('prices_h','');
		$pri1_max	=	JRequest::getVar('prices_h_max','');
		
		$pri2		=	JRequest::getVar('prices_rent','');
		$pri2_max	=	JRequest::getVar('prices_rent_max','');
		
		$selected 	=	' selected="selected" ';
		$html = '';
		
		/* Condo */
		
		$html .= '<select name="prices" class="searchselect2" id="search_price_condo" '.(($pr == '1' || $pr == '0' || $pr == '3')?'':' style="display:none;" ') .' '.(($cls == 'rent')?' style="display:none;" ':'').'>';
		$html .= '	<option value="">总价下限</option>';
		$html .= '	<option value="1" '.(($pri == 1)? $selected :'').'>总价新币 50万以下</option>';
		$html .= '	<option value="2" '.(($pri == 2)? $selected :'').'>总价新币 60 万</option>';
		$html .= '	<option value="3" '.(($pri == 3)? $selected :'').'>总价新币 70 万</option>';
		$html .= '	<option value="4" '.(($pri == 4)? $selected :'').'>总价新币 85 万</option>';
		$html .= '	<option value="5" '.(($pri == 5)? $selected :'').'>总价新币 100 万</option>';
		$html .= '	<option value="6" '.(($pri == 6)? $selected :'').'>总价新币 120 万</option>';
		$html .= '	<option value="7" '.(($pri == 7)? $selected :'').'>总价新币 140 万</option>';
		$html .= '	<option value="8" '.(($pri == 8)? $selected :'').'>总价新币 170 万</option>';
		$html .= '	<option value="9" '.(($pri == 9)? $selected :'').'>总价新币 200 万</option>';
		$html .= '	<option value="10" '.(($pri == 10)? $selected :'').'>总价新币 220 万</option>';
		$html .= '	<option value="11" '.(($pri == 11)? $selected :'').'>总价新币 250 万</option>';
		$html .= '	<option value="12" '.(($pri == 12)? $selected :'').'>总价新币 300 万</option>';
		$html .= '	<option value="13" '.(($pri == 13)? $selected :'').'>总价新币 350 万</option>';
		$html .= '	<option value="14" '.(($pri == 14)? $selected :'').'>总价新币 400 万</option>';
		$html .= '	<option value="15" '.(($pri == 15)? $selected :'').'>总价新币 450 万</option>';
		$html .= '	<option value="16" '.(($pri == 16)? $selected :'').'>总价新币 500 万以上</option>';
		$html .= '</select>';
		
		$html .= '<select name="prices_max" class="searchselect2" id="search_price_condo_max" '.(($pr == '1' || $pr == '0' || $pr == '3')?'':' style="display:none;" ') .' '.(($cls == 'rent')?' style="display:none;" ':'').'>';
		$html .= '	<option value="">总价上限</option>';
		$html .= '	<option value="1" '.(($pri_max == 1)? $selected :'').'>总价新币 50万以下</option>';
		$html .= '	<option value="2" '.(($pri_max == 2)? $selected :'').'>总价新币 60 万</option>';
		$html .= '	<option value="3" '.(($pri_max == 3)? $selected :'').'>总价新币 70 万</option>';
		$html .= '	<option value="4" '.(($pri_max == 4)? $selected :'').'>总价新币 85 万</option>';
		$html .= '	<option value="5" '.(($pri_max == 5)? $selected :'').'>总价新币 100 万</option>';
		$html .= '	<option value="6" '.(($pri_max == 6)? $selected :'').'>总价新币 120 万</option>';
		$html .= '	<option value="7" '.(($pri_max == 7)? $selected :'').'>总价新币 140 万</option>';
		$html .= '	<option value="8" '.(($pri_max == 8)? $selected :'').'>总价新币 170 万</option>';
		$html .= '	<option value="9" '.(($pri_max == 9)? $selected :'').'>总价新币 200 万</option>';
		$html .= '	<option value="10" '.(($pri_max == 10)? $selected :'').'>总价新币 220 万</option>';
		$html .= '	<option value="11" '.(($pri_max == 11)? $selected :'').'>总价新币 250 万</option>';
		$html .= '	<option value="12" '.(($pri_max == 12)? $selected :'').'>总价新币 300 万</option>';
		$html .= '	<option value="13" '.(($pri_max == 13)? $selected :'').'>总价新币 350 万</option>';
		$html .= '	<option value="14" '.(($pri_max == 14)? $selected :'').'>总价新币 400 万</option>';
		$html .= '	<option value="15" '.(($pri_max == 15)? $selected :'').'>总价新币 450 万</option>';
		$html .= '	<option value="16" '.(($pri_max == 16)? $selected :'').'>总价新币 500 万以上</option>';
		$html .= '</select>';
		/* HDB */
		
		$html .= '<select name="prices_h" class="searchselect2" id="search_price_hdb" '.(($pr == '2')?'':' style="display:none;" ') .' '.(($cls == 'rent')?' style="display:none;" ':'').'>';
		$html .= '	<option value="">总价下限</option>';
		$html .= '	<option value="1" '.(($pri1 == 1)? $selected :'').'>总价新币 30 万以下</option>';
		$html .= '	<option value="2" '.(($pri1 == 2)? $selected :'').'>总价新币 35 万</option>';
		$html .= '	<option value="3" '.(($pri1 == 3)? $selected :'').'>总价新币 40 万</option>';
		$html .= '	<option value="4" '.(($pri1 == 4)? $selected :'').'>总价新币 45 万</option>';
		$html .= '	<option value="5" '.(($pri1 == 5)? $selected :'').'>总价新币 50 万</option>';
		$html .= '	<option value="6" '.(($pri1 == 6)? $selected :'').'>总价新币 60 万</option>';
		$html .= '	<option value="7" '.(($pri1 == 7)? $selected :'').'>总价新币 70 万</option>';
		$html .= '	<option value="8" '.(($pri1 == 8)? $selected :'').'>总价新币 80 万以上</option>';
		$html .= '</select>';
		
		$html .= '<select name="prices_h_max" class="searchselect2" id="search_price_hdb_max" '.(($pr == '2')?'':' style="display:none;" ') .' '.(($cls == 'rent')?' style="display:none;" ':'').'>';
		$html .= '	<option value="">总价上限</option>';
		$html .= '	<option value="1" '.(($pri1_max == 1)? $selected :'').'>总价新币 30 万以下</option>';
		$html .= '	<option value="2" '.(($pri1_max == 2)? $selected :'').'>总价新币 35 万</option>';
		$html .= '	<option value="3" '.(($pri1_max == 3)? $selected :'').'>总价新币 40 万</option>';
		$html .= '	<option value="4" '.(($pri1_max == 4)? $selected :'').'>总价新币 45 万</option>';
		$html .= '	<option value="5" '.(($pri1_max == 5)? $selected :'').'>总价新币 50 万</option>';
		$html .= '	<option value="6" '.(($pri1_max == 6)? $selected :'').'>总价新币 60 万</option>';
		$html .= '	<option value="7" '.(($pri1_max == 7)? $selected :'').'>总价新币 70 万</option>';
		$html .= '	<option value="8" '.(($pri1_max == 8)? $selected :'').'>总价新币 80 以上</option>';
		
		$html .= '</select>';
		
		/* rental price */
		$html .= '<select name="prices_rent" class="searchselect2" id="search_price_rent" '.(($cls == 'rent')?'':' style="display:none;" ') .'>';
		$html .= '	<option value="">新元</option>';
		$html .= '	<option value="1" '.(($pri2 == 1)? $selected :'').'>新元 500 及以下</option>';
		$html .= '	<option value="2" '.(($pri2 == 2)? $selected :'').'>新元 1000</option>';
		$html .= '	<option value="3" '.(($pri2 == 3)? $selected :'').'>新元 1500</option>';
		$html .= '	<option value="4" '.(($pri2 == 4)? $selected :'').'>新元 2000</option>';
		$html .= '	<option value="5" '.(($pri2 == 5)? $selected :'').'>新元 2500</option>';
		$html .= '	<option value="6" '.(($pri2 == 6)? $selected :'').'>新元 3000</option>';
		$html .= '	<option value="7" '.(($pri2 == 7)? $selected :'').'>新元 3500</option>';
		$html .= '	<option value="8" '.(($pri2 == 8)? $selected :'').'>新元 4000</option>';
		$html .= '	<option value="9" '.(($pri2 == 9)? $selected :'').'>新元 5000</option>';
		$html .= '	<option value="10" '.(($pri2 == 10)? $selected :'').'>新元 6000</option>';
		$html .= '	<option value="11" '.(($pri2 == 11)? $selected :'').'>新元 7000</option>';
		$html .= '	<option value="12" '.(($pri2 == 12)? $selected :'').'>新元 8000</option>';
		$html .= '	<option value="13" '.(($pri2 == 13)? $selected :'').'>新元 9000</option>';
		$html .= '	<option value="14" '.(($pri2 == 14)? $selected :'').'>新元 10000</option>';
		$html .= '	<option value="15" '.(($pri2 == 15)? $selected :'').'>新元 12000</option>';
		$html .= '	<option value="16" '.(($pri2 == 16)? $selected :'').'>新元 15000</option>';
		$html .= '	<option value="17" '.(($pri2 == 17)? $selected :'').'>新元 20000</option>';
		$html .= '	<option value="18" '.(($pri2 == 18)? $selected :'').'>新元 30000</option>';
		$html .= '	<option value="19" '.(($pri2 == 19)? $selected :'').'>新元 40000</option>';
		$html .= '	<option value="20" '.(($pri2 == 20)? $selected :'').'>新元 50000 及以上</option>';
		$html .= '</select>';
		
		$html .= '<select name="prices_rent_max" class="searchselect2" id="search_price_rent_max" '.(($cls == 'rent')?'':' style="display:none;" ') .'>';
		$html .= '	<option value="">新元</option>';
		$html .= '	<option value="1" '.(($pri2_max == 1)? $selected :'').'>新元 500 及以下</option>';
		$html .= '	<option value="2" '.(($pri2_max == 2)? $selected :'').'>新元 1000</option>';
		$html .= '	<option value="3" '.(($pri2_max == 3)? $selected :'').'>新元 1500</option>';
		$html .= '	<option value="4" '.(($pri2_max == 4)? $selected :'').'>新元 2000</option>';
		$html .= '	<option value="5" '.(($pri2_max == 5)? $selected :'').'>新元 2500</option>';
		$html .= '	<option value="6" '.(($pri2_max == 6)? $selected :'').'>新元 3000</option>';
		$html .= '	<option value="7" '.(($pri2_max == 7)? $selected :'').'>新元 3500</option>';
		$html .= '	<option value="8" '.(($pri2_max == 8)? $selected :'').'>新元 4000</option>';
		$html .= '	<option value="9" '.(($pri2_max == 9)? $selected :'').'>新元 5000</option>';
		$html .= '	<option value="10" '.(($pri2_max == 10)? $selected :'').'>新元 6000</option>';
		$html .= '	<option value="11" '.(($pri2_max == 11)? $selected :'').'>新元 7000</option>';
		$html .= '	<option value="12" '.(($pri2_max == 12)? $selected :'').'>新元 8000</option>';
		$html .= '	<option value="13" '.(($pri2_max == 13)? $selected :'').'>新元 9000</option>';
		$html .= '	<option value="14" '.(($pri2_max == 14)? $selected :'').'>新元 10000</option>';
		$html .= '	<option value="15" '.(($pri2_max == 15)? $selected :'').'>新元 12000</option>';
		$html .= '	<option value="16" '.(($pri2_max == 16)? $selected :'').'>新元 15000</option>';
		$html .= '	<option value="17" '.(($pri2_max == 17)? $selected :'').'>新元 20000</option>';
		$html .= '	<option value="18" '.(($pri2_max == 18)? $selected :'').'>新元 30000</option>';
		$html .= '	<option value="19" '.(($pri2_max == 19)? $selected :'').'>新元 40000</option>';
		$html .= '	<option value="20" '.(($pri2_max == 20)? $selected :'').'>新元 50000 及以上</option>';
		$html .= '</select>';
		
		return $html;
	}
	
	function getBedRoomListHTML() {
		
		$pr = JRequest::getVar('prop_type', 0);
		
		$bed		=	JRequest::getCmd('bedroom','');
		//$bed1		=	JRequest::getCmd('bedroom_h','');
		
		$selected	=	' selected="selected" ';	
		$html = '';
		
		/* Condo */
		$html .= '<select name="bedroom" class="searchselect2" id="search_bedroom_condo" '.(($pr == '1' || $pr == '0' || $pr == '3')?'':' style="display:none;" ') .'>';
		$html .= '	<option value="">户型不限</option>';
		
		$html .= '	<option value="1" '.(($bed == 1)? $selected :'').'>1房</option>';
		$html .= '	<option value="2" '.(($bed == 2)? $selected :'').'>2房</option>';
		$html .= '	<option value="3" '.(($bed == 3)? $selected :'').'>3房</option>';
		$html .= '	<option value="4" '.(($bed == 4)? $selected :'').'>4房</option>';
		$html .= '	<option value="5" '.(($bed == 5)? $selected :'').'>5房以上</option>';

		$html .= '</select>';
		
		/* HDB */
		
		/*$html .= '<select name="bedroom_h" class="searchselect2" id="search_bedroom_hdb" '.(($pr == '2')?'':' style="display:none;" ') .'>';
		$html .= '	<option value="">户型不限</option>';
		
		$html .= '	<option value="1" '.(($bed1 == 1)? $selected :'').'>1房</option>';
		$html .= '	<option value="2" '.(($bed1 == 2)? $selected :'').'>2房</option>';
		$html .= '	<option value="3" '.(($bed1 == 3)? $selected :'').'>3房</option>';
		$html .= '	<option value="4" '.(($bed1 == 4)? $selected :'').'>4房</option>';
		$html .= '	<option value="5" '.(($bed1 == 5)? $selected :'').'>5房</option>';
		$html .= '	<option value="6" '.(($bed1 == 6)? $selected :'').'>公寓式</option>';

		$html .= '</select>';
		*/
		return $html;
	}
	
	function getDistrictListHTML() {
		$db		=& 	JFactory::getDBO();
		
		$pr = JRequest::getVar('prop_type', 0);
		
		$query 	= 'SELECT * FROM #__ihouse_district ';
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		$html = '';
		
		/* Condo */
		
		$html .= '<select name="district_sector[]" class="searchselect_if" multiple="multiple" size="5" id="district_condo" '.(($pr == '1' || $pr == '0' || $pr == '3')?'':' style="display:none;" ') .'>';
		$html .= '<option value="">All</option>';
		foreach($rows as $r) :
			$t = JRequest::getVar('district_sector', array());
		
			$selected = '';
			if(in_array($r->code,$t)) 
				$selected = 'selected="selected"';
				
			$html .= '	<option value="'.$r->code.'" '.$selected.' >'.$r->code.' '.$r->district_ch.' '.$r->district_en.'</option>';
		endforeach;
		
		$html .= '</select>';
		
		/* HDB */
		
		$query 	= 'SELECT * FROM #__ihouse_hdb_town ';
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		$html .= '<select name="hdb_town[]" class="searchselect_if" multiple="multiple" size="5" id="district_hdb" '.(($pr == '2')?'':' style="display:none;" ') .'>';
		$html .= '<option value="">All</option>';
		
		foreach($rows as $r) :
			$t = JRequest::getVar('hdb_town', array());
		
			$selected = '';
			if(in_array( $r->hdb_town_ch,$t)) 
				$selected = 'selected="selected"';
				
			$html .= '	<option value="'.$r->hdb_town_ch.'" '.$selected.' >'.$r->hdb_town_ch.' '.$r->hdb_town_en.'</option>';
		endforeach;
		
		$html .= '</select>';
		
		return $html;
	}
	
	function getNearestMRTListHTML() {
		$db		=& 	JFactory::getDBO();

		$query 	= 'SELECT DISTINCT(name_ch), name_en FROM #__ihouse_station '
					. " ORDER BY name_en ASC ";
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		$html = '';
		
		$html .= '<select name="nearest_station" style="width:100px;" class="searchselect">';
		$html .= '	<option value="">选择地铁</option>';
		foreach($rows as $r) :
			$t = JRequest::getVar('nearest_station', '');
		
			$selected = '';
			if($t == $r->name_ch) 
				$selected = 'selected="selected"';
			
			if(($r->name_ch) != '')
				$html .= '	<option value="'.$r->name_ch.'" '.$selected.' >'.$r->name_ch.' '.$r->name_en.'</option>';
				
		endforeach;
		
		$html .= '</select>';
		
		return $html;
	}
	
	function getNearestSchoolListHTML() {
		$db		=& 	JFactory::getDBO();

		$where 	= ' WHERE ( type != "primary_top30" OR type != "secondary_top30") '; 
		$query 	= 'SELECT DISTINCT(name_ch), name_en FROM #__ihouse_school '
					. $where
					. " ORDER BY name_en ASC ";
					
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		$html = '';
		
		$html .= '<select name="nearest_school" style="width:100px;" class="searchselect">';
		$html .= '	<option value="">选择学区</option>';
		foreach($rows as $r) :
			$t = JRequest::getVar('nearest_school', '');
		
			$selected = '';
			if($t == $r->name_ch) 
				$selected = 'selected="selected"';
		
			$html .= '	<option value="'.$r->name_ch.'" '.$selected.' >'.$r->name_ch.' '.$r->name_en.'</option>';
		endforeach;
		
		$html .= '</select>';
		
		return $html;
		
	}
	
	function getPropertyType($type = 'checkbox') {
		$db		=& 	JFactory::getDBO();

		$exist = array('Condo', 'HDB');
		
		$where = 
		
		$query 	= 'SELECT * FROM #__ihouse_property_type'
					. ' WHERE LOWER(name_en) = "hdb" OR LOWER(name_en) = "condo" ';
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		$html = '';
		
		
		
		foreach($rows as $r):
			$t = JRequest::getVar(strtolower($r->name_en), '');
		
			$checked = '';
			if($t == $r->name_ch) 
				$checked = 'checked="checked"';
		
			$html .= '<input type="checkbox" value="'.$r->name_ch.'" name="'.strtolower($r->name_en).'" '.$checked.' />'.$r->name_en;
		endforeach;
		
		return $html;
	}
	
}