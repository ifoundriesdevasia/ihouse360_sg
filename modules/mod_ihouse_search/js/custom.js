function toggle(id) {
	
	var t = $(id).getStyle('display');

	if(t == '') {
		$(id).setStyle('display', 'none');
	} else {
		$(id).setStyle('display','');
	}
	
}

function dummy1(mode){
	
	var proptype = $('input_prop_cls').getProperty('value');
	
	switch(mode) {
		case 'condo' 	:
		case 'other'	:	
			if(proptype != 'rent') { 
				$('search_price_condo').setStyle('display','');
				$('search_price_condo_max').setStyle('display','');
				$('search_price_hdb').setStyle('display','none');
				$('search_price_hdb_max').setStyle('display','none');
			} else {
				$('search_price_rent').setStyle('display','');
				$('search_price_rent_max').setStyle('display','');
			}
			
			//$('search_bedroom_hdb').setStyle('display','none');
			$('search_bedroom_condo').setStyle('display','');
			
			$('district_hdb').setStyle('display','none');
			$('district_condo').setStyle('display','');
			
			$('flattype_hdb').setStyle('display','none');
			break;
		case 'hdb' 		:
			if(proptype != 'rent') { 
				$('search_price_hdb').setStyle('display','');
				$('search_price_hdb_max').setStyle('display','');
				$('search_price_condo').setStyle('display','none');
				$('search_price_condo_max').setStyle('display','none');
			} else {
				$('search_price_rent').setStyle('display','');
				$('search_price_rent_max').setStyle('display','');
			}
			//$('search_bedroom_hdb').setStyle('display','');
			$('search_bedroom_condo').setStyle('display','none');
			
			$('district_hdb').setStyle('display','');
			$('district_condo').setStyle('display','none');
			
			$('flattype_hdb').setStyle('display','');
			break;
	}
}

function propertyClassSetup(tabs) {

	$$('.search-menu-text-ads').setStyle('background-color','');
	$$('.search-menu-text-ads').setStyle('color','');
	$('input_prop_cls').setProperty('value', tabs);
	
	$('search_' + tabs).setStyle('background-color','#FB9014');
	$('search_' + tabs).setStyle('color','#fff');
	
	var hdb = $('prop_type2').getProperty('checked');
	var condo = $('prop_type1').getProperty('checked');
	var alls = $('prop_type0').getProperty('checked');
	
	if(tabs == 'rent') {
		$('search_price_rent').setStyle('display','');
		$('search_price_rent_max').setStyle('display','');
		$('search_price_condo').setStyle('display','none');
		$('search_price_condo_max').setStyle('display','none');
		$('search_price_hdb').setStyle('display','none');
		$('search_price_hdb_max').setStyle('display','none');
	} else {
		$('search_price_rent').setStyle('display','none');
		$('search_price_rent_max').setStyle('display','none');
		if(condo || alls ) {
			$('search_price_condo').setStyle('display','');
			$('search_price_condo_max').setStyle('display','');
		}
		
		if(hdb) {
			$('search_price_hdb').setStyle('display','');
			$('search_price_hdb_max').setStyle('display','');
		}
	}
}

function searchkeywordInput(value, info) {
	if(value == '') {
		$('keyword_text_ads_search').setProperty('value', info);
		$('keyword_text_ads_search').setStyle('color', '#ccc');
	} else if(value == info) {
		$('keyword_text_ads_search').setProperty('value', '');
		$('keyword_text_ads_search').setStyle('color', '#000');
	} 
}

function ihseSearch(info) {
	var value = $('keyword_text_ads_search').getProperty('value');
	if(info == value) {
		$('keyword_text_ads_search').setProperty('value', '');
	}
	document.ads_adv_search_form.submit();
}