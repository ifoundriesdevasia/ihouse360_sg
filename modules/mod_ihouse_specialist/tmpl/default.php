<?php // no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<?php JHTML::_('script', 'ihouse.js', 'components/com_ihouse/assets/'); ?>  
<?php //JHTML::_('script', 'contacts.js', 'components/com_ihouse/assets/'); ?>   

<div class="component-pos" style="margin-top:20px;">
	<div id="sub-small-component-border" style="background-color:#fff;">
    	<div id="sub-small-component">
        	<div id="component-headerpos">
            	<div id="component-logo">
                	<div id="component-title"><span class="chinese_text1">新加坡公寓置业顾问</span>&nbsp;<i>Singapore Condo Consultant</i></div>
                </div>
            </div>
        </div>
        
        <div id="sub-small-component-content" >
        	<div style="position:relative;z-index:1000;">				
        	<img id="sliders_pt_lft" src="<?php echo JRoute::_('templates/main/images/arrow_left_1.png') ?>" onclick="javascript:movePage1('prev');" style="display:none;" />
        	<img id="sliders_pt_rgt" src="<?php echo JRoute::_('templates/main/images/arrow_right_1.png') ?>" onclick="javascript:movePage1('next');" />
        	</div>                
                       	<div class="specialist_info">
                        	<div class="specialist_info_content" id="specialistDetail"> 
                           	<?php for($i= 0; $i < 6; $i++): ?>
                            
                            <div class="list_specialist">
                            <?php 	if(!empty($rows[$i])) : ?>
                            	<table width="100%" cellpadding=0 cellspacing=0>
                                    <tr>
                                    	<td width="50%">
                            			<div style="text-align:center;height:92px;">
                                        <?php
		  								if(empty($rows[$i]['user_image'])) {
											$user_image = JRoute::_('templates/main/images/no_avatar.jpg');
										} else {
											$user_image = 'image.php?size=92&type=1&path='.$rows[$i]['user_image'];
										}
		  								?>
                        					<a style="text-decoration:none;color:#fff;" href="<?php echo JRoute::_('index.php?option=com_user&view=agent_profile&id='.$rows[$i]['user_id'].'&Itemid=136'); ?>">
            									<img style="border:none;" src="<?php echo $user_image; ?>" />
                							</a>
                                        
                                        
                                		</div>
                                        <div style="clear:both"></div>
                                        <div style="margin-top:4px;text-align:center;">
						<!--<input type="button" onclick="javascript:emailFormPopup('<?php echo $rows[$i]['email'] ?>','');" id="agent_sub" name="agent_sub" class="contactbutton" value="Contact">-->
					</div>   
                                    	</td>
                                	    <td width="50%">         
                                		
                                      
                                        <?php 
			
				if(empty($rows[$i]['company_logo'])) {
					$comp_logo = JRoute::_('templates/main/images/thumb-nologo.jpg');
				}else{
					$comp_logo = 'image.php?size=100&type=1&path='.$rows[$i]['company_logo'];
				}
			
				?>
            
            	<?php if($rows[$i]['user_category'] == 'Agent'): ?>
            		<img src="<?php echo $comp_logo ?>" />
           	 	<?php endif; ?> 
                                       
                                    	</td>
                                    </tr>
                                                 
                                	<tr>
                                    	<td colspan="2" style="padding-top:10px;">
                                        <div class="thisspecialistprofile">
    			<div class="l">姓名</div>
        		<div class="m">:</div>
        		<div class="r">
				<?php 
				if(!empty($rows[$i]['name']) && !empty($rows[$i]['chinese_name'])) {
							$name = '';
							if (!empty($rows[$i]['name'])) {
								$name .= $rows[$i]['name'];
							}
							if (!empty($rows[$i]['chinese_name'])) {
								$name .= '</br>'.$rows[$i]['chinese_name'];
							}
							
							echo $name;
				} else { 
					  		echo 'N/A';
				}
				?>
            	</div>
            <div style="clear:both"></div>
            </div>	
            <div class="thisspecialistprofile">
    			<div class="l">公司名称</div>
        		<div class="m">:</div>
        		<div class="r"><?php echo (!empty($rows[$i]['company_name']))?wordwrap($rows[$i]['company_name'], 15, "\n", true):'N/A' ?></div>
            <div style="clear:both"></div>
        	</div>		
            <div class="thisspecialistprofile">
    			<div class="l">电话</div>
        		<div class="m">:</div>
        		<div class="r"><?php echo ($rows[$i]['mobile_contact'])?$rows[$i]['mobile_contact']:'N/A' ?></div>
            <div style="clear:both"></div>
        	</div>
            <div class="thisspecialistprofile">
    			<div class="l">个人网址</div>
        		<div class="m">:</div>
        		<div class="r">
	<?php if(!empty($rows[$i]['website'])) : ?>
	<a href='http://<?php echo (!empty($rows[$i]['website']))?wordwrap($rows[$i]['website'], 15, "\n", true):'N/A' ?>' target="_blank">点击进入</a>
	<?php else : ?>
	未填写
	<?php endif; ?>
				</div>
            <div style="clear:both"></div>
        	</div>		
  			<div class="thisspecialistprofile">
    			<div class="l">经纪证号</div>
        		<div class="m">:</div>
        		<div class="r">
                <?php if(!empty($rows[$i]['cea_reg_no'])) : ?>
                	<?php if(preg_match('/^L/', $rows[$i]['cea_reg_no'] )): ?>
                		<a href="http://www.cea.gov.sg/cea/app/newimplpublicregister/view.jspa?licNo=<?php echo $rows[$i]['cea_reg_no'] ?>" target="_blank">
                    <?php elseif(preg_match('/^R/', $rows[$i]['cea_reg_no'] )) : ?>    
                    	 <a href='http://www.cea.gov.sg/cea/app/newimplpublicregister/view.jspa?licNo=""&regNo=<?php echo $rows[$i]['cea_reg_no'] ?>' target="_blank">
                    <?php endif; ?>   
						<?php echo $rows[$i]['cea_reg_no'] ?>
                    </a>
                <?php else : ?>
                	N/A    
                <?php endif; ?>
                </div>
            <div style="clear:both"></div>
        	</div>  	
                                        </td>
                                    </tr>
                                </table>  
                            <?php else : ?>
                            	<table width="100%" cellpadding=0 cellspacing=0>
                                    <tr>
                                    	<td width="50%">
                            			<div style="border:1px solid #ccc;float:left;">
                                		<img src="<?php echo JRoute::_('templates/main/images/no_avatar.jpg'); ?>" />
                                		</div>
                                    	</td>
                                	    <td width="50%">         
                                		<div id="">
										目前空缺
                                		</div>
                                    	</td>
                                    </tr>
                                    </table>              
                            <?php endif; ?>     
                            </div>
                            
                            <?php endfor; ?>
                            </div>
                         </div>
		</div><!-- sub-small-component-content -->
	</div>
</div>