<?php

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// Include the syndicate functions only once
require_once( dirname(__FILE__).DS.'helper.php' );

$layout = $params->get('layout', 'default');
$path = JModuleHelper::getLayoutPath('mod_ihouse_financial_calc', $layout);

$db =& JFactory::getDBO();
	
$type				= modiHouseFinancialCalcHelper::getType();
$amt				= modiHouseFinancialCalcHelper::getAmountOfLoad();
$numof				= modiHouseFinancialCalcHelper::getNumberOf();
$interest			= modiHouseFinancialCalcHelper::getInterestAnnually();
$monthpayment		= modiHouseFinancialCalcHelper::getMonthlyPayment();


if (file_exists($path)) {
	require($path);
}