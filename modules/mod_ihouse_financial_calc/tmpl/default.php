<?php // no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>
<script type="text/javascript">
function formatNumberFincCalc(value, decimal, id) {
     //decimal  - the number of decimals after the digit from 0 to 3
     //-- Returns the passed number as a string in the xxx,xxx.xx format.
	 var StartNumber = value;
	 var StartNumber = StartNumber.replace(/\,/g,'');
	 var ReplacedNumber = StartNumber.replace(/\t\r\n/g,'');
       anynum=ReplacedNumber;
	   
       divider =10;
       switch(decimal){
            case 0:
                divider =1;
                break;
            case 1:
                divider =10;
                break;
            case 2:
                divider =100;
                break;
            default:       //for 3 decimal places
                divider =1000;
        } 

       workNum=Math.abs((Math.round(anynum*divider)/divider)); 

       workStr=""+workNum 

       if (workStr.indexOf(".")==-1){workStr+="."} 

       dStr=workStr.substr(0,workStr.indexOf("."));dNum=dStr-0
       pStr=workStr.substr(workStr.indexOf(".")) 

       while (pStr.length-1< decimal){pStr+="0"} 

       if(pStr =='.') pStr =''; 

       //--- Adds a comma in the thousands place.    
       if (dNum>=1000) {
          dLen=dStr.length
          dStr=parseInt(""+(dNum/1000))+","+dStr.substring(dLen-3,dLen)
       } 

       //-- Adds a comma in the millions place.
       if (dNum>=1000000) {
          dLen=dStr.length
          dStr=parseInt(""+(dNum/1000000))+","+dStr.substring(dLen-7,dLen)
       }
	   
	   //-- Adds a comma in the billions place.
       if (dNum>=1000000000) {
          dLen=dStr.length
          dStr=parseInt(""+(dNum/1000000000))+","+dStr.substring(dLen-11,dLen)
       }
	   
	   //-- Adds a comma in the trillions place.
       if (dNum>=1000000000000) {
          dLen=dStr.length
          dStr=parseInt(""+(dNum/1000000000000))+","+dStr.substring(dLen-15,dLen)
       }
	   
       retval = dStr + pStr
	   
	  
       //-- Put numbers in parentheses if negative.
    if (anynum<0) {retval="("+retval+")";} 

	if(retval == 0) {
		   retval = '';
	}
	  
	  
	$(id).setProperty('value', retval);
}

function isNumberKeyFincCalc(evt)
{
	var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
       return false;
	}
		 
    return true;
}

function loancalculation() {
	
	var type 		= $('type_compounded');
	var loan_amt 	= $('loan_amt');
	var numberof	= $('numberof');
	var interest 	= $('interest');
	var monthpay 	= $('monthly_payment');
	
	var compounded 	=	'';
	var	P			=	'';
	var n			=	'';
	var i			=	'';
	
	var value		=	'';
	
	if(type) {
		compounded = type.getProperty('value');
		if(compounded == 'year') {
			$('timeline').setHTML('年');
		} else {
			$('timeline').setHTML('月');
		}
	}
	
	if(loan_amt) {
		var number = loan_amt.getProperty('value');
		var StartNumber = number.replace(/\,/g,'');
		P	=	parseInt(StartNumber);
	}
	
	if(numberof) {
		if(compounded == 'year') {
			n	=	parseInt(numberof.getProperty('value'))
		} else {
			n	=	parseInt(numberof.getProperty('value')) * 12;
		}
	}
	
	if(interest) {
		i	=	parseFloat(interest.getProperty('value'));
		if(compounded == 'year') {
			i = i * 1 / 100;
		}
		else {
			i	=	i / 100 ;
			i 	=	i / 12;
		}
	}
	
	if(P && n && i) {
		var tmp = 1;
		
		for (g=1 ; g <= n ; g++)
		{
			tmp = tmp * (1 + i);
		}
		
		value =	(P * i * tmp) / (tmp - 1);
		//formatNumberFincCalc(value,2,'monthly_payment');
		monthpay.setProperty('value', roundNumber(value,2));
	} else {
		monthpay.setProperty('value', '');
	}
	
}

function roundNumber(num, dec) {
	var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
	return result;
}
</script>


<div id="module-padding" style="width:100%;">
            <div id="module-pos4">
            	<div id="small-module-border" style="width:100%;">
                	<div id="small-module" style="width:100%;">
                    	<div id="module-headerpos">
                            <div id="module-title"><span class="chinese_text1">房贷计算器</span>&nbsp;<i>Financial Calculator</i></div>
                        </div>
                    </div>
                   
                    <div id="small-module-content" style="width:100%;">
                    	<div id="module-content-padding" style="width:90%;padding-top:0;padding-left:10px;">

                            <div style="clear:both"></div>
                            <div class="content-border" style="width:100%;float:left;border-bottom:1px dotted #ccc;">
                                <div id="financial_calculator_box">
                               		<div class="label_finc_calc">类别 : </div>
                                    <div class="value_finc_calc"><?php echo $type ?></div>
                                    <div style="clear:both"></div> 
                                    <div class="label_finc_calc">贷款总额（S$）:</div>
                                     <div class="value_finc_calc"><?php echo $amt ?></div>
                                     <div style="clear:both"></div> 
                                    <div class="label_finc_calc">贷款年限（年）:</div>
                                     <div class="value_finc_calc"><?php echo $numof ?></div>
                                     <div style="clear:both"></div> 
                                    <div class="label_finc_calc">贷款利息（%）:</div>
                                     <div class="value_finc_calc"><?php echo $interest ?>%</div>
                                     <div style="clear:both"></div> 
                                    <div class="label_finc_calc"><span id="timeline">月</span>偿付额（S$）:</div>
                                     <div class="value_finc_calc"><?php echo $monthpayment ?></div>
                                    <div style="clear:both"></div>
									<span style="float:right;margin-top:1px;"><a href="http://singapore.ihouse360.com/property_loan.html" target="_blank">完整版</a></span>
                                </div>
                            </div>
                            <div style="clear:both"></div>
                        </div>
                    </div>
                </div>
            </div>
            </div>      