﻿<?php
/**
 * Helper class for Slider Gallery Module
 * 
 * @package    Joomla
 * @subpackage Modules
 * @link http://www.yashvyas.in
 * @license        GNU/GPL
 * mod_galleryview is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */
class modiHouseFinancialCalcHelper
{

	/*function getNewCondosForSale() {
		
		$db		=& 	JFactory::getDBO();
				
		return $rows;
	}
	*/
	
	function getType() {
		$html = '';
		
		$html .= '<select id="type_compounded" onchange="javascript:loancalculation();" >';
		$html .= '	<option value="month" selected="selected">Monthly</option>';
		$html .= '	<option value="year">Yearly</option>';
		$html .= '</select>';
		
		return $html;
	}
	
	function getAmountOfLoad() {
		$html = '<input type="text" id="loan_amt" name="loan_amt" value="" onkeyup="javascript:formatNumberFincCalc(this.value,0,\'loan_amt\');loancalculation();" />';
		
		return $html;
	}
	
	function getNumberOf() {
		$html = '<input type="text" id="numberof" name="numberof" value="" onkeyup="javascript:loancalculation();" />';
		
		return $html;
	}
	
	function getInterestAnnually() {
		$html = '<input type="text" id="interest" name="interest" value="" onkeyup="javascript:loancalculation();" />';
		
		return $html;
	}
	
	function getMonthlyPayment() {
		$html = '<input type="text" id="monthly_payment" value="" readonly="readonly" />';
		
		return $html;
	}
	
}