<?php

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// Include the syndicate functions only once
require_once( dirname(__FILE__).DS.'helper.php' );

$layout = $params->get('layout', 'default');
$path = JModuleHelper::getLayoutPath('mod_ihouse_similar_properties', $layout);

$row			= modiHouseSimilarPropertiesHelper::getList();



if (file_exists($path)) {
	require($path);
}