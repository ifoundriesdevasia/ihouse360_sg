﻿<?php // no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<div class="component-pos" style="">
	<div id="sub-small-component-border">
    	<div id="sub-small-component">
        	<div id="component-headerpos">
            	<div id="component-logo">
                	<div id="component-title"><span class="chinese_text1">你可能会感兴趣的类似的房源</span>&nbsp;<i>Similiar properties you might be interested in</i></div>
                </div>
            </div>
        </div>
        <div style="clear:both;"></div>
        <div id="sub-small-component-content" >
                    	
                        <div id="">
                            <div id="">
                            <div>
                            <table class="table-border-ihouse" width="100%" cellspacing=0 cellpadding=0 >
                            <tr align="center" style="background-color:#f0f0f0;">
                            	<th width="40%" align="left" style="padding:10px;"><span style="color:#FF8200">项目名</span></th>
                                <th width="20%"><span style="color:#FF8200">户型</span></th>
                                <th width="10%"><span style="color:#FF8200">邮区</span></th>
                                <th width="15%"><span style="color:#FF8200">面积</span></th>
                                <th width="15%"><span style="color:#FF8200">价格(SGD)</span></th>
                            </tr>
                            <?php if(!empty($row)) : ?>
                            <?php foreach($row as $p) : ?>
                            <tr align="center">
                            	<td id="tabtext-padding1" align="left" style="padding:10px;">
                                	<a href="<?php echo JRoute::_('index.php?option=com_ihouse&view=ihouse&layout=property_detail&id='.$p->id.'&postcode='.$p->postcode); ?>">
									<?php echo $p->name_en ?>
									<?php echo ($p->name_ch)?'<br />('.$p->name_ch.')' : '' ; ?>
                                    </a>
                                </td>
                                <td>
								<?php 
								if(!empty($p->floor_plan)) : 
									$c = explode('|',$p->floor_plan);
									echo implode(',', $c); 
								else :
									echo '-';
								endif;
								?>
                                </td>
                                <td><?php echo ($p->district_id)?$p->district_id:'-'; ?></td>
                                <td><?php echo ($p->room_size)?$p->room_size:'-'; ?></td>
                                <td><?php echo ($p->price)?number_format($p->price):'-'; ?></td>
                            </tr>
                            <?php endforeach; ?>
                            <?php else : ?>
                            <tr align="center">
                            	<td id="tabtext-padding1" align="center" style="padding:10px;" colspan="5">
                                	No Similar Properties
                                </td>
                            </tr>
                            <?php endif; ?>
                            </table> 
                            </div>
                            </div>
                    	</div>
                        
                        
		</div>
	</div>
</div>