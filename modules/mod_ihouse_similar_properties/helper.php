﻿<?php
/**
 * Helper class for Slider Gallery Module
 * 
 * @package    Joomla
 * @subpackage Modules
 * @link http://www.yashvyas.in
 * @license        GNU/GPL
 * mod_galleryview is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */
class modiHouseSimilarPropertiesHelper
{
	
	function getList() {
		$db		=& 	JFactory::getDBO();
		
		$property_id = JRequest::getCmd('id');
		
		$query 	= 	'SELECT t.floor_plan, p.price FROM #__ihouse_property AS t '
					.	" LEFT JOIN #__ihouse_property_price AS p ON p.postcode = t.postcode "
					.	" WHERE t.id = '$property_id' "
					.	' LIMIT 1 ';
					
			$db->setQuery( $query ) ;
			$row = $db->loadObject();
			
		$floorplan 	= 	explode('|',$row->floor_plan);
		$price		=	$row->price;
		
		$where = array();
		foreach($floorplan AS $fp) :
		
			$where_or[] = " t.floor_plan LIKE '%$fp%' ";
		
		endforeach;
		$where[] 	= ( count( $where_or ) ? ' (' . implode( ' OR ', $where_or ).')' : '' );
		
		if(!empty($price)) { 
			$low		=	$price - ($price * 0.1);
			$high		=	$price + ($price * 0.1);
			
			$where[] 	= 	" p.price >= '$low' ";
			$where[]	=	" p.price <= '$high' ";
			
		}
		
		$where 		= ( count( $where ) ? ' WHERE (' . implode( ' AND ', $where ).')' : '' );
		
		
		
		$query 	= 	'SELECT t.*, p.price FROM #__ihouse_property AS t '
					.	" LEFT JOIN #__ihouse_property_price AS p ON p.postcode = t.postcode "
					.	$where
					.	" ORDER BY RAND() "
					. 	" LIMIT 6 ";
					
			$db->setQuery( $query );
			$row = $db->loadObjectList();
			
		return $row;
	}
}