<?php // no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<?php

$width = $params->get('width','600');
$height = $params->get('height','150');
$thumb_width = $params->get('thumb_width','50');
$path = $params->get('path','modules/mod_galleryview/gallery');
$tras_speed = $params->get('trans_speed','400');
$bgcolor = $params->get('bgcolor','#000');
$border_color = $params->get('border_color','#000');
$theme = $params->get('theme','light');
$auto_slide = $params->get('auto_slide','1');
$thumb_gen = $params->get('thumb_gen','0');
$transition_interval = $params->get('transition_interval','6000');
$loadjq = $params->get('loadjq','1');
$load_scripts = $params->get('load_scripts','1');
$uniuqe_id = $params->get('uniuqe_id','');
$show_film = $params->get('show_film','');

/* iFoundries Modified */
$postcode = JRequest::getVar('postcode');

/* EXCEPTION BY IFOUNDRIES */

$ihousephotos 	= $_POST['ihousephotos'];
$session_id		= $_POST['session_id'];

$a_img = array();
$b_img = array();
/* END */
$imgdir = JRoute::_($path.'/'.$postcode.'/images');  //'modules/mod_galleryview/gallery';  the directory, where your images are stored
$imgdir1 = JRoute::_('images/ihouse/ads_images/'.$session_id);
				
$allowed_types 	= array('png','jpg','gif'); // list of filetypes you want to show
$allowed_types1 = array('jpeg');

?>


<?php
				/* iFoundries Modified */
				
				/* End */
				
				

				$image_exist1 = false;
				$image_exist2 = false;
				
				if(!empty($ihousephotos)) :
					foreach($ihousephotos as $fileimg)
					{
						
				 		if(in_array(strtolower(substr($fileimg,-3)),$allowed_types))
				 		{
				  			$a_img[] = $fileimg;
				  			sort($a_img);
				  			reset ($a_img);
				 		}
						
						if(in_array(strtolower(substr($fileimg,-4)),$allowed_types1))
				 		{
				  			$a_img[] = $fileimg;
				  			sort($a_img);
				  			reset ($a_img);
				 		}
					}
					
					$totimg = count($a_img); // total image number
					
					if($totimg)
						$image_exist1 = true;
					
				endif;
				
				$query = " SELECT * FROM #__ihouse_ads_image WHERE sess_id = '$session_id'";
					$db->setQuery( $query );
					$imgrows = $db->loadObjectList();
				
				if(is_dir($imgdir1)) {
					
					if(!empty($imgrows)) {
						foreach($imgrows as $r)
						{
							$fileimg = $r->name;
							
							$imgfile = $imgdir1.'/'.$fileimg;
							if (file_exists($imgfile)) {
							
				 				if(in_array(strtolower(substr($fileimg,-3)),$allowed_types))
				 				{
				  					$b_img[] = $fileimg;
				  					sort($b_img);
				  					reset ($b_img);
				 				}
								
								if(in_array(strtolower(substr($fileimg,-4)),$allowed_types1))
				 				{
				  					$b_img[] = $fileimg;
				  					sort($b_img);
				  					reset ($b_img);
				 				}
							}
						}
					
						$totimg1 = count($b_img); // total image number
					
						if($totimg1)
							$image_exist2 = true;
					}
				}
				
				
				
?>

	<?php if($image_exist1 || $image_exist2 ) : ?>
	<?php //if($loadjq) {?>
		<script type="text/javascript" src="<?php //echo JURI::root(); ?>modules/mod_galleryview/js/jquery-1.3.2.min.js"></script>
    <?php //}?>
    <script type="text/javascript">
  		
	  // Code that uses other library's $ can follow here.
	</script>
	
    
    <?php if($load_scripts) {?> 
	<script type="text/javascript" src="<?php echo JURI::root(); ?>modules/mod_galleryview/js/jquery.galleryview-1.1-pack.js"></script>
    <script type="text/javascript" src="<?php echo JURI::root(); ?>modules/mod_galleryview/js/jquery.timers-1.1.2.js"></script>
    <script type="text/javascript" src="<?php echo JURI::root(); ?>modules/mod_galleryview/js/jquery.easing.1.3.js"></script>
    <?php } ?>

	<script type="text/javascript">
		jQuery.noConflict();
		
		jQuery(document).ready(function($) {
            jQuery('#photos<?php echo "-".$postcode; ?>').galleryView({
            panel_width:<?php echo $width; ?>,
            panel_height:<?php echo $height; ?>,
            frame_width:<?php echo $thumb_width; ?>,
            frame_height:<?php echo $thumb_width; ?>,
            transition_speed:<?php echo $tras_speed; ?>,
			background_color: '<?php echo $bgcolor; ?>',
			overlay_position: 'bottom',
			border: '1px solid <?php echo $border_color; ?>',
			nav_theme: '<?php echo $theme; ?>',
			easing: 'easeInQuad',
			transition_interval:<?php echo $transition_interval;  ?>
        });
			
    });
    </script>
    <?php endif; ?>
<!-- Andreas Junizar : Must hack this one...need to conclude what will be for this floorplan -->

<div id="photos<?php echo "-".$postcode; ?>" class="galleryview" style="height:490px;">
	<?php if(!$image_exist1 && !$image_exist2) : ?>
    	No Images Available
    <?php endif; ?>
    
    
    <?php for($x=0; $x < $totimg; $x++)	{ ?>
    <div class="panel">    	
		<img src="<?php echo $imgdir.'/'.$a_img[$x]; ?>" />        
	</div>
    <?php } ?>
    
     <?php for($x=0; $x < $totimg1; $x++)	{ ?>
    <div class="panel">    	
		<img src="<?php echo $imgdir1.'/'.$b_img[$x]; ?>" />        
	</div>
    <?php } ?>
    
    
    
    <?php if ($show_film) { ?>
		<?php if ($thumb_gen) { ?>
        <ul class="filmstrip">
        
            <?php for($x=0; $x < $totimg; $x++)	{ ?>
                <?php $temp = explode(".",$a_img[$x]); ?>
                <li><img src="<?php echo $imgdir.'/thumb/'.$temp[0].'_thumb.'.$temp[1]; ?>" alt="<?php echo $a_img[$x]; ?>" title="<?php echo $a_img[$x]; ?>" height="<?php echo $thumb_width; ?>" width="<?php echo $thumb_width; ?>" /></li>
            <?php } ?>     
            
            <?php for($x=0; $x < $totimg1; $x++)	{ ?>
                <?php $temp = explode(".",$b_img[$x]); ?>
                <li><img src="<?php echo $imgdir1.'/thumb/'.$temp[0].'_thumb.'.$temp[1]; ?>" alt="<?php echo $b_img[$x]; ?>" title="<?php echo $b_img[$x]; ?>" height="<?php echo $thumb_width; ?>" width="<?php echo $thumb_width; ?>" /></li>
            <?php } ?>     
            
        </ul>
        <?php } else { ?>
        <ul class="filmstrip">
        
            <?php for($x=0; $x < $totimg; $x++)	{ ?>
                <?php $temp = explode(".",$a_img[$x]); ?>
                <li><img src="<?php echo $imgdir.'/s/'.$a_img[$x]; ?>" alt="<?php echo $a_img[$x]; ?>" title="<?php echo $a_img[$x]; ?>" height="<?php echo $thumb_width; ?>" width="<?php echo $thumb_width; ?>" /></li>
            <?php } ?>     
            
            <?php for($x=0; $x < $totimg1; $x++)	{ ?>
                <?php $temp = explode(".",$b_img[$x]); ?>
                <li><img src="<?php echo $imgdir1.'/small-'.$temp[0].'.'.$temp[1]; ?>" alt="<?php echo $b_img[$x]; ?>" title="<?php echo $b_img[$x]; ?>" height="<?php echo $thumb_width; ?>" width="<?php echo $thumb_width; ?>" /></li>
            <?php } ?> 
            
        </ul>
        <?php } ?>
    <?php } ?>
    
    
    
</div>