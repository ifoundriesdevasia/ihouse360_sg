﻿<?php // no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>
<?php JHTML::_('script', 'ads.js', 'modules/mod_ihouse_side_features/js/'); ?>
<div id="module-padding">
	<div id="module-pos4">
    	<div id="sub-small-module-border">
        	<div id="sub-small-module">
            	<div id="module-headerpos">
                	<div id="module-logo">
                    	<div id="module-title"><span class="chinese_text1">精品楼盘</span> Prime Properties</div>
                    </div>
                </div>
            </div>
			<div id="sub-small-module-content">
            <!--Start of tabs-->
            <div id="module-content-padding6">
            	
                <div id="menu_list_property_detail">
				<ul id="" class="tab_nav5">   
                  
				<li id="tab_li5_sell" class="tabactive5"><a href="javascript:primePropertiesTab('sell');"><span class="chinese_text_menu">出售</span></a></li>
				<li id="tab_li5_rent"><a href="javascript:primePropertiesTab('rent');"><span class="chinese_text_menu">出租</span></a></li>
				
				</ul>   
				</div>
                
               	<div id="ajax-prime-properties"></div>
			</div><!-- module-content-padding6 -->
            <div style="border-top:1px dotted #ccc;">&nbsp;</div>
			<!--End of tabs-->
                          
            </div>   
            
                   
		</div>
	</div>
</div>