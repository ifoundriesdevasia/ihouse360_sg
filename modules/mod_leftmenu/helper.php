<?php
/**
* @version		$Id: helper.php 14401 2010-01-26 14:10:00Z louis $
* @package		Joomla
* @copyright	Copyright (C) 2005 - 2010 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

class modLeftmenu
{
	function menuSubCat(){
	$db = &JFactory::getDBO();
	
	$menu	= &JSite::getMenu();
	$active	= $menu->getActive();
	
	$tree = $active->tree;
	//$active = $tree[1];
	//$active = count($tree);
	
		$query = "SELECT name, link FROM #__menu WHERE parent = '".$tree[0]."'";
		$db->setQuery($query);
		$subcat = $db->loadRowList();	

	return $subcat;
	}//end function
	
	function menuSubsubcat(){
	$db = &JFactory::getDBO();
	
	$menu	= &JSite::getMenu();
	$active	= $menu->getActive();
	
	$tree = $active->tree;
	
		$query = "SELECT name, link FROM #__menu WHERE parent = '".$tree[1]."'";
		$db->setQuery($query);
		$subsubcat = $db->loadRowList();	

	return $subsubcat;
	}//end function

}//end class
