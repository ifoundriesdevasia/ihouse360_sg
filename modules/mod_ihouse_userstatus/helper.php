<?php
/**
 * Helper class for Slider Gallery Module
 * 
 * @package    Joomla
 * @subpackage Modules
 * @link http://www.yashvyas.in
 * @license        GNU/GPL
 * mod_galleryview is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */
class modiHouseUserStatusHelper
{
    function getUserStatusHTML()
    {
		$db		=& 	JFactory::getDBO();

		$user	=	JFactory::getUser();
		$uid	=	$user->id;
		$yourname = $user->name;
		$chinese_name = $user->chinese_name;
		
		$option = JRequest::getVar('option');
		
		$name = '';
		
		if($yourname) {
			$name = $yourname;
		}
		
		if($chinese_name) {
			$name = $chinese_name;
		}
		
		$return = '';
		switch($option) {
			case 'com_kunena' :
				$return = '&return='.base64_encode($_SERVER['REQUEST_URI']);
				break;
		}
		//$query	=	" SELECT * FROM #__menu WHERE link = 'index.php?option=com_user&view=login' ";
		
		$html = '';
		if($uid) {
			$html .= '<span style="color:#062284;font-size:14px;font-weight:bold;">Hi</span> , <a style="color:#062284;font-size:14px;font-weight:bold;text-decoration:none;" href="'.JRoute::_('index.php?option=com_ads&view=account_info&Itemid=127').'">'.$name.'</a>';
		} else {
			$html .= '<span style="color:#062284;font-size:14px;font-weight:bold;">Hi</span> , <a style="color:#062284;font-size:14px;font-weight:bold;text-decoration:none;" href="'.JRoute::_('index.php?option=com_user&view=login'.$return).'">Guest</a>';
		}
		
		return $html;
    }
}