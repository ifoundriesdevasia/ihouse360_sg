                      CBSubs - Version 1.0.3
                      ----------------------

Please read the manual to install first time and operate.
The new CBSubs 1.0.3 manual can be downloaded in CBSubs area in Downloads section of http://www.joomlapolis.com/

To upgrade from an earlier CBSubs 1.0.x you can do an easy upgrade with all settings kept.
Please refer to section 4 of the CBSubs manual for exact steps.

Copyright (c) 2007-2009 and Trademark of Lightning MultiCom SA, Switzerland, and its licensors.

http://www.joomlapolis.com/

