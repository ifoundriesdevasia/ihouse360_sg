function select_user(sel) {
	window.addEvent('domready',function() {	
		var foo = new Date; // Generic JS date object
		var unixtime_ms = foo.getTime();								
		//var url = 'components/com_user/user_selection_table.php?r=' + unixtime_ms;	
		
		if(sel.options.selectedIndex == 0){
			var usertype = '';
		} else if (sel.options.selectedIndex == 1){
			var usertype = 'Individual';
			document.getElementById('reg_header').innerHTML="User Registration";
			document.getElementById('namemsg').innerHTML="Name *";
			document.getElementById('countrymsg').innerHTML="Country of Origin";
			document.getElementById('mobile_contactmsg').innerHTML="Mobile Contact";
			$('reg_border').setStyle('display','block');
			$('agent_coy_info').setStyle('display','none');
			$('register_div').setStyle('display','block');
			$('standard_table').setStyle('display','block');
			$('agent_owner_table').setStyle('display','none');  
			$('login_details_table').setStyle('display','block');
			$('organization_table').setStyle('display','none');
			$('country_table').setStyle('display','block');
			$('mobile_div').setStyle('display','block');
			$('phone1_div').setStyle('display','none');
			$('personal_details_table').setStyle('display','none');
			$('home_tel_table').setStyle('display','none');
			$('personal_table').setStyle('display','none');
			$('phone_table').setStyle('display','none');
			$('misc_table').setStyle('display','block');
			$('home_add_table').setStyle('display','none');
			$('rental_add_table').setStyle('display','none');
			$('receive_table').setStyle('display','none');
			$('button_div').setStyle('display','block');
			var company_name = $('company_name');
			company_name.className = "inputbox";
			var salute = $('salute');
			salute.className = "inputbox required";
			var name = $('name');
			name.className = "inputbox required";
			var nric = $('nric');
			nric.className = "inputbox";
			var country = $('country');
			country.className = "inputbox";
			var mobile_contact = $('mobile_contact');
			mobile_contact.className = "inputbox";
			var home_address = $('home_address');
			home_address.className = "inputbox";
			var home_postalcode = $('home_postalcode');
			home_postalcode.className = "inputbox";
			
			var organization_name = $('organization_name');
			organization_name.className = "inputbox";
			var organization_address = $('organization_address');
			organization_address.className = "inputbox";
			var phone_1 = $('phone_1');
			phone_1.className = "inputbox";

		} else if (sel.options.selectedIndex == 2){
			var usertype = 'Merchant';
			
			document.getElementById('reg_header').innerHTML="Merchant Registration";
			document.getElementById('namemsg').innerHTML="Name *";
			document.getElementById('countrymsg').innerHTML="Country";
			document.getElementById('phone_1msg').innerHTML="Telephone 1 *";
			$('reg_border').setStyle('display','block');
			$('agent_coy_info').setStyle('display','none');
			$('register_div').setStyle('display','block');
			$('standard_table').setStyle('display','block');
			$('agent_owner_table').setStyle('display','none');
			$('login_details_table').setStyle('display','block');
			$('organization_table').setStyle('display','block');
			$('country_table').setStyle('display','block');
			$('mobile_div').setStyle('display','none');
			$('phone1_div').setStyle('display','block');
			$('personal_details_table').setStyle('display','none');
			$('home_tel_table').setStyle('display','none');
			$('personal_table').setStyle('display','none');
			$('phone_table').setStyle('display','block');
			$('misc_table').setStyle('display','none');
			$('home_add_table').setStyle('display','none');
			$('rental_add_table').setStyle('display','none');
			$('receive_table').setStyle('display','none');
			$('button_div').setStyle('display','block');
			var company_name = $('company_name');
			company_name.className = "inputbox";
			var salute = $('salute');
			salute.className = "inputbox required";
			var name = $('name');
			name.className = "inputbox required";
			var nric = $('nric');
			nric.className = "inputbox";
			var country = $('country');
			country.className = "inputbox";
			var mobile_contact = $('mobile_contact');
			mobile_contact.className = "inputbox";
			var home_address = $('home_address');
			home_address.className = "inputbox";
			var home_postalcode = $('home_postalcode');
			home_postalcode.className = "inputbox";
			
			var organization_name = $('organization_name');
			organization_name.className = "inputbox required";
			var organization_address = $('organization_address');
			organization_address.className = "inputbox required";
			var phone_1 = $('phone_1');
			phone_1.className = "inputbox required";

		} else if (sel.options.selectedIndex == 3){
			var usertype = 'Owner';
			document.getElementById('reg_header').innerHTML="Owner Registration";
			document.getElementById('namemsg').innerHTML="Given Name *";
			document.getElementById('nricmsg').innerHTML="NRIC/Passport No./FIN *";
			document.getElementById('countrymsg').innerHTML="Country *";
			document.getElementById('mobile_contactmsg').innerHTML="Mobile Number *";
			document.getElementById('home_addressmsg').innerHTML="Home Address *";
			document.getElementById('home_postalcodemsg').innerHTML="Home Postal Code *";
			$('reg_border').setStyle('display','block');
			$('agent_coy_info').setStyle('display','none');
			$('register_div').setStyle('display','block');
			$('standard_table').setStyle('display','block');
			$('agent_owner_table').setStyle('display','block');
			$('login_details_table').setStyle('display','block');
			$('organization_table').setStyle('display','none');
			$('country_table').setStyle('display','block');
			$('mobile_div').setStyle('display','block');
			$('phone1_div').setStyle('display','none');
			$('personal_details_table').setStyle('display','block');
			$('home_tel_table').setStyle('display','block');
			$('personal_table').setStyle('display','none');
			$('phone_table').setStyle('display','none');
			$('misc_table').setStyle('display','none');
			$('home_add_table').setStyle('display','block');
			$('rental_add_table').setStyle('display','block');
			$('receive_table').setStyle('display','block');
			$('button_div').setStyle('display','block');
			var company_name = $('company_name');
			company_name.className = "inputbox";
			var salute = $('salute');
			salute.className = "inputbox required";
			var name = $('name');
			name.className = "inputbox required";
			var nric = $('nric');
			nric.className = "inputbox required";
			var country = $('country');
			country.className = "inputbox required";
			var mobile_contact = $('mobile_contact');
			mobile_contact.className = "inputbox required";
			var home_address = $('home_address');
			home_address.className = "inputbox required";
			var home_postalcode = $('home_postalcode');
			home_postalcode.className = "inputbox required";
			
			var organization_name = $('organization_name');
			organization_name.className = "inputbox";
			var organization_address = $('organization_address');
			organization_address.className = "inputbox";
			var phone_1 = $('phone_1');
			phone_1.className = "inputbox";

		} else if (sel.options.selectedIndex == 4){
			var usertype = 'Agent';
			document.getElementById('reg_header').innerHTML="Agent Registration";
			document.getElementById('namemsg').innerHTML="Given Name *";
			document.getElementById('nricmsg').innerHTML="NRIC *";
			document.getElementById('countrymsg').innerHTML="Country *";
			document.getElementById('mobile_contactmsg').innerHTML="Mobile Number *";
			document.getElementById('home_addressmsg').innerHTML="Home Address";
			document.getElementById('home_postalcodemsg').innerHTML="Home Postal Code";
			$('reg_border').setStyle('display','block');
			$('agent_coy_info').setStyle('display','block');
			$('register_div').setStyle('display','block');
			$('standard_table').setStyle('display','block');
			$('agent_owner_table').setStyle('display','block');
			$('login_details_table').setStyle('display','block');
			$('organization_table').setStyle('display','none');
			$('country_table').setStyle('display','block');
			$('mobile_div').setStyle('display','block');
			$('phone1_div').setStyle('display','none');
			$('personal_details_table').setStyle('display','block');
			$('home_tel_table').setStyle('display','none');
			$('personal_table').setStyle('display','block');
			$('phone_table').setStyle('display','none');
			$('misc_table').setStyle('display','none');
			$('home_add_table').setStyle('display','block');
			$('rental_add_table').setStyle('display','none');
			$('receive_table').setStyle('display','block');
			$('button_div').setStyle('display','block');
			var company_name = $('company_name');
			company_name.className = "inputbox required";
			var salute = $('salute');
			salute.className = "inputbox required";
			var name = $('name');
			name.className = "inputbox required";
			var nric = $('nric');
			nric.className = "inputbox required";
			var country = $('country');
			country.className = "inputbox required";
			var mobile_contact = $('mobile_contact');
			mobile_contact.className = "inputbox required";
			var home_address = $('home_address');
			home_address.className = "inputbox";
			var home_postalcode = $('home_postalcode');
			home_postalcode.className = "inputbox";
			
			var organization_name = $('organization_name');
			organization_name.className = "inputbox";
			var organization_address = $('organization_address');
			organization_address.className = "inputbox";
			var phone_1 = $('phone_1');
			phone_1.className = "inputbox";


		}
	});
}