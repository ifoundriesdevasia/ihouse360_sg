<?php // no direct access
defined('_JEXEC') or die('Restricted access'); 
require_once( JPATH_COMPONENT.DS.'image.php' );

?>

<script type="text/javascript">
<!--
	Window.onDomReady(function(){
		document.formvalidator.setHandler('passverify', function (value) { return ($('password').value == value); }	);
	});
// -->
</script>

<script type="text/javascript" src="<?php echo JURI::root(); ?>components/com_user/js/userselect.js?r=<?php echo microtime(true); ?>"></script>
<?php
	if(isset($this->message)){
		$this->display('message');
	}
	

?>

<form action="<?php echo JRoute::_( 'index.php?option=com_user' ); ?>" method="post" id="josForm" name="josForm" class="form-validate" onsubmit="submit_frm(this);" enctype="multipart/form-data">
<div style="padding-left:20px; padding-bottom:20px;" id="selection_div">
Please choose the type of Users you would like to register as.&nbsp;&nbsp;
<select id="user_category" name="user_category" class="ajax_input" onchange="javascript:select_user(this);return false;">
<option value="">Please select</option>
<option value="Individual">Individual</option>
<option value="Merchant">Merchant</option>
<option value="Owner">Owner</option>
<option value="Agent">Agent</option>
</select>
</div>
<div id="agent_info" style="padding:20px;">
* 68 credits will be credit into his/her account once registering as an Agent is successful.
</div>


<div id="reg_border" style="display:none;">
<div id="reg_header"></div>
</div>

<div id="agent_coy_info" style="display:none;">
<div id="reg_agent_cont"><div id="reg_agent_bg"><div id="reg_agent_header">Company Information</div></div></div>

<div id="register-table">
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane"  id="user-table">
<tr>
	<td width="40%" height="40">
		<label id="company_namemsg" for="company_name">
			<?php echo JText::_( 'Serving Organization' ); ?> *
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="company_name" id="company_name" size="40" value="<?php echo $this->escape($this->user->get( 'company_name' ));?>" class="inputbox" maxlength="50" />
        </div>
	</td>
  	<td width="40%" height="60">
    <div style="padding-top:12px;">
  		<label id="company_logomsg" for="company_logo">
			<?php echo JText::_( 'Company Logo' ); ?>
		</label>
    </div>
        <div style="padding-top:5px;">
        <?php
			frontimageUpload($this->user->get('company_logo'),$this->user->get('company_logo'),'company_logo');
		?>
        </div>
  	</td>
</tr>
</table>
</div>

<div id="reg_agent_cont"><div id="reg_agent_bg">
<div id="reg_agent_header">Personal Particular</div>
</div></div>

</div>

<div id="register_div" style="display:none;">
<div id="register-table">
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="standard_table">
<tr>
	<td width="40%" height="60">
		<label id="salutemsg" for="salute">
			<?php echo JText::_( 'Salute' ); ?> *
		</label>
        <div style="padding-top:5px;">
        <?php echo JHTML::_('select.genericlist',  $this->saluteOptions, 'salute', 'class="inputbox" size="1"', 'value', 'text', $this->user->get('salute') );?>
        </div>
	</td>
  	<td width="40%" height="60">
  		<label id="namemsg" for="name">
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="name" id="name" size="40" value="<?php echo $this->escape($this->user->get( 'name' ));?>" class="inputbox" />
        </div>
  	</td>
</tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="agent_owner_table">
<tr>
	<td width="40%" height="60">
		<label id="surnamemsg" for="surname">
			<?php echo JText::_( 'Surname' ); ?> *
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="surname" id="surname" size="40" value="<?php echo $this->escape($this->user->get( 'surname' ));?>" class="inputbox" maxlength="50" />
        </div>
	</td>
  	<td width="40%" height="60">
  		<label id="chinese_namemsg" for="chinese_name">
			<?php echo JText::_( 'Chinese Name' ); ?>
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="chinese_name" id="chinese_name" size="40" value="<?php echo $this->escape($this->user->get( 'chinese_name' ));?>" class="inputbox" maxlength="50" />
        </div>
  	</td>
</tr>
<tr>
	<td width="40%" height="60">
		<label id="nricmsg" for="nric">
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="nric" id="nric" size="40" value="<?php echo $this->escape($this->user->get( 'nric' ));?>" class="inputbox" maxlength="50" />
        </div>
	</td>
  	<td width="40%" height="60">
    	<div style="padding-top:12px;">
  		<label id="user_imagemsg" for="user_image">
			<?php echo JText::_( 'Your Image' ); ?>
		</label>
        </div>
        <div style="padding-top:5px;">
        <?php
			frontimageUpload($this->user->get('user_image'),$this->user->get('user_image'),'user_image');
		?>

        </div>
  	</td>
</tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="login_details_table">
<tr>
	<td width="40%" height="60">
		<label id="usernamemsg" for="username">
			<?php echo JText::_( 'Email Address' ); ?> * (User Login ID)
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="username" id="username" size="40" value="<?php echo $this->escape($this->user->get( 'username' ));?>" class="inputbox required validate-email" />
       
        </div>
	</td>
	<td width="40%" height="60">&nbsp;
		
	</td>
</tr>
<tr>
	<td width="40%" height="60">
		<label id="pwmsg" for="password">
			<?php echo JText::_( 'Password' ); ?> *
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox required validate-password" type="password" id="password" name="password" size="40" value="" />
        </div>
	</td>
  	<td width="40%" height="60">
		<label id="pw2msg" for="password2">
			<?php echo JText::_( 'Verify Password' ); ?> *
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox required validate-passverify" type="password" id="password2" name="password2" size="40" value="" />
        </div>
	</td>
</tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="organization_table">
<tr>
	<td width="40%" height="60">
		<label id="organization_namemsg" for="organization_name">
			<?php echo JText::_( 'Organization Name' ); ?> *
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="organization_name" name="organization_name" size="40" value="" />
        </div>
	</td>
  	<td width="40%" height="60">
		<label id="organization_addressmsg" for="organization_address">
			<?php echo JText::_( 'Organization Address' ); ?> *
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="organization_address" name="organization_address" size="40" value="" />
        </div>
	</td>
</tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="country_table">
<tr>
<td width="40%" height="60">
  		<label id="countrymsg" for="country">
		</label>
        <div style="padding-top:5px;">
        <?php echo JHTML::_('select.genericlist',  $this->countryOptions, 'country', 'class="inputbox" size="1"', 'value', 'text', $this->user->get('country') );?>
        </div>
  	</td>
<td width="40%" height="60">
	<div id="mobile_div">
		<label id="mobile_contactmsg" for="mobile_contact">
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="mobile_contact" id="mobile_contact" size="40" value="<?php echo $this->escape($this->user->get( 'mobile_contact' ));?>" class="inputbox" maxlength="50" />
        </div>
     </div>
     <div id="phone1_div">
		<label id="phone_1msg" for="phone_1">
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="phone_1" id="phone_1" size="40" value="<?php echo $this->escape($this->user->get( 'phone_1' ));?>" class="inputbox" maxlength="50" />
        </div>
     </div>
	</td>
</tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="personal_details_table">
<tr>
	<td width="40%" height="60">
		<label id="dobmsg" for="dob">
			<?php echo JText::_( 'Date of Birth' ); ?>
		</label>
        <div style="padding-top:5px;">
        <?php echo $this->dayList;?>&nbsp;
		<?php echo $this->monthList;?>&nbsp;
        <?php echo $this->yearList;?>
        </div>
	</td>
    <td width="40%" height="60">
		<div id="home_tel_table">
        <label id="home_contactmsg" for="home_contact">
			<?php echo JText::_( 'Home Telephone' ); ?>
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="home_contact" id="home_contact" size="40" value="<?php echo $this->escape($this->user->get( 'home_contact' ));?>" class="inputbox" maxlength="50" />
        </div>
        </div>
        
        <div id="personal_table">
        <label id="websitemsg" for="website">
			<?php echo JText::_( 'Personal Website' ); ?>
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="website" name="website" size="40" value="" />
        </div>
        </div>
	</td>
</tr>
</table>


<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="phone_table">
<tr>
<td width="40%" height="60">
		<label id="phone_2msg" for="phone_2">
			<?php echo JText::_( 'Telephone 2' ); ?>
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="phone_2" name="phone_2" size="40" value="" />
        </div>
	</td>
<td width="40%" height="60">
		<label id="faxmsg" for="fax">
			<?php echo JText::_( 'Fax' ); ?>
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="fax" name="fax" size="40" value="" />
        </div>
	</td>
</tr>
<tr>
<td width="40%" height="60">
		<label id="merchant_interestmsg" for="merchant_interest">
			<?php echo JText::_( 'Interest In' ); ?>
		</label>
        <div style="padding-top:5px;">
        <?php echo JHTML::_('select.genericlist',  $this->merchantintOptions, 'merchant_interest', 'class="inputbox" size="1"', 'value', 'text', $this->user->get('merchant_interest') );?>
        </div>
	</td>
<td width="40%" height="60">&nbsp;
	
	</td>
</tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="misc_table">
<tr>
<td width="40%" height="60">
		<label id="residence_statusmsg">
			<?php echo JText::_( 'Residence Status' ); ?>
		</label>
        <div style="padding-top:5px;">
        <?php echo JHTML::_('select.genericlist',  $this->residenceOptions, 'residence_status', 'class="inputbox" size="1"', 'value', 'text', $this->user->get('residence_status') );?>
        </div>
	</td>
<td width="40%" height="60">
		<label id="user_interestmsg" for="user_interest">
			<?php echo JText::_( 'Interest In' ); ?>
		</label>
        <div style="padding-top:5px;">
        <?php echo JHTML::_('select.genericlist',  $this->userintOptions, 'user_interest', 'class="inputbox" size="1"', 'value', 'text', $this->user->get('user_interest') );?>
        </div>
	</td>
</tr>
</table>



<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="home_add_table">
<tr>
	<td width="40%" height="60">
		<label id="home_addressmsg" for="home_address">
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="home_address" name="home_address" size="40" value="" />
        </div>
	</td>
  	<td width="40%" height="60">
		<label id="home_postalcodemsg" for="home_postalcode">
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="home_postalcode" name="home_postalcode" size="40" value="" />
        </div>
	</td>
</tr>
</table>


<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="rental_add_table">
<tr>
<td width="40%" height="60">
		<label id="rental_addressmsg" for="rental_address">
			<?php echo JText::_( 'Rental Address' ); ?>
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="rental_address" name="rental_address" size="40" value="" />
        </div>
	</td>
<td width="40%" height="60">
		<label id="rental_postalcodemsg" for="rental_postalcode">
			<?php echo JText::_( 'Rental Postal Code' ); ?>
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="rental_postalcode" name="rental_postalcode" size="40" value="" />
        </div>
	</td>
</tr>
</table>


<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="receive_table">
<tr>
<td width="25%" height="60">
Agree to receive
</td>
<td width="60%" height="60">
<input type="checkbox" name="receive[]" id="receive[]" value="ihouse360 Newsletter" checked="checked" />&nbsp;ihouse360 Newsletter&nbsp;&nbsp;&nbsp;<input type="checkbox" name="receive[]" id="receive[]" value="3rd Party Newsletter" checked="checked" />&nbsp;Third Party Newsletter<br /><input type="checkbox" name="receive[]" id="receive[]" value="Email Alert When Customer Enquiry" checked="checked" />Email Alert When Customer Enquiry
</td>
</tr>
</table>

<table>
<tr>
	<td colspan="2" height="40">
		<?php echo JText::_( '* Compulsary field' ); ?>
	</td>
</tr>
</table>
</div>

</div>

<?php /*?><?php if ( $this->params->def( 'show_page_title', 1 ) ) : ?>
<div class="componentheading<?php echo $this->escape($this->params->get('pageclass_sfx')); ?>">
<?php echo $this->escape($this->params->get('page_title')); ?>
</div>
<div class="linebreak">&nbsp;</div>
<?php endif; ?><?php */?>

<div id="button_div" style="display:none; padding-left:30px;">
	<button class="contactbutton validate" type="submit"><?php echo JText::_('Register'); ?></button>
	<input type="hidden" name="task" value="register_save" />
    <input type="hidden" id="email" name="email"  value=""/>
	<input type="hidden" name="id" value="0" />
	<input type="hidden" name="gid" value="0" />
	<?php echo JHTML::_( 'form.token' ); ?>
</div>
<script>
function submit_frm()
{
var userval = document.josForm.username.value;
document.josForm.email.value=userval;
} 
</script>

</form>
