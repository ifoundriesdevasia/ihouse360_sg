<?php
/**
* @version		$Id: view.html.php 14401 2010-01-26 14:10:00Z louis $
* @package		Joomla
* @subpackage	Registration
* @copyright	Copyright (C) 2005 - 2010 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');
require_once(JPATH_ROOT.DS.'includes'.DS.'tools.php');
/**
 * HTML View class for the Registration component
 *
 * @package		Joomla
 * @subpackage	Registration
 * @since 1.0
 */
class UserViewRegister extends JView
{
	function display($tpl = null)
	{
		global $mainframe;

		// Check if registration is allowed
		$usersConfig = &JComponentHelper::getParams( 'com_users' );
		if (!$usersConfig->get( 'allowUserRegistration' )) {
			JError::raiseError( 403, JText::_( 'Access Forbidden' ));
			return;
		}

		$pathway  =& $mainframe->getPathway();
		$document =& JFactory::getDocument();
		$params	= &$mainframe->getParams();

	 	// Page Title
		$menus	= &JSite::getMenu();
		$menu	= $menus->getActive();

		// because the application sets a default page title, we need to get it
		// right from the menu item itself
		if (is_object( $menu )) {
			$menu_params = new JParameter( $menu->params );
			if (!$menu_params->get( 'page_title')) {
				$params->set('page_title',	JText::_( 'Registration' ));
			}
		} else {
			$params->set('page_title',	JText::_( 'Registration' ));
		}
		$document->setTitle( $params->get( 'page_title' ) );

		$pathway->addItem( JText::_( 'New' ));

		// Load the form validation behavior
		JHTML::_('behavior.formvalidation');
		
		
		$saluteOptions[] = JHTML::_('select.option', '', JText::_('Please Select'));
		$saluteOptions[] = JHTML::_('select.option', 'Mr', JText::_('Mr'));
		$saluteOptions[] = JHTML::_('select.option', 'Mrs', JText::_('Mrs'));
		$saluteOptions[] = JHTML::_('select.option', 'Mdm', JText::_('Mdm'));
		$saluteOptions[] = JHTML::_('select.option', 'Miss', JText::_('Miss'));
		$saluteOptions[] = JHTML::_('select.option', 'Dr', JText::_('Dr'));
		
		$residenceOptions[] = JHTML::_('select.option', '', JText::_('Please Select'));
		$residenceOptions[] = JHTML::_('select.option', 'Singapore Citizen', JText::_('Singapore Citizen'));
		$residenceOptions[] = JHTML::_('select.option', 'Singapore PR', JText::_('Singapore PR'));
		$residenceOptions[] = JHTML::_('select.option', 'Others', JText::_('Others'));
		
		$userintOptions[] = JHTML::_('select.option', '', JText::_('Please Select'));
		$userintOptions[] = JHTML::_('select.option', 'Rent', JText::_('Rent Property'));
		$userintOptions[] = JHTML::_('select.option', 'Buy', JText::_('Buy Property'));
		$userintOptions[] = JHTML::_('select.option', 'None', JText::_('None'));
		
		$merchantintOptions[] = JHTML::_('select.option', '', JText::_('Please Select'));
		$merchantintOptions[] = JHTML::_('select.option', 'Place Ads', JText::_('Place Ads'));
		$merchantintOptions[] = JHTML::_('select.option', 'EDM Services', JText::_('EDM Services'));
		$merchantintOptions[] = JHTML::_('select.option', 'Receive Free Events Invitation', JText::_('Receive Free Events Invitation'));
		
		$country_list = array(

		"Afghanistan",
	
		"Albania",
	
		"Algeria",
	
		"Andorra",
	
		"Angola",
	
		"Antigua and Barbuda",
	
		"Argentina",
	
		"Armenia",
	
		"Australia",
	
		"Austria",
	
		"Azerbaijan",
	
		"Bahamas",
	
		"Bahrain",
	
		"Bangladesh",
	
		"Barbados",
	
		"Belarus",
	
		"Belgium",
	
		"Belize",
	
		"Benin",
	
		"Bhutan",
	
		"Bolivia",
	
		"Bosnia and Herzegovina",
	
		"Botswana",
	
		"Brazil",
	
		"Brunei",
	
		"Bulgaria",
	
		"Burkina Faso",
	
		"Burundi",
	
		"Cambodia",
	
		"Cameroon",
	
		"Canada",
	
		"Cape Verde",
	
		"Central African Republic",
	
		"Chad",
	
		"Chile",
	
		"China",
	
		"Colombi",
	
		"Comoros",
	
		"Congo (Brazzaville)",
	
		"Congo",
	
		"Costa Rica",
	
		"Cote d��Ivoire",
	
		"Croatia",
	
		"Cuba",
	
		"Cyprus",
	
		"Czech Republic",
	
		"Denmark",
	
		"Djibouti",
	
		"Dominica",
	
		"Dominican Republic",
	
		"East Timor (Timor Timur)",
	
		"Ecuador",
	
		"Egypt",
	
		"El Salvador",
	
		"Equatorial Guinea",
	
		"Eritrea",
	
		"Estonia",
	
		"Ethiopia",
	
		"Fiji",
	
		"Finland",
	
		"France",
	
		"Gabon",
	
		"Gambia, The",
	
		"Georgia",
	
		"Germany",
	
		"Ghana",
	
		"Greece",
	
		"Grenada",
	
		"Guatemala",
	
		"Guinea",
	
		"Guinea-Bissau",
	
		"Guyana",
	
		"Haiti",
	
		"Honduras",
	
		"Hungary",
	
		"Iceland",
	
		"India",
	
		"Indonesia",
	
		"Iran",
	
		"Iraq",
	
		"Ireland",
	
		"Israel",
	
		"Italy",
	
		"Jamaica",
	
		"Japan",
	
		"Jordan",
	
		"Kazakhstan",
	
		"Kenya",
	
		"Kiribati",
	
		"Korea, North",
	
		"Korea, South",
	
		"Kuwait",
	
		"Kyrgyzstan",
	
		"Laos",
	
		"Latvia",
	
		"Lebanon",
	
		"Lesotho",
	
		"Liberia",
	
		"Libya",
	
		"Liechtenstein",
	
		"Lithuania",
	
		"Luxembourg",
	
		"Macedonia",
	
		"Madagascar",
	
		"Malawi",
	
		"Malaysia",
	
		"Maldives",
	
		"Mali",
	
		"Malta",
	
		"Marshall Islands",
	
		"Mauritania",
	
		"Mauritius",
	
		"Mexico",
	
		"Micronesia",
	
		"Moldova",
	
		"Monaco",
	
		"Mongolia",
	
		"Morocco",
	
		"Mozambique",
	
		"Myanmar",
	
		"Namibia",
	
		"Nauru",
	
		"Nepa",
	
		"Netherlands",
	
		"New Zealand",
	
		"Nicaragua",
	
		"Niger",
	
		"Nigeria",
	
		"Norway",
	
		"Oman",
	
		"Pakistan",
	
		"Palau",
	
		"Panama",
	
		"Papua New Guinea",
	
		"Paraguay",
	
		"Peru",
	
		"Philippines",
	
		"Poland",
	
		"Portugal",
	
		"Qatar",
	
		"Romania",
	
		"Russia",
	
		"Rwanda",
	
		"Saint Kitts and Nevis",
	
		"Saint Lucia",
	
		"Saint Vincent",
	
		"Samoa",
	
		"San Marino",
	
		"Sao Tome and Principe",
	
		"Saudi Arabia",
	
		"Senegal",
	
		"Serbia and Montenegro",
	
		"Seychelles",
	
		"Sierra Leone",
	
		"Singapore",
	
		"Slovakia",
	
		"Slovenia",
	
		"Solomon Islands",
	
		"Somalia",
	
		"South Africa",
	
		"Spain",
	
		"Sri Lanka",
	
		"Sudan",
	
		"Suriname",
	
		"Swaziland",
	
		"Sweden",
	
		"Switzerland",
	
		"Syria",
	
		"Taiwan",
	
		"Tajikistan",
	
		"Tanzania",
	
		"Thailand",
	
		"Togo",
	
		"Tonga",
	
		"Trinidad and Tobago",
	
		"Tunisia",
	
		"Turkey",
	
		"Turkmenistan",
	
		"Tuvalu",
	
		"Uganda",
	
		"Ukraine",
	
		"United Arab Emirates",
	
		"United Kingdom",
	
		"United States",
	
		"Uruguay",
	
		"Uzbekistan",
	
		"Vanuatu",
	
		"Vatican City",
	
		"Venezuela",
	
		"Vietnam",
	
		"Yemen",
	
		"Zambia",
	
		"Zimbabwe"
	
		);
	
		$countryOptions[] = JHTML::_('select.option', '', JText::_('Please Select'));
		foreach ($country_list as $country) {
		$countryOptions[] = JHTML::_('select.option', $country,$country);
		}
		
		// building day list
		$daysList[] = JHTML::_('select.option', 'dd','day');
		for ( $i = 1; $i <= 31; $i++ )
		{
		$daysList[] = JHTML::_('select.option', $i,$i);
		}
		$dayList = JHTML::_('select.genericlist',  $daysList, 'day', 'class="dinputbox" size="1"', 'value', 'text', '' );

		
		// building month list
		$monthsList[] = JHTML::_('select.option', 'mm','month');
		$monthsList[] = JHTML::_('select.option', '1', JText::_('January'));
		$monthsList[] = JHTML::_('select.option', '2', JText::_('February'));
		$monthsList[] = JHTML::_('select.option', '3', JText::_('March'));
		$monthsList[] = JHTML::_('select.option', '4', JText::_('April'));
		$monthsList[] = JHTML::_('select.option', '5', JText::_('May'));
		$monthsList[] = JHTML::_('select.option', '6', JText::_('June'));
		$monthsList[] = JHTML::_('select.option', '7', JText::_('July'));
		$monthsList[] = JHTML::_('select.option', '8', JText::_('August'));
		$monthsList[] = JHTML::_('select.option', '9', JText::_('September'));
		$monthsList[] = JHTML::_('select.option', '10', JText::_('October'));
		$monthsList[] = JHTML::_('select.option', '11', JText::_('November'));
		$monthsList[] = JHTML::_('select.option', '12', JText::_('December'));
		$monthList = JHTML::_('select.genericlist',  $monthsList, 'month', 'class="minputbox" size="1"', 'value', 'text', '' );

		
		// building year list
		$yearsList[] = JHTML::_('select.option', 'yyyy','year');
		$y = date ( "Y", extcal_get_local_time ( ) );
		
		for ( $i = 1; $i <= 70; $i++ )
		{
		$yearsList[] = JHTML::_('select.option', $y,$y);
		
			$y--;
		}
		$yearList = JHTML::_('select.genericlist',  $yearsList, 'year', 'class="yinputbox" size="1"', 'value', 'text', '' );


		$user =& JFactory::getUser();
		$this->assignRef('user', $user);
		$this->assignRef('params',		$params);
		$this->assignRef('saluteOptions', $saluteOptions );
		$this->assignRef('residenceOptions', $residenceOptions );
		$this->assignRef('userintOptions', $userintOptions );
		$this->assignRef('merchantintOptions', $merchantintOptions );
		$this->assignRef('countryOptions', $countryOptions );
		$this->assignRef('dayList', $dayList );
		$this->assignRef('monthList', $monthList );
		$this->assignRef('yearList', $yearList );
		parent::display($tpl);
	}
}
