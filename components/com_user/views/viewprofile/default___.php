﻿<?php defined('_JEXEC') or die; ?>
<?php JHTML::_('script', 'ihouse.js', 'components/com_ads/rokbox/'); ?>
<!--Start View-->
<div style="margin-top:-20px;">

<div id="main-search-header-text" style="padding-left:15px;border-width:2px;">会员中心&nbsp;<span style="font-size:15px;font-weight:100;"><i><?php echo $this->users->user_category ?>&nbsp;Centre</i></span></div>
<?php 
	if(empty($this->users->user_image)) {
	$user_image = JRoute::_('templates/main/images/no_avatar.jpg');
	}else{
	$user_image = 'image.php?size=120&type=1&path='.$this->users->user_image;
	}
	
	if(empty($this->users->company_logo)) {
	$company_logo = JRoute::_('templates/main/images/thumb-nologo.jpg');
	}else{
	$company_logo = 'image.php?size=120&type=1&path='.$this->users->company_logo;
	}
	
	$condo = explode(",", $this->sareas->condo);
	$condo_type = explode(",", $this->sareas->condo_type);
	$hdb = explode(",", $this->sareas->hdb);
	$hdb_type = explode(",", $this->sareas->hdb_type);
	$private_estate = explode(",", $this->sareas->private_estate);
	$industrial_estate = explode(",", $this->sareas->industrial_estate);
	
	$db = & JFactory::getDBO();
	for($a = 0; $a < (count($hdb_type)); $a++){
	$query = "SELECT * FROM #__ihouse_hdb_town WHERE serial_no = '".$hdb_type[$a]."'";
		$db->setQuery($query);
		$hdbtype[] = $db->loadRow();
	}
?>
<style type="text/css">
#tablist {
	border-left:3px solid #FF8200;
	border-bottom:3px solid #FF8200;
	border-right:3px solid #FF8200;
	width:99.1%;
	padding:0;
	margin:0;
	min-height:300px;
	height:auto;
	float:left;
}

table#prop_det_info td{
	border:1px solid #999;
}

ul.tab_nav5 {
list-style-image:none;
list-style-type:none;
}
</style>

<div style="padding:15px;"><!--start-->

<div id="menu_list_profile_detail">
<ul id="" class="tab_nav5">     
	<li id="tab_li5_account"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=account_info&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">账号信息</span></a></li>
     <?php
	if(($this->users->user_category == 'Agent')||($this->users->user_category == 'Owner')||($this->users->user_category == 'Individual')){
	?>
	<li id="tab_li5_profile" class="tabactive5"><a href="<?php echo JURI::root();?>index.php?option=com_user&view=profiler&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">个人资料</span></a></li>
    <?php
	}
	if(($this->users->user_category == 'Agent')||($this->users->user_category == 'Owner')){
	?>
	<li id="tab_li5_ads"><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=ads_management&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">房地产广告</span></a></li>
    <?php
	}
	if($this->users->user_category == 'Agent'){
	?>
    <li id="tab_li5_transaction" ><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=transaction&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">交易记录</span></a></li>
    <li id="tab_li5_top_up" ><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=top_up&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">充值</span></a></li>
    <?php
	}
	?>
</ul>   
</div>

<div id="tablist"> 
    <div id="tab_profile" class="tabular1">
    	<div style="padding:15px;">
            <div>
            	<div id="static-text">
                    	个人资料&nbsp;&nbsp;<font style="color:#494949;">Profile</font>
                    </div>
                    
                    <div style="float:left; width:630px; height:auto;">
                    <?php
					if(($this->users->user_category == 'Owner')||($this->users->user_category == 'Agent')){
					?>
                    <!--Left view-->
                    <div style="width:130px; float:left; padding-right:10px;">
                    	<div style="width:130px; height:auto; float:left;">
                            <img src="<?php echo $user_image; ?>" />
                        </div>
                        <div style="width:130px; height:auto; float:left; padding-top:5px;">
                            <img src="<?php echo $company_logo; ?>" />
                        </div>
                        
                        <div id="profile_menu">
                            <input type="button" value="Edit Profile" class="button9022" onClick="location.href='index.php?option=com_user&view=edit_profile&Itemid=126&id=<?php echo $this->users->id;?>'" />
                        </div>
                    </div>
                    <!--End Left view-->
                    
                    <!--Right view-->
                    <div style="float:left; width:490px; height:auto;">
                   
					<?php
					}
					?>
                    <?php
						if($this->users->user_category == 'Agent'){
					?>
                    <div style="float:left; width:490px; height:auto;">
                    	<div style="float:left; width:160px; height:40px; border-right:dotted 2px #fcd29d; border-bottom:dotted 2px #fcd29d; padding-top:10px;">
                        	<div align="center" style="font-weight:bold;"><font color="#fb9116">经纪证号</font><br />CEA Registration No.</div>
                        </div>
                        
                        <div style="float:left; width:320px; border-bottom:dotted 2px #fcd29d;">
                        	<div style="padding-left:20px;  width:280px; height:30px; float:left; padding-top:20px;">
							<?php echo $this->users->cea_reg_no;?>
                            </div>
                        </div>
                    </div>
                    
                     <div style="float:left; width:490px; height:auto;">
                    	<div style="float:left; width:160px; height:40px; border-right:dotted 2px #fcd29d; border-bottom:dotted 2px #fcd29d; padding-top:10px;">
                        	<div align="center" style="font-weight:bold;"><font color="#fb9116">公司名称</font><br />Serving Organization</div>
                        </div>
                        
                        <div style="float:left; width:320px; border-bottom:dotted 2px #fcd29d;">
                        	<div style="padding-left:20px;  width:280px; height:30px; float:left; padding-top:20px;">
							<?php echo $this->users->company_name;?>
                            </div>
                        </div>
                    </div>
                     <?php
						}
					 ?>
                     
                     <div style="float:left; width:490px; height:auto;">
                    	<div style="float:left; width:160px; height:40px; border-right:dotted 2px #fcd29d; border-bottom:dotted 2px #fcd29d; padding-top:10px;">
                        	<div align="center" style="font-weight:bold;"><font color="#fb9116">姓名</font><br />Name</div>
                        </div>
                        
                        <div style="float:left; width:320px; border-bottom:dotted 2px #fcd29d;">
                        	<div style="padding-left:20px;  width:280px; height:30px; float:left; padding-top:20px;">
							<?php echo $this->users->name. ' ' . $this->users->chinese_name ?>
                            </div>
                        </div>
                    </div>
                    
                    <div style="float:left; width:490px; height:auto;">
                    	<div style="float:left; width:160px; height:40px; border-right:dotted 2px #fcd29d; border-bottom:dotted 2px #fcd29d; padding-top:10px;">
                        	<div align="center" style="font-weight:bold;"><font color="#fb9116">手机</font><br />Mobile Number</div>
                        </div>
                        
                        <div style="float:left; width:320px; border-bottom:dotted 2px #fcd29d;">
                        	<div style="padding-left:20px;  width:280px; height:30px; float:left; padding-top:20px;">
							<?php if($this->users->mobile_contact == '0'){ echo "Unknown"; } else { echo $this->users->mobile_contact; } ?>
                            </div>
                        </div>
                    </div>
                    <?php
					if($this->users->user_category == 'Owner'){
					?>
                    <div style="float:left; width:490px; height:auto;">
                    	<div style="float:left; width:160px; height:40px; border-right:dotted 2px #fcd29d; border-bottom:dotted 2px #fcd29d; padding-top:10px;">
                        	<div align="center" style="font-weight:bold;"><font color="#fb9116">住家电话</font><br />Home Contact</div>
                        </div>
                        
                        <div style="float:left; width:320px; border-bottom:dotted 2px #fcd29d;">
                        	<div style="padding-left:20px;  width:280px; height:30px; float:left; padding-top:20px;">
							<?php if($this->users->home_contact == '0'){ echo "Unknown"; } else { echo $this->users->home_contact; } ?>
                            </div>
                        </div>
                    </div>
                    <?php
					}
					if($this->users->user_category == 'Agent'){
					?>
                    <div style="float:left; width:490px; height:auto;">
                    	<div style="float:left; width:160px; height:40px; border-right:dotted 2px #fcd29d; border-bottom:dotted 2px #fcd29d; padding-top:10px;">
                        	<div align="center" style="font-weight:bold;"><font color="#fb9116">网址</font><br />Website</div>
                        </div>
                        
                        <div style="float:left; width:320px; border-bottom:dotted 2px #fcd29d;">
                        	<div style="padding-left:20px;  width:280px; height:30px; float:left; padding-top:20px;">
							<?php if($this->users->website == ''){ echo "Unknown"; } else { echo $this->users->website; } ?>
                            </div>
                        </div>
                    </div>
                    
                    <div style="float:left; width:490px; height:auto;">
                    	<div style="float:left; width:490px; height:auto; border-bottom:dotted 2px #fcd29d;">
                    	<div style="float:left; width:160px; height:auto; padding-top:10px;">
                        	<div align="center" style="font-weight:bold;"><font color="#fb9116">简介</font><br />Description</div>
                        </div>
                        
                        <div style="float:left; width:320px; border-left:dotted 2px #fcd29d; padding-bottom:20px;">
                        	<div style="padding-left:20px;  width:280px; height:auto; float:left; padding-top:20px;">
							<?php if($this->users->short_desc == ''){ echo "Unknown"; } else { echo $this->users->short_desc; } ?>
                            </div>
                        </div>
                        </div>
                    </div>
                    <?php
					}
					?>
                            
                    </div>
                    <!--End Right view-->
                    <?php
					if($this->users->user_category == "Agent"){
					?>
                    <div style="padding-top:20px; float:left; width:100%; height:auto; padding-bottom:20px;">
                            <div style="padding-left:10px; line-height:32px; font-weight:bold; background-color:#908373; width:615px; height:auto; color:#fff; font-size:14px;">专注于 Service Provide</div>
                            <div style="float:left; width:623px; height:auto; border:solid 1px #e1e8ff;">
                            
                                <div style="float:left; width:623px; height:70px; border-bottom:solid 1px #e1e8ff;">
                                    <div style="float:left; width:70px; height:50px; color:#fe8500; font-weight:bold; font-size:13px; padding-top:10px; padding-bottom:10px; background-color:#f6f6f6; padding-left:12px; padding-right:10px; ">
                                    公寓
                                    </div>
                                    <div style="float:left; width:530px; height:50px; border-left:solid 1px #e1e8ff;">
                                    	<div style="float:left; width:510px; padding:10px; border-bottom:solid 1px #e1e8ff;">
                                        	<?php 
											for($a = 0; $a < (count($condo)-1); $a++){
											echo $condo[$a];
											echo " , ";
											}
											echo $condo[(count($condo) - 1)];
											?>
                                        </div>
                                        
                                        <div style="float:left; width:510px;">
                                        	<div style="float:left; background-color:#f7f7f7; width:50px;">类型</div>
                                            <div style="float:left; width:400px;">
                                            	<?php 
												for($a = 0; $a < (count($condo_type)-1); $a++){
												echo $condo_type[$a];
												echo " , ";
												}
												echo $condo_type[(count($condo_type) - 1)];
												?>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                
                            <div style="float:left; width:623px; height:auto;">
                            <table cellspacing="0" cellpadding="0" width="623">
                                <tr>
                                	<td width="70" height="50" bgcolor="#f6f6f6">
                                    	<div style="color:#fe8500; font-weight:bold; font-size:13px; padding-left:10px; padding-top:10px;">公寓</div>
                                    </td>
                                    <td width="380" align="left" valign="top" colspan="2">
                                        <div style="float:left; width:300px; height:auto; padding-left:5px;">
                                        	<?php 
											for($a = 0; $a < (count($condo)-1); $a++){
											echo $condo[$a];
											echo " , ";
											}
											echo $condo[(count($condo) - 1)];
											?>
                                        </div>
                                    </td>
                            	</tr>
                                
                                <tr>
                                	<td width="80">
                                    	<div style="color:#334d7a; float:left; width:99%;">&nbsp;</div>
                                        <div style="float:right; width:1%; color:#334d7a;">&nbsp;</div>
                                    </td>
                                    <td width="40" align="left" valign="top">
                                        <div style="color:#334d7a; float:left; padding-left:5px; width:40px;">类型</div>
                                        <div style="float:right; width:1%; color:#334d7a;">:</div>
                                    </td>
                                    <td width="380" align="left" valign="top">
                                        <div style="float:left; width:300px; height:auto;">
                                        	<?php 
											for($a = 0; $a < (count($condo_type)-1); $a++){
											echo $condo_type[$a];
											echo " , ";
											}
											echo $condo_type[(count($condo_type) - 1)];
											?>
                                        </div>
                                    </td>
                            	</tr>
                            </table>
                            </div>
                            
                            <div style="border-bottom:solid 1px #dadada;">&nbsp;</div>
                            
                            <div style="float:left; padding-left:5px; padding-top:10px; width:620px; height:auto;">
                            <table cellspacing="3" cellpadding="3" width="100%">
                            	<tr>
                                	<td width="95">
                                    	<div style="color:#334d7a; float:left; width:99%;">HDB</div>
                                        <div style="float:right; width:1%; color:#334d7a;">:</div>
                                    </td>
                                    <td width="380" align="left" valign="top" colspan="2">
                                        <div style="float:left; width:300px; height:auto; padding-left:5px;">
                                        	<?php 
											for($a = 0; $a < (count($hdb)-1); $a++){
											echo $hdb[$a];
											echo " , ";
											}
											echo $hdb[(count($hdb) - 1)];
											?>
                                        </div>
                                    </td>
                            	</tr>
                                <tr>
                                	<td width="100" colspan="3">
                                    	<div style="color:#334d7a; float:left; width:94px;">HDB 区</div>
                                        <div style="float:left; width:1%; color:#334d7a;">:</div>
                                	</td>
                                </tr>
                                </table>
                                </div>
                                
                            <div style="float:left; padding-left:5px; width:625px; height:auto;">
                               <div style="float:left; width:625px; height:auto;">
                                    <div style="float:left; width:620px; height:auto; padding-left:5px;">
                                        	<?php 
											for($a = 0; $a < (count($hdbtype)-1); $a++){
											echo $hdbtype[$a][1].". ";
											echo $hdbtype[$a][2]." ";
											echo "(".$hdbtype[$a][3].")";
											echo " , ";
											}
											echo $hdbtype[(count($hdbtype) - 1)][1].". ";
											echo $hdbtype[(count($hdbtype) - 1)][2]." ";
											echo $hdbtype[(count($hdbtype) - 1)][3];
											?>
                                    </div>
                               </div>

                            </div>
                            
                            <div style="border-bottom:solid 1px #dadada;">&nbsp;</div>
                            
                            <div style="float:left; padding-left:5px; padding-top:10px; width:620px; height:auto;">
                            <table cellspacing="3" cellpadding="3" width="100%">
                            	<tr>
                                	<td width="95">
                                    	<div style="color:#334d7a; float:left; width:99%;">有地住宅</div>
                                        <div style="float:right; width:1%; color:#334d7a;">:</div>
                                    </td>
                                    <td width="380" align="left" valign="top" colspan="2">
                                        <div style="float:left; width:300px; height:auto; padding-left:5px;">
                                        	<?php 
											for($a = 0; $a < (count($private_estate)-1); $a++){
											echo $private_estate[$a];
											echo " , ";
											}
											echo $private_estate[(count($private_estate) - 1)];
											?>
                                        </div>
                                    </td>
                            	</tr>
                            </table>
                            </div>
                            
                            <div style="border-bottom:solid 1px #dadada;">&nbsp;</div>
                            
                            <div style="float:left; padding-left:5px; padding-top:10px; padding-bottom:10px; width:620px; height:auto;">
                            <table cellspacing="3" cellpadding="3" width="100%">
                            	<tr>
                                	<td width="95">
                                    	<div style="color:#334d7a; float:left; width:99%;">商业房产</div>
                                        <div style="float:right; width:1%; color:#334d7a;">:</div>
                                    </td>
                                    <td width="380" align="left" valign="top" colspan="2">
                                        <div style="float:left; width:300px; height:auto; padding-left:5px;">
                                        	<?php 
											for($a = 0; $a < (count($industrial_estate)-1); $a++){
											echo $industrial_estate[$a];
											echo " , ";
											}
											echo $industrial_estate[(count($industrial_estate) - 1)];
											?>
                                        </div>
                                    </td>
                            	</tr>
                            </table>
                            </div>

                            </div>
    
                    </div>      
                            
                    <?php 
					}
					?>
    
                    </div>
                    
                   <?php /*?> <div style="float:left; width:100%; height:auto; padding-top:20px;">
                    	<div style="border-top:solid 2px #CCCCCC; float:left; width:100%; height:5px;">&nbsp;</div>
                        <div style="float:left; width:440px; padding-bottom:5px;">
                        	<div id="profile_menu">
                            	<input type="button" value="Edit Profile" class="contactbutton" onClick="location.href='index.php?option=com_user&view=edit_profile&Itemid=126&id=<?php echo $this->users->id;?>'" />
                            </div>
                        </div>
                    </div><?php */?>
            </div>
        </div>
    </div><!--tabular1-->

</div>    

</div><!--End-->



</div>
<!--End View-->
