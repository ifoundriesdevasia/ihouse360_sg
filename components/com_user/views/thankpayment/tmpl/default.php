﻿<?php defined('_JEXEC') or die('Restricted access'); ?>
<div id="reg_border">
			<div id="reg_header">
			Registration Submitted 注册提交
            </div>
</div>

<div style="padding:20px;">

<?php
if(count($this->rows)){
foreach($this->rows AS $row){	
?>
<div style="margin:auto; padding:auto; width:640px;">

<div style="border:solid 1px #d7d7d7; width:640px; height:auto;">

<div style="background-color:#f8f8f8; border-bottom:solid 1px #d7d7d7; font-weight:bold; padding-left:10px; line-height:30px;">
Thank You
</div>


<div align="justify" id="returnmsg" style="padding:10px;">
Thank you for registering with iHOUSE360.com.<br />
谢谢您注册爱豪士环球佳居网<br /><br />

Please note that your account <b>is not yet active</b> - the account activation requires one more step to verify your details:<br />
请注意您的帐户还没激活 – 您需要再多一个验证步骤才可完成：<br /><br />

<b>Step 1</b> - You will receive an activation email from us. Please click on the activation link in the email.<br />
步骤1：您将收到我们发的激活邮件，请点击该邮件中的超链，以此完成您的帐户激活。<br /><br />

If you have not received your activation email within the next 1 hour, please call our help desk at +65 6567 1186 (Mon - Fri, 9am - 6pm).<br />
如果您在1小时内还没收到我们的激活邮件，请与我们的服务台联系，电话：+65 6567 1186 (星期一 -- 星期五, 9am - 6pm).<br /><br />

<font style="color:#FF0000;"><b>Note:</b> Some e-mail spam filters could send the activation email to the Junk Mail folder. Please check your Junk Mail folder before calling us.<br />
注意：有些邮件过滤器可能会把激活邮件放到垃圾邮件夹，请在电话联络我们以前先检查该邮件夹。<br /><br /></font>
<br /><br />
</div>


</div>
</div>
<?php
	}
}
?>

</div>
<?php /*?><div style="padding-left:25px; padding-bottom:10px; float:left;">
<input type="button" value="Continue" class="contactbutton" onClick="location.href='index.php'" />
</div><?php */?>
<div id="thanklink">Click <a href="<?php echo JURI::root();?>index.php">HERE</a> to get back to home page. </div>
<div id="thanklink" style="padding-bottom:20px;">点击 <a href="<?php echo JURI::root();?>index.php">这里</a> 回到主页。</div>