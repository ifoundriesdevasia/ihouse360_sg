﻿<?php 
defined('_JEXEC') or die; 
JHTML::_('behavior.tooltip'); 
require_once( JPATH_COMPONENT.DS.'image.php' );
?>
<?php JHTML::_('script', 'ihouse.js', 'components/com_ads/rokbox/'); ?>
<!--Start View-->
<div style="margin-top:-20px;">

<div id="main-search-header-text" style="padding-left:15px;border-width:2px;">会员中心&nbsp;<span style="font-size:15px;font-weight:100;"><i><?php echo $this->users->user_category ?>&nbsp;Centre</i></span></div>
<?php 
	if(empty($this->users->user_image)) {
	$user_image = JRoute::_('templates/main/images/no_avatar.jpg');
	}else{
	$user_image = 'image.php?size=136&type=1&path='.$this->users->user_image;
	}
	
	if(empty($this->users->company_logo)) {
	$company_logo = JRoute::_('templates/main/images/thumb-nologo.jpg');
	}else{
	$company_logo = 'image.php?size=120&type=1&path='.$this->users->company_logo;
	}
	
	$condo = explode(",", $this->sareas->condo);
	$condo_type = explode(",", $this->sareas->condo_type);
	$hdb = explode(",", $this->sareas->hdb);
	$hdb_type = explode(",", $this->sareas->hdb_type);
	$private_estate = explode(",", $this->sareas->private_estate);
	$industrial_estate = explode(",", $this->sareas->industrial_estate);
?>
<style type="text/css">
#tablist {
	border-left:3px solid #FF8200;
	border-bottom:3px solid #FF8200;
	border-right:3px solid #FF8200;
	width:99.1%;
	padding:0;
	margin:0;
	min-height:300px;
	height:auto;
	float:left;
}

table#prop_det_info td{
	border:1px solid #999;
}

ul.tab_nav3 {
list-style-image:none;
list-style-type:none;
}
</style>

<div style="padding:15px;"><!--start-->

<div id="menu_list_profile_detail">
<ul id="" class="tab_nav5">     
	<li id="tab_li5_account"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=account_info&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Acct. Info</span></a></li>  
     <?php
	if(($this->users->user_category == 'Agent')||($this->users->user_category == 'Owner')||($this->users->user_category == 'Individual')){
	?>
	<li id="tab_li5_profile" class="tabactive5"><a href="<?php echo JURI::root();?>index.php?option=com_user&view=profiler&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Profile</span></a></li>
    <?php
	}
	if(($this->users->user_category == 'Agent')||($this->users->user_category == 'Owner')){
	?>
	<li id="tab_li5_ads"><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=ads_management&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Your Ads</span></a></li>
    <?php
	}
	if($this->users->user_category == 'Agent'){
	?>
    <li id="tab_li5_transaction" ><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=transaction&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Transactions</span></a></li>
    <li id="tab_li5_top_up" ><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=top_up&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Top-Up</span></a></li>
    <?php
	}
	?>
    <li id="tab_li77_specialist"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=favoriteads&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Favorites</span></a></li>
</ul>
</div>

<div id="tablist"> 
	<div id="tab_profile" class="tabular1">
    	<div style="padding:15px;">
            <div>
            	<div id="static-text">
                    	会员简介&nbsp;&nbsp;<font style="color:#494949;"><?php echo $this->users->user_category ?>&nbsp;Profile</font>
                    </div>
                    
                    <div style="float:left; width:100%; height:auto;">
                    
                    <?php
					if(($this->users->user_category == 'Owner')||($this->users->user_category == 'Agent')){
					?>
                    <!--Left view-->
                    <div style="width:136px; float:left; padding-right:20px;">
                    	<div style="width:136px; height:auto; float:left;">
                            <img src="<?php echo $user_image; ?>" />
                        </div>
                        <div style="width:136px; height:auto; float:left; padding-top:10px;">
                            <img src="<?php echo $company_logo; ?>" />
                        </div>
                    </div>
                    <!--End Left view-->
                    
                    <!--Right view-->
                    <div style="float:left; width:460px; height:auto;">
                   
					<?php
					}
					?>
                   		<form action="index.php?option=com_user" method="post" name="form2" id="form2" class="form-validate" enctype="multipart/form-data">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" id="user-table">
                         <?php
							if($this->users->user_category == 'Agent'){
						 ?>
                        <tr>
                            <td width="120" height="40">
                                <label id="company_namemsg" for="company_name">
                                    <?php echo JText::_( 'Serving Organization' ); ?>
                                </label>
                            </td>
                            <td width="10">:</td>
                            <td width="100" height="40">
                                <input type="text" name="company_name" id="company_name" size="40" value="<?php echo $this->users->company_name;?>" class="inputbox" maxlength="50" />
                            </td>
                        </tr>
                        <tr>
                            <td width="120" height="40">
                                <label id="company_namemsg" for="company_name">
                                    <?php echo JText::_( 'CEA Reg. No.' ); ?>
                                </label>
                            </td>
                            <td width="10">:</td>
                            <td width="100" height="40">
                                <input type="text" name="cea_reg_no" id="cea_reg_no" size="40" value="<?php echo $this->users->cea_reg_no;?>" class="inputbox" maxlength="50" />
                            </td>
                        </tr>
						<?php
						} //agent
						?>
                        <tr>
                            <td width="120" height="40">
                                <label id="namemsg" for="name">
                                   <?php echo JText::_( 'Name' ); ?>
                                </label>
                            </td>
                            <td width="10">:</td>
                            <td width="120" height="40">
                                <input type="text" name="name" id="name" size="40" value="<?php echo $this->users->name;?>" class="inputbox" />
                            </td>
                        </tr>
                        
                        <?php 
						if(($this->users->user_category == 'Agent')||($this->users->user_category == 'Owner')){
						?>
                        <tr>
							<td width="120" height="40">
								<label id="chinese_namemsg" for="chinese_name">
									<?php echo JText::_( 'Chinese Name' ); ?>
								</label>
                            </td>
                            <td width="10">:</td>
                            <td width="120" height="40">
								<input type="text" name="chinese_name" id="chinese_name" size="40" value="<?php echo $this->users->chinese_name;?>" class="inputbox" maxlength="50" />
							</td>
						</tr>
						<tr>
							<td width="120" height="40">
								<label id="nricmsg" for="nric">
									<?php echo JText::_( 'NRIC' ); ?>
								</label>
                            </td>
                            <td width="10">:</td>
                            <td width="120" height="40">
								<input type="text" name="nric" id="nric" size="40" value="<?php echo $this->users->nric;?>" class="inputbox" maxlength="50" />
							</td>
						</tr>
						<?php
						}
						?>
                        <tr>
                            <td width="120" height="40">
                                    <label id="countrymsg" for="country">
                                        <?php echo JText::_( 'Country' ); ?>
                                    </label>
                            </td>
                            <td width="10">:</td>
                            <td width="120" height="40">
                                <?php echo JHTML::_('select.genericlist',  $this->countryOptions, 'country', 'class="inputbox" size="1"', 'value', 'text', $this->users->country );?>
                            </td>
                         </tr>
                         <?php 
							if(($this->users->user_category == 'Individual')||($this->users->user_category == 'Owner')||($this->users->user_category == 'Agent')){
						 ?>
                         <tr>
                            <td width="120" height="40">
                                <label id="mobile_contactmsg" for="mobile_contact">
                                Mobile Number
                                </label>   
                            </td>
                            <td width="10">:</td>
                            <td width="120" height="40">
                                <input type="text" name="mobile_contact" id="mobile_contact" size="40" value="<?php echo $this->users->mobile_contact;?>" class="inputbox" maxlength="50" />
                            </td>
                         </tr>
                         <?php
							}
						 ?>
                         
                        <?php 
						if($this->users->user_category == 'Merchant'){
						?>
						<tr>
							<td width="120" height="40">
								<label id="organization_namemsg" for="organization_name">
									<?php echo JText::_( 'Organization Name' ); ?>
								</label>
                            </td>
                            <td width="10">:</td>
                            <td width="120" height="40">
								<input class="inputbox" type="text" id="organization_name" name="organization_name" size="40" value="<?php echo $this->users->organization_name;?>" />
							</td>
                        </tr>
                        <tr>
							<td width="120" height="40">
								<label id="organization_addressmsg" for="organization_address">
									<?php echo JText::_( 'Organization Address' ); ?>
								</label>
                            </td>
                            <td width="10">:</td>
                            <td width="120" height="40">
								<input class="inputbox" type="text" id="organization_address" name="organization_address" size="40" value="<?php echo $this->users->organization_address;?>" />
							</td>
						</tr>
						<?php
						}
						?>
                        <?php 
						if(($this->users->user_category == 'Owner')||($this->users->user_category == 'Agent')){
						?>
						<tr>
							<td width="120" height="40">
								<label id="dobmsg" for="dob">
									<?php echo JText::_( 'Date of Birth' ); ?>
								</label>
                            </td>
                            <td width="10">:</td>
                            <td width="270" height="40">
								<?php echo $this->dayList;?>&nbsp;
								<?php echo $this->monthList;?>&nbsp;
								<?php echo $this->yearList;?>
							</td>
                        </tr>
                        <tr>
							<td width="120" height="40">
								<?php 
								if($this->users->user_category == 'Owner'){
								?>
								<label id="home_contactmsg" for="home_contact">
									<?php echo JText::_( 'Home Telephone' ); ?>
								</label>
                                <?php
								} else {
								?>
                                <label id="websitemsg" for="website">
									<?php echo JText::_( 'Personal Website' ); ?>
								</label>
                                <?php
								}
								?>
                            </td>
                            <td width="10">:</td>
                            <td width="120" height="40">
                            	<?php 
								if($this->users->user_category == 'Owner'){
								?>
								<input type="text" name="home_contact" id="home_contact" size="40" value="<?php echo $this->users->home_contact;?>" class="inputbox" maxlength="50" />
                                <?php
								} else {
								?>
                                <input class="inputbox" type="text" id="website" name="website" size="40" value="<?php echo $this->users->website?>" />
                                <?php
								}
								?>
                            </td>
						</tr>
						<?php
						}
						?>
                        <?php 
						if($this->users->user_category == 'Merchant'){
						?>
                        <tr>
                            <td width="120" height="40">
                            	<label id="phone_1msg" for="phone_1">
                                    Telephone 1
                                </label>
                            </td>
                            <td width="10">:</td>
                            <td width="120" height="40">
                                <input type="text" name="phone_1" id="phone_1" size="40" value="<?php echo $this->users->phone_1;?>" class="inputbox" maxlength="50" />
                            </td>
                        </tr>
						<tr>
                            <td width="120" height="40">
                                    <label id="phone_2msg" for="phone_2">
                                        <?php echo JText::_( 'Telephone 2' ); ?>
                                    </label>
                            </td>
                            <td width="10">:</td>
                            <td width="120" height="40">
								<input class="inputbox" type="text" id="phone_2" name="phone_2" size="40" value="<?php echo $this->users->phone_2;?>" />
							</td>
                        </tr>
                        <tr>
							<td width="120" height="40">
								<label id="faxmsg" for="fax">
									<?php echo JText::_( 'Fax' ); ?>
								</label>
                            </td>
                            <td width="10">:</td>
                            <td width="120" height="40">
								<input class="inputbox" type="text" id="fax" name="fax" size="40" value="<?php echo $this->users->fax;?>" />
							</td>
						</tr>
						<tr>
                            <td width="120" height="40">
                                    <label id="merchant_interestmsg" for="merchant_interest">
                                        <?php echo JText::_( 'Interest In' ); ?>
                                    </label>
                            </td>
                            <td width="10">:</td>
                            <td width="120" height="40">
                                    <?php echo JHTML::_('select.genericlist',  $this->merchantintOptions, 'merchant_interest', 'class="inputbox" size="1"', 'value', 'text', $this->users->merchant_interest);?>
                            </td>
						</tr>
						<?php
						}
						?>
                        <?php 
						if($this->users->user_category == 'Individual'){
						?>
						<tr>
                            <td width="120" height="40">
                                    <label id="residence_statusmsg">
                                        <?php echo JText::_( 'Residence Status' ); ?>
                                    </label>
                            </td>
                            <td width="10">:</td>
                            <td width="120" height="40">
								<?php echo JHTML::_('select.genericlist',  $this->residenceOptions, 'residence_status', 'class="inputbox" size="1"', 'value', 'text', $this->users->residence_status);?>
							</td>
                        </tr>
                        <tr>
                            <td width="120" height="40">
                                    <label id="user_interestmsg" for="user_interest">
                                        <?php echo JText::_( 'Interest In' ); ?>
                                    </label>
                            </td>
                            <td width="10">:</td>
                            <td width="120" height="40">
								<?php echo JHTML::_('select.genericlist',  $this->userintOptions, 'user_interest', 'class="inputbox" size="1"', 'value', 'text', $this->users->user_interest);?>
							</td>
						</tr>
						<?php
						}
						?>
                        <?php 
						if(($this->users->user_category == 'Owner')||($this->users->user_category == 'Agent')){
						?>
						<tr>
							<td width="120" height="40">
								<label id="home_addressmsg" for="home_address">
								Home Address
								</label>
                            </td>
                            <td width="10">:</td>
                            <td width="120" height="40">
								<input class="inputbox" type="text" id="home_address" name="home_address" size="40" value="<?php echo $this->users->home_address;?>" />
							</td>
                        </tr>
                        <tr>
							<td width="120" height="40">
								<label id="home_postalcodemsg" for="home_postalcode">
								Home Postal Code
								</label>
                            </td>
                            <td width="10">:</td>
                            <td width="120" height="40">
								<input class="inputbox" type="text" id="home_postalcode" name="home_postalcode" size="40" value="<?php echo $this->users->home_postalcode;?>" />
							</td>
						</tr>
						<?php 
						}
						?>
                        <?php
						if($this->users->user_category == 'Owner'){
						?>
						<tr>
                            <td width="120" height="40">
                                    <label id="rental_addressmsg" for="rental_address">
                                        <?php echo JText::_( 'Rental Address' ); ?>
                                    </label>
                            </td>
                            <td width="10">:</td>
                            <td width="120" height="40">
								<input class="inputbox" type="text" id="rental_address" name="rental_address" size="40" value="<?php echo $this->users->rental_address;?>" />
							</td>
                        </tr>
                        <tr>
                            <td width="120" height="40">
                                    <label id="rental_postalcodemsg" for="rental_postalcode">
                                        <?php echo JText::_( 'Rental Postal Code' ); ?>
                                    </label>
                            </td>
                            <td width="10">:</td>
                            <td width="120" height="40">
								<input class="inputbox" type="text" id="rental_postalcode" name="rental_postalcode" size="40" value="<?php echo $this->users->rental_postalcode;?>" />
							</td>
						</tr>
						<?php
						}
						?>
                        <?php
						if($this->users->user_category == 'Agent'){
						?>
						<tr>
                            <td width="120" height="40">
                                    <label id="short_descmsg" for="short_desc">
                                        <?php echo JText::_( 'Short Description' ); ?>
                                    </label>
                            </td>
                            <td width="10">:</td>
                            <td width="120" height="40">
                            	<textarea id="short_desc" name="short_desc" cols="30" rows="5"><?php echo $this->users->short_desc;?></textarea>
							</td>
                        </tr>
						<?php
						}
						?>
                        
                         
                         <tr>
                            <td width="120" height="40">
                                <label id="pwmsg" for="password">
                                    <?php echo JText::_( 'Password' ); ?>
                                </label>
                            </td>
                            <td width="10">:</td>
                            <td width="120" height="40">
                                <input class="inputbox" type="password" id="password" name="password" size="40" value="" />
                            </td>
                           </tr>
                           <tr>
                                <td width="120" height="40">
                                    <label id="pw2msg" for="password2">
                                        <?php echo JText::_( 'Verify Password' ); ?>
                                    </label>
                                </td>
                                <td width="10">:</td>
                                <td width="120" height="40">
                                    <input class="inputbox" type="password" id="password2" name="password2" size="40" value="" />
                                </td>
                            </tr>
                           
                        <?php 
						if(($this->users->user_category == 'Agent')||($this->users->user_category == 'Owner')){
						?>
                         <tr>
                            <td width="120" height="40">
                                <label id="user_imagemsg" for="user_image">
                                    <?php echo JText::_( 'Your Image' ); ?>
                                </label>
                            </td>
                            <td width="10">:</td>
                            <td width="100" style="padding-top:15px; height:2px;">
                            <?php
                                frontimageUpload($this->users->user_image,$this->users->user_image,'user_image');
                            ?>
                    
                        	</td>
						</tr>
                        <?php
						}
						if($this->users->user_category == 'Agent'){
						?>
                        <tr>
                            <td width="120" height="40">
                                <label id="company_logomsg" for="company_logo">
                                    <?php echo JText::_( 'Company Logo' ); ?>
                                </label>
                            </td>
                            <td width="10">:</td>
                            <td width="100" style="padding-top:15px; height:2px;">
                                <?php
                                    frontimageUpload($this->users->company_logo,$this->users->company_logo,'company_logo');
                                ?>
                            </td>
                        </tr>
                        <?php
						}
						?>
                        </table>
                        
                        
                    <?php	
                       if($this->users->user_category == 'Agent'){
					?>     
                    </div>
                    <!--End Right view-->
                    
                    
						
                    <div style="padding-top:20px; float:left; width:100%; height:auto;">
                            <div style="padding-left:10px; line-height:32px; font-weight:bold; background-color:#908373; width:615px; height:auto; color:#fff; font-size:14px;">专注于 Service Provide</div>
                            <div style="float:left; width:625px; height:auto; background-color:#f6f6f6;">
                            
                            <div style="float:left; padding-left:5px; padding-top:10px; width:620px; height:auto;">
                            <table cellspacing="3" cellpadding="3" width="100%">
                                <tr>
                                	<td width="80">
                                    	<div style="color:#334d7a; float:left; width:99%;">公寓</div>
                                        <div style="float:right; width:1%; color:#334d7a;">:</div>
                                    </td>
                                    <td width="80" align="left" valign="top">
                                        <div style="float:left;"><input type="checkbox" name="condo[]" id="condo[]" value="新公寓" <?php if(in_array('新公寓', $condo)) { echo "checked"; }?> /></div>
                                        <div style="float:left; line-height:20px; padding-left:5px;">新公寓</div>
                                    </td>
                                    <td width="80" align="left" valign="top">
                                        <div style="float:left;"><input type="checkbox" name="condo[]" id="condo[]" value="转售" <?php if(in_array('转售', $condo)) { echo "checked"; }?> /></div>
                                        <div style="float:left; line-height:20px; padding-left:5px;">转售</div>
                                    </td>
                                    <td width="80" align="left" valign="top" colspan="4">
                                        <div style="float:left;"><input type="checkbox" name="condo[]" id="condo[]" value="租赁" <?php if(in_array('租赁', $condo)) { echo "checked"; }?> /></div>
                                        <div style="float:left; line-height:20px; padding-left:5px;">租赁</div>
                                    </td>
                            	</tr>
                                
                                <tr>
                                	<td width="80">
                                    	<div style="color:#334d7a; float:left; width:99%;">&nbsp;</div>
                                        <div style="float:right; width:1%; color:#334d7a;">&nbsp;</div>
                                    </td>
                                    <td width="80" align="left" valign="top">
                                        <div style="color:#334d7a; float:left; padding-left:5px; width:60px;">类型</div>
                                        <div style="float:right; width:1%; color:#334d7a;">:</div>
                                    </td>
                                    <td width="90" align="left" valign="top">
                                        <div style="float:left;"><input type="checkbox" name="condo_type[]" id="condo_type[]" value="高档 (豪华)" <?php if(in_array('高档 (豪华)', $condo_type)) { echo "checked"; }?> /></div>
                                        <div style="float:left; line-height:20px; padding-left:5px;">高档 (豪华)</div>
                                    </td>
                                    <td width="90" align="left" valign="top">
                                        <div style="float:left;"><input type="checkbox" name="condo_type[]" id="condo_type[]" value="中档" <?php if(in_array('中档', $condo_type)) { echo "checked"; }?> /></div>
                                        <div style="float:left; line-height:20px; padding-left:5px;">中档</div>
                                    </td>
                                    <td width="60" align="left" valign="top" colspan="3">
                                        <div style="float:left;"><input type="checkbox" name="condo_type[]" id="condo_type[]" value="大众" <?php if(in_array('大众', $condo_type)) { echo "checked"; }?> /></div>
                                        <div style="float:left; line-height:20px; padding-left:5px;">大众</div>
                                    </td>
                            	</tr>
                            </table>
                            </div>
                            
                            <div style="border-bottom:solid 1px #dadada;">&nbsp;</div>
                            
                            <div style="float:left; padding-left:5px; padding-top:10px; width:620px; height:auto;">
                            <table cellspacing="3" cellpadding="3" width="100%">
                            	<tr>
                                	<td width="80">
                                    	<div style="color:#334d7a; float:left; width:99%;">HDB</div>
                                        <div style="float:right; width:1%; color:#334d7a;">:</div>
                                    </td>
                                    <td width="80" align="left" valign="top">
                                        <div style="float:left;"><input type="checkbox" name="hdb[]" id="hdb[]" value="转售" <?php if(in_array('转售', $hdb)) { echo "checked"; }?> /></div>
                                        <div style="float:left; line-height:20px; padding-left:5px;">转售</div>
                                    </td>
                                    <td width="80" align="left" valign="top">
                                        <div style="float:left;"><input type="checkbox" name="hdb[]" id="hdb[]" value="租赁" <?php if(in_array('租赁', $hdb)) { echo "checked"; }?> /></div>
                                        <div style="float:left; line-height:20px; padding-left:5px;">租赁</div>
                                    </td>
                                    <td width="168" align="left" valign="top" colspan="4">&nbsp;</td>

                            	</tr>
                                <tr>
                                	<td colspan="7">
                                    	<div style="color:#334d7a; float:left; width:79px;">HDB 区</div>
                                        <div style="float:left; width:1%; color:#334d7a;">:</div>
                                	</td>
                                </tr>
                                </table>
                                </div>
                            <div style="float:left; padding-left:5px; width:625px; height:auto;">
                               <div style="float:left; width:150px; height:45px;">
                               		<div style="float:left; width:20px; height:45px;"><input type="checkbox" name="hdb_type[]" id="hdb_type[]" value="1" <?php if(in_array('1', $hdb_type)) { echo "checked"; }?> />
                                    </div>
                                    <div style="float:left; line-height:20px; width:125px;">
                                        <font style="color:#908373; font-weight:bold;">1.</font> Ang Mo Kio 宏茂桥
                                    </div>	
                               </div>
                               
                               <div style="float:left; width:150px; height:45px;">
                               		<div style="float:left; width:20px; height:45px;"><input type="checkbox" name="hdb_type[]" id="hdb_type[]" value="2" <?php if(in_array('2', $hdb_type)) { echo "checked"; }?> />
                                    </div>
                                    <div style="float:left; line-height:20px; width:125px;">
                                        <font style="color:#908373; font-weight:bold;">2.</font> Bedok 勿洛
                                    </div>	
                               </div>
                               
                               <div style="float:left; width:150px; height:45px;">
                               		<div style="float:left; width:20px; height:45px;"><input type="checkbox" name="hdb_type[]" id="hdb_type[]" value="3" <?php if(in_array('3', $hdb_type)) { echo "checked"; }?> />
                                    </div>
                                    <div style="float:left; line-height:20px; width:125px;">
                                        <font style="color:#908373; font-weight:bold;">3.</font> Bishan 碧山
                                    </div>	
                               </div>
                               
                               <div style="float:left; width:150px; height:45px;">
                               		<div style="float:left; width:20px; height:45px;"><input type="checkbox" name="hdb_type[]" id="hdb_type[]" value="4" <?php if(in_array('4', $hdb_type)) { echo "checked"; }?> />
                                    </div>
                                    <div style="float:left; line-height:20px; width:125px;">
                                        <font style="color:#908373; font-weight:bold;">4.</font> Bukit Batok 武吉巴督
                                    </div>	
                               </div>
                               
                               
                               <div style="float:left; width:150px; height:45px;">
                               		<div style="float:left; width:20px; height:45px;"><input type="checkbox" name="hdb_type[]" id="hdb_type[]" value="5" <?php if(in_array('5', $hdb_type)) { echo "checked"; }?> />
                                    </div>
                                    <div style="float:left; line-height:20px; width:125px;">
                                        <font style="color:#908373; font-weight:bold;">5.</font> Bukit Merah 红山
                                    </div>	
                               </div>
                               
                               <div style="float:left; width:150px; height:45px;">
                               		<div style="float:left; width:20px; height:45px;"><input type="checkbox" name="hdb_type[]" id="hdb_type[]" value="6" <?php if(in_array('6', $hdb_type)) { echo "checked"; }?> />
                                    </div>
                                    <div style="float:left; line-height:20px; width:125px;">
                                        <font style="color:#908373; font-weight:bold;">6.</font> Bukit Panjang<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;武吉班让
                                    </div>	
                               </div>
                               
                               <div style="float:left; width:150px; height:45px;">
                               		<div style="float:left; width:20px; height:45px;"><input type="checkbox" name="hdb_type[]" id="hdb_type[]" value="7" <?php if(in_array('7', $hdb_type)) { echo "checked"; }?> />
                                    </div>
                                    <div style="float:left; line-height:20px; width:125px;">
                                        <font style="color:#908373; font-weight:bold;">7.</font> Bukit Timah<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;武吉知马
                                    </div>	
                               </div>
                               
                               <div style="float:left; width:150px; height:45px;">
                               		<div style="float:left; width:20px; height:45px;"><input type="checkbox" name="hdb_type[]" id="hdb_type[]" value="8" <?php if(in_array('8', $hdb_type)) { echo "checked"; }?> />
                                    </div>
                                    <div style="float:left; line-height:20px; width:125px;">
                                        <font style="color:#908373; font-weight:bold;">8.</font> Central Area 中央
                                    </div>	
                               </div>
                               
                               
                               <div style="float:left; width:150px; height:45px;">
                               		<div style="float:left; width:20px; height:45px;"><input type="checkbox" name="hdb_type[]" id="hdb_type[]" value="9" <?php if(in_array('9', $hdb_type)) { echo "checked"; }?> />
                                    </div>
                                    <div style="float:left; line-height:20px; width:125px;">
                                        <font style="color:#908373; font-weight:bold;">9.</font> Choa Chu Kang<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;蔡厝港
                                    </div>	
                               </div>
                               
                               <div style="float:left; width:150px; height:45px;">
                               		<div style="float:left; width:20px; height:45px;"><input type="checkbox" name="hdb_type[]" id="hdb_type[]" value="10" <?php if(in_array('10', $hdb_type)) { echo "checked"; }?> />
                                    </div>
                                    <div style="float:left; line-height:20px; width:125px;">
                                        <font style="color:#908373; font-weight:bold;">10.</font> Clementi 金文泰
                                    </div>	
                               </div>
                               
                               <div style="float:left; width:150px; height:45px;">
                               		<div style="float:left; width:20px; height:45px;"><input type="checkbox" name="hdb_type[]" id="hdb_type[]" value="11" <?php if(in_array('11', $hdb_type)) { echo "checked"; }?> />
                                    </div>
                                    <div style="float:left; line-height:20px; width:125px;">
                                        <font style="color:#908373; font-weight:bold;">11.</font> Geylang 芽笼
                                    </div>	
                               </div>
                               
                               <div style="float:left; width:150px; height:45px;">
                               		<div style="float:left; width:20px; height:45px;"><input type="checkbox" name="hdb_type[]" id="hdb_type[]" value="12" <?php if(in_array('12', $hdb_type)) { echo "checked"; }?> />
                                    </div>
                                    <div style="float:left; line-height:20px; width:125px;">
                                        <font style="color:#908373; font-weight:bold;">12.</font> Hougang 后港
                                    </div>	
                               </div>
                               
                               <div style="float:left; width:150px; height:45px;">
                               		<div style="float:left; width:20px; height:45px;"><input type="checkbox" name="hdb_type[]" id="hdb_type[]" value="13" <?php if(in_array('13', $hdb_type)) { echo "checked"; }?> />
                                    </div>
                                    <div style="float:left; line-height:20px; width:125px;">
                                        <font style="color:#908373; font-weight:bold;">13.</font> Jurong East 裕廊东
                                    </div>	
                               </div>
                               
                               <div style="float:left; width:150px; height:45px;">
                               		<div style="float:left; width:20px; height:45px;"><input type="checkbox" name="hdb_type[]" id="hdb_type[]" value="14" <?php if(in_array('14', $hdb_type)) { echo "checked"; }?> />
                                    </div>
                                    <div style="float:left; line-height:20px; width:125px;">
                                        <font style="color:#908373; font-weight:bold;">14.</font> Jurong West<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;裕廊西
                                    </div>	
                               </div>
                               
                               <div style="float:left; width:150px; height:45px;">
                               		<div style="float:left; width:20px; height:45px;"><input type="checkbox" name="hdb_type[]" id="hdb_type[]" value="15" <?php if(in_array('15', $hdb_type)) { echo "checked"; }?> />
                                    </div>
                                    <div style="float:left; line-height:20px; width:125px;">
                                        <font style="color:#908373; font-weight:bold;">15.</font> Kallang/Whampoa<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;加冷/黄埔
                                    </div>	
                               </div>
                               
                               <div style="float:left; width:150px; height:45px;">
                               		<div style="float:left; width:20px; height:45px;"><input type="checkbox" name="hdb_type[]" id="hdb_type[]" value="16" <?php if(in_array('16', $hdb_type)) { echo "checked"; }?> />
                                    </div>
                                    <div style="float:left; line-height:20px; width:125px;">
                                        <font style="color:#908373; font-weight:bold;">16.</font> Marine Parade<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;马林百列
                                    </div>	
                               </div>
                               
                               <div style="float:left; width:150px; height:45px;">
                               		<div style="float:left; width:20px; height:45px;"><input type="checkbox" name="hdb_type[]" id="hdb_type[]" value="17" <?php if(in_array('17', $hdb_type)) { echo "checked"; }?> />
                                    </div>
                                    <div style="float:left; line-height:20px; width:125px;">
                                        <font style="color:#908373; font-weight:bold;">17.</font> Pasir Ris 巴西立
                                    </div>	
                               </div>
                               
                               <div style="float:left; width:150px; height:45px;">
                               		<div style="float:left; width:20px; height:45px;"><input type="checkbox" name="hdb_type[]" id="hdb_type[]" value="18" <?php if(in_array('18', $hdb_type)) { echo "checked"; }?> />
                                    </div>
                                    <div style="float:left; line-height:20px; width:125px;">
                                        <font style="color:#908373; font-weight:bold;">18.</font> Punggol 榜鹅
                                    </div>	
                               </div>
                               
                               <div style="float:left; width:150px; height:45px;">
                               		<div style="float:left; width:20px; height:45px;"><input type="checkbox" name="hdb_type[]" id="hdb_type[]" value="19" <?php if(in_array('19', $hdb_type)) { echo "checked"; }?> />
                                    </div>
                                    <div style="float:left; line-height:20px; width:125px;">
                                        <font style="color:#908373; font-weight:bold;">19.</font> Queenstown<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;女皇镇
                                    </div>	
                               </div>
                               
                               <div style="float:left; width:150px; height:45px;">
                               		<div style="float:left; width:20px; height:45px;"><input type="checkbox" name="hdb_type[]" id="hdb_type[]" value="20" <?php if(in_array('20', $hdb_type)) { echo "checked"; }?> />
                                    </div>
                                    <div style="float:left; line-height:20px; width:125px;">
                                        <font style="color:#908373; font-weight:bold;">20.</font> Sembawang<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;三巴旺
                                    </div>	
                               </div>
                               
                               <div style="float:left; width:150px; height:45px;">
                               		<div style="float:left; width:20px; height:45px;"><input type="checkbox" name="hdb_type[]" id="hdb_type[]" value="21" <?php if(in_array('21', $hdb_type)) { echo "checked"; }?> />
                                    </div>
                                    <div style="float:left; line-height:20px; width:125px;">
                                        <font style="color:#908373; font-weight:bold;">21.</font> Sengkang 盛港
                                    </div>	
                               </div>
                               
                               <div style="float:left; width:150px; height:45px;">
                               		<div style="float:left; width:20px; height:45px;"><input type="checkbox" name="hdb_type[]" id="hdb_type[]" value="22" <?php if(in_array('22', $hdb_type)) { echo "checked"; }?> />
                                    </div>
                                    <div style="float:left; line-height:20px; width:125px;">
                                        <font style="color:#908373; font-weight:bold;">22.</font> Serangoon 实龙岗
                                    </div>	
                               </div>
                               
                               <div style="float:left; width:150px; height:45px;">
                               		<div style="float:left; width:20px; height:45px;"><input type="checkbox" name="hdb_type[]" id="hdb_type[]" value="23" <?php if(in_array('23', $hdb_type)) { echo "checked"; }?> />
                                    </div>
                                    <div style="float:left; line-height:20px; width:125px;">
                                        <font style="color:#908373; font-weight:bold;">23.</font> Tampanies 淡滨尼
                                    </div>	
                               </div>
                               
                               <div style="float:left; width:150px; height:45px;">
                               		<div style="float:left; width:20px; height:45px;"><input type="checkbox" name="hdb_type[]" id="hdb_type[]" value="24" <?php if(in_array('24', $hdb_type)) { echo "checked"; }?> />
                                    </div>
                                    <div style="float:left; line-height:20px; width:125px;">
                                        <font style="color:#908373; font-weight:bold;">24.</font> Toa Payoh 大巴窑
                                    </div>	
                               </div>
                               
                               <div style="float:left; width:150px; height:45px;">
                               		<div style="float:left; width:20px; height:45px;"><input type="checkbox" name="hdb_type[]" id="hdb_type[]" value="25" <?php if(in_array('25', $hdb_type)) { echo "checked"; }?> />
                                    </div>
                                    <div style="float:left; line-height:20px; width:125px;">
                                        <font style="color:#908373; font-weight:bold;">25.</font> Woodlands 兀兰
                                    </div>	
                               </div>
                               
                               <div style="float:left; width:150px; height:45px;">
                               		<div style="float:left; width:20px; height:45px;"><input type="checkbox" name="hdb_type[]" id="hdb_type[]" value="26" <?php if(in_array('26', $hdb_type)) { echo "checked"; }?> />
                                    </div>
                                    <div style="float:left; line-height:20px; width:125px;">
                                        <font style="color:#908373; font-weight:bold;">26.</font> Yishun 义顺
                                    </div>	
                               </div>
                               
                               
                            </div>
                            
                            <div style="border-bottom:solid 1px #dadada;">&nbsp;</div>
                            
                            <div style="float:left; padding-left:5px; padding-top:10px; width:620px; height:auto;">
                            <table cellspacing="3" cellpadding="3" width="100%">
                            	<tr>
                                	<td width="80">
                                    	<div style="color:#334d7a; float:left; width:99%;">有地住宅</div>
                                        <div style="float:right; width:1%; color:#334d7a;">:</div>
                                    </td>
                                    <td width="80" align="left" valign="top">
                                        <div style="float:left;"><input type="checkbox" name="private_estate[]" id="private_estate" value="买卖" <?php if(in_array('买卖', $private_estate)) { echo "checked"; }?> /></div>
                                        <div style="float:left; line-height:20px; padding-left:5px;">买卖</div>
                                    </td>
                                    <td width="80" align="left" valign="top">
                                        <div style="float:left;"><input type="checkbox" name="private_estate[]" id="private_estate" value="租赁" <?php if(in_array('租赁', $private_estate)) { echo "checked"; }?> /></div>
                                        <div style="float:left; line-height:20px; padding-left:5px;">租赁</div>
                                    </td>
                                    <td width="168" align="left" valign="top" colspan="4">&nbsp;</td>
                            	</tr>
                            </table>
                            </div>
                            
                            <div style="border-bottom:solid 1px #dadada;">&nbsp;</div>
                            
                            <div style="float:left; padding-left:5px; padding-top:10px; padding-bottom:10px; width:620px; height:auto;">
                            <table cellspacing="3" cellpadding="3" width="100%">
                            	<tr>
                                	<td width="80">
                                    	<div style="color:#334d7a; float:left; width:99%;">商业房产</div>
                                        <div style="float:right; width:1%; color:#334d7a;">:</div>
                                    </td>
                                    <td width="80" align="left" valign="top">
                                        <div style="float:left;"><input type="checkbox" name="industrial_estate[]" id="industrial_estate[]" value="买卖" <?php if(in_array('买卖', $industrial_estate)) { echo "checked"; }?> />
                                        </div>
                                        <div style="float:left; line-height:20px; padding-left:5px;">买卖</div>
                                    </td>
                                    <td width="80" align="left" valign="top">
                                        <div style="float:left;"><input type="checkbox" name="industrial_estate[]" id="industrial_estate[]" value="租赁" <?php if(in_array('租赁', $industrial_estate)) { echo "checked"; }?> />
                                        </div>
                                        <div style="float:left; line-height:20px; padding-left:5px;">租赁</div>
                                    </td>
                                    <td width="168" align="left" valign="top" colspan="4">&nbsp;</td>
                            	</tr>
                            </table>
                            </div>
                            
                            
                            </div>
                    </div>      
                    <?php
					}
					?>
                    <div id="button_div" style="float:left; padding-bottom:20px; padding-top:20px;">
                            <button class="contactbutton validate" type="submit"><?php echo JText::_('Update'); ?></button>
                            <input type="button" value="Cancel" name="cancel" id="cancel" class="contactbutton" onClick="history.go(-1);" />
                            <input type="hidden" name="option" value="com_user" />
                            <input type="hidden" name="Itemid" value="126" />
                            <input type="hidden" name="task" value="profile_update" />
                            <input type="hidden" name="id" value="0" />
                            <input type="hidden" name="gid" value="0" />
                            <?php echo JHTML::_( 'form.token' ); ?>
                    </div>
                    </form>       
                            
                            
                            
                    
                    </div>
             
            </div>
        </div>
    </div><!--tabular1-->

</div>    

</div><!--End-->



</div>
<!--End View-->
