<?php
/**
 * @version		$Id: view.html.php 11646 2009-03-01 19:34:56Z ian $
 * @package		Joomla
 * @subpackage	Edit Profile
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');
require_once(JPATH_ROOT.DS.'includes'.DS.'tools.php');

/**
 * HTML View class for the Form component
 *
 * @static
 * @package		Joomla
 * @subpackage	Edit Profile
 * @since 1.0
 */
class UserViewEdit_profile extends JView
{
		/**
	 * Registry namespace prefix
	 *
	 * @var	string
	 */
	var $_namespace	= 'com_user.reset.';

	/**
	 * Display function
	 *
	 * @since 1.5
	 */
	function display($tpl = null)
	{
		jimport('joomla.html.html');

		global $mainframe;

		// Load the form validation behavior
		JHTML::_('behavior.formvalidation');
		
		// Add the tooltip behavior
		JHTML::_('behavior.tooltip');

		// Get the layout
		$layout	= $this->getLayout();
		
		$user 	=& JFactory::getUser();
		$user_id = $user->get('id');
		
		$saluteOptions[] = JHTML::_('select.option', '', JText::_('Please Select'));
		$saluteOptions[] = JHTML::_('select.option', 'Mr', JText::_('Mr'));
		$saluteOptions[] = JHTML::_('select.option', 'Mrs', JText::_('Mrs'));
		$saluteOptions[] = JHTML::_('select.option', 'Mdm', JText::_('Mdm'));
		$saluteOptions[] = JHTML::_('select.option', 'Miss', JText::_('Miss'));
		$saluteOptions[] = JHTML::_('select.option', 'Dr', JText::_('Dr'));
		
		$residenceOptions[] = JHTML::_('select.option', '', JText::_('Please Select'));
		$residenceOptions[] = JHTML::_('select.option', 'Singapore Citizen', JText::_('Singapore Citizen'));
		$residenceOptions[] = JHTML::_('select.option', 'Singapore PR', JText::_('Singapore PR'));
		$residenceOptions[] = JHTML::_('select.option', 'Others', JText::_('Others'));
		
		$userintOptions[] = JHTML::_('select.option', '', JText::_('Please Select'));
		$userintOptions[] = JHTML::_('select.option', 'Rent', JText::_('Rent Property'));
		$userintOptions[] = JHTML::_('select.option', 'Buy', JText::_('Buy Property'));
		$userintOptions[] = JHTML::_('select.option', 'None', JText::_('None'));
		
		$merchantintOptions[] = JHTML::_('select.option', '', JText::_('Please Select'));
		$merchantintOptions[] = JHTML::_('select.option', 'Place Ads', JText::_('Place Ads'));
		$merchantintOptions[] = JHTML::_('select.option', 'EDM Services', JText::_('EDM Services'));
		$merchantintOptions[] = JHTML::_('select.option', 'Receive Free Events Invitation', JText::_('Receive Free Events Invitation'));
		
		$country_list = array(

		"Afghanistan",
	
		"Albania",
	
		"Algeria",
	
		"Andorra",
	
		"Angola",
	
		"Antigua and Barbuda",
	
		"Argentina",
	
		"Armenia",
	
		"Australia",
	
		"Austria",
	
		"Azerbaijan",
	
		"Bahamas",
	
		"Bahrain",
	
		"Bangladesh",
	
		"Barbados",
	
		"Belarus",
	
		"Belgium",
	
		"Belize",
	
		"Benin",
	
		"Bhutan",
	
		"Bolivia",
	
		"Bosnia and Herzegovina",
	
		"Botswana",
	
		"Brazil",
	
		"Brunei",
	
		"Bulgaria",
	
		"Burkina Faso",
	
		"Burundi",
	
		"Cambodia",
	
		"Cameroon",
	
		"Canada",
	
		"Cape Verde",
	
		"Central African Republic",
	
		"Chad",
	
		"Chile",
	
		"China",
	
		"Colombi",
	
		"Comoros",
	
		"Congo (Brazzaville)",
	
		"Congo",
	
		"Costa Rica",
	
		"Cote d��Ivoire",
	
		"Croatia",
	
		"Cuba",
	
		"Cyprus",
	
		"Czech Republic",
	
		"Denmark",
	
		"Djibouti",
	
		"Dominica",
	
		"Dominican Republic",
	
		"East Timor (Timor Timur)",
	
		"Ecuador",
	
		"Egypt",
	
		"El Salvador",
	
		"Equatorial Guinea",
	
		"Eritrea",
	
		"Estonia",
	
		"Ethiopia",
	
		"Fiji",
	
		"Finland",
	
		"France",
	
		"Gabon",
	
		"Gambia, The",
	
		"Georgia",
	
		"Germany",
	
		"Ghana",
	
		"Greece",
	
		"Grenada",
	
		"Guatemala",
	
		"Guinea",
	
		"Guinea-Bissau",
	
		"Guyana",
	
		"Haiti",
	
		"Honduras",
	
		"Hungary",
	
		"Iceland",
	
		"India",
	
		"Indonesia",
	
		"Iran",
	
		"Iraq",
	
		"Ireland",
	
		"Israel",
	
		"Italy",
	
		"Jamaica",
	
		"Japan",
	
		"Jordan",
	
		"Kazakhstan",
	
		"Kenya",
	
		"Kiribati",
	
		"Korea, North",
	
		"Korea, South",
	
		"Kuwait",
	
		"Kyrgyzstan",
	
		"Laos",
	
		"Latvia",
	
		"Lebanon",
	
		"Lesotho",
	
		"Liberia",
	
		"Libya",
	
		"Liechtenstein",
	
		"Lithuania",
	
		"Luxembourg",
	
		"Macedonia",
	
		"Madagascar",
	
		"Malawi",
	
		"Malaysia",
	
		"Maldives",
	
		"Mali",
	
		"Malta",
	
		"Marshall Islands",
	
		"Mauritania",
	
		"Mauritius",
	
		"Mexico",
	
		"Micronesia",
	
		"Moldova",
	
		"Monaco",
	
		"Mongolia",
	
		"Morocco",
	
		"Mozambique",
	
		"Myanmar",
	
		"Namibia",
	
		"Nauru",
	
		"Nepa",
	
		"Netherlands",
	
		"New Zealand",
	
		"Nicaragua",
	
		"Niger",
	
		"Nigeria",
	
		"Norway",
	
		"Oman",
	
		"Pakistan",
	
		"Palau",
	
		"Panama",
	
		"Papua New Guinea",
	
		"Paraguay",
	
		"Peru",
	
		"Philippines",
	
		"Poland",
	
		"Portugal",
	
		"Qatar",
	
		"Romania",
	
		"Russia",
	
		"Rwanda",
	
		"Saint Kitts and Nevis",
	
		"Saint Lucia",
	
		"Saint Vincent",
	
		"Samoa",
	
		"San Marino",
	
		"Sao Tome and Principe",
	
		"Saudi Arabia",
	
		"Senegal",
	
		"Serbia and Montenegro",
	
		"Seychelles",
	
		"Sierra Leone",
	
		"Singapore",
	
		"Slovakia",
	
		"Slovenia",
	
		"Solomon Islands",
	
		"Somalia",
	
		"South Africa",
	
		"Spain",
	
		"Sri Lanka",
	
		"Sudan",
	
		"Suriname",
	
		"Swaziland",
	
		"Sweden",
	
		"Switzerland",
	
		"Syria",
	
		"Taiwan",
	
		"Tajikistan",
	
		"Tanzania",
	
		"Thailand",
	
		"Togo",
	
		"Tonga",
	
		"Trinidad and Tobago",
	
		"Tunisia",
	
		"Turkey",
	
		"Turkmenistan",
	
		"Tuvalu",
	
		"Uganda",
	
		"Ukraine",
	
		"United Arab Emirates",
	
		"United Kingdom",
	
		"United States",
	
		"Uruguay",
	
		"Uzbekistan",
	
		"Vanuatu",
	
		"Vatican City",
	
		"Venezuela",
	
		"Vietnam",
	
		"Yemen",
	
		"Zambia",
	
		"Zimbabwe"
	
		);
	
		$countryOptions[] = JHTML::_('select.option', '', JText::_('Please Select'));
		foreach ($country_list as $country) {
		$countryOptions[] = JHTML::_('select.option', $country,$country);
		}
		
		$db = & JFactory::getDBO();
		$query = "SELECT dob FROM #__users WHERE id = '".$user_id."'";
		$db->setQuery($query);
		$dob = $db->loadResult();
		$birthday = explode("-", $dob);
		
		// building day list
		$daysList[] = JHTML::_('select.option', 'dd','day');
		for ( $i = 1; $i <= 31; $i++ )
		{
		$daysList[] = JHTML::_('select.option', $i,$i);
		}
		$dayList = JHTML::_('select.genericlist',  $daysList, 'day', 'class="dinputbox" size="1"', 'value', 'text', $birthday[2] );

		
		// building month list
		$monthsList[] = JHTML::_('select.option', 'mm','month');
		$monthsList[] = JHTML::_('select.option', '1', JText::_('January'));
		$monthsList[] = JHTML::_('select.option', '2', JText::_('February'));
		$monthsList[] = JHTML::_('select.option', '3', JText::_('March'));
		$monthsList[] = JHTML::_('select.option', '4', JText::_('April'));
		$monthsList[] = JHTML::_('select.option', '5', JText::_('May'));
		$monthsList[] = JHTML::_('select.option', '6', JText::_('June'));
		$monthsList[] = JHTML::_('select.option', '7', JText::_('July'));
		$monthsList[] = JHTML::_('select.option', '8', JText::_('August'));
		$monthsList[] = JHTML::_('select.option', '9', JText::_('September'));
		$monthsList[] = JHTML::_('select.option', '10', JText::_('October'));
		$monthsList[] = JHTML::_('select.option', '11', JText::_('November'));
		$monthsList[] = JHTML::_('select.option', '12', JText::_('December'));
		$monthList = JHTML::_('select.genericlist',  $monthsList, 'month', 'class="minputbox" size="1"', 'value', 'text', $birthday[1] );

		
		// building year list
		$yearsList[] = JHTML::_('select.option', 'yyyy','year');
		$y = date ( "Y", extcal_get_local_time ( ) );
		
		for ( $i = 1; $i <= 70; $i++ )
		{
		$yearsList[] = JHTML::_('select.option', $y,$y);
		
			$y--;
		}
		$yearList = JHTML::_('select.genericlist',  $yearsList, 'year', 'class="yinputbox" size="1"', 'value', 'text', $birthday[0] );



		$this->assignRef('params',		$params);
		$this->assignRef('saluteOptions', $saluteOptions );
		$this->assignRef('residenceOptions', $residenceOptions );
		$this->assignRef('userintOptions', $userintOptions );
		$this->assignRef('merchantintOptions', $merchantintOptions );
		$this->assignRef('countryOptions', $countryOptions );
		$this->assignRef('dayList', $dayList );
		$this->assignRef('monthList', $monthList );
		$this->assignRef('yearList', $yearList );

		$this->assignRef('items', $this->getAdsOfThisUser($user_id));
		$this->assignRef('users', $this->getInfoOfThisUser($user_id));
		$this->assignRef('sareas', $this->getSAOfThisUser($user_id));
		//$this->assignRef('params',		$params);

		parent::display($tpl);
	}
	
	function getInfoOfThisUser($id = 0) {
		
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT * FROM #__users "
						.	" WHERE id = '$id'" ;
						
			$db->setQuery( $query );
			$list = $db->loadObject();		
			//echo print_r($list);
		return $list;	
	}
	
	function getSAOfThisUser($id = 0) {
		
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT * FROM #__users_speciality_areas "
						.	" WHERE user_id = '$id'" ;
						
			$db->setQuery( $query );
			$list = $db->loadObject();		
			//echo print_r($list);
		return $list;	
	}
	
	function getAdsOfThisUser($id = 0) {
		if(!$id)
			return false;
		
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT a.ad_title, a.ad_type, p.name_en, p.name_ch, p.address, a.ask_price, a.posting_date, a.property_type , i.name,a.id AS ads_id FROM #__ihouse_ads AS a "
						.	" LEFT JOIN #__ihouse_property AS p ON p.postcode = a.postcode "
						.	" LEFT JOIN #__ihouse_ads_image AS i ON i.ads_id = a.id "
						.	" WHERE a.posted_by = '$id' ";
						
			$db->setQuery( $query );
			$lists = $db->loadObjectList();
			
		return $lists;
	}
}
