<?php defined('_JEXEC') or die('Restricted access'); ?>
<?php JHTML::_('script', 'ihouse.js', 'components/com_ads/rokbox/'); ?>
<style type="text/css">
#tablist7 {

	width:99.1%;
	padding:0;
	margin:0;
	height:auto;
	float:left;

}

#tablist1 {
	/*border-left:3px solid #FF8200;
	border-bottom:3px solid #FF8200;
	border-right:3px solid #FF8200;*/
	width:99.1%;
	padding:10px;
	margin:0;
	height:auto;
	float:left;
}

#tablist2 {
	width:99.1%;
	padding:0;
	margin:0;
	min-height:300px;
	height:auto;
	float:left;
}

#tablist3 {
	width:99.1%;
	padding:0;
	margin:0;
	min-height:300px;
	height:auto;
	float:left;
}

#tablist6 {
	width:99.1%;
	padding:0;
	margin:0;
	min-height:300px;
	height:auto;
	float:left;
}

table#prop_det_info td{
	border:1px solid #999;
}

ul.tab_nav88 {
list-style-image:none;
list-style-type:none;
}

</style>
<div id="reg_border">
			<div id="reg_header">
			会员注册 Member Registration
            </div>
</div>

<div style="padding:15px;"><!--start-->

<div id="menu_list_user_register">
<ul id="" class="tab_nav88">     
	<li id="tab_li88_individual" class="tabactive88"><a href="javascript:userRegisterInfoTab('individual')"><span class="chinese_text_reg"><br />普通用户注册<br /><font class="english_text_reg">Individual</font></span></a></li>
	<li id="tab_li88_agent"><a href="javascript:userRegisterInfoTab('agent')"><span class="chinese_text_reg"><br />中介注册<br /><font class="english_text_reg">Agent</font></span></a></li>
    <li id="tab_li88_owner" ><a href="javascript:userRegisterInfoTab('owner')"><span class="chinese_text_reg"><br />屋主注册<br /><font class="english_text_reg">Owner</font></span></a></li>
    <li id="tab_li88_merchant" ><a href="javascript:userRegisterInfoTab('merchant')"><span class="chinese_text_reg"><br />商家注册<br /><font class="english_text_reg">Merchant</font></span></a></li>
</ul>   
</div>

<?php //echo $this->tid;?>
<?php
if($this->tid == '2'){
?>
<script type="text/javascript">
window.addEvent('domready',function() {	
$('tab_li88_agent').addClass('tabactive88');
$('tab_li88_owner').className = "";
$('tab_li88_merchant').className = "";
$('tab_li88_individual').className = "";

userRegisterInfoTab('agent');
});
</script>
<?php
}
if($this->tid == '3'){
?>
<script type="text/javascript">
window.addEvent('domready',function() {	
$('tab_li88_owner').addClass('tabactive88');
$('tab_li88_agent').className = "";
$('tab_li88_merchant').className = "";
$('tab_li88_individual').className = "";

userRegisterInfoTab('owner');
});
</script>
<?php
}
if($this->tid == '4'){
?>
<script type="text/javascript">
window.addEvent('domready',function() {	
$('tab_li88_owner').className = "";
$('tab_li88_agent').className = "";
$('tab_li88_merchant').addClass('tabactive88');
$('tab_li88_individual').className = "";

userRegisterInfoTab('merchant');
});
</script>
<?php
}
if($this->tid == '1'){
?>
<script type="text/javascript">
window.addEvent('domready',function() {	
$('tab_li88_owner').className = "";
$('tab_li88_agent').className = "";
$('tab_li88_merchant').className = "";
$('tab_li88_individual').addClass('tabactive88');

userRegisterInfoTab('individual');
});
</script>
<?php
}
?>
   <form action="index.php?option=com_user" method="post" name="form2" id="form2">
<div id="tablist7"> 
    <div id="tab_individual" class="tabular1">
 
        <div style="padding:10px;">
        	
            <div id="menu_list_user_register">
<ul id="" class="tab_nav999">     
	<li id="tab_li999_chinese" class="tabactive999"><a href="javascript:languageiTab('chinese')">中文</a></li>
	<li id="tab_li999_english"><a href="javascript:languageiTab('english')">English</a></li>
    		</ul>
            </div>
     
            <div id="tablist1"> 
            <div id="tab_chinese" class="tabular4">
            
        	<div style="float:left; width:650px; padding-top:20px;">
           当您注册成为iHOUSE360的尊贵会员，您将享有以下<font style="color:#FF0000">免费服务。</font>并且我们还会不断地推陈出新，提供更好更有价值的服务给您。<br /><br />
            </div>
            <div style="padding-bottom:5px;"><u>注册用户权利</u></div>
            <div>
            (1) 通过iHOUSE360平台，发信息给登广告的经纪；<br />
            (2) 接收免费市场简报；<br />
            (3) 给您熟悉的公寓评分和写评论，与大众分享您的经验；<br />
            (4) 收藏您感兴趣的公寓，方便下次再访问；<br />
            (5) 接收新登场的公寓信息、信息发布会或相关活动通知；<br />
            (6) 联系iHOUSE360获取直接或更多的服务。 
            </div>
            
             <div style="padding-top:10px; padding-bottom:20px; float:left; width:620px;">
        	<div style="float:right;">
            	<input type="submit" value="继续" class="contactbutton" name="individual_form" id="individual_form1" onClick="javascript:individual1(this);" />
            </div>
        </div>
            
            </div><!--tabular4-->
            
            <div id="tab_english" class="tabular4" style="display:none;">
            
        	<div style="float:left; width:650px; padding-top:20px;">
            When you register with iHOUSE360, you will get the following benefits and we will bring more free services to you in the near future. And what's more, <font style="color:#FF0000">the registration is totally free!</font><br /><br />
            </div>
            <div style="padding-bottom:5px;"><u>Benefits</u></div>
            <div>
            (1) Contact advertised agent by email sent through ihouse360 portal;<br />
            (2) Receive e-newsletter in regular base;<br />
            (3) Make rating and write review comment for any Condo project(s);<br />
            (4) Keep interested projects in favourites folder at your user account;<br />
            (5) Email notification for new project launching, event, etc;<br />
            (6) Contact ihouse360 for direct services, 
            </div>
            
             <div style="padding-top:10px; padding-bottom:20px; float:left; width:620px;">
        	<div style="float:right;">
            	<input type="submit" value="Continue" class="contactbutton" name="individual_form" id="individual_form" onClick="javascript:individual(this);" />
            </div>
        	</div>
            
            </div><!--tabular4-->
            
            </div><!--tablist1-->
            
        
        </div>
        
       
        
    </div>
    
    <div id="tab_agent" class="tabular1" style="display:none;">
    	<div style="padding:10px;">
        
        	 <div id="menu_list_user_register">
<ul id="" class="tab_nav996">     
	<li id="tab_li996_agent_chi" class="tabactive996"><a href="javascript:languagebTab('agent_chi')">中文</a></li>
	<li id="tab_li996_agent_eng"><a href="javascript:languagebTab('agent_eng')">English</a></li>
    		</ul>
            </div>
            
            
        	<div id="tablist6"> 
            <div id="tab_agent_chi" class="tabular7">
            
        	<div style="float:left; width:650px; padding-top:20px;">
           当您注册成为iHOUSE360的尊贵会员，您将享有以下权益与服务。<font style="color:#FF0000">经纪注册将只收一次性的S$20账户管理费，没有年维护费，并且账户永久有效。</font>同时我们还会不断地推陈出新，提供更好更有价值的服务给您。<br /><br />
            </div>
            <div style="padding-bottom:5px;"><u>注册用户权利</u></div>
            <div>
            (1) 注册成功后您的注册账户内将能立即收到S$30等价的信用点数，您可以轻松试用iHOUSE360的各项广告上载服务；<br />
            (2) 有资格订购各类属于房产经纪专用的广告服务配套；<br />
            (3) 接收免费市场简报；<br />
            (4) 给您熟悉的公寓评分和写评论，与大众分享您的经验；<br />
            (5) 收藏您感兴趣的公寓，方便下次再访问；<br />
            (6) 接收信息发布会或相关活动通知；<br />
            (7) 联系iHOUSE360获取直接或更多的服务。
            </div>
            
            <div style="padding-left:20px">
            
            	<div style="padding-bottom:5px; padding-top:20px; color:#FF0000;">Listing plans</div>
                
                <div style="padding-left:10px;">
                <div style="border:solid 1px #666666; float:left; width:550px; height:auto;">
                
                	<div style="background-color:#e4dfec; float:left; width:550px; height:20px; line-height:20px;">
                    	<div style="float:left; width:20px; padding-left:10px;">&nbsp;</div>
                        <div style="float:left; width:120px;">Plan</div>
                        <div style="float:left; width:100px;">List Price</div>
                        <div style="float:left; width:270px;">Feature</div>
                    </div>
                    
                    <div style="background-color:#ffffff; float:left; width:550px; height:50px; padding-top:10px; line-height:15px;">
                    	<div style="float:left; width:20px; padding-left:10px;">(a)</div>
                        <div style="float:left; width:120px;">Per-pay base</div>
                        <div style="float:left; width:100px;">S$6/Listing</div>
                        <div style="float:left; width:300px;">All listings in this category will be 90 days live.</div>
                    </div>
                    
                    <div style="background-color:#ccc0da; float:left; width:550px; height:50px; padding-top:10px; line-height:15px;">
                    	<div style="float:left; width:20px; padding-left:10px;">(b)</div>
                        <div style="float:left; width:120px;">Quarterly Subscription Plan</div>
                        <div style="float:left; width:100px;">S$120/Quarter</div>
                        <div style="float:left; width:300px;">Up to 50 concurrent listings and unlimited refreshing</div>
                    </div>
                    
                    <div style="background-color:#ffffff; float:left; width:550px; height:50px; padding-top:10px; line-height:15px;">
                    	<div style="float:left; width:20px; padding-left:10px;">(c)</div>
                        <div style="float:left; width:120px;">Yearly Subscription Plan</div>
                        <div style="float:left; width:100px;">S$388/Year</div>
                        <div style="float:left; width:300px;">Up to 50 concurrent listings and unlimited refreshing</div>
                    </div>
                    
                    <div style="background-color:#ccc0da; float:left; width:550px; height:40px; padding-top:10px; line-height:15px;">
                    	<div align="center" style="float:left; width:540px; padding-left:10px; color:#FF0000;">* Plan (b) and (c): On top of S$30 credits can be used, currently entitle 25% discount over listed price!</div>
                    </div>
                </div>
                </div>
                
                <div style="padding-bottom:5px; padding-top:20px; color:#FF0000; width:550px; float:left;">Featured Ads subscription Plans</div>
                <div style="padding-left:10px;">
                <div style="border:solid 1px #666666; float:left; width:550px; height:auto;">
                	<div style="background-color:#f3f3f3; float:left; width:550px; height:20px; line-height:20px;">
                    	<div style="float:left; width:20px; padding-left:10px;">&nbsp;</div>
                        <div style="float:left; width:140px;">Plan</div>
                        <div style="float:left; width:70px;">Max. No</div>
                        <div style="float:left; width:310px;">Exposure</div>
                    </div>
                    
                    <div style="background-color:#d9e5bd; float:left; width:550px; height:50px; padding-top:10px; line-height:15px;">
                    	<div style="float:left; width:20px; padding-left:10px;">1</div>
                        <div style="float:left; width:140px;">The Guru for this Condo</div>
                        <div style="float:left; width:70px;">1/Condo</div>
                        <div style="float:left; width:310px;">Extremely outstanding place at the subscribed Condo</div>
                    </div>
                    
                    <div style="background-color:#fff; float:left; width:550px; height:50px; padding-top:10px; line-height:15px;">
                    	<div style="float:left; width:20px; padding-left:10px;">2</div>
                        <div style="float:left; width:140px;">Gold Specialist for this Condo</div>
                        <div style="float:left; width:70px;">3/Condo</div>
                        <div style="float:left; width:310px;">Outstanding place at the subscribed Condo</div>
                    </div>
                    
                    <div style="background-color:#d9e5bd; float:left; width:550px; height:50px; padding-top:10px; line-height:15px;">
                    	<div style="float:left; width:20px; padding-left:10px;">3</div>
                        <div style="float:left; width:140px;">Condo Expert</div>
                        <div style="float:left; width:70px;">6</div>
                        <div style="float:left; width:310px;">Always there at Condo Directory page</div>
                    </div>
                    
                    <div style="background-color:#fff; float:left; width:550px; height:50px; padding-top:10px; line-height:15px;">
                    	<div style="float:left; width:20px; padding-left:10px;">4</div>
                        <div style="float:left; width:140px;">Side-bar featured Listing</div>
                        <div style="float:left; width:70px;">30/Rent<br />30/Sale</div>
                        <div style="float:left; width:310px;">Always there at property relevant pages except home page</div>
                    </div>
                    
                    <div style="background-color:#d9e5bd; float:left; width:550px; height:50px; padding-top:10px; line-height:15px;">
                    	<div style="float:left; width:20px; padding-left:10px;">5</div>
                        <div style="float:left; width:140px;">Featured Listings @ Property Detail</div>
                        <div style="float:left; width:70px;">8/Condo</div>
                        <div style="float:left; width:310px;">Always there at Property Details page, applicable to the same Condo per listed</div>
                    </div>
                    
                    <div style="background-color:#fff; float:left; width:550px; height:50px; padding-top:10px; line-height:15px;">
                    	<div style="float:left; width:20px; padding-left:10px;">6</div>
                        <div style="float:left; width:140px;">Featured Listings @ SellAdListing or @ RentAdListing</div>
                        <div style="float:left; width:70px;">8/Rent<br />8/Sale</div>
                        <div style="float:left; width:310px;">Always there at Listings search page outstanding place</div>
                    </div>
                    
                    <div style="background-color:#d9e5bd; float:left; width:550px; height:50px; padding-top:10px; line-height:15px;">
                    	<div style="float:left; width:20px; padding-left:10px;">7</div>
                        <div style="float:left; width:140px;">Featured Listings for New Condo for sale</div>
                        <div style="float:left; width:70px;">50</div>
                        <div style="float:left; width:310px;">Always there at Condo Directory page</div>
                    </div>
                    
                </div>
                </div>
                
            </div>
            
             <div style="padding-top:10px; padding-bottom:20px; float:left; width:620px;">
        	<div style="float:right;">
            	<input type="submit" value="继续" class="contactbutton" name="agent_form1" id="agent_form1" onClick="javascript:agent1(this);" />
            </div>
        </div>
            
            </div><!--tabular7-->
            
            <div id="tab_agent_eng" class="tabular7" style="display:none;">
            
        	<div style="float:left; width:650px; padding-top:20px;">
            When you register with iHOUSE360, you will get the following benefits and we will bring more valuable services to you in the near future. And what's more, <font style="color:#FF0000">the registration is one time charge at S$20 for life period, no yearly maintenance fee!</font><br /><br />
            </div>
            
            <div style="padding-bottom:5px;"><u>Benefits</u></div>
            
            <div>
            (1) Instantly receive S$30 equivalent credits (automatically credit into your user account) for advertisement trial with any Listing plans available at iHOUSE360 portal.<br />
            (2) Eligible to subscribe various Listing plans and featured ads at your choice.<br />
            (3) Receive e-newsletter in regular base;<br />
            (4) Keep interested projects in favourites folder at your user account;<br />
            (5) Email notification for relevant events;<br />
            (6) Contact ihouse360 for direct services,<br />
            </div>
            
             
                   
            <div style="padding-left:20px">
            
            	<div style="padding-bottom:5px; padding-top:20px; color:#FF0000;">Listing plans</div>
                
                <div style="padding-left:10px;">
                <div style="border:solid 1px #666666; float:left; width:550px; height:auto;">
                
                	<div style="background-color:#e4dfec; float:left; width:550px; height:20px; line-height:20px;">
                    	<div style="float:left; width:20px; padding-left:10px;">&nbsp;</div>
                        <div style="float:left; width:120px;">Plan</div>
                        <div style="float:left; width:100px;">List Price</div>
                        <div style="float:left; width:270px;">Feature</div>
                    </div>
                    
                    <div style="background-color:#ffffff; float:left; width:550px; height:50px; padding-top:10px; line-height:15px;">
                    	<div style="float:left; width:20px; padding-left:10px;">(a)</div>
                        <div style="float:left; width:120px;">Per-pay base</div>
                        <div style="float:left; width:100px;">S$6/Listing</div>
                        <div style="float:left; width:300px;">All listings in this category will be 90 days live.</div>
                    </div>
                    
                    <div style="background-color:#ccc0da; float:left; width:550px; height:50px; padding-top:10px; line-height:15px;">
                    	<div style="float:left; width:20px; padding-left:10px;">(b)</div>
                        <div style="float:left; width:120px;">Quarterly Subscription Plan</div>
                        <div style="float:left; width:100px;">S$120/Quarter</div>
                        <div style="float:left; width:300px;">Up to 50 concurrent listings and unlimited refreshing</div>
                    </div>
                    
                    <div style="background-color:#ffffff; float:left; width:550px; height:50px; padding-top:10px; line-height:15px;">
                    	<div style="float:left; width:20px; padding-left:10px;">(c)</div>
                        <div style="float:left; width:120px;">Yearly Subscription Plan</div>
                        <div style="float:left; width:100px;">S$388/Year</div>
                        <div style="float:left; width:300px;">Up to 50 concurrent listings and unlimited refreshing</div>
                    </div>
                    
                    <div style="background-color:#ccc0da; float:left; width:550px; height:40px; padding-top:10px; line-height:15px;">
                    	<div align="center" style="float:left; width:540px; padding-left:10px; color:#FF0000;">* Plan (b) and (c): On top of S$30 credits can be used, currently entitle 25% discount over listed price!</div>
                    </div>
                </div>
                </div>
                
                <div style="padding-bottom:5px; padding-top:20px; color:#FF0000; width:550px; float:left;">Featured Ads subscription Plans</div>
                <div style="padding-left:10px;">
                <div style="border:solid 1px #666666; float:left; width:550px; height:auto;">
                	<div style="background-color:#f3f3f3; float:left; width:550px; height:20px; line-height:20px;">
                    	<div style="float:left; width:20px; padding-left:10px;">&nbsp;</div>
                        <div style="float:left; width:140px;">Plan</div>
                        <div style="float:left; width:70px;">Max. No</div>
                        <div style="float:left; width:310px;">Exposure</div>
                    </div>
                    
                    <div style="background-color:#d9e5bd; float:left; width:550px; height:50px; padding-top:10px; line-height:15px;">
                    	<div style="float:left; width:20px; padding-left:10px;">1</div>
                        <div style="float:left; width:140px;">The Guru for this Condo</div>
                        <div style="float:left; width:70px;">1/Condo</div>
                        <div style="float:left; width:310px;">Extremely outstanding place at the subscribed Condo</div>
                    </div>
                    
                    <div style="background-color:#fff; float:left; width:550px; height:50px; padding-top:10px; line-height:15px;">
                    	<div style="float:left; width:20px; padding-left:10px;">2</div>
                        <div style="float:left; width:140px;">Gold Specialist for this Condo</div>
                        <div style="float:left; width:70px;">3/Condo</div>
                        <div style="float:left; width:310px;">Outstanding place at the subscribed Condo</div>
                    </div>
                    
                    <div style="background-color:#d9e5bd; float:left; width:550px; height:50px; padding-top:10px; line-height:15px;">
                    	<div style="float:left; width:20px; padding-left:10px;">3</div>
                        <div style="float:left; width:140px;">Condo Expert</div>
                        <div style="float:left; width:70px;">6</div>
                        <div style="float:left; width:310px;">Always there at Condo Directory page</div>
                    </div>
                    
                    <div style="background-color:#fff; float:left; width:550px; height:50px; padding-top:10px; line-height:15px;">
                    	<div style="float:left; width:20px; padding-left:10px;">4</div>
                        <div style="float:left; width:140px;">Side-bar featured Listing</div>
                        <div style="float:left; width:70px;">30/Rent<br />30/Sale</div>
                        <div style="float:left; width:310px;">Always there at property relevant pages except home page</div>
                    </div>
                    
                    <div style="background-color:#d9e5bd; float:left; width:550px; height:50px; padding-top:10px; line-height:15px;">
                    	<div style="float:left; width:20px; padding-left:10px;">5</div>
                        <div style="float:left; width:140px;">Featured Listings @ Property Detail</div>
                        <div style="float:left; width:70px;">8/Condo</div>
                        <div style="float:left; width:310px;">Always there at Property Details page, applicable to the same Condo per listed</div>
                    </div>
                    
                    <div style="background-color:#fff; float:left; width:550px; height:50px; padding-top:10px; line-height:15px;">
                    	<div style="float:left; width:20px; padding-left:10px;">6</div>
                        <div style="float:left; width:140px;">Featured Listings @ SellAdListing or @ RentAdListing</div>
                        <div style="float:left; width:70px;">8/Rent<br />8/Sale</div>
                        <div style="float:left; width:310px;">Always there at Listings search page outstanding place</div>
                    </div>
                    
                    <div style="background-color:#d9e5bd; float:left; width:550px; height:50px; padding-top:10px; line-height:15px;">
                    	<div style="float:left; width:20px; padding-left:10px;">7</div>
                        <div style="float:left; width:140px;">Featured Listings for New Condo for sale</div>
                        <div style="float:left; width:70px;">50</div>
                        <div style="float:left; width:310px;">Always there at Condo Directory page</div>
                    </div>
                    
                </div>
                </div>
                
            </div>

        
        <div style="padding-top:10px; padding-bottom:20px; float:left; width:630px;">
        	<div style="float:right;">
            	<input type="submit" value="Continue" class="contactbutton" name="agent_form" id="agent_form" onClick="javascript:agent(this);" />
            </div>
        </div>
        
    </div><!--tabular7-->
            
            </div><!--tablist1-->
            
        
        </div>
        
       
        
    </div>
    
    <div id="tab_owner" class="tabular1" style="display:none;">
    	<div style="padding:10px;">
        	<div id="menu_list_user_register">
<ul id="" class="tab_nav998">     
	<li id="tab_li998_owner_chi" class="tabactive998"><a href="javascript:languageaTab('owner_chi')">中文</a></li>
	<li id="tab_li998_owner_eng"><a href="javascript:languageaTab('owner_eng')">English</a></li>
    		</ul>
            </div>
     
            <div id="tablist2"> 
            <div id="tab_owner_chi" class="tabular5">
            
        	<div style="float:left; width:650px; padding-top:20px;">
           当您注册成为iHOUSE360的尊贵会员，您将享有以下<font style="color:#FF0000">免费服务。</font>并且我们还会不断地推陈出新，提供更好更有价值的服务给您。<br /><br />
            </div>
            <div style="padding-bottom:5px;"><u>注册用户权利</u></div>
            <div>
            (1) 您可以免费上载一则房源出租广告，拥有和经纪相同的广告搜索功能；<br />
            (2) 如果您需要经纪服务，可通过iHOUSE360平台，发信息给登广告的经纪；<br />
            (3) 接收免费市场简报；<br />
            (4) 给您熟悉的公寓评分和写评论，与大众分享您的经验；<br />
            (5) 收藏您感兴趣的公寓，方便下次再访问；<br />
            (6) 接收新登场的公寓信息、信息发布会或相关活动通知；<br />
            (7) 联系iHOUSE360获取直接或更多的服务。 
            </div>
            
            <div style="padding-top:10px; padding-bottom:20px; float:left; width:640px;">
        	<div style="float:right;">
            	<input type="submit" value="继续" class="contactbutton" name="owner_form" id="owner_form1" onClick="javascript:owner1(this);" />
            </div>
        </div>
            
            </div><!--tabular4-->
            
            <div id="tab_owner_eng" class="tabular5" style="display:none;">
            
        	<div style="float:left; width:650px; padding-top:20px;">
            When you register with iHOUSE360, you will get the following benefits and we will bring more free services to you in the near future. And what's more, <font style="color:#FF0000">the registration is totally free!</font><br /><br />
            </div>
            <div style="padding-bottom:5px;"><u>Benefits</u></div>
            <div>
            (1) Posting one property for rental ads to market with same search function as agent ads, and it is totally free.<br />
            (2) Contact advertised agent by email sent through ihouse360 portal, if you need agent service you;<br />
            (3) Receive e-newsletter in regular base;
            (4) Give rating and write review comment(s) for any Condo project(s);<br />
            (5) Keep interested projects in favourites folder at your user account;<br />
            (6) Email notification for new project launching, event, etc;<br />
            (7) Contact ihouse360 for direct services,
            </div>
            
            <div style="padding-top:10px; padding-bottom:20px; float:left; width:640px;">
        	<div style="float:right;">
            	<input type="submit" value="Continue" class="contactbutton" name="owner_form" id="owner_form" onClick="javascript:owner(this);" />
            </div>
       		 </div>
        
            
            </div><!--tabular4-->
            
            </div><!--tablist1-->
            
        
        </div>
        
       
        
    </div>
    
    <div id="tab_merchant" class="tabular1" style="display:none;">
    	<div style="padding:10px;">
        
        	<div id="menu_list_user_register">
<ul id="" class="tab_nav997">     
	<li id="tab_li997_merchant_chi" class="tabactive997"><a href="javascript:languagemTab('merchant_chi')">中文</a></li>
	<li id="tab_li997_merchant_eng"><a href="javascript:languagemTab('merchant_eng')">English</a></li>
    		</ul>
            </div>
     
            <div id="tablist3"> 
            <div id="tab_merchant_chi" class="tabular6">
            
        	<div style="float:left; width:650px; padding-top:20px;">
           当您注册成为iHOUSE360的尊贵会员，您将享有以下<font style="color:#FF0000">免费服务。</font>并且我们还会不断地推陈出新，提供更好更有价值的服务给您。<br /><br />
            </div>
            <div style="padding-bottom:5px;"><u>注册用户权利</u></div>
            <div>
            (1) 接收免费市场简报，包括分享注册用户数及其地区分布、网页浏览量等统计数据；<br />
            (2) 有资格获取iHOUSE360不定期的促销和优惠固本；<br />
            (3) 接收信息发布会或相关活动通知；<br />
            (4) 联系iHOUSE360获取直接或更多的服务。
            </div>
            
        <div style="padding-top:10px; padding-bottom:20px; float:left; width:640px;">
        	<div style="float:right;">
            	<input type="submit" value="继续" class="contactbutton" name="merchant_form" id="merchant_form1" onClick="javascript:merchant1(this);" />
            </div>
        </div>
            
            </div><!--tabular4-->
            
            <div id="tab_merchant_eng" class="tabular6" style="display:none;">
            
        	<div style="float:left; width:650px; padding-top:20px;">
            When you register with iHOUSE360, you will get the following benefits and we will bring more free services to you in the near future. And what's more, <font style="color:#FF0000">the registration is totally free!</font><br /><br />
            </div>
            <div style="padding-bottom:5px;"><u>Benefits</u></div>
            <div>
            (1) (1)Receive e-newsletter on a regular base and receive exclusive statistic data on the registered user number and its formation, web pages hits, etc.;<br />
            (2) Receive e-coupon or promotion from iHOUSE360 from time to time;<br />
            (3) Email notification for relevant events and activities, etc;<br />
            (4) Contact ihouse360 for direct services,
            </div>
            
            <div style="padding-top:10px; padding-bottom:20px; float:left; width:640px;">
        	<div style="float:right;">
            	<input type="submit" value="Continue" class="contactbutton" name="merchant_form" id="merchant_form" onClick="javascript:merchant(this);" />
            </div>
        </div>

            </div><!--tabular4-->
            
            </div><!--tablist1-->
            
        
        </div>
        
       
        
    </div>
    <input type="hidden" name="option" value="com_user" />
    <input type="hidden" name="view" value="register" />
    <input type="hidden" name="Itemid" value="79" />
    <input type="hidden" name="user_type" id="user_type" value="" />
    <input type="hidden" name="lang_type" id="lang_type" value="" />
    <script type="text/javascript">

						function individual(submit) {
							var a = "individual";
							document.getElementById('user_type').value = a;
							var b = "eng";
							document.getElementById('lang_type').value = b;
						}
						
						function agent(submit) {
							var a = "agent";
							document.getElementById('user_type').value = a;
							var b = "eng";
							document.getElementById('lang_type').value = b;
						}
						
						function owner(submit) {
							var a = "owner";
							document.getElementById('user_type').value = a;
							var b = "eng";
							document.getElementById('lang_type').value = b;
						}
						
						function merchant(submit) {
							var a = "merchant";
							document.getElementById('user_type').value = a;
							var b = "eng";
							document.getElementById('lang_type').value = b;
						}
						
						function individual1(submit) {
							var a = "individual";
							document.getElementById('user_type').value = a;
							var b = "chi";
							document.getElementById('lang_type').value = b;
						}
						
						function agent1(submit) {
							var a = "agent";
							document.getElementById('user_type').value = a;
							var b = "chi";
							document.getElementById('lang_type').value = b;
						}
						
						function owner1(submit) {
							var a = "owner";
							document.getElementById('user_type').value = a;
							var b = "chi";
							document.getElementById('lang_type').value = b;
						}
						
						function merchant1(submit) {
							var a = "merchant";
							document.getElementById('user_type').value = a;
							var b = "chi";
							document.getElementById('lang_type').value = b;
						}
						
	</script>
  
</div><!--tablist-->
  </form>
</div><!--end-->
