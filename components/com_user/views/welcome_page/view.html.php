<?php
/**
 * @version		$Id: view.html.php 11646 2009-03-01 19:34:56Z ian $
 * @package		Joomla
 * @subpackage	Users
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Form component
 *
 * @static
 * @package		Joomla
 * @subpackage	Welcome page
 * @since 1.0
 */
class UserViewWelcome_page extends JView
{
	function display($tpl = null)
	{
		global $mainframe;
		$db = & JFactory::getDBO();
		
		$tid = JRequest::getVar( 'tid', '', 'get', 'tid' );
		
		/* ANDREAS */
		$bid = JRequest::getVar( 'bid', '');
		
		if($bid) {
			$query=" UPDATE #__banner SET clicks = clicks + 1 WHERE bid = ".$bid;
				$db->setQuery($query);
				$db->query();
		}
		/*$sid = JRequest::getVar( 'sid', '', 'get', 'sid' );
		$id		= substr($sid, 3, -3);
		
		
		
		$db = & JFactory::getDBO();
	
		$query="SELECT * FROM #__users WHERE id = ".$id;
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		
		$query="SELECT params FROM #__ihouse_promotion WHERE id = '1'";
		$db->setQuery($query);
		$params = $db->loadResult();
		
		$credits = explode("=", $params);

			if($row->user_category == 'Agent'){
		$query = "INSERT INTO #__ihouse_users_credit(id,user_id,credit,total_buy)"
		
			. "\n VALUES ( NULL, '".$id."','".$credits[1]."', '0')";
			$db->setQuery( $query );
			$db->query();
				}*/
			$this->assignRef('tid', $tid );
			$this->assignRef('rows', $rows );
			parent::display($tpl);

		
	}
}
