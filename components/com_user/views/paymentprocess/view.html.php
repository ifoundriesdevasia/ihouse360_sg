<?php
/**
 * @version		$Id: view.html.php 11646 2009-03-01 19:34:56Z ian $
 * @package		Joomla
 * @subpackage	Users
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Users component
 *
 * @static
 * @package		Joomla
 * @subpackage	Payment Process
 * @since 1.0
 */
class UserViewPaymentprocess extends JView
{
	function display($tpl = null)    
	{
		global $mainframe;
		$db = & JFactory::getDBO();
		
		$sid	= JRequest::getVar('sid', '', 'get', 'sid');
		$id		= substr($sid, 3, -3);
		$rows = $this->getpaypal();
		
		$this->assignRef('id'	, 	$id );
		$this->assignRef('rows'	, 	$rows );
		parent::display($tpl);
	}
	
	function getpaypal() {
	
	$db = & JFactory::getDBO();
	
	$query="SELECT * FROM #__paypal_config";
	$db->setQuery($query);
	$row = $db->loadObjectList();
	
	return $row;
	
	}//end function
	

}
