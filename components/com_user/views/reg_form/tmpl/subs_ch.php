<?php defined('_JEXEC') or die('Restricted access'); 
JHTML::_('behavior.formvalidation');
require_once( JPATH_COMPONENT.DS.'image.php' );
?>
<script type="text/javascript">
<!--
	Window.onDomReady(function(){
	
		document.formvalidator.setHandler('passverify', function (value) { return ($('password').value == value); }	);
	});
// -->
</script>
<?php
require_once(JPATH_ROOT.DS.'includes'.DS.'tools.php');
$saluteOptions[] = JHTML::_('select.option', '', JText::_('Please Select'));
		$saluteOptions[] = JHTML::_('select.option', 'Mr', JText::_('Mr'));
		$saluteOptions[] = JHTML::_('select.option', 'Mrs', JText::_('Mrs'));
		$saluteOptions[] = JHTML::_('select.option', 'Mdm', JText::_('Mdm'));
		$saluteOptions[] = JHTML::_('select.option', 'Miss', JText::_('Miss'));
		$saluteOptions[] = JHTML::_('select.option', 'Dr', JText::_('Dr'));
		
		$residenceOptions[] = JHTML::_('select.option', '', JText::_('Please Select'));
		$residenceOptions[] = JHTML::_('select.option', 'Singapore Citizen', JText::_('Singapore Citizen'));
		$residenceOptions[] = JHTML::_('select.option', 'Singapore PR', JText::_('Singapore PR'));
		$residenceOptions[] = JHTML::_('select.option', 'Others', JText::_('Others'));
		
		$userintOptions[] = JHTML::_('select.option', '', JText::_('Please Select'));
		$userintOptions[] = JHTML::_('select.option', 'Rent', JText::_('Rent Property'));
		$userintOptions[] = JHTML::_('select.option', 'Buy', JText::_('Buy Property'));
		$userintOptions[] = JHTML::_('select.option', 'None', JText::_('None'));
		
		$merchantintOptions[] = JHTML::_('select.option', '', JText::_('Please Select'));
		$merchantintOptions[] = JHTML::_('select.option', 'Place Ads', JText::_('Place Ads'));
		$merchantintOptions[] = JHTML::_('select.option', 'EDM Services', JText::_('EDM Services'));
		$merchantintOptions[] = JHTML::_('select.option', 'Receive Free Events Invitation', JText::_('Receive Free Events Invitation'));
		
		$country_list = array(

		"Afghanistan",
	
		"Albania",
	
		"Algeria",
	
		"Andorra",
	
		"Angola",
	
		"Antigua and Barbuda",
	
		"Argentina",
	
		"Armenia",
	
		"Australia",
	
		"Austria",
	
		"Azerbaijan",
	
		"Bahamas",
	
		"Bahrain",
	
		"Bangladesh",
	
		"Barbados",
	
		"Belarus",
	
		"Belgium",
	
		"Belize",
	
		"Benin",
	
		"Bhutan",
	
		"Bolivia",
	
		"Bosnia and Herzegovina",
	
		"Botswana",
	
		"Brazil",
	
		"Brunei",
	
		"Bulgaria",
	
		"Burkina Faso",
	
		"Burundi",
	
		"Cambodia",
	
		"Cameroon",
	
		"Canada",
	
		"Cape Verde",
	
		"Central African Republic",
	
		"Chad",
	
		"Chile",
	
		"China",
	
		"Colombi",
	
		"Comoros",
	
		"Congo (Brazzaville)",
	
		"Congo",
	
		"Costa Rica",
	
		"Cote d’Ivoire",
	
		"Croatia",
	
		"Cuba",
	
		"Cyprus",
	
		"Czech Republic",
	
		"Denmark",
	
		"Djibouti",
	
		"Dominica",
	
		"Dominican Republic",
	
		"East Timor (Timor Timur)",
	
		"Ecuador",
	
		"Egypt",
	
		"El Salvador",
	
		"Equatorial Guinea",
	
		"Eritrea",
	
		"Estonia",
	
		"Ethiopia",
	
		"Fiji",
	
		"Finland",
	
		"France",
	
		"Gabon",
	
		"Gambia, The",
	
		"Georgia",
	
		"Germany",
	
		"Ghana",
	
		"Greece",
	
		"Grenada",
	
		"Guatemala",
	
		"Guinea",
	
		"Guinea-Bissau",
	
		"Guyana",
	
		"Haiti",
	
		"Honduras",
	
		"Hungary",
	
		"Iceland",
	
		"India",
	
		"Indonesia",
	
		"Iran",
	
		"Iraq",
	
		"Ireland",
	
		"Israel",
	
		"Italy",
	
		"Jamaica",
	
		"Japan",
	
		"Jordan",
	
		"Kazakhstan",
	
		"Kenya",
	
		"Kiribati",
	
		"Korea, North",
	
		"Korea, South",
	
		"Kuwait",
	
		"Kyrgyzstan",
	
		"Laos",
	
		"Latvia",
	
		"Lebanon",
	
		"Lesotho",
	
		"Liberia",
	
		"Libya",
	
		"Liechtenstein",
	
		"Lithuania",
	
		"Luxembourg",
	
		"Macedonia",
	
		"Madagascar",
	
		"Malawi",
	
		"Malaysia",
	
		"Maldives",
	
		"Mali",
	
		"Malta",
	
		"Marshall Islands",
	
		"Mauritania",
	
		"Mauritius",
	
		"Mexico",
	
		"Micronesia",
	
		"Moldova",
	
		"Monaco",
	
		"Mongolia",
	
		"Morocco",
	
		"Mozambique",
	
		"Myanmar",
	
		"Namibia",
	
		"Nauru",
	
		"Nepa",
	
		"Netherlands",
	
		"New Zealand",
	
		"Nicaragua",
	
		"Niger",
	
		"Nigeria",
	
		"Norway",
	
		"Oman",
	
		"Pakistan",
	
		"Palau",
	
		"Panama",
	
		"Papua New Guinea",
	
		"Paraguay",
	
		"Peru",
	
		"Philippines",
	
		"Poland",
	
		"Portugal",
	
		"Qatar",
	
		"Romania",
	
		"Russia",
	
		"Rwanda",
	
		"Saint Kitts and Nevis",
	
		"Saint Lucia",
	
		"Saint Vincent",
	
		"Samoa",
	
		"San Marino",
	
		"Sao Tome and Principe",
	
		"Saudi Arabia",
	
		"Senegal",
	
		"Serbia and Montenegro",
	
		"Seychelles",
	
		"Sierra Leone",
	
		"Singapore",
	
		"Slovakia",
	
		"Slovenia",
	
		"Solomon Islands",
	
		"Somalia",
	
		"South Africa",
	
		"Spain",
	
		"Sri Lanka",
	
		"Sudan",
	
		"Suriname",
	
		"Swaziland",
	
		"Sweden",
	
		"Switzerland",
	
		"Syria",
	
		"Taiwan",
	
		"Tajikistan",
	
		"Tanzania",
	
		"Thailand",
	
		"Togo",
	
		"Tonga",
	
		"Trinidad and Tobago",
	
		"Tunisia",
	
		"Turkey",
	
		"Turkmenistan",
	
		"Tuvalu",
	
		"Uganda",
	
		"Ukraine",
	
		"United Arab Emirates",
	
		"United Kingdom",
	
		"United States",
	
		"Uruguay",
	
		"Uzbekistan",
	
		"Vanuatu",
	
		"Vatican City",
	
		"Venezuela",
	
		"Vietnam",
	
		"Yemen",
	
		"Zambia",
	
		"Zimbabwe"
	
		);
	
		$countryOptions[] = JHTML::_('select.option', '', JText::_('Please Select'));
		foreach ($country_list as $country) {
		$countryOptions[] = JHTML::_('select.option', $country,$country);
		}
		
		// building day list
		$daysList[] = JHTML::_('select.option', 'dd','day');
		for ( $i = 1; $i <= 31; $i++ )
		{
		$daysList[] = JHTML::_('select.option', $i,$i);
		}
		$dayList = JHTML::_('select.genericlist',  $daysList, 'day', 'class="dinputbox" size="1"', 'value', 'text', '' );
	

		
		// building month list
		$monthsList[] = JHTML::_('select.option', 'mm','month');
		$monthsList[] = JHTML::_('select.option', '1', JText::_('January'));
		$monthsList[] = JHTML::_('select.option', '2', JText::_('February'));
		$monthsList[] = JHTML::_('select.option', '3', JText::_('March'));
		$monthsList[] = JHTML::_('select.option', '4', JText::_('April'));
		$monthsList[] = JHTML::_('select.option', '5', JText::_('May'));
		$monthsList[] = JHTML::_('select.option', '6', JText::_('June'));
		$monthsList[] = JHTML::_('select.option', '7', JText::_('July'));
		$monthsList[] = JHTML::_('select.option', '8', JText::_('August'));
		$monthsList[] = JHTML::_('select.option', '9', JText::_('September'));
		$monthsList[] = JHTML::_('select.option', '10', JText::_('October'));
		$monthsList[] = JHTML::_('select.option', '11', JText::_('November'));
		$monthsList[] = JHTML::_('select.option', '12', JText::_('December'));
		$monthList = JHTML::_('select.genericlist',  $monthsList, 'month', 'class="minputbox" size="1"', 'value', 'text', '' );


		
		// building year list
		$yearsList[] = JHTML::_('select.option', 'yyyy','year');
		$y = date ( "Y", extcal_get_local_time ( ) );
		
		for ( $i = 1; $i <= 70; $i++ )
		{
		$yearsList[] = JHTML::_('select.option', $y,$y);
		
			$y--;
		}
		$yearList = JHTML::_('select.genericlist',  $yearsList, 'year', 'class="yinputbox" size="1"', 'value', 'text', '' );
?>
 <!-- Chinese Version - Start -->
<div id="menu_list_user_register1">
<ul id="" class="tab_nav999">     
	<li id="tab_li999_chinese" class="tabactive999"><a href="javascript:subsLang1('ch','<?php echo $user_type?>');">中文</a></li>
	<li id="tab_li999_english"><a href="javascript:subsLang1('en','<?php echo $user_type?>');">English</a></li>
</ul>
</div>

<div><!--start-->
<input type="hidden" id="validInput" name="validInput" value="0"  />
<div id="register_div">
<div id="register-table">
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="standard_table">
<tr>
	<td width="40%" height="60">
		<label id="salutemsg" for="salute">
			尊称 * 
		</label>
        <div style="padding-top:5px;">
        <?php echo JHTML::_('select.genericlist',  $saluteOptions, 'salute', 'class="inputbox" size="1"', 'value', 'text', '' );?>
        </div>
	</td>
  	<td width="40%" height="60">
  		<label id="namemsg" for="name">
        	姓名 *
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="name" id="name" size="40" value="" class="inputbox required" />
        </div>
  	</td>
</tr>
</table>

<?php
if($user_type == 'owner'){
?>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="agent_owner_table">
<tr>
	<td width="40%" height="60">
		<label id="surnamemsg" for="surname">
			姓氏 *
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="surname" id="surname" size="40" value="" class="inputbox" maxlength="50" />
        </div>
	</td>
  	<td width="40%" height="60">
  		<label id="chinese_namemsg" for="chinese_name">
			中文姓名
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="chinese_name" id="chinese_name" size="40" value="" class="inputbox" maxlength="50" />
        </div>
  	</td>
</tr>
<tr>
	<td width="40%" height="60">
		<label id="nricmsg" for="nric">
        身份证号码
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="nric" id="nric" size="40" value="" class="inputbox" maxlength="50" />
        </div>
	</td>
  	<td width="40%" height="60">
    	<div style="padding-top:12px;">
  		<label id="user_imagemsg" for="user_image">
			您的照片
		</label>
        </div>
        <div style="padding-top:5px;">
        <?php
			frontimageUpload('user_image','user_image','user_image');
		?>

        </div>
  	</td>
</tr>
</table>
<?php
}
?>

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="login_details_table">
<tr>
	<td width="40%" height="60">
		<label id="usernamemsg" for="username">
			邮址 * (会员登陆名)
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="username" id="username" size="40" value="" class="inputbox required validate-email" />
       
        </div>
	</td>
  
    <td width="40%" height="60">&nbsp;</td>
 
</tr>
<tr>
	<td width="40%" height="60">
		<label id="passwordmsg" for="password">
			密码 * (最少6个字母)
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox required validate-password" type="password" id="password" name="password" size="40" value="" />
        </div>
	</td>
  	<td width="40%" height="60">
		<label id="password2msg" for="password2">
			确认密码 * (最少6个字母)
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox required validate-passverifychi" type="password" id="password2" name="password2" size="40" value="" />
        </div>
	</td>
</tr>
</table>

<?php
if($user_type == 'merchant'){
?>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="organization_table">
<tr>
	<td width="40%" height="60">
		<label id="organization_namemsg" for="organization_name">
			公司名 *
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="organization_name" name="organization_name" size="40" value="" />
        </div>
	</td>
  	<td width="40%" height="60">
		<label id="organization_addressmsg" for="organization_address">
			公司地址 *
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="organization_address" name="organization_address" size="40" value="" />
        </div>
	</td>
</tr>
</table>
<?php
}
?>

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="country_table">
<tr>
<td width="40%" height="60">
  		<label id="countrymsg" for="country">
        国籍
		</label>
        <div style="padding-top:5px;">
        <?php echo JHTML::_('select.genericlist',  $countryOptions, 'country', 'class="inputbox" size="1"', 'value', 'text', '');?>
        </div>
  	</td>
<td width="40%" height="60">
  <?php
if($user_type == 'merchant'){
?>
     <div id="phone1_div">
		<label id="phone_1msg" for="phone_1">
        联系电话1
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="phone_1" id="phone_1" size="40" value="" class="inputbox" maxlength="50" />
        </div>
     </div>
     <?php
	 } else {
	 ?>
	<div id="mobile_div">
		<label id="mobile_contactmsg" for="mobile_contact">
        联系电话 *
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="mobile_contact" id="mobile_contact" size="40" value="" class="inputbox" maxlength="50" />
        </div>
     </div>
<?php
}
?>   
	</td>
</tr>
</table>
<?php
if($user_type == 'owner'){
?>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="personal_details_table">
<tr>
	<td width="40%" height="60">
		<label id="dobmsg" for="dob">
			出生日期
		</label>
        <div style="padding-top:5px;">
        <?php echo $dayList;?>&nbsp;
		<?php echo $monthList;?>&nbsp;
        <?php echo $yearList;?>
        </div>
	</td>
    <td width="40%" height="60">

		<div id="home_tel_table">
        <label id="home_contactmsg" for="home_contact">
			住家电话
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="home_contact" id="home_contact" size="40" value="" class="inputbox" maxlength="50" />
        </div>
        </div>

	</td>
</tr>
</table>
<?php
}
?>
<?php
if($user_type == 'merchant'){
?>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="phone_table">
<tr>
<td width="40%" height="60">
		<label id="phone_2msg" for="phone_2">
			联系电话2
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="phone_2" name="phone_2" size="40" value="" />
        </div>
	</td>
<td width="40%" height="60">
		<label id="faxmsg" for="fax">
			传真
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="fax" name="fax" size="40" value="" />
        </div>
	</td>
</tr>
<tr>
<td width="40%" height="60">
		<label id="merchant_interestmsg" for="merchant_interest">
			有兴趣
		</label>
        <div style="padding-top:5px;">
        <?php echo JHTML::_('select.genericlist',  $merchantintOptions, 'merchant_interest', 'class="inputbox" size="1"', 'value', 'text', '');?>
        </div>
	</td>
<td width="40%" height="60">&nbsp;
	
	</td>
</tr>
</table>
<?php
}
?>
<?php
if($user_type == 'individual'){
?>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="misc_table">
<tr>
<td width="40%" height="60">
		<label id="residence_statusmsg">
			居民状况
		</label>
        <div style="padding-top:5px;">
        <?php echo JHTML::_('select.genericlist',  $residenceOptions, 'residence_status', 'class="inputbox" size="1"', 'value', 'text', '');?>
        </div>
	</td>
<td width="40%" height="60">
		<label id="user_interestmsg" for="user_interest">
			有兴趣
		</label>
        <div style="padding-top:5px;">
        <?php echo JHTML::_('select.genericlist',  $userintOptions, 'user_interest', 'class="inputbox" size="1"', 'value', 'text', '');?>
        </div>
	</td>
</tr>
</table>
<?php
}
?>
<?php
if($user_type == 'owner'){
?>

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="home_add_table">
<tr>
	<td width="40%" height="60">
		<label id="home_addressmsg" for="home_address">
        通讯地址
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="home_address" name="home_address" size="40" value="" />
        </div>
	</td>
  	<td width="40%" height="60">
		<label id="home_postalcodemsg" for="home_postalcode">
        邮编
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="home_postalcode" name="home_postalcode" size="40" value="" />
        </div>
	</td>
</tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="rental_add_table">
<tr>
<td width="40%" height="60">
		<label id="rental_addressmsg" for="rental_address">
			（出租物业）地址
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="rental_address" name="rental_address" size="40" value="" />
        </div>
	</td>
<td width="40%" height="60">
		<label id="rental_postalcodemsg" for="rental_postalcode">
			（出租物业）邮编
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="rental_postalcode" name="rental_postalcode" size="40" value="" />
        </div>
	</td>
</tr>
</table>
<?php
}
?>

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="receive_table">
<tr>
<td width="25%" height="60">
我要
</td>
<td width="60%" height="60">
<input type="checkbox" name="receive[]" id="receive[]" value="ihouse360 Newsletter" checked="checked" />&nbsp;爱豪世定期简报&nbsp;&nbsp;&nbsp;<input type="checkbox" name="receive[]" id="receive[]" value="3rd Party Newsletter" checked="checked" />&nbsp;其他商家简报<br /><input type="checkbox" name="receive[]" id="receive[]" value="Email Alert When Customer Enquiry" checked="checked" />邮件通知有关客户询问
</td>
</tr>
</table>

<table>
<tr>
	<td colspan="2" height="40">
		<?php echo JText::_( '* Compulsary field' ); ?><br />
        <?php
if($user_type == 'agent'){
?>
    * 30 credits will be credit into his/her account once registering as an Agent is successful.
<?php
	}
?>
	</td>
</tr>

</table>
</div>

</div>

<div id="button_div" style="padding-left:5px;">
	<input type="submit" value="注册" class="contactbutton" name="register_form" id="register_form" onClick="javascript:submit_frm();" />
</div>

</div>