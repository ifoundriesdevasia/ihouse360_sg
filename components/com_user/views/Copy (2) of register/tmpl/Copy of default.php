﻿<?php // no direct access
defined('_JEXEC') or die('Restricted access'); 
require_once( JPATH_COMPONENT.DS.'image.php' );

?>
<?php JHTML::_('script', 'ihouse.js', 'components/com_ads/rokbox/'); ?>

<script type="text/javascript">
<!--
	Window.onDomReady(function(){
		document.formvalidator.setHandler('passverifychi', function (value) { return ($('passwordchi').value == value); }	);
		document.formvalidator.setHandler('passverify', function (value) { return ($('password').value == value); }	);
	});
// -->
</script>

<script type="text/javascript" src="<?php echo JURI::root(); ?>components/com_user/js/userselect.js?r=<?php echo microtime(true); ?>"></script>
<?php
	if(isset($this->message)){
		$this->display('message');
	}
	

?>

<style>
#tablist1 {
	width:99.1%;
	padding:0;
	margin:0;
	min-height:300px;
	height:auto;
	float:left;
}
</style>
<form action="<?php echo JRoute::_( 'index.php?option=com_user' ); ?>" method="post" id="josForm" name="josForm" class="form-validate" <?php /*?>onsubmit="submit_frm(this);"<?php */?> enctype="multipart/form-data">

<div id="reg_border">
			<div id="reg_header">
			会员注册 Member Registration
            </div>
</div>

<div style="padding:15px;"><!--start-->


<div id="menu_list_user_register">
<ul id="" class="tab_nav88">     
	<li id="tab_li88_individual"><a href="<?php echo JURI::root();?>index.php?option=com_user&view=welcome_page&tid=1&Itemid=79"><span class="chinese_text_reg"><br />普通用户注册<br /><font class="english_text_reg">Individual</font></span></a></li>
	<li id="tab_li88_agent"><a href="<?php echo JURI::root();?>index.php?option=com_user&view=welcome_page&tid=2&Itemid=79"><span class="chinese_text_reg"><br />中介注册<br /><font class="english_text_reg">Agent</font></span></a></li>
    <li id="tab_li88_owner" ><a href="<?php echo JURI::root();?>index.php?option=com_user&view=welcome_page&tid=3&Itemid=79"><span class="chinese_text_reg"><br />屋主注册<br /><font class="english_text_reg">Owner</font></span></a></li>
    <li id="tab_li88_merchant" ><a href="<?php echo JURI::root();?>index.php?option=com_user&view=welcome_page&tid=4&Itemid=79"><span class="chinese_text_reg"><br />商家注册<br /><font class="english_text_reg">Merchant</font></span></a></li>
</ul>   
</div>
<?php
if($this->user_type == 'agent'){
?>
<script type="text/javascript">
$('tab_li88_agent').addClass('tabactive88');
</script>
<?php
}
if($this->user_type == 'owner'){
?>
<script type="text/javascript">
$('tab_li88_owner').addClass('tabactive88');
</script>
<?php
}
if($this->user_type == 'merchant'){
?>
<script type="text/javascript">
$('tab_li88_merchant').addClass('tabactive88');
</script>
<?php
}
if($this->user_type == 'individual'){
?>
<script type="text/javascript">
$('tab_li88_individual').addClass('tabactive88');
</script>
<?php
}
?>
<?php
if($this->user_type == 'agent'){
?>
<script type="text/javascript">
window.addEvent('domready',function() {	
languageagentTab('english');
$('tab_english').setStyle('display','block');
});
</script>
<?php
}
?>

<div id="tablist"> 
<div id="tab_overall" class="tabular1">

<?php
if($this->user_type == 'agent'){
?>
<div id="menu_list_user_register2">
<ul id="" class="tab_nav995">     
	<li id="tab_li995_english" class="tabactive995"><a href="javascript:languageagentTab('english')">&nbsp;</a></li>
</ul>
</div>
<?php
} else {
?>
<div id="menu_list_user_register1">
<ul id="" class="tab_nav999">     
	<li id="tab_li999_chinese" class="tabactive999"><a href="javascript:languagerTab('chinese')">中文</a></li>
	<li id="tab_li999_english"><a href="javascript:languagerTab('english')">English</a></li>
</ul>
</div>

<?php
}
?>
     
<div id="tablist1"> 
<div id="tab_chinese" class="tabular4">

<div id="register_div">
<div id="register-table">
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="standard_table">
<tr>
	<td width="40%" height="60">
		<label id="salutechimsg" for="salutechi">
			尊称 * 
		</label>
        <div style="padding-top:5px;">
        <?php echo JHTML::_('select.genericlist',  $this->saluteOptions, 'salutechi', 'class="inputbox" size="1"', 'value', 'text', $this->user->get('salute') );?>
        </div>
	</td>
  	<td width="40%" height="60">
  		<label id="namechimsg" for="namechi">
        	姓名 *
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="namechi" id="namechi" size="40" value="<?php echo $this->escape($this->user->get( 'name' ));?>" class="inputbox" />
        </div>
  	</td>
</tr>
</table>

<?php
if($this->user_type == 'owner'){
?>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="agent_owner_table">
<tr>
	<td width="40%" height="60">
		<label id="surnamechimsg" for="surnamechi">
			姓氏 *
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="surnamechi" id="surnamechi" size="40" value="<?php echo $this->escape($this->user->get( 'surname' ));?>" class="inputbox" maxlength="50" />
        </div>
	</td>
  	<td width="40%" height="60">
  		<label id="chinese_namechimsg" for="chinese_namechi">
			中文姓名
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="chinese_namechi" id="chinese_namechi" size="40" value="<?php echo $this->escape($this->user->get( 'chinese_name' ));?>" class="inputbox" maxlength="50" />
        </div>
  	</td>
</tr>
<tr>
	<td width="40%" height="60">
		<label id="nricchimsg" for="nricchi">
        身份证号码
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="nricchi" id="nricchi" size="40" value="<?php echo $this->escape($this->user->get( 'nric' ));?>" class="inputbox" maxlength="50" />
        </div>
	</td>
  	<td width="40%" height="60">
    	<?php /*?><div style="padding-top:12px;">
  		<label id="user_imagechimsg" for="user_imagechi">
			您的照片
		</label>
        </div>
        <div style="padding-top:5px;">
        <?php
			frontimageUpload($this->user->get('user_imagechi'),$this->user->get('user_imagechi'),'user_imagechi');
		?>

        </div><?php */?>
  	</td>
</tr>
</table>
<?php
}
?>

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="login_details_table">
<tr>
	<td width="40%" height="60">
		<label id="usernamechimsg" for="usernamechi">
			邮址 * (会员登陆名)
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="usernamechi" id="usernamechi" size="40" value="<?php echo $this->escape($this->user->get( 'username' ));?>" class="inputbox required validate-email" />
       
        </div>
	</td>
  
    <td width="40%" height="60">&nbsp;</td>
 
</tr>
<tr>
	<td width="40%" height="60">
		<label id="passwordchimsg" for="passwordchi">
			密码 * (最少6个字母)
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox required validate-password" type="password" id="passwordchi" name="passwordchi" size="40" value="" />
        </div>
	</td>
  	<td width="40%" height="60">
		<label id="password2chimsg" for="password2chi">
			确认密码 * (最少6个字母)
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox required validate-passverifychi" type="password" id="password2chi" name="password2chi" size="40" value="" />
        </div>
	</td>
</tr>
</table>

<?php
if($this->user_type == 'merchant'){
?>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="organization_table">
<tr>
	<td width="40%" height="60">
		<label id="organization_namechimsg" for="organization_namechi">
			公司名 *
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="organization_namechi" name="organization_namechi" size="40" value="" />
        </div>
	</td>
  	<td width="40%" height="60">
		<label id="organization_addresschimsg" for="organization_addresschi">
			公司地址 *
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="organization_addresschi" name="organization_addresschi" size="40" value="" />
        </div>
	</td>
</tr>
</table>
<?php
}
?>

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="country_table">
<tr>
<td width="40%" height="60">
  		<label id="countrychimsg" for="countrychi">
        国籍
		</label>
        <div style="padding-top:5px;">
        <?php echo JHTML::_('select.genericlist',  $this->countryOptions, 'countrychi', 'class="inputbox" size="1"', 'value', 'text', $this->user->get('country') );?>
        </div>
  	</td>
<td width="40%" height="60">
  <?php
if($this->user_type == 'merchant'){
?>
     <div id="phone1_div">
		<label id="phone_1chimsg" for="phone_1chi">
        联系电话1
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="phone_1chi" id="phone_1chi" size="40" value="<?php echo $this->escape($this->user->get( 'phone_1' ));?>" class="inputbox" maxlength="50" />
        </div>
     </div>
     <?php
	 } else {
	 ?>
	<div id="mobile_div">
		<label id="mobile_contactchimsg" for="mobile_contactchi">
        联系电话 *
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="mobile_contactchi" id="mobile_contactchi" size="40" value="<?php echo $this->escape($this->user->get( 'mobile_contact' ));?>" class="inputbox" maxlength="50" />
        </div>
     </div>
<?php
}
?>   
	</td>
</tr>
</table>
<?php
if($this->user_type == 'owner'){
?>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="personal_details_table">
<tr>
	<td width="40%" height="60">
		<label id="dobchimsg" for="dobchi">
			出生日期
		</label>
        <div style="padding-top:5px;">
        <?php echo $this->chidayList;?>&nbsp;
		<?php echo $this->chimonthList;?>&nbsp;
        <?php echo $this->chiyearList;?>
        </div>
	</td>
    <td width="40%" height="60">

		<div id="home_tel_table">
        <label id="home_contactchimsg" for="home_contactchi">
			住家电话
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="home_contactchi" id="home_contactchi" size="40" value="<?php echo $this->escape($this->user->get( 'home_contact' ));?>" class="inputbox" maxlength="50" />
        </div>
        </div>

	</td>
</tr>
</table>
<?php
}
?>
<?php
if($this->user_type == 'merchant'){
?>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="phone_table">
<tr>
<td width="40%" height="60">
		<label id="phone_2chimsg" for="phone_2chi">
			联系电话2
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="phone_2chi" name="phone_2chi" size="40" value="" />
        </div>
	</td>
<td width="40%" height="60">
		<label id="faxchimsg" for="faxchi">
			传真
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="faxchi" name="faxchi" size="40" value="" />
        </div>
	</td>
</tr>
<tr>
<td width="40%" height="60">
		<label id="merchant_interestchimsg" for="merchant_interestchi">
			有兴趣
		</label>
        <div style="padding-top:5px;">
        <?php echo JHTML::_('select.genericlist',  $this->merchantintOptions, 'merchant_interestchi', 'class="inputbox" size="1"', 'value', 'text', $this->user->get('merchant_interest') );?>
        </div>
	</td>
<td width="40%" height="60">&nbsp;
	
	</td>
</tr>
</table>
<?php
}
?>
<?php
if($this->user_type == 'individual'){
?>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="misc_table">
<tr>
<td width="40%" height="60">
		<label id="residence_statuschimsg">
			居民状况
		</label>
        <div style="padding-top:5px;">
        <?php echo JHTML::_('select.genericlist',  $this->residenceOptions, 'residence_statuschi', 'class="inputbox" size="1"', 'value', 'text', $this->user->get('residence_status') );?>
        </div>
	</td>
<td width="40%" height="60">
		<label id="user_interestchimsg" for="user_interestchi">
			有兴趣
		</label>
        <div style="padding-top:5px;">
        <?php echo JHTML::_('select.genericlist',  $this->userintOptions, 'user_interestchi', 'class="inputbox" size="1"', 'value', 'text', $this->user->get('user_interest') );?>
        </div>
	</td>
</tr>
</table>
<?php
}
?>
<?php
if($this->user_type == 'owner'){
?>

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="home_add_table">
<tr>
	<td width="40%" height="60">
		<label id="home_addresschimsg" for="home_addresschi">
        通讯地址
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="home_addresschi" name="home_addresschi" size="40" value="" />
        </div>
	</td>
  	<td width="40%" height="60">
		<label id="home_postalcodechimsg" for="home_postalcodechi">
        邮编
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="home_postalcodechi" name="home_postalcodechi" size="40" value="" />
        </div>
	</td>
</tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="rental_add_table">
<tr>
<td width="40%" height="60">
		<label id="rental_addresschimsg" for="rental_addresschi">
			（出租物业）地址
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="rental_addresschi" name="rental_addresschi" size="40" value="" />
        </div>
	</td>
<td width="40%" height="60">
		<label id="rental_postalcodechimsg" for="rental_postalcodechi">
			（出租物业）邮编
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="rental_postalcodechi" name="rental_postalcodechi" size="40" value="" />
        </div>
	</td>
</tr>
</table>
<?php
}
?>

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="receive_table">
<tr>
<td width="25%" height="60">
我要
</td>
<td width="60%" height="60">
<input type="checkbox" name="receivechi[]" id="receivechi[]" value="ihouse360 Newsletter" checked="checked" />&nbsp;爱豪世定期简报&nbsp;&nbsp;&nbsp;<input type="checkbox" name="receivechi[]" id="receivechi[]" value="3rd Party Newsletter" checked="checked" />&nbsp;其他商家简报<br /><input type="checkbox" name="receivechi[]" id="receivechi[]" value="Email Alert When Customer Enquiry" checked="checked" />邮件通知有关客户询问
</td>
</tr>
</table>

<table>
<tr>
	<td colspan="2" height="40">
		<?php echo JText::_( '* Compulsary field' ); ?><br />
        <?php
if($this->user_type == 'agent'){
?>
    * 68 credits will be credit into his/her account once registering as an Agent is successful.
<?php
	}
?>
	</td>
</tr>

</table>
</div>

</div>

<div id="button_div" style="padding-left:5px;">
	<input type="submit" value="注册" class="contactbutton validate" name="register_form" id="register_form1" onClick="javascript:register_chi(this);" />
</div>

</div>
<div id="tab_english" class="tabular4" style="display:none">
<?php
if($this->user_type == 'agent'){
?>
<div id="agent_coy_info" style="float:left; width:100%;">
<div id="reg_agent_cont"><div id="reg_agent_bg"><div id="reg_agent_header">Business Information</div></div></div>

<div id="register-table">
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane"  id="user-table">
<tr>
	<td width="40%" height="40">
		<label id="company_namemsg" for="company_name">
			<?php echo JText::_( 'Serving Organization' ); ?> *
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="company_name" id="company_name" size="40" value="<?php echo $this->escape($this->user->get( 'company_name' ));?>" class="inputbox" maxlength="50" />
        </div>
	</td>
  	<td width="40%" height="60">
    <div style="padding-top:12px;">
  		<label id="company_logomsg" for="company_logo">
			<?php echo JText::_( 'Company Logo' ); ?>
		</label>
    </div>
        <div style="padding-top:5px;">
        <?php
			frontimageUpload($this->user->get('company_logo'),$this->user->get('company_logo'),'company_logo');
		?>
        </div>
  	</td>
</tr>
</table>
</div><!--register-table-->

<div id="reg_agent_cont"><div id="reg_agent_bg">
<div id="reg_agent_header">Personal Particular</div>
</div></div>

</div>
<?php
}
?>

<div id="register_div">
<div id="register-table">
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="standard_table">
<tr>
	<td width="40%" height="60">
		<label id="saluteengmsg" for="saluteeng">
			<?php echo JText::_( 'Salute' ); ?> * 
		</label>
        <div style="padding-top:5px;">
        <?php echo JHTML::_('select.genericlist',  $this->saluteOptions, 'saluteeng', 'class="inputbox" size="1"', 'value', 'text', $this->user->get('salute') );?>
        </div>
	</td>
  	<td width="40%" height="60">
  		<label id="nameengmsg" for="nameeng">
        	<?php echo JText::_( 'Name' ); ?> *
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="name" id="name" size="40" value="<?php echo $this->escape($this->user->get( 'name' ));?>" class="inputbox" />
        </div>
  	</td>
</tr>
</table>

<?php
if(($this->user_type == 'agent')||($this->user_type == 'owner')){
?>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="agent_owner_table">
<tr>
	<td width="40%" height="60">
		<label id="surnameengmsg" for="surnameeng">
			<?php echo JText::_( 'Surname' ); ?> *
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="surnameeng" id="surnameeng" size="40" value="<?php echo $this->escape($this->user->get( 'surname' ));?>" class="inputbox" maxlength="50" />
        </div>
	</td>
  	<td width="40%" height="60">
  		<label id="chinese_nameengmsg" for="chinese_nameeng">
			<?php echo JText::_( 'Chinese Name' ); ?>
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="chinese_nameeng" id="chinese_nameeng" size="40" value="<?php echo $this->escape($this->user->get( 'chinese_name' ));?>" class="inputbox" maxlength="50" />
        </div>
  	</td>
</tr>
<tr>
	<td width="40%" height="60">
		<label id="nricengmsg" for="nriceng">
        NRIC
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="nriceng" id="nriceng" size="40" value="<?php echo $this->escape($this->user->get( 'nric' ));?>" class="inputbox" maxlength="50" />
        </div>
	</td>
  	<td width="40%" height="60">
    	<?php /*?><div style="padding-top:12px;">
  		<label id="user_imageengmsg" for="user_imageeng">
			<?php echo JText::_( 'Your Image' ); ?>
		</label>
        </div>
        <div style="padding-top:5px;">
        <?php
			frontimageUpload($this->user->get('user_imageeng'),$this->user->get('user_imageeng'),'user_imageeng');
		?>

        </div><?php */?>
  	</td>
</tr>
</table>
<?php
}
?>

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="login_details_table">
<tr>
	<td width="40%" height="60">
		<label id="usernameengmsg" for="usernameeng">
			<?php echo JText::_( 'Email Address' ); ?> * (User Login ID)
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="username" id="username" size="40" value="<?php echo $this->escape($this->user->get( 'username' ));?>" class="inputbox required validate-email" />
       
        </div>
	</td>
    <?php
	if($this->user_type == 'agent'){
	?>
	<td width="40%" height="60">
		<label id="cea_reg_nomsg" for="name">
        	<?php echo JText::_( 'CEA Registration No.' ); ?> *
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="cea_reg_no" id="cea_reg_no" size="40" value="" class="inputbox required" />
        </div>
	</td>
    <?php
	} else {
	?>
    <td width="40%" height="60">&nbsp;</td>
    <?php
	}
	?>
</tr>
<tr>
	<td width="40%" height="60">
		<label id="passwordengmsg" for="passwordeng">
			<?php echo JText::_( 'Password' ); ?> * (Min. 6 characters)
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox required validate-password" type="password" id="password" name="password" size="40" value="" />
        </div>
	</td>
  	<td width="40%" height="60">
		<label id="password2engmsg" for="password2eng">
			<?php echo JText::_( 'Verify Password' ); ?> * (Min. 6 characters)
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox required validate-passverify" type="password" id="password2" name="password2" size="40" value="" />
        </div>
	</td>
</tr>
</table>

<?php
if($this->user_type == 'merchant'){
?>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="organization_table">
<tr>
	<td width="40%" height="60">
		<label id="organization_nameengmsg" for="organization_nameeng">
			<?php echo JText::_( 'Organization Name' ); ?> *
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="organization_nameeng" name="organization_nameeng" size="40" value="" />
        </div>
	</td>
  	<td width="40%" height="60">
		<label id="organization_addressengmsg" for="organization_addresseng">
			<?php echo JText::_( 'Organization Address' ); ?> *
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="organization_addresseng" name="organization_addresseng" size="40" value="" />
        </div>
	</td>
</tr>
</table>
<?php
}
?>

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="country_table">
<tr>
<td width="40%" height="60">
  		<label id="countryengmsg" for="countryeng">
        Country
		</label>
        <div style="padding-top:5px;">
        <?php echo JHTML::_('select.genericlist',  $this->countryOptions, 'countryeng', 'class="inputbox" size="1"', 'value', 'text', $this->user->get('country') );?>
        </div>
  	</td>
<td width="40%" height="60">
  <?php
if($this->user_type == 'merchant'){
?>
     <div id="phone1_div">
		<label id="phone_1engmsg" for="phone_1eng">
        Telephone 1
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="phone_1eng" id="phone_1eng" size="40" value="<?php echo $this->escape($this->user->get( 'phone_1' ));?>" class="inputbox" maxlength="50" />
        </div>
     </div>
     <?php
	 } else {
	 ?>
	<div id="mobile_div">
		<label id="mobile_contactengmsg" for="mobile_contacteng">
        Mobile Number *
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="mobile_contacteng" id="mobile_contacteng" size="40" value="<?php echo $this->escape($this->user->get( 'mobile_contact' ));?>" class="inputbox" maxlength="50" />
        </div>
     </div>
<?php
}
?>   
	</td>
</tr>
</table>
<?php
if(($this->user_type == 'agent')||($this->user_type == 'owner')){
?>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="personal_details_table">
<tr>
	<td width="40%" height="60">
		<label id="dobmsg" for="dob">
			<?php echo JText::_( 'Date of Birth' ); ?>
		</label>
        <div style="padding-top:5px;">
        <?php echo $this->dayList;?>&nbsp;
		<?php echo $this->monthList;?>&nbsp;
        <?php echo $this->yearList;?>
        </div>
	</td>
    <td width="40%" height="60">
    <?php
if($this->user_type == 'owner'){
?>
		<div id="home_tel_table">
        <label id="home_contactengmsg" for="home_contacteng">
			<?php echo JText::_( 'Home Telephone' ); ?>
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="home_contacteng" id="home_contacteng" size="40" value="<?php echo $this->escape($this->user->get( 'home_contact' ));?>" class="inputbox" maxlength="50" />
        </div>
        </div>
 <?php } else { ?>
        <div id="personal_table">
        <label id="websitemsg" for="website">
			<?php echo JText::_( 'Personal Website' ); ?>
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="website" name="website" size="40" value="" />
        </div>
        </div>
<?php } ?>
	</td>
</tr>
</table>
<?php
}
?>
<?php
if($this->user_type == 'merchant'){
?>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="phone_table">
<tr>
<td width="40%" height="60">
		<label id="phone_2engmsg" for="phone_2eng">
			<?php echo JText::_( 'Telephone 2' ); ?>
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="phone_2eng" name="phone_2eng" size="40" value="" />
        </div>
	</td>
<td width="40%" height="60">
		<label id="faxengmsg" for="faxeng">
			<?php echo JText::_( 'Fax' ); ?>
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="faxeng" name="faxeng" size="40" value="" />
        </div>
	</td>
</tr>
<tr>
<td width="40%" height="60">
		<label id="merchant_interestengmsg" for="merchant_interesteng">
			<?php echo JText::_( 'Interest In' ); ?>
		</label>
        <div style="padding-top:5px;">
        <?php echo JHTML::_('select.genericlist',  $this->merchantintOptions, 'merchant_interesteng', 'class="inputbox" size="1"', 'value', 'text', $this->user->get('merchant_interest') );?>
        </div>
	</td>
<td width="40%" height="60">&nbsp;
	
	</td>
</tr>
</table>
<?php
}
?>
<?php
if($this->user_type == 'individual'){
?>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="misc_table">
<tr>
<td width="40%" height="60">
		<label id="residence_statusengmsg">
			<?php echo JText::_( 'Residence Status' ); ?>
		</label>
        <div style="padding-top:5px;">
        <?php echo JHTML::_('select.genericlist',  $this->residenceOptions, 'residence_statuseng', 'class="inputbox" size="1"', 'value', 'text', $this->user->get('residence_status') );?>
        </div>
	</td>
<td width="40%" height="60">
		<label id="user_interestengmsg" for="user_interesteng">
			<?php echo JText::_( 'Interest In' ); ?>
		</label>
        <div style="padding-top:5px;">
        <?php echo JHTML::_('select.genericlist',  $this->userintOptions, 'user_interesteng', 'class="inputbox" size="1"', 'value', 'text', $this->user->get('user_interest') );?>
        </div>
	</td>
</tr>
</table>
<?php
}
?>
<?php
if(($this->user_type == 'agent')||($this->user_type == 'owner')){
?>

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="home_add_table">
<tr>
	<td width="40%" height="60">
		<label id="home_addressengmsg" for="home_addresseng">
        Home Address
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="home_addresseng" name="home_addresseng" size="40" value="" />
        </div>
	</td>
  	<td width="40%" height="60">
		<label id="home_postalcodeengmsg" for="home_postalcodeeng">
        Home Postal Code
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="home_postalcodeeng" name="home_postalcodeeng" size="40" value="" />
        </div>
	</td>
</tr>
</table>
<?php
}
?>
<?php
if($this->user_type == 'owner'){
?>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="rental_add_table">
<tr>
<td width="40%" height="60">
		<label id="rental_addressengmsg" for="rental_addresseng">
			<?php echo JText::_( 'Rental Address' ); ?>
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="rental_addresseng" name="rental_addresseng" size="40" value="" />
        </div>
	</td>
<td width="40%" height="60">
		<label id="rental_postalcodeengmsg" for="rental_postalcodeeng">
			<?php echo JText::_( 'Rental Postal Code' ); ?>
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="rental_postalcodeeng" name="rental_postalcodeeng" size="40" value="" />
        </div>
	</td>
</tr>
</table>
<?php
}
?>

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="receive_table">
<tr>
<td width="25%" height="60">
Agree to receive
</td>
<td width="60%" height="60">
<input type="checkbox" name="receiveeng[]" id="receiveeng[]" value="ihouse360 Newsletter" checked="checked" />&nbsp;ihouse360 Newsletter&nbsp;&nbsp;&nbsp;<input type="checkbox" name="receiveeng[]" id="receiveeng[]" value="3rd Party Newsletter" checked="checked" />&nbsp;Third Party Newsletter<br /><input type="checkbox" name="receiveeng[]" id="receiveeng[]" value="Email Alert When Customer Enquiry" checked="checked" />Email Alert When Customer Enquiry
</td>
</tr>
</table>

<table>
<tr>
	<td colspan="2" height="40">
		<?php echo JText::_( '* Compulsary field' ); ?><br />
        <?php
if($this->user_type == 'agent'){
?>
    * 68 credits will be credit into his/her account once registering as an Agent is successful.
<?php
	}
?>
	</td>
</tr>

</table>
</div>

</div>

<div id="button_div" style="padding-left:5px;">
	<input type="submit" value="Register" class="contactbutton validate" name="register_form" id="register_form" onClick="javascript:register_eng(this);" />
</div>
	<input type="hidden" name="option" id="option" value="com_user" />
	<input type="hidden" name="task" value="register_save" />
    <input type="hidden" name="Itemid" value="79" />
    <input type="hidden" id="email" name="email"  value=""/>
	<input type="hidden" name="id" value="0" />
	<input type="hidden" name="gid" value="0" />
    <input type="hidden" name="user_category" value="<?php echo ucfirst($this->user_type);?>" />
    <input type="hidden" name="lang_selection" id="lang_selection" value="" />
   
	<?php echo JHTML::_( 'form.token' ); ?>

<script>
/*function submit_frm()
{
var userval = document.josForm.username.value;
document.josForm.email.value=userval;
}*/ 

function register_chi(submit) {
	/*var userval = document.josForm.usernamechi.value;
document.josForm.username.value=userval;
var userval = document.josForm.usernamechi.value;
document.josForm.email.value=userval;
var userval = document.josForm.passwordchi.value;
document.josForm.password.value=userval;
var userval = document.josForm.namechi.value;
document.josForm.name.value=userval;*/

	var a = "chinese";
	document.getElementById('lang_selection').value = a;
	
	/*var c = document.getElementById('username').value;
	document.getElementById('email').value = c;*/
	
	/*var b = document.getElementById('usernamechi').value;
	document.getElementById('username').value = b;
	
	var c = document.getElementById('usernamechi').value;
	document.getElementById('email').value = c;
	
	var d = document.getElementById('passwordchi').value;
	document.getElementById('password').value = d;
	
	var e = document.getElementById('namechi').value;
	document.getElementById('name').value = e;
	
	var f = document.getElementById('countrychi').value;
	document.getElementById('country').value = f;*/
	
}

function register_eng(submit) {
	/*var userval = document.josForm.usernameeng.value;
document.josForm.username.value=userval;
	var userval = document.josForm.usernameeng.value;
document.josForm.email.value=userval;
var userval = document.josForm.passwordeng.value;
document.josForm.password.value=userval;
var userval = document.josForm.nameeng.value;
document.josForm.name.value=userval;*/

	var a = "english";
	document.getElementById('lang_selection').value = a;
	
	/*var b = document.getElementById('usernameeng').value;
	document.getElementById('username').value = b;*/
	
	var c = document.getElementById('username').value;
	document.getElementById('email').value = c;
	
	/*var d = document.getElementById('passwordeng').value;
	document.getElementById('password').value = d;*/
	
	/*var e = document.getElementById('nameeng').value;
	document.getElementById('name').value = e;*/
	
	/*var f = document.getElementById('countryeng').value;
	document.getElementById('country').value = f;*/
	
}
</script>
</div>
</div><!--tablist1-->


</div>
</div><!--tablist-->
</div>

</form>
