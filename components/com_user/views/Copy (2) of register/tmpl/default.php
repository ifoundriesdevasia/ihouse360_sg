﻿<?php // no direct access
defined('_JEXEC') or die('Restricted access'); 
require_once( JPATH_COMPONENT.DS.'image.php' );
JHTML::_('behavior.formvalidation');
?>
<?php JHTML::_('script', 'ihouse.js', 'components/com_ads/rokbox/'); ?>
<?php //JHTML::_('script', 'registration.js', 'templates/main/js/'); ?>

<?php
if($this->user_type == 'agent'){
?>
<script type="text/javascript">
window.addEvent('domready', function(){
	subsLang1('en','<?php echo $this->user_type;?>');								 
});
</script>

<?php } else { ?>
<script type="text/javascript">
window.addEvent('domready', function(){
	subsLang1('ch','<?php echo $this->user_type;?>');								 
});
</script>
<?php } ?>


<?php
	if(isset($this->message)){
		$this->display('message');
	}
?>

<style>
#tablist1 {
	width:99.1%;
	padding:0;
	margin:0;
	min-height:300px;
	height:auto;
	float:left;
}
</style>


<div id="reg_border" onload="javascript:subsLang1('ch,'<?php echo $this->user_type;?>');">
			<div id="reg_header">
			会员注册 Member Registration
            </div>
</div><!--reg_border-->

<div style="padding:15px;"><!--start-->


<div id="menu_list_user_register">
<ul id="" class="tab_nav88">     
	<li id="tab_li88_individual"><a href="<?php echo JURI::root();?>index.php?option=com_user&view=welcome_page&tid=1&Itemid=79"><span class="chinese_text_reg"><br />普通用户注册<br /><font class="english_text_reg">Individual</font></span></a></li>
	<li id="tab_li88_agent"><a href="<?php echo JURI::root();?>index.php?option=com_user&view=welcome_page&tid=2&Itemid=79"><span class="chinese_text_reg"><br />中介注册<br /><font class="english_text_reg">Agent</font></span></a></li>
    <li id="tab_li88_owner" ><a href="<?php echo JURI::root();?>index.php?option=com_user&view=welcome_page&tid=3&Itemid=79"><span class="chinese_text_reg"><br />屋主注册<br /><font class="english_text_reg">Owner</font></span></a></li>
    <li id="tab_li88_merchant" ><a href="<?php echo JURI::root();?>index.php?option=com_user&view=welcome_page&tid=4&Itemid=79"><span class="chinese_text_reg"><br />商家注册<br /><font class="english_text_reg">Merchant</font></span></a></li>
</ul>   
</div>
<?php
if($this->user_type == 'agent'){
?>
<script type="text/javascript">
$('tab_li88_agent').addClass('tabactive88');
</script>
<?php
}
if($this->user_type == 'owner'){
?>
<script type="text/javascript">
$('tab_li88_owner').addClass('tabactive88');
</script>
<?php
}
if($this->user_type == 'merchant'){
?>
<script type="text/javascript">
$('tab_li88_merchant').addClass('tabactive88');
</script>
<?php
}
if($this->user_type == 'individual'){
?>
<script type="text/javascript">
$('tab_li88_individual').addClass('tabactive88');
</script>
<?php
}
?>
<?php
//if($this->user_type == 'agent'){
?>
<script type="text/javascript">
/*window.addEvent('domready',function() {	
languageagentTab('english');
$('tab_english').setStyle('display','block');
});*/
</script>
<?php
//}
?>

<div id="tablist"> 
<div id="tab_overall" class="tabular1">
<form action="<?php echo JRoute::_( 'index.php?option=com_user&view=register' ); ?>" method="post" id="josForm" name="josForm" class="form-validate" <?php /*?>onsubmit="submit_frm();"<?php */?> enctype="multipart/form-data">
<div id="subs_form"></div>
	<input type="hidden" name="option" id="option" value="com_user" />
	<input type="hidden" name="task" value="register_save" />
    <input type="hidden" name="Itemid" value="79" />
    <input type="hidden" id="email" name="email"  value=""/>
	<input type="hidden" name="id" value="0" />
	<input type="hidden" name="gid" value="0" />
    <input type="hidden" name="user_category" value="<?php echo ucfirst($this->user_type);?>" />
    <input type="hidden" name="lang_selection" id="lang_selection" value="" />
    <input type="hidden" name="user_type" id="user_type" value="<?php echo $this->user_type;?>" />
   
	<?php echo JHTML::_( 'form.token' ); ?>
<script>
function submit_frm()
{

/*for(i=0; i<document.josForm.elements.length;i++)
	{ 
	document.josForm.elements[i].setStyle('color','red');
	}*/
	
	/* validate salute (Drop Down) required //*/
	if ( document.josForm.salute.selectedIndex == 0 )
		{
		alert ( "Please Select your salutation." );
		return false;
		} //else { return true };
	
	/* validate Name (Text Box) required //*/
	if(document.josForm.name.value.length==0)
		{
		alert("Enter a Name");
		document.josForm.name.focus();
		document.josForm.name.select();
		return false;
		} //else { return true };
	
	/* validate Email (Text Box) characters: @ . required */
	var intAtSymbol = document.josForm.username.value.indexOf("@");
	var intDotSymbol = document.josForm.username.value.indexOf(".");
	var intLength = document.josForm.username.value.length;
	
	if(intAtSymbol == 0 || intAtSymbol > intLength-5)
		{
		alert("Username must contain valid @ eg: name@email.com")
		document.josForm.username.focus();
		document.josForm.username.select();
		return false;
		} //else { return true };
	if(intDotSymbol < (intAtSymbol+2)|| intDotSymbol > (intLength-3))
		{
		alert("Username must contain valid . eg: name@email.com")
		document.josForm.username.focus();
		document.josForm.username.select();
		return false;
		} //else { return true };
	
	if(document.josForm.password.value.length < 6)
		{
		alert("Password must be at least 6 characters.");
		document.josForm.password.focus();
		document.josForm.password.select();
		return false;
		} //else { return true };
	
	var obja = document.getElementById('password');
	var objb = document.getElementById('password2');
	
	if (obja.value==objb.value) {
		return true;
	} else {
		alert("Password does not match, please re-type");
		return false
	}

	var userval = document.josForm.username.value;
	document.josForm.email.value=userval;
	return true;
	document.josForm.submit();
} 
function register_chi(submit) {
	
	$('salute').setStyle('color','');
	$('name').setStyle('color','');
	$('email').setStyle('color','');
	
	var salute 		= $('salute').getProperty('value');
	var name		= $('name').getProperty('value');
	var email		= $('email').getProperty('value');
	
	var how 		= true;
	
	if(salute == '') {
		$('salute').setStyle('color','red');
		how = false;
		return false;
	}
	
	if(name == '') {
		$('name').setStyle('color','red');
		how = false;
		return false;
	}
	
	if(email == '') {
		$('email').setStyle('color','red');
		how = false;
		return false;
	} 
	
	/*if(how) {
		$('validInput').setProperty('value', 1);
		sendSubs(langs);
	}
	else {
		$('validInput').setProperty('value', 0);
	}*/
	
	/*var a = "chinese";
	document.getElementById('lang_selection').value = a;
	
	var c = document.getElementById('username').value;
	document.getElementById('email').value = c;
	document.josForm.submit();*/
}

/*function register_eng(submit) {
	var a = "english";
	document.getElementById('lang_selection').value = a;

	var c = document.getElementById('username').value;
	document.getElementById('email').value = c;
}*/
</script>
</form>
</div><!--tabular1-->
</div><!--tablist-->
</div><!--start-->
