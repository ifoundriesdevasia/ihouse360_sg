<?php // no direct access
defined('_JEXEC') or die('Restricted access'); 
require_once( JPATH_COMPONENT.DS.'image.php' );
JHTML::_('behavior.formvalidation');
?>
<?php JHTML::_('script', 'ihouse.js', 'components/com_ads/rokbox/'); ?>
<?php //JHTML::_('script', 'registration.js', 'templates/main/js/'); ?>

<?php /*?><?php
if($this->user_type == 'agent'){
?>
<script type="text/javascript">
window.addEvent('domready', function(){
	subsLang1('en','<?php echo $this->user_type;?>');								 
});
</script>

<?php } else { ?>
<script type="text/javascript">
window.addEvent('domready', function(){
	subsLang1('ch','<?php echo $this->user_type;?>');								 
});
</script>
<?php } ?><?php */?>
<script type="text/javascript">
<!--
	Window.onDomReady(function(){
		document.formvalidator.setHandler('passverify', function (value) { return ($('password').value == value); }	);
	});
// -->
</script>

<?php
	if(isset($this->message)){
		$this->display('message');
	}
?>

<style>
#tablist1 {
	width:99.1%;
	padding:0;
	margin:0;
	min-height:300px;
	height:auto;
	float:left;
}
</style>


<div id="reg_border">
			<div id="reg_header">
			会员注册 Member Registration
            </div>
</div><!--reg_border-->

<div style="padding:15px;"><!--start-->


<div id="menu_list_user_register">
<ul id="" class="tab_nav88">     
	<li id="tab_li88_individual"><a href="<?php echo JURI::root();?>index.php?option=com_user&view=welcome_page&tid=1&Itemid=79"><span class="chinese_text_reg"><br />普通用户注册<br /><font class="english_text_reg">Individual</font></span></a></li>
	<li id="tab_li88_agent"><a href="<?php echo JURI::root();?>index.php?option=com_user&view=welcome_page&tid=2&Itemid=79"><span class="chinese_text_reg"><br />中介注册<br /><font class="english_text_reg">Agent</font></span></a></li>
    <li id="tab_li88_owner" ><a href="<?php echo JURI::root();?>index.php?option=com_user&view=welcome_page&tid=3&Itemid=79"><span class="chinese_text_reg"><br />屋主注册<br /><font class="english_text_reg">Owner</font></span></a></li>
    <li id="tab_li88_merchant" ><a href="<?php echo JURI::root();?>index.php?option=com_user&view=welcome_page&tid=4&Itemid=79"><span class="chinese_text_reg"><br />商家注册<br /><font class="english_text_reg">Merchant</font></span></a></li>
</ul>   
</div>
<?php
if($this->user_type == 'agent'){
?>
<script type="text/javascript">
$('tab_li88_agent').addClass('tabactive88');
</script>
<?php
}
if($this->user_type == 'owner'){
?>
<script type="text/javascript">
$('tab_li88_owner').addClass('tabactive88');
</script>
<?php
}
if($this->user_type == 'merchant'){
?>
<script type="text/javascript">
$('tab_li88_merchant').addClass('tabactive88');
</script>
<?php
}
if($this->user_type == 'individual'){
?>
<script type="text/javascript">
$('tab_li88_individual').addClass('tabactive88');
</script>
<?php
}
?>
<?php
//if($this->user_type == 'agent'){
?>
<script type="text/javascript">
/*window.addEvent('domready',function() {	
languageagentTab('english');
$('tab_english').setStyle('display','block');
});*/
</script>
<?php
//}
?>

<div id="tablist8"> 
<div id="tab_overall" class="tabular1">
<form action="<?php echo JRoute::_( 'index.php?option=com_user&view=register' ); ?>" method="post" id="josForm" name="josForm" class="form-validate" onsubmit="submit_frm();" enctype="multipart/form-data">

<div><!--start-->

<?php
if($this->user_type == 'agent'){
?>
<div id="agent_coy_info" style="float:left; width:100%;">
<div id="reg_agent_cont"><div id="reg_agent_bg"><div id="reg_agent_header">Business Information</div></div></div>

<div id="register-table">
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane"  id="user-table">
<tr>
	<td width="40%" height="40">
		<label id="company_namemsg" for="company_name">
			公司名 <?php echo JText::_( 'Serving Organization' ); ?> *
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="company_name" id="company_name" size="40" value="" class="inputbox" maxlength="50" />
        </div>
	</td>
  	<td width="40%" height="60">
    <div style="padding-top:12px;">
  		<label id="company_logomsg" for="company_logo">
			公司标志 <?php echo JText::_( 'Company Logo' ); ?>
		</label>
    </div>
        <div style="padding-top:5px;">
        <?php
			frontimageUpload($this->user->get('company_logo'),$this->user->get('company_logo'),'company_logo');
		?>
        </div>
  	</td>
</tr>
</table>
</div><!--register-table-->

<div id="reg_agent_cont"><div id="reg_agent_bg">
<div id="reg_agent_header">Personal Particular</div>
</div></div>

</div>
<?php
}
?>

<div id="register_div">
<div id="register-table">
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="standard_table">
<tr>
	<td width="40%" height="60">
		<label id="salutemsg" for="salute">
			尊称 <?php echo JText::_( 'Salute' ); ?> * 
		</label>
        <div style="padding-top:5px;">
        <?php echo JHTML::_('select.genericlist',  $this->saluteOptions, 'salute', 'class="inputbox" size="1"', 'value', 'text', '' );?>
        </div>
	</td>
  	<td width="40%" height="60">
  		&nbsp;
  	</td>
</tr>
</table>


<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="agent_owner_table">
<tr>
	<td width="40%" height="60">
  		<label id="namemsg" for="name">
        	名 <?php echo JText::_( 'Name' ); ?> *
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="name" id="name" size="40" value="" class="inputbox" />
        </div>
  	</td>
    <?php
	if(($this->user_type == 'agent')||($this->user_type == 'owner')){
	?>
  	<td width="40%" height="60">
  		<label id="chinese_namemsg" for="chinese_name">
			中文名 <?php echo JText::_( 'Chinese Name' ); ?>
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="chinese_name" id="chinese_name" size="40" value="" class="inputbox" maxlength="50" />
        </div>
  	</td>
    <?php } else { ?>
    <td width="40%" height="60">&nbsp;</td>
    <?php
	}
	?>
</tr>
 <?php
	if(($this->user_type == 'agent')||($this->user_type == 'owner')){
	?>
<tr>
	<td width="40%" height="60">
		<label id="nricmsg" for="nric">
        身份证号 NRIC
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="nric" id="nric" size="40" value="" class="inputbox" maxlength="50" />
        </div>
	</td>
  	<td width="40%" height="60">
    	<div style="padding-top:12px;">
  		<label id="user_imagemsg" for="user_image">
			您的照片 <?php echo JText::_( 'Your Image' ); ?>
		</label>
        </div>
        <div style="padding-top:5px;">
        <?php
			frontimageUpload($this->user->get('user_image'),$this->user->get('user_image'),'user_image');
		?>

        </div>
  	</td>
</tr>
<?php }?>
</table>


<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="login_details_table">
<tr>
	<td width="40%" height="60">
		<label id="usernamemsg" for="username">
			邮箱地址 <?php echo JText::_( 'Email Address' ); ?> * (User Login ID)
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="username" id="username" size="40" value="" class="inputbox required validate-email" />
       
        </div>
	</td>
    <?php
	if($this->user_type == 'agent'){
	?>
	<td width="40%" height="60">
		<label id="cea_reg_nomsg" for="name">
        	经纪证号 <?php echo JText::_( 'CEA Registration No.' ); ?> *
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="cea_reg_no" id="cea_reg_no" size="40" value="" class="inputbox required" />
        </div>
	</td>
    <?php
	} else {
	?>
    <td width="40%" height="60">&nbsp;</td>
    <?php
	}
	?>
</tr>
<tr>
	<td width="40%" height="60">
		<label id="passwordmsg" for="password">
			密码 <?php echo JText::_( 'Password' ); ?> * (Min. 6 characters)
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox required validate-password" type="password" id="password" name="password" size="40" value="" />
        </div>
	</td>
  	<td width="40%" height="60">
		<label id="password2msg" for="password2">
			确认密码 <?php echo JText::_( 'Verify Password' ); ?> * (Min. 6 characters)
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox required validate-passverify" type="password" id="password2" name="password2" size="40" value="" />
        </div>
	</td>
</tr>
</table>

<?php
if($this->user_type == 'merchant'){
?>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="organization_table">
<tr>
	<td width="40%" height="60">
		<label id="organization_namemsg" for="organization_name">
			公司名 <?php echo JText::_( 'Organization Name' ); ?> *
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="organization_name" name="organization_name" size="40" value="" />
        </div>
	</td>
  	<td width="40%" height="60">
		<label id="organization_addressmsg" for="organization_address">
			公司地址 <?php echo JText::_( 'Organization Address' ); ?> *
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="organization_address" name="organization_address" size="40" value="" />
        </div>
	</td>
</tr>
</table>
<?php
}
?>

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="country_table">
<tr>
<td width="40%" height="60">
  		<label id="countrymsg" for="country">
        国籍 Country
		</label>
        <div style="padding-top:5px;">
        <?php echo JHTML::_('select.genericlist',  $this->countryOptions, 'country', 'class="inputbox" size="1"', 'value', 'text', '' );?>
        </div>
  	</td>
<td width="40%" height="60">
  <?php
if($this->user_type == 'merchant'){
?>
     <div id="phone1_div">
		<label id="phone_1msg" for="phone_1">
        联系电话1 Telephone 1
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="phone_1" id="phone_1" size="40" value="" class="inputbox" maxlength="50" />
        </div>
     </div>
     <?php
	 } else {
	 ?>
	<div id="mobile_div">
		<label id="mobile_contactmsg" for="mobile_contact">
        手提电话 Mobile Number *
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="mobile_contact" id="mobile_contact" size="40" value="" class="inputbox" maxlength="50" />
        </div>
     </div>
<?php
}
?>   
	</td>
</tr>
</table>
<?php
if(($this->user_type == 'agent')||($this->user_type == 'owner')){
?>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="personal_details_table">
<tr>
	<td width="40%" height="60">
		<label id="dobmsg" for="dob">
			出生日期 <?php echo JText::_( 'Date of Birth' ); ?>
		</label>
        <div style="padding-top:5px;">
        <?php echo $this->dayList;?>&nbsp;
		<?php echo $this->monthList;?>&nbsp;
        <?php echo $this->yearList;?>
        </div>
	</td>
    <td width="40%" height="60">
    <?php
if($this->user_type == 'owner'){
?>
		<div id="home_tel_table">
        <label id="home_contactmsg" for="home_contact">
			住家电话 <?php echo JText::_( 'Home Telephone' ); ?>
		</label>
        <div style="padding-top:5px;">
        <input type="text" name="home_contact" id="home_contact" size="40" value="" class="inputbox" maxlength="50" />
        </div>
        </div>
 <?php } else { ?>
        <div id="personal_table">
        <label id="websitemsg" for="website">
			个人网址 <?php echo JText::_( 'Personal Website' ); ?>
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="website" name="website" size="40" value="" />
        </div>
        </div>
<?php } ?>
	</td>
</tr>
</table>
<?php
}
?>
<?php
if($this->user_type == 'merchant'){
?>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="phone_table">
<tr>
<td width="40%" height="60">
		<label id="phone_2msg" for="phone_2">
			联系电话2 <?php echo JText::_( 'Telephone 2' ); ?>
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="phone_2" name="phone_2" size="40" value="" />
        </div>
	</td>
<td width="40%" height="60">
		<label id="faxmsg" for="fax">
			传真 <?php echo JText::_( 'Fax' ); ?>
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="fax" name="fax" size="40" value="" />
        </div>
	</td>
</tr>
<tr>
<td width="40%" height="60">
		<label id="merchant_interestmsg" for="merchant_interest">
			有兴趣 <?php echo JText::_( 'Interest In' ); ?>
		</label>
        <div style="padding-top:5px;">
        <?php echo JHTML::_('select.genericlist',  $this->merchantintOptions, 'merchant_interest', 'class="inputbox" size="1"', 'value', 'text', '' );?>
        </div>
	</td>
<td width="40%" height="60">&nbsp;
	
	</td>
</tr>
</table>
<?php
}
?>
<?php
if($this->user_type == 'individual'){
?>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="misc_table">
<tr>
<td width="40%" height="60">
		<label id="residence_statusmsg">
			居民状况 <?php echo JText::_( 'Residence Status' ); ?>
		</label>
        <div style="padding-top:5px;">
        <?php echo JHTML::_('select.genericlist',  $this->residenceOptions, 'residence_status', 'class="inputbox" size="1"', 'value', 'text', '' );?>
        </div>
	</td>
<td width="40%" height="60">
		<label id="user_interestmsg" for="user_interest">
			有兴趣 <?php echo JText::_( 'Interest In' ); ?>
		</label>
        <div style="padding-top:5px;">
        <?php echo JHTML::_('select.genericlist',  $this->userintOptions, 'user_interest', 'class="inputbox" size="1"', 'value', 'text', '' );?>
        </div>
	</td>
</tr>
</table>
<?php
}
?>
<?php
if(($this->user_type == 'agent')||($this->user_type == 'owner')){
?>

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="home_add_table">
<tr>
	<td width="40%" height="60">
		<label id="home_addressmsg" for="home_address">
        通讯地址 Home Address
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="home_address" name="home_address" size="40" value="" />
        </div>
	</td>
  	<td width="40%" height="60">
		<label id="home_postalcodemsg" for="home_postalcode">
        邮编 Home Postal Code
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="home_postalcode" name="home_postalcode" size="40" value="" />
        </div>
	</td>
</tr>
</table>
<?php
}
?>
<?php
if($this->user_type == 'owner'){
?>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="rental_add_table">
<tr>
<td width="40%" height="60">
		<label id="rental_addressmsg" for="rental_address">
			(出租物业) 地址 <?php echo JText::_( 'Rental Address' ); ?>
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="rental_address" name="rental_address" size="40" value="" />
        </div>
	</td>
<td width="40%" height="60">
		<label id="rental_postalcodemsg" for="rental_postalcode">
			(出租物业) 邮编 <?php echo JText::_( 'Rental Postal Code' ); ?>
		</label>
        <div style="padding-top:5px;">
        <input class="inputbox" type="text" id="rental_postalcode" name="rental_postalcode" size="40" value="" />
        </div>
	</td>
</tr>
</table>
<?php
}
?>

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane" id="receive_table">
<tr>
<td width="25%" height="60">
我要 Agree to receive
</td>
<td width="60%" height="60">
<input type="checkbox" name="receive[]" id="receive[]" value="ihouse360 Newsletter" checked="checked" />&nbsp;ihouse360 Newsletter&nbsp;&nbsp;&nbsp;<input type="checkbox" name="receive[]" id="receive[]" value="3rd Party Newsletter" checked="checked" />&nbsp;Third Party Newsletter<br /><input type="checkbox" name="receive[]" id="receive[]" value="Email Alert When Customer Enquiry" checked="checked" />Email Alert When Customer Enquiry
</td>
</tr>
</table>

<table>
<tr>
	<td colspan="2" height="40">
		<?php echo JText::_( '* Compulsary field' ); ?><br />
        <?php
if($this->user_type == 'agent'){
?>
    * 30 credits will be credit into his/her account once registering as an Agent is successful.
<?php
	}
?>
	</td>
</tr>

</table>
</div>

</div>

<div id="button_div" style="padding-left:5px;">
	<button class="button validate" type="submit"><?php echo JText::_('Register'); ?></button>
</div>
</div>



	<input type="hidden" name="option" id="option" value="com_user" />
	<input type="hidden" name="task" value="register_save" />
    <input type="hidden" name="Itemid" value="79" />
    <input type="hidden" id="email" name="email"  value=""/>
	<input type="hidden" name="id" value="0" />
	<input type="hidden" name="gid" value="0" />
    <input type="hidden" name="user_category" value="<?php echo ucfirst($this->user_type);?>" />
    <input type="hidden" name="lang_selection" id="lang_selection" value="" />
    <input type="hidden" name="user_type" id="user_type" value="<?php echo $this->user_type;?>" />
   
	<?php echo JHTML::_( 'form.token' ); ?>
<script>
function submit_frm()
{
	var userval = document.josForm.username.value;
	document.josForm.email.value=userval;

} 

</script>
</form>
</div><!--tabular1-->
</div><!--tablist-->
</div><!--start-->
