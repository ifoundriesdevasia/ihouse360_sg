<?php defined('_JEXEC') or die; ?>
<?php JHTML::_('script', 'ihouse.js', 'components/com_ads/rokbox/'); ?>
<?php JHTML::_('script', 'contacts.js', 'components/com_user/js/'); ?> 
<?php require_once( JPATH_ROOT.DS.'includes'.DS.'tools.php' ); ?>
<?php $userid = JRequest::getVar('id', 0); ?>
<script type="text/javascript">
function getAds(page, value, filter, orderfilter) {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();			
	var myArray = [];

	var url = 'index.php?option=com_user&task=ajaxLoadProfileAdslisting&r=' + unixtime_ms;		
	var req = new Ajax(url, {
	   					data		: 	
									{
										'user_category'	: value,
										'id'	: '<?php echo JRequest::getVar('id', 0) ?>',
										'filter'			: filter,
										'orderfilter'		: orderfilter,
										'page'			: page
										
									},
						method		: "get",
		    			onSuccess	: function(data) {
							$('ajax-adslisting-' + value).setHTML(data);
		   				},
						evalScripts : true
				}).request();				
}

function AdsRetrieveSearch(usercat ,uid){
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();			
	
	var url = 'index.php?option=com_user&task=loopBackSession&r=' + unixtime_ms;
	var req = new Ajax(url, {
	   					data		: 	
									{
										'usercat' : usercat,
										'user_id' : uid
									},
						method		: "post",
		    			onSuccess	: function(data) {
							var obj1 		= 	Json.evaluate(data);
							
							var page		=	obj1.page;
							var orderfilter	=	obj1.orderfilter;
							var filter		=	obj1.filter;
							
							//alert(page + ' ' + usercat + ' ' + filter + ' ' + orderfilter);
							getAds(page,usercat,filter,orderfilter);
		   				},
						evalScripts : true
				}).request();							
}

AdsRetrieveSearch('agent', '<?php echo $userid?>');

</script>
<!--Start View-->
<div style="margin-top:-9px;">
<div style="float:left; padding-bottom:20px;">
<div style="padding-bottom:10px; float:left; width:690px; background-color:#FFFFFF; border:solid 1px #e1e8ff;">
<div id="main-search-header-text" style="padding-left:15px;border-width:2px; line-height:30px;">会员中心&nbsp;<span style="font-size:15px;font-weight:100;"><i><?php echo $this->users->user_category ?>'s&nbsp;Profile</i></span></div>
<?php 
	if(empty($this->users->user_image)) {
	$user_image = JRoute::_('templates/main/images/no_avatar.jpg');
	}else{
	$user_image = 'image.php?size=120&type=1&path='.$this->users->user_image;
	}
	
	if(empty($this->users->company_logo)) {
	$company_logo = JRoute::_('templates/main/images/thumb-nologo.jpg');
	}else{
	$company_logo = 'image.php?size=120&type=1&path='.$this->users->company_logo;
	}
	
	$condo = explode(",", $this->sareas->condo);
	$condo_type = explode(",", $this->sareas->condo_type);
	$hdb = explode(",", $this->sareas->hdb);
	$hdb_type = explode(",", $this->sareas->hdb_type);
	$private_estate = explode(",", $this->sareas->private_estate);
	$industrial_estate = explode(",", $this->sareas->industrial_estate);
	
	$db = & JFactory::getDBO();
	for($a = 0; $a < (count($hdb_type)); $a++){
	$query = "SELECT * FROM #__ihouse_hdb_town WHERE serial_no = '".$hdb_type[$a]."'";
		$db->setQuery($query);
		$hdbtype[] = $db->loadRow();
	}
?>
<style type="text/css">
#tablist {
	border-left:3px solid #FF8200;
	border-bottom:3px solid #FF8200;
	border-right:3px solid #FF8200;
	width:99.1%;
	padding:0;
	margin:0;
	min-height:300px;
	height:auto;
	float:left;
}

table#prop_det_info td{
	border:1px solid #999;
}

ul.tab_nav5 {
list-style-image:none;
list-style-type:none;
}
</style>

<div style="padding:15px;"><!--start-->

<div id="menu_list_profile_detail">
<ul id="" class="tab_nav5">     
	<li id="tab_li5_profile" class="tabactive5"><a href="#"><span class="chinese_text_menu">会员资料</span></a></li>
</ul>   
</div>

<div id="tablist"> 
    <div id="tab_profile" class="tabular1">
    	<div style="padding:15px;">
            <div>
            	<div id="static-text">
                    	会员资料&nbsp;&nbsp;<font style="color:#494949;"><?php echo $this->users->user_category ?>'s&nbsp;Profile</font>
                    </div>
                    
                    <div style="float:left; width:630px; height:auto;">
                    <?php
					if(($this->users->user_category == 'Owner')||($this->users->user_category == 'Agent')){
					?>
                    <!--Left view-->
                    <div style="width:130px; float:left; padding-right:10px;">
                    	<div style="width:130px; height:auto; float:left;">
                            <img src="<?php echo $user_image; ?>" />
                        </div>
                        <div style="width:130px; height:auto; float:left; padding-top:5px;" align="center">
                            <img src="<?php echo $company_logo; ?>" />
                        </div>
                       
                    </div>
                    <!--End Left view-->
                    
                    <!--Right view-->
                    <div style="float:left; width:490px; height:auto;">
                   
					<?php
					}
					?>
                    <?php
						if($this->users->user_category == 'Agent'){
					?>
                    <div style="float:left; width:490px; height:auto;">
                    	<div style="float:left; width:160px; height:40px; border-right:dotted 2px #fcd29d; border-bottom:dotted 2px #fcd29d; padding-top:10px;">
                        	<div align="center" style="font-weight:bold;"><font color="#fb9116">经纪证号</font><br />CEA Registration No.</div>
                        </div>
                        
                        <div style="float:left; width:320px; border-bottom:dotted 2px #fcd29d;">
                        	<div style="padding-left:20px;  width:280px; height:30px; float:left; padding-top:20px;">
							<?php echo $this->users->cea_reg_no;?>
                            </div>
                        </div>
                    </div>
                    
                     <div style="float:left; width:490px; height:auto;">
                    	<div style="float:left; width:160px; height:40px; border-right:dotted 2px #fcd29d; border-bottom:dotted 2px #fcd29d; padding-top:10px;">
                        	<div align="center" style="font-weight:bold;"><font color="#fb9116">公司名称</font><br />Serving Organization</div>
                        </div>
                        
                        <div style="float:left; width:320px; border-bottom:dotted 2px #fcd29d;">
                        	<div style="padding-left:20px;  width:280px; height:30px; float:left; padding-top:20px;">
							<?php echo $this->users->company_name;?>
                            </div>
                        </div>
                    </div>
                     <?php
						}
					 ?>
                     
                     <div style="float:left; width:490px; height:auto;">
                    	<div style="float:left; width:160px; height:40px; border-right:dotted 2px #fcd29d; border-bottom:dotted 2px #fcd29d; padding-top:10px;">
                        	<div align="center" style="font-weight:bold;"><font color="#fb9116">姓名</font><br />Name</div>
                        </div>
                        
                        <div style="float:left; width:320px; border-bottom:dotted 2px #fcd29d;">
                        	<div style="padding-left:20px;  width:280px; height:30px; float:left; padding-top:20px;">
							<?php echo $this->users->name. ' ' . $this->users->chinese_name ?>
                            </div>
                        </div>
                    </div>
                    
                    <div style="float:left; width:490px; height:auto;">
                    	<div style="float:left; width:160px; height:40px; border-right:dotted 2px #fcd29d; border-bottom:dotted 2px #fcd29d; padding-top:10px;">
                        	<div align="center" style="font-weight:bold;"><font color="#fb9116">手机</font><br />Mobile Number</div>
                        </div>
                        
                        <div style="float:left; width:320px; border-bottom:dotted 2px #fcd29d;">
                        	<div style="padding-left:20px;  width:280px; height:30px; float:left; padding-top:20px;">
							<?php if($this->users->mobile_contact == '0'){ echo "Unknown"; } else { echo '(0065)'.$this->users->mobile_contact; } ?>
                            </div>
                        </div>
                    </div>
                    <?php
					if($this->users->user_category == 'Owner'){
					?>
                    <div style="float:left; width:490px; height:auto;">
                    	<div style="float:left; width:160px; height:40px; border-right:dotted 2px #fcd29d; border-bottom:dotted 2px #fcd29d; padding-top:10px;">
                        	<div align="center" style="font-weight:bold;"><font color="#fb9116">住家电话</font><br />Home Contact</div>
                        </div>
                        
                        <div style="float:left; width:320px; border-bottom:dotted 2px #fcd29d;">
                        	<div style="padding-left:20px;  width:280px; height:30px; float:left; padding-top:20px;">
							<?php if($this->users->home_contact == '0'){ echo "Unknown"; } else { echo $this->users->home_contact; } ?>
                            </div>
                        </div>
                    </div>
                    <?php
					}
					if($this->users->user_category == 'Agent'){
					?>
                    <div style="float:left; width:490px; height:auto;">
                    	<div style="float:left; width:160px; height:40px; border-right:dotted 2px #fcd29d; border-bottom:dotted 2px #fcd29d; padding-top:10px;">
                        	<div align="center" style="font-weight:bold;"><font color="#fb9116">网址</font><br />Website</div>
                        </div>
                        
                        <div style="float:left; width:320px; border-bottom:dotted 2px #fcd29d;">
                        	<div style="padding-left:20px;  width:280px; height:30px; float:left; padding-top:20px;">
							<?php if($this->users->website == ''){ echo "Unknown"; } else { echo "<a href='http://
".$this->users->website."' target='_blank'>".$this->users->website."</a>"; } ?>
                            </div>
                        </div>
                    </div>
                    
                    <?php
					}
					?>
                            
                    </div>
					<div style="width:620px; height:auto;">
                    	<div style="float:left; width:620px; height:auto; border-bottom:dotted 2px #fcd29d;">
                    	<div style="width:100%; height:40px; padding-top:5px;border-bottom:dotted 2px #fcd29d;">
                        	<div align="left" style="font-weight:bold;"><font color="#fb9116">个人简介</font><br />Description</div>
                        </div>
                        
                        <div style="float:left; width:310px; padding-bottom:20px;">
                        	<div style="padding-left:20px;  width:600px; height:auto; float:left; padding-top:20px;">
							<?php if($this->users->short_desc == ''){ echo "用户未填写个人简介资料."; } else { echo nl2br($this->users->short_desc); } ?>
                            </div>
                        </div>
                        </div>
                    </div>
                    <!--End Right view-->
                    
                     <?php
					if($this->users->user_category == "Agent"){
					?>
                    <div id="service_cont">
                    
                            <div id="service_header">
                            专注楼盘： The Guru
                            </div>
                            
                            <!--Start content-->
                            <div id="service_container">
                            	<div style="padding:10px;">
                            <?php if(!empty($this->gurus)) : ?>
                            <?php 	foreach($this->gurus as $g) : ?>
                            			<div>
                                        	<a style="text-decoration:none;color:#062284;font-weight:bold;" href="<?php echo JRoute::_('index.php?option=com_ihouse&view=ihouse&layout=property_detail&id='.$g->id.'&postcode='.$g->postcode) ?>"><?php echo ($g->name_ch)?$g->name_ch.' ':'' ?><?php echo $g->name_en ?></a>
                                        </div>
                            <?php 	endforeach; ?>
                            <?php else : ?>
                            	No Guru
                            <?php endif; ?>
                            	</div>
                            </div><!--service_cont-->    
                    
                    <div id="service_cont">
                    
                            <div id="service_header">
                            专注于 Service Provide
                            </div>
                            
                            <!--Start content-->
                            <div id="service_container">
                            	
                                <!--Start row-->
                                <div id="row_container">
                                
                                    <div id="left_column">公寓</div>
                                    
                                    <div id="right_column">
                                    	<div id="right_top">
                                        	<?php 
											if($condo[0] == '0'){
											echo 'N.A';
											} else { 
											for($a = 0; $a < (count($condo)-1); $a++){
											echo $condo[$a];
											echo " , ";
											}
											echo $condo[(count($condo) - 1)];
											}
											?>
                                        </div><!--right_top-->
                                        
                                        <div id="right_bottom">
                                        	<div id="right_bottom_left">类型</div>
                                            
                                            <div id="right_bottom_right">
                                            	<?php 
												if($condo[0] == '0'){
												echo 'N.A';
												} else { 
												for($a = 0; $a < (count($condo_type)-1); $a++){
												echo $condo_type[$a];
												echo " , ";
												}
												echo $condo_type[(count($condo_type) - 1)];
												}
												?>
                                            </div><!--right_bottom_right-->
                                        </div><!--right_bottom-->
                                    </div>
                                    <!--End right_column--> 
                                           
                                    </div>
                                    <!--End row-->
                                    
                                    <!--Start row-->
                                	<div id="row_container1">
                                    	<table width="623" border="0" cellpadding="0" cellspacing="0">
                                          <tr>
                                            <td align="left" valign="top" width="78" rowspan="2" style="border-right:solid 1px #e1e8ff; background-color:#f0f0f0; padding:10px; color:#fe8500; font-weight:bold; font-size:13px;">HDB</td>
                                            <td width="540" colspan="2" style="border-bottom:solid 1px #e1e8ff; padding:10px;">
                                            	<?php 
												if($hdb[0] == '0'){
												echo 'N.A';
												} else { 
												for($a = 0; $a < (count($hdb)-1); $a++){
												echo $hdb[$a];
												echo " , ";
												}
												echo $hdb[(count($hdb) - 1)];
												}
												?>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td width="30" style="border-right:solid 1px #e1e8ff; background-color:#f7f7f7; padding:10px; color:#fe8500; font-weight:bold; font-size:13px;">邮区</td>
                                            <td width="490" style="padding:10px;">
                                            	<?php 
												if($hdb[0] == '0'){
												echo 'N.A';
												} else { 
												for($a = 0; $a < (count($hdbtype)-1); $a++){
												
												if(strlen($hdbtype[$a][1]) == '1'){
												$num = "0".$hdbtype[$a][1];
												} else {
												$num = $hdbtype[$a][1];
												}
												?>
                                                <div style="float:left; width:210px; line-height:20px; font-size:13px;">
												<?php echo $num.". ".$hdbtype[$a][2]." (".$hdbtype[$a][3].")" ?>
                                                </div>
                                                <?php
												}
												if(strlen($hdbtype[(count($hdbtype) - 1)][1]) == '1'){
												$num1 = "0".$hdbtype[(count($hdbtype) - 1)][1];
												} else {
												$num1 = $hdbtype[(count($hdbtype) - 1)][1];
												}
												?>
                                                <div style="float:left; width:210px; line-height:20px; font-size:13px;">
												<?php echo $num1.". ".$hdbtype[(count($hdbtype) - 1)][2]." ".$hdbtype[(count($hdbtype) - 1)][3]; ?>
                                                </div>
                                                <?php
												}
												?>
                                            </td>
                                          </tr>
                                        </table>
                                    
                                    </div>

                                    <!--Start row-->
                                	<div id="row_container1">
                                
                                    	<div id="left_column1">有地住宅</div>
                                    
                                    <div id="right_column">
                                    	<div id="right_top">
                                        	<?php 
											if($private_estate[0] == '0'){
											echo 'N.A';
											} else { 
											for($a = 0; $a < (count($private_estate)-1); $a++){
											echo $private_estate[$a];
											echo " , ";
											}
											echo $private_estate[(count($private_estate) - 1)];
											}
											?>
                                        </div><!--right_top-->
                                    </div><!--End right_column-->     
                                    </div>
                                    <!--End row-->
                                    
                                    <!--Start row-->
                                	<div id="row_container1">
                                
                                    	<div id="left_column1">商业房产</div>
                                    
                                    <div id="right_column">
                                    	<div id="right_top">
                                        	<?php
											if($industrial_estate[0] == '0'){
											echo 'N.A';
											} else { 
											for($a = 0; $a < (count($industrial_estate)-1); $a++){
											echo $industrial_estate[$a];
											echo " , ";
											}
											echo $industrial_estate[(count($industrial_estate) - 1)];
											}
											?>
                                        </div><!--right_top-->
                                    </div><!--End right_column-->       
                                    </div>
                                    <!--End row-->
                                </div>
                                <!--End content-->
                           
                            </div><!--service_cont-->      
                    <?php 
					}
					?>
    
                    </div>

            </div>
        </div>
    </div><!--tabular1-->

</div>    

</div><!--End-->
</div>
</div>
</div>
<!--End View-->

<!--Start-->
<div style="width:690px;">
<div style="width:690px; float:left; background-color:#FFFFFF; border:solid 1px #e2e9fe;">


<div style="float:left; width:100%;"><!--Start of 2nd module-->
                                                
                  <div style="float:left; width:680px; height:auto;"> <!--Start of content-->
                    <?php /*?><div style="border-bottom:solid 1px #ccc; padding-bottom:10px; float:left; width:100%; height:auto; padding-left:10px;">
                        <div style="float:left; color:#3a3a3a; width:100px; line-height:20px;">
                        <input type="checkbox" />&nbsp;&nbsp;&nbsp;全选 / 不选
                        </div>
                        
                        <div style="float:left; width:300px;">
                        <input type="submit" value="比较" class="searchbutton" />&nbsp;
                        </div>
                        
                        <div style="float:left; line-height:25px;">
                        排序&nbsp;&nbsp;
                        <img src="<?php echo JRoute::_('templates/main/images/arrow.png'); ?>" />&nbsp;&nbsp;
                        <font style="color:#062284;">
                        户型&nbsp;&nbsp; | &nbsp;&nbsp;类型&nbsp;&nbsp; | &nbsp;&nbsp;价格 &nbsp;&nbsp; | &nbsp;&nbsp;更新日期
                        </font>
                        </div>
                        
                    </div><?php */?>
                    <div id="ajax-adslisting-agent">
                    
                	</div>
                    <?php //for($i= 0; $i < count($this->fads); $i++): ?>
                    <?php 
					/*if(empty($this->fads[$i]->user_image)) {
					$user_image = JRoute::_('templates/main/images/agentimg.jpg');
					}else{
					$user_image = 'image.php?size=90&type=1&path='.$this->fads[$i]->user_image;
					}*/
				
					?>
                    <?php /*?><div style="border-bottom:solid 1px #ccc; padding-bottom:10px; float:left; width:100%; height:auto; padding-left:10px; padding-top:20px; padding-bottom:20px; color:#5b5b5b;">
                        <div style="float:left; width:30px;">
                        <input type="checkbox" />
                        </div>
                        
                        <div style="float:left; width:170px;">
                        <?php if(!empty($this->fads[$i]->img_name)):?>
                        <img style="width:150px;" src="<?php echo JURI::root();?>/images/ihouse/ads_images/<?php echo $this->fads[$i]->sess_id;?>/<?php echo $this->fads[$i]->img_name;?>" />
                         <?php else : ?>
                         <img style="width:150px;" src="<?php echo JRoute::_('templates/main/images/thumb-noimage.jpg'); ?>" />
                         <?php endif; ?>
                        </div>
                        
                        <div id="tabtext-padding1" style="float:left; width:280px;" id="adsdetails_link">
                            <a href="index.php?option=com_ads&view=adsdetails&id=<?php echo $this->fads[$i]->id;?>&postcode=<?php echo $this->fads[$i]->postcode;?>"><?php echo $this->fads[$i]->property_name;?></a>
                            <div style="float:left; width:100%; line-height:18px;">
                                <div style="float:left; width:60px;">地址</div>
                                <div style="float:left; width:10px;">:</div>
                                <div style="float:left; width:200px;"><?php echo $this->fads[$i]->street_name;?></div>
                            </div>
                            
                            <div style="float:left; width:100%; line-height:18px;">
                                <div style="float:left; width:60px;">价格</div>
                                <div style="float:left; width:10px;">:</div>
                                <div style="float:left; width:200px;">SGD$ <?php echo formatMoney($this->fads[$i]->ask_price);?></div>
                            </div>
                            
                            <div style="float:left; width:100%; line-height:18px;">
                                <div style="float:left; width:60px;">均价</div>
                                <div style="float:left; width:10px;">:</div>
                                <div style="float:left; width:200px;">每平方米 SGD$ <?php echo formatMoney($this->fads[$i]->psm_price);?></div>
                            </div>
                            
                            <div style="float:left; width:100%; line-height:18px;">
                                <div style="float:left; width:60px;">面积</div>
                                <div style="float:left; width:10px;">:</div>
                                <div style="float:left; width:200px;"><?php echo $this->fads[$i]->size;?> 平方米</div>
                            </div>
                            
                            <div style="float:left; width:100%; line-height:18px;">
                                <div style="float:left; width:60px;">户型</div>
                                <div style="float:left; width:10px;">:</div>
                                <div style="float:left; width:200px;"><?php echo $this->fads[$i]->no_of_room;?>房<?php echo $this->fads[$i]->no_of_hall;?>厅</div>
                            </div>
                            
                            <div style="float:left; width:100%; line-height:18px;">
                                <div style="float:left; width:60px;">邮区</div>
                                <div style="float:left; width:10px;">:</div>
                                <div style="float:left; width:200px;"><?php echo $this->fads[$i]->postcode;?></div>
                            </div>
                            
                            <div style="float:left; width:100%; line-height:18px;">
                                <div style="float:left; width:60px;">更新日期</div>
                                <div style="float:left; width:10px;">:</div>
                                <div style="float:left; width:200px;">
                                <?php
								$currentdate = explode( "-" , $this->fads[$i]->posting_date);
								$cyear = $currentdate[0];
								$cmonth = $currentdate[1];
								$cday = substr($currentdate[2], 0, 2);
								?>
                                <?php echo $cyear; ?>年<?php echo $cmonth;?>月<?php echo $cday;?>日
                                </div>
                            </div>
                            
                            <div style="float:left; width:100%; line-height:18px;">
                                <div style="float:left; width:60px;">目前状态</div>
                                <div style="float:left; width:10px;">:</div>
                                <div style="float:left; width:200px;"><?php echo $this->fads[$i]->status_id;?></div>
                            </div>
                        </div>
                        
                        <div style="float:left; width:88px;">
                            <div>
                            <a href="index.php?option=com_user&view=agent_profile&id=<?php echo $this->fads[$i]->userid;?>&Itemid=136">
                            	<img src="<?php echo $user_image; ?>" />
                            </a>
                            </div>
                        </div>
                        
                        <div style="float:left; width:65px; padding-left:10px;">
                        <?php echo $this->fads[$i]->chinese_name;?><br /><?php echo $this->fads[$i]->mobile_contact;?>
                        </div>
                    </div><?php */?>
                <?php //endfor; ?>
                </div><!--End of content-->
            </div><!--End of 2nd module-->

</div>
</div>
<!--End-->   
<!-- POPUP -->
<div id="popupForm" style="display:none;">
</div>