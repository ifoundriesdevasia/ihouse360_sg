<?php
/**
 * @version		$Id: view.html.php 14401 2010-01-26 14:10:00Z louis $
 * @package		Joomla
 * @subpackage	User
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant to the
 * GNU General Public License, and as distributed it includes or is derivative
 * of works licensed under the GNU General Public License or other free or open
 * source software licenses. See COPYRIGHT.php for copyright notices and
 * details.
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * HTML View class for the Users component
 *
 * @package		Joomla
 * @subpackage	User
 * @since		1.5
 */
class UserViewAgent_profile extends JView
{
	/**
	 * Registry namespace prefix
	 *
	 * @var	string
	 */
	//var $_namespace	= 'com_user.reset.';

	/**
	 * Display function
	 *
	 * @since 1.5
	 */
	function display($tpl = null)
	{
		jimport('joomla.html.html');
		jimport( 'joomla.application.application' );


		global $mainframe;

		// Load the form validation behavior
		JHTML::_('behavior.formvalidation');

		// Add the tooltip behavior
		JHTML::_('behavior.tooltip');

		$user 	=& JFactory::getUser();
		
		$user_id = JRequest::getVar( 'id', '', 'get', 'id' );
		$session = JFactory::getSession();
		
		$session->set('sessionsave', 1);
		
		//$session->set('profile_page'.$user_id, 0);
		//$session->set('profile_orderfilter'.$user_id, 'DESC');
		//$session->set('profile_filter'.$user_id, 'a.posting_date');

		if ( $user->get('id') == $user_id) {
			
			$mainframe->redirect('index.php?option=com_user&view=profiler&Itemid=126');
		}

		if($this->hasVisitedByIP('profile', $user_id)) {
			$this->profileCounter($user_id);
		}

		$this->assignRef('items', $this->getAdsOfThisUser($user_id));
		$this->assignRef('users', $this->getInfoOfThisUser($user_id));
		$this->assignRef('sareas', $this->getSAOfThisUser($user_id));
		$this->assignRef('fads', $this->getFeaturedAdsDetails($user_id));
		$this->assignRef('gurus', $this->getGurus($user_id));

		parent::display($tpl);
	}
	function getGurus($id = 0) {
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT p.* FROM #__ihouse_ads_type AS a "
						.	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = a.subscription_id "
						.	" LEFT JOIN #__ihouse_property AS p ON p.id = a.property_id "
						.	" WHERE a.ads_type_config = 1 AND a.user_id = '$id' AND cb.status = 'A' " ;
						
			$db->setQuery( $query );
			$list = $db->loadObjectList();		
			
		return $list;	
	}
	function getInfoOfThisUser($id = 0) {
		
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT * FROM #__users "
						.	" WHERE id = '$id'" ;
						
			$db->setQuery( $query );
			$list = $db->loadObject();		
			//echo print_r($list);
		return $list;	
	}
	
	function getSAOfThisUser($id = 0) {
		
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT * FROM #__users_speciality_areas "
						.	" WHERE user_id = '$id'" ;
						
			$db->setQuery( $query );
			$list = $db->loadObject();		
			//echo print_r($list);
		return $list;	
	}
	
	function getAdsOfThisUser($id = 0) {
		if(!$id)
			return false;
		
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT a.ad_title, a.ad_type, p.name_en, p.name_ch, p.address, a.ask_price, a.posting_date, a.property_type , i.name,a.id AS ads_id FROM #__ihouse_ads AS a "
						.	" LEFT JOIN #__ihouse_property AS p ON p.postcode = a.postcode "
						.	" LEFT JOIN #__ihouse_ads_image AS i ON i.ads_id = a.id "
						.	" WHERE a.posted_by = '$id' ";
						
			$db->setQuery( $query );
			$lists = $db->loadObjectList();
			
		return $lists;
	}
	
	function getFeaturedAdsDetails($id = 0) {
		
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
		
	
		$query	=	'SELECT a.*,i.sess_id, i.name AS img_name, u.name, u.id AS userid, u.chinese_name, u.user_image,u.mobile_contact  '
							.	' FROM #__ihouse_ads AS a '
							.	' LEFT JOIN #__ihouse_ads_image AS i ON i.ads_id = a.id '
							.	' LEFT JOIN #__users AS u ON u.id = a.posted_by '
							.	' LEFT JOIN #__cbsubs_subscriptions AS c ON c.user_id = a.posted_by AND c.id = a.subscription_id '
							.   ' WHERE a.publish = "1" AND i.is_primary = "1" AND c.status = "A" AND a.posted_by = "'.$id.'"';

						
			$db->setQuery( $query );
			$list = $db->loadObjectList();
		//print_r($list);
		return $list;	
	}
	
	function hasVisitedByIP($type, $id) {
		global $mainframe;
		
		$ip = getenv('REMOTE_ADDR');
		
		$db 	  	=& 	JFactory::getDBO();
		
		if($type == 'profile') {
			$params	=	"profile=".$id;
		}
		
		$query	=	"SELECT id FROM #__ihouse_ip_log "
					. " WHERE ip = '$ip'  "
					. " AND type = '$type' "
					. " AND param LIKE '%".$params."%' ";

			$db->setQuery( $query ) ;
			$is_exist = $db->loadResult();		
		
		if($is_exist) 
			return false;
		
		if($type == 'profile') {
			
			$query	=	"INSERT INTO #__ihouse_ip_log VALUES(NULL, '$type','$ip','$params')";
			
				$db->setQuery( $query ) ;
				$db->query();
				
			return true;	
		}
			
	}
	
	function profileCounter($id = 0) {
		
		global $mainframe;
		
		$db 	  	=& 	JFactory::getDBO();
		
		if(!$id)
			return false;
		
		$query	=	" SELECT id FROM #__ihouse_popular2 "
					.	" WHERE aid = '$id' LIMIT 1" ;
			
			$db->setQuery( $query ) ;
			$is_exist	=	$db->loadResult();
		
		
		
		if($is_exist) {
			
			$query	=	"UPDATE #__ihouse_popular2 SET counter = counter + 1"
					.	" WHERE aid = '$id' AND type = 'profile' " ;
			
				$db->setQuery( $query ) ;
				$db->query();
			
		} else {
			
			$query	=	"INSERT INTO #__ihouse_popular2 VALUES(NULL, 'profile',1,'$id')";
			
				$db->setQuery( $query ) ;
				$db->query();
				
		}
		return true;
	}
}
