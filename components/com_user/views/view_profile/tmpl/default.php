﻿<?php defined('_JEXEC') or die; ?>
<?php JHTML::_('script', 'ihouse.js', 'components/com_ads/rokbox/'); ?>
<!--Start View-->
<div style="margin-top:-20px;">

<div id="main-search-header-text" style="padding-left:15px;border-width:2px;">会员中心&nbsp;<span style="font-size:15px;font-weight:100;"><i><?php echo $this->users->user_category ?>&nbsp;Centre</i></span></div>
<?php 
	if(empty($this->users->user_image)) {
	$user_image = JRoute::_('templates/main/images/no_avatar.jpg');
	}else{
	$user_image = 'image.php?size=120&type=1&path='.$this->users->user_image;
	}
	
	if(empty($this->users->company_logo)) {
	$company_logo = JRoute::_('templates/main/images/thumb-nologo.jpg');
	}else{
	$company_logo = 'image.php?size=120&type=1&path='.$this->users->company_logo;
	}
	
	$condo = explode(",", $this->sareas->condo);
	$condo_type = explode(",", $this->sareas->condo_type);
	$hdb = explode(",", $this->sareas->hdb);
	$hdb_type = explode(",", $this->sareas->hdb_type);
	$private_estate = explode(",", $this->sareas->private_estate);
	$industrial_estate = explode(",", $this->sareas->industrial_estate);
	
	$db = & JFactory::getDBO();
	for($a = 0; $a < (count($hdb_type)); $a++){
	$query = "SELECT * FROM #__ihouse_hdb_town WHERE serial_no = '".$hdb_type[$a]."'";
		$db->setQuery($query);
		$hdbtype[] = $db->loadRow();
	}
?>
<style type="text/css">
#tablist {
	border-left:3px solid #FF8200;
	border-bottom:3px solid #FF8200;
	border-right:3px solid #FF8200;
	width:99.1%;
	padding:0;
	margin:0;
	min-height:300px;
	height:auto;
	float:left;
}

table#prop_det_info td{
	border:1px solid #999;
}

ul.tab_nav5 {
list-style-image:none;
list-style-type:none;
}
</style>

<div style="padding:15px;"><!--start-->

<div id="menu_list_profile_detail">
<ul id="" class="tab_nav5">     
	<li id="tab_li5_profile" class="tabactive5"><a href="#"><span class="chinese_text_menu">个人资料</span></a></li>
</ul>   
</div>

<div id="tablist"> 
    <div id="tab_profile" class="tabular1">
    	<div style="padding:15px;">
            <div>
            	<div id="static-text">
                    	个人资料&nbsp;&nbsp;<font style="color:#494949;">Profile</font>
                    </div>
                    
                    <div style="float:left; width:630px; height:auto;">
                    <?php
					if(($this->users->user_category == 'Owner')||($this->users->user_category == 'Agent')){
					?>
                    <!--Left view-->
                    <div style="width:130px; float:left; padding-right:10px;">
                    	<div style="width:130px; height:auto; float:left;">
                            <img src="<?php echo $user_image; ?>" />
                        </div>
                        <div style="width:130px; height:auto; float:left; padding-top:5px;">
                            <img src="<?php echo $company_logo; ?>" />
                        </div>
                        
                        <div id="profile_menu">
                            <input type="button" value="Edit Profile" class="button9022" onClick="location.href='index.php?option=com_user&view=edit_profile&Itemid=126&id=<?php echo $this->users->id;?>'" />
                        </div>
                    </div>
                    <!--End Left view-->
                    
                    <!--Right view-->
                    <div style="float:left; width:490px; height:auto;">
                   
					<?php
					}
					?>
                    <?php
						if($this->users->user_category == 'Agent'){
					?>
                    <div style="float:left; width:490px; height:auto;">
                    	<div style="float:left; width:160px; height:40px; border-right:dotted 2px #fcd29d; border-bottom:dotted 2px #fcd29d; padding-top:10px;">
                        	<div align="center" style="font-weight:bold;"><font color="#fb9116">经纪证号</font><br />CEA Registration No.</div>
                        </div>
                        
                        <div style="float:left; width:320px; border-bottom:dotted 2px #fcd29d;">
                        	<div style="padding-left:20px;  width:280px; height:30px; float:left; padding-top:20px;">
							<?php echo $this->users->cea_reg_no;?>
                            </div>
                        </div>
                    </div>
                    
                     <div style="float:left; width:490px; height:auto;">
                    	<div style="float:left; width:160px; height:40px; border-right:dotted 2px #fcd29d; border-bottom:dotted 2px #fcd29d; padding-top:10px;">
                        	<div align="center" style="font-weight:bold;"><font color="#fb9116">公司名称</font><br />Serving Organization</div>
                        </div>
                        
                        <div style="float:left; width:320px; border-bottom:dotted 2px #fcd29d;">
                        	<div style="padding-left:20px;  width:280px; height:30px; float:left; padding-top:20px;">
							<?php echo $this->users->company_name;?>
                            </div>
                        </div>
                    </div>
                     <?php
						}
					 ?>
                     
                     <div style="float:left; width:490px; height:auto;">
                    	<div style="float:left; width:160px; height:40px; border-right:dotted 2px #fcd29d; border-bottom:dotted 2px #fcd29d; padding-top:10px;">
                        	<div align="center" style="font-weight:bold;"><font color="#fb9116">姓名</font><br />Name</div>
                        </div>
                        
                        <div style="float:left; width:320px; border-bottom:dotted 2px #fcd29d;">
                        	<div style="padding-left:20px;  width:280px; height:30px; float:left; padding-top:20px;">
							<?php echo $this->users->name. ' ' . $this->users->chinese_name ?>
                            </div>
                        </div>
                    </div>
                    
                    <div style="float:left; width:490px; height:auto;">
                    	<div style="float:left; width:160px; height:40px; border-right:dotted 2px #fcd29d; border-bottom:dotted 2px #fcd29d; padding-top:10px;">
                        	<div align="center" style="font-weight:bold;"><font color="#fb9116">手机</font><br />Mobile Number</div>
                        </div>
                        
                        <div style="float:left; width:320px; border-bottom:dotted 2px #fcd29d;">
                        	<div style="padding-left:20px;  width:280px; height:30px; float:left; padding-top:20px;">
							<?php if($this->users->mobile_contact == '0'){ echo "Unknown"; } else { echo $this->users->mobile_contact; } ?>
                            </div>
                        </div>
                    </div>
                    <?php
					if($this->users->user_category == 'Owner'){
					?>
                    <div style="float:left; width:490px; height:auto;">
                    	<div style="float:left; width:160px; height:40px; border-right:dotted 2px #fcd29d; border-bottom:dotted 2px #fcd29d; padding-top:10px;">
                        	<div align="center" style="font-weight:bold;"><font color="#fb9116">住家电话</font><br />Home Contact</div>
                        </div>
                        
                        <div style="float:left; width:320px; border-bottom:dotted 2px #fcd29d;">
                        	<div style="padding-left:20px;  width:280px; height:30px; float:left; padding-top:20px;">
							<?php if($this->users->home_contact == '0'){ echo "Unknown"; } else { echo $this->users->home_contact; } ?>
                            </div>
                        </div>
                    </div>
                    <?php
					}
					if($this->users->user_category == 'Agent'){
					?>
                    <div style="float:left; width:490px; height:auto;">
                    	<div style="float:left; width:160px; height:40px; border-right:dotted 2px #fcd29d; border-bottom:dotted 2px #fcd29d; padding-top:10px;">
                        	<div align="center" style="font-weight:bold;"><font color="#fb9116">网址</font><br />Website</div>
                        </div>
                        
                        <div style="float:left; width:320px; border-bottom:dotted 2px #fcd29d;">
                        	<div style="padding-left:20px;  width:280px; height:30px; float:left; padding-top:20px;">
							<?php if($this->users->website == ''){ echo "Unknown"; } else { echo $this->users->website; } ?>
                            </div>
                        </div>
                    </div>
                    
                    <div style="float:left; width:480px; height:auto;">
                    	<div style="float:left; width:480px; height:auto; border-bottom:dotted 2px #fcd29d;">
                    	<div style="float:left; width:160px; height:auto; padding-top:10px;">
                        	<div align="center" style="font-weight:bold;"><font color="#fb9116">简介</font><br />Description</div>
                        </div>
                        
                        <div style="float:left; width:310px; border-left:dotted 2px #fcd29d; padding-bottom:20px;">
                        	<div style="padding-left:20px;  width:280px; height:auto; float:left; padding-top:20px;">
							<?php if($this->users->short_desc == ''){ echo "Unknown"; } else { echo $this->users->short_desc; } ?>
                            </div>
                        </div>
                        </div>
                    </div>
                    <?php
					}
					?>
                            
                    </div>
                    <!--End Right view-->
                    
                     <?php
					if($this->users->user_category == "Agent"){
					?>
                    <div id="service_cont">
                    
                            <div id="service_header">
                            专注于 Service Provide
                            </div>
                            
                            <!--Start content-->
                            <div id="service_container">
                            	
                                <!--Start row-->
                                <div id="row_container">
                                
                                    <div id="left_column">公寓</div>
                                    
                                    <div id="right_column">
                                    	<div id="right_top">
                                        	<?php 
											if($condo[0] == '0'){
											echo 'N.A';
											} else { 
											for($a = 0; $a < (count($condo)-1); $a++){
											echo $condo[$a];
											echo " , ";
											}
											echo $condo[(count($condo) - 1)];
											}
											?>
                                        </div><!--right_top-->
                                        
                                        <div id="right_bottom">
                                        	<div id="right_bottom_left">类型</div>
                                            
                                            <div id="right_bottom_right">
                                            	<?php 
												if($condo[0] == '0'){
												echo 'N.A';
												} else { 
												for($a = 0; $a < (count($condo_type)-1); $a++){
												echo $condo_type[$a];
												echo " , ";
												}
												echo $condo_type[(count($condo_type) - 1)];
												}
												?>
                                            </div><!--right_bottom_right-->
                                        </div><!--right_bottom-->
                                    </div>
                                    <!--End right_column--> 
                                           
                                    </div>
                                    <!--End row-->
                                    
                                    <!--Start row-->
                                	<div id="row_container1">
                                    	<table width="623" border="0" cellpadding="0" cellspacing="0">
                                          <tr>
                                            <td align="left" valign="top" width="78" rowspan="2" style="border-right:solid 1px #e1e8ff; background-color:#f0f0f0; padding:10px; color:#fe8500; font-weight:bold; font-size:13px;">HDB</td>
                                            <td width="540" colspan="2" style="border-bottom:solid 1px #e1e8ff; padding:10px;">
                                            	<?php 
												if($hdb[0] == '0'){
												echo 'N.A';
												} else { 
												for($a = 0; $a < (count($hdb)-1); $a++){
												echo $hdb[$a];
												echo " , ";
												}
												echo $hdb[(count($hdb) - 1)];
												}
												?>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td width="30" style="border-right:solid 1px #e1e8ff; background-color:#f7f7f7; padding:10px; color:#fe8500; font-weight:bold; font-size:13px;">邮区</td>
                                            <td width="490" style="padding:10px;">
                                            	<?php 
												if($hdb[0] == '0'){
												echo 'N.A';
												} else { 
												for($a = 0; $a < (count($hdbtype)-1); $a++){
												
												if(strlen($hdbtype[$a][1]) == '1'){
												$num = "0".$hdbtype[$a][1];
												} else {
												$num = $hdbtype[$a][1];
												}
												?>
                                                <div style="float:left; width:210px; line-height:20px; font-size:13px;">
												<?php echo $num.". ".$hdbtype[$a][2]." (".$hdbtype[$a][3].")" ?>
                                                </div>
                                                <?php
												}
												if(strlen($hdbtype[(count($hdbtype) - 1)][1]) == '1'){
												$num1 = "0".$hdbtype[(count($hdbtype) - 1)][1];
												} else {
												$num1 = $hdbtype[(count($hdbtype) - 1)][1];
												}
												?>
                                                <div style="float:left; width:210px; line-height:20px; font-size:13px;">
												<?php echo $num1.". ".$hdbtype[(count($hdbtype) - 1)][2]." ".$hdbtype[(count($hdbtype) - 1)][3]; ?>
                                                </div>
                                                <?php
												}
												?>
                                            </td>
                                          </tr>
                                        </table>
                                    
                                    </div>

                                    <!--Start row-->
                                	<div id="row_container1">
                                
                                    	<div id="left_column1">有地住宅</div>
                                    
                                    <div id="right_column">
                                    	<div id="right_top">
                                        	<?php 
											if($private_estate[0] == '0'){
											echo 'N.A';
											} else { 
											for($a = 0; $a < (count($private_estate)-1); $a++){
											echo $private_estate[$a];
											echo " , ";
											}
											echo $private_estate[(count($private_estate) - 1)];
											}
											?>
                                        </div><!--right_top-->
                                    </div><!--End right_column-->     
                                    </div>
                                    <!--End row-->
                                    
                                    <!--Start row-->
                                	<div id="row_container1">
                                
                                    	<div id="left_column1">商业房产</div>
                                    
                                    <div id="right_column">
                                    	<div id="right_top">
                                        	<?php
											if($industrial_estate[0] == '0'){
											echo 'N.A';
											} else { 
											for($a = 0; $a < (count($industrial_estate)-1); $a++){
											echo $industrial_estate[$a];
											echo " , ";
											}
											echo $industrial_estate[(count($industrial_estate) - 1)];
											}
											?>
                                        </div><!--right_top-->
                                    </div><!--End right_column-->       
                                    </div>
                                    <!--End row-->
                                </div>
                                <!--End content-->
                           
                            </div><!--service_cont-->      
                    <?php 
					}
					?>
    
                    </div>

            </div>
        </div>
    </div><!--tabular1-->

</div>    

</div><!--End-->



</div>
<!--End View-->
