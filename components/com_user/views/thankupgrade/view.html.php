﻿<?php
/**
 * @version		$Id: view.html.php 11646 2009-03-01 19:34:56Z ian $
 * @package		Joomla
 * @subpackage	Users
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Form component
 *
 * @static
 * @package		Joomla
 * @subpackage	Thank Payment
 * @since 1.0
 */
class UserViewThankupgrade extends JView
{
	function display($tpl = null)
	{
		global $mainframe;
		$rid = JRequest::getVar( 'rid', '', 'get', 'rid' );
		$sid = JRequest::getVar( 'sid', '', 'get', 'sid' );
		$id		= substr($sid, 3, -3);
		
		
		
		$db = & JFactory::getDBO();
	
		$query="SELECT * FROM #__users WHERE id = ".$id;
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		
		if($rid == '3'){ //if is register
	$body = "
	Hello ".$rows[0]->name.",<br /><br />
 
Thank you for registering at iHouse360. Your account is created and must be activated before you can use it.<br /><br />
To activate the account click on the following link or copy-paste it in your browser:<br />
<a href='".JURI::root()."index.php?option=com_user&task=activate&activation=".$rows[0]->activation."'>".JURI::root()."index.php?option=com_user&task=activate&activation=".$rows[0]->activation."</a><br /><br />
 
After activation you may login to <a href='".JURI::root()."'>".JURI::root()."</a> using the username and password that you have created.
	
	
	";
	
	$config     = &JFactory::getConfig();
	$from       = $config->getValue('mailfrom');
	$fromname = $config->getValue('fromname');
	
	$mail = JFactory::getMailer();
	
	$mail->addRecipient( $rows[0]->email );
	$mail->setSender( array( $from , $fromname ) );
	$mail->setSubject( 'Account Details for '.$rows[0]->name.' at iHouse360');
	$mail->setBody( $body );
	$mail->IsHTML(true);
	
	 //Send the Mail
	$sent = $mail->Send(); 
	
	
	
	$body1 = "
	Hello Administrator,<br /><br />
 
A new user has registered at iHouse360 .<br /><br />
This e-mail contains their details: <br /><br />
Name:  ".$rows[0]->name."<br />
E-mail:  ".$rows[0]->email."<br />
Username: ".$rows[0]->username."<br /><br />
 
Please do not respond to this message. It is automatically generated and is for information purposes only.
	";
	
	$config     = &JFactory::getConfig();
	$from       = $config->getValue('mailfrom');
	$fromname = $config->getValue('fromname');
	
	$mail1 = JFactory::getMailer();
	
	$mail1->addRecipient( $from );
	$mail1->setSender( array( $from , $fromname ) );
	$mail1->setSubject( 'Account Details for '.$rows[0]->name.' at iHouse360');
	$mail1->setBody( $body1 );
	$mail1->IsHTML(true);
	
	 //Send the Mail
	$sent = $mail1->Send(); 
	
	}
			$this->assignRef('rows', $rows );
			$this->assignRef('rid', $rid );
			$this->assignRef('id', $id );
			parent::display($tpl);
	
	}
	
}