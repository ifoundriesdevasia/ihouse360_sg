<?php
/**
 * @version		$Id: view.html.php 14401 2010-01-26 14:10:00Z louis $
 * @package		Joomla
 * @subpackage	User
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant to the
 * GNU General Public License, and as distributed it includes or is derivative
 * of works licensed under the GNU General Public License or other free or open
 * source software licenses. See COPYRIGHT.php for copyright notices and
 * details.
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * HTML View class for the Users component
 *
 * @package		Joomla
 * @subpackage	User
 * @since		1.5
 */
class UserViewProfiler extends JView
{
	/**
	 * Registry namespace prefix
	 *
	 * @var	string
	 */
	var $_namespace	= 'com_user.reset.';

	/**
	 * Display function
	 *
	 * @since 1.5
	 */
	function display($tpl = null)
	{
		jimport('joomla.html.html');

		global $mainframe;

		// Load the form validation behavior
		JHTML::_('behavior.formvalidation');

		// Add the tooltip behavior
		JHTML::_('behavior.tooltip');

		// Get the layout
		$layout	= $this->getLayout();
		
		$user 	=& JFactory::getUser();
		$user_id = $user->get('id');
		
/*
		if ($layout == 'complete')
		{
			$id		= $mainframe->getUserState($this->_namespace.'id');
			$token	= $mainframe->getUserState($this->_namespace.'token');

			if (is_null($id) || is_null($token))
			{
				$mainframe->redirect('index.php?option=com_user&view=reset');
			}
		}

		// Get the page/component configuration
		$params = &$mainframe->getParams();

		$menus	= &JSite::getMenu();
		$menu	= $menus->getActive();

		// because the application sets a default page title, we need to get it
		// right from the menu item itself
		if (is_object( $menu )) {
			$menu_params = new JParameter( $menu->params );
			if (!$menu_params->get( 'page_title')) {
				$params->set('page_title',	JText::_( 'FORGOT_YOUR_PASSWORD' ));
			}
		} else {
			$params->set('page_title',	JText::_( 'FORGOT_YOUR_PASSWORD' ));
		}
		*/
		//$document	= &JFactory::getDocument();
		//$document->setTitle( $params->get( 'page_title' ) );
		//$user_id	=	JRequest::getCmd('id');
		$this->assignRef('howmanyviews', $this->getHowManyViews($user_id));
		$this->assignRef('items', $this->getAdsOfThisUser($user_id));
		$this->assignRef('users', $this->getInfoOfThisUser($user_id));
		$this->assignRef('sareas', $this->getSAOfThisUser($user_id));
		//$this->assignRef('params',		$params);

		parent::display($tpl);
	}
	function getHowManyViews($id = 0) {
		
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT counter FROM #__ihouse_popular2 "
						.	" WHERE aid = '$id' AND type = 'profile' LIMIT 1 " ;
						
			$db->setQuery( $query );
			$total = $db->loadResult();		
		
		if(empty($total))
			$total = 0;
		
		return $total;	
	}
	function getInfoOfThisUser($id = 0) {
		
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT * FROM #__users "
						.	" WHERE id = '$id'" ;
						
			$db->setQuery( $query );
			$list = $db->loadObject();		
			//echo print_r($list);
		return $list;	
	}
	
	function getSAOfThisUser($id = 0) {
		
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT * FROM #__users_speciality_areas "
						.	" WHERE user_id = '$id'" ;
						
			$db->setQuery( $query );
			$list = $db->loadObject();		
			//echo print_r($list);
		return $list;	
	}
	
	function getAdsOfThisUser($id = 0) {
		if(!$id)
			return false;
		
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT a.ad_title, a.ad_type, p.name_en, p.name_ch, p.address, a.ask_price, a.posting_date, a.property_type , i.name,a.id AS ads_id FROM #__ihouse_ads AS a "
						.	" LEFT JOIN #__ihouse_property AS p ON p.postcode = a.postcode "
						.	" LEFT JOIN #__ihouse_ads_image AS i ON i.ads_id = a.id "
						.	" WHERE a.posted_by = '$id' ";
						
			$db->setQuery( $query );
			$lists = $db->loadObjectList();
			
		return $lists;
	}
}
