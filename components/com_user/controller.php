<?php
/**
 * @version		$Id: controller.php 16385 2010-04-23 10:44:15Z ian $
 * @package		Joomla
 * @subpackage	Content
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant to the
 * GNU General Public License, and as distributed it includes or is derivative
 * of works licensed under the GNU General Public License or other free or open
 * source software licenses. See COPYRIGHT.php for copyright notices and
 * details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport('joomla.application.component.controller');

/**
 * User Component Controller
 *
 * @package		Joomla
 * @subpackage	Weblinks
 * @since 1.5
 */
class UserController extends JController
{
	/**
	 * Method to display a view
	 *
	 * @access	public
	 * @since	1.5
	 */
	function display()
	{
		parent::display();
	}

	function edit()
	{
		global $mainframe, $option;

		$db		=& JFactory::getDBO();
		$user	=& JFactory::getUser();

		if ( $user->get('guest')) {
			JError::raiseError( 403, JText::_('Access Forbidden') );
			return;
		}

		JRequest::setVar('layout', 'form');

		parent::display();
	}

	function save()
	{
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$user	 =& JFactory::getUser();
		$userid = JRequest::getVar( 'id', 0, 'post', 'int' );

		// preform security checks
		if ($user->get('id') == 0 || $userid == 0 || $userid <> $user->get('id')) {
			JError::raiseError( 403, JText::_('Access Forbidden') );
			return;
		}

		//clean request
		$post = JRequest::get( 'post' );
		$post['username']	= JRequest::getVar('username', '', 'post', 'username');
		$post['password']	= JRequest::getVar('password', '', 'post', 'string', JREQUEST_ALLOWRAW);
		$post['password2']	= JRequest::getVar('password2', '', 'post', 'string', JREQUEST_ALLOWRAW);
	
		// get the redirect
		$return = JURI::base();
		
		// do a password safety check
		if(strlen($post['password']) || strlen($post['password2'])) { // so that "0" can be used as password e.g.
			if($post['password'] != $post['password2']) {
				$msg	= JText::_('PASSWORDS_DO_NOT_MATCH');
				// something is wrong. we are redirecting back to edit form.
				// TODO: HTTP_REFERER should be replaced with a base64 encoded form field in a later release
				$return = str_replace(array('"', '<', '>', "'"), '', @$_SERVER['HTTP_REFERER']);
				if (empty($return) || !JURI::isInternal($return)) {
					$return = JURI::base();
				}
				$this->setRedirect($return, $msg, 'error');
				return false;
			}
		}

		// we don't want users to edit certain fields so we will unset them
		unset($post['gid']);
		unset($post['block']);
		unset($post['usertype']);
		unset($post['registerDate']);
		unset($post['activation']);

		// store data
		$model = $this->getModel('user');

		if ($model->store($post)) {
			$msg	= JText::_( 'Your settings have been saved.' );
		} else {
			//$msg	= JText::_( 'Error saving your settings.' );
			$msg	= $model->getError();
		}

		
		$this->setRedirect( $return, $msg );
	}

	function cancel()
	{
		$this->setRedirect( 'index.php' );
	}

	function login()
	{
		// Check for request forgeries
		JRequest::checkToken('request') or jexit( 'Invalid Token' );

		global $mainframe;
		
		if ($return = JRequest::getVar('return', '', 'method', 'base64')) {
			$return = base64_decode($return);
			if (!JURI::isInternal($return)) {
				$return = '';
			}
		}

		$options = array();
		$options['remember'] = JRequest::getBool('remember', false);
		$options['return'] = $return;

		$credentials = array();
		$credentials['username'] = JRequest::getVar('username', '', 'method', 'username');
		$credentials['password'] = JRequest::getString('passwd', '', 'post', JREQUEST_ALLOWRAW);

		//preform the login action
		$error = $mainframe->login($credentials, $options);

		if(!JError::isError($error))
		{
			// Redirect if the return url is not registration or login
			if ( ! $return ) {
				$return	= 'index.php?option=com_user&view=profiler';
			}

			$mainframe->redirect( $return );
		}
		else
		{
			// Facilitate third party login forms
			if ( ! $return ) {
				
				$return	= 'index.php?option=com_user&view=login&Itemid=80&msg=1';
			} else {
				
				$return = 'index.php?option=com_user&view=login&Itemid=80&msg=1';
			}

			// Redirect to a login form
			$mainframe->redirect( $return);
		}
		
	}

	function logout()
	{
		global $mainframe;

		//preform the logout action
		$error = $mainframe->logout();

		if(!JError::isError($error))
		{
			if ($return = JRequest::getVar('return', '', 'method', 'base64')) {
				$return = base64_decode($return);
				if (!JURI::isInternal($return)) {
					$return = '';
				}
			}

			// Redirect if the return url is not registration or login
			if ( $return && !( strpos( $return, 'com_user' )) ) {
				$mainframe->redirect( $return );
			}
		} else {
			parent::display();
		}
	}
	
	/*function loadForm() {
		global $mainframe;
		
		$lang	=	JRequest::getVar('lang', 'ch');
		$user_type	=	JRequest::getVar('user_type', 'user_type');

		if($lang == 'ch') 
			require('views/reg_form/tmpl/subs_ch.php');
		else if($lang == 'en')
			require('views/reg_form/tmpl/subs_en.php');
		$mainframe->close;
		exit;
	}*/

	/**
	 * Prepares the registration form
	 * @return void
	 */
	function register()
	{
		$usersConfig = &JComponentHelper::getParams( 'com_users' );
		if (!$usersConfig->get( 'allowUserRegistration' )) {
			JError::raiseError( 403, JText::_( 'Access Forbidden' ));
			return;
		}

		$user 	=& JFactory::getUser();

		if ( $user->get('guest')) {
			JRequest::setVar('view', 'register');
		} else {
			$this->setredirect('index.php?option=com_user&task=edit',JText::_('You are already registered.'));
		}

		parent::display();
	}

	/**
	 * Save user registration and notify users and admins if required
	 * @return void
	 */
	function register_save()
	{
		global $mainframe;
		require_once( JPATH_COMPONENT.DS.'image.php' );
		require_once( JPATH_ROOT.DS.'includes'.DS.'tools.php' );
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );

		// Get required system objects
		$db = & JFactory::getDBO();
		$user 		= clone(JFactory::getUser());
		$pathway 	=& $mainframe->getPathway();
		$config		=& JFactory::getConfig();
		$authorize	=& JFactory::getACL();
		$document   =& JFactory::getDocument();

		// If user registration is not allowed, show 403 not authorized.
		$usersConfig = &JComponentHelper::getParams( 'com_users' );
		if ($usersConfig->get('allowUserRegistration') == '0') {
			JError::raiseError( 403, JText::_( 'Access Forbidden' ));
			return;
		}

		// Initialize new usertype setting
		$newUsertype = $usersConfig->get( 'new_usertype' );
		if (!$newUsertype) {
			$newUsertype = 'Registered';
		}
		
		
		$lang_selection = JRequest::getVar('lang_selection', '', 'post', 'lang_selection');
		
		
		$user_category = JRequest::getVar('user_category', '', 'post', 'user_category');
		$receive = JRequest::getVar( 'receive', array(0), '', 'array' );
		
		$day = JRequest::getVar('day', '', 'post', 'day');
		$month = JRequest::getVar('month', '', 'post', 'month');
		$year = JRequest::getVar('year', '', 'post', 'year');
		

		if(($day == 'dd')||($month== 'mm')||($year == 'yyyy')){
		$dob = '0000-00-00';
		} else {
		$dob = date('Y-m-d' ,mktime(0, 0, 0, $month, $day, $year));
		}

		if($_FILES['company_logo']['name']){
		$post['company_logo'] = saveFileUpload($_FILES['company_logo']['name'],$_FILES['company_logo']['type'],$_FILES['company_logo']['tmp_name'],1);
		}

		if($_FILES['user_image']['name']){
		$post['user_image'] = saveFileUpload($_FILES['user_image']['name'],$_FILES['user_image']['type'],$_FILES['user_image']['tmp_name'],1);
		}
		
		
		$post['agree_to_receive'] = implode(",", $receive);

		$user->set('dob', $dob);
		$user->set('company_logo', $post['company_logo']);
		$user->set('user_image', $post['user_image']);
		$user->set('agree_to_receive', $post['agree_to_receive']);

		// Bind the post array to the user object
		if (!$user->bind( JRequest::get('post'), 'usertype' )) {
			JError::raiseError( 500, $user->getError());
		}

		// Set some initial user values
		$user->set('id', 0);
		$user->set('usertype', $newUsertype);
		$user->set('gid', $authorize->get_group_id( '', $newUsertype, 'ARO' ));

		$date =& JFactory::getDate();
		$user->set('registerDate', $date->toMySQL());

		// If user activation is turned on, we need to set the activation information
		$useractivation = $usersConfig->get( 'useractivation' );
		if ($useractivation == '1')
		{
			jimport('joomla.user.helper');
			$user->set('activation', JUtility::getHash( JUserHelper::genRandomPassword()) );
			$user->set('block', '1');
		}

		// If there was an error with registration, set the message and display form
		if ( !$user->save() )
		{
			JError::raiseWarning('', JText::_( $user->getError()));
			$this->register();
			return false;
		}

		
		
		
		if($user_category == 'Agent'){

		$this->setRedirect('index.php?option=com_user&view=paymentprocess&Itemid=126&sid='.getrandom().$user->get('id').getrandom(), $message);
		
		} else {
		// Send registration confirmation mail
		$password = JRequest::getString('password', '', 'post', JREQUEST_ALLOWRAW);
		$password = preg_replace('/[\x00-\x1F\x7F]/', '', $password); //Disallow control chars in the email
		UserController::_sendMail($user, $password);

		// Everything went fine, set relevant message depending upon user activation state and display message
		if ( $useractivation == 1 ) {
			$message  = JText::_( 'REG_COMPLETE_ACTIVATE' );
		} else {
			$message = JText::_( 'REG_COMPLETE' );
		}

		//print_r($user);
		$this->setRedirect('index.php?option=com_user&view=thankpayment&Itemid=79&sid='.getrandom().$user->get('id').getrandom());
		}
	}
	
	function act_request()
	{
		
		JRequest::setVar('view', 'request_activation');
		parent::display();
	}
	
	
	
	function profiler()
	{
		$user 	=& JFactory::getUser();

		if ( $user->get('guest')) {
			$this->setredirect('index.php?option=com_user&view=login&Itemid=80');
		} else {
			JRequest::setVar('view', 'profiler');
		}
	
		parent::display();
	}
	
	function edit_profile()
	{
		$user 	=& JFactory::getUser();

		if ( $user->get('guest')) {
			$this->setredirect('index.php?option=com_user&view=login&Itemid=80');
		} else {
			JRequest::setVar('view', 'edit_profile');
		}
	
		parent::display();
	}
	
	/*function account_info()
	{
		$user	=& JFactory::getUser();

		if ( $user->get('guest')) {
			JError::raiseError( 403, JText::_('Access Forbidden') );
			return;
		}
		
	JRequest::setVar('view', 'account_info');
	
		parent::display();
	}*/
	
	function agent_profile()
	{
	
		JRequest::setVar('view', 'agent_profile');

		$session 	= 	JFactory::getSession();
		$user_id	=	JRequest::getVar('id',0);
		if($session->get('sessionsave')) {
			$sessionsave = $session->get('sessionsave');
			$session->set('sessionsave', 0);
		} else {
			$sessionsave = JRequest::getVar('sessionsave',0);
			$session->set('sessionsave', 1);
		}
		
		/* RESET BACK SESSIONS */
		if(!$sessionsave) {
			$session->set('profile_page'.$user_id, 0);
			$session->set('profile_orderfilter'.$user_id, 'DESC');
			$session->set('profile_filter'.$user_id, 'a.posting_date');
		}
		//echo print_r($session);
		parent::display();
	}
	function loopBackSession() {
		global $mainframe;
		include_once('assets/json.php');
		
		//make json object...
		
		$json = new Services_JSON();
		
		header('Content-type: application/json');
		
		$usercat 	= 	JRequest::getVar('usercat');
		$user_id	=	JRequest::getVar('user_id');
		
		$session = JFactory::getSession();
		
		if($usercat == 'agent') {
			$page			=	$session->get('profile_page'.$user_id, 0);
			$orderfilter	=	$session->get('profile_orderfilter'.$user_id, 'DESC');
			$filter			=	$session->get('profile_filter'.$user_id, 'a.posting_date');
			
		}
		
		if(!isset($page))
			$page = 0;
		
		if(!isset($orderfilter))
			$orderfilter = 'DESC';
		
		if(!isset($filter))
			$filter = 'a.posting_date';
		
		echo $json->encode(array(	
								 	"page"			=> 	$page,
									"orderfilter"	=>	$orderfilter,
									"filter"		=>	$filter
							));
		
		$mainframe->close();
		exit;
	}
	function ajaxLoadProfileAdslisting() 
	{
		global $mainframe;
		$model		= &$this->getModel('profiledetails');
		
		$usercat	= 	JRequest::getVar('user_category', '');
		$id			= 	JRequest::getVar('id', 0);
		
		$db		=	JFactory::getDBO(); 
		$obj	=	$model->getAdsObject($id, $usercat);
		
		$model1		= &$this->getModel('pagination');
		
		
		$page					=	JRequest::getVar('page',0 );
		$filter					=	JRequest::getVar('filter', '');
		$orderfilter			=	JRequest::getVar('orderfilter', '');

		/* DEFAULT */
		$orderfilter1	=	'ASC';
		$orderfilter2	=	'ASC';
		$orderfilter3	=	'ASC';
		$orderfilter4	=	'ASC';
		
		$image1			= '';
		$image2			= '';
		$image3 		= '';
		$image4			= '';
		
		if($filter == 'a.no_of_room') {
			
			if($orderfilter == 'ASC') {
        		$orderfilter1 = 'DESC';
				$image1 = '<img src="'.JRoute::_('images/sort_asc.png').'" />';
			}else{
				$orderfilter1 = 'ASC';
				$image1 = '<img src="'.JRoute::_('images/sort_desc.png').'" />';
			}
		}
		if($filter == 'a.property_type') {
			
			if($orderfilter == 'ASC') {
        		$orderfilter2 = 'DESC';
				$image2 = '<img src="'.JRoute::_('images/sort_asc.png').'" />';
			}else{
				$orderfilter2 = 'ASC';
				$image2 = '<img src="'.JRoute::_('images/sort_desc.png').'" />';
			}
		} else if($filter == 'a.ask_price') {
			
			if($orderfilter == 'ASC') {
        		$orderfilter3 = 'DESC';
				$image3 = '<img src="'.JRoute::_('images/sort_asc.png').'" />';
			}else{
				$orderfilter3 = 'ASC';
				$image3 = '<img src="'.JRoute::_('images/sort_desc.png').'" />';
			}
		} else if($filter == 'a.posting_date') {
			
			if($orderfilter == 'ASC')  {
        		$orderfilter4 = 'DESC';
				$image4 = '<img src="'.JRoute::_('images/sort_asc.png').'" />';
			} else { 
				$orderfilter4 = 'ASC';
				$image4 = '<img src="'.JRoute::_('images/sort_desc.png').'" />';
			}
		}
		
		$session = JFactory::getSession();
		
		$session->set('profile_page'.$id, $page);
		$session->set('profile_orderfilter'.$id, $orderfilter);
		$session->set('profile_filter'.$id, $filter);
		
		$pagination_html	=	$model1->getPaginationAdsListing($obj->total, 5, $usercat, $filter, $orderfilter);
		
		require('views/profile_ajax/tmpl/default.php');
		
		$mainframe->close();
		exit;
	}
	
	function profile_update()
	{
		global $mainframe;
		require_once( JPATH_COMPONENT.DS.'image.php' );
		require_once( JPATH_ROOT.DS.'includes'.DS.'tools.php' );
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );

		// Get required system objects
		$db = & JFactory::getDBO();
		$user 		=& JFactory::getUser();               
		
		$company_name = JRequest::getVar('company_name', '', 'post', 'company_name');
		$cea_reg_no = JRequest::getVar('cea_reg_no', '', 'post', 'cea_reg_no');
		$name = JRequest::getVar('name', '', 'post', 'name');
		//$surname = JRequest::getVar('surname', '', 'post', 'surname');
		$chinese_name = JRequest::getVar('chinese_name', '', 'post', 'chinese_name');
		$nric = JRequest::getVar('nric', '', 'post', 'nric');
		$country = JRequest::getVar('country', '', 'post', 'country');
		$short_desc = JRequest::getVar('short_desc', '', 'post', 'short_desc');
		$mobile_contact = JRequest::getVar('mobile_contact', '', 'post', 'mobile_contact');
		$organization_name = JRequest::getVar('organization_name', '', 'post', 'organization_name');
		$organization_address = JRequest::getVar('organization_address', '', 'post', 'organization_address');
		$home_contact = JRequest::getVar('home_contact', '', 'post', 'home_contact');
		$website = JRequest::getVar('website', '', 'post', 'website');
		$phone_1 = JRequest::getVar('phone_1', '', 'post', 'phone_1');
		$phone_2 = JRequest::getVar('phone_2', '', 'post', 'phone_2');
		$fax = JRequest::getVar('fax', '', 'post', 'fax');
		$merchant_interest = JRequest::getVar('merchant_interest', '', 'post', 'merchant_interest');
		$residence_status = JRequest::getVar('residence_status', '', 'post', 'residence_status');
		$user_interest = JRequest::getVar('user_interest', '', 'post', 'user_interest');
		$home_address = JRequest::getVar('home_address', '', 'post', 'home_address');
		$home_postalcode = JRequest::getVar('home_postalcode', '', 'post', 'home_postalcode');
		$rental_address = JRequest::getVar('rental_address', '', 'post', 'rental_address');
		$rental_postalcode = JRequest::getVar('rental_postalcode', '', 'post', 'rental_postalcode');
		
		$condo1 = JRequest::getVar( 'condo', array(0), '', 'array' );
		$condo = implode(",", $condo1);
		$condo_type1 = JRequest::getVar( 'condo_type', array(0), '', 'array' );
		$condo_type = implode(",", $condo_type1);
		$hdb1 = JRequest::getVar( 'hdb', array(0), '', 'array' );
		$hdb = implode(",", $hdb1); 
		$hdb_type1 = JRequest::getVar( 'hdb_type', array(0), '', 'array' );
		$hdb_type = implode(",", $hdb_type1);
		$private_estate1 = JRequest::getVar( 'private_estate', array(0), '', 'array' );
		$private_estate = implode(",", $private_estate1);
		$industrial_estate1 = JRequest::getVar( 'industrial_estate', array(0), '', 'array' );
		$industrial_estate = implode(",", $industrial_estate1); 
		
		$pwd = JRequest::getString('password', '', 'post', JREQUEST_ALLOWRAW);
		$pwd = preg_replace('/[\x00-\x1F\x7F]/', '', $pwd); //Disallow control chars in the email
		
		$query = "SELECT password FROM #__users WHERE id = '".$user->get('id')."'";
		$db->setQuery($query);
		$original_pwd = $db->loadResult();
		
		if($pwd == ''){
			$password = $original_pwd;
		} else {
			$password = md5($pwd);
		}
		
		
		$user_category = JRequest::getVar('user_category', '', 'post', 'user_category');
		$day = JRequest::getVar('day', '', 'post', 'day');
		$month = JRequest::getVar('month', '', 'post', 'month');
		$year = JRequest::getVar('year', '', 'post', 'year');

		if(($day == 'dd')||($month== 'mm')||($year == 'yyyy')){
		$dob = '0000-00-00';
		} else {
		$dob = date('Y-m-d' ,mktime(0, 0, 0, $month, $day, $year));
		}

		$query = "SELECT company_logo FROM #__users WHERE id = '".$user->get('id')."'";
		$db->setQuery($query);
		$original_logo = $db->loadResult();
		
		$query = "SELECT user_image FROM #__users WHERE id = '".$user->get('id')."'";
		$db->setQuery($query);
		$original_img = $db->loadResult();
		
		if($_FILES['company_logo']['name']){
		$company_logo = saveFileUpload($_FILES['company_logo']['name'],$_FILES['company_logo']['type'],$_FILES['company_logo']['tmp_name'],1);
		}

		if($company_logo == ''){
			$company_logo = $original_logo;
		} else {
			$company_logo = $company_logo;
		}

		$user_image = JRequest::getVar('user_image', '', 'post', 'user_image');
		if($_FILES['user_image']['name']){
		$user_image = saveFileUpload($_FILES['user_image']['name'],$_FILES['user_image']['type'],$_FILES['user_image']['tmp_name'],1);
		}
		
		if($user_image == ''){
			$user_image = $original_img;
		} else {
			$user_image = $user_image;
		}
		
		$receive = JRequest::getVar( 'receive', array(0), '', 'array' );
		$post['agree_to_receive'] = implode(",", $receive);
		
		
		$sql = "UPDATE #__users set company_name = '".$company_name."', cea_reg_no = '".$cea_reg_no."', name = '".$name."', chinese_name = '".$chinese_name."', nric = '".$nric."', country = '".$country."', short_desc = '".$short_desc."', mobile_contact = '".$mobile_contact."', organization_name = '".$organization_name."', organization_address = '".$organization_address."', home_contact = '".$home_contact."', website = '".$website."', phone_1 = '".$phone_1."', phone_2 = '".$phone_2."', fax = '".$fax."', merchant_interest = '".$merchant_interest."', residence_status = '".$residence_status."', user_interest = '".$user_interest."', home_address = '".$home_address."', home_postalcode = '".$home_postalcode."', rental_address = '".$rental_address."', rental_postalcode = '".$rental_postalcode."', dob = '".$dob."', password = '".$password."', user_image = '".$user_image."', company_logo = '".$company_logo."'  WHERE id = '".$user->get('id')."'";
		$db->setQuery($sql);
		$db->query();
		
		$query = "SELECT count(*) FROM #__users_speciality_areas WHERE user_id = '".$user->get('id')."'";
		$db->setQuery($query);
		$id_exist = $db->loadResult();
		
		if($id_exist == '0'){
		$sql = "INSERT INTO #__users_speciality_areas VALUES (NULL, '".$user->get('id')."', '".$condo."', '".$condo_type."', '".$hdb."', '".$hdb_type."', '".$private_estate."', '".$industrial_estate."', CURRENT_TIMESTAMP)";
		$db->setQuery($sql);
		$db->query();
		
		} else {
		$query = "UPDATE #__users_speciality_areas set condo = '".$condo."', condo_type = '".$condo_type."', hdb = '".$hdb."', hdb_type = '".$hdb_type."', private_estate = '".$private_estate."', industrial_estate = '".$industrial_estate."', datetime = CURRENT_TIMESTAMP WHERE user_id = '".$user->get('id')."'";
			$db->setQuery( $query );
			$db->query();
		}
		
		$this->setRedirect('index.php?option=com_user&view=profiler&Itemid=126&sid='.$user->get('id'));
		
	}

	function paymentprocess()
	{

		JRequest::setVar('view', 'paymentprocess');
	
		parent::display();
		
	}
	
	function paymentsuccess()
	{
		JRequest::setVar('view', 'paymentsuccess');
		parent::display();
	}
	
	function thankpayment()
	{
		$user 	=& JFactory::getUser();

		if ( $user->get('guest')) {
			$this->setredirect('index.php?option=com_user&view=login&Itemid=80');
		} else {
			JRequest::setVar('view', 'thankpayment');
		}
	
		parent::display();
	}
	
	function thankupgrade()
	{
		$user 	=& JFactory::getUser();
		$pathway 	=& $mainframe->getPathway();
		$config		=& JFactory::getConfig();
		$authorize	=& JFactory::getACL();
		$document   =& JFactory::getDocument();
		
		// If user registration is not allowed, show 403 not authorized.
		$usersConfig = &JComponentHelper::getParams( 'com_users' );
		if ($usersConfig->get('allowUserRegistration') == '0') {
			JError::raiseError( 403, JText::_( 'Access Forbidden' ));
			return;
		}

		
		// If user activation is turned on, we need to set the activation information
		$useractivation = $usersConfig->get( 'useractivation' );
		if ($useractivation == '1')
		{
			jimport('joomla.user.helper');
			$user->set('activation', JUtility::getHash( JUserHelper::genRandomPassword()) );
			$user->set('block', '1');
		}


		if ( $user->get('guest')) {
			$this->setredirect('index.php?option=com_user&view=login&Itemid=80');
		} else {
		
				// Send registration confirmation mail
			//$password = JRequest::getString('password', '', 'post', JREQUEST_ALLOWRAW);
			//$password = preg_replace('/[\x00-\x1F\x7F]/', '', $password); //Disallow control chars in the email
			//UserController::_sendMail($user, $password);
			/*UserController::_sendMail();
	
			// Everything went fine, set relevant message depending upon user activation state and display message
			if ( $useractivation == 1 ) {
				$message  = JText::_( 'REG_COMPLETE_ACTIVATE' );
			} else {
				$message = JText::_( 'REG_COMPLETE' );
			}*/
		
			
			//JRequest::setVar('view', 'thankupgrade');
		}
		/*echo 'test';
		exit;*/
	//print_r($usersConfig);
		//parent::display();
	}
	
	function send_activation()
	{
		global $mainframe;
		$username = JRequest::getVar('username', '', 'post', 'username');
		$db = & JFactory::getDBO();
		
		$query = "SELECT * FROM #__users WHERE username = '".$username."'";
		$db->setQuery($query);
		$lists = $db->loadObjectList();
		
		if($lists[0]->one_time_fee == '1'){
			if(!empty($lists[0]->activation)){
		$body = "
	Hello ".$lists[0]->name.",<br /><br />
 
Thank you for registering at iHouse360. Your account is created and must be activated before you can use it.<br /><br />
To activate the account click on the following link or copy-paste it in your browser:<br />
<a href='".JURI::root()."index.php?option=com_user&task=activate&activation=".$lists[0]->activation."'>".JURI::root()."index.php?option=com_user&task=activate&activation=".$lists[0]->activation."</a><br /><br />
 
After activation you may login to <a href='".JURI::root()."'>".JURI::root()."</a> using the username and password that you have created.
	";
	
		$config     = &JFactory::getConfig();
		$from       = $config->getValue('mailfrom');
		$fromname = $config->getValue('fromname');
		
		$mail = JFactory::getMailer();
		
		$mail->addRecipient( $lists[0]->email );
		$mail->setSender( array( $from , $fromname ) );
		$mail->setSubject( 'Account Details for '.$lists[0]->name.' at iHouse360');
		$mail->setBody( $body );
		$mail->IsHTML(true);
		
				//update#gunmail
		$conf =& JFactory::getConfig();
		if($conf->getValue('config.mailer') == 'smtp'){
			$mail->useSMTP(
			$conf->getValue('config.smtpauth'), 
			$conf->getValue('config.smtphost'), 
			$conf->getValue('config.smtpuser'), 
			$conf->getValue('config.smtppass'), 
			$conf->getValue('config.smtpsecure'), 
			$conf->getValue('config.smtpport')
			);
		}
		//update#end
		
		 //Send the Mail
		$sent = $mail->Send(); 
		
		if ( JError::isError($sent) ) {
			echo "Error occurs";
			return false;
		} else {
			$msg = JText::_( 'Activation Link has been sent to your email.');
			$link = JRoute::_('index.php?option=com_user&view=login&Itemid=80', false);
			$this->setRedirect($link, $msg);
			return true;
		}
		
		} else {
		
			$msg = JText::_( 'This account has already been activated.');
			$link = JRoute::_('index.php?option=com_user&view=login&Itemid=80', false);
			$this->setRedirect($link, $msg);
		
		}//end function
		
		} else {
		
			$query = "SELECT * FROM #__users WHERE username = '".$username."'";
			$db->setQuery($query);
			$userlist = $db->loadObject();
			
			if(($userlist->one_time_fee == '0')&&($userlist->user_category == 'Agent')&&(!empty($userlist->activation))){	
				$this->setRedirect('index.php?option=com_user&view=send_activation&username='.$username.'&Itemid=80');
			} else {
				$this->setRedirect('index.php');
			}
		
		}

	}
	
	function emailContactForm() {
		global $mainframe;
		
		$db		=	JFactory::getDBO(); 
		
		$email	=	JRequest::getVar('email','');
		$ads_id =	JRequest::getVar('ads_id','');
		$uid 	=	JRequest::getVar('uid','');
		
		require('views/contact_form/tmpl/default.php');
		$mainframe->close();
		exit;
	}
	
	function email() {
		global $mainframe;
		include_once('assets/json.php');
		
		//make json object...
		
		$json = new Services_JSON();
		
		$model		= &$this->getModel('contact');
		
		header('Content-type: application/json');
		
		$status		=	$model->sendEmail();
		
		echo $json->encode(array("status" => $status	
								 
							));
		$mainframe->close();
		exit;
	}

	function activate()
	{
		global $mainframe;

		// Initialize some variables
		$db			=& JFactory::getDBO();
		$user 		=& JFactory::getUser();
		$document   =& JFactory::getDocument();
		$pathway 	=& $mainframe->getPathWay();

		$usersConfig = &JComponentHelper::getParams( 'com_users' );
		$userActivation			= $usersConfig->get('useractivation');
		$allowUserRegistration	= $usersConfig->get('allowUserRegistration');

		// Check to see if they're logged in, because they don't need activating!
		if ($user->get('id')) {
			// They're already logged in, so redirect them to the home page
			$mainframe->redirect( 'index.php' );
		}

		if ($allowUserRegistration == '0' || $userActivation == '0') {
			JError::raiseError( 403, JText::_( 'Access Forbidden' ));
			return;
		}

		// create the view
		require_once (JPATH_COMPONENT.DS.'views'.DS.'register'.DS.'view.html.php');
		$view = new UserViewRegister();

		$message = new stdClass();

		// Do we even have an activation string?
		$activation = JRequest::getVar('activation', '', '', 'alnum' );
		$activation = $db->getEscaped( $activation );

		if (empty( $activation ))
		{
			// Page Title
			$document->setTitle( JText::_( 'REG_ACTIVATE_NOT_FOUND_TITLE' ) );
			// Breadcrumb
			$pathway->addItem( JText::_( 'REG_ACTIVATE_NOT_FOUND_TITLE' ));

			$message->title = JText::_( 'REG_ACTIVATE_NOT_FOUND_TITLE' );
			$message->text = JText::_( 'REG_ACTIVATE_NOT_FOUND' );
			$view->assign('message', $message);
			$view->display('message');
			return;
		}

		// Lets activate this user
		jimport('joomla.user.helper');
		if (JUserHelper::activateUser($activation))
		{
			// Page Title
			$document->setTitle( JText::_( 'REG_ACTIVATE_COMPLETE_TITLE' ) );
			// Breadcrumb
			$pathway->addItem( JText::_( 'REG_ACTIVATE_COMPLETE_TITLE' ));

			$message->title = JText::_( 'REG_ACTIVATE_COMPLETE_TITLE' );
			$message->text = JText::_( 'REG_ACTIVATE_COMPLETE' );
		}
		else
		{
			// Page Title
			$document->setTitle( JText::_( 'REG_ACTIVATE_NOT_FOUND_TITLE' ) );
			// Breadcrumb
			$pathway->addItem( JText::_( 'REG_ACTIVATE_NOT_FOUND_TITLE' ));

			$message->title = JText::_( 'REG_ACTIVATE_NOT_FOUND_TITLE' );
			$message->text = JText::_( 'REG_ACTIVATE_NOT_FOUND' );
		}

		$view->assign('message', $message);
		$view->display('message');
	}

	/**
	 * Password Reset Request Method
	 *
	 * @access	public
	 */
	function requestreset()
	{
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );

		// Get the input
		$email		= JRequest::getVar('email', null, 'post', 'string');

		// Get the model
		$model = &$this->getModel('Reset');

		// Request a reset
		if ($model->requestReset($email) === false)
		{
			$message = JText::sprintf('PASSWORD_RESET_REQUEST_FAILED', $model->getError());
			$this->setRedirect('index.php?option=com_user&view=reset', $message);
			return false;
		}

		$this->setRedirect('index.php?option=com_user&view=reset&layout=confirm');
	}

	/**
	 * Password Reset Confirmation Method
	 *
	 * @access	public
	 */
	function confirmreset()
	{
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );

		// Get the input
		$token = JRequest::getVar('token', null, 'post', 'alnum');
		$username = JRequest::getVar('username', null, 'post');

		// Get the model
		$model = &$this->getModel('Reset');

		// Verify the token
		if ($model->confirmReset($token, $username) !== true)
		{
			$message = JText::sprintf('PASSWORD_RESET_CONFIRMATION_FAILED', $model->getError());
			$this->setRedirect('index.php?option=com_user&view=reset&layout=confirm', $message);
			return false;
		}
		$this->setRedirect('index.php?option=com_user&view=reset&layout=complete');
	}

	/**
	 * Password Reset Completion Method
	 *
	 * @access	public
	 */
	function completereset()
	{
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );

		// Get the input
		$password1 = JRequest::getVar('password1', null, 'post', 'string', JREQUEST_ALLOWRAW);
		$password2 = JRequest::getVar('password2', null, 'post', 'string', JREQUEST_ALLOWRAW);

		// Get the model
		$model = &$this->getModel('Reset');

		// Reset the password
		if ($model->completeReset($password1, $password2) === false)
		{
			$message = JText::sprintf('PASSWORD_RESET_FAILED', $model->getError());
			$this->setRedirect('index.php?option=com_user&view=reset&layout=complete', $message);
			return false;
		}

		$message = JText::_('PASSWORD_RESET_SUCCESS');
		$this->setRedirect('index.php?option=com_user&view=login', $message);
	}

	/**
	 * Username Reminder Method
	 *
	 * @access	public
	 */
	function remindusername()
	{
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );

		// Get the input
		$email = JRequest::getVar('email', null, 'post', 'string');

		// Get the model
		$model = &$this->getModel('Remind');

		// Send the reminder
		if ($model->remindUsername($email) === false)
		{
			$message = JText::sprintf('USERNAME_REMINDER_FAILED', $model->getError());
			$this->setRedirect('index.php?option=com_user&view=remind', $message);
			return false;
		}

		$message = JText::sprintf('USERNAME_REMINDER_SUCCESS', $email);
		$this->setRedirect('index.php?option=com_user&view=login', $message);
	}

	function _sendMail(&$user, $password)
	{
		global $mainframe;

		$db		=& JFactory::getDBO();

		$name 		= $user->get('name');
		$email 		= $user->get('email');
		$username 	= $user->get('username');

		$usersConfig 	= &JComponentHelper::getParams( 'com_users' );
		$sitename 		= $mainframe->getCfg( 'sitename' );
		$useractivation = $usersConfig->get( 'useractivation' );
		$mailfrom 		= $mainframe->getCfg( 'mailfrom' );
		$fromname 		= $mainframe->getCfg( 'fromname' );
		$siteURL		= JURI::base();

		$subject 	= sprintf ( JText::_( 'Account details for' ), $name, $sitename);
		$subject 	= html_entity_decode($subject, ENT_QUOTES);

		if ( $useractivation == 1 ){
			$message = sprintf ( JText::_( 'SEND_MSG_ACTIVATE' ), $name, $sitename, $siteURL."index.php?option=com_user&task=activate&activation=".$user->get('activation'), $siteURL, $username, $password);
		} else {
			$message = sprintf ( JText::_( 'SEND_MSG' ), $name, $sitename, $siteURL);
		}

		$message = html_entity_decode($message, ENT_QUOTES);

		//get all super administrator
		$query = 'SELECT name, email, sendEmail' .
				' FROM #__users' .
				' WHERE LOWER( usertype ) = "super administrator"';
		$db->setQuery( $query );
		$rows = $db->loadObjectList();

		// Send email to user
		if ( ! $mailfrom  || ! $fromname ) {
			$fromname = $rows[0]->name;
			$mailfrom = $rows[0]->email;
		}

		JUtility::sendMail($mailfrom, $fromname, $email, $subject, $message);

		// Send notification to all administrators
		$subject2 = sprintf ( JText::_( 'Account details for' ), $name, $sitename);
		$subject2 = html_entity_decode($subject2, ENT_QUOTES);

		// get superadministrators id
		foreach ( $rows as $row )
		{
			if ($row->sendEmail)
			{
				$message2 = sprintf ( JText::_( 'SEND_MSG_ADMIN' ), $row->name, $sitename, $name, $email, $username);
				$message2 = html_entity_decode($message2, ENT_QUOTES);
				JUtility::sendMail($mailfrom, $fromname, $row->email, $subject2, $message2);
			}
		}
	}
}
?>
