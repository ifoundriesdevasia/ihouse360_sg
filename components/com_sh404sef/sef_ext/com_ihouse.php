<?php
/**
 * sh404SEF support for com_XXXXX component.
 * Author : 
 * contact :
 * 
 * This is a sample sh404SEF native plugin file
 *    
 */
defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );

// ------------------  standard plugin initialize function - don't change ---------------------------
global $sh_LANG;
$sefConfig = & shRouter::shGetConfig();  
$shLangName = '';
$shLangIso = '';
$title = array();
$shItemidString = '';
$dosef = shInitializePlugin( $lang, $shLangName, $shLangIso, $option);
if ($dosef == false) return;
// ------------------  standard plugin initialize function - don't change ---------------------------

// ------------------  load language file - adjust as needed ----------------------------------------
$shLangIso = shLoadPluginLanguage( 'com_ihouse', $shLangIso, '_SEF_SAMPLE_TEXT_STRING');
// ------------------  load language file - adjust as needed ----------------------------------------

// remove common URL from GET vars list, so that they don't show up as query string in the URL
shRemoveFromGETVarsList('option');
shRemoveFromGETVarsList('lang');

if (!empty($Itemid))
  	shRemoveFromGETVarsList('Itemid');
if (!empty($limit))  
	shRemoveFromGETVarsList('limit');
if (isset($limitstart)) 
  	shRemoveFromGETVarsList('limitstart'); // limitstart can be zero



	
// start by inserting the menu element title (just an idea, this is not required at all)
$view = isset($view) ? $view : null;
$task = isset($task) ? $task : null;
$Itemid = isset($Itemid) ? $Itemid : null;

$shSampleName = shGetComponentPrefix($option); 
$shSampleName = empty($shSampleName) ?  
		getMenuTitle($option, $task, $Itemid, null, $shLangName) : $shSampleName;
$shSampleName = (empty($shSampleName) || $shSampleName == '/') ? 'SampleCom':$shSampleName;

switch ($view) {	
	case 'ihouse':
		$tmp = '';
		if (isset($layout)) {
			$tmp .= 'Property-';
  			shRemoveFromGETVarsList('layout'); 
		}

		if (isset($postcode)) {
			$tmp .= $postcode.'-';
		  	shRemoveFromGETVarsList('postcode'); 
		}

		if(isset($id)) {
			$q = 'SELECT name_en FROM #__ihouse_property WHERE id = '.$database->Quote($id);  
      		$database->setQuery($q);                                
      	
    		if (shTranslateUrl($option, $shLangName))              
				$sampleTitle = $database->loadObject( );
      		else 
	  			$sampleTitle = $database->loadObject(false);         
		
			if ($sampleTitle) {   
        		$title[] = $tmp.$sampleTitle->name_en;     
			}
			//shMustCreatePageId( 'set', true);
			shRemoveFromGETVarsList('id');
      	}  
		shRemoveFromGETVarsList('view');
		break;        
	default:
	  	$dosef = false;  // these tasks do not require SEF URL
		break;														 
}

if(!empty($sector)) {
	$title[] = 'Sector';
	
	if (shTranslateUrl($option, $shLangName))       
		$title[] = $sector;
    else 
	  	$title[] = $sector;
		
	shRemoveFromGETVarsList('sector');	
	//$dosef = true;
}

if(!empty($filter)) {
	$filter = 'filter-'.$filter;
	
	$title[] = $filter;
	
	shRemoveFromGETVarsList('filter');	
	//$dosef = true;
}

if(!empty($orderfilter)) {
	$orderfilter = 'order-'.$orderfilter;
	
	$title[] = $orderfilter;
	
	shRemoveFromGETVarsList('orderfilter');	
	//$dosef = true;
}


// also remove task, as it is not neede
// because we can revert the SEF URL without it  

// ------------------  standard plugin finalize function - don't change ---------------------------  
if ($dosef){
   $string = shFinalizePlugin( $string, $title, $shAppendString, $shItemidString, 
      (isset($limit) ? @$limit : null), (isset($limitstart) ? @$limitstart : null), 
      (isset($shLangName) ? @$shLangName : null));
}      
// ------------------  standard plugin finalize function - don't change ---------------------------
