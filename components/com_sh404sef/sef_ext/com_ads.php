<?php
/**
 * sh404SEF support for com_XXXXX component.
 * Author : 
 * contact :
 * 
 * This is a sample sh404SEF native plugin file
 *    
 */
defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );

// ------------------  standard plugin initialize function - don't change ---------------------------
global $sh_LANG;
$sefConfig = & shRouter::shGetConfig();  
$shLangName = '';
$shLangIso = '';
$title = array();
$shItemidString = '';
$dosef = shInitializePlugin( $lang, $shLangName, $shLangIso, $option);
if ($dosef == false) return;
// ------------------  standard plugin initialize function - don't change ---------------------------

// ------------------  load language file - adjust as needed ----------------------------------------
$shLangIso = shLoadPluginLanguage( 'com_ads', $shLangIso, '_SEF_SAMPLE_TEXT_STRING');
// ------------------  load language file - adjust as needed ----------------------------------------

// remove common URL from GET vars list, so that they don't show up as query string in the URL
shRemoveFromGETVarsList('option');
shRemoveFromGETVarsList('lang');

if (!empty($Itemid))
  	shRemoveFromGETVarsList('Itemid');
if (!empty($limit))  
	shRemoveFromGETVarsList('limit');
if (isset($limitstart)) 
  	shRemoveFromGETVarsList('limitstart'); // limitstart can be zero
	
// start by inserting the menu element title (just an idea, this is not required at all)
$view = isset($view) ? $view : null;
$task = isset($task) ? $task : null;
$Itemid = isset($Itemid) ? $Itemid : null;

$shSampleName = shGetComponentPrefix($option); 
$shSampleName = empty($shSampleName) ?  
		getMenuTitle($option, $task, $Itemid, null, $shLangName) : $shSampleName;
$shSampleName = (empty($shSampleName) || $shSampleName == '/') ? 'SampleCom':$shSampleName;

switch ($view) {	
	case 'adsdetails':
		$title[] = 'ads';
		if(isset($id)) {
			$q = 'SELECT property_name,ad_title, street_name FROM #__ihouse_ads WHERE id = '.$database->Quote($id);  
      		$database->setQuery($q);                                
      	
    		if (shTranslateUrl($option, $shLangName))              
				$sampleTitle = $database->loadObject( );
      		else 
	  			$sampleTitle = $database->loadObject(false);         
		
			if ($sampleTitle) {   
        		$title[] = $sampleTitle->property_name.'-'.$sampleTitle->street_name.'-'.$sampleTitle->ad_title;     
			}
			//shMustCreatePageId( 'set', true);
			shRemoveFromGETVarsList('id');
      	}  
		break;        
	default:
	  	$dosef = false;  // these tasks do not require SEF URL
		break;														 
}


// also remove task, as it is not neede
// because we can revert the SEF URL without it

shRemoveFromGETVarsList('view');	  

// ------------------  standard plugin finalize function - don't change ---------------------------  
if ($dosef){
   $string = shFinalizePlugin( $string, $title, $shAppendString, $shItemidString, 
      (isset($limit) ? @$limit : null), (isset($limitstart) ? @$limitstart : null), 
      (isset($shLangName) ? @$shLangName : null));
}      
// ------------------  standard plugin finalize function - don't change ---------------------------
  
