/**
 * Copyright 2008 - 2009 www.joomlapolis.com and its licensors
 * License: Proprietary. Copy not authorized.
 */
// var cbpayHideFields;

(function($) {
	var cbpaySels;
	var cbpayFirstTimeDone = false;
	function paidsubsChange() {
		var fieldsToShow = new Array();
		var fieldsToHide = new Array();
		var r  = new RegExp('^cbpaidsubscriptionsplan[EN]\\[(\\d+)\\]\\[selected\\]\\[\\]$', '');
		if ( ! cbpayFirstTimeDone ) {
			// first time:
			cbpaySels.each( function(i) {
				if ( $(this).attr('checked') ) {
					$(this).attr('cbsubschkdef', '1' );
				}
			});
			cbpaySels.each( function(i) {
				var iPlan = r.exec( $(this).attr('name') );
				if ( iPlan[1] == '0' ) {
					// checks if the selected plan has no parent, its childrens are set to default values:
					if ( $(this).attr('checked') ) {
						cbpaySels.filter( '[name=\'cbpaidsubscriptionsplanE[' + $(this).val() + '][selected][]\'],[name=\'cbpaidsubscriptionsplanN[' + $(this).val() + '][selected][]\']' ).filter( function() {
							return ( $(this).attr('cbsubschkdef') == '1');
						} ).attr( 'checked', true );
					}
				} else {
					// checks that parent plan is checked, if exists, otherwise unchecks this:
					if ( cbpaySels.filter( '[value=\'' + parseInt(iPlan[1]) + '\']' ).filter( '[name=\'cbpaidsubscriptionsplanE[0][selected][]\'],[name=\'cbpaidsubscriptionsplanN[0][selected][]\']' ).attr('checked') == '' ) {
						$(this).attr( 'checked', false );
					}
				}
			});
		} else {
			var clickedPlan = r.exec($(this).attr('name'));
			if ( clickedPlan !== false ) {
				if ( clickedPlan[1] == '0' ) {
					// a parent plan has been clicked:
					if ( $(this).attr('checked') ) {
						// if it's now checked, its childrens are set to default values:
						cbpaySels.filter( '[name=\'cbpaidsubscriptionsplanE[' + $(this).val() + '][selected][]\'],[name=\'cbpaidsubscriptionsplanN[' + $(this).val() + '][selected][]\']' ).filter( function() {
							return ( $(this).attr('cbsubschkdef') == '1');
						} ).attr( 'checked', true );
					}
				} else {
					// a child plan has been clicked:
					// then, if a child is checked, checks its parent too:
					var p = cbpaySels.filter( '[value=\'' + parseInt(clickedPlan[1]) + '\']' ).filter( '[name=\'cbpaidsubscriptionsplanE[0][selected][]\'],[name=\'cbpaidsubscriptionsplanN[0][selected][]\']' );
					if ( $(this).attr('checked') == true ) {
						p.attr( 'checked', true );
						// and uncheck the children of other now unchecked (in case of radios) parents below
					}					
				}
			}
		}
		cbpaySels.each( function(i) {
			var iPlan = r.exec( $(this).attr('name') );
			// and uncheck the children of unchecked parents:
			if ( ( iPlan[1] == '0' ) && ( $(this).val() != '0' ) && ( ! $(this).attr('checked') ) ) {
				cbpaySels.filter( '[name=\'cbpaidsubscriptionsplanE[' + $(this).val() + '][selected][]\'],[name=\'cbpaidsubscriptionsplanN[' + $(this).val() + '][selected][]\']' ).attr( 'checked', false );
			}
		});
		cbpaySels.each( function(i) {
			// decides which CB fields to show or hide:
			if ( $(this).attr('checked') ) {
				fieldsToHide = fieldsToHide.concat( cbpayHideFields[$(this).val()] );
			} else {
				fieldsToShow = fieldsToShow.concat( cbpayHideFields[$(this).val()] );
			}
		});
		// Show or hide fields:
		if ( cbpayFirstTimeDone ) {
			for (var i=0;i<fieldsToShow.length;i++) {
				$('#cbfr_' + fieldsToShow[i]).fadeIn("slow").find('.fieldCell input.requiredDisabled,.fieldCell select.requiredDisabled').removeClass('requiredDisabled').addClass('required').attr('mosreq','1');
			}
			for (var i=0;i<fieldsToHide.length;i++) {
				$('#cbfr_' + fieldsToHide[i]).fadeOut("slow").find('.fieldCell input.required,.fieldCell select.required').removeClass('required').addClass('requiredDisabled').attr('mosreq','0');
			}
		} else {
			for (var i=0;i<fieldsToShow.length;i++) {
				$('#cbfr_' + fieldsToShow[i]).show().find('.fieldCell input.requiredDisabled,.fieldCell select.requiredDisabled').removeClass('requiredDisabled').addClass('required').attr('mosreq','1');
			}
			for (var i=0;i<fieldsToHide.length;i++) {
				$('#cbfr_' + fieldsToHide[i]).hide().find('.fieldCell input.required,.fieldCell select.required').removeClass('required').addClass('requiredDisabled').attr('mosreq','0');
			}
		}
		// Finally hides or shows children:
		$('#cbregUpgrades .cbregDoHideChildren .cbregTick input').each( function() {
			var subPlans = $(this).parent().next('.cbRegNameDesc').find('.cbregSubPlanSelector');
			if ( $(this).attr( 'checked' ) == true ) {
				if ( cbpayFirstTimeDone ) {
					subPlans.slideDown('slow');
				} else {
					subPlans.show();
				}
				$(this).parent().parent().removeClass('cbregDoHideChildrenHidden').addClass('cbregDoHideChildrenVisible');
			} else {
				if ( cbpayFirstTimeDone ) {
					subPlans.slideUp('slow');
				} else {
					subPlans.hide();
				}
				$(this).parent().parent().removeClass('cbregDoHideChildrenVisible').addClass('cbregDoHideChildrenHidden');
			}
		});
		cbpayFirstTimeDone = true;
	}
	var donationSelval = [];
	/*
	 * Handles the Donation plans select drop-down:
	 */
	function paidsubsDonationSelect() {
		// [1]: prefix, [2]: parent_id, [3]: plan_id :
		var iNamePrefix = /^([^\[]+)[EN]\[([^\]]+)\]\[donate\]\[plan([^\]]+)\]\[donsel\]$/.exec($(this).attr('name'));
		// unchecks the selection of the plan if the donation is not selected:
		$( '#'+iNamePrefix[1]+iNamePrefix[3] ).attr( 'checked', ( $(this).val() !== '' ) ).triggerHandler('click');
		// unhides the free donation field if the selected value is 'other' (0):
		if ( $(this).val() === '0' ) {
			$(this).parent().next('span.cbregDonationValue').fadeIn('slow');
			if ( ( typeof( donationSelval[$(this).attr('name')] ) == 'undefined' ) || ( ! donationSelval[$(this).attr('name')] ) ) {
				$(this).parent().next('span.cbregDonationValue').children('input.cbregDonationFreeValue').focus();
				donationSelval[$(this).attr('name')] = true;
			}
		} else {
			$(this).parent().next('span.cbregDonationValue').fadeOut('slow');
			donationSelval[$(this).attr('name')] = false;
		}
		return true;
	}
	/*
	 * Handles the Donation plans free donation value text input box:
	 */
	function paidsubsDonationValueBlur() {
		// if the donation selector selects 'other' (0):
		if ( $(this).parent().prev('span.cbregDonationSelect').children('select.cbregDonationSelector').val() === '0' ) {
			// [1]: prefix, [2]: parent_id, [3]: plan_id :
			var iNamePrefix = /^([^\[]+)[EN]\[([^\]]+)\]\[donate\]\[plan([^\]]+)\]\[donval\]$/.exec($(this).attr('name'));
			// checks if the value is non-zero float or empty:
			var isZeroOrEmpty = ( /^ *0*\.?0* *$/.test( $(this).val() ) );
			// ticks/unticks the corresponding donation plan selector depending of free donation value and triggers the other events:
			$( '#'+iNamePrefix[1]+iNamePrefix[3] ).attr( 'checked', ! isZeroOrEmpty ).triggerHandler('click');
		}
		return true;
	}
	/*
	 * Handles the Donation plans free donation value text input box:
	 */
	function paidsubsDonationValueChangeKeyUp() {
		// avoids non-float value character inputs:
		$(this).val( /[0-9]*\.?[0-9]*/.exec( $(this).val() ) );
		return true;
	}
	$.extend({
		cbpaidsubs : {
			paidsubsInit : function() {
				// parent and child plans selection:
				cbpaySels = $('#cbregUpgrades input[type!=hidden]').filter( function(index) {
					return /^cbpaidsubscriptionsplan[EN]\[(\d+)\]\[selected\]\[\]$/.test( $(this).attr('name') );
				});
				paidsubsChange();
				cbpaySels.click( paidsubsChange );
				// donations:
				$('#cbregUpgrades select.cbregDonationSelector').click( paidsubsDonationSelect );
				$('#cbregUpgrades input.cbregDonationFreeValue').blur( paidsubsDonationValueBlur ).change( paidsubsDonationValueChangeKeyUp ).keyup( paidsubsDonationValueChangeKeyUp );
			}
		}
	});
})(jQuery);
