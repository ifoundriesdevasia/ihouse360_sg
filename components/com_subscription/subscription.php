<?php

// no direct access
defined('_JEXEC') or die('Restricted access');

// Require the base controller
require_once (JPATH_COMPONENT.DS.'controller.php');

//JTable::addIncludePath(JPATH_COMPONENT.DS.'com_insureasia'.DS.'tables');
//JTable::addIncludePath(JPATH_COMPONENT.DS.'components'.DS.'com_receipt'.DS.'tables');//components/com_receipt/tables

require_once(JPATH_COMPONENT.DS.'helpers'.DS.'icon.php');

// Component Helper
jimport('joomla.application.component.helper');

// Create the controller
$controller	= new subscriptionController( );

$controller->execute( JRequest::getCmd( 'task' ) );
$controller->redirect();
