<?php
/**
* @version		$Id: poll.php 10752 2008-08-23 01:53:31Z eddieajau $
* @package		Joomla
* @subpackage	Polls
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class SubscriptionModelSubscribe extends JModel
{
	function __construct()
	{
		global $mainframe;

		parent::__construct();

	}
	
	function getSubscribed() {
		$valid		=	JRequest::getVar('validInput');
		if(!$valid)
			return false;
			
		$db = $this->getDBO();
		
		
		$salute		=	JRequest::getVar('salute','');
		$name		=	$salute.' '.JRequest::getVar('name','');
		$email		=	JRequest::getVar('email','');
		$usertype	=	JRequest::getVar('usertype','');
		$org_name	=	JRequest::getVar('organization_name','');
		$times		=	time();
		
		/*if(!preg_match("/^([a-z0-9])(([-a-z0-9._])*([a-z0-9]))*\@([a-z0-9])*(\.([a-z0-9])([-a-z0-9_-])([a-z0-9])+)*$/", $email)) {
			return false;
		}*/
		
		
		if(!preg_match("/^[^0-9][A-z0-9_]+([.][A-z0-9_]+)*[@][A-z0-9_]+([.][A-z0-9_]+)*[.][A-z]{2,4}$/", $email)) {
			return false;
		}
		
		$regexp = "";
		
		$query	=	" SELECT COUNT(*) FROM #__acymailing_subscriber WHERE email = '$email' ";
			$db->setQuery( $query );
			$exist = $db->loadResult();
		
		if($exist)
			return false;
		
		$query = " INSERT INTO #__acymailing_subscriber VALUES(NULL, '$email', NULL,'$name', '$times', 1,1,1,'0.0.0.0',1,NULL ); ";

				$db->setQuery( $query );
				$db->query();
				
			$subid = $db->insertid();

		
		if($usertype == 'individual'){
			
			$query = " INSERT INTO #__acymailing_listsub VALUES(4, '$subid','$times',NULL,1); ";
			
				$db->setQuery( $query );
				$db->query();
		} else if($usertype == 'merchant') {
			
			$query = " INSERT INTO #__acymailing_listsub VALUES(3, '$subid','$times',NULL,1 ); ";
			
				$db->setQuery( $query );
				$db->query();
		} else if($usertype == 'owner') {
			
			$query = " INSERT INTO #__acymailing_listsub VALUES(2, '$subid','$times',NULL,1 ); ";
			
				$db->setQuery( $query );
				$db->query();
		} else if($usertype == 'agent') {
			
			$query = " INSERT INTO #__acymailing_listsub VALUES(1, '$subid','$times',NULL,1 ); ";
			
				$db->setQuery( $query );
				$db->query();
		}
		
		return true;
			
	}
	
	function checkEmail() {
		$email	=	JRequest::getVar('email');
		
		if(!preg_match("/^([a-z0-9])(([-a-z0-9._])*([a-z0-9]))*\@([a-z0-9])*(\.([a-z0-9])([-a-z0-9_-])([a-z0-9])+)*$/", $email)) {
			return false;
		}
		
		$db = $this->getDBO();
		$query = " SELECT COUNT(*) FROM #__acymailing_subscriber WHERE email = '$email' ";

				$db->setQuery( $query );
				$count = $db->loadResult();

		if($count)
			return false;
		else
			return true;
	}
}