﻿<?php //defined('_JEXEC') or die('Restricted access'); ?>
 <!-- English Version - Start -->

                        <div id="small-module-content" >
                        	<div id="module-content-padding">
                            	<div id="small-module-content-cont">
                                    <div id="small-module-content-text" style="font-size:11px;width:170px;">Register to receive our newsletter</div>
                                    <div id="small-module-content-text1" style="width:110px;text-align:right;"><a href="javascript:subsLang('ch');" style="text-decoration:none;color:#66460C">Chinese</a> | <a href="javascript:subsLang('en');" style="text-decoration:none;color:#fff;background-color:#ff8200">English</a></div>
                                </div>
                                
                                <div id="small-module-content-cont">
                                    <div id="small-module-content-text2" style="font-size:12px;"><span id="sub_salute">Salute</span><font color="#fa8b11">*</font></div>
                                    <div class="floatleft"><input type="text" id="salute" name="salute" class="newsletter_text" /></div>
                                </div>
                                
                                <div id="small-module-content-cont">
                                    <div id="small-module-content-text2" style="font-size:12px;"><span id="sub_name">Name</span><font color="#fa8b11">*</font></div>
                                    <div class="floatleft"><input type="text" id="name" name="name" class="newsletter_text" /></div>
                                </div>
                                
                                <div id="small-module-content-cont">
                                    <div id="small-module-content-text2" style="font-size:12px;"><span id="sub_email">Email</span><font color="#fa8b11">*</font></div>
                                    <div class="floatleft"><input type="text" id="email" name="email" class="newsletter_text" onkeyup="" value="" /></div>
                                </div>
                                
                                <div id="small-module-content-cont">
                                    <div id="small-module-content-text2" style="font-size:12px;">You are</div>
                                    <div id="small-module-content-text3">
                                    	<input type="radio" id="usertype" name="usertype" value="individual" checked="yes" />&nbsp;Individual&nbsp;<input type="radio" id="usertype" name="usertype" value="merchant" />&nbsp;Merchant&nbsp;<input type="radio" id="usertype" name="usertype" value="owner" />&nbsp;Owner&nbsp;<input type="radio" id="usertype" name="usertype" value="agent" />&nbsp;Agent
                                    </div>
                                </div>
                                
                               <div id="small-module-content-cont">
                                    <div id="small-module-content-text2" style="font-size:12px;width:150px;">If merchant, organization name</div>
                                    <div class="floatleft"><input type="text" id="organization_name" name="organization_name" class="newsletter_text" style="width:120px;" /></div>
                               </div>
                               
                               <div id="small-module-content-cont1">
                                    <div id="small-module-content-text4" >
                                    	<div style="float:left;">*Compulsory field</div>
                                        <div id="subscribeLoader" style="float:left;margin-left:10px;display:none;">
                    						<img src="<?php echo JRoute::_('templates/main/images/ajax-loader.gif'); ?>"  />
                						</div>
                                        </div>
                                    <input type="hidden" id="validInput" name="validInput" value="0"  />
                                    <div class="floatleft"><input type="button" value="submit" class="enter_button" onclick="javascript: validationSubscription('en');return false;" /></div>
                                    <div style="clear:both"></div>
                                    <div id="subs-err"></div>
                                   
                               </div>
                        	</div>
                        </div>

						<!-- English Version - End -->
                        
                    <div id="homepage_popup_en" class="homepage_popup" style="display:none;">
                	<div class="l">&nbsp;</div>
                    <div class="m">
                    	<div style="padding:20px 10px 20px 10px;">
                        	<div style="padding-top:15px;">Thank you for subscription, do you want to register as user to get free online services provided by us?</div>
                            <div style="padding-top:35px;">
                            <div style="float:right;">
                            	<input type="button" value="No" class="enter_button" onclick="javascript:closeSubsPopup('en');" />
                            </div>
                             <div style="float:right;margin-right:30px;">
                            	<input type="button" value="Yes" class="enter_button" onclick="javascript:redirect('<?php echo JRoute::_('index.php?option=com_user&view=welcome_page&Itemid=79') ?>');" />
                            </div>
                            
                            </div>
                        </div>
                    </div>
                    <div class="r">&nbsp;</div>
                	<div style="clear:both"></div>
                	</div>