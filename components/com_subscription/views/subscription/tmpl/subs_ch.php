﻿<?php //defined('_JEXEC') or die('Restricted access'); ?>
 <!-- Chinese Version - Start -->
                        <div id="small-module-content" >
                        	<div id="module-content-padding">
                            	<div id="small-module-content-cont">
                                    <div id="small-module-content-text">注册并接收定期简报</div>
                                    <div id="small-module-content-text1"><a href="javascript:subsLang('ch');" style="text-decoration:none;color:#fff;background-color:#ff8200">中文</a> | <a href="javascript:subsLang('en');" style="text-decoration:none;color:#66460C">英文</a></div>
                                </div>
                                
                                <div id="small-module-content-cont">
                                    <div id="small-module-content-text2"><span id="sub_salute">尊称</span><font color="#fa8b11">*</font></div>
                                    <div class="floatleft"><input type="text" id="salute" name="salute" class="newsletter_text" /></div>
                                </div>
                                
                                <div id="small-module-content-cont">
                                    <div id="small-module-content-text2"><span id="sub_name">姓名</span><font color="#fa8b11">*</font></div>
                                    <div class="floatleft"><input type="text" id="name" name="name" class="newsletter_text" /></div>
                                </div>
                                
                                <div id="small-module-content-cont">
                                    <div id="small-module-content-text2"><span id="sub_email">电邮</span><font color="#fa8b11">*</font></div>
                                    <div class="floatleft"><input type="text" id="email" name="email" class="newsletter_text" onkeyup="" value="" /></div>
                                </div>
                                
                                <div id="small-module-content-cont">
                                    <div id="small-module-content-text2">您是</div>
                                    <div id="small-module-content-text3">
                                    	<input type="radio" id="usertype" name="usertype" value="individual" checked="yes" />&nbsp;个人&nbsp;<input type="radio" id="usertype" name="usertype" value="merchant" />&nbsp;商家&nbsp;<input type="radio" id="usertype" name="usertype" value="owner" />&nbsp;屋主&nbsp;<input type="radio" id="usertype" name="usertype" value="agent" />&nbsp;经纪
                                    </div>
                                </div>
                                
                               <div id="small-module-content-cont">
                                    <div id="small-module-content-text2">公司名</div>
                                    <div class="floatleft"><input type="text" name="organization_name" class="newsletter_text" /></div>
                               </div>
                               
                               <div id="small-module-content-cont1">
                                    <div id="small-module-content-text4" >
                                    	<div style="float:left;">*表示必须填写</div>
                                        <div id="subscribeLoader" style="float:left;margin-left:10px;display:none;">
                    						<img src="<?php echo JRoute::_('templates/main/images/ajax-loader.gif'); ?>"  />
                						</div>
                                        </div>
                                    <input type="hidden" id="validInput" name="validInput" value="0"  />
                                    <div class="floatleft"><input type="button" value="提交" class="enter_button" onclick="javascript: validationSubscription('ch');return false;" /></div>
                                    <div style="clear:both"></div>
                                    <div id="subs-err"></div>
                                   
                               </div>
                        	</div>
                        </div>

                        <!-- Chinese Version - end -->
                        
                    <div id="homepage_popup_ch" class="homepage_popup" style="display:none;">
                	<div class="l">&nbsp;</div>
                    <div class="m">
                    	<div style="padding:20px 10px 20px 10px;">
                        	<div style="padding-top:15px;">非常感谢您的注册，您是否要注册成为免费用户以使用网站全面的免费功能?</div>
                            <div style="padding-top:35px;">
                            <div style="float:right;">
                            	<input type="button" value="否" class="enter_button" onclick="javascript:closeSubsPopup('ch');" />
                            </div>
                             <div style="float:right;margin-right:30px;">
                            	<input type="button" value="是" class="enter_button" onclick="javascript:redirect('<?php echo JRoute::_('index.php?option=com_user&view=welcome_page&Itemid=79') ?>');" />
                            </div>
                            
                            </div>
                        </div>
                    </div>
                    <div class="r">&nbsp;</div>
                	<div style="clear:both"></div>
                	</div>
                        
                        