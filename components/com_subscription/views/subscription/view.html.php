﻿<?php
/**
* @version		$Id: view.html.php 10711 2008-08-21 10:09:03Z eddieajau $
* @package		Joomla
* @subpackage	Poll
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Poll component
 *
 * @static
 * @package		Joomla
 * @subpackage	Poll
 * @since 1.0
 */
class IhouseViewIhouse extends JView
{
	function display($tpl = null)
	{
		global $mainframe, $option;
		$document =& JFactory::getDocument();

		//$document->addScript(JURI::root(true).'/components/com_ihouse/assets/jquery-1.3.2.js');
		//$document->addScript(JURI::root(true).'/components/com_ihouse/assets/jquery-ui-1.7.1.custom.min.js');
		//$document->addScript(JURI::root(true).'/components/com_ihouse/assets/slider.js');
		
		$user		=& 	JFactory::getUser();
		
		$uid		=	$user->get('id');	
		
		$gid		=	$user->get('gid');
		
		$layout		= 	JRequest::getCmd('layout', 'default');
		
		$db 	  	=& 	JFactory::getDBO();
		
		$limitstart =	JRequest::getCmd('limitstart', 0);
		$limit		=	JRequest::getCmd('limit', 10);
		
		$sector		=	JRequest::getVar('sector');

		/* NEW Condos Module */
		$searchtype	=	JRequest::getCmd('searchtype', '');
		

		$where 		= array();
		$where_or 	= array();
		$sentosa 	= '';
		
		if($searchtype == 'new') {
			$where[] = ' LOWER(a.property_category) LIKE '.$db->Quote( '%'.$db->getEscaped( '新房', true ).'%', false );
		}
		
		if ($sector) 
		{
			$arr_sector = explode('|',$sector);
			
			foreach($arr_sector as $s) :
				$t = explode(',',$s);
				$sct 	= 	$t[0];
				$pl		=	$t[1];
				
				if(!empty($pl)) 
					$sentosa = ' AND property_category = '. $db->Quote( $db->getEscaped( '圣淘沙', true ), false );
			
				$where_or[] = ' UPPER(property_district_id) = '.$db->Quote( $db->getEscaped( $sct, true ), false );
			endforeach;
			
			$where[] = "(".implode( ' OR ', $where_or ).") ". $sentosa;
			
		}
		
		//$where[] = " i.is_primary = 1 ";
		
		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
		
		/* IMG */
		$query = " SELECT ads_id, sess_id, name FROM #__ihouse_ads_image WHERE is_primary = 1 ";
			$db->setQuery( $query );
			$img_primary = $db->loadAssocList();
			

		switch($layout) {
			case 'property_detail' 	:
				$id 			= 	JRequest::getCmd('id');
				$postcode 		= 	JRequest::getCmd('postcode');
				
				/* Log IP */
				if($this->hasVisitedByIP('property', $id)) {
					$this->addPopularCounter($id);
				}
				
				$document->addScript('http://maps.google.com/maps/api/js?sensor=false&region=GB');
				$document->addScriptDeclaration($this->getGoogleMapAPI($postcode));
				
				$row			= 	$this->getProperty($id);
		
				$interestedProp	=	$this->getPropertyInterestedIn();
				$imgChart 		= 	$this->getHistoricalChartImage($postcode);
				$imgFloorPlan 	= 	$this->getFloorPlanImagesHTML($postcode); 
				
				$price			=	$this->getPropertyAvgPrice($postcode);
				$this->assignRef('price', $price);
				
				$guru			=	$this->getPropertyGuru($id);
				
				$this->assignRef('historical_chart_img', $imgChart);
				$this->assignRef('floor_plan_img', $imgFloorPlan);
				$this->assignRef('property_no',	$id);
				$this->assignRef('data', $row);
				
				$this->assignRef('nearby_mrt', $this->getNearbyStation($row->nearby_mrt));
				$this->assignRef('nearby_school', $this->getNearbySchool($row->nearby_school));
				$this->assignRef('interested_prop', $interestedProp);
				
				break;
			
			case 'default' 			:
			default 				:
			
				/* NEW Condos Module */
				$tsk 	=	JRequest::getCmd('task', '');
				switch($tsk) {
					case 'ad_type':
						$orderby 	=	" ORDER BY t.start_date DESC ";
					
						$query	=	' SELECT a.*, u.name, u.chinese_name, u.company_name ,u.company_logo ,u.phone_1, u.website,u.user_image '
							.	' FROM #__ihouse_ads_type AS t '
							.	' LEFT JOIN #__ihouse_ads AS a ON a.id = t.ads_id '
							.	' LEFT JOIN #__users AS u ON u.id = a.posted_by '
							.	$where
							.	' '
							.	$orderby;
						break;
					default:
						$orderby = ' ORDER BY posting_date DESC ' ;	
					
						$query	=	' SELECT a.*, u.name, u.chinese_name, u.company_name ,u.company_logo ,u.phone_1, u.website,u.user_image '
							.	' FROM #__ihouse_ads AS a '
							.	' LEFT JOIN #__users AS u ON u.id = a.posted_by '
							.	$where
							.	' '
							.	$orderby;

						$db->setQuery( $query , $limitstart, $limit);
						
						$rows = $db->loadObjectList();

						if ($db->getErrorNum())
						{
							echo $db->stderr();
							return false;
						}
					
					
						/* COUNT TOTAL */
						$query = 'SELECT COUNT(id) '
							. ' FROM #__ihouse_ads '
							. $where
						;
						
						$db->setQuery( $query );
						$total = $db->loadResult();
					
						jimport('joomla.html.pagination');
						$pagination = new JPagination( $total, $limitstart, $limit );
					
						//$imgPhoto		= $this->getPropertyPhoto($postcode);
					
						//$this->assignRef('photo_img', $imgPhoto);
						$this->assignRef('items',		$rows);
				
						$this->assignRef('user',		JFactory::getUser());
						$this->assignRef('lists',		$lists);
	
						$this->assignRef('pagination',	$pagination);
					break;
				}
				
				break;
		}
		
		$this->assignRef('db', $db);
		//echo print_r($img_primary);
		//$this->assignRef('img_primary', $img_primary);
		parent::display($tpl);
		
	}
	
	
	function getPropertyHighlight($id = 0) {
		$db 	  		=& 	JFactory::getDBO();
		
		$query			= 	"SELECT a.*, i.img1 FROM #__ihouse_ads_type AS t "
							.	" LEFT JOIN #__ihouse_ads AS a ON a.id = t.ads_id "
							.	" LEFT JOIN #__ihouse_ads_image AS i ON i.ads_id = t.ads_id "
							.	" WHERE t.ads_type_config = 5 "
							.	" AND t.property_id = '$id' "
							.	" LIMIT 8";
			$db->setQuery( $query );
			$lists = $db->loadAssocList();
			
		return $lists;					
	}
	
	function getPropertyGuru($id = 0) {
		$db 	  		=& 	JFactory::getDBO();
		
		$query			= 	"SELECT * FROM #__ihouse_ads_type AS t "
							.	" LEFT JOIN #__ihouse_property AS p ON p.id = t.property_id "
							.	" WHERE t.ads_type_config = 1 "
							.	" AND t.property_id = '$id' "
							.	" LIMIT 1";
		
			$db->setQuery( $query );
			$list = $db->loadObject();
			
		return $list;	
	}
	
	function getPropertyAvgPrice($postcode) {
		$db 	  		=& 	JFactory::getDBO();
		
		$query			= 	"SELECT price FROM #__ihouse_property_price "
							.	" WHERE postcode = '$postcode' ";
		
		
			$db->setQuery( $query );
			$list = $db->loadResult();
			
		return $list;	
	}
	
	function getNearbyStation($station) {
		$html = '';
		
		$tmp	=	explode('|', $station);
		
		foreach($tmp as $t) :
			$html .= '<div id="static-text" style="float:left;font-size:12px;">'.$t.'</div>';
			$html .= '<div style="clear:both;"></div>';
		endforeach;
		
		return $html;
	}
	
	function getNearbySchool($school){
		$html = '';
		
		$tmp	=	explode('|', $school);
		
		foreach($tmp as $t) :
			$html .= '<div id="static-text" style="float:left;font-size:12px;">'.$t.'</div>';
			$html .= '<div style="clear:both;"></div>';
		endforeach;
		
		return $html;
	}
	/* Based on Price and RoomType */
	
	function getPropertyInterestedIn() {
		$db 	  		=& 	JFactory::getDBO();
		
		$query			= 	"SELECT p.*,pp.price AS price_psm  FROM #__ihouse_property AS p "
							.	" LEFT JOIN #__ihouse_property_price AS pp ON pp.postcode = p.postcode "
							.	" LIMIT 6";
		
			$db->setQuery( $query );
			$lists = $db->loadObjectList();
		
		return $lists;
			
	}
	
	
	function getFloorPlanImagesHTML($postcode = '') {
		
		$html_tabs = '';
		$html = ''; 
		
		if(empty($postcode))
			return false;
		
		$db 	  		=& 	JFactory::getDBO();
		
		$query	= "SELECT * FROM #__ihouse_floorplan ";
			$db->setQuery( $query );
			$floorplan = $db->loadObjectList();

		$query	= "SELECT floor_plan FROM #__ihouse_property WHERE postcode = ".$postcode;
			$db->setQuery( $query );
			$result = $db->loadResult();
	
		$room_type_ch = explode('|', $result);
	
		$pic = array();
	
		$default = '1brm'; // display : '';
		
		/* MUST CHANGE -- to accepting TABS */
		
		$html_tabs .= '<ul class="tab_nav4">';
	
		foreach($floorplan as $fp) :
			$floorplan_path = 'images/ihouse/postcode/'.$postcode.'/floorplan/'.$fp->type_en;
			
			if(is_dir($floorplan_path)) {
				
				$style_mode = '';
				if($default == $fp->type_en)
					$style_mode = 'class="tabactive4"';
				
				$html_tabs .= '<li id="bed_'.$fp->type_en.'" style="margin-left:5px;cursor:pointer;" '.$style_mode.' ><a href="javascript:displayFloorPlanTypeTabs(\''.$fp->type_en.'\')">'.$fp->type_ch.'</a></li>';
				
				if ($handle = opendir($floorplan_path)) {
					
					$displ_mode = 'style="display:none;" ';
					
					if($default == $fp->type_en)
						$displ_mode = 'style=""';
					
					$html .= '<div class="displFloorPlan" id="'.$fp->type_en.'_'.$postcode.'" '.$displ_mode.' >';
					
		    		while (false !== ($file = readdir($handle))) {
		       			if(is_dir($file)) {
						
							continue;
						
						}else {
							
							$imgfile = $floorplan_path.'/'.$file;
							$html .= '<div style="float:left;padding:5px;width:95px;white-space:nowrap;">';
							$html .= '<a style="color:#062284" href="'.$imgfile.'" rel="rokbox" title="'.$fp->type_en.' '.$postcode.' :: '.$file.'" style="cursor:pointer;">';
							$html .= '<center><img class="fp_img" src="'.$imgfile.'" width="80px" height="60px;" /><br />';
							$html .= $file.'</center>';
							$html .= '</a>';
							$html .= '</div>';
						}
		    		}
					
					$html .= '<div style="clear:both"></div>';
					$html .= '</div>';
				} 
			}
		endforeach;
		$html_tabs .= '</ul>';
		
		$html_tabs .= '<div style="clear:both">&nbsp;</div>'.$html;
		
		return $html_tabs;
		
	}
	
	function getHistoricalChartImage($postcode = '') {
		
		if(empty($postcode))
			return false;
		
		$graph_path = JRoute::_('images/ihouse/history_graph/'.$postcode.'.jpg');
		$result 	=  '<img src="'.$graph_path.'" alt="history_graph_'.$postcode.'" />';
		
		if(file_exists($graph_path)) {
			return $result;
		}
		
		$db			=& JFactory::getDBO();
		
		$query 		= "SELECT * FROM #__ihouse_history "
						. " WHERE postcode = '$postcode'";
			
			$db->setQuery( $query );
			
			$lists = $db->loadAssocList(); /* month_year, postcode, avg_psm */

		$data = array();
		
		foreach($lists as $s) :
			$data[strtolower($s['month_year'])] = $s['avg_psm'];
		endforeach;
		
		if(!empty($data)) {
		
			$max_value = max($data);
			$min_value = min($data);
		
		} else {
			
			$max_value = 0;
			$min_value = 0;
		}
		

		
		$month_x = array();
		$year_x = array();
		$month_year_x = array();
		
		if(!empty($lists)) {
			
			$x_axis = array();
			$j = 0;
			for($i = 4; $i >= -16; $i--) : /* 21 cols */
				$month_x[] 		= 	date("M",strtotime( strval($i) . " month"));
				$year_x[]		= 	date("Y",strtotime( strval($i) . " month"));
				$month_year_x[]	=	strtolower(date("MY",strtotime( strval($i) . " month")));
				$x_axis[] 		= 	$j*5;
				$j++;
			endfor;
			
			krsort($month_x);
			krsort($year_x);
			krsort($month_year_x);
			
			$psm_y = array();
			foreach($month_year_x as $l) : // e.g. Aug2009
				
				if(!empty($data[$l])) {
					
					$psm_y[] 	= 	$data[$l];
					
				} else {
					
					$psm_y[] = 0;
					
				}
				
			endforeach;
			
			if($max_value <= 10000) {
				$max = 10000;
				$min = 0;
				
				$test = array();
				for($i = 1; $i <= 10; $i++) : /* based on 6000 */
					$test[] = strval($i)."K";
				endfor;
			}
			else 
			{
				$max = 60000;
				$min = 0;
				
				$test = array();
				for($i = 1; $i <= 10; $i++) : /* based on 6000 */
					$test[] = strval(6*$i)."K";
				endfor;
			}
			
			
			/* */
			$y_pt 	= array();
			$xx		= array();
			
			$partly_points = array();
			$o = 0;
			foreach($psm_y as $y) :
				$value	= (int) ($y - $min) / ($max - $min) * 100 ;
				
				if($value != 0) {
					$y_pt[] =  $value;
					$xx[] = $x_axis[$o];
				} else { // if finds 0, skip it, get a new line
					$x1		=	implode(',', 	$xx);
					$y1		=	implode(',', $y_pt);
					
					if(!empty($x1) && !empty($y1)) {
						$partly_points[]	=	$x1.'|'.$y1;
					}
					$y_pt = array(); // reset to get a new array
					$xx = array();
				}
				
				$o++;
					
			endforeach;
			
			$bullets		=	array();
			for($i = 0; $i < count($partly_points); $i++) {
				$bullets[] 	= 	"o,0066FF,".$i.",-1,6";
			}
			
			$real_points = implode('|',$partly_points);
			
			
			$month_x 	= 	implode('|', 	$month_x);
			$year_x		=	implode('|',	$year_x);
			

			$value_y = '0|'.implode('|', $test); 

			
			$url = "http://chart.apis.google.com/chart?cht=lxy&chd=t:".$real_points."&chs=640x300";
			
			
			$url .= "&chxt=x,y,x,y&chxl=0:|".$month_x."|1:|".$value_y."|2:|".$year_x."|3:|(SGD)&chxp=3,100&chxs=0,ff0000,12,0,lt|1,0000ff,10,1,lt";
			$url .= "&chg=5,10,4,1,0,0&chm=".implode('|', $bullets);
			//echo $url;
			
		

			
			$ch = curl_init();
			curl_setopt( $ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1" );
    		curl_setopt( $ch, CURLOPT_URL, $url );
			curl_setopt( $ch, CURLOPT_HEADER, 0);
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1); 
			
			$content = curl_exec( $ch );
			
			$file = fopen($graph_path, 'w+');

        	fwrite($file, $content);

        	fclose($file); 
		
			return $result;
		}
		
		return false;
			
	}
	
	function getPropertyPhoto($ids) {
		/*$loc_path = 'images/ihouse/postcode/'.$postcode.'/images';
		
		if(is_dir(JRoute::_($loc_path)))
			return "yes";
		
		
		$html = '<img src="'. JRoute::_('images/ihouse/postcode/'.$postcode.'/images/').'" />';
		*/
	}
	
	function getProperty($id = 0) {
		
		if(!$id)
			return false;
		
		$db 	  =& JFactory::getDBO();
		
		$query	=	"SELECT * FROM #__ihouse_property "
					. " WHERE id = '$id' ";
		
			$db->setQuery( $query );
		
		return $db->loadObject();
	}

	function hasVisitedByIP($type, $id) {
		
		$ip = getenv('REMOTE_ADDR');
		
		$db 	  =& JFactory::getDBO();
		
		$params	=	"property_id=".$id;
		
		$query	=	"SELECT id FROM #__ihouse_ip_log "
					. " WHERE ip = '$ip'  "
					. " AND type = '$type' "
					. " AND param LIKE '%".$params."%' ";
		
			$db->setQuery( $query ) ;
			$is_exist = $db->loadResult();		
		
		if($is_exist) 
			return false;
		
		
		
		if($type == 'property') {
			
			$query	=	"INSERT INTO #__ihouse_ip_log VALUES(NULL, '$type','$ip','$params')";
			
				$db->setQuery( $query ) ;
				$db->query();
				
			return true;	
		}
			
	}
	
	function addPopularCounter($id = 0) {
		if(!$id)
			return false;
		
		$db 	  =& JFactory::getDBO();
		
		$query	=	"SELECT id FROM #__ihouse_popular "
					.	" WHERE property_id = '$id' LIMIT 1" ;
			
			$db->setQuery( $query ) ;
			$is_exist	=	$db->loadResult();
		
		
		
		if($is_exist) {
			
			$query	=	"UPDATE #__ihouse_popular SET counter = counter + 1"
					.	" WHERE property_id = '$id' " ;
			
				$db->setQuery( $query ) ;
				$db->query();
				
		} else {
			
			$query	=	"INSERT INTO #__ihouse_popular VALUES(NULL, 'property',1,'$id')";
			
				$db->setQuery( $query ) ;
				$db->query();
		}
	}

	function getGoogleMapAPI($postcode){
  		$javascript = "
				var map;
				var geocoder;
		
				function initialize(){
					geocoder = new google.maps.Geocoder();
  					var latlng = new google.maps.LatLng(1.3667,103.8);
  					var options = {
   						zoom: 15,
    					center: latlng,
    					mapTypeId: google.maps.MapTypeId.ROADMAP
  					};
       
  					map = new google.maps.Map(document.getElementById('canvas_map'), options);
					
					
				}
				
				function geocodeGoogleMap() {
					geocoder.geocode( { 'address' : '".$postcode."'}, function(results, status) {
      					if (status == google.maps.GeocoderStatus.OK) {
        					map.setCenter(results[0].geometry.location);
        					var marker = new google.maps.Marker({
            								map: map, 
            								position: results[0].geometry.location
        					});
							
      					} else {
        					alert('Geocode was not successful for the following reason: ' + status);
      					}
    				});
				}
				window.addEvent('domready', function(){ 
					initialize();
					geocodeGoogleMap();
					/*$('google_map_image').setStyle('display','none');*/
				});
";
	    return $javascript;
	}

}
?>
