<?php


// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.controller');


class subscriptionController extends JController
{
	/**
	 * constructor (registers additional tasks to methods)
	 * @return void
	 */
	function __construct()
	{
		parent::__construct();

		// Register Extra tasks
		//$this->registerTask( 'add'  , 'edit' );
		
		// Set the table directory
		//JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ihouse'.DS.'tables');

	}
	
	function display()
	{
		$viewName	= JRequest::getVar('view', 'ihouse', 'default', 'cmd');

		JRequest::setVar('view',$viewName);
		
		$task	= JRequest::getCmd('task');
		
		$document =& JFactory::getDocument();
		
		$viewType	= $document->getType();
		
		$layout = JRequest::getVar('layout', 'default');
		
		// View caching logic -- simple... are we logged in?
		
		$user = &JFactory::getUser();
		
		$viewcache = JRequest::getVar('viewcache',1,'POST','INT');

		$userid = JRequest::getVar( 'id', 0, 'post', 'int' );

		// preform security checks
		/*if ($user->get('id') == 0 || $userid == 0 || $userid <> $user->get('id')) {
			JError::raiseError( 403, JText::_('Access Forbidden') );
			return;
		}
		*/
		
		
		
		if ($user->get('id')) {
			parent::display(false);
		} else {
			
			parent::display(true);
		}
		
		
	}
	
	function subscribe() {
		global $mainframe;
		include_once('assets/json.php');
		
		//make json object...
		
		$json = new Services_JSON();
		
		$model		= &$this->getModel('subscribe');
		
		header('Content-type: application/json');
		
		$tmp	=	$model->getSubscribed();
		
		echo $json->encode(array(	
								 "status"=> $tmp
							));

		$mainframe->close;
		exit;
	}
	
	function checkEmail() {
		global $mainframe;
		include_once('assets/json.php');
		
		//make json object...
		
		$json = new Services_JSON();
		
		$model		= &$this->getModel('subscribe');
		
		header('Content-type: application/json');
		
		$tmp	=	$model->checkEmail();
		
		echo $json->encode(array(	
								 "status"=> $tmp
							));

		$mainframe->close;
		exit;
	}

	function loadForm() {
		global $mainframe;
		
		$lang	=	JRequest::getVar('lang');
		
		if($lang == 'ch') { 
			require('views/subscription/tmpl/subs_ch.php');
		} else if($lang == 'en') {
			require('views/subscription/tmpl/subs_en.php');
		}
		
		$mainframe->close;
		exit;
	}

}