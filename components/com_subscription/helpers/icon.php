<?php
// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Content Component HTML Helper
 *
 * @static
 * @package		Joomla
 * @subpackage	Content
 * @since 1.5
 */
class JHTMLIcon
{
	function print_popup($data)
	{
		$url  = "index.php?option=com_receipt&controller=task&task=generate&cid=".$data->id."&view=form&tmpl=component&print=1&layout=default&page=&option=com_receipt";
		
		$status = 'status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no';

		
			$text = JHTML::_('image.site',  'print.png', '../images/', NULL, NULL, JText::_( 'Print' ) );
		

		$attribs['title']	= JText::_( 'Print' );
		$attribs['onclick'] = "window.open(this.href,'win2','".$status."'); return false;";
		$attribs['rel']     = 'nofollow';

		return JHTML::_('link', JRoute::_($url), $text);
	}

	function print_screen($data)
	{
		
		$text = JHTML::_('image.site',  'print.png', '../images/', NULL, NULL, JText::_( 'Print' ) );
		
		return '<a href="#" onclick="window.print();return false;">'.$text.'</a>';
	}
	
	function pdf($data)
	{
		$url  = "pdf.php?cid=". $data->id;

		$status = 'status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no';

		// checks template image directory for image, if non found default are loaded
		
			$text = JHTML::_('image.site', 'pdf_button.png', '../images/M_images/', NULL, NULL, JText::_('PDF'));
	
		

		$attribs['title']	= JText::_( 'PDF' );
		$attribs['onclick'] = "window.open(this.href,'win2','".$status."'); return false;";
		$attribs['rel']     = 'nofollow';

		return JHTML::_('link', JRoute::_($url), $text, $attribs);
	}
}
