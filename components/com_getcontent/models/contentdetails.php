﻿<?php
/**
* @version		$Id: poll.php 10752 2008-08-23 01:53:31Z eddieajau $
* @package		Joomla
* @subpackage	Credits
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class GetcontentModelContentdetails extends JModel
{
	
	function getConObject($id) {
		global $mainframe;
		$db = & JFactory::getDBO();
		
		$user =& JFactory::getUser();
		$user_id = $user->get('id');

		$obj	=	new StdClass;
		
		$query	=	'SELECT * FROM #__k2_items'
			.   ' WHERE id = "'.$id.'"';
		
			$db->setQuery( $query);
			$list = $db->loadObjectList();
		$obj->rows	=	$list;
		
		return $obj;
	}
	
	function getSecObject($id) {
		global $mainframe;
		$db = & JFactory::getDBO();
		
		$user =& JFactory::getUser();
		$user_id = $user->get('id');

		$obj	=	new StdClass;
		
		$query	=	'SELECT catid FROM #__k2_items'
			.   ' WHERE id = "'.$id.'"';
		
			$db->setQuery( $query);
			$catid = $db->loadResult();
			
		$query	=	'SELECT description FROM #__k2_categories'
			.   ' WHERE id = "'.$catid.'"';
		
			$db->setQuery( $query);
			$desc = $db->loadResult();
			
		//$obj->rows	=	$list;
		
		return $desc;
	}
	
	function getsid($id) {
		global $mainframe;
		$db = & JFactory::getDBO();
		
		$user =& JFactory::getUser();
		$user_id = $user->get('id');

		$obj	=	new StdClass;
		
		$query	=	'SELECT catid FROM #__k2_items'
			.   ' WHERE id = "'.$id.'"';
		
			$db->setQuery( $query);
			$catid = $db->loadResult();
			
		$query	=	'SELECT description FROM #__k2_categories'
			.   ' WHERE id = "'.$catid.'"';
		
			$db->setQuery( $query);
			$desc = $db->loadResult();
			
		if(!empty($desc)){
			$sid = '2';
		} else {
			$sid = '1';
		}
		
		return $sid;
	}
	
	
	
	
}
