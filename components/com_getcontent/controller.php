﻿<?php
/**
 * @version		$Id: controller.php 12538 2009-07-22 17:26:51Z ian $
 * @package		Joomla
 * @subpackage	iHouse360 Ads
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant to the
 * GNU General Public License, and as distributed it includes or is derivative
 * of works licensed under the GNU General Public License or other free or open
 * source software licenses. See COPYRIGHT.php for copyright notices and
 * details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport('joomla.application.component.controller');

/**
 * Ads Component Controller
 *
 * @package		Joomla
 * @subpackage	ihouse360 Ads
 * @since 1.5
 */
class GetContentController extends JController
{
	function content_details() 
	{
		JRequest::setVar('view','content_details'); 
		parent::display();
	}
	
	 function ajaxLoadContent() 
	{
		global $mainframe;
		$model		= &$this->getModel('contentdetails');
		
		$id	= 	JRequest::getVar('id', '');
		
		$db		=	JFactory::getDBO(); 
		$obj	=	$model->getConObject($id);
		
		require('views/content_ajax/tmpl/default.php');
		
		$mainframe->close();
		exit;
	}
	
	function ajaxLoadSecContent() 
	{
		global $mainframe;
		$model		= &$this->getModel('contentdetails');
		
		$id	= 	JRequest::getVar('id', '');
		$sid	= 	JRequest::getVar('sid', '');
		
		$db		=	JFactory::getDBO(); 
		$obj	=	$model->getSecObject($id);
		
		require('views/seccontent_ajax/tmpl/default.php');
		
		$mainframe->close();
		exit;
	}
	
	function ajaxGetSid()
	{
		global $mainframe;
		include_once('assets/json.php');
		$id	= 	JRequest::getVar('id', '');
		//make json object...
		
		$json = new Services_JSON();
		
		$model		= &$this->getModel('contentdetails');
		
		header('Content-type: application/json');
		
		$status		=	$model->getsid($id);
		
		echo $json->encode(array("status" => $status	
								 
							));
		$mainframe->close();
		exit;
		/*global $mainframe;
		$model		= &$this->getModel('contentdetails');
		
		$id	= 	JRequest::getVar('id', '');
		
		$db		=	JFactory::getDBO(); 
		$obj	=	$model->getsid($id);
		
		require('views/getSid/tmpl/default.php');
		
		$mainframe->close();
		exit;*/
	
	}
	
	function services() 
	{
		JRequest::setVar('view','services'); 
		parent::display();
	}
	
	function emailpage()
	{
		global $mainframe;
		$model		= &$this->getModel('sendemail');
		
		$classification		= JRequest::getVar('classification', '', 'post', 'classification');
		$usertype			= JRequest::getVar('usertype', '', 'post', 'usertype');
		$property_req		= JRequest::getVar('property_req', '', 'post', 'property_req');
		$budget_timeframe	= JRequest::getVar('budget_timeframe', '', 'post', 'budget_timeframe');
		$contact_by			= JRequest::getVar('contact_by', '', 'post', 'contact_by');
		$salute				= JRequest::getVar('salute', '', 'post', 'salute');
		$name				= JRequest::getVar('name', '', 'post', 'name');
		$email				= JRequest::getVar('email', '', 'post', 'email');
		$phone				= JRequest::getVar('phone', '', 'post', 'phone');
		
		$body = '<table cellpadding="0" cellspacing="0" border="0" width="100%" id="user-table">
    <tr>
        <td width="200" height="40">
            Classification / 分类
        </td>
        <td width="10">:</td>
        <td width="100" height="40">
            '.$classification.'
        </td>
    </tr>
    <tr>
        <td width="200" height="40">
            You are / 您是
        </td>
        <td width="10">:</td>
        <td width="100" height="40">
            '.$usertype.'
        </td>
    </tr>
    <tr>
        <td width="200" height="40">
            Property Requirements / 产业需求信息
        </td>
        <td width="10">:</td>
        <td width="100" height="40">
            '.$property_req.'
        </td>
    </tr>
    <tr>
        <td width="200" height="40">
            Budget & Timeframe / 预算和时间要求
        </td>
        <td width="10">:</td>
        <td width="100" height="40">
            '.$budget_timeframe.'
        </td>
    </tr>
    <tr>
        <td width="200" height="40">
            Contact by / 联系方式
        </td>
        <td width="10">:</td>
        <td width="100" height="40">
            '.$contact_by.'
        </td>
    </tr>
    <tr>
        <td width="200" height="40">
            Salute (Mr/Ms/Dr) / 尊称
        </td>
        <td width="10">:</td>
        <td width="100" height="40">
            '.$salute.'
        </td>
    </tr>
    <tr>
        <td width="200" height="40">
            Name / 姓名
        </td>
        <td width="10">:</td>
        <td width="100" height="40">
            '.$name.'
        </td>
    </tr>
    <tr>
        <td width="200" height="40">
            Email address / 电邮
        </td>
        <td width="10">:</td>
        <td width="100" height="40">
            '.$email.'
        </td>
    </tr>
    <tr>
        <td width="200" height="40">
            Phone number / 联系电话
        </td>
        <td width="10">:</td>
        <td width="100" height="40">
            '.$phone.'
        </td>
    </tr>
</table>
		
		
		
		';
		
		$config     = &JFactory::getConfig();
		$from       = $config->getValue('mailfrom');
		$fromname = $config->getValue('fromname');
		
		$mail = JFactory::getMailer();
	
		$mail->addRecipient( $from );
		$mail->setSender( array( $from , $fromname ) );
		$mail->setSubject( '我要团购/散客');
		$mail->setBody( $body );
		$mail->IsHTML(true);
	
		//update#gunmail
		$conf =& JFactory::getConfig();
		if($conf->getValue('config.mailer') == 'smtp'){
			$mail->useSMTP(
			$conf->getValue('config.smtpauth'), 
			$conf->getValue('config.smtphost'), 
			$conf->getValue('config.smtpuser'), 
			$conf->getValue('config.smtppass'), 
			$conf->getValue('config.smtpsecure'), 
			$conf->getValue('config.smtpport')
			);
		}
		//update#end
		
		// Send the Mail
		$sent = $mail->Send(); 
		
		// Check for an error
		if ( JError::isError($sent) ) {
			echo "Error occurs";
			return false;
		} else {
			$msg = JText::_( 'Thank you for your enquiry');
			$link = JRoute::_('index.php?option=com_getcontent&view=thankyou&Itemid=92', false);
			$this->setRedirect($link, $msg);
			return true;
		}
	}
	
	function thankyou()
	{
	
	//echo "galo";
		JRequest::setVar('view','thankyou'); 
		parent::display();
	}


}//end class
?>
