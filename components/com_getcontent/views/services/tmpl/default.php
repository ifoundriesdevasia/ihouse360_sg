﻿<?php defined('_JEXEC') or die('Restricted access'); ?>
<?php require_once( JPATH_ROOT.DS.'includes'.DS.'tools.php' ); ?>
<!--Start of component-->

<div id="module-padding3">


<div id="reg_border">
			<div id="reg_header1">
			为您服务
            </div>
</div>

<div id="contentbody">

<!--Start of contact us-->
<div style="padding-bottom:50px;">
<?php 
if(!empty($this->obj)) : ?>
<?php 
foreach($this->obj as $row) : 
?>
<div style="font-weight:bold; font-size:14px;"><a name="contactus"><?php echo $row->title;?></a></div>
<div><?php echo $row->introtext;?></div>

<?php endforeach; ?>
<?php endif; ?>  
</div>
<!--End of contact us-->


<!--Start of ads-->
<div style="padding-bottom:50px;">
<?php 
if(!empty($this->getads)) : ?>
<?php 
foreach($this->getads as $ads) : 
?>
<div style="font-weight:bold; font-size:14px;"><a name="getads"><?php echo $ads->title;?></a></div>
<div><?php echo $ads->introtext;?></div>

<?php endforeach; ?>
<?php endif; ?>  
</div>
<!--End of ads-->



<!--Start of form-->
<div style="padding-bottom:50px;">
<div style="font-weight:bold; font-size:14px;"><a name="purchase">我要团购/散客</a></div>
<div>
<form action="index.php?option=com_getcontent" method="post" name="form2" id="form2" class="form-validate">
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="user-table">
    <tr>
        <td width="120" height="40">
            <label id="company_namemsg" for="company_name">
                <?php echo JText::_( 'Classification / 分类' ); ?>
            </label>
        </td>
        <td width="10">:</td>
        <td width="100" height="40">
            <input type="radio" name="classification" id="classification" value="团购" />团购&nbsp;
            <input type="radio" name="classification" id="classification" value="散客" />散客
        </td>
    </tr>
    <tr>
        <td width="120" height="40">
            <label id="company_namemsg" for="company_name">
                <?php echo JText::_( 'You are / 您是' ); ?>
            </label>
        </td>
        <td width="10">:</td>
        <td width="100" height="40">
            <input type="radio" name="usertype" id="usertype" value="个人" />个人&nbsp;
            <input type="radio" name="usertype" id="usertype" value="公司" />公司
        </td>
    </tr>
    <tr>
        <td width="120" height="40">
            <label id="company_namemsg" for="company_name">
                <?php echo JText::_( 'Property Requirements / 产业需求信息' ); ?>
            </label>
        </td>
        <td width="10">:</td>
        <td width="100" height="40">
            <textarea cols="30" rows="5" name="property_req" id="property_req"></textarea>
        </td>
    </tr>
    <tr>
        <td width="120" height="40">
            <label id="company_namemsg" for="company_name">
                <?php echo JText::_( 'Budget & Timeframe / 预算和时间要求' ); ?>
            </label>
        </td>
        <td width="10">:</td>
        <td width="100" height="40">
            <textarea cols="30" rows="5" name="budget_timeframe" id="budget_timeframe"></textarea>
        </td>
    </tr>
    <tr>
        <td width="120" height="40">
            <label id="company_namemsg" for="company_name">
                <?php echo JText::_( 'Contact by / 联系方式' ); ?>
            </label>
        </td>
        <td width="10">:</td>
        <td width="100" height="40">
            <input type="radio" name="contact_by" id="contact_by" value="Email" />Email&nbsp;
            <input type="radio" name="contact_by" id="contact_by" value="Phone" />Phone&nbsp;
            <input type="radio" name="contact_by" id="contact_by" value="Both" />Both
        </td>
    </tr>
    <tr>
        <td width="120" height="40">
            <label id="company_namemsg" for="company_name">
                <?php echo JText::_( 'Salute (Mr/Ms/Dr) / 尊称' ); ?>
            </label>
        </td>
        <td width="10">:</td>
        <td width="100" height="40">
            <input type="text" name="salute" id="salute" value="<?php if(!empty($this->userdetails)){ echo $this->userdetails[0]->salute; }?>" />
        </td>
    </tr>
    <tr>
        <td width="120" height="40">
            <label id="company_namemsg" for="company_name">
                <?php echo JText::_( 'Name / 姓名' ); ?>
            </label>
        </td>
        <td width="10">:</td>
        <td width="100" height="40">
            <input type="text" name="name" id="name" value="<?php if(!empty($this->userdetails)){ echo $this->userdetails[0]->name; }?>" />
        </td>
    </tr>
    <tr>
        <td width="120" height="40">
            <label id="company_namemsg" for="company_name">
                <?php echo JText::_( 'Email address / 电邮' ); ?>
            </label>
        </td>
        <td width="10">:</td>
        <td width="100" height="40">
            <input type="text" name="email" id="email" value="<?php if(!empty($this->userdetails)){ echo $this->userdetails[0]->email; }?>" />
        </td>
    </tr>
    <tr>
        <td width="120" height="40">
            <label id="company_namemsg" for="company_name">
                <?php echo JText::_( 'Phone number / 联系电话' ); ?>
            </label>
        </td>
        <td width="10">:</td>
        <td width="100" height="40">
            <input type="text" name="phone" id="phone" value="<?php if(!empty($this->userdetails)){ echo $this->userdetails[0]->mobile_contact; }?>" />
        </td>
    </tr>
    <tr>
        <td width="120" height="40">
            <input type="submit" value="Submit" class="contactbutton" />
        </td>
        <td colspan="2">&nbsp;</td>
    </tr>
</table>
<input type="hidden" name="option" value="com_getcontent" />
<input type="hidden" name="Itemid" value="92" />
<input type="hidden" name="view" value="emailpage" />
</div> 
</div>
<!--End of form-->



<!--Start of feedback-->
<div style="padding-bottom:20px;">
<?php 
if(!empty($this->feedback)) : ?>
<?php 
foreach($this->feedback as $feedbk) : 
?>
<div style="font-weight:bold; font-size:14px;"><a name="marketing"><?php echo $feedbk->title;?></a></div>
<div><?php echo $feedbk->introtext;?></div>

<?php endforeach; ?>
<?php endif; ?>  
</div>
<!--End of feedback-->


</div>

</div>
