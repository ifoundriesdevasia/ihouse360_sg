﻿<?php
/**
 * @version		$Id: view.html.php 11646 2009-03-01 19:34:56Z ian $
 * @package		Joomla
 * @subpackage	iHouse360 Static Content
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the iHouse360 component
 *
 * @static
 * @package		Joomla
 * @subpackage	Static Content view
 * @since 1.0
 */
class GetcontentViewServices extends JView
{
	function display($tpl = null)
	{
		global $mainframe;
		$db 	  	=& 	JFactory::getDBO();
		
		$user 	=& JFactory::getUser();
		$id 	= 	$user->get('id');
		

		$this->assignRef('obj', $this->getcontact());
		$this->assignRef('getads', $this->getads());
		$this->assignRef('feedback', $this->getfeedback());
		$this->assignRef('userdetails', $this->getuserdetails($id));
		parent::display($tpl);
	}
	
	
	function getcontact() {
	
		$db 	  	=& 	JFactory::getDBO();
		
	
		$query	=	'SELECT * FROM #__k2_items WHERE id = "63"';
						
			$db->setQuery( $query );
			$list = $db->loadObjectList();
		return $list;	
	}
	
	function getads() {
	
		$db 	  	=& 	JFactory::getDBO();
		
	
		$query	=	'SELECT * FROM #__k2_items WHERE id = "64"';
						
			$db->setQuery( $query );
			$list = $db->loadObjectList();
		return $list;	
	}
	
	function getfeedback() {
	
		$db 	  	=& 	JFactory::getDBO();
		
	
		$query	=	'SELECT * FROM #__k2_items WHERE id = "66"';
						
			$db->setQuery( $query );
			$list = $db->loadObjectList();
		return $list;	
	}
	
	function getuserdetails($id) {
	
		$db 	  	=& 	JFactory::getDBO();
		
	
		$query	=	'SELECT * FROM #__users WHERE id = "'.$id.'"';
						
			$db->setQuery( $query );
			$list = $db->loadObjectList();
		return $list;	
	}
	
}
