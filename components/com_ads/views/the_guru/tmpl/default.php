﻿<?php defined('_JEXEC') or die; ?>
<?php JHTML::_('script', 'ihouse.js', 'components/com_ads/rokbox/'); ?>
<!--Start View-->
<div style="margin-top:-20px;">

<div id="main-search-header-text" style="padding-left:15px;border-width:2px;">会员中心&nbsp;<span style="font-size:15px;font-weight:100;"><i><?php echo $this->users->user_category ?>&nbsp;Centre</i></span></div>


<style type="text/css">
#tablist {
	border-left:3px solid #FF8200;
	border-bottom:3px solid #FF8200;
	border-right:3px solid #FF8200;
	width:99.1%;
	padding:0;
	margin:0;
	min-height:300px;
	height:auto;
	float:left;
}

#tablist1 {
	width:99.1%;
	padding:0;
	margin:0;
	min-height:300px;
	height:auto;
	float:left;
}

#tablist2 {
	border-left:2px solid #eeeeee;
	border-bottom:2px solid #eeeeee;
	border-right:2px solid #eeeeee;
	width:99.3%;
	padding:0;
	margin:0;
	/*min-height:100px;*/
	height:auto;
	float:left;
}

table#prop_det_info td{
	border:1px solid #999;
}

ul.tab_nav3 {
list-style-image:none;
list-style-type:none;
}
</style>

<div style="padding:15px;"><!--start-->

<div id="menu_list_profile_detail">
<ul id="" class="tab_nav5">   
	<li id="tab_li5_account"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=account_info&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Acct. Info</span></a></li>  
     <?php
	if(($this->users->user_category == 'Agent')||($this->users->user_category == 'Owner')||($this->users->user_category == 'Individual')){
	?>
	<li id="tab_li5_profile"><a href="<?php echo JURI::root();?>index.php?option=com_user&view=profiler&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Profile</span></a></li>
    <?php
	}
	if(($this->users->user_category == 'Agent')||($this->users->user_category == 'Owner')){
	?>
	<li id="tab_li5_ads" class="tabactive5"><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=ads_management&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Your Ads</span></a></li>
    <?php
	}
	if($this->users->user_category == 'Agent'){
	?>
    <li id="tab_li5_transaction"><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=transaction&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Transactions</span></a></li>
    <li id="tab_li5_top_up" ><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=top_up&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Top-up</span></a></li>
    <?php
	}
	?>
    <li id="tab_li77_specialist"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=favoriteads&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Favorites</span></a></li>
</ul>   
</div>

<div id="tablist"> 
    <div id="tab_profile" class="tabular1" style="display:none;">
    	<div style="padding:15px;">
            <div>
            	&nbsp;
            </div>
        </div>
    </div><!--tabular1-->
    
    <div id="tab_ads" class="tabular1">
     	<div style="padding: 10px;">
            <div>
            	 
<div id="menu_list_ads_listing">
<ul id="" class="tab_nav77">     
	<li id="tab_li77_listing"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=ads_management&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Listing</span></a></li>
   
	<li id="tab_li77_guru" class="tabactive77"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=the_guru&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">The Guru</span></a></li>
   
    <li id="tab_li77_specialist"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=postads&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Place New Ads</span></a></li>
   
     <li id="tab_li77_specialist"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=renewal&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">My Featured Ads Renew</span></a></li>
   
</ul>   
</div><!--menu_list_profile_detail-->


<div id="tablist1"> 
<div id="tab_guru" class="tabular2">
<form action="index.php?option=com_ads" method="post" name="form2" id="form2">
<div style="padding-top:15px; padding-bottom:15px; float:left; width:632px;"><!--The Guru-->

	<div id="menu_list_ads_listing">
        <ul id="" class="tab_nav99">     
        <li id="tab_li99_adsguru"><span class="chinese_text_menu">The Guru</span></li>
        </ul>
    </div><!--menu_list_profile_detail-->
    
    <div id="tablist2"> 
    	<div style="float:left; width:600px; height:23px; background-color:#f1f1f1; padding-left:15px; padding-right:13px; padding-top:2px;">
        	<div style="float:left; padding-right:20px;">Select Directory: <?php echo $this->codeList;?></div>
            <div style="float:left;">
                Select Property: 
                <select name="propertylist" id="propertylist" class="propertylist"><option selected="selected">--Select Property--</option></select>
                
            </div>
        </div>
        
        <div id="guru_content" style="float:left; width:628px; height:auto; padding:20px 50px;">
            <div style="float:left; width:520px; height:auto;">
        		Please select the district and property type.
            </div>
        </div>
    
        <div id="explainlink">
        [<a href="#" onclick="setVisible('layer1');return false" target="_self">Explain This</a>]
        </div>
        
        <div id="layer1">
          <span id="close"><a href="javascript:setVisible('layer1')" style="text-decoration: none;"><strong>Hide</strong></a></span>
          {modulepos explain_guru}
          <br><br>
        </div><!--layer1-->
       
        <div style="float:left; width:612px; height:23px; background-color:#f1f1f1; padding-left:15px; padding-right:1px; padding-top:2px;">
            <div style="float:right;"><input type="button" value="Subscribe" onClick="javascript:guru_submit(this);" /></div>
        </div>
        
    </div><!--tablist2-->

</div><!--The Guru-->

<div style="padding-top:15px; padding-bottom:15px; float:left; width:632px;"><!--Specialist-->

	<div id="menu_list_ads_listing">
        <ul id="" class="tab_nav99">     
        <li id="tab_li99_adsguru"><span class="chinese_text_menu">Specialist</span></li>
        </ul>
    </div><!--menu_list_profile_detail-->
    
    <div id="tablist2"> 
    	
    	<div style="float:left; width:600px; height:23px; background-color:#f1f1f1; padding-left:15px; padding-right:13px; padding-top:2px;">
        	<div style="float:left; padding-right:20px;">Select Directory: <?php echo $this->scodeList;?></div>
            <div style="float:left;">
                Select Property: 
                <select name="spropertylist" id="spropertylist" class="spropertylist"><option selected="selected">--Select Property--</option></select>
                
            </div>
        </div>
        
        <div id="specialist_content" style="float:left; width:628px; height:auto; padding:20px 50px;">
            <div style="float:left; width:520px; height:auto;">
        		Please select the district and property type.
            </div>
        </div>
        
        <div id="explainlink">[<a href="#" onclick="setVisible('layer2');return false" target="_self">Explain This</a>]</div>
        
        <div id="layer2">
          <span id="close"><a href="javascript:setVisible('layer2')" style="text-decoration: none;"><strong>Hide</strong></a></span>
          {modulepos explain_specialist}
          <br><br>
        </div><!--layer1-->
        
        <div style="float:left; width:612px; height:23px; background-color:#f1f1f1; padding-left:15px; padding-right:1px; padding-top:2px;">
            <div style="float:right;"><input type="button" value="Subscribe" onClick="javascript:specialist_submit(this);" /></div>    
        </div>
        
    </div><!--tablist2-->

</div><!--Specialist-->

<div style="padding-top:15px; padding-bottom:15px; float:left; width:632px;"><!--Condo Specialist-->

	<div id="menu_list_ads_listing">
        <ul id="" class="tab_nav98">     
        <li id="tab_li99_adsguru"><span class="chinese_text_menu">Condo Specialist</span></li>
        </ul>
    </div><!--menu_list_profile_detail-->
    
    <div id="tablist2"> 
    	<?php
			if($this->csslot == '0'){
				if($this->checksubscribe == '0'){
		?>
         <div style="float:left; width:628px; height:auto; padding:20px 50px;">
            <div style="float:left; width:520px; height:auto;">
                <div style="float:left; border-bottom:solid 1px #CCCCCC; width:20px; height:10px; padding:10px 5px;">
                <input type="checkbox" disabled="disabled" />
                </div>
                <div style="float:left; border-left:solid 1px #CCCCCC; border-bottom:solid 1px #CCCCCC; height:6px; width:460px; padding:12px 10px;">
                	Condo Specialist - No more slot. Thanks.
                </div>
            </div>
            <div style="float:left; width:520px; height:auto;">
                <div style="float:left; border-right:solid 1px #CCCCCC; width:20px; height:10px; padding:10px 5px;">&nbsp;</div>
            </div>
        
        </div>
        
        <div id="explainlink">[<a href="#" onclick="setVisible('layer3');return false" target="_self">Explain This</a>]</div>
        
        <div id="layer3">
          <span id="close"><a href="javascript:setVisible('layer3')" style="text-decoration: none;"><strong>Hide</strong></a></span>
          {modulepos explain_condo_specialist}
          <br><br>
        </div><!--layer1-->
        
		<div style="float:left; width:612px; height:23px; background-color:#f1f1f1; padding-left:15px; padding-right:1px; padding-top:2px;">
            <div style="float:right;"><input type="button" value="Subscribe" disabled="disabled" /></div>
        </div>
        <?php
				} else {
		?>
        <div style="float:left; width:628px; height:auto; padding:20px 50px;">
            <div style="float:left; width:520px; height:auto;">
                <div style="float:left; border-bottom:solid 1px #CCCCCC; width:20px; height:10px; padding:10px 5px;">
                <input type="checkbox" disabled="disabled" />
                </div>
                <div style="float:left; border-left:solid 1px #CCCCCC; border-bottom:solid 1px #CCCCCC; height:6px; width:460px; padding:12px 10px;">
                	Condo Specialist - You have subscribed to this featured ads.
                </div>
            </div>
            <div style="float:left; width:520px; height:auto;">
                <div style="float:left; border-right:solid 1px #CCCCCC; width:20px; height:10px; padding:10px 5px;">&nbsp;</div>
            </div>
        
        </div>
        
        <div id="explainlink">[<a href="#" onclick="setVisible('layer4');return false" target="_self">Explain This</a>]</div>
        
        <div id="layer4">
          <span id="close"><a href="javascript:setVisible('layer4')" style="text-decoration: none;"><strong>Hide</strong></a></span>
          {modulepos explain_condo_specialist}
          <br><br>
        </div><!--layer1-->
        
		<div style="float:left; width:612px; height:23px; background-color:#f1f1f1; padding-left:15px; padding-right:1px; padding-top:2px;">
            <div style="float:right;"><input type="button" value="Subscribe" disabled="disabled" /></div>
        </div>
        
        
        <?php
				} //check subscribe when 0 slot
			} else { //when slot available
				if($this->checksubscribe == '0'){
				
				$db = JFactory::getDBO();
				$query = "SELECT params FROM #__ihouse_promotion WHERE id = '4'";
				$db->setQuery($query);
				$params = $db->loadResult();
				
				$discountone = explode("=", $params);
				$discount = $discountone[1];
				
				$after_discount = $this->cscredit * (1 - ($discount));
				$discountpercent = $discount * 100;
		?>
        <div style="float:left; width:628px; height:auto; padding:20px 50px;">
            <div style="float:left; width:520px; height:auto;">
                <div style="float:left; border-bottom:solid 1px #CCCCCC; width:20px; height:10px; padding:10px 5px;">
                	<input type="checkbox" name="cspecialist_slot" id="cspecialist_slot" />
                </div>
                <div style="float:left; border-left:solid 1px #CCCCCC; border-bottom:solid 1px #CCCCCC; height:6px; width:460px; padding:12px 10px;">
                	Condo Specialist - <?php echo round($this->cscredit); ?> credits will be deducted.
                </div>
            </div>
            <div style="float:left; width:520px; height:auto;">
                <div style="float:left; border-right:solid 1px #CCCCCC; width:20px; height:10px; padding:10px 5px;">&nbsp;</div>
                <div style="float:left; height:6px; width:460px; padding:12px 10px; color:#d33a3a;">
                    [Monthly Subscription] - $<?php echo round($after_discount); ?> will be deducted from your credits, after <?php echo $discountpercent; ?>% discount.
                </div>
            </div>
        
        </div>
        
        <div id="explainlink">[<a href="#" onclick="setVisible('layer5');return false" target="_self">Explain This</a>]</div>
        
        <div id="layer5">
          <span id="close"><a href="javascript:setVisible('layer5')" style="text-decoration: none;"><strong>Hide</strong></a></span>
          {modulepos explain_condo_specialist}
          <br><br>
        </div><!--layer1-->
        
		<div style="float:left; width:612px; height:23px; background-color:#f1f1f1; padding-left:15px; padding-right:1px; padding-top:2px;">
            <div style="float:right;"><input type="button" value="Subscribe" onClick="javascript:cspecialist_submit(this);" /></div>
        </div>
        <?php
			} else { //when already subscribe
		?>
        <div style="float:left; width:628px; height:auto; padding:20px 50px;">
            <div style="float:left; width:520px; height:auto;">
                <div style="float:left; border-bottom:solid 1px #CCCCCC; width:20px; height:10px; padding:10px 5px;">
                <input type="checkbox" disabled="disabled" />
                </div>
                <div style="float:left; border-left:solid 1px #CCCCCC; border-bottom:solid 1px #CCCCCC; height:6px; width:460px; padding:12px 10px;">
                	Condo Specialist - You have subscribed to this featured ads.
                </div>
            </div>
            <div style="float:left; width:520px; height:auto;">
                <div style="float:left; border-right:solid 1px #CCCCCC; width:20px; height:10px; padding:10px 5px;">&nbsp;</div>
            </div>
        
        </div>
        
        <div id="explainlink">[<a href="#" onclick="setVisible('layer6');return false" target="_self">Explain This</a>]</div>
        
        <div id="layer6">
          <span id="close"><a href="javascript:setVisible('layer6')" style="text-decoration: none;"><strong>Hide</strong></a></span>
          {modulepos explain_condo_specialist}
          <br><br>
        </div><!--layer1-->
        
		<div style="float:left; width:612px; height:23px; background-color:#f1f1f1; padding-left:15px; padding-right:1px; padding-top:2px;">
            <div style="float:right;"><input type="button" value="Subscribe" disabled="disabled" /></div>
        </div>
        
        <?php
		}//when already subscribe
		?>
        <?php
			}//when slot available
		?>
    </div><!--tablist2-->

</div><!--Condo Specialist-->
<input type="hidden" name="option" id="option" value="com_ads" />
<input type="hidden" name="Itemid" id="Itemid" value="134" />
<input type="hidden" name="view" id="view" value="guru_confirmation_page" /><!--featured_save-->
<input type="hidden" name="plan_id" id="plan_id" value="" />
<input type="hidden" name="ads_type_config" id="ads_type_config" value="" />
<input type="hidden" name="property_id" id="property_id" value="" />
<input type="hidden" name="page_id" id="page_id" value="" />
<script LANGUAGE="JavaScript">
<!--
function guru_submit(submit){
var form = document.form2;
if ( form.guru_slot.checked == false ) { 
alert ( "Please check the Guru box before submit." ); 
return false; 
} 
var a = "5";
document.getElementById('plan_id').value = a;
var b = "1";
document.getElementById('ads_type_config').value = b;
var c = "2";
document.getElementById('page_id').value = c;
form.submit()
}

function nocredits(submit){

alert("You do not have enough credit. Please proceed to top up your credits. Thanks.");
return false;

}

function specialist_submit(submit){
var form = document.form2;
if ( form.specialist_slot.checked == false ) { 
alert ( "Please check the Specialist box before submit." ); 
return false; 
} 

var a = "6";
document.getElementById('plan_id').value = a;
var b = "2";
document.getElementById('ads_type_config').value = b;
var c = "2";
document.getElementById('page_id').value = c;
form.submit()
}

function cspecialist_submit(submit){
var form = document.form2;
if ( form.cspecialist_slot.checked == false ) { 
alert ( "Please check the Condo Specialist box before submit." ); 
return false; 
} 

var a = "7";
document.getElementById('plan_id').value = a;
var b = "3";
document.getElementById('ads_type_config').value = b;
var c = "0";
document.getElementById('property_id').value = c;
var d = "2";
document.getElementById('page_id').value = d;
form.submit()
}

-->
</script>
</form>

</div><!--tab_guru-->

<div id="tab_specialist" class="tabular2" style="display:none;">
</div>

                 
</div>   <!--tablist-->              
            </div>
        </div>
    </div><!--tabular1-->

</div>    

</div><!--End-->



</div>
<!--End View-->
