﻿<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('behavior.tooltip'); ?>
<?php JHTML::_('script', 'ihouse.js', 'components/com_ads/rokbox/'); ?>

<style type="text/css">
#tablist {
	border-left:3px solid #FF8200;
	border-bottom:3px solid #FF8200;
	border-right:3px solid #FF8200;
	width:99.1%;
	padding:0;
	margin:0;
	min-height:300px;
	height:auto;
	float:left;
}

table#prop_det_info td{
	border:1px solid #999;
}

ul.tab_nav3 {
list-style-image:none;
list-style-type:none;
}
</style>
<script type="text/javascript">

function deletebutton(submit) {
	var a = "delete_transaction";
	document.getElementById('view').value = a;
}

function checkAll( n, fldName ) {
 if (!fldName) {
 fldName = 'cb';
 }
 var f = document.form2;
 var c = f.toggle.checked;
 var n2 = 0;
 for (i=0; i < n; i++) {
 cb = eval( 'f.' + fldName + '' + i );
 if (cb) {
 cb.checked = c;
 n2++;
 }
}
 if (c) {
 document.form2.boxchecked.value = n2;
 } else {
 document.form2.boxchecked.value = 0;
 }
} 
function isChecked( checkbox ){
	if (checkbox.checked == true){
		document.form2.boxchecked.value++;
	} else {
		document.form2.boxchecked.value--;
	}
}
</script>

<!--Start View-->
<div style="margin-top:-20px;">

<div id="main-search-header-text" style="padding-left:15px;border-width:2px;">会员中心&nbsp;<span style="font-size:15px;font-weight:100;"><i><?php echo $this->users->user_category ?>&nbsp;Centre</i></span></div>

<div style="padding:15px;"><!--start-->

<div id="menu_list_profile_detail">
<ul id="" class="tab_nav5">     
	<li id="tab_li5_account"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=account_info&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Acct. Info</span></a></li>
    <?php
	if(($this->users->user_category == 'Agent')||($this->users->user_category == 'Owner')||($this->users->user_category == 'Individual')){
	?>
	<li id="tab_li5_profile"><a href="<?php echo JURI::root();?>index.php?option=com_user&view=profiler&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Profile</span></a></li>
    <?php
	}
	if(($this->users->user_category == 'Agent')||($this->users->user_category == 'Owner')){
	?>
	<li id="tab_li5_ads"><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=ads_management&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Your Ads</span></a></li>
    <?php
	}
	if($this->users->user_category == 'Agent'){
	?>
    <li id="tab_li5_transaction" class="tabactive5"><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=transaction&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Transactions</span></a></li>
    <li id="tab_li5_top_up" ><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=top_up&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Top-up</span></a></li>
    <?php
	}
	?>
    <li id="tab_li77_specialist"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=favoriteads&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Favorites</span></a></li>
</ul>   
</div>

<div id="tablist"> 
    <div id="tab_profile" class="tabular1" style="display:none;">
    	<div style="padding:15px;">
            <div>
            	&nbsp;
            </div>
        </div>
    </div><!--tabular1-->
    <div id="tab_ads" class="tabular1" style="display:none;">
     	<div style="padding: 10px;">
            <div>
            	&nbsp;
            </div>
        </div>
    </div><!--tabular1-->
    <div id="tab_account" class="tabular1" style="display:none;">
    	<div style="padding: 10px;">
            <div>
            	&nbsp;
            </div>
        </div>	
    </div><!--tabular1-->
    <div id="tab_transaction" class="tabular1">
    	<div style="padding: 10px;">
            <div>
            	
                <div id="static-text">
                    	交易记录&nbsp;&nbsp;<font style="color:#494949;">Transaction</font>
                </div>

                <div style="float:left; width:100%; height:auto;">
                <form action="index.php?option=com_ads" method="post" name="form2" id="form2">
                	<table cellpadding="5" cellspacing="0" bordercolor="#c1dff9">
                        <tr>
                        <?php /*?><td class="tableheader" width="20" align="center">
                        <div style="font-weight:bold; color:#FFFFFF; font-size:14px;">
                        <input type="checkbox" name="toggle" onclick="checkAll(<?php echo count($this->transactions); ?>);" />
                        </div>
                        </td><?php */?>
                        <td class="tableheader" width="250" align="center"><div style="font-weight:bold; color:#FFFFFF; font-size:14px;">Description</div></td>
                        <td class="tableheader" width="80" align="center"><div style="font-weight:bold; color:#FFFFFF; font-size:14px;">Debit</div></td>
                        <td class="tableheader" width="80" align="center"><div style="font-weight:bold; color:#FFFFFF; font-size:14px;">Credit</div></td>
                        <td class="tableheader" width="150" align="center"><div style="font-weight:bold; color:#FFFFFF; font-size:14px;">Balance</div></td>
                        <td class="tableheader" width="120" align="center"><div style="font-weight:bold; color:#FFFFFF; font-size:14px;">Date</div></td>
                        </tr>
                        <?php
						$k = 0;
						for ($i=0, $n=count( $this->transactions ); $i < $n; $i++)
						{
							$row 	=& $this->transactions[$i];
							$checked=JHTML::_('grid.id', $i, $row->id);
						?>
                        <tr class="<?php echo "row$k"; ?>">
                        <?php /*?><td class="tablerow" width="20" align="center"><?php echo $checked; ?></td><?php */?>
                        <td class="tablerow" width="200" align="center"><?php echo $row->description;?></td>
                        <td class="tablerow" width="100" align="center"><?php echo $row->debit;?></td>
                        <td class="tablerow" width="100" align="center"><?php echo $row->credit;?></td>
                        <td class="tablerow" width="100" align="center"><?php echo $row->balance;?></td>
                        <td class="tablerow1" width="100" align="center"><?php echo $row->datetime;?></td>
                        </tr>
                        <?php
							$k = 1 - $k;
							}
						?>
                   </table>
                   
                   		<div style="padding-top:10px;">
                        	<div style="float:left; padding-right:5px; line-height:22px;">Total [<?php echo count($this->transactions);?>]</div>
                        	<?php /*?><div style="float:left;"><input type="submit" value="Delete" onclick="javascript:if(document.form2.boxchecked.value==0){alert('Please select a transaction from the list to delete');}else{ deletebutton(this)}" /></div><?php */?>
                            <div style="float:right;"><?php echo $this->pagination->getPagesLinks(); ?></div>
                        </div>
                        <input type="hidden" name="option" value="com_ads" />
						<input type="hidden" name="view" value="delete_transaction" />
                        <input type="hidden" name="boxchecked" value="0" />
                   </form>
                </div>
                
                
            </div>
        </div>
    </div><!--tabular1-->
    <div id="tab_top_up" class="tabular1" style="display:none;">
    	<div style="padding: 10px;">
            <div>
            	&nbsp;
            </div>
        </div>
    </div><!--tabular1-->

</div>    




</div><!--End-->
</div>
<!--End View-->