<?php
/**
 * @version		$Id: view.html.php 11646 2009-03-01 19:34:56Z ian $
 * @package		Joomla
 * @subpackage	iHouse360 Transaction
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the iHouse360 component
 *
 * @static
 * @package		Joomla
 * @subpackage	Transaction view
 * @since 1.0
 */
class AdsViewTransaction extends JView
{

		function display($tpl = null)
	{
		global $mainframe, $option;
		
		$id = JRequest::getVar('id', '', 'get', 'id');
		
		$db 	  	=& 	JFactory::getDBO();
		
		$limitstart =	JRequest::getCmd('limitstart', 0);
		$limit		=	JRequest::getCmd('limit', 10);

		$where 	= array();
		
		$where[] = "  user_id = '$id' ";
		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
		
		$query	=	'SELECT *' 
							.	' FROM #__users_transaction_log '
							.	$where
							.	' ORDER BY id DESC';
		
		
		$db->setQuery( $query , $limitstart, $limit);
		
		$transactions = $db->loadObjectList();
		
		if ($db->getErrorNum())
		{
			echo $db->stderr();
			return false;
		}
		
		$query	=	'SELECT COUNT(id) ' 
							.	' FROM #__users_transaction_log '
							.	$where
							.	' ';
			$db->setQuery( $query );
			$total = $db->loadResult();

		jimport('joomla.html.pagination');
		$pagination = new JPagination( $total, $limitstart, $limit );
		
		$this->assignRef('pagination',	$pagination);
		$this->assignRef('users', $this->getInfoOfThisUser($id));
		$this->assignRef('transactions', $transactions);
		
		parent::display($tpl);
	}
	
	function getInfoOfThisUser($id = 0) {
		
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT * FROM #__users "
						.	" WHERE id = '$id'" ;
						
			$db->setQuery( $query );
			$list = $db->loadObject();		
			//echo print_r($list);
		return $list;	
	}
	
}
