<?php
/**
 * @version		$Id: view.html.php 11646 2009-03-01 19:34:56Z ian $
 * @package		Joomla
 * @subpackage	Deduct Credit Management
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Users component
 *
 * @static
 * @package		Joomla
 * @subpackage	Deduct Credit Management
 * @since 1.0
 */
class AdsViewDeduct_credit extends JView
{
	function display($tpl = null)    
	{
		global $mainframe;
		$db = & JFactory::getDBO();
		$user_id	= JRequest::getVar('user_id', '', 'post', 'user_id');
		$plan_id	= JRequest::getVar('plan_id', '', 'post', 'plan_id');
		$credit		= JRequest::getVar('credit', '', 'post', 'credit');
		$sub_credit	= JRequest::getVar('sub_credit', '', 'post', 'sub_credit');
		
		
		$balance = $credit - $sub_credit;
		
		$query = "SELECT validity FROM #__cbsubs_plans WHERE id = '".$plan_id."'";
	
		$db->setQuery($query);
		$validity = $db->loadResult(); //duration

		$date = explode("-", $validity);
		$year = $date[0];
		$month = $date[1];
		$day = substr($date[2], 0, 2);

		$expirydate = date("Y-m-d H:i:s", mktime(date("H"),date("i")+1,date("s"),date("m")+$month,date("d")+$day,date("Y")+$year));

		
		$sql = "UPDATE #__ihouse_users_credit set credit = '".$balance."' WHERE user_id = '".$user_id."'";
		$db->setQuery($sql);
		$db->query();
		
		$query = "SELECT id FROM #__cbsubs_subscriptions WHERE status = 'X' AND user_id = '".$user_id."' AND plan_id = '".$plan_id."'";
		$db->setQuery($query);
		$sub_id = $db->loadResult();
		
		if(isset($sub_id)){ //if owner renew
		
		$sql = "UPDATE #__cbsubs_subscriptions set status = 'A', last_renewed_date = CURRENT_TIMESTAMP, expiry_date = '".$expirydate."' WHERE user_id = '".$user_id."' AND id =".$sub_id;
		$db->setQuery($sql);
		$db->query();
		
		$sql = "UPDATE #__ihouse_ads_subscription_slot set slot = '50', start_date = CURRENT_TIMESTAMP, end_time = CURRENT_TIMESTAMP WHERE user_id = '".$user_id."' AND id =".$sub_id;
		$db->setQuery($sql);
		$db->query();
		
		
		
		} else {
		
		$query = "INSERT INTO #__cbsubs_subscriptions (id,status,user_id,plan_id,parent_plan,parent_subscription,replaces_plan,replaces_subscription,subscription_date,last_renewed_date,expiry_date,next_event_date,next_event_id,next_event_params,autorenew_type,autorecurring_type,regular_recurrings_total,regular_recurrings_used,previous_recurrings_used,previous_expiry_date,previous_status,ip_addresses,integrations)"
	
		. "\n VALUES ( NULL, 'A', '".$user_id."', '".$plan_id."', '0', '0', NULL, NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,'".$expirydate."', NULL, '0', '', '0', '0', '0', '1', '0', NULL, 'R', '".$ip."', '')";
		$db->setQuery( $query );
		$db->query();
		$sid = mysql_insert_id();
		
		$query = "INSERT INTO #__ihouse_ads_subscription_slot(id,user_id,slot,start_date,end_date)"
	
		. "\n VALUES ( NULL, '".$user_id."', '50', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
		$db->setQuery( $query );
		$db->query();
		
		}

	}
	
}
