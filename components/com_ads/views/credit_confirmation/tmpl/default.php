﻿<?php defined('_JEXEC') or die; ?>
<?php JHTML::_('script', 'ihouse.js', 'components/com_ads/rokbox/'); ?>
<!--Start View-->
<div style="margin-top:-20px;">

<div id="main-search-header-text" style="padding-left:15px;border-width:2px;">会员中心&nbsp;<span style="font-size:15px;font-weight:100;"><i><?php echo $this->users->user_category ?>&nbsp;Centre</i></span></div>

<style type="text/css">
#tablist {
	border-left:3px solid #FF8200;
	border-bottom:3px solid #FF8200;
	border-right:3px solid #FF8200;
	width:99.1%;
	padding:0;
	margin:0;
	min-height:300px;
	height:auto;
	float:left;
}

table#prop_det_info td{
	border:1px solid #999;
}

ul.tab_nav3 {
list-style-image:none;
list-style-type:none;
}
</style>

<div style="padding:15px;"><!--start-->

<div id="menu_list_profile_detail">
<ul id="" class="tab_nav5">     
	<li id="tab_li5_account"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=account_info&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">账号信息</span></a></li>
     <?php
	if(($this->users->user_category == 'Agent')||($this->users->user_category == 'Owner')||($this->users->user_category == 'Individual')){
	?>
	<li id="tab_li5_profile"><a href="<?php echo JURI::root();?>index.php?option=com_user&view=profiler&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">个人资料</span></a></li>
    <?php
	}
	if(($this->users->user_category == 'Agent')||($this->users->user_category == 'Owner')){
	?>
	<li id="tab_li5_ads"><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=ads_management&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">房地产广告</span></a></li>
    <?php
	}
	if($this->users->user_category == 'Agent'){
	?>
    <li id="tab_li5_transaction"><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=transaction&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">交易记录</span></a></li>
    <li id="tab_li5_top_up" class="tabactive5"><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=top_up&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">充值</span></a></li>
    <?php
	}
	?>
</ul>   
</div>

<div id="tablist"> 
    <div id="tab_top_up" class="tabular1">
    	<form action="index.php?option=com_ads" method="post" name="form2" id="form2">
    	<div style="padding: 20px;">
            <div>
            	<div style="float:left; width:385px; height:auto;">
            	
                <div id="static-text">
                    	充值&nbsp;&nbsp;<font style="color:#494949;">Top-Up</font>
                </div>
                
                <div style="float:left; width:385px;"><br />
                Click on Continue if you have confirmed the below amounts and you will be redirected to Paypal for payment.<br /><br />
                
                	Top-up amount : $<?php echo $this->top_up_amt; ?><br /><br />
                    <?php if(!empty($this->evoucherno)){ ?>E-voucher discount : <?php echo $this->discount;?>%<br /><br /><?php } ?>
                    Payable amount: $<?php echo $this->actual_payment;?><br /><br />
                    
                    
                    
                </div>
                
                </div>
                
                <div style="float:right; width:225px; height:auto; padding-top:20px;">
                	<img src="<?php echo JURI::root();?>templates/main/images/paypal_img.jpg" />
                </div>
                
            </div>
            
            <div style="float:left; width:550px;">
                	<?php if($this->more_credit != '0'){ ?>
                    * <?php echo $this->more_credit;?> more credits will be added to your credit bank for $<?php echo $this->top_up_amt; ?> of top-up.
                    <?php } ?>
                    <?php if(empty($this->evoucherno)){ ?>
                	<?php } else if(((!empty($this->evoucherno))&&($this->discount == '0'))||($this->user_used != 'no')){ ?>
                    <br /><br />
                	<b>* The E-voucher number that you have entered is invalid / The E-voucher has already been used.</b>
                	<?php } ?>
            </div>
            
            <div style="float:left; width:100%; padding-top:20px; padding-bottom:20px; color:#FF0000;">
            	<input type="submit" value="Continue" class="contactbutton" name="agent_pay" id="agent_pay"/>
            </div>
        </div>
        		<input type="hidden" name="option" id="option"  value="com_ads" />
                <input type="hidden" name="view" id="view" value="paymentprocess" />
                <input type="hidden" name="Itemid" id="Itemid" value="126" />
                <input type="hidden" name="payment_mode" id="payment_mode" value="credits" />
                <input type="hidden" name="evoucherno" id="evoucherno" value="<?php echo $this->evoucherno;?>" />
                <input type="hidden" name="top_up_amt" id="top_up_amt" value="<?php echo $this->top_up_amt;?>" />
                <input type="hidden" name="actual_amt" id="actual_amt" value="<?php echo $this->actual_payment;?>" />
                <input type="hidden" name="discount" id="discount" value="<?php echo $this->discount;?>" />
                <input type="hidden" name="user_id" id="user_id" value="<?php echo $this->users->id;?>" />
                <input type="hidden" name="tempid" id="tempid" value="<?php echo $this->tempid;?>" />
                <input type="hidden" name="user_used" id="user_used" value="<?php echo $this->user_used;?>" />
        </form>
    </div><!--tabular1-->

</div>   

</div><!--End-->



</div>
<!--End View-->
