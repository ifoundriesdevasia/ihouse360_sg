<?php
/**
 * @version		$Id: view.html.php 11646 2009-03-01 19:34:56Z ian $
 * @package		Joomla
 * @subpackage	Credit Confirmation page
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Form component
 *
 * @static
 * @package		Joomla
 * @subpackage	Credit Confirmation page
 * @since 1.0
 */
class AdsViewCredit_confirmation extends JView
{
	function display($tpl = null)
	{
		global $mainframe;
		$db 	  	=& 	JFactory::getDBO();
		$user =& JFactory::getUser();
	 	$id = $user->get('id');
	 
		$payment_mode	= JRequest::getVar('payment_mode', '', 'post', 'payment_mode');
		$top_up_amt		= JRequest::getVar('top_up_amt', '', 'post', 'top_up_amt');
		$evoucherno		= JRequest::getVar('evoucherno', '', 'post', 'evoucherno');
		$tempid			= JRequest::getVar('tempid', '', 'post', 'tempid');
		
		$prefix = substr($evoucherno, 0, 2);
		$voucher_id = substr($evoucherno, 2, 4);
		$voucher_no = substr($evoucherno, 6, 16);
		
		$query="SELECT voucher_key FROM #__ihouse_evoucher_list WHERE prefix = '".$prefix."'";
		$db->setQuery($query);
		$voucher_key = $db->loadResult();
		
		$correct_key = substr(md5($prefix.($voucher_id * $voucher_key)),0,16);
		
		if($voucher_no == $correct_key){
		
		$query = "SELECT discount FROM #__ihouse_evoucher_user WHERE evoucher_num = '".$evoucherno."'";
		$db->setQuery($query);
		$percent = $db->loadResult(); //e-voucher discount
		
		$query="SELECT user_used FROM #__ihouse_evoucher_user WHERE evoucher_num = '".$evoucherno."'";
		$db->setQuery($query);
		$user_used = $db->loadResult();//user used before or not
		
		} else {
		$percent = '';
		}
		
		if((!empty($evoucherno))&&($percent != '')&&($user_used == 'no')){
		$discount = $percent;
		} else {
		$discount = '0';
		}
		
		if($top_up_amt == '300'){
		$more_credit = $top_up_amt * 0.03;
		} else if($top_up_amt == '500'){
		$more_credit = $top_up_amt * 0.05;
		} else {
		$more_credit = '0';
		}
		
		if((!empty($evoucherno))&&($percent != '')&&($user_used == 'no')){
		$actual_payment = $top_up_amt * (1 - ($discount / 100)); 
		} else {
		$actual_payment = $top_up_amt;
		}
	
		$this->assignRef('users', $this->getInfoOfThisUser($id));
		$this->assignRef('payment_mode', $payment_mode);
		$this->assignRef('top_up_amt', $top_up_amt);
		$this->assignRef('evoucherno', $evoucherno);
		$this->assignRef('discount', $discount);
		$this->assignRef('more_credit', $more_credit);
		$this->assignRef('actual_payment', $actual_payment);
		$this->assignRef('user_used', $user_used);
		$this->assignRef('tempid', $tempid);
		parent::display($tpl);
		
	}
	
	function getInfoOfThisUser($id = 0) {
		
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT * FROM #__users "
						.	" WHERE id = '$id'" ;
						
			$db->setQuery( $query );
			$list = $db->loadObject();		
			//echo print_r($list);
		return $list;	
	}
}
