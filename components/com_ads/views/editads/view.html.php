﻿<?php
/**
 * @version		$Id: view.html.php 11646 2009-03-01 19:34:56Z ian $
 * @package		Joomla
 * @subpackage	iHouse360 Sales posting form
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the iHouse360 component
 *
 * @static
 * @package		Joomla
 * @subpackage	Sales posting form view
 * @since 1.0
 */
class AdsViewEditads extends JView
{
	function display($tpl = null)
	{
		global $mainframe;
		
		$user 			=& 	JFactory::getUser();
		$uid			= $user->id;
		$user 			=& 	JFactory::getUser($uid);
		
		$layout = JRequest::getCmd('layout');
		
		$ads_id = JRequest::getVar('selectid',0);
		
		$row = $this->getAdsObject($ads_id);
		
		$property_type_en = '';
		switch($row->property_type) {
			case '公寓'		: $property_type_en = 'condo'; break;
			case '组屋'		: $property_type_en = 'hdb'; break;
			case '服务公寓'	: $property_type_en = 'apartments'; break;
			case '商业'		: $property_type_en = 'commercials'; break;
			case '有地住宅'	: $property_type_en = 'landed'; break;
		}
		
		$this->assignRef('ads', $row);
		
		$session_id = $this->getSessionId($row->ads_id);
		$this->assignRef('session_id', $session_id);
		$this->assignRef('property_type', $this->propertyTypeHTML($row->property_type));
		
		$fp1	=	$this->getFloorPlan($row, 1);

		$this->assignRef('fp1',$this->getFloorPlanList($row,'fp1',1));
		if(!empty($fp1))
			$this->assignRef('fp2', $this->getFloorPlanList($row, 'fp2', 2));
		else {
			$fp2_html = '<select name="fp2"></select>';
			$this->assignRef('fp2', $fp2_html);
		}
		
		
		
		$this->assignRef('fp1_image',$this->getFloorPlan($row, 1));
		$this->assignRef('fp2_image',$this->getFloorPlan($row, 2));
		
		$model =& JModel::getInstance('property','AdsModel');
		$model1 =& JModel::getInstance('ads','AdsModel');
		
		//echo $layout;
		//$this->assignRef('plan_id', JRequest::getInt('plan_id', 0) );
		
		
		$property_category	=	$model->getPropertyCategoryListHTML($row->property_category, $property_type_en);
		$property_name		=	$model->getPropertyNameHTML('',$row->property_id);
		$project_name		=	$model->getProjectNameHTML($row->property_name, $property_type_en); // comm | landed | apartments
		
		$district		=	$model->getPropertyDistrictHTML($row->property_district_id, $property_type_en);
		$tenure			=	$model->getTenureListHTML($row->tenure, $property_type_en);

		$year_built		=	$model->getTopYearListHTML($row->top_year, $property_type_en);
		$postcode		=	$model->getPostcodeHTML($row->postcode, $property_type_en);
		
		$flattype		=	$model->getFlatTypeListHTML($row->flat_type);
		$asking_price 	= 	$model->getAskingPriceHTML($row->ask_price);
		$psm_price		=	$model->getPSMPriceHTML($row->psm_price);
		$psf_price_calc	=	(int) $row->psm_price / 10.764;
		$psf_price		=	$model->getPSFPriceHTML($psf_price_calc);
		$size			=	$model->getSizeHTML($row->size);
		
		$no_of_room		=	$model->getNoOfRoomListHTML($row->no_of_room);
		$no_of_hall		=	$model->getNoOfHallListHTML($row->no_of_hall);
		$study_room		=	$model->getNoOfStudyRoomListHTML($row->study_room);
		$toilet			=	$model->getNoOfToiletListHTML($row->toilet);
		$floor_level	=	$model->getFloorLevelListHTML($row->floor_level);
		$renovation		=	$model->getRenovationListHTML($row->renovation);
		$status			=	$model->getStatusListHTML($row);
		
		$block_no		=	$model->getBlockNoHTML($row->block_no);
		$floor_unit_no	=	$model->getFloorUnitNoHTML($row->floor_unit_no);
		$street_name	=	$model->getStreetNameHTML($row->street_name, $property_type_en);
		$country		=	$model->getCountryHTML($row->country);
		$ad_title		=	$model->getAdTitleHTML($row->ad_title);
		$add_info		=	$model->getAddInfoHTML($row->add_info);
		
		$ad_type		=	$model->getAdTypeLISTHTML($row->ad_type);
		$rental_type	=	$model->getRentalTypeListHTML($row->rental_type);
		
		$image1			=	$this->getUserUploadImage($row->ads_id, $row->sess_id);
		$image2			=	$model1->getImagePhotosHTML($row->postcode, $row->ads_id);
		
		if($row->ad_type == 'sell') {
			$rental_type	= 'N/A';
		}
		
		/* HDB */
		$hdb_town_list 	= 	$model->getHDBTownListHTML($row->hdb_town_id);
		
		$this->assignRef('property_category', $property_category);
		$this->assignRef('property_name', $property_name);
		$this->assignRef('project_name', $project_name);
		
		$this->assignRef('flattype', $flattype);
		$this->assignRef('asking_price', $asking_price);
		$this->assignRef('psm_price', $psm_price);
		$this->assignRef('psf_price', $psf_price);
		$this->assignRef('size', $size);
		
		$this->assignRef('block_no', $block_no);
		$this->assignRef('floor_unit_no', $floor_unit_no);
		$this->assignRef('street_name', $street_name);
		$this->assignRef('country', $country);
		$this->assignRef('ad_title', $ad_title);
		$this->assignRef('add_info', $add_info);
		
		$this->assignRef('ad_type', $row->ad_type);
		//$this->assignRef('ad_type', $ad_type);
		//$this->assignRef('rental_type', $row->rental_type);
		$this->assignRef('rental_type', $rental_type);
		$this->assignRef('district', $district);
		$this->assignRef('tenure', $tenure);
		$this->assignRef('year_built', $year_built);
		$this->assignRef('postcode', $postcode);
		
		$this->assignRef('floor_level', $floor_level);
		$this->assignRef('no_of_room', $no_of_room);
		$this->assignRef('no_of_hall', $no_of_hall);
		$this->assignRef('study_room',$study_room );
		$this->assignRef('toilet', $toilet );
		$this->assignRef('renovation', $renovation);
		$this->assignRef('status', $status);
	
		$this->assignRef('image1', $image1);
		$this->assignRef('image2', $image2);
		
		$this->assignRef('hdb_town', $hdb_town_list);
		$this->assignRef('ads_id', $ads_id);
		
		$this->assignRef('watermark_text', $this->getWatermarkText($user));
		
		parent::display($tpl);
		
	}
	
	function getWatermarkText($user) {
		$db		=& 	JFactory::getDBO();
		$uid	=	$user->get('id');
		
		$query 	= " SELECT word FROM #__ihouse_watermark WHERE user_id = '$uid' ";
			$db->setQuery( $query ) ;
			$result = $db->loadResult();
		
		return $result;
	}
	
	function getAdsObject($id) {
		
		$db		=& 	JFactory::getDBO();
		
		$query 	= " SELECT * FROM #__ihouse_ads AS a " 
					.	" LEFT JOIN #__ihouse_ads_session AS s ON s.ads_id = a.id "
					.	" WHERE a.id = '$id' ";
					
			$db->setQuery( $query ) ;
			$row = $db->loadObject();
		//echo print_r($row);
		return $row;
	}
	
	function propertyTypeHTML($type) {
		
		$prop = '';
		switch($type) {
			case '公寓' :
				$prop = 'Condo';
				break;
			case '组屋' :
				$prop = 'HDB';
				break;
			case '有地住宅' :
				$prop	=	'Landed';
				break;
			case '商业' :
				$prop	=	'Commercials';
				break;
			case '服务公寓' :
				$prop	=	'Service Apartments';
				break;
			default :
				$prop = '---';
				break;
		}
		return $prop;
	}
	
	function getUserUploadImage($ads_id, $session_id) {
		$db		=& 	JFactory::getDBO();
		
		$query  = " DELETE FROM #__ihouse_ads_image WHERE ads_id = 0 AND sess_id = '$session_id' ";
		
			$db->setQuery( $query ) ;
			$db->query();
		
		$query 	= " SELECT * FROM #__ihouse_ads_image WHERE ads_id = '$ads_id' ";
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		$html = '';
		foreach($rows as $r):
		
			$html .= '<div id="file-'.$r->id.'" style="float:left;margin:1px;">';
			$html .= '<div><img src="'.JRoute::_('images/ihouse/ads_images/'.$r->sess_id.'/'.$r->name).'" width="159px" height="126px" />';
    		$html .= '</div>';
			$html .= '<div style="text-align:center;" ><input type="radio" name="primary_photo" value="'.$r->id.'" onclick="javascript:primaryPhotoAjax(this.value,\''.$r->sess_id.'\')" '.(($r->is_primary)? 'checked="yes"' :'').' />Primary Photo<br /><input type="image" src="'.JRoute::_('templates/main/images/btn-delete.png').'" alt="Delete now!" onclick="javascript:deleteTmpAds('.$r->id.',\''.$r->name.'\',\''.$r->sess_id.'\');return false;" /></div>';
			$html .= '</div>';	
		
		endforeach;
		
		return $html;
	}
	
	function getSessionId($ads_id) {
		$db		=& 	JFactory::getDBO();

		$query 	= " SELECT sess_id FROM #__ihouse_ads_session WHERE ads_id = '$ads_id' ";
			$db->setQuery( $query ) ;
			$sess_id = $db->loadResult();
		
		return $sess_id;
	}
	
	function getFloorPlanList($row, $name_id , $ordering) {
		$db		=& 	JFactory::getDBO();

		$ads_id 	= $row->ads_id;
		$postcode 	= $row->postcode;
		
		$query 		= " SELECT fp FROM #__ihouse_ads_floorplan WHERE ads_id = '$ads_id' AND ordering = '$ordering' ";
			$db->setQuery( $query ) ;
			$fpss = $db->loadResult();
		
		$tmp 		= 	explode('/', $fpss);
		$type_en 	= $tmp[0];
		$name		= $tmp[1]; 
		
		$value 		=	$type_en.'|'.$name;
		
		$query	= "SELECT * FROM #__ihouse_floorplan ";
			$db->setQuery( $query );
			$floorplan = $db->loadObjectList();
		
		$html = '';
		$html .= '<select name="'.$name_id.'" id="'.$name_id.'" onchange="javascript:loadFloorPlanPicture(\''.$postcode.'\',\''.$name_id.'\', this.value);">';
		$html .= '<option value="">---</option>';
		
		$allowed_types = array('png','jpg','jpeg','gif'); // list of filetypes you want to show
		
		foreach($floorplan as $fp) {
			$floorplan_path = JRoute::_('images/singapore/'.$postcode.'/floorplan/'.$fp->type_en);

			if(file_exists($floorplan_path)) {
				if ($handle = opendir($floorplan_path)) {
					while (false !== ($file = readdir($handle))) {
						if($file == '.' || $file == '..' || $file == 's')
							continue;
				
						if(in_array(strtolower(substr($file,-3)),$allowed_types))
						{
						
							$value2	=	$fp->type_en.'|'.$file;
							$html .= '<option '.(($value2==$value)?'selected="selected"':'').' value="'.$value2.'">'.$file.'</option>';
						}
					}
				}
			}
		}
		
		$html .= '</select>';
		
		return $html;
	}
	
	function getFloorPlan($row, $ordering) {
		$db		=& 	JFactory::getDBO();

		$ads_id 	= $row->ads_id;
		$postcode 	= $row->postcode;
		$query 		= " SELECT fp FROM #__ihouse_ads_floorplan WHERE ads_id = '$ads_id' AND ordering = '$ordering' ";
			$db->setQuery( $query ) ;
			$fp = $db->loadResult();
			
		$html = '';
		
		if(!empty($fp)) {
			$floorplan_img = JRoute::_('images/singapore/'.$postcode.'/floorplan/'.$fp);
			$html .= '<img src="'.$floorplan_img.'" width="100%" height="100%" />';
		}
		
		return $html;	
	}
	
	
}
