﻿<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('behavior.tooltip'); ?>

<?php JHTML::_('script', 'ajaxupload.js', 'components/com_ads/assets/'); ?>
<?php JHTML::_('script', 'postads.js', 'components/com_ads/assets/'); ?>
<?php JHTML::_('script', 'validation_postads.js', 'components/com_ads/assets/'); ?>

<script type="text/javascript">

function adType(value) {
	var optionstatus = $('option_status_text');
	if(value == 'rent') {
		$('rental_type').setProperty('disabled','');
	} else if(value == 'sell'){
		$('rental_type').setProperty('disabled','disabled');
		$('rental_type').setProperty('value','');
	} else {
		$('rental_type').setProperty('disabled','disabled');
		$('rental_type').setProperty('value','');
	}
	
}

function ajaxUpload(form) {
	$('message_tmp').setHTML('');
	var str = $('imgFile').getProperty('value');
	/* CHECK FILENAME */
	//if(/^[^\!\@\#\$\%\^\*]+(\.[^\!\@\#\$\%\^\*]+)+$/.test(str)) {
		return AIM.submit(form, 
					  {
						'onStart'		:   runThis,
					  	'onComplete' 	: 	loadThis
		});
	//} else {
		//$('message_tmp').setHTML('<span style="color:red;">"!@#$%^*" is restricted. Please change your filename</span>');
	//}
}

function runThis() {
	$('pic_uploading_loader').setStyle('display','block');
}

function loadThis() {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();		
	
	$('pic_uploading_loader').setStyle('display','block');
	
	var url = 'index.php?option=com_ads&view=ajaxLoadAdsTempImage&r=' + unixtime_ms;	
	
	var req = new Ajax(url, {
	   					data		: 	
									{
										'session_id' : '<?php echo $this->session_id ?>'
									},
						method		: "get",
		    			onSuccess	: function(data) {
						
							var obj1 		= Json.evaluate(data);
							var tmp 		= obj1.html;
							
							$('loadAdsImagesPlace').setHTML(tmp);
							
							$('pic_uploading_loader').setStyle('display','none');
		   				},
						async: true,
						evalScripts : true
				}).request();
}


function watermarkOption(value) {
	$('watermark_rad').setProperty('value', value);
}

function watermarkText(value) {
	$('watermark_textinput').setProperty('value', value);
}

</script>
<style type="text/css">
#tablist {
	border-left:3px solid #FF8200;
	border-bottom:3px solid #FF8200;
	border-right:3px solid #FF8200;
	width:99.2%;
	padding:0;
	margin:0;
	min-height:300px;
	height:auto;
	float:left;
}
</style>
<?php
	$ads = $this->ads;
	$user 			=& JFactory::getUser();
	$uid			=	$user->id;
	$user 			=& JFactory::getUser($uid);
	$user_category 	= $user->get('user_category');
?>
<div style="margin-top:-20px;">

<div id="main-search-header-text" style="padding-left:15px;border-width:2px;">
	<?php if($user_category == 'Agent') : ?>经纪中心<?php endif; ?>
    <?php if($user_category == 'Owner') : ?>屋主中心<?php endif;?>
    &nbsp;<span style="font-size:15px;font-weight:100;"><i><?php echo $user_category ?>&nbsp;Centre</i></span>
</div>

<div style="padding:0 10px">
	<div id="menu_list_profile_detail">
		<ul id="" class="tab_nav5">   
			<li id="tab_li5_account"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=account_info&Itemid=126&id=<?php echo $user->id;?>"><span class="chinese_text_menu">账号信息</span></a></li>
    <?php
	if(($user->user_category == 'Agent')||($user->user_category == 'Owner')||($user->user_category == 'Individual')){
	?>
			<li id="tab_li5_profile"><a href="<?php echo JURI::root();?>index.php?option=com_user&view=profiler&Itemid=126&id=<?php echo $user->id;?>"><span class="chinese_text_menu">个人资料</span></a></li>
    <?php
	}
	if(($user->user_category == 'Agent')||($user->user_category == 'Owner')){
	?>
			<li id="tab_li5_ads" class="tabactive5"><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=ads_management&Itemid=126&id=<?php echo $user->id;?>"><span class="chinese_text_menu">房地产广告</span></a></li>
    <?php
	}
	if($user->user_category == 'Agent'){
	?>
    		<li id="tab_li5_transaction"><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=transaction&Itemid=126&id=<?php echo $user->id;?>"><span class="chinese_text_menu">交易记录</span></a></li>
    		<li id="tab_li5_top_up" ><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=top_up&Itemid=126&id=<?php echo $user->id;?>"><span class="chinese_text_menu">充值</span></a></li>
    <?php
	}
	?>
		</ul>   
	</div>

	<div id="tablist">

		<div style="background-color:#fff;width:100%;float:left;margin-top:10px;">
			<div style="margin:10px auto; width:97%;">
			<!--Start-->
    			<div id="menu_list_ads_listing">
					<ul class="tab_nav77" id="">     
						<li  id="tab_li77_listing" class="tabactive77" ><a href="<?php echo JRoute::_('index.php?option=com_ads&amp;view=ads_management&amp;Itemid=126&amp;id='. $user->id); ?>"><span class="chinese_text_menu">Listing</span></a></li>
                        <?php if($user_category == 'Agent') : ?>
    					<li id="tab_li77_guru"><a href="<?php echo JRoute::_('index.php?option=com_ads&amp;view=the_guru&amp;Itemid=126&amp;id='. $user->id); ?>"><span class="chinese_text_menu">The Guru</span></a></li>
                        <?php endif; ?>
   	    				<li id="tab_li77_specialist"><a href="<?php echo JRoute::_('index.php?option=com_ads&amp;view=postads&amp;Itemid=126&amp;id='. $user->id); ?>"><span class="chinese_text_menu">Place New Ads</span></a></li>
    
					</ul>   
				</div><!--menu_list_profile_detail-->


<div style="float:left;background-color:#fff;width:100%;">
	<div style="margin:10px auto;">
	<!--Start-->
		<div style="border-width:2px;" id="main-search-header-text">
			<span style="font-size: 15px; font-weight: 100;"><i>Editing Ads</i></span>
		</div>
		<div id="tabcont-ads" style="background-color:#fff;width:100%;">
			<form name="edit_refresh_ads_form" id="edit_refresh_ads_form" method="post" enctype="multipart/form-data" action="<?php echo JRoute::_('index.php?option=com_ads&view=editing_ads') ?>">
        		<div class="postAdsPadding">
					<label id="property_typemsg" for="property_type">
					Property Type
			   		</label>
					<div style="padding-top:5px;">
                		<div style="float:left">
						<?php echo $this->property_type ?>
                    	</div>
                    	<div id="prop_type_loader">
                    		<img src="<?php echo JRoute::_('templates/main/images/ajax-loader.gif'); ?>"  />
                    	</div>
                    	<div style="clear:both"></div>
					</div>
		 			<div id="property_type_table" style="margin:5px 0 5px 0;">
                	<?php 	
						if($ads->property_type == '组屋' ) {
							require('hdb.php');
						} else if($ads->property_type == '公寓'){
							require('condo.php');
						} else if($ads->property_type == '服务公寓') {
							require('apartments.php');
						} else if($ads->property_type == '商业') {
							require('commercials.php');
						}  else if($ads->property_type == '有地住宅'){
							require('landed.php');
						}
					?>	
					</div>
				</div>
            	<?php if($ads->property_type == '公寓' ) : ?>
				<div style="background-color:#ff8200;color:#fff;height:37px;">
					<div style="float:left;padding:10px;"><strong>Floor Plan</strong></div>
					<div style="clear:both"></div>
				</div>
                <div id="whole_fp_box">
					<div>
						Choose the floorplan you want to add in. You can select up to 2 floorplan
					</div>
			
            		<div>
						<div style="float:left;width:49.5%;background-color:#ccc;text-align:center;">
							<div id="fp1_select"><?php echo $this->fp1 ?></div>
				    		<div id="fp1_img"><?php echo $this->fp1_image ?></div>
			    		</div>
			    		<div style="float:right;width:49.5%;background-color:#ccc;text-align:center;">
				    		<div id="fp2_select"><?php echo $this->fp2 ?></div>
				    		<div id="fp2_img"><?php echo $this->fp2_image ?></div>
			    		</div>
        				<div style="clear:both"></div>
					</div>
                </div>
            	<span id="property_id"></span> 
				<div id="test"></div>
				<div style="background-color:#ff8200;color:#fff;height:37px;">
					<div style="float:left;padding:10px;"><strong>Image from iHouse</strong></div>
					<div style="clear:both;"></div>
				</div>

				<div>
            	
					<div id="ihouseImage">
						<?php echo ($this->image2)?$this->image2:'No Pictures for this property name' ?>
					</div>
                	<div id="ihouseImage_loader" style="float:left;display:none;">
                    	<img src="<?php echo JRoute::_('templates/main/images/ajax-loader.gif'); ?>"  />
                	</div>
					<div style="clear:both"></div>
           		</div>
                <?php endif; ?>
				<input type="hidden" name="task" value="" />
				<input type="hidden" name="option" value="com_ads" />
				<input type="hidden" name="session_id" value="<?php echo $this->session_id ?>" />
    			<input type="hidden" name="ads_id" value="<?php echo $this->ads_id ?>" />
            	<input type="hidden" name="property_type" value="<?php echo $ads->property_type ?>" />
				<input type="hidden" name="agree1" id="t-agree1" value="0" />
				<?php if($user_category == 'Agent') : ?>
				<input type="hidden" name="agree2" id="t-agree2" value="0" />
            	<?php endif; ?>
				<?php echo JHTML::_( 'form.token' ); ?>
        	</form>      
        	<div style="background-color:#ff8200;color:#fff;height:37px;">
				<div style="float:left;padding:10px;"><strong>Current Photos</strong></div>
				<div style="float:right;margin-right:10px;">
					<div style="overflow:hidden;;text-align:center;position:relative;padding:7px 0;">
                        <form name="loadImageForm" enctype="multipart/form-data" method="post" id="loadImageForm" action="<?php echo JRoute::_('index.php?option=com_ads&view=ajaxSave') ?>">
					    	<img src="<?php echo JRoute::_('templates/main/images/btn-upload.png') ?>" />
				    		<input class="imgFileInput" id="imgFile" name="imgFile" type="file" onchange="ajaxUpload(this.form);" />
                            <input type="hidden" name="session_id" value="<?php echo $this->session_id ?>" />
                            <input type="hidden" name="watermark_use" id="watermark_rad" value="1"  />
                            <input type="hidden" name="wmrk_txt" id="watermark_textinput" value="<?php echo $this->watermark_text ?>" />
						</form>       
				   	</div>
				</div>
				<div style="clear:both"></div>
			</div>
            
             <div style="margin:5px 10px">
            	<div style="float:left;"> 
					Watermark <input type="radio" name="watermark_radio" onclick="javascript:watermarkOption(this.value);" value="1" checked="yes" />iHouse Logo
                    <input type="radio" name="watermark_radio" onclick="javascript:watermarkOption(this.value);" value="0" />
                    <input type="text" onkeyup="javascript:watermarkText(this.value);" value="<?php echo $this->watermark_text ?>" maxlength="16" />	
                </div>
            	<div style="clear:both"></div>
            </div>
            
            <div>
            	<div style="float:left;"> 
					Simply click 'Upload' to choose the photos you want to upload. You can upload up to 8 photos.		
                </div>
                <div id="pic_uploading_loader">
                    <img src="<?php echo JRoute::_('templates/main/images/ajax-loader.gif'); ?>"  />
                </div>
                <div style="clear:both"></div>
				<div id="message_tmp">
				</div>
				<div id="loadAdsImagesPlace">
                	<?php echo $this->image1 ?>
				</div>
				<div style="clear:both;"></div>
            </div>
            <div style="border:2px solid #a50c2a;"> 
        		<div style="color:#fff;background-color:#a50c2a;">
            		<div style="padding:2px;">
            			<strong>Important</strong>
                	</div>
            	</div>      
				<input type="checkbox" name="agree1" value="1" onclick="javascript:agree('agree1');" /><span id="err-agree1">I confirm the listing details and photos describe the property accurately</span><br />
				<?php if($user_category == 'Agent') : ?>
                <input type="checkbox" name="agree2" value="1" onclick="javascript:agree('agree2');" /><span id="err-agree2">I confirm I have obtained agreement from the client and/or the client's representing agent to advertise the property</span>
                <?php endif; ?>    
			</div>
			<div id="button_div" style="text-align:center;margin:3px 0;">
        		<button class="button validate" type="submit" onclick="javascript:validateForm('','<?php echo JRoute::_('index.php?option=com_ads&view=editing_ads'); ?>');return false;">
					<?php echo JText::_('Submit'); ?>
        		</button>
			</div>  
		</div><!--End-->
	</div>        
</div>                           

			</div>
		</div>

	</div><!-- tablist -->
</div><!-- padding : 10px -->               

</div><!-- margin : -20px -->  