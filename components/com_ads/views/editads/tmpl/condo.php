﻿<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('behavior.tooltip'); ?>
<style type="text/css">
.ads_form_border {
	border-bottom:1px dotted #ccc;
	float:left;
	width:100%;
}

.ads_form_0 {
	float:left;margin:10px 0;
}

.ads_form_1 {
	float:left;margin:10px 5px;
}
</style>

<div class="ads_form_border">
	<div class="ads_form_0">
		<label id="property_namemsg" for="property_type">
		Property Name *
    	</label>
        <div id="property_name_div" style="padding-top:5px;">
        <?php echo $this->property_name ?>
        </div>
    </div>
    <div class="ads_form_1" style="width:20px;">
    	 <div id="prop_name_loader">
        	<img src="<?php echo JRoute::_('templates/main/images/ajax-loader.gif'); ?>"  />
        </div>   
    </div>
    <div class="ads_form_1">
    	<!--Email-us if the condo is not in the list-->
    </div>
</div>
<div style="clear:both;"></div>
<div class="ads_form_border">
	<div class="ads_form_0">
    	<label id="property_categorymsg" for="property_type">
		Property Category *
        </label>
        <div style="padding-top:5px;">
        <?php echo $this->property_category ?>
        </div>
    </div>
    <div class="ads_form_1">
    	<label id="ad_typemsg" for="property_type">
		Ad Type*: 
    	</label>
    	<div style="padding-top:5px;">
    	<?php echo $this->ad_type ?>
        <input type="hidden" id="ad_type" name="ad_type" value="<?php echo $this->ad_type; ?>"  />
    	</div>
    </div>
    <div class="ads_form_1">
	<label id="property_typemsg" for="property_type">
	Rental Type: 
	</label>
	<div style="padding-top:5px;">
	<?php echo $this->rental_type ?>
	</div>
	</div>
    
    <div class="ads_form_1">
	<label id="property_typemsg" for="property_type">
	Floor Level
	</label>
	<div style="padding-top:5px;">
	 <?php echo $this->floor_level ?>
	</div>
	</div>    
</div>     
<div style="clear:both"></div>
<div class="ads_form_border">
	<div class="ads_form_0">
	<label id="property_typemsg" for="property_type">
		Asking Price (SGD)
	</label>
	<div style="padding-top:5px;">
	<?php echo $this->asking_price ?>
	</div>
    </div>
    
    <div class="ads_form_1">
    	<label id="property_typemsg" for="property_type">
		Size (sqft)
		</label>
		<div style="padding-top:5px;">
		<?php echo $this->size ?>
		</div>
    </div>
    
    <div class="ads_form_1">
    	<div>
    	<label id="property_typemsg" for="property_type">
		Psm Price:
        </label>
        </div>
        <div style="margin-top:7px;">
        <span style="padding-top:5px;">
        	<?php echo $this->psm_price ?>
        </span>
        </div>
	</div>
    <div class="ads_form_1">   
    	<div> 
    	<label id="property_typemsg" for="property_type">
		Psf Price:
        </label>
        </div>
        <div style="margin-top:7px;">
        <span style="padding-top:5px;">
        	<?php echo $this->psf_price ?>
        </span>
        </div>
    </div>
    
</div>
<div style="clear:both"></div>
<div class="ads_form_border">
	<div class="ads_form_0">
		<label id="property_typemsg" for="property_type">
		# of Hall
		</label>
		<div style="padding-top:5px;">
		<?php echo $this->no_of_hall; ?>
		</div>
	</div>
	<div class="ads_form_1">
		<label id="property_typemsg" for="property_type">
		# of Room
		</label>
		<div style="padding-top:5px;">
		<?php echo $this->no_of_room ?>
		</div>
	</div>
	<div class="ads_form_1">
		<label id="property_typemsg" for="property_type">
		Study Room
		</label>
		<div style="padding-top:5px;">
			<?php echo $this->study_room ?>
		</div>
	</div>
    
	<div class="ads_form_1">
		<label id="property_typemsg" for="property_type">
		Toilet
		</label>
		<div style="padding-top:5px;">
		<?php echo $this->toilet ?>
		</div>
	</div>
</div>
<div style="clear:both"></div>
<div class="ads_form_border">
	<div class="ads_form_0">
    	<label id="property_typemsg" for="property_type">
		Block/House Number
		</label>
		<div style="padding-top:5px;">
			 <?php echo $this->block_no ?>
		</div>
    </div>
    
    <div class="ads_form_1">
    	<label id="property_typemsg" for="property_type">
		Floor/Unit Number
		</label>
		<div style="padding-top:5px;">
			<?php echo $this->floor_unit_no ?>                                                         
		</div>
    </div>
    
    <div class="ads_form_1">
    	<label id="property_typemsg" for="property_type">
			Country
		</label>
		<div style="padding-top:5px;">
			<?php echo $this->country ?>
		</div>
    </div>
</div>
<div style="clear:both"></div>
<div class="ads_form_border">
	<div class="ads_form_0">
	<label id="street_name_msg" for="property_type">
			Street Name
	</label>
	<div style="padding-top:5px;">
    	<div id="street_name_id" style="padding-top:5px;">
        <?php echo $this->street_name ?>
        </div>
	</div>   
    </div>
</div>
<div style="clear:both"></div>
<div class="ads_form_border">
	<div class="ads_form_0">
    	<label id="ad_titlemsg" for="property_type">
		Ad Title *
		</label>
		<div style="padding-top:5px;">
		 <?php echo $this->ad_title ?>
		</div>
    </div>
    
    <div class="ads_form_1">
    	<label id="property_typemsg" for="property_type">
			Renovation
		</label>
		<div style="padding-top:5px;">
			<?php echo $this->renovation ?>
		</div>
    </div>
</div>
<div style="clear:both"></div>
<div class="ads_form_border">
	<div class="ads_form_0">
    	<label id="property_typemsg" for="property_type">
			Description
		</label>
		<div style="padding-top:5px;">
			<?php echo $this->add_info ?>
		</div>
    </div>
</div>
<div style="clear:both"></div>
<div class="ads_form_border">
	<div class="ads_form_0">
	<label id="property_typemsg" for="property_type">
	Property District
	</label>
    <div id="property_district_id" style="padding-top:5px;">
    	<?php echo $this->district ?>
    </div>
    </div>
    
    <div class="ads_form_1">
    	<label id="property_typemsg" for="property_type">
		Tenure
        </label>
        <div id="tenure_id" style="padding-top:5px;">
       	<?php echo $this->tenure ?>
        </div>
    </div>
    
    <div class="ads_form_1">
    	<label id="property_typemsg" for="property_type">
		TOP Year
    	</label>
    	<div id="topyear_id" style="padding-top:5px;">
    	<?php echo $this->year_built ?>
    	</div>
    </div>
    
    <div class="ads_form_1">
    	<label id="postal_codemsg" for="property_type">
		Postal Code
        </label>
        <div id="postcode_id" style="padding-top:5px;">
        <?php echo $this->postcode ?>
        </div>
    </div>
    
    <div class="ads_form_1">
    	<label id="statusmsg" for="property_type">
			Status
		</label>
		<div style="padding-top:5px;">
			<?php echo $this->status ?>
		</div>
    </div>
    
</div>
<div style="clear:both"></div>



<!--
<table id="refresh_edit_table" cellpadding="0" cellspacing="0" border="0" width="600" class="contentpane" >
                                                        <tr >
                                                        	<td width="40%" height="60">
                                                            <label id="property_categorymsg" for="property_type">
																Property Category *
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->property_category ?>
                                                            </div>
                                                            </td>
                                                            <td width="40%" height="60">
                                                            
                                                            <label id="property_namemsg" for="property_type">
																Property Name *
                                                            </label>
                                                            
                                                            <div>
                                                            <div style="float:left;">	
                                                            	<div id="property_name_div" style="padding-top:5px;">
                                                            	<?php echo $this->property_name ?>
                                                            	</div>
                                                            </div>
                                                            <div id="prop_name_loader">
                    										<img src="<?php echo JRoute::_('templates/main/images/ajax-loader.gif'); ?>"  />
                    										</div>
                                                            <div style="clear:both"></div>
                                                            </div>
                                                            
                                                            </td>
                                                   		</tr>
                                                        <tr>
                                                        	<td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Asking Price (SGD)
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->asking_price ?>
                                                            </div>
                                                            </td>
                                                            <td width="40%" height="60">
                                                            <div style="float:left;">
                                                            <label id="property_typemsg" for="property_type">
																Psm Price
                                                            </label>
                                                            <div id="psm_price_id" style="padding-top:5px;">
                                                            <?php echo $this->psm_price ?>
                                                            </div>
                                                            </div>
                                                            <div style="float:left;margin-left:10px;">
                                                            <label id="property_typemsg" for="property_type">
																Psf Price
                                                            </label>
                                                            <div id="psf_price_id" style="padding-top:5px;">
                                                            <?php echo $this->psf_price ?>
                                                            </div>
                                                            </div>
                                                            </td>
                                                   		</tr>
                                                        <tr>
                                                        	<td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Size (psm)
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->size ?>
                                                            </div>
                                                            </td>
                                                            <td width="40%" height="60">
                                                            
                                                            <div style="float:left;">
                                                            	<label id="ad_typemsg" for="property_type">
																Ad Type: 
                                                            	</label>
                                                            	<div style="padding-top:5px;">
                                                            	<?php echo $this->ad_type ?>
                                                            	</div>
                                                            </div>
                                                             <div style="float:left;margin-left:10px;">
                                                            	<label id="property_typemsg" for="property_type">
																Rental Type: 
                                                            	</label>
                                                            	<div style="padding-top:5px;">
                                                            	<?php echo $this->rental_type ?>
                                                            	</div>
                                                            </div>
                                                            <div style="float:left;margin-left:10px;">
                                                            <label id="property_typemsg" for="property_type">
																Floor Level
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->floor_level ?>
                                                            
                                                            </div>
                                                            </div>
                                                            <div style="clear:both"></div>
                                                            </td>
                                                   		</tr>
                                                        <tr>
                                                        	<td width="40%" height="60">
                                                            <div style="float:left;">
                                                            <label id="property_typemsg" for="property_type">
																Property District
                                                            </label>
                                                            <div id="property_district_id" style="padding-top:5px;">
                                                            <?php echo $this->district ?>
                                                            </div>
                                                            </div>
                                                            <div style="float:left;margin-left:10px;">
                                                            <label id="postal_codemsg" for="property_type">
																Postal Code
                                                            </label>
                                                            <div id="postcode_id" style="padding-top:5px;">
                                                            <?php echo $this->postcode ?>
                                                            </div>
                                                            </div>
                                                            </td>
                                                        	
                                                            <td width="40%" height="60">
                                                            <div style="float:left">
                                                            <label id="property_typemsg" for="property_type">
																# of Hall
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->no_of_hall; ?>
                                                            </div>
                                                            </div>
                                                            <div style="float:left;margin-left:10px;">
                                                            <label id="property_typemsg" for="property_type">
																# of Room
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->no_of_room ?>
                                                            </div>
                                                            </div>
                                                            <div style="float:left;margin-left:10px;">
                                                            <label id="property_typemsg" for="property_type">
																Study Room
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->study_room ?>
                                                           
                                                            </div>
                                                            </div>
                                                            </td>
                                                   		</tr>
                                                        <tr>
                                                        	<td width="40%" height="60">
                                                            <div style="float:left;">
                                                            <label id="property_typemsg" for="property_type">
																Tenure
                                                            </label>
                                                            <div id="tenure_id" style="padding-top:5px;">
                                                            <?php echo $this->tenure ?>
                                                            </div>
                                                            </div>
                                                            <div style="float:left;margin-left:10px;">
                                                            <label id="property_typemsg" for="property_type">
																TOP Year
                                                            </label>
                                                            <div id="topyear_id" style="padding-top:5px;">
                                                            <?php echo $this->year_built ?>
                                                            </div>
                                                            </div>
                                                            </td>
                                                        	
                                                            <td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Toilet
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->toilet ?>
                                                            
                                                            </div>
                                                            </td>
                                                            
                                                   		</tr>
                                                        <tr>
                                                        	<td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Block/House Number
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->block_no ?>
                                                            
                                                            </div>
                                                            </td>
                                                        	
                                                            <td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Floor/Unit Number
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->floor_unit_no ?>
                                                            
                                                            </div>
                                                            </td>
                                                   		</tr>
                                                        <tr>
                                                        	<td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Street Name
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->street_name ?>
                                                            </div>
                                                            </td>
                                                        	
                                                            <td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Country
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->country ?>
                                                            </div>
                                                            </td>
                                                   		</tr>
                                                        <tr>
                                                        	<td width="40%" height="60">
                                                            <label id="ad_titlemsg" for="property_type">
																Ad Title *
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->ad_title ?>
                                                            </div>
                                                            </td>
                                                            <td width="40%" height="60" rowspan="2" >
                                                            <label id="property_typemsg" for="property_type">
																Additional Info
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->add_info ?>
                                                            </div>
                                                            </td>
                                                   		</tr>
                                                        <tr>
                                                        	<td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Renovation
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->renovation ?>
                                                            </div>
                                                            </td>
                                                           
                                                   		</tr>
                                                        <tr>
                                                        	<td width="40%" height="60">
                                                             <label id="statusmsg" for="property_type">
																Status
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->status ?>
                                                            
                                                            </div>
                                                            </td>
                                                            <td width="40%" height="60">
                                                            
                                                            </td>
                                                   		</tr>
                                                   </table>
                                                   -->