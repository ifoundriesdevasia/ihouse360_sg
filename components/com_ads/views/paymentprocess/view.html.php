<?php
/**
 * @version		$Id: view.html.php 11646 2009-03-01 19:34:56Z ian $
 * @package		Joomla
 * @subpackage	Ads
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Users component
 *
 * @static
 * @package		Joomla
 * @subpackage	Payment Process
 * @since 1.0
 */
class AdsViewPaymentprocess extends JView
{
	function display($tpl = null)    
	{
		global $mainframe;
		$db = & JFactory::getDBO();
		
		$user =& JFactory::getUser();
		$user_id = $user->get('id');
		
		$tempid			= JRequest::getVar('tempid', '', 'post', 'tempid');
		$plan_id		= JRequest::getVar('plan_id', '', 'post', 'plan_id');
		$type_iv		= JRequest::getVar('type_iv', '', 'post', 'type_iv');
		$type_v			= JRequest::getVar('type_v', '', 'post', 'type_v');
		$type_vi		= JRequest::getVar('type_vi', '', 'post', 'type_vi');
		$type_vii		= JRequest::getVar('type_vii', '', 'post', 'type_vii');
		$payment_mode	= JRequest::getVar('payment_mode', '', 'post', 'payment_mode');
		$top_up_amt		= JRequest::getVar('top_up_amt', '', 'post', 'top_up_amt');//user choose amount option
		$evoucherno		= JRequest::getVar('evoucherno', '', 'post', 'evoucherno');
		$actual_amt		= JRequest::getVar('actual_amt', '', 'post', 'actual_amt');//after deduct e-voucher discount
		$discount		= JRequest::getVar('discount', '', 'post', 'discount');
		$user_used	= JRequest::getVar('user_used', '', 'post', 'user_used');
		
		if($plan_id == 'type4'){
		$plan_id = $type_iv;
		}
		
		if($plan_id == 'type5'){
		$plan_id = $type_v;
		}
		
		if($plan_id == 'type6'){
		$plan_id = $type_vi;
		}
		
		if($plan_id == 'type7'){
		$plan_id = $type_vii;
		}
		
		
		$rows = $this->getpaypal();
		
		
		$ip = $_SERVER['REMOTE_ADDR'];
		
		$query = "SELECT validity FROM #__cbsubs_plans WHERE id = '".$plan_id."'";
		$db->setQuery($query);
		$validity = $db->loadResult(); //duration

		$date = explode("-", $validity);
		$year = $date[0];
		$month = $date[1];
		$day = substr($date[2], 0, 2);

		$expirydate = date("Y-m-d H:i:s", mktime(date("H"),date("i")+1,date("s"),date("m")+$month,date("d")+$day,date("Y")+$year));
		
		$query = "SELECT id FROM #__cbsubs_subscriptions WHERE status = 'X' AND user_id = '".$user_id."' AND plan_id = '".$plan_id."'";
		$db->setQuery($query);
		$sub_id = $db->loadResult();
		
		//start payment mode
		if($payment_mode == 'credits'){
		
			if(($user_used == 'no')&&(!empty($evoucherno))&&($discount != '0')){
				$item_amt 		= $actual_amt;
				$evoucher_amt 	= $top_up_amt - $actual_amt;
			} else {
				$item_amt 		= $top_up_amt;
				$evoucher_amt 	= '0';
			}
		
		//$item_amt = $top_up_amt;
		$item_name = "Credits Top-Up";
		
		$query = "INSERT INTO #__users_payment_log (id,user_id,plan_id,payment_description,amount,invoice_no,payment_status,ip_add,datetime,subscription_id)"
		
			. "\n VALUES ( NULL, '".$user_id."', '0', '".$item_name."', '".$item_amt."', '0', '0', '".$ip."', CURRENT_TIMESTAMP, '0')";
			$db->setQuery( $query );
			$db->query();
		$pid = mysql_insert_id();
		
		if(($user_used == 'no')&&(!empty($evoucherno))&&($discount != '0')){
		
		$hquery = "UPDATE #__ihouse_evoucher_user SET user_id = '" .$user_id. "', evoucher_amt = '" .$evoucher_amt. "', payment_logid =  '" .$pid. "'  WHERE evoucher_num = '".$evoucherno."'";
			$db->setQuery($hquery);
			$db->query();
		
		/*$query = "INSERT INTO #__ihouse_evoucher_user(id,evoucher_num,user_id,evoucher_amt,payment_logid,estatus,datetime)"
		
			. "\n VALUES ( NULL, '".$evoucherno."', '".$user_id."', '".$evoucher_amt."', '".$pid."', '0', CURRENT_TIMESTAMP)";
			$db->setQuery( $query );
			$db->query();*/
		}
		
		$sql = "UPDATE #__ihouse_ads_temp set payment_logid = '".$pid."' WHERE id = '".$tempid."'";
		$db->setQuery($sql);
		$db->query();
		
		} else { //payment mode - not credit top up
		
		$item_amt = round($this->getamt($plan_id));
		$item_name = $this->getitem_name($plan_id);
		
	//	if(isset($sub_id)){ //if owner renew
		
		/*$sql = "UPDATE #__cbsubs_subscriptions set status = 'A', last_renewed_date = CURRENT_TIMESTAMP, expiry_date = '".$expirydate."' WHERE user_id = '".$user_id."' AND id =".$sub_id;
		$db->setQuery($sql);
		$db->query();
		
		
		$query = "INSERT INTO #__users_payment_log (id,user_id,plan_id,payment_description,amount,invoice_no,payment_status,ip_add,datetime,subscription_id)"
		
			. "\n VALUES ( NULL, '".$user_id."', '".$plan_id."','', '".$item_amt."', '0', '0', '".$ip."', CURRENT_TIMESTAMP, '".$sub_id."')";
			$db->setQuery( $query );
			$db->query();
		$pid = mysql_insert_id();*/
		
	//	} else { //owner not renew
		
		

		$query = "INSERT INTO #__users_payment_log (id,user_id,plan_id,payment_description,amount,invoice_no,payment_status,ip_add,datetime,subscription_id)"
		
			. "\n VALUES ( NULL, '".$user_id."', '".$plan_id."', '".$item_name."', '".$item_amt."', '0', '0', '".$ip."', CURRENT_TIMESTAMP, '0')";
			$db->setQuery( $query );
			$db->query();
			$pid = mysql_insert_id();
		
		$sql = "UPDATE #__ihouse_ads_temp set payment_logid = '".$pid."' WHERE id = '".$tempid."'";
		$db->setQuery($sql);
		$db->query();
		
		
		//	} //end if owner renew
		} // end if payment mode
		
		
		$this->assignRef('pid'	, 	$pid );
		$this->assignRef('user_id'	, 	$user_id );
		$this->assignRef('item_name'	, 	$item_name );
		$this->assignRef('item_amt'	, 	$item_amt );
		$this->assignRef('rows'	, 	$rows );
		parent::display($tpl);
	}
	
	function getpaypal() {
	
	$db = & JFactory::getDBO();
	
	$query="SELECT * FROM #__paypal_config";
	$db->setQuery($query);
	$row = $db->loadObjectList();
	
	return $row;
	
	}//end function
	
	function getamt($plan_id) {
	
	$db = & JFactory::getDBO();
	
	$query="SELECT rate FROM #__cbsubs_plans WHERE id = ".$plan_id;
	$db->setQuery($query);
	$amt = $db->loadResult();
	
	return $amt;
	
	}//end function
	
	function getitem_name($plan_id) {
	
	$db = & JFactory::getDBO();
	
	$query="SELECT name FROM #__cbsubs_plans WHERE id = ".$plan_id;
	$db->setQuery($query);
	$itemname = $db->loadResult();
	
	return $itemname;
	
	}//end function

}
