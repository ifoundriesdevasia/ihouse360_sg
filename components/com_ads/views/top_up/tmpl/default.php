﻿<?php defined('_JEXEC') or die; ?>
<?php JHTML::_('script', 'ihouse.js', 'components/com_ads/rokbox/'); ?>
<!--Start View-->
<div style="margin-top:-20px;">

<div id="main-search-header-text" style="padding-left:15px;border-width:2px;">会员中心&nbsp;<span style="font-size:15px;font-weight:100;"><i><?php echo $this->users->user_category ?>&nbsp;Centre</i></span></div>

<style type="text/css">
#tablist {
	border-left:3px solid #FF8200;
	border-bottom:3px solid #FF8200;
	border-right:3px solid #FF8200;
	width:99.1%;
	padding:0;
	margin:0;
	min-height:300px;
	height:auto;
	float:left;
}

table#prop_det_info td{
	border:1px solid #999;
}

ul.tab_nav3 {
list-style-image:none;
list-style-type:none;
}
</style>

<div style="padding:15px;"><!--start-->

<div id="menu_list_profile_detail">
<ul id="" class="tab_nav5">     
	<li id="tab_li5_account"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=account_info&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Acct. Info</span></a></li>
     <?php
	if(($this->users->user_category == 'Agent')||($this->users->user_category == 'Owner')||($this->users->user_category == 'Individual')){
	?>
	<li id="tab_li5_profile"><a href="<?php echo JURI::root();?>index.php?option=com_user&view=profiler&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Profile</span></a></li>
    <?php
	}
	if(($this->users->user_category == 'Agent')||($this->users->user_category == 'Owner')){
	?>
	<li id="tab_li5_ads"><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=ads_management&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Your Ads</span></a></li>
    <?php
	}
	if($this->users->user_category == 'Agent'){
	?>
    <li id="tab_li5_transaction"><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=transaction&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Transactions</span></a></li>
    <li id="tab_li5_top_up" class="tabactive5"><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=top_up&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Top-up</span></a></li>
    <?php
	}
	?>
    <li id="tab_li77_specialist"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=favoriteads&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Favorites</span></a></li>
</ul>   
</div>

<div id="tablist"> 
    <div id="tab_top_up" class="tabular1">
    	<div style="padding: 20px;">
            <div>
            	<div style="float:left; width:385px; height:auto;">
            	<form action="index.php?option=com_ads" method="post" name="form2" id="form2">
                <div id="static-text">
                    	充值&nbsp;&nbsp;<font style="color:#494949;">Top-Up</font>
                </div>
                
                <div>
                <br /><br />
            	Select the top-up amount.<br /><br />
                Key in the E-voucher number if you have any,<br />before clicking on Buy to purchase credits.<br /><br />
                You will be redirected to a confirmation page upon clicking on the button.<br /><br />
                
                Top Up Amount:<br />
                <select name="top_up_amt" id="top_up_amt">
                <option value="">Please Select</option>
                <?php
				if($this->reamt == ''){
				?>
                <option value="20">$20</option>
                <option value="50">$50</option>
                <option value="100">$100</option>
                <option value="300">$300</option>
                <option value="500">$500</option>
                <?php
				} else if(!($this->reamt == '')){
				?>
                <?php if($this->reamt <= '20'){?>
                <option value="20">$20</option>
                <?php } ?>
                <?php if($this->reamt <= '50'){?>
                <option value="50">$50</option>
                <?php } ?>
                <?php if($this->reamt <= '100'){?>
                <option value="100">$100</option>
                <?php } ?>
                <?php if($this->reamt <= '300'){?>
                <option value="300">$300</option>
                <?php } ?>
                <?php if($this->reamt <= '500'){?>
                <option value="500">$500</option>
                <?php } ?>
                <option value="<?php echo $this->reamt; ?>">Actual Amount - $<?php echo $this->reamt; ?></option>
                <?php
				}
				?>
                </select>&nbsp;
                
                <br /><br />
                E-Voucher Number:<br />
                <input type="text" name="evoucherno" id="evoucherno" value="" size="40" /><br /><br />
				<input type="submit" value="Buy" name="buy_credit" id="buy_credit" />&nbsp;<br /><br />

                <input name="option" type="hidden" value="com_ads" />
                <?php /*?><input type="hidden" name="view" id="view" value="paypal_owner" /><?php */?>
                <input type="hidden" name="view" id="view" value="credit_confirmation" />
                <input type="hidden" name="Itemid" id="Itemid" value="126" />
                <input type="hidden" name="payment_mode" id="payment_mode" value="credits" />
                <input type="hidden" name="tempid" id="tempid" value="<?php echo $this->tempid;?>" />
                <input type="hidden" name="user_id" id="user_id" value="<?php echo $this->users->id;?>" />
                
                </div>
                </form>
                </div>
                
                <div style="float:left; width:225px; height:auto; padding-top:20px;">
                	<img src="<?php echo JURI::root();?>templates/main/images/paypal_img.jpg" />
                </div>
                
            </div>
            
            <div style="float:left; width:100%; padding-top:20px; padding-bottom:20px; color:#FF0000;">
            	* Promotion : For single top-up with S$300/ S$500, an additional one time 3% | 5% (eg. S$9 | S$25) credits will be given to respective account.
            </div>
        </div>
    </div><!--tabular1-->

</div>   

</div><!--End-->



</div>
<!--End View-->
