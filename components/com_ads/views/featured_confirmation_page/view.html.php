<?php
/**
 * @version		$Id: view.html.php 11646 2009-03-01 19:34:56Z ian $
 * @package		Joomla
 * @subpackage	Featured Confirmation page
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Form component
 *
 * @static
 * @package		Joomla
 * @subpackage	Featured Confirmation page
 * @since 1.0
 */
class AdsViewFeatured_confirmation_page extends JView
{
	function display($tpl = null)
	{
		global $mainframe;
		$db 	  	=& 	JFactory::getDBO();
		$user =& JFactory::getUser();
	 	$id = $user->get('id');
	 
		$page_id = JRequest::getVar( 'page_id', '', 'post', 'page_id' );
		$ads_type_config	= JRequest::getVar('ads_type_config', '', 'post', 'ads_type_config');
		$ads_type	= JRequest::getVar('ads_type', '', 'post', 'ads_type');
		$ads_id	= JRequest::getVar('ads_id', '', 'post', 'ads_id');
		
		$plan_id0	= JRequest::getVar('plan_id0', '', 'post', 'plan_id0');
		$plan_id1	= JRequest::getVar('plan_id1', '', 'post', 'plan_id1');
		$plan_id2	= JRequest::getVar('plan_id2', '', 'post', 'plan_id2');
		$plan_id3	= JRequest::getVar('plan_id3', '', 'post', 'plan_id3');
		$plan_id4	= JRequest::getVar('plan_id4', '', 'post', 'plan_id4');
		$plan_id5	= JRequest::getVar('plan_id5', '', 'post', 'plan_id5');
		
		
		if($plan_id0 != ''){
		$plan_id = $plan_id0;
		}
		
		if($plan_id1 != ''){
		$plan_id = $plan_id1;
		}
		
		if($plan_id2 != ''){
		$plan_id = $plan_id2;
		}
		
		if($plan_id3 != ''){
		$plan_id = $plan_id3;
		}
		
		if($plan_id4 != ''){
		$plan_id = $plan_id4;
		}
		
		if($plan_id5 != ''){
		$plan_id = $plan_id5;
		}
		

	
		$query="SELECT * FROM #__cbsubs_plans WHERE id = ".$plan_id;
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		
		$query = "SELECT rate FROM #__cbsubs_plans WHERE id = '".$plan_id."'";
		$db->setQuery($query);
		$rate = $db->loadResult();
		
		$query="SELECT params FROM #__ihouse_promotion WHERE id = '4'";
		$db->setQuery($query);
		$params = $db->loadResult();
		
		$discountone = explode("=", $params);
		$discount = $discountone[1];
		
		//25% discount for the 4th week
		if(($plan_id == '11')||($plan_id == '15')||($plan_id == '19')||($plan_id == '23')){
		$sub_credit = ((round($rate)) * (1 - 0.25));
		} else {
		$sub_credit = (round($rate));
		}
		
		$scredit = ((round($rate)) * (1 - 0.25));
		
		//80% discount for all category featured ads
		$after_discount = $sub_credit * (1 - ($discount));
	
		$this->assignRef('users', $this->getInfoOfThisUser($id));
		$this->assignRef('plan_id', $plan_id);
		$this->assignRef('page_id', $page_id);
		$this->assignRef('rows', $rows);
		$this->assignRef('rate', $rate);
		$this->assignRef('discount', $discount);
		$this->assignRef('after_discount', $after_discount);
		$this->assignRef('scredit', $scredit);
		$this->assignRef('ads_type_config', $ads_type_config);
		$this->assignRef('ads_type', $ads_type);
		$this->assignRef('ads_id', $ads_id);
		parent::display($tpl);
		
	}
	
	function getInfoOfThisUser($id = 0) {
		
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT * FROM #__users "
						.	" WHERE id = '$id'" ;
						
			$db->setQuery( $query );
			$list = $db->loadObject();		
			//echo print_r($list);
		return $list;	
	}
}
