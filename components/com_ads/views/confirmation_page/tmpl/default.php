﻿<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('behavior.tooltip'); ?>
<?php JHTML::_('script', 'ihouse.js', 'components/com_ads/rokbox/'); ?>

<style type="text/css">
#tablist {
	border-left:3px solid #FF8200;
	border-bottom:3px solid #FF8200;
	border-right:3px solid #FF8200;
	width:99.1%;
	padding:0;
	margin:0;
	min-height:300px;
	height:auto;
	float:left;
}

table#prop_det_info td{
	border:1px solid #999;
}

ul.tab_nav3 {
list-style-image:none;
list-style-type:none;
}
</style>

<!--Start View-->
<div style="margin-top:-20px;">

<div id="main-search-header-text" style="padding-left:15px;border-width:2px;">会员中心&nbsp;<span style="font-size:15px;font-weight:100;"><i><?php echo $this->users->user_category ?>&nbsp;Centre</i></span></div>

<div style="padding:15px;"><!--start-->

<div id="menu_list_profile_detail">
<ul id="" class="tab_nav5">     
	<li id="tab_li5_account" class="tabactive5"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=account_info&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Acct Info</span></a></li>
     <?php
	if(($this->users->user_category == 'Agent')||($this->users->user_category == 'Owner')||($this->users->user_category == 'Individual')){
	?>
	<li id="tab_li5_profile"><a href="<?php echo JURI::root();?>index.php?option=com_user&view=profiler&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Profile</span></a></li>
    <?php
	}
	if(($this->users->user_category == 'Agent')||($this->users->user_category == 'Owner')){
	?>
	<li id="tab_li5_ads"><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=ads_management&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Your Ads</span></a></li>
    <?php
	}
	if($this->users->user_category == 'Agent'){
	?>
    <li id="tab_li5_transaction"><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=transaction&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Transactions</span></a></li>
    <li id="tab_li5_top_up" ><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=top_up&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Top-Up</span></a></li>
    <?php
	}
	?>
</ul>   
</div>

<div id="tablist">   
<?php
if($this->page_id == '1'){ // agent subscription
?>
<form action="index.php?option=com_ads" method="post" name="form2" id="form2">  
    <div id="tab_account" class="tabular1">
    	<div style="padding:10px;">
            <div id="static-text">
                    	<font style="color:#494949;">Subscription Plan Upgrade</font>
            </div>
            
            <div style="padding-bottom:20px;" id="selection_div">
            <br /><br />You have chosen <?php echo $this->rows[0]->name;?> at $<?php echo round($this->rate);?>.<br />
            All subscriptions are given <?php echo (($this->discount)*100);?>% discount.<br /><br />
            $<?php echo $this->after_discount;?> will be deducted from your credits.
            </div>
            
            <div style="font-weight:bold; float:left; width:660px; padding-top:20px; padding-bottom:20px;">
            Your credits will be deducted upon clicking on the button.
            </div>
            
            <div style="padding-top:10px; padding-bottom:20px; float:left; width:660px;">
                <div style="float:left;">
                    <input type="submit" value="Continue" class="contactbutton" name="agent_pay" id="agent_pay"/>
                </div>
            </div>
        </div>
    </div><!--tabular1-->
    <input type="hidden" name="option" id="option" value="com_ads" />
    <input type="hidden" name="Itemid" id="Itemid" value="126" />
    <input type="hidden" name="view" id="view" value="save_subscription" />
    <input type="hidden" name="aplan_exist" value="<?php echo $this->aplan_exist; ?>" />
    <input type="hidden" name="sub_plan" id="sub_plan" value="<?php echo $this->plan_id;?>" />
</form>
<?php
}
?>
</div><!--tablist-->

</div><!--end-->

</div><!--end view-->