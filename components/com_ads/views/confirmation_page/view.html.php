<?php
/**
 * @version		$Id: view.html.php 11646 2009-03-01 19:34:56Z ian $
 * @package		Joomla
 * @subpackage	Confirmation page
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Form component
 *
 * @static
 * @package		Joomla
 * @subpackage	Confirmation page
 * @since 1.0
 */
class AdsViewConfirmation_page extends JView
{
	function display($tpl = null)
	{
		global $mainframe;
		$db 	  	=& 	JFactory::getDBO();
		$user =& JFactory::getUser();
	 	$id = $user->get('id');
	 
		$page_id = JRequest::getVar( 'page_id', '', 'post', 'page_id' );
		$plan_id	= JRequest::getVar('sub_plan', '', 'post', 'sub_plan');
		
		$aplan_exist	= JRequest::getVar('agent_plan_id_exist', '', 'post', 'agent_plan_id_exist');
		
	
		$query="SELECT * FROM #__cbsubs_plans WHERE id = ".$plan_id;
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		
		$query = "SELECT rate FROM #__cbsubs_plans WHERE id = '".$plan_id."'";
		$db->setQuery($query);
		$rate = $db->loadResult();
		
		$query="SELECT params FROM #__ihouse_promotion WHERE id = '2'";
		$db->setQuery($query);
		$params = $db->loadResult();
		
		$discountone = explode("=", $params);
		$discount = $discountone[1];
		
		$after_discount = ((round($rate)) * (1 - ($discount)));
	
		$this->assignRef('users', $this->getInfoOfThisUser($id));
		$this->assignRef('plan_id', $plan_id);
		$this->assignRef('page_id', $page_id);
		$this->assignRef('rows', $rows);
		$this->assignRef('rate', $rate);
		$this->assignRef('discount', $discount);
		$this->assignRef('after_discount', $after_discount);
		
		$this->assignRef('aplan_exist',$aplan_exist);
		parent::display($tpl);
		
	}
	
	function getInfoOfThisUser($id = 0) {
		
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT * FROM #__users "
						.	" WHERE id = '$id'" ;
						
			$db->setQuery( $query );
			$list = $db->loadObject();		
			//echo print_r($list);
		return $list;	
	}
}
