﻿<?php
/**
 * @version		$Id: view.html.php 11646 2009-03-01 19:34:56Z ian $
 * @package		Joomla
 * @subpackage	iHouse360 Sales posting form
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the iHouse360 component
 *
 * @static
 * @package		Joomla
 * @subpackage	Sales posting form view
 * @since 1.0
 */
class AdsViewAdsdetails extends JView
{
	function display($tpl = null)
	{
		global $mainframe;
		
		$session = JFactory::getSession();
		if(preg_match('/task=adssearch/', $_SERVER['HTTP_REFERER'])) {
			$session->set('sessionsave', 1);
		}
		
		
		if(preg_match('/view=agent_profile/', $_SERVER['HTTP_REFERER'])) {
			$session->set('sessionsave', 1);
		}
		
		$layout = JRequest::getCmd('layout');
		
		$user 			=& 	JFactory::getUser();
		$uid				= $user->id;
		
		$id			= JRequest::getInt('id'); // ads_id

		$db		=& 	JFactory::getDBO();
		
		$query = 	" SELECT a.*,p.nearby_mrt, p.nearby_school FROM #__ihouse_ads AS a "
					.	" LEFT JOIN #__ihouse_property AS p ON p.id = a.property_id "
					.	" WHERE a.id = '$id' ";
				$db->setQuery( $query );
				$tmp = $db->loadObject();
		
		$document =& JFactory::getDocument();
		//$document->addScript('http://maps.google.com/maps/api/js?sensor=false&region=SG');
		$document->addScript('https://maps.googleapis.com/maps/api/js?key=AIzaSyAUWlGtel3Nchvh14BSi1aMwi09UtQtw1E&region=SG');
		$document->addScriptDeclaration($this->getGoogleMapAPI($tmp));
		$document->addScript( JURI::base(true). '/components/com_ihouse/assets/contacts.js' );
		
		$anytitle = '';
		$anytitle .=  $tmp->property_name.' : '.$tmp->ad_title;
				
		$document->setTitle($anytitle ."-".  $document->getTitle());
		$document->setDescription($anytitle."-".$document->getDescription());
		
		/* Log IP */
		if($this->hasVisitedByIP('ads', $id)) {
			$this->addPopularCounter($id);
		}
		
		$hasFloorplan	=	$this->hasFloorplan($id);
		$hasImages		=	$this->hasImages($id);
		
		$this->assignRef('hasFloorplan', $hasFloorplan);
		$this->assignRef('hasImages', $hasImages);
		
		$this->assignRef('is_favorited', $this->isFavorited($id, $uid));
		$this->assignRef('fav_id', $this->getFavId($id, $uid));
		$this->assignRef('user_id', $uid);
		//$this->assignRef('number_of_views', $this->numberOfViews($id));
		$this->assignRef('row',$tmp);
		
		$this->assignRef('fp1_img', $this->getFloorPlan($tmp, 1));
		$this->assignRef('fp2_img', $this->getFloorPlan($tmp, 2));
		
		parent::display($tpl);
		
	}
	
	function getPropertyTypeListHTML($selected = '') {
		
		$db		=& 	JFactory::getDBO();

		$query 	= ' SELECT * FROM #__ihouse_property_type WHERE id = 1 OR id = 2 ';
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		$html = '';
		$html .= '<select name="property_type" class="" onchange="javascript:propertyType(this.value);" id="property_type" >';
		$html .= '<option value="">----</option> ';
		foreach($rows as $r):
			$tmp	=	preg_replace('/\s+/','_',strtolower($r->name_en));	
			$html .= '<option value="'.$tmp.'" >'.$r->name_en.'</option>';
		
		endforeach;
       
        $html .= '</select>';
		
		return $html;
	}
	
	function hasFloorplan($ads_id = 0) {
		if(!$ads_id)
			return false;
			
		$db		=& 	JFactory::getDBO();
			$query = " SELECT id FROM #__ihouse_ads_floorplan WHERE ads_id = '$ads_id' ";
				$db->setQuery( $query );
				$rows = $db->loadObjectList();
		
		if(!empty($rows))
			return true;
			
		return false;
	}
	function hasImages($ads_id = 0) {
		if(!$ads_id)
			return false;
			
		$db		=& 	JFactory::getDBO();
		
		$query = " SELECT id FROM #__ihouse_ads_image WHERE ads_id = '$ads_id' ";
				$db->setQuery( $query );
				$rows = $db->loadObjectList();
		//echo print_r($rows);
		if(!empty($rows)) 
			return true;
		
		$query = " SELECT id FROM #__ihouse_ads_image2 WHERE ads_id = '$ads_id' ";
				$db->setQuery( $query );
				$rows = $db->loadObjectList();
		//echo print_r($rows);		
		
		if(!empty($rows)) 
			return true;
			
		return false;
	}
	
	function getGoogleMapAPI($adsObj){
		$db		=& 	JFactory::getDBO();
		
		$postcode 		= 	$adsObj->postcode;
		$property_type 	= 	$adsObj->property_type;
		$address		=	$adsObj->street_name;
		$country		=	$adsObj->country;
		
		$javascript = "
				var map;
				var geocoder;
		
				function initialize(){
					geocoder = new google.maps.Geocoder();
  					var latlng = new google.maps.LatLng(1.3667,103.8);
  					var options = {
   						zoom: 15,
    					center: latlng,
    					mapTypeId: google.maps.MapTypeId.ROADMAP
  					};
       
  					map = new google.maps.Map(document.getElementById('canvas_map'), options);
					

				}
			";	
		
		if($property_type == '公寓' || $property_type == '商业' || $property_type == '服务公寓' || $property_type == '有地住宅') {
		
  			$javascript .= "
				function geocodeGoogleMap() {
					var icon_img = '".JURI::root()."templates/mapsearch/images/building.png';	
					
					geocoder.geocode( { 'address': '".$country." ".$postcode." ".$address."'}, function(results, status) {
      					if (status == google.maps.GeocoderStatus.OK) {
		  
        					map.setCenter(results[0].geometry.location);
        					var marker = new google.maps.Marker({
            							map: map, 
            							position: results[0].geometry.location,
										icon : icon_img
        					});
      					} 
    				});
        					
				}
				
				window.addEvent('load', function(){ 
					setTimeout('initialize();',500);
					setTimeout('geocodeGoogleMap()', 1500);
				});
			";
		}
		else if($property_type == '组屋') {
			
			$ads_id			= $adsObj->id;
			$block_no		= $adsObj->block_no;
			$address		= $adsObj->street_name;

			$format_address = $block_no.' '.$address.' '.$postcode.' Singapore';
			
			$javascript .= "
				function geocodeGoogleMap() {
					var icon_img = '".JRoute::_('templates/mapsearch/images/building.png')."';	
					
					geocoder.geocode( { 'address': '".$format_address."'}, function(results, status) {
      					if (status == google.maps.GeocoderStatus.OK) {
							var marker = new google.maps.Marker({
            								map: map, 
            								position: results[0].geometry.location,
											title : '".$postcode."' ,
											icon : icon_img
        					});
							
        					map.setCenter(results[0].geometry.location);
        				} 
    				});	
				}
				
				window.addEvent('load', function(){ 
					setTimeout('initialize();',500);
					setTimeout('geocodeGoogleMap()', 1500);
				});
			";
		}
	    return $javascript;
	}
	
	function getFloorPlan($row, $ordering) {
		$db		=& 	JFactory::getDBO();
		
		$postcode		=	$row->postcode;
		$ads_id			=	$row->id;
			
		$html = '';
		
		$query 	= " SELECT fp FROM #__ihouse_ads_floorplan WHERE ads_id = '$ads_id' AND ordering = '$ordering' ";
			$db->setQuery( $query ) ;
			$result = $db->loadResult();
		
		if($result) {	
			$floorplan_img = 'http://images.sg.ihouse360.com/'.$postcode.'/floorplan/'.$result;
		
			$html .= '<img src="'.JRoute::_($floorplan_img).'" width="100%" height="100%" />';
		}
		return $html;	
	}
	
	function hasVisitedByIP($type, $id) {
		
		$ip = getenv('REMOTE_ADDR');
		
		$db 	  =& JFactory::getDBO();
		
		$params	=	"ads_id=".$id;
		
		$query	=	"SELECT id FROM #__ihouse_ip_log "
					. " WHERE ip = '$ip'  "
					. " AND type = '$type' "
					. " AND param LIKE '%".$params."%' ";
		
			$db->setQuery( $query ) ;
			$is_exist = $db->loadResult();		
		
		if($is_exist) 
			return false;
		
		
		
		if($type == 'ads') {
			
			$query	=	"INSERT INTO #__ihouse_ip_log VALUES(NULL, '$type','$ip','$params')";
			
				$db->setQuery( $query ) ;
				$db->query();
				
			return true;	
		}
			
	}
	
	function addPopularCounter($id = 0) {
		if(!$id)
			return false;
		
		$db 	  =& JFactory::getDBO();
		
		/* FOR TOTAL COUNTER (PROPERTY DETAIL + ADS DETAIL) - START - reseted every week */
		
		$query 	=	" SELECT a.property_id, a.property_type FROM #__ihouse_ads AS a "
					.	" WHERE a.id = '$id' "
					; 
					
			$db->setQuery( $query );
			$tmp		=	$db->loadObject();
			
			$property_id = $tmp->property_id;
			$property_type = $tmp->property_type;
		
		$query 	=	" SELECT * FROM #__ihouse_popular WHERE type = 'total' AND property_id = '$property_id' ";
			$db->setQuery( $query );
			$exists = $db->loadObject();
			
		if($property_type == '公寓') : // condo
		
		if(empty($exists)) {
			
			$query	=	"INSERT INTO #__ihouse_popular VALUES(NULL, 'total',1,'$property_id',0)";
			
				$db->setQuery( $query ) ;
				$db->query();
				
		} else {
			
			$query	=	" UPDATE #__ihouse_popular SET counter = counter + 1 "
					.	" WHERE property_id = '$property_id' AND type = 'total' " ;
					
				$db->setQuery( $query );
				$db->query();
		}
		
		endif;
	
	
		/* FOR TOTAL COUNTER (PROPERTY DETAIL + ADS DETAIL) - END */
	
		/* FOR COUNTER (ADS DETAIL) - START - no reset */
		$query	=	" SELECT id FROM #__ihouse_popular "
					.	" WHERE ads_id = '$id' AND type = 'ads' LIMIT 1" ;
			
			$db->setQuery( $query ) ;
			$is_exist	=	$db->loadResult();
		
		
		
		if($is_exist) {
			
			$query	=	"UPDATE #__ihouse_popular SET counter = counter + 1"
					.	" WHERE ads_id = '$id' AND type = 'ads' " ;
			
				$db->setQuery( $query ) ;
				$db->query();
			
		} else {
			
			$query	=	"INSERT INTO #__ihouse_popular VALUES(NULL, 'ads',1,0,'$id')";
			
				$db->setQuery( $query ) ;
				$db->query();
				
		}
		
		
		return true;
	}
	
	function numberOfViews($id = 0) {
		if(!$id)
			return false;
		
		$db 	  =& JFactory::getDBO();
		
		$query	=	"SELECT counter FROM #__ihouse_popular "
					.	" WHERE ads_id = '$id' AND type = 'ads' LIMIT 1" ;
			
			$db->setQuery( $query ) ;
			$counter	=	$db->loadResult();
		
		return $counter;
	}
	
	function isFavorited($ads_id, $uid) {
		if(!$uid)
			return 0;
		
		$db 	  =& JFactory::getDBO();
		
		$query	=	" SELECT * FROM #__ihouse_favorites "
					.	" WHERE aid = '$ads_id' AND user_id = '$uid' AND type = 'ads' LIMIT 1" ;
			
			$db->setQuery( $query ) ;
			$row	=	$db->loadObject();
		
		if(!empty($row)) {
			return 2;
		} else {
			return 1;
		}
	}
	
	function getFavId($ads_id, $uid) {
		if(!$uid)
			return false;
		
		$db 	  =& JFactory::getDBO();
		
		$query	=	"SELECT * FROM #__ihouse_favorites "
					.	" WHERE aid = '$ads_id' AND user_id = '$uid' AND type = 'ads' LIMIT 1" ;
			
			$db->setQuery( $query ) ;
			$row	=	$db->loadObject();
		
		return $row->id;
	}
}
