﻿<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('behavior.tooltip'); ?>

<style type="text/css">

#tablist {

 padding-bottom:5px;
 width:661px; 
 overflow:hidden; 
 height:auto; 
 min-height:270px;
}

#tablist h2 {
 display:none;
}
#tablist h3 {
 display:none;
}

.tabberlive#tab2 #tablist {
 height:200px;
 overflow:auto;
}

</style>


<div><!--Start view-->


<!--Start of tabs-->
                            <div id="module-content-padding7">
                            	<div class="tabber">
                                     <div class="tabbertab" id="tablist">
                                      <h2>项目详细</h2>
                                      <div class="tabItem1">
                                        <div>
                                            
                                            <!--Start-->
                                            <div id="tabcont-ads">
                                                   <div id="ads-header-text">项目详细</div>
                                                   
                                                   <div id="ads-module">
                                                        <div id="module-headerpos">
                                                                <div id="module-title">Featured Properties</div>
                                                        </div>
                                                       
                                                    </div>
                                                    
                                                     <div style="float:left; width:659px; height:auto; padding-top:10px; padding-bottom:10px; border-left:solid 1px #ccc; border-right:solid 1px #ccc; border-bottom:solid 1px #ccc;"> <!--Start of rokbox-->
                                                        
                                                        <div style="position:relative;z-index:1000;">				
        	<img id="sliders_pt_lft" src="<?php echo JRoute::_('templates/main/images/arrow_left_1.png') ?>" onclick="javascript:movePage('prev');" style="display:none;" />
        	<img id="sliders_pt_rgt" src="<?php echo JRoute::_('templates/main/images/arrow_right_1.png') ?>" onclick="javascript:movePage('next');" />
        	</div>
            
            											<div class="wrapper_info">
                        
                        	<div class="wrapper_info_content" id="propertyHighlightDetail"> 
                           	<?php for($i= 1; $i <= 8; $i++): ?>
                            <div class="list_prop_high">
                            	<div id="tabimg">
                                <img style="width:150px;" src="<?php echo JRoute::_('templates/main/images/thumb-noimage.jpg'); ?>">
                                </div>
                                <div style="clear:both"></div>                
                                <div id="">
                                	<div id="tabtext-padding1">Serangoon North 4 room HDB flat <?php echo $i ?><br>(实龙岗北4房式组屋)</div>
                                	<div id="tabtext-font">户型：2房1厅<br>楼层：8/14 层<br>价格：SGD$ 400,000</div>
                                </div>
                            </div>
                            
                            <?php endfor; ?>
                            </div>
                         </div>
                                                        
                                                        
                                                        
                                                     </div><!--End of rokbox-->
                                                     
                                                     
                                            <div style="padding-top:10px; float:left; width:100%;"><!--Start of 2nd module-->
                                            	<div id="ads-module">
                                                    <div id="module-headerpos">
                                                            <div id="module-title">Featured Properties by Agent</div>
                                                    </div>
                                                </div>
                                                
                                                <div style="float:left; width:659px; height:auto; padding-top:10px; padding-bottom:10px; border-left:solid 1px #ccc; border-right:solid 1px #ccc; border-bottom:solid 1px #ccc;"> <!--Start of content-->
                                                
                                                </div><!--End of content-->
                                            </div><!--End of 2nd module-->
                                            
                                            <div style="padding-top:10px; float:left; width:100%;"><!--Start of 3rd module-->
                                            	<div id="ads-module">
                                                    <div id="module-headerpos">
                                                            <div id="module-title">Featured Properties by Owner</div>
                                                    </div>
                                                </div>
                                                
                                                <div style="float:left; width:659px; height:auto; padding-top:10px; padding-bottom:10px; border-left:solid 1px #ccc; border-right:solid 1px #ccc; border-bottom:solid 1px #ccc;"> <!--Start of content-->
                                                
                                                </div><!--End of content-->
                                            </div><!--End of 3rd module-->
                                                    
                                            </div>
                                            <!--End-->
                                            
                                          
                                      	</div>
                                      </div><!--tabItem1-->
                                     </div><!--tabbertab-->
                                
                                
                                     <div class="tabbertab" id="tablist">
                                      <h2>出售</h2>
                                      <div class="tabItem1">
                                        <div>
                                        	<!--Start-->
                                            <div id="tabcont-ads">
                                                   <div id="ads-header-text">出售</div>
                                                   
                                                   <div id="ads-module">
                                                        <div id="module-headerpos">
                                                                <div id="module-title">Featured Properties</div>
                                                        </div>
                                                       
                                                    </div>
                                                    
                                                     <div style="float:left; width:659px; height:auto; padding-top:10px; padding-bottom:10px; border-left:solid 1px #ccc; border-right:solid 1px #ccc; border-bottom:solid 1px #ccc;"> <!--Start of rokbox-->
                                                        
                                                        <div style="position:relative;z-index:1000;">				
        	<img id="sliders_pt_lft1" src="<?php echo JRoute::_('templates/main/images/arrow_left_1.png') ?>" onclick="javascript:movePage1('prev');" style="display:none;" />
        	<img id="sliders_pt_rgt1" src="<?php echo JRoute::_('templates/main/images/arrow_right_1.png') ?>" onclick="javascript:movePage1('next');" />
        	</div>
            
            											<div class="wrapper_info">
                        
                        	<div class="wrapper_info_content" id="propertyHighlightDetail1"> 
                           	<?php for($i= 1; $i <= 8; $i++): ?>
                            <div class="list_prop_high">
                            	<div id="tabimg">
                                <img style="width:150px;" src="<?php echo JRoute::_('templates/main/images/thumb-noimage.jpg'); ?>">
                                </div>
                                <div style="clear:both"></div>                
                                <div id="">
                                	<div id="tabtext-padding1">Serangoon North 4 room HDB flat <?php echo $i ?><br>(实龙岗北4房式组屋)</div>
                                	<div id="tabtext-font">户型：2房1厅<br>楼层：8/14 层<br>价格：SGD$ 400,000</div>
                                </div>
                            </div>
                            
                            <?php endfor; ?>
                            </div>
                         </div>
                                                        
                                                        
                                                        
                                                     </div><!--End of rokbox-->
                                                     
                                                     
                                            <div style="padding-top:10px; float:left; width:100%;"><!--Start of 2nd module-->
                                            	
                                                
                                               <?php /*?> <div style="float:left; width:659px; height:auto; padding-top:10px; padding-bottom:10px; border-left:solid 1px #ccc; border-right:solid 1px #ccc; border-bottom:solid 1px #ccc;"> <?php */?><!--Start of content-->
                                                
                                                  <div style="float:left; width:659px; height:auto; padding-top:10px; padding-bottom:10px; border-left:solid 1px #ccc; border-right:solid 1px #ccc; border-bottom:solid 1px #ccc; border-top:solid 1px #ccc;"> <!--Start of content-->
                                                	<div style="border-bottom:solid 1px #ccc; padding-bottom:10px; float:left; width:100%; height:auto; padding-left:10px;">
                                                    	<div style="float:left; color:#3a3a3a; width:100px; line-height:20px;">
                                                        <input type="checkbox" />&nbsp;&nbsp;&nbsp;全选 / 不选
                                                        </div>
                                                        
                                                        <div style="float:left; width:300px;">
                                                        <input type="submit" value="比较" class="searchbutton" />&nbsp;
                                                        <input type="submit" value="联络经纪" class="contactbutton"  />
                                                        </div>
                                                        
                                                        <div style="float:left; line-height:25px;">
                                                        排序&nbsp;&nbsp;
                                                        <img src="<?php echo JRoute::_('templates/main/images/arrow.png'); ?>" />&nbsp;&nbsp;
                                                        <font style="color:#062284;">
                                                        户型&nbsp;&nbsp; | &nbsp;&nbsp;类型&nbsp;&nbsp; | &nbsp;&nbsp;价格 &nbsp;&nbsp; | &nbsp;&nbsp;更新日期
                                                        </font>
                                                        </div>
                                                        
                                                    </div>
                                                    
                                                    <div style="border-bottom:solid 1px #ccc; padding-bottom:10px; float:left; width:100%; height:auto; padding-left:10px; padding-top:20px; padding-bottom:20px; color:#5b5b5b;">
                                                    	<div style="float:left; width:30px;">
                                                        <input type="checkbox" />
                                                        </div>
                                                        
                                                        <div style="float:left; width:170px;">
                                                        <img src="<?php echo JRoute::_('templates/main/images/adsimg.jpg'); ?>" />
                                                        </div>
                                                        
                                                        <div style="float:left; width:280px;">
                                                        	<div style="float:left; width:280px; color:#062284; font-weight:bold; padding-bottom:5px;">Chantilly Rise</div>
                                                            <div style="float:left; width:100%; line-height:18px;">
                                                            	<div style="float:left; width:60px;">地址</div>
                                                                <div style="float:left; width:10px;">:</div>
                                                                <div style="float:left; width:200px;">82 Hillview Avenue Condominium</div>
                                                            </div>
                                                            
                                                            <div style="float:left; width:100%; line-height:18px;">
                                                            	<div style="float:left; width:60px;">价格</div>
                                                                <div style="float:left; width:10px;">:</div>
                                                                <div style="float:left; width:200px;">SGD$ 1200</div>
                                                            </div>
                                                            
                                                            <div style="float:left; width:100%; line-height:18px;">
                                                            	<div style="float:left; width:60px;">户型</div>
                                                                <div style="float:left; width:10px;">:</div>
                                                                <div style="float:left; width:200px;">3房1厅</div>
                                                            </div>
                                                            
                                                            <div style="float:left; width:100%; line-height:18px;">
                                                            	<div style="float:left; width:60px;">邮区</div>
                                                                <div style="float:left; width:10px;">:</div>
                                                                <div style="float:left; width:200px;">553121</div>
                                                            </div>
                                                            
                                                            <div style="float:left; width:100%; line-height:18px;">
                                                            	<div style="float:left; width:60px;">更新日期</div>
                                                                <div style="float:left; width:10px;">:</div>
                                                                <div style="float:left; width:200px;">2010年9月5日</div>
                                                            </div>
                                                            
                                                            <div style="float:left; width:100%; line-height:18px;">
                                                            	<div style="float:left; width:60px;">目前状态</div>
                                                                <div style="float:left; width:10px;">:</div>
                                                                <div style="float:left; width:200px;">待租</div>
                                                            </div>
                                                            
                                                            <div style="float:left; width:100%; line-height:18px;">
                                                            	<div style="float:left; width:60px;">物业类别</div>
                                                                <div style="float:left; width:10px;">:</div>
                                                                <div style="float:left; width:200px;">组屋</div>
                                                            </div>
                                                            
                                                            <div style="float:left; width:100%; line-height:18px;">
                                                            	<div style="float:left; width:60px;">类型</div>
                                                                <div style="float:left; width:10px;">:</div>
                                                                <div style="float:left; width:200px;">房间</div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div style="float:left; width:88px;">
                                                        	<div style="border:solid 1px #d1d1d1;">
                                                        		<img src="<?php echo JRoute::_('templates/main/images/agentimg.jpg'); ?>" />
                                                        	</div>
                                                        </div>
                                                        
                                                        <div style="float:left; width:65px; padding-left:10px;">
                                                        林经纪<br />9331 2112
                                                        </div>
                                                    </div>
                                                    
                                                <?php /*?></div><?php */?><!--End of content-->
                                                
                                                </div><!--End of content-->
                                            </div><!--End of 2nd module-->
                                            
                                          
                                                    
                                            </div>
                                            <!--End-->
                                        
                                        
                                        </div>
                                      </div>
                                     </div><!--tabbertab-->
                                     
                                     <div class="tabbertab" id="tablist">
                                      <h2>出租</h2>
                                      <div class="tabItem1">
                                        <div>
                                            
                                             <!--Start-->
                                            <div id="tabcont-ads">
                                                   <div id="ads-header-text">出租</div>
                                                   
                                                   <div id="ads-module">
                                                        <div id="module-headerpos">
                                                                <div id="module-title">Featured Properties</div>
                                                        </div>
                                                       
                                                    </div>
                                                    
                                                     <div style="float:left; width:659px; height:auto; padding-top:10px; padding-bottom:10px; border-left:solid 1px #ccc; border-right:solid 1px #ccc; border-bottom:solid 1px #ccc;"> <!--Start of rokbox-->
                                                        
                                                        <div style="position:relative;z-index:1000;">				
        	<img id="sliders_pt_lft2" src="<?php echo JRoute::_('templates/main/images/arrow_left_1.png') ?>" onclick="javascript:movePage2('prev');" style="display:none;" />
        	<img id="sliders_pt_rgt2" src="<?php echo JRoute::_('templates/main/images/arrow_right_1.png') ?>" onclick="javascript:movePage2('next');" />
        	</div>
            
            											<div class="wrapper_info">
                        
                        	<div class="wrapper_info_content" id="propertyHighlightDetail2"> 
                           	<?php for($i= 1; $i <= 8; $i++): ?>
                            <div class="list_prop_high">
                            	<div id="tabimg">
                                <img style="width:150px;" src="<?php echo JRoute::_('templates/main/images/thumb-noimage.jpg'); ?>">
                                </div>
                                <div style="clear:both"></div>                
                                <div id="">
                                	<div id="tabtext-padding1">Serangoon North 4 room HDB flat <?php echo $i ?><br>(实龙岗北4房式组屋)</div>
                                	<div id="tabtext-font">户型：2房1厅<br>楼层：8/14 层<br>价格：SGD$ 400,000</div>
                                </div>
                            </div>
                            
                            <?php endfor; ?>
                            </div>
                         </div>
                                                        
                                                        
                                                        
                                                     </div><!--End of rokbox-->
                                                     
                                                     
                                            <div style="padding-top:10px; float:left; width:100%;"><!--Start of 2nd module-->
                                            	<div id="ads-module">
                                                    <div id="module-headerpos">
                                                            <div id="module-title">Featured Properties by Agent</div>
                                                    </div>
                                                </div>
                                                
                                                <div style="float:left; width:659px; height:auto; padding-top:10px; padding-bottom:10px; border-left:solid 1px #ccc; border-right:solid 1px #ccc; border-bottom:solid 1px #ccc;"> <!--Start of content-->
                                                
                                                	<div style="border-bottom:solid 1px #ccc; padding-bottom:10px; float:left; width:100%; height:auto; padding-left:10px;">
                                                    	<div style="float:left; color:#3a3a3a; width:100px; line-height:20px;">
                                                        <input type="checkbox" />&nbsp;&nbsp;&nbsp;全选 / 不选
                                                        </div>
                                                        
                                                        <div style="float:left; width:300px;">
                                                        <input type="submit" value="比较" class="searchbutton" />&nbsp;
                                                        <input type="submit" value="联络经纪" class="contactbutton"  />
                                                        </div>
                                                        
                                                        <div style="float:left; line-height:25px;">
                                                        排序&nbsp;&nbsp;
                                                        <img src="<?php echo JRoute::_('templates/main/images/arrow.png'); ?>" />&nbsp;&nbsp;
                                                        <font style="color:#062284;">
                                                        户型&nbsp;&nbsp; | &nbsp;&nbsp;类型&nbsp;&nbsp; | &nbsp;&nbsp;价格 &nbsp;&nbsp; | &nbsp;&nbsp;更新日期
                                                        </font>
                                                        </div>
                                                        
                                                    </div>
                                                    
                                                    <div style="border-bottom:solid 1px #ccc; padding-bottom:10px; float:left; width:100%; height:auto; padding-left:10px; padding-top:20px; padding-bottom:20px; color:#5b5b5b;">
                                                    	<div style="float:left; width:30px;">
                                                        <input type="checkbox" />
                                                        </div>
                                                        
                                                        <div style="float:left; width:170px;">
                                                        <img src="<?php echo JRoute::_('templates/main/images/adsimg.jpg'); ?>" />
                                                        </div>
                                                        
                                                        <div style="float:left; width:280px;">
                                                        	<div style="float:left; width:280px; color:#062284; font-weight:bold; padding-bottom:5px;">Chantilly Rise</div>
                                                            <div style="float:left; width:100%; line-height:18px;">
                                                            	<div style="float:left; width:60px;">地址</div>
                                                                <div style="float:left; width:10px;">:</div>
                                                                <div style="float:left; width:200px;">82 Hillview Avenue Condominium</div>
                                                            </div>
                                                            
                                                            <div style="float:left; width:100%; line-height:18px;">
                                                            	<div style="float:left; width:60px;">价格</div>
                                                                <div style="float:left; width:10px;">:</div>
                                                                <div style="float:left; width:200px;">SGD$ 1200</div>
                                                            </div>
                                                            
                                                            <div style="float:left; width:100%; line-height:18px;">
                                                            	<div style="float:left; width:60px;">户型</div>
                                                                <div style="float:left; width:10px;">:</div>
                                                                <div style="float:left; width:200px;">3房1厅</div>
                                                            </div>
                                                            
                                                            <div style="float:left; width:100%; line-height:18px;">
                                                            	<div style="float:left; width:60px;">邮区</div>
                                                                <div style="float:left; width:10px;">:</div>
                                                                <div style="float:left; width:200px;">553121</div>
                                                            </div>
                                                            
                                                            <div style="float:left; width:100%; line-height:18px;">
                                                            	<div style="float:left; width:60px;">更新日期</div>
                                                                <div style="float:left; width:10px;">:</div>
                                                                <div style="float:left; width:200px;">2010年9月5日</div>
                                                            </div>
                                                            
                                                            <div style="float:left; width:100%; line-height:18px;">
                                                            	<div style="float:left; width:60px;">目前状态</div>
                                                                <div style="float:left; width:10px;">:</div>
                                                                <div style="float:left; width:200px;">待租</div>
                                                            </div>
                                                            
                                                            <div style="float:left; width:100%; line-height:18px;">
                                                            	<div style="float:left; width:60px;">物业类别</div>
                                                                <div style="float:left; width:10px;">:</div>
                                                                <div style="float:left; width:200px;">组屋</div>
                                                            </div>
                                                            
                                                            <div style="float:left; width:100%; line-height:18px;">
                                                            	<div style="float:left; width:60px;">类型</div>
                                                                <div style="float:left; width:10px;">:</div>
                                                                <div style="float:left; width:200px;">房间</div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div style="float:left; width:88px;">
                                                        	<div style="border:solid 1px #d1d1d1;">
                                                        		<img src="<?php echo JRoute::_('templates/main/images/agentimg.jpg'); ?>" />
                                                        	</div>
                                                        </div>
                                                        
                                                        <div style="float:left; width:65px; padding-left:10px;">
                                                        林经纪<br />9331 2112
                                                        </div>
                                                    </div>
                                                    
                                                </div><!--End of content-->
                                            </div><!--End of 2nd module-->
                                            
                                            <div style="padding-top:10px; float:left; width:100%;"><!--Start of 3rd module-->
                                            	<div id="ads-module">
                                                    <div id="module-headerpos">
                                                            <div id="module-title">Featured Properties by Owner</div>
                                                    </div>
                                                </div>
                                                
                                                <div id="featured_owner_cont"> <!--Start of content-->
                                                	<div style="border-bottom:solid 1px #ccc; padding-bottom:10px; float:left; width:100%; height:auto; padding-left:10px;">
                                                    	<div style="float:left; color:#3a3a3a; width:100px; line-height:20px;">
                                                        <input type="checkbox" />&nbsp;&nbsp;&nbsp;全选 / 不选
                                                        </div>
                                                        
                                                        <div style="float:left; width:300px;">
                                                        <input type="submit" value="比较" class="searchbutton" />&nbsp;
                                                        <input type="submit" value="联络经纪" class="contactbutton"  />
                                                        </div>
                                                        
                                                        <div style="float:left; line-height:25px;">
                                                        排序&nbsp;&nbsp;
                                                        <img src="<?php echo JRoute::_('templates/main/images/arrow.png'); ?>" />&nbsp;&nbsp;
                                                        <font style="color:#062284;">
                                                        户型&nbsp;&nbsp; | &nbsp;&nbsp;类型&nbsp;&nbsp; | &nbsp;&nbsp;价格 &nbsp;&nbsp; | &nbsp;&nbsp;更新日期
                                                        </font>
                                                        </div>
                                                        
                                                    </div>
                                                    
                                                    <div style="border-bottom:solid 1px #ccc; padding-bottom:10px; float:left; width:100%; height:auto; padding-left:10px; padding-top:20px; padding-bottom:20px; color:#5b5b5b;">
                                                    	<div style="float:left; width:30px;">
                                                        <input type="checkbox" />
                                                        </div>
                                                        
                                                        <div style="float:left; width:170px;">
                                                        <img src="<?php echo JRoute::_('templates/main/images/adsimg.jpg'); ?>" />
                                                        </div>
                                                        
                                                        <div style="float:left; width:280px;">
                                                        	<div style="float:left; width:280px; color:#062284; font-weight:bold; padding-bottom:5px;">Chantilly Rise</div>
                                                            <div style="float:left; width:100%; line-height:18px;">
                                                            	<div style="float:left; width:60px;">地址</div>
                                                                <div style="float:left; width:10px;">:</div>
                                                                <div style="float:left; width:200px;">82 Hillview Avenue Condominium</div>
                                                            </div>
                                                            
                                                            <div style="float:left; width:100%; line-height:18px;">
                                                            	<div style="float:left; width:60px;">价格</div>
                                                                <div style="float:left; width:10px;">:</div>
                                                                <div style="float:left; width:200px;">SGD$ 1200</div>
                                                            </div>
                                                            
                                                            <div style="float:left; width:100%; line-height:18px;">
                                                            	<div style="float:left; width:60px;">户型</div>
                                                                <div style="float:left; width:10px;">:</div>
                                                                <div style="float:left; width:200px;">3房1厅</div>
                                                            </div>
                                                            
                                                            <div style="float:left; width:100%; line-height:18px;">
                                                            	<div style="float:left; width:60px;">邮区</div>
                                                                <div style="float:left; width:10px;">:</div>
                                                                <div style="float:left; width:200px;">553121</div>
                                                            </div>
                                                            
                                                            <div style="float:left; width:100%; line-height:18px;">
                                                            	<div style="float:left; width:60px;">更新日期</div>
                                                                <div style="float:left; width:10px;">:</div>
                                                                <div style="float:left; width:200px;">2010年9月5日</div>
                                                            </div>
                                                            
                                                            <div style="float:left; width:100%; line-height:18px;">
                                                            	<div style="float:left; width:60px;">目前状态</div>
                                                                <div style="float:left; width:10px;">:</div>
                                                                <div style="float:left; width:200px;">待租</div>
                                                            </div>
                                                            
                                                            <div style="float:left; width:100%; line-height:18px;">
                                                            	<div style="float:left; width:60px;">物业类别</div>
                                                                <div style="float:left; width:10px;">:</div>
                                                                <div style="float:left; width:200px;">组屋</div>
                                                            </div>
                                                            
                                                            <div style="float:left; width:100%; line-height:18px;">
                                                            	<div style="float:left; width:60px;">类型</div>
                                                                <div style="float:left; width:10px;">:</div>
                                                                <div style="float:left; width:200px;">房间</div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div style="float:left; width:88px;">
                                                        	<div style="border:solid 1px #d1d1d1;">
                                                        		<img src="<?php echo JRoute::_('templates/main/images/agentimg.jpg'); ?>" />
                                                        	</div>
                                                        </div>
                                                        
                                                        <div style="float:left; width:65px; padding-left:10px;">
                                                        林经纪<br />9331 2112
                                                        </div>
                                                    </div>
                                                    
                                                </div><!--End of content-->
                                            </div><!--End of 3rd module-->
                                                    
                                            </div>
                                            <!--End-->
                                            
                                            
                                            
                                        </div>
                                      </div>
                                     </div><!--tabbertab-->
                                     
                                      <div class="tabbertab tabbertabdefault" id="tablist">
                                      <h2>Posting of Ads</h2>
                                      <div class="tabItem1">
                                        <div>
                                            
                                             <!--Start-->
                                            <div id="tabcont-ads">
                                                   <table cellpadding="0" cellspacing="0" border="0" width="600" class="contentpane">
                                                   		<tr>
                                                        	<td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Property Type *
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <select class="inputbox">
                                                            	<option value="公寓">Condo</option>
                                                                <option value="组屋">HDB</option>
                                                                <option value="服务公寓">Service Apartment</option>
                                                                <option value="商业">Industrial Estate</option>
                                                                <option value="其他">Others</option>
                                                            </select>
                                                            </div>
                                                            </td>
                                                            <td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																HDB Estate *
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <input type="text" name="property_type" id="property_type" size="40" value="" class="inputbox" maxlength="50" />
                                                            </div>
                                                            </td>
                                                   		</tr>
                                                        <tr>
                                                        	<td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Property Category *
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <select class="inputbox">
                                                            	<option value="新房">New Flat</option>
                                                                <option value="圣淘沙">Sentosa</option>
                                                                <option value="其他">Others</option>
                                                            </select>
                                                            </div>
                                                            </td>
                                                            <td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Property Name *
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <input type="text" name="property_type" id="property_type" size="40" value="" class="inputbox" maxlength="50" />
                                                            </div>
                                                            </td>
                                                   		</tr>
                                                        <tr>
                                                        	<td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Asking Price
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <input type="text" name="property_type" id="property_type" size="40" value="" class="inputbox" maxlength="50" />
                                                            </div>
                                                            </td>
                                                            <td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Psm Price
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <input type="text" name="property_type" id="property_type" size="40" value="" class="inputbox" maxlength="50" />
                                                            </div>
                                                            </td>
                                                   		</tr>
                                                        <tr>
                                                        	<td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Size *
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <input type="text" name="property_type" id="property_type" size="40" value="" class="inputbox" maxlength="50" />
                                                            </div>
                                                            </td>
                                                            <td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Number of Room
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <select class="inputbox">
                                                            	<option>1</option>
                                                                <option>2</option>
                                                                <option>3</option>
                                                                <option>4</option>
                                                                <option>5</option>
                                                                <option>6</option>
                                                            </select>
                                                            </div>
                                                            </td>
                                                   		</tr>
                                                        <tr>
                                                        	<td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Property District *
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <input type="text" name="property_type" id="property_type" size="40" value="" class="inputbox" maxlength="50" />
                                                            </div>
                                                            </td>
                                                        	
                                                            <td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Number of Hall
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <select class="inputbox">
                                                            	<option>0</option>
                                                            	<option>1</option>
                                                                <option>2</option>
                                                                <option>3</option>
                                                            </select>
                                                            </div>
                                                            </td>
                                                   		</tr>
                                                        <tr>
                                                        	<td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Postal Code
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <input type="text" name="property_type" id="property_type" size="40" value="" class="inputbox" maxlength="50" />
                                                            </div>
                                                            </td>
                                                        	
                                                            <td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Study Room
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <select class="inputbox">
                                                            	<option>0</option>
                                                            	<option>1</option>
                                                            </select>
                                                            </div>
                                                            </td>
                                                            
                                                   		</tr>
                                                        <tr>
                                                        	
                                                            <td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Tenure
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <input type="text" name="property_type" id="property_type" size="40" value="" class="inputbox" maxlength="50" />
                                                            </div>
                                                            </td>
                                                            <td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Toilet
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <select class="inputbox">
                                                            	<option>1</option>
                                                                <option>2</option>
                                                                <option>3</option>
                                                                <option>4</option>
                                                            </select>
                                                            </div>
                                                            </td>
                                                            
                                                   		</tr>
                                                        <tr>
                                                        	<td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																TOP Year
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <input type="text" name="property_type" id="property_type" size="40" value="" class="inputbox" maxlength="50" />
                                                            </div>
                                                            </td>
                                                            <td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Floor Level
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <select class="inputbox">
                                                            	<option value="高">High</option>
                                                                <option value="中">Mid</option>
                                                                <option value="低">Low</option>
                                                            </select>
                                                            </div>
                                                            </td>
                                                            
                                                   		</tr>
                                                        <tr>
                                                        	<td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Block/House Number
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <input type="text" name="property_type" id="property_type" size="40" value="" class="inputbox" maxlength="50" />
                                                            </div>
                                                            </td>
                                                        	
                                                            <td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Floor/Unit Number
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <input type="text" name="property_type" id="property_type" size="40" value="" class="inputbox" maxlength="50" />
                                                            </div>
                                                            </td>
                                                   		</tr>
                                                        <tr>
                                                        	<td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Street Name
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <input type="text" name="property_type" id="property_type" size="40" value="" class="inputbox" maxlength="50" />
                                                            </div>
                                                            </td>
                                                        	
                                                            <td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Country *
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <input type="text" name="property_type" id="property_type" size="40" value="" class="inputbox" maxlength="50" />
                                                            </div>
                                                            </td>
                                                   		</tr>
                                                        <tr>
                                                        	<td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Ad Title *
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <input type="text" name="property_type" id="property_type" size="40" value="" class="inputbox" maxlength="50" />
                                                            </div>
                                                            </td>
                                                            <td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Additional Info
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <input type="text" name="property_type" id="property_type" size="40" value="" class="inputbox" maxlength="50" />
                                                            </div>
                                                            </td>
                                                   		</tr>
                                                        <tr>
                                                        	<td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Renovation
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                           	<select class="inputbox">
                                                            	<option value="新装修">Newly Renovated</option>
                                                                <option value="普通">Normal</option>
                                                                <option value="豪华">Luxurious</option>
                                                            </select>
                                                            </div>
                                                            </td>
                                                            <td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Status
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <select class="inputbox">
                                                            	<option value="待售">Pending</option>
                                                                <option value="已售">Sold</option>
                                                            </select>
                                                            </div>
                                                            </td>
                                                   		</tr>
                                                        <tr>
                                                        	<td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Posting Date
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <input type="text" name="property_type" id="property_type" size="40" value="" class="inputbox" maxlength="50" />
                                                            </div>
                                                            </td>
                                                            <td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Images Uploading
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <input type="text" name="property_type" id="property_type" size="40" value="" class="inputbox" maxlength="50" />
                                                            </div>
                                                            </td>
                                                   		</tr>
                                                   </table>
                                                    <div id="button_div">
	<button class="button validate" type="submit"><?php echo JText::_('Submit'); ?></button>
	<input type="hidden" name="task" value="register_save" />
    <input type="hidden" id="email" name="email"  value=""/>
	<input type="hidden" name="id" value="0" />
	<input type="hidden" name="gid" value="0" />
	<?php echo JHTML::_( 'form.token' ); ?>
</div>
                                                   
      
                                            </div>
                                            <!--End-->
                                            
                                            
                                            
                                        </div>
                                      </div>
                                     </div><!--tabbertab-->
                                
                                </div><!--tabber-->

                             </div>
                             <!--End of tabs-->


</div> <!--End view-->
