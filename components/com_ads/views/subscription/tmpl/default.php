﻿<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('script', 'ihouse.js', 'components/com_ads/rokbox/'); ?>

<style type="text/css">
#tablist {
	border-left:3px solid #FF8200;
	border-bottom:3px solid #FF8200;
	border-right:3px solid #FF8200;
	width:99.1%;
	padding:0;
	margin:0;
	min-height:300px;
	height:auto;
	float:left;
}

table#prop_det_info td{
	border:1px solid #999;
}

ul.tab_nav3 {
list-style-image:none;
list-style-type:none;
}
</style>

<!--Start View-->
<div style="margin-top:-20px;">

<div id="main-search-header-text" style="padding-left:15px;border-width:2px;">会员中心&nbsp;<span style="font-size:15px;font-weight:100;"><i><?php echo $this->users->user_category ?>&nbsp;Centre</i></span></div>

<div style="padding:15px;"><!--start-->

<div id="menu_list_profile_detail">
<ul id="" class="tab_nav5">     
	<li id="tab_li5_account" class="tabactive5"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=account_info&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Acct Info</span></a></li>
     <?php
	if(($this->users->user_category == 'Agent')||($this->users->user_category == 'Owner')||($this->users->user_category == 'Individual')){
	?>
	<li id="tab_li5_profile"><a href="<?php echo JURI::root();?>index.php?option=com_user&view=profiler&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Profile</span></a></li>
    <?php
	}
	if(($this->users->user_category == 'Agent')||($this->users->user_category == 'Owner')){
	?>
	<li id="tab_li5_ads"><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=ads_management&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Your Ads</span></a></li>
    <?php
	}
	if($this->users->user_category == 'Agent'){
	?>
    <li id="tab_li5_transaction"><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=transaction&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Transactions</span></a></li>
    <li id="tab_li5_top_up" ><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=top_up&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Top-Up</span></a></li>
    <?php
	}
	?>
</ul>   
</div>

<div id="tablist">   
<form action="index.php?option=com_ads" method="post" name="form2" id="form2">  
    <div id="tab_account" class="tabular1">
    	<div style="padding:10px;">
            <div id="static-text">
                    	<font style="color:#494949;">Subscription Plan Upgrade</font>
            </div>
            
            
        <?php
		if($this->users->user_category == 'Agent'){
		?>
        <div style="padding-bottom:20px;" id="selection_div">
        <br /><br />Please choose the type of Subscription plan you would like to upgrade to.&nbsp;&nbsp;
        </div>
       
        	
            <div id="agentsub">
        		<div style="float:left; width:600px; height:40px;">
                	<div style="float:left; width:150px;"><font color="#fe8700">订阅的广告计划</font><br />Account Type</div>
                	<div style="float:left; width:450px;">
                    	<select id="sub_plan" name="sub_plan" class="ajax_input">
                        <option value="">Please select</option>
                        <?php if(!$this->aplanid_exist) : ?>
                        <option value="2">Quarterly Subscription @ S$120</option>
                        <?php endif ?>
                        <option value="3">Yearly Subscription @ S$388</option>
                        </select>
                    </div>
                </div>
                
                <div style="font-weight:bold; float:left; width:660px; padding-top:20px; padding-bottom:20px;">
                <?php /*?>Your credits will be deducted upon clicking on the button.<?php */?>
                </div>
                
                <div style="padding-top:10px; padding-bottom:20px; float:left; width:660px;">
                    <div style="float:left;">
                        <input type="submit" value="Continue" class="contactbutton" name="agent_pay" id="agent_pay" onClick="javascript:abcdd(this);" />
                    </div>
                </div>
        
        	</div><!--agentsub-->
        <?php
		}
		?>
        
        <?php
		if($this->users->user_category == 'Owner'){
		?>
        
        	<div id="ownersub">            
        	
            <div><br /><br />
            Upon subscribing to the Unlimited Refresh Plan for merely SG20, you will get the Unlimited Refresh and with no expiry date.</font><br /><br />
            </div>
          
            
            <div style="font-weight:bold; float:left; width:660px;">
            <div style="float:left; width:380px;">You will be redirected to paypal upon clicking on the button.</div>
            <div style="float:left; width:230px;"><img src="<?php echo JURI::root();?>templates/main/images/paypal_img.jpg" /></div>
            </div>
            
            

        <div style="padding-top:10px; padding-bottom:20px; float:left; width:660px;">
        	<div style="float:left;">
            	<input type="submit" value="Continue" class="contactbutton" name="owner_pay" id="owner_pay" onClick="javascript:ownersub(this);" />
            </div>
        </div>
        
        </div><!--ownersub-->
<?php
}
?>
        
        </div> <!--padding-->
        
    </div><!--tabular1-->
    <input type="hidden" name="option" id="option" value="com_ads" />
    <input type="hidden" name="Itemid" id="Itemid" value="126" />
    <input type="hidden" name="view" id="view" value="<?php if($this->users->user_category == 'Owner'){ echo 'paymentprocess'; } else { echo 'confirmation_page'; }?>" />
    <input type="hidden" name="action" id="action" value="<?php if($this->users->user_category == 'Agent'){ echo '2'; }?>" />
    <input type="hidden" name="plan_id" id="plan_id" value="<?php if($this->users->user_category == 'Owner'){ echo '1'; }?>" />
    <?php if($this->users->user_category == 'Agent'): ?>
    <input type="hidden" name="agent_plan_id_exist" value="<?php echo $this->aplanid_exist ?>"  />
    <?php endif; ?>
    <input type="hidden" name="page_id" id="page_id" value="1" />
<script LANGUAGE="JavaScript">
<!--
function ownersub(submit){
var form = document.form2;
form.submit()
}

function agentsub(submit){
var form = document.form2;
form.submit()
}
</script>
</form>
</div>  <!--tablist-->  

</div><!--End-->
</div>
<!--End View-->