<?php
/**
 * @version		$Id: view.html.php 11646 2009-03-01 19:34:56Z ian $
 * @package		Joomla
 * @subpackage	iHouse360 Subscription
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the iHouse360 component
 *
 * @static
 * @package		Joomla
 * @subpackage	Subscription view
 * @since 1.0
 */
class AdsViewSubscription extends JView
{

		function display($tpl = null)
	{
		global $mainframe, $option;
		
		$user =& JFactory::getUser();
		$id = $user->get('id');
		
		$db 	  	=& 	JFactory::getDBO();

		$this->assignRef('users', $this->getInfoOfThisUser($id));
		$this->assignRef('transactions', $transactions);
		
		/* For Quarterly upgrade to Yearly Subs */
		$this->assignRef('aplanid_exist', $this->getAPlanId($id) );
		parent::display($tpl);
	}
	
	function getInfoOfThisUser($id = 0) {
		
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT * FROM #__users "
						.	" WHERE id = '$id'" ;
						
			$db->setQuery( $query );
			$list = $db->loadObject();		
			//echo print_r($list);
		return $list;	
	}
	function getAPlanId($id) {
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
			
		$query		=	" SELECT plan_id FROM #__cbsubs_subscriptions "
						.	" WHERE user_id = '$id' AND status = 'A' AND (plan_id = '2')" ;
						
			$db->setQuery( $query );
			$list = $db->loadResult();	

		return $list;	
	}
	
}
