﻿<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('behavior.tooltip'); ?>

<?php //JHTML::_('script', 'ihouse.js', 'components/com_ads/rokbox/'); ?>
<?php //JHTML::_('script', 'rokbox.js', 'components/com_ads/rokbox/'); ?>
<?php //JHTML::_('script', 'rokbox-config.js', 'components/com_ads/rokbox/themes/light/'); ?>
<?php //JHTML::stylesheet('rokbox-style.css','components/com_ads/rokbox/themes/light/', array('media'=>'all')); ?>

<script type="text/javascript">
function propertyType(value) {
	switch(value) {
		case '组屋' :
			$('hdb_town_select').removeAttribute('disabled');
			$('prop_category_select').setProperty('value', '');
			$('prop_category_select').setProperty('disabled','disabled');
			break;
		case '公寓' :	
			$('prop_category_select').removeAttribute('disabled');
			$('hdb_town_select').setProperty('value', '');
			$('hdb_town_select').setProperty('disabled','disabled');
			break;
		default : 	
			$('hdb_town_select').setProperty('value', '');
			$('hdb_town_select').setProperty('disabled','disabled');
			$('prop_category_select').setProperty('value', '');
			$('prop_category_select').setProperty('disabled','disabled');
		break;	
	}
}

function adType(value) {
	switch(value) {
		case 'rent' :
			$('rental_type_select').removeAttribute('disabled');
			break;
		case 'sell' :	
		default :
			$('rental_type_select').setProperty('value', '');
			$('rental_type_select').setProperty('disabled','disabled');
			break;
	}
}

function loadFloorPlanPicture(name, value) {
		var postcode = '<?php echo $this->item->postcode ?>';								
		var foo = new Date; // Generic JS date object
		var unixtime_ms = foo.getTime();			
		
		var url = 'index.php?option=com_ads&view=ajaxLoadFloorplanPicture&r=' + unixtime_ms;		
		var req = new Ajax(url, {
	   					data		: 	
									{
										'postcode' 	: 	postcode,
										'value'		:	value
									},
						method		: "get",
		    			onSuccess	: function(data) {
							var obj1 		= Json.evaluate(data);
							var fp_img 	= obj1.fp;
							
							$(name + '_img').setHTML(fp_img);
		   				},
						evalScripts : true
				}).request();				 
				
}
</script>

<!--
ad_title
additional_info
property_type
property_category
property_name = dont change, just display
ask_price
size
no_of_room
no_of_hall
no_of_study_room
toilet

property_district = cannot change
postcode = canno change
tenure
topyear
block
floor
street_name = cannot change

floor_level
country
renovation
status

image_uploading ( 8 upload image )

Refresh : 2 credit, at max = 12 credit

-->
<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('behavior.tooltip'); ?>

<style type="text/css">

#tablist {

 padding-bottom:5px;
 width:661px; 
 overflow:hidden; 
 height:auto; 
 min-height:270px;
}

#tablist h2 {
 display:none;
}
#tablist h3 {
 display:none;
}

.tabberlive#tab2 #tablist {
 height:200px;
 overflow:auto;
}

table#refresh_edit_table tr{
	vertical-align:top;
}
</style>

<div style="background-color:#fff">
<div style="margin-top:-20px;"><!--Start view-->
<!--Start-->
<div style=" border-width: 2px;" id="main-search-header-text">
	<div style="padding-left:10px;">
	<span style="font-size: 15px; font-weight: 100;"><i>Edit Ads</i></span>
    </div>
</div>

<form name="edit_refresh_ads_form" method="post" enctype="multipart/form-data" action="<?php echo JRoute::_('index.php?option=com_ads&view=refreshAds') ?>">
                                            <div id="tabcont-ads" style="width:100%;background-color:#fff;">
                                            <div style="padding:0 10px 0 10px;">
                                                   <table id="refresh_edit_table" cellpadding="0" cellspacing="0" border="0" width="600" class="contentpane" >
                                                   		<tr >
                                                        	<td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Property Type *
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                           	<?php echo $this->property_type ?>
                                                            </div>
                                                            </td>
                                                            <td width="40%" height="60">
                                                            
                                                            <label id="property_typemsg" for="property_type">
																HDB Estate *
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                           	<?php echo $this->hdb_town; ?>
                                                            </div>
                                                            
                                                            </td>
                                                   		</tr>
                                                        <tr >
                                                        	<td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Property Category *
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->property_category ?>
                                                            </div>
                                                            </td>
                                                            <td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Property Name *
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->property_name ?>
                                                            </div>
                                                            </td>
                                                   		</tr>
                                                        <tr>
                                                        	<td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Asking Price (SGD)
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->asking_price ?>
                                                            </div>
                                                            </td>
                                                            <td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Psm Price
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->psm_price ?>
                                                            </div>
                                                            </td>
                                                   		</tr>
                                                        <tr>
                                                        	<td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Size *
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->size ?>
                                                            </div>
                                                            </td>
                                                            <td width="40%" height="60">
                                                            
                                                            <div style="float:left;">
                                                            	<label id="property_typemsg" for="property_type">
																Ad Type: 
                                                            	</label>
                                                            	<div style="padding-top:5px;">
                                                            	<?php echo $this->ad_type ?>
                                                            	</div>
                                                            </div>
                                                             <div style="float:left;margin-left:10px;">
                                                            	<label id="property_typemsg" for="property_type">
																Rental Type: 
                                                            	</label>
                                                            	<div style="padding-top:5px;">
                                                            	<?php echo $this->rental_type ?>
                                                            	</div>
                                                            </div>
                                                            <div style="float:left;margin-left:10px;">
                                                            <label id="property_typemsg" for="property_type">
																Floor Level
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->floor_level ?>
                                                            
                                                            </div>
                                                            </div>
                                                            <div style="clear:both"></div>
                                                            </td>
                                                   		</tr>
                                                        <tr>
                                                        	<td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Property District *
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->property_district ?>
                                                            </div>
                                                            </td>
                                                        	
                                                            <td width="40%" height="60">
                                                            <div style="float:left">
                                                            <label id="property_typemsg" for="property_type">
																Number of Hall
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->no_of_hall; ?>
                                                            </div>
                                                            </div>
                                                            <div style="float:left;margin-left:10px;">
                                                            <label id="property_typemsg" for="property_type">
																Number of Room
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->no_of_room ?>
                                                            </div>
                                                            </div>
                                                            <div style="float:left;margin-left:10px;">
                                                            <label id="property_typemsg" for="property_type">
																Study Room
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->study_room ?>
                                                           
                                                            </div>
                                                            </div>
                                                            </td>
                                                   		</tr>
                                                        <tr>
                                                        	<td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Postal Code
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->postcode ?>
                                                            </div>
                                                            </td>
                                                        	
                                                            <td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Toilet
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->toilet ?>
                                                            
                                                            </div>
                                                            </td>
                                                            
                                                   		</tr>
                                                        <tr>
                                                        	
                                                            <td width="40%" height="60">
                                                            <div style="float:left;">
                                                            <label id="property_typemsg" for="property_type">
																Tenure
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->tenure ?>
                                                            </div>
                                                            </div>
                                                            <div style="float:left;margin-left:10px;">
                                                            <label id="property_typemsg" for="property_type">
																TOP Year
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->topyear ?>
                                                            </div>
                                                            </div>
                                                            </td>
                                                            <td width="40%" height="60">
                                                           
                                                            </td>
                                                            
                                                   		</tr>
                                                        <tr>
                                                        	<td width="40%" height="60">
                                                            
                                                            </td>
                                                            <td width="40%" height="60">
                                                            
                                                            </td>
                                                            
                                                   		</tr>
                                                        <tr>
                                                        	<td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Block/House Number
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->block_no ?>
                                                            
                                                            </div>
                                                            </td>
                                                        	
                                                            <td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Floor/Unit Number
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->floor_unit_no ?>
                                                            
                                                            </div>
                                                            </td>
                                                   		</tr>
                                                        <tr>
                                                        	<td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Street Name
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->street_name ?>
                                                            </div>
                                                            </td>
                                                        	
                                                            <td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Country *
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->country ?>
                                                            </div>
                                                            </td>
                                                   		</tr>
                                                        <tr>
                                                        	<td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Ad Title *
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->ad_title ?>
                                                            </div>
                                                            </td>
                                                            <td width="40%" height="60" rowspan="2" >
                                                            <label id="property_typemsg" for="property_type">
																Additional Info
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->add_info ?>
                                                            </div>
                                                            </td>
                                                   		</tr>
                                                        <tr>
                                                        	<td width="40%" height="60">
                                                            <label id="property_typemsg" for="property_type">
																Renovation
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->renovation ?>
                                                            </div>
                                                            </td>
                                                           
                                                   		</tr>
                                                        <tr>
                                                        	<td width="40%" height="60">
                                                             <label id="property_typemsg" for="property_type">
																Status
                                                            </label>
                                                            <div style="padding-top:5px;">
                                                            <?php echo $this->status ?>
                                                            
                                                            </div>
                                                            </td>
                                                            <td width="40%" height="60">
                                                            
                                                            </td>
                                                   		</tr>
                                                   </table>
												</div>
<style type="text/css">

table#refresh_ads_img_table th,
table#refresh_ads_fp_table th{
	background-color:#FF8200;
	color:#FFFFFF;
	padding:10px;
}

</style>
<div>
<table id="refresh_ads_fp_table" width="100%">
<tr>
	<th align="center">Floorplan</th>
    <th align="left" width="70%">Upload Images</th>
</tr>
<tr>
	<td>
    <div id="fp1_img">
    <?php if(!empty($this->fp_img->floorplan1)) : ?>
    	<img width="156px" height="129px" src="<?php echo JRoute::_('images/ihouse/postcode/'.$this->item->postcode.'/floorplan/'.$this->fp_img->floorplan1) ?>" />
    <?php else : ?>
    	<img src="<?php echo JRoute::_('templates/main/images/thumb-noimage.jpg') ?>" />
    <?php endif; ?>    
    </div>
    </td>
    <td><?php echo $this->fp1 ?></td>
</tr>
<tr>
	<td>
    <div id="fp2_img">
   	<?php if(!empty($this->fp_img->floorplan2)) : ?>
    	<img width="156px" height="129px" src="<?php echo JRoute::_('images/ihouse/postcode/'.$this->item->postcode.'/floorplan/'.$this->fp_img->floorplan2) ?>" />
    <?php else : ?>
    	<img src="<?php echo JRoute::_('templates/main/images/thumb-noimage.jpg') ?>" />
    <?php endif; ?>    
    </div>
    </td>
    <td><?php echo $this->fp2 ?></td>
</tr>
</table>
</div>



<div>
<?php
	$ads_id = $this->images->ads_id;
?>


<table id="refresh_ads_img_table" width="100%">
<tr>
	<th align="center">Images</th>
    <th align="left" width="70%">Upload Images</th>
</tr>
<tr>
	<td><?php echo ($this->images->img1) ? '<img width="156px" height="129px" src="'.JRoute::_('images/ihouse/ads/'.$ads_id.'/'.$this->images->img1).'">' : '<img src="'.JRoute::_('templates/main/images/thumb-noimage.jpg').'" />' ?></td>
    <td><input type="file" name="img1" value="" /></td>
</tr>
<tr>
	<td><?php echo ($this->images->img2) ? '<img width="156px" height="129px" src="'.JRoute::_('images/ihouse/ads/'.$ads_id.'/'.$this->images->img2).'">' : '<img src="'.JRoute::_('templates/main/images/thumb-noimage.jpg').'" />' ?></td>
    <td><input type="file" name="img2" value="" /></td>
</tr>
<tr>
	<td><?php echo ($this->images->img3) ? '<img width="156px" height="129px" src="'.JRoute::_('images/ihouse/ads/'.$ads_id.'/'.$this->images->img3).'">' : '<img src="'.JRoute::_('templates/main/images/thumb-noimage.jpg').'" />' ?></td>
    <td><input type="file" name="img3" value="" /></td>
</tr>
<tr>
	<td><?php echo ($this->images->img4) ? '<img width="156px" height="129px" src="'.JRoute::_('images/ihouse/ads/'.$ads_id.'/'.$this->images->img4).'">' : '<img src="'.JRoute::_('templates/main/images/thumb-noimage.jpg').'" />' ?></td>
    <td><input type="file" name="img4" value="" /></td>
</tr>
<tr>
	<td><?php echo ($this->images->img5) ? '<img width="156px" height="129px" src="'.JRoute::_('images/ihouse/ads/'.$ads_id.'/'.$this->images->img5).'">' : '<img src="'.JRoute::_('templates/main/images/thumb-noimage.jpg').'" />' ?></td>
    <td><input type="file" name="img5" value="" /></td>
</tr>
<tr>
	<td><?php echo ($this->images->img6) ? '<img width="156px" height="129px" src="'.JRoute::_('images/ihouse/ads/'.$ads_id.'/'.$this->images->img6).'">' : '<img src="'.JRoute::_('templates/main/images/thumb-noimage.jpg').'" />' ?></td>
    <td><input type="file" name="img6" value="" /></td>
</tr>
<tr>
	<td><?php echo ($this->images->img7) ? '<img width="156px" height="129px" src="'.JRoute::_('images/ihouse/ads/'.$ads_id.'/'.$this->images->img7).'">' : '<img src="'.JRoute::_('templates/main/images/thumb-noimage.jpg').'" />' ?></td>
    <td><input type="file" name="img7" value="" /></td>
</tr>
<tr>
	<td><?php echo ($this->images->img8) ? '<img width="156px" height="129px" src="'.JRoute::_('images/ihouse/ads/'.$ads_id.'/'.$this->images->img8).'">' : '<img src="'.JRoute::_('templates/main/images/thumb-noimage.jpg').'" />' ?></td>
    <td><input type="file" name="img8" value="" /></td>
</tr>
</table>
</div>
                                                    <div id="button_div">
	<button class="button validate" type="submit"><?php echo JText::_('Submit'); ?></button>
	<input type="hidden" name="task" value="" />
    <input type="hidden" id="email" name="email"  value=""/>
	<input type="hidden" name="id" value="<?php echo JRequest::getCmd('id') ?>" />
	<input type="hidden" name="gid" value="0" />
	<?php echo JHTML::_( 'form.token' ); ?>
</div>
           </div>
                                            <!--End-->
</form>
</div> <!--End view-->

</div>
  