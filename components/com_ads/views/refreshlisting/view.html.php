<?php
/**
 * @version		$Id: view.html.php 11646 2009-03-01 19:34:56Z ian $
 * @package		Joomla
 * @subpackage	iHouse360 Rent Ads Listing
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the iHouse360 component
 *
 * @static
 * @package		Joomla
 * @subpackage	Rent Ads Details view
 * @since 1.0
 */
class AdsViewRefreshlisting extends JView
{
	function display($tpl = null)
	{
		global $mainframe;
		
		$id		=	JRequest::getCmd('id');
		
		$row	=	$this->getAdsData($id);
		$this->assignRef('item', $row);
		
		$this->assignRef('images', $this->getAdsImages($id));
		
		$this->assignRef('fp1',$this->getFloorPlanHTML('fp1',$row->postcode));
		$this->assignRef('fp2',$this->getFloorPlanHTML('fp2',$row->postcode));
		
		$this->assignRef('fp_img',$this->getFloorPlanImage($id));
		
		$this->assignRef('property_type',$this->getPropertyTypeListHTML($row->property_type));
		$this->assignRef('hdb_town',$this->getHDBTownListHTML($row));
		$this->assignRef('property_category',$this->getPropertyCategoryListHTML($row));
		$this->assignRef('property_name', $this->getPropertyNameHTML($row->property_name));
		$this->assignRef('asking_price', $this->getAskingPriceHTML($row->ask_price));
		$this->assignRef('psm_price',$this->getPSMPriceHTML($row->psm_price));
		$this->assignRef('size', $this->getSizeHTML($row->size));
		$this->assignRef('no_of_room', $this->getNoOfRoomListHTML($row->no_of_room));
		$this->assignRef('no_of_hall', $this->getNoOfHallListHTML($row->no_of_hall));
		$this->assignRef('postcode',  $this->getPostcodeHTML($row->postcode));
		$this->assignRef('study_room', $this->getNoOfStudyRoomListHTML($row->study_room));
		$this->assignRef('toilet', $this->getNoOfToiletListHTML($row->toilet));
		
		$this->assignRef('rental_type', $this->getRentalTypeListHTML($row));
		
		
		
		$this->assignRef('property_district',$this->getPropertyDistrictHTML($row->property_district_id));
		$this->assignRef('tenure',$this->getTenureListHTML($row->tenure));
		$this->assignRef('topyear',$this->getTopYearListHTML($row->top_year));
		$this->assignRef('floor_level', $this->getFloorLevelListHTML($row->floor_level));
		$this->assignRef('renovation', $this->getRenovationListHTML($row->renovation));
		$this->assignRef('status', $this->getStatusListHTML($row->status));
		
		$this->assignRef('block_no', $this->getBlockNoHTML($row->block_no));
		$this->assignRef('floor_unit_no', $this->getFloorUnitNoHTML($row->floor_unit_no));
		$this->assignRef('street_name', $this->getStreetNameHTML($row->street_name));
		$this->assignRef('country', $this->getCountryHTML($row->country));
		$this->assignRef('ad_title', $this->getAdTitleHTML($row->ad_title));
		$this->assignRef('add_info', $this->getAddInfoHTML($row->add_info));
		
		$this->assignRef('ad_type', $this->getAdTypeLISTHTML($row->ad_type));
		
		
		$this->assignRef('what_type', $this->getWhatTypeHTML($row));
		parent::display($tpl);
	}
	function getPropertyTypeListHTML($selected) {
		
		$db		=& 	JFactory::getDBO();

		$query 	= ' SELECT * FROM #__ihouse_property_type ';
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		$html = '';
		$html .= '<select name="property_type" class="" onchange="javascript:propertyType(this.value);">';
		$html .= '<option value="">----</option> ';
		foreach($rows as $r):
        	$t = '';
			if($selected == $r->name_ch)
				$t = 'selected="selected"';
				
		$html .= '<option value="'.$r->name_ch.'" '.$t.'>'.$r->name_en.'</option>';
		
		endforeach;
       
        $html .= '</select>';
		
		return $html;
	}
	function getHDBTownListHTML($r) {
		$db		=& 	JFactory::getDBO();

		$query 	= ' SELECT * FROM #__ihouse_hdb_town ';
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		$html = '';
		$html .= '<select name="hdb_town" id="hdb_town_select" class="" '.(($r->property_type == '组屋')?'':'disabled="disabled"').'>';
		$html .= '<option value="">----</option> ';
		foreach($rows as $r):
       		$t = '';
			if($r->hdb_town_id == $r->hdb_town_ch)
				$t = 'selected="selected"';
				
		$html .= '<option value="'.$r->hdb_town_ch.'" '.$t.'>'.$r->hdb_town_en.'</option>';
		
		endforeach;
       
        $html .= '</select>';
		
		return $html;
	}
	function getPropertyCategoryListHTML($r) {
		$db		=& 	JFactory::getDBO();

		$query 	= ' SELECT * FROM #__ihouse_ads_property_category ';
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		$html = '';
		$html .= '<select name="property_category" id="prop_category_select" class="" '.(($r->property_type == '公寓')?'':'disabled="disabled"').'>';
		$html .= '<option value="">----</option> ';
		foreach($rows as $rw):
        	$t = '';
			if($r->property_category == $rw->name_ch)
				$t = 'selected="selected"';
				
		$html .= '<option value="'.$rw->name_ch.'" '.$t.'>'.$rw->name_en.'</option>';
		
		endforeach;
       
        $html .= '</select>';
		
        return $html;
	}
	
	
	function getPropertyNameHTML($input = '') {
		
		$html = '<input type="text" name="property_name" id="property_name" size="40" value="'.$input.'" class="" maxlength="50" />';
		
		return $html;
	}
	
	
	function getAskingPriceHTML($input = '') {
		
		$html = '<input type="text" name="asking_price" id="asking_price" size="40" value="'.$input.'" class="" maxlength="50" />';
		
		return $html;
	}
	
	function getPSMPriceHTML($input = '') {
		$html = '<input type="text" name="psm_price" id="psm_price" size="40" value="'.$input.'" class="" maxlength="50" />';
		
		return $html;
	}
	
	function getSizeHTML($input = '') {
		$html = '<input type="text" name="size" id="size" size="40" value="'.$input.'" class="" maxlength="50" />';
		
		return $html;
	}
	
	function getNoOfRoomListHTML($selected) {
		$html = '';
		$html .= '<select name="no_of_room" class="">';
		for($i = 0; $i < 7; $i++) :
			$t = '';
			if($selected == $i)
				$t = 'selected="selected"';
		
			$html .= '<option value="'.$i.'" '.$t.'>'.$i.'</option>';
		endfor;
		$html .= '</select>';
		
		return $html;
	}

	function getNoOfHallListHTML($selected) {
		$html = '';
		$html .= '<select name="no_of_hall" class="">';
		for($i = 0; $i < 4; $i++) :
			$t = '';
			if($selected == $i)
				$t = 'selected="selected"';
		
			$html .= '<option value="'.$i.'" '.$t.'>'.$i.'</option>';
		endfor;
		$html .= '</select>';
		
		return $html;

	}
	function getPostcodeHTML($input = '') {
		$html = '<input type="text" name="postcode" id="postcode" size="40" value="'.$input.'" class="" maxlength="50" readonly="readonly" />';
		
		return $html;
	}
	function getNoOfStudyRoomListHTML($selected) {
		$html = '';
		$html .= '<select name="no_study_room" class="">';
		for($i = 0; $i < 2; $i++) :
			$t = '';
			if($selected == $i)
				$t = 'selected="selected"';
				
			$html .= '<option value="'.$i.'" '.$t.'>'.$i.'</option>';
		endfor;
		$html .= '</select>';
		
		return $html;

	}                 
	
	function getNoOfToiletListHTML() {
		$html = '';
		$html .= '<select name="toilet" class="">';
		
		for($i = 1; $i < 5; $i++) :
			$t = '';
			if($selected == $i)
				$t = 'selected="selected"';
				
			$html .= '<option value="'.$i.'" '.$t.'>'.$i.'</option>';
		endfor;
		$html .= '</select>';
		
		return $html;
		
	}
	
	function getRentalTypeListHTML($r) {
		$html = '';
		$html .= '<select name="rental_type" class="" id="rental_type_select" '.(($r->ad_type == 'rent')?'':'disabled="disabled"').'>';
		$html .= '<option value="">----</option> ';
		$html .= '<option value="整套" '.(($r->rental_type == '整套')?'selected="selected"':'').'>Whole Unit</option>';
		$html .= '<option value="单间" '.(($r->rental_type == '单间')?'selected="selected"':'').'>Room</option>';
		$html .= '<option value="搭房" '.(($r->rental_type == '搭房')?'selected="selected"':'').'>Share Room</option>';
		$html .= '</select>';
		
		return $html;
	}
	function getPropertyDistrictHTML($selected) {
		$db		=& 	JFactory::getDBO();

		$query 	= " SELECT * FROM #__ihouse_district ";
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		$html = '';
		$html .= '<select name="property_district" class="">';
		$html .= '<option value="">----</option> ';
		foreach($rows as $r):
			$t = '';
			if($selected == $r->code)
				$t = 'selected="selected"';
        
		$html .= '<option value="'.$r->code.'" '.$t.'>'.$r->code.' '.$r->district_en.' '.$r->district_ch.'</option>';
		
		endforeach;
       
        $html .= '</select>';
		
		return $html;
	}
	function getTenureListHTML($selected) {
		$html = '';
		$html .= '<select name="tenure" class="">';
		$html .= '<option value="">----</option> ';
		$html .= '<option value="99" '.(($selected == '99')?'selected="selected"':'').'>99</option>';
		$html .= '<option value="999" '.(($selected == '999')?'selected="selected"':'').'>999</option>';
		$html .= '<option value="永久" '.(($selected == '永久')?'selected="selected"':'').'>永久</option>';
		$html .= '</select>';
		
		return $html;
	}
	
	function getTopYearListHTML($selected) {
		$db		=& 	JFactory::getDBO();

		$html = '';
		$html .= '<select name="top_year" class="">';
		$html .= '<option value="0">----</option> ';
		for($i = 1950; $i <= 2020; $i++) {
			$t = '';
			if($selected == $i)
				$t = 'selected="selected"';
				
			$html .= '<option value="'.$i.'" '.$t.'>'.$i.'</option>';
		}
		$html .= '</select>';
		
		return $html;
	}
	
	function getFloorLevelListHTML($selected) {
		$html = '<select name="floor_level" class="">';
		$html .= '<option value="">----</option> ';
        $html .= '<option value="高" '.(($selected == '高')?'selected="selected"':'').'>High</option>';
        $html .= '<option value="中" '.(($selected == '中')?'selected="selected"':'').'>Mid</option>';
        $html .= '<option value="低" '.(($selected == '低')?'selected="selected"':'').'>Low</option>';
        $html .= '</select>';
		
		return $html;
	}
	
	function getRenovationListHTML($selected) {
		
		$html = '<select name="renovation" class="">';
		$html .= '<option value="">----</option> ';
		$html .= '<option value="新装修" '.(($selected == '新装修')? 'selected="selected"':'').'>Newly Renovated</option>';
        $html .= '<option value="普通" '.(($selected == '普通')? 'selected="selected"':'').'>Normal</option>';
        $html .= '<option value="豪华" '.(($selected == '豪华')? 'selected="selected"':'').'>Luxurious</option>';
        $html .= '</select>	';
		
		return $html;
	}
	
	function getStatusListHTML($selected) {
		
		$html = '<select name="status" class="">';
		$html .= '<option value="">----</option> ';
		$html .= '<option value="待售" '.(($selected == '待售')? 'selected="selected"':'').'>Pending</option>';
        $html .= '<option value="已售" '.(($selected == '已售')? 'selected="selected"':'').'>Sold</option>';
        $html .= '</select>	';
		
		return $html;
	}
	
	function getBlockNoHTML($input = '') {
		$html = '<input type="text" name="block_house_no" id="block_house_no" size="40" value="'.$input.'" class="" maxlength="50" />';
		
		return $html;
	}
	
	function getFloorUnitNoHTML($input = '') {
		$html = '<input type="text" name="floor_unit_no" id="floor_unit_no" size="40" value="'.$input.'" class="" maxlength="50" />';
		
		return $html;
	}
	
	function getStreetNameHTML($input = '') {
		$html = '<input type="text" name="street_name" id="street_name" size="40" value="'.$input.'" class="" maxlength="50" />';
		
		return $html;
	}
	function getCountryHTML($input = '') {
		$html = '<input type="text" name="country" id="country" size="40" value="'.$input.'" class="" maxlength="50" />';
		
		return $html;
	}
	function getAdTitleHTML($input = '') {
		$html = '<input type="text" name="ad_title" id="ad_title" size="40" value="'.$input.'" class="" maxlength="50" />';
		
		return $html;
	}
	function getAddInfoHTML($input = '') {
		$html = '<textarea name="add_info" id="add_info" cols="40" rows="5" />'.$input.'</textarea>';
		
		return $html;
	}
	
	function getAdTypeLISTHTML($selected) {
			
		$html = '<select name="ad_type" class="" onchange="javascript:adType(this.value);">';
		$html .= '<option value="">----</option> ';
		$html .= '<option value="sell" '.(($selected == 'sell')? 'selected="selected"':'').'>Sale</option>';
        $html .= '<option value="rent" '.(($selected == 'rent')? 'selected="selected"':'').'>Rent</option>';
        $html .= '</select>	';
		
		return $html;
	}
	
	function getWhatTypeHTML($r) {
		switch($r->what_type) {
			case 0: // pay per basis
				$html = '<span>Note : This is a Pay Per Basis Ads, Refresh the ads for 2 credit(s) and capped at max 12 credit(s) </span>';
				break;
			case 1:
				$html = '<span>Note : This is a Subscription Ads, Ads has unlimited refresh. </span>';
				break;
		}
		
		return $html;
	}
	function getAdsData($id) {
		$db		=& 	JFactory::getDBO();
		
		$query 	= " SELECT * FROM #__ihouse_ads AS a " 
					. " "
					. " WHERE a.id = '$id' ";
			$db->setQuery( $query );
			$row	=	$db->loadObject();
			
		return $row;	
	}
	
	function getAdsImages($id) {
		$db		=& 	JFactory::getDBO();
		
		$query 	= " SELECT * FROM #__ihouse_ads_image AS a " 
					. " "
					. " WHERE a.ads_id = '$id' ";
			$db->setQuery( $query );
			$row	=	$db->loadObject();
			
		return $row;	
	}
	
	function getFloorPlanHTML($name_id ,$postcode) {
		$db		=& 	JFactory::getDBO();
		
		$query	= "SELECT * FROM #__ihouse_floorplan ";
			$db->setQuery( $query );
			$floorplan = $db->loadObjectList();
		
		$html = '';
		$html .= '<select name="'.$name_id.'" id="'.$name_id.'" onchange="javascript:loadFloorPlanPicture(\''.$name_id.'\', this.value);">';
		$html .= '<option value="">---</option>';
		
		foreach($floorplan as $fp) :
			$floorplan_path = 'images/ihouse/postcode/'.$postcode.'/floorplan/'.$fp->type_en;
			
			if(is_dir($floorplan_path)) {
				if ($handle = opendir($floorplan_path)) {
					while (false !== ($file = readdir($handle))) {
		       			if(is_dir($file)) {
						
							continue;
						
						}else {
							$html .= '<option value="'.$fp->type_en.'|'.$file.'">'.$file.'</option>';
						}
		    		}
				}
			}
		endforeach;
		
		$html .= '</select>';
		
		return $html;
	}
	
	function getFloorPlanImage($id) {
		$db		=& 	JFactory::getDBO();
		
		$query	= "SELECT floorplan1, floorplan2 FROM #__ihouse_ads_image WHERE ads_id = '$id' LIMIT 1 ";
			$db->setQuery( $query );
			$row = $db->loadObject();
		
		return $row;
	}
	
	
}
