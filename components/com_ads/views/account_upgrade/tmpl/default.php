<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('behavior.tooltip'); ?>
<?php JHTML::_('script', 'ihouse.js', 'components/com_ads/rokbox/'); ?>
<script type="text/javascript" src="<?php echo JURI::root(); ?>components/com_ads/rokbox/userselect.js?r=<?php echo microtime(true); ?>"></script>
<style type="text/css">
#tablist {
	border-left:3px solid #FF8200;
	border-bottom:3px solid #FF8200;
	border-right:3px solid #FF8200;
	width:99.1%;
	padding:0;
	margin:0;
	min-height:300px;
	height:auto;
	float:left;
}

table#prop_det_info td{
	border:1px solid #999;
}

ul.tab_nav3 {
list-style-image:none;
list-style-type:none;
}
</style>
<!--Start View-->
<div style="margin-top:-20px;">

<div id="main-search-header-text" style="padding-left:15px;border-width:2px;">会员中心&nbsp;<span style="font-size:15px;font-weight:100;"><i><?php echo $this->users->user_category ?>&nbsp;Centre</i></span></div>

<div style="padding:15px;"><!--start-->
<div id="menu_list_profile_detail">
<ul id="" class="tab_nav5">     
	<li id="tab_li5_account" class="tabactive5"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=account_info&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Acct Info</span></a></li>
     <?php
	if(($this->users->user_category == 'Agent')||($this->users->user_category == 'Owner')||($this->users->user_category == 'Individual')){
	?>
	<li id="tab_li5_profile"><a href="<?php echo JURI::root();?>index.php?option=com_user&view=profiler&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Profile</span></a></li>
    <?php
	}
	if(($this->users->user_category == 'Agent')||($this->users->user_category == 'Owner')){
	?>
	<li id="tab_li5_ads"><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=ads_management&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Your Ads</span></a></li>
    <?php
	}
	if($this->users->user_category == 'Agent'){
	?>
    <li id="tab_li5_transaction"><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=transaction&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Transactions</span></a></li>
    <li id="tab_li5_top_up" ><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=top_up&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Top-Up</span></a></li>
    <?php
	}
	?>
</ul>   
</div>
<?php
		if($this->users->user_category == 'Owner'){
		?>
        <script type="text/javascript">
		window.addEvent('domready',function() {	
		$('agentdiv').setStyle('display','block');
		});
		</script>
<?php
}
?>
<div id="tablist">   
<form action="index.php?option=com_ads" method="post" name="form2" id="form2">  
    <div id="tab_account" class="tabular1">
    	<div style="padding:10px;">
            <div id="static-text" style="float:left; width:100%;">
                    	<font style="color:#494949;">Account Upgrade</font>
            </div>
        <?php
		if($this->users->user_category == 'Individual'){
		?>
        <div style="padding-bottom:20px;" id="selection_div">
        Please choose the type of Users you would like to upgrade to.&nbsp;&nbsp;
        <select id="user_category" name="user_category" class="ajax_input" onchange="javascript:select_user(this);return false;">
        <option value="">Please select</option>
        <option value="Owner">Owner</option>
        <option value="Agent">Agent</option>
        </select>
        </div>
       <?php
		}
		?>
        	
            <div id="ownerdiv" style="display:none;">
        		<div>
                Thank you for your support to iHOUSE360.<br /><br />
                When you upgrade your account to owner, you will get the following additional benefits and we will bring more free services to you in the near future. And what's more, <font style="color:#FF0000">this upgrading is totally free!</font>
                </div>
                <div style="padding-bottom:5px;"><u>Benefits</u></div>
                <div>
                (1) Posting one property for rental ads to market with same search function as agent ads, and it is totally free;<br />
                (2) Contact advertised agent by email sent through ihouse360 portal, if you need agent service you;<br />
                (3) Other benefits are the same as individual user.
                </div>
                
                <div style="padding-top:10px; padding-bottom:20px; float:left; width:660px;">
                    <div style="float:left;">
                        <input type="submit" value="Continue" class="contactbutton" name="owner_form" id="owner_form" onClick="javascript:owner(this);" />
                    </div>
                </div>
        
        	</div><!--ownerdiv-->
        
        
        	<div id="agentdiv" style="display:none;">
        	<div>
            Thank you for your support to iHOUSE360.<br /><br />
            When you upgrade your account to agent, you must be the CEA approved real estate agent, you will get the following benefits after upgrading, and we will bring more valuable services to you in the near future. <font style="color:#FF0000">This upgrading has one time charge at S$20 for life period, no yearly maintenance fee!</font><br /><br />
            </div>
            <div style="padding-bottom:5px;"><u>Benefits</u></div>
            <div>
            (1) Instantly receive S$30 equivalent credits (automatically credit into your user account) for advertisement trial with any Listing plans available at iHOUSE360 portal.<br />
            (2) Eligible to subscribe various Listing plans and featured ads at your choice.<br />
            (3) Receive e-newsletter in regular base;<br />
            (4) Keep interested projects in favourites folder at your user account;<br />
            (5) Email notification for relevant events;<br />
            (6) Contact ihouse360 for direct services,<br />
            </div>
            
            <div style="padding-left:20px">
            	<div style="padding-bottom:5px; padding-top:20px; color:#FF0000;">Listing plans</div>
                
                <div style="padding-left:10px;">
                <div style="border:solid 1px #666666; float:left; width:550px; height:auto;">
                	<div style="background-color:#e4dfec; float:left; width:550px; height:20px; line-height:20px;">
                    	<div style="float:left; width:20px; padding-left:10px;">&nbsp;</div>
                        <div style="float:left; width:120px;">Plan</div>
                        <div style="float:left; width:100px;">List Price</div>
                        <div style="float:left; width:270px;">Feature</div>
                    </div>
                    
                    <div style="background-color:#ffffff; float:left; width:550px; height:50px; padding-top:10px; line-height:15px;">
                    	<div style="float:left; width:20px; padding-left:10px;">(a)</div>
                        <div style="float:left; width:120px;">Per-pay base</div>
                        <div style="float:left; width:100px;">S$6/Listing</div>
                        <div style="float:left; width:300px;">All listings in this category will be 90 days live.</div>
                    </div>
                    
                    <div style="background-color:#ccc0da; float:left; width:550px; height:50px; padding-top:10px; line-height:15px;">
                    	<div style="float:left; width:20px; padding-left:10px;">(b)</div>
                        <div style="float:left; width:120px;">Quarterly Subscription Plan</div>
                        <div style="float:left; width:100px;">S$120/Quarter</div>
                        <div style="float:left; width:300px;">Up to 50 concurrent listings and unlimited refreshing</div>
                    </div>
                    
                    <div style="background-color:#ffffff; float:left; width:550px; height:50px; padding-top:10px; line-height:15px;">
                    	<div style="float:left; width:20px; padding-left:10px;">(c)</div>
                        <div style="float:left; width:120px;">Yearly Subscription Plan</div>
                        <div style="float:left; width:100px;">S$388/Year</div>
                        <div style="float:left; width:300px;">Up to 50 concurrent listings and unlimited refreshing</div>
                    </div>
                    
                    <div style="background-color:#ccc0da; float:left; width:550px; height:40px; padding-top:10px; line-height:15px;">
                    	<div align="center" style="float:left; width:540px; padding-left:10px; color:#FF0000;">* Plan (b) and (c): On top of S$30 credits can be used, currently entitle 25% discount over listed price!</div>
                    </div>
                </div>
                </div>
                
                <div style="padding-bottom:5px; padding-top:20px; color:#FF0000; width:550px; float:left;">Featured Ads subscription Plans</div>
                <div style="padding-left:10px;">
                <div style="border:solid 1px #666666; float:left; width:550px; height:auto;">
                	<div style="background-color:#f3f3f3; float:left; width:550px; height:20px; line-height:20px;">
                    	<div style="float:left; width:20px; padding-left:10px;">&nbsp;</div>
                        <div style="float:left; width:140px;">Plan</div>
                        <div style="float:left; width:70px;">Max. No</div>
                        <div style="float:left; width:310px;">Exposure</div>
                    </div>
                    
                    <div style="background-color:#d9e5bd; float:left; width:550px; height:50px; padding-top:10px; line-height:15px;">
                    	<div style="float:left; width:20px; padding-left:10px;">1</div>
                        <div style="float:left; width:140px;">The Guru for this Condo</div>
                        <div style="float:left; width:70px;">1/Condo</div>
                        <div style="float:left; width:310px;">Extremely outstanding place at the subscribed Condo</div>
                    </div>
                    
                    <div style="background-color:#fff; float:left; width:550px; height:50px; padding-top:10px; line-height:15px;">
                    	<div style="float:left; width:20px; padding-left:10px;">2</div>
                        <div style="float:left; width:140px;">Gold Specialist for this Condo</div>
                        <div style="float:left; width:70px;">3/Condo</div>
                        <div style="float:left; width:310px;">Outstanding place at the subscribed Condo</div>
                    </div>
                    
                    <div style="background-color:#d9e5bd; float:left; width:550px; height:50px; padding-top:10px; line-height:15px;">
                    	<div style="float:left; width:20px; padding-left:10px;">3</div>
                        <div style="float:left; width:140px;">Condo Expert</div>
                        <div style="float:left; width:70px;">6</div>
                        <div style="float:left; width:310px;">Always there at Condo Directory page</div>
                    </div>
                    
                    <div style="background-color:#fff; float:left; width:550px; height:50px; padding-top:10px; line-height:15px;">
                    	<div style="float:left; width:20px; padding-left:10px;">4</div>
                        <div style="float:left; width:140px;">Side-bar featured Listing</div>
                        <div style="float:left; width:70px;">30/Rent<br />30/Sale</div>
                        <div style="float:left; width:310px;">Always there at property relevant pages except home page</div>
                    </div>
                    
                    <div style="background-color:#d9e5bd; float:left; width:550px; height:50px; padding-top:10px; line-height:15px;">
                    	<div style="float:left; width:20px; padding-left:10px;">5</div>
                        <div style="float:left; width:140px;">Featured Listings @ Property Detail</div>
                        <div style="float:left; width:70px;">8/Condo</div>
                        <div style="float:left; width:310px;">Always there at Property Details page, applicable to the same Condo per listed</div>
                    </div>
                    
                    <div style="background-color:#fff; float:left; width:550px; height:50px; padding-top:10px; line-height:15px;">
                    	<div style="float:left; width:20px; padding-left:10px;">6</div>
                        <div style="float:left; width:140px;">Featured Listings @ SellAdListing or @ RentAdListing</div>
                        <div style="float:left; width:70px;">8/Rent<br />8/Sale</div>
                        <div style="float:left; width:310px;">Always there at Listings search page outstanding place</div>
                    </div>
                    
                    <div style="background-color:#d9e5bd; float:left; width:550px; height:50px; padding-top:10px; line-height:15px;">
                    	<div style="float:left; width:20px; padding-left:10px;">7</div>
                        <div style="float:left; width:140px;">Featured Listings for New Condo for sale</div>
                        <div style="float:left; width:70px;">50</div>
                        <div style="float:left; width:310px;">Always there at Condo Directory page</div>
                    </div>
                    
                </div>
                </div>
                
            </div>

        <div style="padding-top:10px; padding-bottom:20px; float:left; width:660px;">
        	<div style="float:left;">
            	<input type="submit" value="Continue" class="contactbutton" name="agent_form" id="agent_form" onClick="javascript:agent(this);" />
            </div>
        </div>
        
        </div><!--agent div-->
        
        
        </div> <!--padding-->
        
    </div><!--tabular1-->
    <input type="hidden" name="option" id="option" value="com_ads" />
    <input type="hidden" name="Itemid" id="Itemid" value="126" />
    <input type="hidden" name="view" id="view" value="save_upgrade" />
    <input type="hidden" name="action" id="action" value="" />
<script LANGUAGE="JavaScript">
<!--
function owner(submit){
var form = document.form2;

var a = "1";
document.getElementById('action').value = a;
form.submit()
}

function agent(submit){
var form = document.form2;

var a = "2";
document.getElementById('action').value = a;
form.submit()
}
</script>
</form>
</div>  <!--tablist-->  

</div><!--End-->
</div>
<!--End View-->