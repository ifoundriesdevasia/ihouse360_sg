﻿<?php
/**
 * @version		$Id: view.html.php 11646 2009-03-01 19:34:56Z ian $
 * @package		Joomla
 * @subpackage	iHouse360 Sales Ads Listing
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the iHouse360 component
 *
 * @static
 * @package		Joomla
 * @subpackage	Sales Ads Listing view
 * @since 1.0
 */
class AdsViewAdslisting extends JView
{
	function display($tpl = null)
	{
		global $mainframe;
		$db 	  	=& 	JFactory::getDBO();
		
		$id	= JRequest::getVar('id', '', 'get', 'id');
		
		/*$limitstart =	JRequest::getCmd('limitstart', 0);
		$limit		=	JRequest::getCmd('limit', 10);
		
		$filter_order		= $mainframe->getUserStateFromRequest( "$option.filter_order",		'filter_order',		'a.ad_title',	'cmd' );
		$filter_order_Dir	= $mainframe->getUserStateFromRequest( "$option.filter_order_Dir",	'filter_order_Dir',	'asc',			'word' );
		
		$where = array();
		// ensure filter_order has a valid value.
		if (!in_array($filter_order, array('a.ad_title', 'a.id'))) {
			$filter_order = 'a.ad_title';
		}

		$orderby = ' ORDER BY '. $filter_order .' '. $filter_order_Dir;
		$where = ( count( $where ) ? ' WHERE (' . implode( ') AND (', $where ) . ')' : '' );
		
		$query = 'SELECT COUNT(a.id)'
		.	' FROM #__ihouse_ads AS a '
		.	' LEFT JOIN #__ihouse_ads_image AS i ON i.ads_id = a.id '
		.	' LEFT JOIN #__users AS u ON u.id = a.posted_by '
		.	' LEFT JOIN #__cbsubs_subscriptions AS c ON c.user_id = a.posted_by AND c.id = a.subscription_id '
		.   ' WHERE a.ad_type = "sell" AND a.publish = "1" AND c.status = "A" AND a.status_id = "待售" AND a.property_id = "'.$id.'"';
		;
		$db->setQuery( $query );
		$total = $db->loadResult();
		
		jimport('joomla.html.pagination');
		$pagination = new JPagination( $total, $limitstart, $limit );

		$query	=	'SELECT a.*,i.sess_id, i.name AS img_name, u.name, u.id AS userid, u.chinese_name, u.user_image,u.mobile_contact  '
			.	' FROM #__ihouse_ads AS a '
			.	' LEFT JOIN #__ihouse_ads_image AS i ON i.ads_id = a.id '
			.	' LEFT JOIN #__users AS u ON u.id = a.posted_by '
			.	' LEFT JOIN #__cbsubs_subscriptions AS c ON c.user_id = a.posted_by AND c.id = a.subscription_id '
			.   ' WHERE a.ad_type = "sell" AND a.publish = "1" AND c.status = "A" AND a.status_id = "待售" AND a.property_id = "'.$id.'"'
			. $where
			//. ' GROUP BY a.id'
			. $orderby
		;
		$db->setQuery( $query, $pagination->limitstart, $pagination->limit );
		$rows = $db->loadObjectList();

		// table ordering
		$lists['order_Dir']	= $filter_order_Dir;
		$lists['order']		= $filter_order;*/
		
		//print_r($this->getPriImage($id));
		$this->assignRef('ads', $this->getAdsDetails($id));
		//$this->assignRef('fads', $this->getFeaturedAdsDetails($id));
		$this->assignRef('fads',		$rows);
		$this->assignRef('lists', $lists);
		$this->assignRef('pagination',	$pagination);
		parent::display($tpl);
	}
	
	
	function getAdsDetails($id = 0) {
		
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
		
	
		$query	=	'SELECT a.*, u.name, u.chinese_name, u.user_image,u.mobile_contact  '
							.	' FROM #__ihouse_ads AS a '
							//.	' LEFT JOIN #__ihouse_ads_image AS i ON i.ads_id = a.id '
							//.	' LEFT JOIN #__ihouse_ads_session s ON s.ads_id = a.id '
							//.	' LEFT JOIN #__ihouse_ads_image2 g ON g.ads_id = a.id '
							.	' LEFT JOIN #__users AS u ON u.id = a.posted_by '
							.	' LEFT JOIN #__ihouse_ads_type AS t ON t.user_id = a.posted_by AND t.ads_id = a.id'
							.	' LEFT JOIN #__cbsubs_subscriptions AS c ON c.user_id = a.posted_by AND c.id = t.subscription_id '
							.   ' WHERE t.property_type = "sell" AND a.publish = "1" AND t.ads_type_config = "6" AND c.status = "A" ORDER BY RAND()';
							//.   ' WHERE i.is_primary = "1" AND a.ad_type = "sell" AND t.ads_type_config = "6" AND c.status = "A" AND a.status_id = "待售" AND a.property_id = "'.$id.'"';

						
			$db->setQuery( $query );
			$list = $db->loadObjectList();
		//print_r($list);
		return $list;	
	}
	
	function getFeaturedAdsDetails($id = 0) {
		
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
		
	
		$query	=	'SELECT a.*,i.sess_id, i.name AS img_name, u.name, u.id AS userid, u.chinese_name, u.user_image,u.mobile_contact  '
							.	' FROM #__ihouse_ads AS a '
							.	' LEFT JOIN #__ihouse_ads_image AS i ON i.ads_id = a.id '
							.	' LEFT JOIN #__users AS u ON u.id = a.posted_by '
							.	' LEFT JOIN #__cbsubs_subscriptions AS c ON c.user_id = a.posted_by AND c.id = a.subscription_id '
							.   ' WHERE a.ad_type = "sell" AND a.publish = "1" AND c.status = "A" AND a.status_id = "待售" AND a.property_id = "'.$id.'"';

						
			$db->setQuery( $query );
			$list = $db->loadObjectList();
		//print_r($list);
		return $list;	
	}
}
