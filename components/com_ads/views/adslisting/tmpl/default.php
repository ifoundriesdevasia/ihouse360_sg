<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('behavior.tooltip'); ?>
<?php require_once( JPATH_ROOT.DS.'includes'.DS.'tools.php' ); ?>
<?php JHTML::_('script', 'ihouse.js', 'components/com_ads/rokbox/'); ?>
<?php JHTML::_('script', 'contacts.js', 'components/com_ads/rokbox/'); ?> 
<?php JHTML::_('script', 'rokbox.js', 'components/com_ads/rokbox/'); ?>
<?php JHTML::_('script', 'rokbox-config.js', 'components/com_ads/rokbox/themes/light/'); ?>
<?php JHTML::stylesheet('rokbox-style.css','components/com_ads/rokbox/themes/light/', array('media'=>'all')); ?>
<?php 
	$postcode 	= JRequest::getVar('postcode');
	$id			= JRequest::getVar('id');
?>
<script type="text/javascript">
function getAds(page, value, filter, orderfilter) {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();			
	//alert(<?php echo JRequest::getVar('id', '', 'get', 'id') ?>);
	var myArray = [];
//	var arrTmp = '<?php //echo serialize(JRequest::getVar('district_sector', array())) ?>';
	
	var url = 'index.php?option=com_ads&view=ajaxLoadAdslisting&r=' + unixtime_ms;		
	var req = new Ajax(url, {
	   					data		: 	
									{
										'user_category'	: value,
										'id'	: '<?php echo JRequest::getVar('id', '', 'get', 'id') ?>',
										'filter'			: filter,
										'orderfilter'		: orderfilter,
										'page'			: page
										
									},
						method		: "get",
		    			onSuccess	: function(data) {
							$('ajax-adslisting-' + value).setHTML(data);
		   				},
						evalScripts : true
				}).request();				
}

getAds(0,'agent','a.posting_date','DESC');

</script>
<style type="text/css">

#tablist {

 padding-bottom:5px;
 width:661px; 
 overflow:hidden; 
 height:auto; 
 min-height:270px;
}

#tablist h2 {
 display:none;
}
#tablist h3 {
 display:none;
}

.tabberlive#tab2 #tablist {
 height:200px;
 overflow:auto;
}

#tablist {
	border-left:3px solid #FF8200;
	border-bottom:3px solid #FF8200;
	border-right:3px solid #FF8200;
	width:99.3%;
	padding:0;
	margin:0;
	min-height:300px;
}

table#prop_det_info td{
	border:1px solid #999;
}
</style>


<div><!--Start view-->
<div style="margin-top:-10px;">

<div id="top_menu_list_property_detail">
<ul id="" class="tab_nav6">     
	<li id="tab_">
    	<a href="<?php echo JRoute::_('index.php?option=com_ihouse&layout=property_detail&id='.$id.'&postcode='.$postcode) ?>">
    <span class="chinese_text_menu">项目详细</span></a></li>
	<li id="tab_" class="tabactive6"><a href="<?php echo JRoute::_('index.php?option=com_ads&view=adslisting&id='.$id.'&postcode='.$postcode) ?>"><span class="chinese_text_menu">出售</span></a></li>
	<li id="tab_"><a href="<?php echo JRoute::_('index.php?option=com_ads&view=rentadslisting&id='.$id.'&postcode='.$postcode) ?>"><span class="chinese_text_menu">出租</span></a></li>
</ul>
</div><!--top_menu_list_property_detail-->

<style type="text/css">
td#table-detail-font {
	color:#45599F;font-size:14px;
}
td.table-detail-font-value {
	font-size:13px;
}

.table_space{
	margin:5px;
	font-weight:bold;
}
</style>

<!--Start of tabs-->
<div style="width:690px; float:left; background-color:#FFFFFF; border:solid 1px #e2e9fe;">
<!--Start--><div id="tabcont-ads">
                                                   <div id="ads-header-text" style="padding-left:10px;">出售</div>
                                                   
                                                   <div id="ads-module1">
                                                        <div id="module-headerpos">
                                                                <div id="module-title">
                                                                	<span class="chinese_text1">其他精选房源</span>&nbsp;<i>Recommended Properties</i>
                                                                </div>
                                                        </div>
                                                       
                                                    </div>
                                                    
                                                     <div style="float:left; width:660px; height:auto; padding-top:10px; padding-left:10px; padding-bottom:10px;"> <!--Start of rokbox-->
                                                        
                                                        <div style="position:relative;z-index:1000;">				
        	<img id="sliders_pt_lft1" src="<?php echo JRoute::_('templates/main/images/arrow_left_1.png') ?>" onclick="javascript:movePage1('prev');" style="display:none;" />
        	<img id="sliders_pt_rgt1" src="<?php echo JRoute::_('templates/main/images/arrow_right_1.png') ?>" onclick="javascript:movePage1('next');" />
        	</div>
            
            											<div class="wrapper_info">
                        
                        	<div class="wrapper_info_content" id="propertyHighlightDetail1"> 
                           	<?php for($i= 0; $i < 8; $i++): ?>
                            <?php //if(!empty($this->ads[$i])):?>
                            <div class="list_prop_high88">
                            	<?php if(!empty($this->ads[$i])):?>
                            	<div id="tabimg" style="height:130px;">
                                
                                <?php
								$db 	  	=& 	JFactory::getDBO();
								$query = " SELECT ads_id, sess_id, name FROM #__ihouse_ads_image WHERE ads_id = '".$this->ads[$i]->id."' AND is_primary = 1 ";
									$db->setQuery( $query );
									$tmps = $db->loadObject();
								
								if(empty($tmps)) {
									$query = " SELECT image FROM #__ihouse_ads_image2 WHERE ads_id = '".$this->ads[$i]->id."' ORDER BY RAND() LIMIT 1 ";
									$db->setQuery( $query );
									$ads_image = $db->loadResult();
									
									if(empty($ads_image)) {
										$ads_image = JRoute::_('templates/main/images/thumb-noimage.jpg');
									}else{
										$ads_image = 'image.php?size=140&type=3&path='.$this->ads[$i]->postcode.'/images/'.$ads_image;
									}
								}
								?>
								<div class="recommendedadsimgbg" style="height:120px;">
                                <a href="<?php echo JRoute::_('index.php?option=com_ads&view=adsdetails&id='.$this->ads[$i]->id.'&Itemid=135'); ?>">
								<?php if(!empty($tmps->name)) : ?>
                                
                                <img src="<?php echo JRoute::_('images/ihouse/ads_images/'.$tmps->sess_id.'/med-'.$tmps->name) ?>" width="140px" height="104px" />
                                
                                <?php elseif(!empty($ads_image)) : ?>
                                
                                <img width="140px" style="border:none;" src="<?php echo $ads_image ?>" />
                                
                                <?php else : ?>
                                    <img width="140px" height="104px" src="<?php echo JRoute::_('templates/main/images/thumb-noimage.jpg') ?>" />
                            
                                <?php endif; ?>
                               </a>
							   </div>
                                </div>
                                <div style="clear:both"></div>                
                                <div id="">
                                	<div id="tabtext-padding1"><a href="<?php echo JRoute::_('index.php?option=com_ads&view=adsdetails&id='.$this->ads[$i]->id.'&Itemid=135'); ?>"><?php echo $this->ads[$i]->ad_title;?></a></div>
                                	<div id="tabtext-font">地址：<?php echo $this->ads[$i]->street_name;?><br>户型：<?php echo $this->ads[$i]->no_of_room;?>房<?php echo $this->ads[$i]->no_of_hall;?>厅<br>楼层：<?php echo $this->ads[$i]->floor_level;?>层<br>价格：SGD$ <?php echo formatMoney($this->ads[$i]->ask_price);?></div>
                                </div>
                                 <?php else : ?>
                                <div id="tabimg" style="height:130px;">
                                <div class="recommendedadsimgbg" style="height:120px;">
								<img style="width:140px; height:104px;" src="<?php echo JRoute::_('templates/main/images/thumb-noimage.jpg'); ?>">
								</div>
                                </div>
                                <div style="clear:both"></div>                
                                <div id="">
                                	<div id="tabtext-padding1">No ad available</div>
                                </div>
                                <?php endif; ?>
                            </div>
                            <?php //endif; ?>
                            <?php endfor; ?>
                            </div>
                         </div>
                                                        
                                                        
                                                        
 </div><!--End of rokbox-->
 </div><!--End-->
 
</div>
<!--End of tabs-->


<!--Start-->
<div id="adscontainer1">
<div style="width:690px; float:left; background-color:#FFFFFF; border:solid 1px #e2e9fe;">


<div style="float:left; width:100%;"><!--Start of 2nd module-->
                                                
                  <div style="float:left; width:680px; height:auto; padding-bottom:10px;"> <!--Start of content-->
                    <?php /*?><div style="border-bottom:solid 1px #ccc; padding-bottom:10px; float:left; width:100%; height:auto; padding-left:10px;">
                        <div style="float:left; color:#3a3a3a; width:100px; line-height:20px;">
                        <input type="checkbox" />&nbsp;&nbsp;&nbsp;全选 / 不选
                        </div>
                        
                        <div style="float:left; width:300px;">
                        <input type="submit" value="比较" class="searchbutton" />&nbsp;
                        </div>
                        
                        <div style="float:left; line-height:25px;">
                        排序&nbsp;&nbsp;
                        <img src="<?php echo JRoute::_('templates/main/images/arrow.png'); ?>" />&nbsp;&nbsp;
                        <font style="color:#062284;">
                        <?php echo JHTML::_('grid.sort',   '户型', 'a.psm_price', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
                        &nbsp;&nbsp; | &nbsp;&nbsp;类型&nbsp;&nbsp; | &nbsp;&nbsp;价格 &nbsp;&nbsp; | &nbsp;&nbsp;更新日期
                        </font>
                        </div>
                        
                    </div><?php */?>
                    <div id="ajax-adslisting-agent">
                    
                	</div>
                    
                    <?php //for($i= 0; $i < count($this->fads); $i++): ?>
                    <?php 
					/*if(empty($this->fads[$i]->user_image)) {
					$user_image = JRoute::_('templates/main/images/agentimg.jpg');
					}else{
					$user_image = 'image.php?size=90&type=1&path='.$this->fads[$i]->user_image;
					}*/
				
					?>
                    <?php /*?><div style="border-bottom:solid 1px #ccc; padding-bottom:10px; float:left; width:100%; height:auto; padding-left:10px; padding-top:20px; padding-bottom:20px; color:#5b5b5b;">
                        <div style="float:left; width:30px;">
                        <input type="checkbox" />
                        </div>
                        
                        <div style="float:left; width:170px;">
                        <?php if(!empty($this->fads[$i]->img_name)):?>
                        <img style="width:150px;" src="<?php echo JURI::root();?>/images/ihouse/ads_images/<?php echo $this->fads[$i]->sess_id;?>/<?php echo $this->fads[$i]->img_name;?>" />
                         <?php else : ?>
                         <img style="width:150px;" src="<?php echo JRoute::_('templates/main/images/thumb-noimage.jpg'); ?>" />
                         <?php endif; ?>
                        </div>
                        
                        <div id="tabtext-padding1" style="float:left; width:280px;" id="adsdetails_link">
                            <a href="index.php?option=com_ads&view=adsdetails&id=<?php echo $this->fads[$i]->id;?>&postcode=<?php echo $this->fads[$i]->postcode;?>"><?php echo $this->fads[$i]->property_name;?></a>
                            <div style="float:left; width:100%; line-height:18px;">
                                <div style="float:left; width:60px;">地址</div>
                                <div style="float:left; width:10px;">:</div>
                                <div style="float:left; width:200px;"><?php echo $this->fads[$i]->street_name;?></div>
                            </div>
                            
                            <div style="float:left; width:100%; line-height:18px;">
                                <div style="float:left; width:60px;">价格</div>
                                <div style="float:left; width:10px;">:</div>
                                <div style="float:left; width:200px;">SGD$ <?php echo formatMoney($this->fads[$i]->ask_price);?></div>
                            </div>
                            
                            <div style="float:left; width:100%; line-height:18px;">
                                <div style="float:left; width:60px;">均价</div>
                                <div style="float:left; width:10px;">:</div>
                                <div style="float:left; width:200px;">每平方米 SGD$ <?php echo formatMoney($this->fads[$i]->psm_price);?></div>
                            </div>
                            
                            <div style="float:left; width:100%; line-height:18px;">
                                <div style="float:left; width:60px;">面积</div>
                                <div style="float:left; width:10px;">:</div>
                                <div style="float:left; width:200px;"><?php echo $this->fads[$i]->size;?> 平方米</div>
                            </div>
                            
                            <div style="float:left; width:100%; line-height:18px;">
                                <div style="float:left; width:60px;">户型</div>
                                <div style="float:left; width:10px;">:</div>
                                <div style="float:left; width:200px;"><?php echo $this->fads[$i]->no_of_room;?>房<?php echo $this->fads[$i]->no_of_hall;?>厅</div>
                            </div>
                            
                            <div style="float:left; width:100%; line-height:18px;">
                                <div style="float:left; width:60px;">邮区</div>
                                <div style="float:left; width:10px;">:</div>
                                <div style="float:left; width:200px;"><?php echo $this->fads[$i]->postcode;?></div>
                            </div>
                            
                            <div style="float:left; width:100%; line-height:18px;">
                                <div style="float:left; width:60px;">更新日期</div>
                                <div style="float:left; width:10px;">:</div>
                                <div style="float:left; width:200px;">
                                <?php
								$currentdate = explode( "-" , $this->fads[$i]->posting_date);
								$cyear = $currentdate[0];
								$cmonth = $currentdate[1];
								$cday = substr($currentdate[2], 0, 2);
								?>
                                <?php echo $cyear; ?>年<?php echo $cmonth;?>月<?php echo $cday;?>日
                                </div>
                            </div>
                            
                            <div style="float:left; width:100%; line-height:18px;">
                                <div style="float:left; width:60px;">目前状态</div>
                                <div style="float:left; width:10px;">:</div>
                                <div style="float:left; width:200px;"><?php echo $this->fads[$i]->status_id;?></div>
                            </div>
                        </div>
                        
                        <div style="float:left; width:88px;">
                            <div>
                            	<a href="index.php?option=com_user&view=agent_profile&id=<?php echo $this->fads[$i]->userid;?>&Itemid=136">
                                <img src="<?php echo $user_image; ?>" />
                                </a>
                            </div>
                        </div>
                        
                        <div style="float:left; width:65px; padding-left:10px;">
                        <?php echo $this->fads[$i]->chinese_name;?><br /><?php echo $this->fads[$i]->mobile_contact;?>
                        </div>
                    </div><?php */?>
                <?php //endfor; ?>
                <?php /*?><div style="float:right; padding-top:10px;"><?php echo $this->pagination->getPagesLinks(); ?></div><?php */?>
                </div><!--End of content-->
            </div><!--End of 2nd module-->

</div>
</div>
<!--End-->       
</div>
<!--End-->

</div> <!--End view-->
<!-- POPUP -->
<div id="popupForm" style="display:none;">
</div>