﻿<?php defined('_JEXEC') or die; ?>
<?php JHTML::_('behavior.tooltip'); ?>
<?php JHTML::_('script', 'ihouse.js', 'components/com_ads/rokbox/'); ?>
<?php JHTML::_('script', 'favorite.js', 'components/com_ads/assets/'); ?>
<!--Start View-->
<div style="margin-top:-20px;">

<div id="main-search-header-text" style="padding-left:15px;border-width:2px;">会员中心&nbsp;<span style="font-size:15px;font-weight:100;"><i><?php echo $this->users->user_category ?>&nbsp;Centre</i></span></div>

<style type="text/css">
#tablist {
	border-left:3px solid #FF8200;
	border-bottom:3px solid #FF8200;
	border-right:3px solid #FF8200;
	width:99.1%;
	padding:0;
	margin:0;
	min-height:300px;
	height:auto;
	float:left;
}

#tablist1 {
	width:99.1%;
	padding:0;
	margin:0;
	min-height:300px;
	height:auto;
	float:left;
}

#tablist2 {
	border-left:2px solid #eeeeee;
	border-bottom:2px solid #eeeeee;
	border-right:2px solid #eeeeee;
	width:99.3%;
	padding:0;
	margin:0;
	/*min-height:100px;*/
	height:auto;
	float:left;
}

table#prop_det_info td{
	border:1px solid #999;
}

ul.tab_nav3 {
list-style-image:none;
list-style-type:none;
}
</style>

<div style="padding:15px;"><!--start-->

<div id="menu_list_profile_detail">
<ul id="" class="tab_nav5">   
	<li id="tab_li5_account"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=account_info&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Acct. Info</span></a></li>
     <?php
	if(($this->users->user_category == 'Agent')||($this->users->user_category == 'Owner')||($this->users->user_category == 'Individual')){
	?>
	<li id="tab_li5_profile"><a href="<?php echo JURI::root();?>index.php?option=com_user&view=profiler&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Profile</span></a></li>
    <?php
	}
	if(($this->users->user_category == 'Agent')||($this->users->user_category == 'Owner')){
	?>
	<li id="tab_li5_ads"><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=ads_management&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Your Ads</span></a></li>
    <?php
	}
	if($this->users->user_category == 'Agent'){
	?>
    <li id="tab_li5_transaction"><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=transaction&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Transactions</span></a></li>
    <li id="tab_li5_top_up" ><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=top_up&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Top-up</span></a></li>
    <?php
	}
	?>
    <li id="tab_li77_specialist" class="tabactive5"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=favoriteads&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Favorites</span></a></li>
</ul>   
</div>

<div id="tablist"> 
    <div id="tab_profile" class="tabular1" style="display:none;">
    	<div style="padding:15px;">
            <div>
            	&nbsp;
            </div>
        </div>
    </div><!--tabular1-->
    
    <div id="tab_ads" class="tabular1">
     	<div style="padding: 10px;">
            <div>
            	 

<div id="tablist1"> 
<div id="tab_listing" class="tabular2">
	<div style="padding-top:20px; color:#fa8b11; font-weight:bold; line-height:30px;">
        <div style="background-color:#f0f0f0; border:solid 1px #e0e0e0; float:left; width:632px; height:auto;">
            <div style="float:left; width:40px;">&nbsp;</div>
            <div style="float:left; width:200px;">Ad Title</div>
            <div style="float:left; width:70px;"></div>
        </div>
    </div>
    <?php 
		if(empty($this->adslistings)) :
	?>
    <div style="clear:both"></div>
    <div style="margin:10px;">
    	No Favorited Ads
    </div>    
    <?php
		endif;
	?>
    <?php
						$k = 0;
						for ($i=0, $n=count( $this->adslistings ); $i < $n; $i++)
						{
							$row 	=& $this->adslistings[$i];
	?>
    <div style="border-bottom:solid 1px #e0e0e0; float:left; height:auto; width:632px; line-height:15px; padding-top:5px; padding-bottom:2px;">
    	<div style="float:left; width:30px; padding-left:10px;">
        	&nbsp;
        	<!--<input type="radio" name="ads_id" id="ads_id" value="<?php echo $row->ads_id;?>" />-->
            
        </div>
        <div id="listinglink">
			<a href="<?php echo JRoute::_('index.php?option=com_ads&view=adsdetails&id='.$row->aid); ?>"><?php echo $row->ad_title;?></a><br />
        </div>
        <div style="float:left; width:70px;">
        	<img class="icon-img" src="<?php echo JRoute::_('templates/main/images/icon-fav-remove.png'); ?>" />&nbsp;
        	<a class="no_decoration" href="javascript:removeFavorite('<?php echo $row->fav_id; ?>','back','<?php echo JRoute::_('templates/main/images/icon-fav.png') ?>','<?php echo JRoute::_('templates/main/images/icon-fav-remove.png') ?>');">
        	<strong>不喜欢</strong>
            </a>
        </div>
    </div>
    <?php
		}
	?>
</div>

<div id="tablist1"> 
<div id="tab_listing" class="tabular2">
	<div style="padding-top:20px; color:#fa8b11; font-weight:bold; line-height:30px;">
        <div style="background-color:#f0f0f0; border:solid 1px #e0e0e0; float:left; width:632px; height:auto;">
            <div style="float:left; width:40px;">&nbsp;</div>
            <div style="float:left; width:200px;">Property</div>
            <div style="float:left; width:70px;"></div>
        </div>
    </div>
    <?php 
		if(empty($this->propertylistings)) :
	?>
    <div style="clear:both"></div>
    <div style="margin:10px;">
    	No Favorited Property
    </div>    
    <?php
		endif;
	?>

    <?php
						$k = 0;
						for ($i=0, $n=count( $this->propertylistings ); $i < $n; $i++)
						{
							$row 	=& $this->propertylistings[$i];
							
	?>
    <div style="border-bottom:solid 1px #e0e0e0; float:left; height:auto; width:632px; line-height:15px; padding-top:5px; padding-bottom:2px;">
    	<div style="float:left; width:30px; padding-left:10px;">
        	&nbsp;
        	<!--<input type="radio" name="ads_id" id="ads_id" value="<?php echo $row->ads_id;?>" />-->
            
        </div>
        <div id="listinglink">
			<a href="<?php echo JRoute::_('index.php?option=com_ihouse&view=ihouse&layout=property_detail&postcode='.$row->postcode.'&id='.$row->aid); ?>"><?php echo $row->name_en;?></a><br />
        </div>
        <div style="float:left; width:70px;">
        	<img class="icon-img" src="<?php echo JRoute::_('templates/main/images/icon-fav-remove.png'); ?>" />&nbsp;
        	<a class="no_decoration" href="javascript:removeFavorite('<?php echo $row->fav_id; ?>','back','<?php echo JRoute::_('templates/main/images/icon-fav.png') ?>','<?php echo JRoute::_('templates/main/images/icon-fav-remove.png') ?>');">
        	<strong>不喜欢</strong>
            </a>
        </div>
    </div>
    <?php
		}
	?>
</div>

<div id="tab_guru" class="tabular2" style="display:none;">
&nbsp;
</div><!--tab_guru-->

<div id="tab_specialist" class="tabular2" style="display:none;">rr
</div>

                 
</div>   <!--tablist-->              
            </div>
        </div>
    </div><!--tabular1-->
    
  


</div>    




</div><!--End-->



</div>
<!--End View-->
