<?php
/**
 * @version		$Id: view.html.php 11646 2009-03-01 19:34:56Z ian $
 * @package		Joomla
 * @subpackage	iHouse360 Ads Action
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the iHouse360 component
 *
 * @static
 * @package		Joomla
 * @subpackage	Ads Action view
 * @since 1.0
 */
class AdsViewOwner_post extends JView
{
	function display($tpl = null)
	{
		global $mainframe;
		
		$user =& JFactory::getUser();
		$user_id = $user->get('id');
		$db = & JFactory::getDBO();
	
		$query = "SELECT user_category FROM #__users WHERE id = ".$user_id;
	
		$db->setQuery($query);
		$user_category = $db->loadResult();
		
		$query = "SELECT count(*) FROM #__ihouse_ads a, #__ihouse_ads_type t WHERE a.id = t.ads_id AND t.ads_type_config = '8' AND a.posted_by = ".$user_id;
	
		$db->setQuery($query);
		$count_ads = $db->loadResult();
		
		$query = "SELECT count(*) FROM #__cbsubs_subscriptions WHERE plan_id = '1' AND user_id = ".$user_id;
	
		$db->setQuery($query);
		$owner_sub = $db->loadResult();
		
		$query = "SELECT credit FROM #__ihouse_users_credit WHERE user_id = ".$user_id;
	
		$db->setQuery($query);
		$credit = $db->loadResult();
		
		$query = "SELECT count(*) FROM #__cbsubs_subscriptions WHERE plan_id = '2' AND user_id = ".$user_id;
	
		$db->setQuery($query);
		$quarter_sub = $db->loadResult();
		
		$query = "SELECT count(*) FROM #__cbsubs_subscriptions WHERE plan_id = '3' AND user_id = ".$user_id;
	
		$db->setQuery($query);
		$yearly_sub = $db->loadResult();

		$this->assignRef('user_category',  $user_category);
		$this->assignRef('count_ads',  $count_ads);
		$this->assignRef('owner_sub',  $owner_sub);
		$this->assignRef('credit',  $credit);
		$this->assignRef('quarter_sub',  $quarter_sub);
		$this->assignRef('yearly_sub',  $yearly_sub);

		parent::display($tpl);
	}
}
