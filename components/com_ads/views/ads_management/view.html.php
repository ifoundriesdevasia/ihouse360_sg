<?php
/**
 * @version		$Id: view.html.php 11646 2009-03-01 19:34:56Z ian $
 * @package		Joomla
 * @subpackage	iHouse360 Ads Management
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the iHouse360 component
 *
 * @static
 * @package		Joomla
 * @subpackage	 Ads Management view
 * @since 1.0
 */
class AdsViewAds_management extends JView
{
	function display($tpl = null)
	{
		global $mainframe;
		$db = & JFactory::getDBO();
		
		//$id = JRequest::getVar('id', '', 'get', 'id');
		$user 	=& JFactory::getUser();
		$id = $user->get('id');
		
		$query = "SELECT code FROM #__ihouse_district";
		$db->setQuery($query);
		$codes = $db->loadResultArray();
		
		$codeOptions[] = JHTML::_('select.option', '', JText::_('Please Select'));
		foreach ($codes as $code) {
		$codeOptions[] = JHTML::_('select.option', $code,$code);
		}
		$codeList = JHTML::_('select.genericlist',  $codeOptions, 'district', 'class="district" size="1"', 'value', 'text', '' );
		$scodeList = JHTML::_('select.genericlist',  $codeOptions, 'sdistrict', 'class="sdistrict" size="1"', 'value', 'text', '' );
		

		$this->assignRef('codeList', $codeList );
		$this->assignRef('scodeList', $scodeList );
		$this->assignRef('users', $this->getInfoOfThisUser($id));
		$this->assignRef('credits', $this->UserCredits($id));
		$this->assignRef('guru', $this->GuruCredits());
		$this->assignRef('specialist', $this->SpecialistCredits());
		$this->assignRef('condospecialist', $this->CondoSpecialistCredits());
		$this->assignRef('csslot', $this->getCSSlot());
		$this->assignRef('cscredit', $this->getCSCredit());
		$this->assignRef('checksubscribe', $this->checkSubscribe($id));
		$this->assignRef('ownerrefresh', $this->ownerrefresh($id));
		$this->assignRef('adslistings', $this->getAdsListing($id));
		parent::display($tpl);
	}
	
	function getAdsListing($id = 0) {
		if(!$id)
			return false;
		
		$db 	  	=& 	JFactory::getDBO();
		
		$query= "SELECT a.*,cb.plan_id FROM #__ihouse_ads AS a "
				.	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = a.subscription_id "
				.	" WHERE a.posted_by = '".$id."' "
				.	" ORDER BY a.creation_date DESC ";
				
		$db->setQuery($query);
		$list = $db->loadObjectList();
		
		return $list;	
	}
	
	function checkSubscribe($id = 0) {
		if(!$id)
			return false;
		
		$db 	  	=& 	JFactory::getDBO();
		
		$query="SELECT count(*) FROM #__ihouse_ads_type WHERE ads_type_config = '3' AND user_id = '".$id."'";
		$db->setQuery($query);
		$subscribe = $db->loadResult();
		
		return $subscribe;	
	}
	
	function ownerrefresh($id = 0) {
		if(!$id)
			return false;
		
		$db 	  	=& 	JFactory::getDBO();
		
		$query="SELECT count(*) FROM #__cbsubs_subscriptions WHERE plan_id = '1' AND status = 'A' AND user_id = '".$id."'";
		$db->setQuery($query);
		$subscribe = $db->loadResult();
		
		return $subscribe;	
	}
	

	function getCSCredit() {
		
		$db 	  	=& 	JFactory::getDBO();
		
		$query="SELECT rate FROM #__cbsubs_plans WHERE id = '7'";
		$db->setQuery($query);
		$rate = $db->loadResult();
		
		return $rate;	
	}

	function getCSSlot() {
		
		$db 	  	=& 	JFactory::getDBO();
		
		$query="SELECT space FROM #__ihouse_ads_type_slot WHERE ads_type_config = '3'";
		$db->setQuery($query);
		$space = $db->loadResult();
		
		return $space;	
	}
	
	function getInfoOfThisUser($id = 0) {
		
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT * FROM #__users "
						.	" WHERE id = '$id'" ;
						
			$db->setQuery( $query );
			$list = $db->loadObject();		

		return $list;	
	}
	
	function UserCredits($id = 0) {
		
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT credit FROM #__ihouse_users_credit "
						.	" WHERE user_id = '$id'" ;
						
			$db->setQuery( $query );
			$credit = $db->loadResult();		

		return $credit;	
	}
	
	function GuruCredits() {

		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT rate FROM #__cbsubs_plans "
						.	" WHERE id = '5'" ;
						
			$db->setQuery( $query );
			$guru = $db->loadResult();		

		return $guru;	
	}
	
	function SpecialistCredits() {

		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT rate FROM #__cbsubs_plans "
						.	" WHERE id = '6'" ;
						
			$db->setQuery( $query );
			$specialist = $db->loadResult();		

		return $specialist;	
	}
	
	function CondoSpecialistCredits() {
	
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT rate FROM #__cbsubs_plans "
						.	" WHERE id = '7'" ;
						
			$db->setQuery( $query );
			$cspecialist = $db->loadResult();		
			
		return $cspecialist;	
	}
}
