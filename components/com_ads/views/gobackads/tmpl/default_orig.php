﻿<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('behavior.tooltip'); ?>

<?php JHTML::_('script', 'ajaxupload.js', 'components/com_ads/assets/'); ?>
<?php JHTML::_('script', 'postads.js', 'components/com_ads/assets/'); ?>
<?php JHTML::_('script', 'validation_postads.js', 'components/com_ads/assets/'); ?>

<script type="text/javascript">

function adType(value) {
	
	if(value == 'rent') {
		$('rental_type').setProperty('disabled','');
		$('status_text').setProperty('value','待租');
	} else if(value == 'sell'){
		$('rental_type').setProperty('disabled','disabled');
		$('rental_type').setProperty('value','');
		$('status_text').setProperty('value','待售');
	} else {
		$('rental_type').setProperty('disabled','disabled');
		$('rental_type').setProperty('value','');
		$('status_text').setProperty('value','');
	}
}

function ajaxUpload(form) {
	$('message_tmp').setHTML('');
	var str = $('imgFile').getProperty('value');
	/* CHECK FILENAME */
	//if(/^[^\!\@\#\$\%\^\*]+(\.[^\!\@\#\$\%\^\*]+)+$/.test(str)) {
		return AIM.submit(form, 
					  {
						'onStart'		:   runThis, 
					  	'onComplete' : loadThis
		});
	//} else {
		//$('message_tmp').setHTML('<span style="color:red;">"!@#$%^*" is restricted. Please change your filename</span>');
	//}
	
}

function runThis() {
	$('pic_uploading_loader').setStyle('display','block');
}

function loadThis() {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();		
	
	$('pic_uploading_loader').setStyle('display','block');
	
	var url = 'index.php?option=com_ads&view=ajaxLoadAdsTempImage&r=' + unixtime_ms;	
	
	var req = new Ajax(url, {
	   					data		: 	
									{
										'session_id' : '<?php echo $this->session_id ?>'
									},
						method		: "get",
		    			onSuccess	: function(data) {
						
							var obj1 		= Json.evaluate(data);
							var tmp 		= obj1.html;
							
							$('loadAdsImagesPlace').setHTML(tmp);
							
							$('pic_uploading_loader').setStyle('display','none');
		   				},
						async: true,
						evalScripts : true
				}).request();
}


function watermarkOption(value) {
	$('watermark_rad').setProperty('value', value);
}

function watermarkText(value) {
	$('watermark_textinput').setProperty('value', value);
}

</script>
<?php
	$user 			=& JFactory::getUser();
	$user_category 	= $user->get('user_category');
?>
<div style="float:left;background-color:#fff;width:100%;">
	<div style="margin:10px auto; width:97%;">
	<!--Start-->
		<div style="border-width:2px;" id="main-search-header-text">
			<span style="font-size: 15px; font-weight: 100;"><i>Posting Ads</i></span>
		</div>
		
        <div id="tabcont-ads" style="background-color:#fff;width:100%;">
			<form name="edit_refresh_ads_form" id="edit_refresh_ads_form" method="post" enctype="multipart/form-data" action="<?php echo JRoute::_('index.php?option=com_ads&view=posting_ads') ?>">
        	<div class="postAdsPadding">
				<label id="property_typemsg" for="property_type">
				Property Type
			   	</label>
				<div style="padding-top:5px;">
                	<div style="float:left">
					<?php echo $this->property_type ?>
                    </div>
                    <div id="prop_type_loader">
                    	<img src="<?php echo JRoute::_('templates/main/images/ajax-loader.gif'); ?>"  />
                    </div>
                    <div style="clear:both"></div>
				</div>
		 		<div id="property_type_table" style="margin:5px 0 5px 0;">
                <?php 	
					if($this->property_type_ch == '组屋' ) {
						require('hdb.php');
					} else if($this->property_type_ch == '公寓'){
						require('condo.php');
					} else if($this->property_type_ch == '服务公寓'){
						require('apartments.php');
					} else if($this->property_type_ch == '商业'){
						require('commercials.php');
					} else if($this->property_type_ch == '有地住宅'){
						require('landed.php');
					}
				?>	
				</div>
			</div>
            <span id="property_id"><input type="hidden" name="property_id" value="<?php echo $this->property_id ?>" /></span>
           
            <div id="floorplan_box_postads" <?php echo ($this->property_type_ch == '组屋' || $this->property_type_ch == '服务公寓' || $this->property_type_ch == '商业' || $this->property_type_ch == '有地住宅' )?'style="display:none;"':'' ?>>
			<div style="background-color:#ff8200;color:#fff;height:37px;">
				<div style="float:left;padding:10px;"><strong>Floor Plan</strong></div>
				<div style="clear:both"></div>
			</div>
            <div id="whole_fp_box">    
				<div style="padding:10px;">
					Choose the floorplan you want to add in. You can select up to 2 floorplan
				</div>
			
            	<div>
					<div style="float:left;width:49.5%;background-color:#ccc;text-align:center;">
						<div id="fp1_select"><?php echo $this->fp1 ?></div>
				    	<div id="fp1_img"><?php echo $this->fp1_image ?></div>
			    	</div>
			    	<div style="float:right;width:49.5%;background-color:#ccc;text-align:center;">
				    	<div id="fp2_select"><?php echo $this->fp2 ?></div>
				    	<div id="fp2_img"><?php echo $this->fp2_image ?></div>
			    	</div>
        			<div style="clear:both"></div>
				</div>
			 </div>  
                
            
			<div id="test"></div>

			<div style="background-color:#ff8200;color:#fff;height:37px;">
				<div style="float:left;padding:10px;"><strong>Image from iHouse</strong></div>
				<div style="clear:both;"></div>
			</div>

			<div style="margin:10px;">
            	
				<div id="ihouseImage">
					<?php echo ($this->image2)?$this->image2:'No Pictures for this property name' ?>
				</div>
                <div id="ihouseImage_loader" style="float:left;display:none;">
                    	<img src="<?php echo JRoute::_('templates/main/images/ajax-loader.gif'); ?>"  />
                </div>
				<div style="clear:both"></div>
           	</div>
            </div>   
			<input type="hidden" name="task" value="" />
			<input type="hidden" name="option" value="com_ads" />
			<input type="hidden" name="session_id" value="<?php echo $this->session_id ?>" />
			<input type="hidden" name="agree1" id="t-agree1" value="0" />
			<?php if($user_category == 'Agent') : ?>
			<input type="hidden" name="agree2" id="t-agree2" value="0" />
            <?php endif; ?>
			<?php echo JHTML::_( 'form.token' ); ?>
        	</form>      
            
            <div id="upload_box_postads" <?php echo ($this->property_type_ch == '组屋' || $this->property_type_ch == '公寓' || $this->property_type_ch == '服务公寓' || $this->property_type_ch == '商业' || $this->property_type_ch == '有地住宅')?'':'style="display:none;"' ?>>
        	<div style="background-color:#ff8200;color:#fff;height:37px;">
				<div style="float:left;padding:10px;"><strong>Current Photos</strong></div>
				<div style="float:right;margin-right:10px;">
					<div style="overflow:hidden;;text-align:center;position:relative;padding:7px 0;">
                        <form name="loadImageForm" enctype="multipart/form-data" method="post" id="loadImageForm" action="<?php echo JRoute::_('index.php?option=com_ads&view=ajaxSave') ?>">
					    	<img src="<?php echo JRoute::_('templates/main/images/btn-upload.png') ?>" />
				    		<input class="imgFileInput" id="imgFile" name="imgFile" type="file" onchange="ajaxUpload(this.form);" />
                            <input type="hidden" name="session_id" value="<?php echo $this->session_id ?>" />
                            <input type="hidden" name="watermark_use" id="watermark_rad" value="1"  />
                            <input type="hidden" name="wmrk_txt" id="watermark_textinput" value="<?php echo $this->watermark_text ?>" />
						</form>       
				   	</div>
				</div>
				<div style="clear:both"></div>
			</div>
			
             <div style="margin:5px 10px">
            	<div style="float:left;"> 
					Watermark <input type="radio" name="watermark_radio" onclick="javascript:watermarkOption(this.value);" value="1" checked="yes" />iHouse Logo
                    <input type="radio" name="watermark_radio" onclick="javascript:watermarkOption(this.value);" value="0" />
                    <input type="text" onkeyup="javascript:watermarkText(this.value);" value="<?php echo $this->watermark_text ?>" maxlength="16" />	
                </div>
            	<div style="clear:both"></div>
            </div>
            
            <div style="padding:10px;">
            	<div style="float:left;"> 
					Simply click 'Upload' to choose the photos you want to upload. You can upload up to 8 photos.		
                </div>
                <div id="pic_uploading_loader">
                	<div style="float:left;">
                    <img src="<?php echo JRoute::_('templates/main/images/ajax-loader.gif'); ?>"  />
                    </div> 
                    <div style="float:left;margin-left:5px;">
                    Wait! Loading
                    </div>
                    <div style="clear:both"></div>
                </div>
                <div style="clear:both"></div>
				<div id="message_tmp">
				</div>
				<div id="loadAdsImagesPlace">
                	<?php echo $this->image1 ?>
				</div>
            </div>
            <div style="clear:both;"></div>
         </div>
         <div style="border:2px solid #a50c2a;"> 
        	<div style="color:#fff;background-color:#a50c2a;">
            	<div style="padding:2px;">
            		<strong>Important</strong>
                </div>
            </div>  
				
				<input type="checkbox" name="agree1" value="1" onclick="javascript:agree('agree1');" /><span id="err-agree1">I confirm the listing details and photos describe the property accurately</span><br />
				<?php if($user_category == 'Agent') : ?>
                <input type="checkbox" name="agree2" value="1" onclick="javascript:agree('agree2');" />
                	<span id="err-agree2">I confirm I have obtained agreement from the client and/or the client's representing agent to advertise the property</span>
                <?php endif; ?>    
			</div>
			<div id="button_div" style="text-align:center;margin:3px 0;">
        		<button class="button validate" type="submit" onclick="javascript:validateForm('submit','<?php echo JRoute::_('index.php?option=com_ads&view=posting_ads'); ?>');return false;">
					<?php echo JText::_('Submit'); ?>
        		</button>
                <button class="button validate" type="submit" onclick="javascript:validateForm('preview','<?php echo JRoute::_('index.php?option=com_ads&view=preads'); ?>');return false;">
					<?php echo JText::_('Preview'); ?>
        		</button>
			</div>  
		</div><!--End-->
    
    </div>
</div>                                          