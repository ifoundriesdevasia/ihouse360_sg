﻿<?php
/**
 * @version		$Id: view.html.php 11646 2009-03-01 19:34:56Z ian $
 * @package		Joomla
 * @subpackage	iHouse360 Sales posting form
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the iHouse360 component
 *
 * @static
 * @package		Joomla
 * @subpackage	Sales posting form view
 * @since 1.0
 */
class AdsViewGobackads extends JView
{
	function display($tpl = null)
	{
		global $mainframe;
		
		$layout = JRequest::getCmd('layout');
		
		$session_id			= JRequest::getVar('session_id','');

		$property_id		= JRequest::getVar('property_id');

		$property_type 		= JRequest::getVar('property_type');
		$property_type_ch	= $property_type;
		
		switch($property_type_ch) {
			case '公寓': 
				$property_name 		= JRequest::getVar('property_name','');
				$property_type_en = 'condo'; break;
			case '组屋': 
				$property_type_en = 'hdb'; break;
			case '商业': 
				$project_name 		= JRequest::getVar('project_name','');
				$property_type_en = 'commercials'; break;
			case '服务公寓': 
				$project_name 		= JRequest::getVar('project_name','');
				$property_type_en = 'apartments'; break;
			case '有地住宅': 
				$project_name 		= JRequest::getVar('project_name','');
				$property_type_en = 'landed'; break;	
		}
		
		$property_category 	= JRequest::getVar('property_category');
		
		$asking_price 		= JRequest::getVar('asking_price','');
		$psm_price 			= JRequest::getVar('psm_price','');
		$psf_price 			= JRequest::getVar('psf_price','');
		$size 				= JRequest::getVar('size','');
		$ad_type 			= JRequest::getVar('ad_type','');
		$rental_type 		= JRequest::getVar('rental_type','');
		$floor_level 		= JRequest::getVar('floor_level','');
		
		$flattype			= JRequest::getVar('flattype','');
		
		$district 			= JRequest::getVar('district','');
		$postcode 			= JRequest::getVar('postcode','');
		$tenure 			= JRequest::getVar('tenure','');
		$year_built 		= JRequest::getVar('year_built','');
		
		$no_of_hall 		= JRequest::getVar('no_of_hall','');
		$no_of_room 		= JRequest::getVar('no_of_room','');
		$no_study_room 		= JRequest::getVar('no_study_room','');
		$toilet 			= JRequest::getVar('toilet','');
		
		$block_house_no 	= JRequest::getVar('block_house_no','');
		$floor_unit_no 		= JRequest::getVar('floor_unit_no','');
		$street_name_1 		= JRequest::getVar('street_name','');
		$country 			= JRequest::getVar('country','');
		$ad_title 			= JRequest::getVar('ad_title','');
		$add_info 			= JRequest::getVar('add_info','');
		$renovation 		= JRequest::getVar('renovation','');
		$status				= JRequest::getVar('status','');
		
		$fp1				= JRequest::getVar('fp1','');
		$fp2				= JRequest::getVar('fp2','');
		
		$ihouse_photos_banks = JRequest::getVar('ihousephotos',array());
		
		$this->assignRef('property_type_ch', $property_type_ch);
		$this->assignRef('ihousephotos', $ihouse_photos_banks);
		
		
		
				
		$model =& JModel::getInstance('property','AdsModel');
		/* HDB */
		$hdb_town	= JRequest::getVar('hdb_town','');
		
		$hdb_town_list 	= $model->getHDBTownListHTML($hdb_town);
		
		/* Condo */
		$property_type		=	$this->getPropertyTypeListHTML($session_id, $property_type_ch);
		$property_category	=	$model->getPropertyCategoryListHTML($property_category, $property_type_en);
		$property_name		=	$model->getPropertyNameHTML($session_id, $property_id);
		
		/* Coomercials | Landed */
		$project_name		=	$model->getProjectNameHTML($project_name,$property_type_en);
		
		$district_id	=	$model->getPropertyDistrictHTML($district, $property_type_en);
		$tenure			=	$model->getTenureListHTML($tenure, $property_type_en);
		$topyear		=	$model->getTopYearListHTML($year_built, $property_type_en);
		$postcode		=	$model->getPostcodeHTML($postcode, $property_type_en);
		
		$flattype		=	$model->getFlatTypeListHTML($flattype);
		$asking_price 	= 	$model->getAskingPriceHTML($asking_price);
		$psm_price_html	=	$model->getPSMPriceHTML($psm_price);
		$psf_price_html	=	$model->getPSFPriceHTML($psf_price);
		$size			=	$model->getSizeHTML($size);
		
		$no_of_room		=	$model->getNoOfRoomListHTML($no_of_room);
		$no_of_hall		=	$model->getNoOfHallListHTML($no_of_hall);
		$study_room		=	$model->getNoOfStudyRoomListHTML($no_study_room);
		$toilet			=	$model->getNoOfToiletListHTML($toilet);
		$floor_level	=	$model->getFloorLevelListHTML($floor_level);
		$renovation		=	$model->getRenovationListHTML($renovation);
		//$status			=	$model->getStatusListHTML($status);
		
		$block_no		=	$model->getBlockNoHTML($block_house_no);
		$floor_unit_no	=	$model->getFloorUnitNoHTML($floor_unit_no);
		
		$street_name	=	$model->getStreetNameHTML($street_name_1, $property_type_en);
		$country		=	$model->getCountryHTML($country);
		$ad_title		=	$model->getAdTitleHTML($ad_title);
		$add_info		=	$model->getAddInfoHTML($add_info);
		
		$ad_type		=	$model->getAdTypeLISTHTML($ad_type);
		$rental_type	=	$model->getRentalTypeListHTML($rental_type);
		
		$image1			=	$this->getUserUploadImage($session_id);
		$image2			=	$this->getImagePhotosHTML();
		
		/* HDB */
		$this->assignRef('hdb_town', $hdb_town_list);
		
		/* Condo */
		
		$this->assignRef('property_id', $property_id);
		$this->assignRef('property_type', $property_type);
		$this->assignRef('property_category', $property_category);
		$this->assignRef('property_name', $property_name);
		$this->assignRef('project_name', $project_name);
		
		$this->assignRef('flattype', $flattype);
		$this->assignRef('asking_price', $asking_price);
		$this->assignRef('psm_price', $psm_price_html);
		$this->assignRef('psf_price', $psf_price_html);
		$this->assignRef('size', $size);
		
		$this->assignRef('block_no', $block_no);
		$this->assignRef('floor_unit_no', $floor_unit_no);
		$this->assignRef('street_name', $street_name);
		$this->assignRef('country', $country);
		$this->assignRef('ad_title', $ad_title);
		$this->assignRef('add_info', $add_info);
		
		$this->assignRef('ad_type', $ad_type);
		$this->assignRef('rental_type', $rental_type);
		$this->assignRef('district', $district_id);
		$this->assignRef('tenure', $tenure);
		$this->assignRef('topyear', $topyear);
		$this->assignRef('postcode', $postcode);
		
		$this->assignRef('floor_level', $floor_level);
		$this->assignRef('no_of_room', $no_of_room);
		$this->assignRef('no_of_hall', $no_of_hall);
		$this->assignRef('study_room',$study_room );
		$this->assignRef('toilet', $toilet );
		$this->assignRef('renovation', $renovation);
		$this->assignRef('status', $status);
	
		$this->assignRef('session_id', $session_id);
		
		$this->assignRef('fp1', $this->getFloorPlanList($fp1, 'fp1'));
		
		if(!empty($fp1))
			$this->assignRef('fp2', $this->getFloorPlanList($fp2, 'fp2'));
		else {
			$fp2_html = '<select name="fp2"></select>';
			$this->assignRef('fp2', $fp2_html);
		}
		$this->assignRef('fp1_image', $this->getFloorPlan($fp1, 1));
		$this->assignRef('fp2_image', $this->getFloorPlan($fp2, 2));
		
		$this->assignRef('image1', $image1);
		$this->assignRef('image2', $image2);
		
		$user	=& JFactory::getUser();
		
		$this->assignRef('watermark_text', $this->getWatermarkText($user));
		parent::display($tpl);
		
	}
	function getWatermarkText($user) {
		$db		=& 	JFactory::getDBO();
		$uid	=	$user->get('id');
		
		$query 	= ' SELECT word FROM #__ihouse_watermark WHERE user_id = '. $uid;
			$db->setQuery( $query ) ;
			$result = $db->loadResult();
		
		return $result;
	}
	function getPropertyTypeListHTML($sess_id, $property_type = '') {
		
		$db		=& 	JFactory::getDBO();
		
		$user 			=& 	JFactory::getUser();
		
		$where	=	array();
		if($user->user_category == 'Agent') {
			$where[] = ' id = 1 ';
			$where[] = ' id = 2 ';
			$where[] = ' id = 3 ';
			$where[] = ' id = 4 ';
			$where[] = ' id = 6 ';
		} else if($user->user_category == 'Owner') {
			$where[] = ' id = 1 ';
			$where[] = ' id = 2 ';
		}
		
		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' OR ', $where ) : '' );

		$query 	= ' SELECT * FROM #__ihouse_property_type '.$where;
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();

		$html = '';
		$html .= '<select name="property_type" class="" onchange="javascript:propertyType(this.value,\''.$sess_id.'\');" id="property_type" >';
		$html .= '<option value="">----</option> ';
		
		foreach($rows as $r):
			$tmp	=	preg_replace('/\s+/','_',strtolower($r->name_en));	
			
			$html .= '<option value="'.$tmp.'" '.(($r->name_ch == $property_type)?' selected="selected" ':'') .' >'.$r->label.'</option>';
		
		endforeach;
       
        $html .= '</select>';
		
		return $html;
	}
	
	function getUserUploadImage($session_id) {
		$db		=& 	JFactory::getDBO();
		
		$query 	= " SELECT * FROM #__ihouse_ads_image WHERE sess_id = '$session_id' ";
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		$html = '';
		foreach($rows as $r):
		
			$html .= '<div id="file-'.$r->id.'" style="float:left;margin:2px;">';
			$html .= '<div><img src="'.JRoute::_('images/ihouse/ads_images/'.$r->sess_id.'/'.$r->name).'" width="159px" height="126px" />';
    		$html .= '</div>';
			$html .= '<div style="text-align:center;" ><input type="radio" name="primary_photo" value="'.$r->id.'" onclick="javascript:primaryPhotoAjax(this.value,\''.$r->sess_id.'\')" '.(($r->is_primary)? 'checked="yes"' :'').' />Primary Photo<br /><input type="image" src="'.JRoute::_('templates/main/images/btn-delete.png').'" alt="Delete now!" onclick="javascript:deleteTmpAds('.$r->id.',\''.$r->name.'\',\''.$r->sess_id.'\');return false;" /></div>';
			$html .= '</div>';	
		
		endforeach;
		
		return $html;
	}
	
	function getImagePhotosHTML() {
		$postcode		=	JRequest::getVar('postcode','');
		
		$session_id		=	JRequest::getVar('session_id','');
		
		$rows			=	JRequest::getVar('ihousephotos',array());
		
		$img_path 		= JRoute::_('images/singapore/'.$postcode.'/images');

		$html = '';
		$i = 1;

		$allowed_types = array('png','jpg','jpeg','gif'); // list of filetypes you want to show

		if(file_exists($img_path)) {
			if ($handle = opendir($img_path)) {
				while (false !== ($file = readdir($handle))) {
					if($file == '.' || $file == '..' || $file == 's')
						continue;
					
					if(in_array(strtolower(substr($file,-3)),$allowed_types))
					{	
						$html .= '<div style="float:left;margin:3px">';
						$html .= '<div>';
						$html .= '	<img src="'.$img_path.'/'.$file.'" width="156px" height="129px" />';
			   			$html .= '</div>';
						$html .= '<div style="text-align:center;">';
						//$html .= '<input type="radio" name="primary_photo" value="'.$file.'" />Cover Photo';
						$html .= '</div>';
						$html .= '<div style="text-align:center;">';
						$html .= '<input type="checkbox" name="ihousephotos[]" value="'.$file.'" '.((in_array($file,$rows))?'checked="yes"':'') . ' />Tick to Display';
						$html .= '</div>';
						$html .= '</div>';
					}
				}
			}
		}
		
		return $html;
	}
	
	function getFloorPlanList($value, $name_id) {
		$db		=& 	JFactory::getDBO();
		
		$postcode		=	JRequest::getVar('postcode','');
		
		//$session_id		=	JRequest::getVar('session_id','');
		
		$tmp 		= 	explode('|', $value);
		$type_en 	= $tmp[0];
		$name		= $tmp[1]; 
		
		$query	= "SELECT * FROM #__ihouse_floorplan ";
			$db->setQuery( $query );
			$floorplan = $db->loadObjectList();
		
		
		$html = '';
		$html .= '<select name="'.$name_id.'" id="'.$name_id.'" onchange="javascript:loadFloorPlanPicture(\''.$postcode.'\',\''.$name_id.'\', this.value);">';
		$html .= '<option value="">---</option>';
		
		$allowed_types = array('png','jpg','jpeg','gif'); // list of filetypes you want to show
		
		foreach($floorplan as $fp) {
			$floorplan_path = JRoute::_('images/singapore/'.$postcode.'/floorplan/'.$fp->type_en);

			if(file_exists($floorplan_path)) {
				if ($handle = opendir($floorplan_path)) {
					while (false !== ($file = readdir($handle))) {
						if($file == '.' || $file == '..' || $file == 's')
							continue;
						
						if(in_array(strtolower(substr($file,-3)),$allowed_types))
						{
							$value2	=	$fp->type_en.'|'.$file;
							$html .= '<option '.(($value2==$value)?'selected="selected"':'').' value="'.$value2.'">'.$file.'</option>';
						}
					}
				}
			}
		}
		
		$html .= '</select>';
		
		return $html;
	}
	function getFloorPlan($fp, $ordering) {
		$db		=& 	JFactory::getDBO();
		
		$postcode		=	JRequest::getVar('postcode','');
		
			
		$html = '';
		
		if(!empty($fp)) {
			$tmp 		= 	explode('|', $fp);
			$type_en 	= $tmp[0];
			$name		= $tmp[1];
			
			$floorplan_img = JRoute::_('images/singapore/'.$postcode.'/floorplan/'.$type_en.'/'.$name);
			$html .= '<img src="'.$floorplan_img.'" width="100%" height="100%" />';
		}
		
		return $html;	
	}
	
	
}
