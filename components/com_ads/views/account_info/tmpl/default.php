<?php defined('_JEXEC') or die; ?>
<?php JHTML::_('script', 'ihouse.js', 'components/com_ads/rokbox/'); ?>
<?php JHTML::_('script', 'renew.js', 'components/com_ads/assets/'); ?>
<!--Start View-->
<div style="margin-top:-20px;">

<div id="main-search-header-text" style="padding-left:15px;border-width:2px;">会员中心&nbsp;<span style="font-size:15px;font-weight:100;"><i><?php echo $this->users->user_category ?>&nbsp;Centre</i></span></div>

<style type="text/css">
#tablist {
	border-left:3px solid #FF8200;
	border-bottom:3px solid #FF8200;
	border-right:3px solid #FF8200;
	width:99.1%;
	padding:0;
	margin:0;
	min-height:300px;
	height:auto;
	float:left;
}

table#prop_det_info td{
	border:1px solid #999;
}

ul.tab_nav3 {
list-style-image:none;
list-style-type:none;
}
</style>

<div style="padding:15px;"><!--start-->

<div id="menu_list_profile_detail">
<ul id="" class="tab_nav5">     
	<li id="tab_li5_account" class="tabactive5"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=account_info&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Acct. Info</span></a></li>
    <?php
	if(($this->users->user_category == 'Agent')||($this->users->user_category == 'Owner')||($this->users->user_category == 'Individual')){
	?>
	<li id="tab_li5_profile"><a href="<?php echo JURI::root();?>index.php?option=com_user&view=profiler&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Profile</span></a></li>
    <?php
	}
	if(($this->users->user_category == 'Agent')||($this->users->user_category == 'Owner')){
	?>
	<li id="tab_li5_ads"><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=ads_management&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Your Ads</span></a></li>
    <?php
	}
	if($this->users->user_category == 'Agent'){
	?>
    <li id="tab_li5_transaction" ><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=transaction&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Transactions</span></a></li>
    <li id="tab_li5_top_up" ><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=top_up&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Top-up</span></a></li>
    <?php
	}
	?>
    <li id="tab_li77_specialist"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=favoriteads&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Favorites</span></a></li>
</ul>   
</div>

<div id="tablist"> 
<form action="index.php?option=com_ads" method="post" name="form2" id="form2">
    <div id="tab_account" class="tabular1">
    	<div id="acct_info_cont">
                    	<div id="acctinfo_border">Account Information</div>
        </div>
        
    	<div style="padding-top:15px; padding-left:30px; padding-right:30px; float:left; width:595px;">
       	      * <a href="http://singapore.ihouse360.com/userguide/" target="_blank"><i>Read User Guide First</i></a> | <a href="http://singapore.ihouse360.com/pdf/ChineseVocabulary.xls">Chinese Vocabulary Download</a>
                <div style="float:left; width:595px; height:auto; padding-bottom:30px; color:#494949;">
                	<div>
                    	<div style="float:left; width:200px; height:40px; border-right:dotted 2px #fcd29d; border-bottom:dotted 2px #fcd29d; padding-top:10px;">
                        	<div align="center" style="font-weight:bold;"><font color="#fb9116">电子邮箱</font><br />Email</div>
                        </div>
                        <div style="float:left; width:390px; height:30px; border-bottom:dotted 2px #fcd29d; padding-top:20px;">
                        	<div style="padding-left:100px;"><?php echo $this->users->email;?></div>
                        </div>
                    </div>
                    
                    <div>
                    	<div style="float:left; width:200px; height:40px; border-right:dotted 2px #fcd29d; border-bottom:dotted 2px #fcd29d; padding-top:10px;">
                        	<div align="center" style="font-weight:bold;"><font color="#fb9116">注册日期</font><br />Registration Date</div>
                        </div>
                        <div style="float:left; width:390px; height:30px; border-bottom:dotted 2px #fcd29d; padding-top:20px;">
                        	<div style="padding-left:100px;">
							<?php
							$currentdate = explode( "-" , $this->users->registerDate);
							$cyear = $currentdate[0];
							$cmonth = $currentdate[1];
							$cday = substr($currentdate[2], 0, 2);
							echo date("d-m-Y", mktime(0, 0, 0, $cmonth, $cday, $cyear));
							
							?>
                            </div>
                        </div>
                    </div>
                    
                    <div>
                    	<div style="float:left; width:200px; height:40px; border-right:dotted 2px #fcd29d; border-bottom:dotted 2px #fcd29d; padding-top:10px;">
                        	<div align="center" style="font-weight:bold;"><font color="#fb9116">帐号类型</font><br />Account Type</div>
                        </div>
                        <div style="float:left; width:390px; border-bottom:dotted 2px #fcd29d;">
                        	<div style="padding-left:100px;  width:150px; height:30px; float:left; padding-top:20px;"><?php echo $this->users->user_category;?></div>
                            <?php
							if($this->users->user_category == 'Individual'){
							?>
                            <div style="float:left; width:80; height:35px; padding-left:30px; padding-top:15px;">
                            <input type="button" value="Upgrade" class="contactbutton" name="individualupgrade" onclick="javascript:upgrade_individual(this);" />
                            </div>
                            <?php
							}
                            ?>
                            
                             <?php
							if($this->users->user_category == 'Owner'){
							?>
                            <div style="float:left; width:80; height:35px; padding-left:30px; padding-top:15px;">
                            <input type="button" value="Upgrade" class="contactbutton" name="ownerupgrade" onclick="javascript:upgrade_owner(this);" />
                            </div>
                            <?php
							}
                            ?>
                        </div>
                    </div>
                    
                    <!-- Start Subscription -->
					<?php
                    if(($this->users->user_category == 'Agent')||($this->users->user_category == 'Owner')){
					?>
                    <div>
                    	<div style="float:left; width:200px; height:40px; border-right:dotted 2px #fcd29d; border-bottom:dotted 2px #fcd29d; padding-top:10px;">
                        	<div align="center" style="font-weight:bold;">
                            	<font color="#fb9116">当前订阅的广告计划</font><br />Current Ads subscription plan
                            </div>
                        </div>
                        
                        <div style="float:left; width:390px; border-bottom:dotted 2px #fcd29d;">
                        	<?php 
							if($this->users->user_category == 'Agent'){
								if($this->aplanid == '0'){
							?>
                            <div style="padding-left:100px; width:150px; height:30px; float:left; padding-top:20px;"><?php echo 'Per Pay Basis'; ?></div>
                            <div style="float:left; width:80; height:35px; padding-left:30px; padding-top:15px;">
                            <input type="button" value="Upgrade" class="contactbutton" name="agent_sub" id="agent_sub" onclick="javascript:sub_agent(this);" />
                            </div>
                            <?php
								} else {
							?>
                            
                            <div style="padding-left:100px; width:150px; height:30px; float:left; padding-top:20px;"><?php echo $this->aplanname;  ?></div>
                            <?php if($this->agent_plan_id == 2) : /* Quarterly */ ?>
                            <div style="float:left; width:80; height:35px; padding-left:30px; padding-top:15px;">
                            <input type="button" value="Upgrade" class="contactbutton" name="agent_sub" id="agent_sub" onclick="javascript:sub_agent(this);" />
                            <?php endif; ?>
                            <?php
								}
							}//end agent
							?>
                            
                            
                            <?php 
							if($this->users->user_category == 'Owner'){
								if($this->oplanid == '0'){
							?>
                            <div style="padding-left:100px; width:150px; height:30px; float:left; padding-top:20px;"><?php echo 'Basic Package'; ?></div>
                            <div style="float:left; width:80; height:35px; padding-left:30px; padding-top:15px;">
                            <input type="button" value="Upgrade" class="contactbutton" name="owner_sub" id="owner_sub" onclick="javascript:sub_owner(this);" />
                            </div>
                            <?php
								} else {
							?>
                            <div style="padding-left:100px; width:150px; height:30px; float:left; padding-top:20px;"><?php echo $this->ownerplanname;  ?>
                            </div>
                            
                            <?php
								}
							}//end owner
							?>
                        </div>
                    </div>
                    
                    <!-- Start Expiry -->
                    <div>
                    	<div style="float:left; width:200px; height:40px; border-right:dotted 2px #fcd29d; border-bottom:dotted 2px #fcd29d; padding-top:10px;">
                        	<div align="center" style="font-weight:bold;"><font color="#fb9116">过期时间</font><br />Expiry Date</div>
                        </div>
                        <div style="float:left; width:390px; height:30px; border-bottom:dotted 2px #fcd29d; padding-top:20px;">
                        	<div style="padding-left:100px;float:left;">
                            <?php
							
							if($this->users->user_category == 'Agent'){
								if($this->aplanid == '0'){
									echo 'N.A';
								} else {
									$currentdate = explode( "-" , $this->agentexpiry);
									$cyear = $currentdate[0];
									$cmonth = $currentdate[1];
									$cday = substr($currentdate[2], 0, 2);
									echo date("d-m-Y", mktime(0, 0, 0, $cmonth, $cday, $cyear));
								}
							} 
							
							if ($this->users->user_category == 'Owner'){
								if($this->oplanid == '0'){
									echo 'N.A';
								} else {
									$currentdate = explode( "-" , $this->ownerexpiry);
									$cyear = $currentdate[0];
									$cmonth = $currentdate[1];
									$cday = substr($currentdate[2], 0, 2);
									echo date("d-m-Y", mktime(0, 0, 0, $cmonth, $cday, $cyear));
								}
							}
							?>
                            
                            </div>
                            <div style="float:left;margin-left:5px;margin-top:-4px;">
                            <?php 
								if ($this->users->user_category == 'Owner' && $this->oplanid != '0'){
							?>
                            	 <input type="button" value="Renew" class="searchbutton" onclick="javascript:renewUserSubscription(<?php echo $this->ownersubsid ?>);">
                            <?php		
								}
								
								if ($this->users->user_category == 'Agent' && $this->aplanid != '0'){
							?>
                            	 <input type="button" value="Renew" class="searchbutton" onclick="javascript:renewUserSubscription(<?php echo $this->agentsubsid ?>);">
                            <?php		
								}
							?>
                           
                            </div>
                            <div style="float:left;margin-left:5px;">                            
                            
                            <?php 
								
							
								if ($this->users->user_category == 'Owner'){
									if($this->oplanid != '0'){
										echo 'Price :'; 
										if($this->discount > 0.0) {
											echo '<span style="text-decoration:line-through;color:red;">';
											echo $this->oprice->currency.' '.(int) $this->oprice->rate;
											echo '</span>&nbsp;';
											echo $this->oprice->currency.' '.(int) ceil($this->oprice->rate * $this->discount);
										} else {
											echo $this->oprice->currency.' '.(int) $this->oprice->rate;	
										}
									}
								}
								
								if ($this->users->user_category == 'Agent'){
									if($this->aplanid != '0'){
										echo 'Price :'; 
										if($this->discount > 0.0) {
											echo '<span style="text-decoration:line-through;color:red;">';
											echo $this->aprice->currency.' '.(int) $this->aprice->rate;
											echo '</span>&nbsp;';
											echo $this->aprice->currency.' '.(int) ceil($this->aprice->rate * $this->discount);
										} else {
											echo $this->aprice->currency.' '.(int) $this->aprice->rate;
										}
									}
								}
							?>
                            </div>
                            <div style="clear:both"></div>
                        </div>
                    </div>
                    <?php
					}//end expiry and subscription
					?>
                    
                    <?php
							if($this->users->user_category == 'Agent'){
					?>
                    <div>
                    	<div style="float:left; width:200px; height:40px; border-right:dotted 2px #fcd29d; border-bottom:dotted 2px #fcd29d; padding-top:10px;">
                        	<div align="center" style="font-weight:bold;"><font color="#fb9116">当前帐号余额</font><br />Current Credit Balance</div>
                        </div>
                        <div style="float:left; width:390px; height:30px; border-bottom:dotted 2px #fcd29d; padding-top:20px;">
                        	<div style="padding-left:100px;">
							<?php echo $this->credit;?>
                            </div>
                        </div>
                    </div>
                    <?php
					}//end credit
					?>
                    
                    
                </div>

        </div>
        
        <!--Start of image-->
        <div style="padding-top:5px; padding-bottom:20px; padding-left:15px; padding-right:15px; float:left; width:625px;">
        	<?php
			if($this->users->user_category == 'Agent'){
			?>
        	<img src="<?php echo JURI::root(); ?>/templates/main/images/agent.jpg" />
            <?php } else { ?>
            <img src="<?php echo JURI::root(); ?>/templates/main/images/owner.jpg" />
            <?php } ?>
        </div>
        <!--End of image-->
    </div><!--tabular1-->
    <input type="hidden" name="option" id="option" value="com_ads" />
    <input type="hidden" name="Itemid" id="Itemid" value="126" />
    <input type="hidden" name="view" id="view" value="" />
    <input type="hidden" name="action" id="action" value="" />
<script LANGUAGE="JavaScript">
<!--
function upgrade_individual(submit){
var form = document.form2;

var a = "1";
document.getElementById('action').value = a;
var b = "account_upgrade";
document.getElementById('view').value = b;
form.submit()
}

function upgrade_owner(submit){
var form = document.form2;

var a = "2";
document.getElementById('action').value = a;
var b = "account_upgrade";
document.getElementById('view').value = b;
form.submit()
}

function sub_agent(submit){
var form = document.form2;

var a = "3";
document.getElementById('action').value = a;
var b = "subscription";
document.getElementById('view').value = b;
form.submit()
}

function sub_owner(submit){
var form = document.form2;

var a = "4";
document.getElementById('action').value = a;
var b = "subscription";
document.getElementById('view').value = b;
form.submit()
}

-->
</script>
</form>
</div>    




</div><!--End-->



</div>
<!--End View-->
