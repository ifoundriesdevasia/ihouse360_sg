<?php
/**
 * @version		$Id: view.html.php 11646 2009-03-01 19:34:56Z ian $
 * @package		Joomla
 * @subpackage	iHouse360 Account Info
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the iHouse360 component
 *
 * @static
 * @package		Joomla
 * @subpackage	Account Info view
 * @since 1.0
 */
class AdsViewAccount_info extends JView
{
	function display($tpl = null)
	{
		global $mainframe;
		$db = & JFactory::getDBO();
		
		//$id = JRequest::getVar('id', '', 'get', 'id');
		$user =& JFactory::getUser();
		$id = $user->get('id');
		
		$oplanid = '1';
		
		$oplanid = $this->getOPlanId($id);
		$aplanid = $this->getAPlanId($id);

		$this->assignRef('users', $this->getInfoOfThisUser($id));
		$this->assignRef('credit', $this->getCreditOfThisUser($id));
		$this->assignRef('aplanid', $this->getAgentPlanid($id));
		$this->assignRef('aplanname', $this->getAgentPlanname($id));
		$this->assignRef('oplanid', $this->getOwnerPlanid($id));
		$this->assignRef('ownerplanname', $this->getOwnerPlanname($id));
		$this->assignRef('agentexpiry', $this->getAgentExpirydate($id));
		$this->assignRef('ownerexpiry', $this->getOwnerExpirydate($id));
		
		$this->assignRef('agentsubsid', $this->getAgentSubscriptionId($id));
		$this->assignRef('ownersubsid', $this->getOwnerSubscriptionId($id));
		
		$this->assignRef('oprice', $this->getPriceSubscription($oplanid));
		$this->assignRef('aprice', $this->getPriceSubscription($aplanid));
		
		$this->assignRef('discount', $this->getDiscount());
		
		$this->assignRef('agent_plan_id', $aplanid);
		$this->assignRef('owner_plan_id', $oplanid);
		
		parent::display($tpl);
	}
	
	function getInfoOfThisUser($id = 0) {
		
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT * FROM #__users "
						.	" WHERE id = '$id'" ;
						
			$db->setQuery( $query );
			$list = $db->loadObject();		
			//echo print_r($list);
		return $list;	
	}
	
	function getCreditOfThisUser($id = 0) {
		
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT credit FROM #__ihouse_users_credit "
						.	" WHERE user_id = '$id'" ;
						
			$db->setQuery( $query );
			$list = $db->loadResult();		
			//echo print_r($list);
		return $list;	
	}
	
	function getAgentPlanid($id = 0) {
		
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT count(*) FROM #__cbsubs_subscriptions "
						.	" WHERE user_id = '$id' AND status = 'A' AND (plan_id = '2' OR plan_id = '3')" ;
						
			$db->setQuery( $query );
			$plan_id = $db->loadResult();	
			
			//echo print_r($list);
		return $plan_id;	
	}
	
	function getAgentPlanname($id = 0){
	
	if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
		
	$query		=	" SELECT plan_id FROM #__cbsubs_subscriptions "
						.	" WHERE user_id = '$id' AND status = 'A' AND (plan_id = '2' OR plan_id = '3')" ;
						
			$db->setQuery( $query );
			$plan_id = $db->loadResult();
	
	$query		=	" SELECT name FROM #__cbsubs_plans "
						.	" WHERE id = '".$plan_id."'" ;
						
			$db->setQuery( $query );
			$list = $db->loadResult();
				
		return $list;
	
	}
	
	function getAgentExpirydate($id = 0) {
		
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
			
		$query		=	" SELECT expiry_date FROM #__cbsubs_subscriptions "
						.	" WHERE user_id = '$id' AND status = 'A' AND (plan_id = '2' OR plan_id = '3')" ;
						
			$db->setQuery( $query );
			$list = $db->loadResult();	
			//echo print_r($list);
		return $list;	
	}
	
	function getOwnerPlanid($id = 0) {
		
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT count(*) FROM #__cbsubs_subscriptions "
						.	" WHERE user_id = '$id' AND status = 'A' AND plan_id = '1'" ;
						
			$db->setQuery( $query );
			$plan_id = $db->loadResult();	
			
			//echo print_r($list);
		return $plan_id;	
	}
	
	function getOwnerPlanname($id = 0) {
		
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
			
		$query		=	" SELECT name FROM #__cbsubs_plans "
						.	" WHERE id = '1'" ;
						
			$db->setQuery( $query );
			$list = $db->loadResult();	
			//echo print_r($list);
		return $list;	
	}
	
	function getOwnerExpirydate($id = 0) {
		
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
			
		$query		=	" SELECT expiry_date FROM #__cbsubs_subscriptions "
						.	" WHERE user_id = '$id' AND status = 'A' AND plan_id = '1'" ;
						
			$db->setQuery( $query );
			$list = $db->loadResult();	
			//echo print_r($list);
		return $list;	
	}
	
	function getAgentSubscriptionId($id = 0) {
		
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
			
		$query		=	" SELECT id FROM #__cbsubs_subscriptions "
						.	" WHERE user_id = '$id' AND status = 'A' AND (plan_id = '2' OR plan_id = '3')" ;
						
			$db->setQuery( $query );
			$list = $db->loadResult();	
			//echo print_r($list);
		return $list;	
	}
	function getOwnerSubscriptionId($id = 0) {
		
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
			
		$query		=	" SELECT id FROM #__cbsubs_subscriptions "
						.	" WHERE user_id = '$id' AND status = 'A' AND plan_id = '1'" ;
						
			$db->setQuery( $query );
			$list = $db->loadResult();	
			//echo print_r($list);
		return $list;	
	}
	function getPriceSubscription($planid) {
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT rate, currency FROM #__cbsubs_plans "
						.	" WHERE id = '$planid' " ;
						
			$db->setQuery( $query );
			$list = $db->loadObject();	
		
		return $list;	
	}
	function getAPlanId($id) {
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT plan_id FROM #__cbsubs_subscriptions "
						.	" WHERE user_id = '$id' AND status = 'A' AND (plan_id = '2' OR plan_id = '3')" ;
						
			$db->setQuery( $query );
			$plan_id = $db->loadResult();
		return $plan_id;	
	}
	function getOPlanId($id) {
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
			
		$query		=	" SELECT plan_id FROM #__cbsubs_subscriptions "
						.	" WHERE user_id = '$id' AND status = 'A' AND (plan_id = '1')" ;
						
			$db->setQuery( $query );
			$list = $db->loadResult();	
			//echo print_r($list);
		return $list;	
	}
	function getDiscount() {
		$db 	  	=& 	JFactory::getDBO();
			
		$query		=	" SELECT params FROM #__ihouse_promotion "
						.	" WHERE id = '2' " ;
						
			$db->setQuery( $query );
			$param = $db->loadResult();	
		
		$discount 	= str_replace('discount=','',$param);

		$gets 	= ( 1 - (float) $discount) ;
		
		return (float) $gets;	
	}
	
}
