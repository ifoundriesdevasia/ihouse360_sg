﻿<?php
/**
 * @version		$Id: view.html.php 11646 2009-03-01 19:34:56Z ian $
 * @package		Joomla
 * @subpackage	iHouse360 Featured Posting Form
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the iHouse360 component
 *
 * @static
 * @package		Joomla
 * @subpackage	Featured posting form view
 * @since 1.0
 */
class AdsViewPostfeatured extends JView
{
	function display($tpl = null)
	{
		global $mainframe;
		
		$db = & JFactory::getDBO();
		$user_id	= JRequest::getVar('user_id', '', 'post', 'user_id');
		$plan_id	= JRequest::getVar('plan_id', '', 'post', 'plan_id');
		$ads_type_config	= JRequest::getVar('ads_type_config', '', 'post', 'ads_type_config');
		
		$query = "SELECT name FROM #__cbsubs_plans WHERE id = '".$plan_id."'";
		$db->setQuery($query);
		$sub_name = $db->loadResult();
		
		$query = "SELECT p.id AS id, p.name_en AS name FROM #__ihouse_property p, #__ihouse_ads_type_slot s WHERE s.ads_type_config = '1' AND s.space <> '0' AND s.property_id = p.id ORDER BY p.name_en ASC";
		$db->setQuery($query);
		$typeones = $db->loadObjectList();
		
		$oneOptions[] = JHTML::_('select.option', '', JText::_('Please Select'));
		foreach ($typeones as $typeone) {
		$oneOptions[] = JHTML::_('select.option', $typeone->id,$typeone->name);
		}
		
		$query = "SELECT p.id AS id, p.name_en AS name FROM #__ihouse_property p, #__ihouse_ads_type_slot s WHERE s.ads_type_config = '2' AND s.space <> '0' AND s.property_id = p.id ORDER BY p.name_en ASC";
		$db->setQuery($query);
		$typetwos = $db->loadObjectList();
		
		$twoOptions[] = JHTML::_('select.option', '', JText::_('Please Select'));
		foreach ($typetwos as $typetwo) {
		$twoOptions[] = JHTML::_('select.option', $typetwo->id,$typetwo->name);
		}
		
		$query = "SELECT count(*) FROM #__ihouse_ads_type_slot WHERE ads_type_config = '4' AND property_type = 'rent' AND space <> '0'";
		$db->setQuery($query);
		$rent4_slot = $db->loadResult();
		
		$query = "SELECT count(*) FROM #__ihouse_ads_type_slot WHERE ads_type_config = '4' AND property_type = 'sell' AND space <> '0'";
		$db->setQuery($query);
		$sell4_slot = $db->loadResult();
		
		$query = "SELECT * FROM #__ihouse_ads WHERE posted_by = '".$user_id."' ORDER BY property_name ASC";
		$db->setQuery($query);
		$ads_names = $db->loadObjectList();
		
		$adsOptions[] = JHTML::_('select.option', '', JText::_('Please Select'));
		foreach ($ads_names as $ads_name) {
		$adsOptions[] = JHTML::_('select.option', $ads_name->id,$ads_name->ad_title);
		}
		
		$query = "SELECT p.id AS id, p.name_en AS name FROM #__ihouse_property p, #__ihouse_ads_type_slot s WHERE s.ads_type_config = '5' AND s.space <> '0' AND s.property_id = p.id ORDER BY p.name_en ASC";
		$db->setQuery($query);
		$typefives = $db->loadObjectList();
		
		$fiveOptions[] = JHTML::_('select.option', '', JText::_('Please Select'));
		foreach ($typefives as $typefive) {
		$fiveOptions[] = JHTML::_('select.option', $typefive->id,$typefive->name);
		}
		
		$query = "SELECT count(*) FROM #__ihouse_ads_type_slot WHERE ads_type_config = '6' AND property_type = 'rent' AND space <> '0'";
		$db->setQuery($query);
		$rent6_slot = $db->loadResult();
		
		$query = "SELECT count(*) FROM #__ihouse_ads_type_slot WHERE ads_type_config = '6' AND property_type = 'sell' AND space <> '0'";
		$db->setQuery($query);
		$sell6_slot = $db->loadResult();

		$this->assignRef('user_id'			, 	$user_id );
		$this->assignRef('plan_id'			, 	$plan_id );
		$this->assignRef('sub_name'			, 	$sub_name );
		$this->assignRef('ads_type_config'	, 	$ads_type_config );
		$this->assignRef('oneOptions'		, 	$oneOptions );
		$this->assignRef('twoOptions'		, 	$twoOptions );
		$this->assignRef('fiveOptions'		, 	$fiveOptions );
		$this->assignRef('rent4_slot'		, 	$rent4_slot );
		$this->assignRef('sell4_slot'		, 	$sell4_slot );
		$this->assignRef('rent4_slot'		, 	$rent6_slot );
		$this->assignRef('sell4_slot'		, 	$sell6_slot );
		$this->assignRef('adsOptions'		, 	$adsOptions );
		parent::display($tpl);
		
	}
	
	
}
