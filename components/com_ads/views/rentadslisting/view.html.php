﻿<?php
/**
 * @version		$Id: view.html.php 11646 2009-03-01 19:34:56Z ian $
 * @package		Joomla
 * @subpackage	iHouse360 Rent Ads Listing
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the iHouse360 component
 *
 * @static
 * @package		Joomla
 * @subpackage	Rent Ads Listing view
 * @since 1.0
 */
class AdsViewRentAdslisting extends JView
{
	function display($tpl = null)
	{
		global $mainframe;
		$db 	  	=& 	JFactory::getDBO();
		
		$id	= JRequest::getVar('id', '', 'get', 'id');
		
		
		
		
		//print_r($this->getPriImage($id));
		$this->assignRef('ads', $this->getAdsDetails($id));
		//$this->assignRef('agentrent', $this->getAgentRentDetails($id));
		//$this->assignRef('ownerrent', $this->getOwnerRentDetails($id));
		parent::display($tpl);
	}
	
	function getAdsDetails($id = 0) {
		
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
		
	
		$query	=	'SELECT a.*, u.name, u.chinese_name, u.user_image,u.mobile_contact  '
							.	' FROM #__ihouse_ads AS a '
							//.	' LEFT JOIN #__ihouse_ads_image AS i ON i.ads_id = a.id '
							.	' LEFT JOIN #__users AS u ON u.id = a.posted_by '
							.	' LEFT JOIN #__ihouse_ads_type AS t ON t.user_id = a.posted_by AND t.ads_id = a.id'
							.	' LEFT JOIN #__cbsubs_subscriptions AS c ON c.user_id = a.posted_by AND c.id = t.subscription_id '
							.   ' WHERE t.property_type = "rent" AND a.publish = "1" AND t.ads_type_config = "6" AND c.status = "A" ORDER BY RAND()';
							//.   ' WHERE i.is_primary = "1" AND a.ad_type = "rent" AND t.ads_type_config = "6" AND c.status = "A" AND a.status_id = "待租" AND a.property_id = "'.$id.'"';

						
			$db->setQuery( $query );
			$list = $db->loadObjectList();

		return $list;	
	}
	
	function getAgentRentDetails($id = 0) {
		
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
		
	
		$query	=	'SELECT a.*,i.sess_id, i.name AS img_name, u.name, u.id AS userid, u.chinese_name, u.user_image,u.mobile_contact  '
							.	' FROM #__ihouse_ads AS a '
							.	' LEFT JOIN #__ihouse_ads_image AS i ON i.ads_id = a.id '
							.	' LEFT JOIN #__users AS u ON u.id = a.posted_by '
							.	' LEFT JOIN #__cbsubs_subscriptions AS c ON c.user_id = a.posted_by AND c.id = a.subscription_id '
							.   ' WHERE a.ad_type = "rent" AND a.publish = "1" AND c.status = "A" AND a.status_id = "待租" AND u.user_category = "Agent" AND a.property_id = "'.$id.'"';

						
			$db->setQuery( $query );
			$list = $db->loadObjectList();

		return $list;	
	}
	
	function getOwnerRentDetails($id = 0) {
		
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
		
	
		$query	=	'SELECT a.*,i.sess_id, i.name AS img_name, u.name, u.chinese_name, u.user_image,u.mobile_contact  '
							.	' FROM #__ihouse_ads AS a '
							.	' LEFT JOIN #__ihouse_ads_image AS i ON i.ads_id = a.id '
							.	' LEFT JOIN #__users AS u ON u.id = a.posted_by '
							.	' LEFT JOIN #__cbsubs_subscriptions AS c ON c.user_id = a.posted_by AND c.id = a.subscription_id '
							.   ' WHERE a.ad_type = "rent" AND i.is_primary = "1" AND a.publish = "1" AND c.status = "A" AND a.status_id = "待租" AND u.user_category = "Owner" AND a.property_id = "'.$id.'"';

						
			$db->setQuery( $query );
			$list = $db->loadObjectList();

		return $list;	
	}
}
