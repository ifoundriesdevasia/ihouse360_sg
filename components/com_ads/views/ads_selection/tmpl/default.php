﻿<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('behavior.tooltip'); ?>
<?php
$user =& JFactory::getUser();
$user_id = $user->get('id');
?>
<div> <!--Start view-->
<div id="reg_border">
			<div id="reg_header">
			Select Ad Type
            </div>
</div>

<div style="padding:20px;">

<table cellpadding="5" cellspacing="2">
<tr>
<td bgcolor="#f98b10" width="200" align="center"><div style="font-weight:bold; color:#FFFFFF; font-size:14px;">Ad Type</div></td>
<td bgcolor="#f98b10" width="100" align="center"><div style="font-weight:bold; color:#FFFFFF; font-size:14px;">Price</div></td>
<td bgcolor="#f98b10" width="200" align="center"><div style="font-weight:bold; color:#FFFFFF; font-size:14px;">Features</div></td>
<td bgcolor="#f98b10" width="100" align="center"><div style="font-weight:bold; color:#FFFFFF; font-size:14px;">Action</div></td>
</tr>
<form method="post" name="ads_form" id="ads_form" action="<?php echo JURI::root(); ?>index.php?option=com_ads">
<?php
if($this->user_category == 'Owner'){
?>
<tr>
<td bgcolor="#c1dff9" width="200" align="center">Basic Package</td>
<td bgcolor="#c1dff9" width="100" align="center">Free</td>
<td bgcolor="#c1dff9" width="200" align="center">One free ad</td>
<td bgcolor="#c1dff9" width="100" align="center">
<?php
if($this->count_ads == '1'){
?>
<input type="button" value="Unavailable" disabled="disabled" name="button_disable" id="button_disable" />
<?php
} else if ($this->count_ads == '0'){
?>
<input type="submit" value="Click here to post" name="owner_postads" id="owner_postads" onClick="javascript:owner_post_ads(this);" />
<?php
}
?>
</td>

</tr>
<tr>
<td bgcolor="#c1dff9" width="200" align="center">Unlimited Refresh Package<br />
<font style="color:#FF0000; font-weight:bold; font-size:10px;"><?php if($this->owner_sub == '1'){ echo 'Subscribe'; }?></font>
</td>
<td bgcolor="#c1dff9" width="100" align="center">$20/yr</td>
<td bgcolor="#c1dff9" width="200" align="center">Additional free ad with unlimited refresh and with no expiry date</td>
<td bgcolor="#c1dff9" width="100" align="center">
<?php
	if($this->owner_sub == '0'){
?>
<input type="submit" value="Subscribe now" name="owner_subscribe" id="owner_subscribe" onClick="javascript:owner_paypal(this);" />
<?php
	} else {
	
	if ($this->count_paid_ads == '1'){
?>
<input type="button" value="Unavailable" disabled="disabled" name="button_disable" id="button_disable" />
<?php
	} else {
?>
<input type="submit" value="Click here to post" name="owner_postads1" id="owner_postads1" onClick="javascript:owner_post_ads1(this);"  />
<?php
	}
}
?>
</td>

</tr>
<?php
}
?>

<?php
if($this->user_category == 'Agent'){
?>
<tr>
<td bgcolor="#c1dff9" width="200" align="center">Per-pay basis</td>
<td bgcolor="#c1dff9" width="100" align="center">$6/listing</td>
<td bgcolor="#c1dff9" width="200" align="center">All listings in this category will be 90 days live. Refresh ads at $2/time and capped at $12/listing.</td>
<td bgcolor="#c1dff9" width="100" align="center">
<input type="submit" value="Click here to post" name="perpaybasis" id="perpaybasis" onClick="javascript:per_pay_basis(this);" />
</td>
</tr>
<tr>
<td bgcolor="#c1dff9" width="200" align="center">
Quarterly Subscription<br />
<font style="color:#FF0000; font-weight:bold; font-size:10px;"><?php if($this->quarter_sub == '1'){ echo 'Subscribe'; }?></font>
</td>
<td bgcolor="#c1dff9" width="100" align="center">$120</td>
<td bgcolor="#c1dff9" width="200" align="center">Up to 50 concurrent listings and unlimited refreshing.</td>
<td bgcolor="#c1dff9" width="100" align="center">
<?php
if(($this->quarter_sub == '0')&&($this->yearly_sub == '0')){ //no subscription yet
?>
<input type="submit" value="Subscribe now" name="quarter_subscribe" id="quarter_subscribe" onClick="javascript:quarter_sub(this);" />
<?php
} else if (($this->quarter_sub == '1')&&($this->yearly_sub == '0')){
?>
<input type="submit" value="Click here to post" name="agent_postads" id="agent_postads" onClick="javascript:agent_post_ads1(this);" />
<?php
} else if (($this->quarter_sub == '0')&&($this->yearly_sub == '1')){
?>
<input type="button" value="Unavailable" disabled="disabled" name="button_disable" id="button_disable" />
<?php
}
?>
</td>
</tr>
<tr>
<td bgcolor="#c1dff9" width="200" align="center">
Yearly Subscription<br />
<font style="color:#FF0000; font-weight:bold; font-size:10px;"><?php if($this->yearly_sub == '1'){ echo 'Subscribe'; }?></font>
</td>
<td bgcolor="#c1dff9" width="100" align="center">$388</td>
<td bgcolor="#c1dff9" width="200" align="center">Up to 50 concurrent listings and unlimited refreshing.</td>
<td bgcolor="#c1dff9" width="100" align="center">
<?php
if(($this->quarter_sub == '0')&&($this->yearly_sub == '0')){ //no subscription yet
?>
<input type="submit" value="Subscribe now" name="yearly_subscribe" id="yearly_subscribe" onClick="javascript:yearly_sub(this);" />
<?php
} else if (($this->quarter_sub == '0')&&($this->yearly_sub == '1')){
?>
<input type="submit" value="Click here to post" name="agent_postads" id="agent_postads" onClick="javascript:agent_post_ads2(this);" />
<?php
} else if (($this->quarter_sub == '1')&&($this->yearly_sub == '0')){
?>
<input type="button" value="Unavailable" disabled="disabled" name="button_disable" id="button_disable" />
<?php
}
?>
</td>
</tr>
<tr><td colspan="4">&nbsp;</td></tr>
<tr>
<td colspan="4" bgcolor="#f98b10"><div style="padding-left:10px; font-weight:bold; color:#FFFFFF; font-size:14px;">Optional : Featured ads subscription</div></td>
</tr>
<tr>
<td bgcolor="#c1dff9" width="200" align="center">
Expert for the Condo<br />
<font style="font-size:10px; color:#FF0000; font-weight:bold"><?php if($this->type1 == '0'){ echo 'No free slot'; }?></font>
</td>
<td bgcolor="#c1dff9" width="100" align="center">$200/mth</td>
<td bgcolor="#c1dff9" width="200" align="center">1 space per condo</td>
<td bgcolor="#c1dff9" width="100" align="center">
<?php
if($this->type1 == '0'){
?>
<input type="button" value="Unavailable" disabled="disabled" name="button_disable" id="button_disable" />
<?php
} else {
?>
<input type="submit" value="Click here to post" name="expertforcondo" id="expertforcondo" onClick="javascript:expert_for_condo(this);" />
<?php
}
?>
</td>
</tr>
<tr>
<td bgcolor="#c1dff9" width="200" align="center">
Recommended Specialist<br />
<font style="font-size:10px; color:#FF0000; font-weight:bold"><?php if($this->type2 == '0'){ echo 'No free slot'; }?></font>
</td>
<td bgcolor="#c1dff9" width="100" align="center">$120/mth</td>
<td bgcolor="#c1dff9" width="200" align="center">3 spaces per condo</td>
<td bgcolor="#c1dff9" width="100" align="center">
<?php
if($this->type2 == '0'){
?>
<input type="button" value="Unavailable" disabled="disabled" name="button_disable" id="button_disable" />
<?php
} else {
?>
<input type="submit" value="Click here to post" name="recommendspecialist" id="recommendspecialist" onClick="javascript:recommend_specialist(this);" />
<?php
}
?>
</td>
</tr>
<tr>
<td bgcolor="#c1dff9" width="200" align="center">
Condo Expert<br />
<font style="font-size:10px; color:#FF0000; font-weight:bold"><?php if($this->type3 == '0'){ echo 'No free slot'; }?></font>
</td>
<td bgcolor="#c1dff9" width="100" align="center">$300/mth</td>
<td bgcolor="#c1dff9" width="200" align="center">3x2 space</td>
<td bgcolor="#c1dff9" width="100" align="center">
<?php
if($this->type3 == '0'){
?>
<input type="button" value="Unavailable" disabled="disabled" name="button_disable" id="button_disable" />
<?php
} else {
?>
<input type="submit" value="Click here to post" name="condoexpert" id="condoexpert" onClick="javascript:condo_expert(this);" />
<?php
}
?>
</td>
</tr>
<tr>
<td bgcolor="#c1dff9" width="200" align="center">
Type IV: Featured Listing<br />
<font style="color:#FF0000; font-weight:bold; font-size:10px;">* Book 4-week in a row and get 4th week FREE!</font><br />
<font style="font-size:10px; color:#FF0000; font-weight:bold"><?php if($this->type4 == '0'){ echo 'No free slot'; }?></font>
</td>
<td bgcolor="#c1dff9" width="100" align="center">$30/week</td>
<td bgcolor="#c1dff9" width="200" align="center">Side bar of all pages</td>
<td bgcolor="#c1dff9" width="100" align="center"><br />
<input type="radio" value="8" name="type_iv" id="type_iv" checked="checked" />1 week<br />
<input type="radio" value="9" name="type_iv" id="type_iv" />2 week<br />
<input type="radio" value="10" name="type_iv" id="type_iv" />3 week<br />
<input type="radio" value="11" name="type_iv" id="type_iv" />4 week<br /><br />
<?php
if($this->type4 == '0'){
?>
<input type="button" value="Unavailable" disabled="disabled" name="button_disable" id="button_disable" />
<?php
} else {
?>
<input type="submit" value="Click here to post" name="typeIV" id="typeIV" onClick="javascript:type4_subs(this);" />
<?php
}
?>
</td>
</tr>
<tr>
<td bgcolor="#c1dff9" width="200" align="center">
Type V: Featured Listing<br />
<font style="color:#FF0000; font-weight:bold; font-size:10px;">* Book 4-week in a row and get 4th week FREE!</font><br />
<font style="font-size:10px; color:#FF0000; font-weight:bold"><?php if($this->type5 == '0'){ echo 'No free slot'; }?></font>
</td>
<td bgcolor="#c1dff9" width="100" align="center">$10/week</td>
<td bgcolor="#c1dff9" width="200" align="center">Display at particular condo only</td>
<td bgcolor="#c1dff9" width="100" align="center"><br />
<input type="radio" value="12" name="type_v" id="type_v" checked="checked" />1 week<br />
<input type="radio" value="13" name="type_v" id="type_v" />2 week<br />
<input type="radio" value="14" name="type_v" id="type_v" />3 week<br />
<input type="radio" value="15" name="type_v" id="type_v" />4 week<br /><br />
<?php
if($this->type5 == '0'){
?>
<input type="button" value="Unavailable" disabled="disabled" name="button_disable" id="button_disable" />
<?php
} else {
?>
<input type="submit" value="Click here to post" name="typeV" id="typeV" onClick="javascript:type5_subs(this);" />
<?php
}
?>
</td>
</tr>
<tr>
<td bgcolor="#c1dff9" width="200" align="center">
Type VI: Featured Listing<br />
<font style="color:#FF0000; font-weight:bold; font-size:10px;">* Book 4-week in a row and get 4th week FREE!</font><br />
<font style="font-size:10px; color:#FF0000; font-weight:bold"><?php if($this->type6 == '0'){ echo 'No free slot'; }?></font>
</td>
<td bgcolor="#c1dff9" width="100" align="center">$50/week</td>
<td bgcolor="#c1dff9" width="200" align="center">Display at outstanding place and always there</td>
<td bgcolor="#c1dff9" width="100" align="center"><br />
<input type="radio" value="16" name="type_vi" id="type_vi" checked="checked" />1 week<br />
<input type="radio" value="17" name="type_vi" id="type_vi" />2 week<br />
<input type="radio" value="18" name="type_vi" id="type_vi" />3 week<br />
<input type="radio" value="19" name="type_vi" id="type_vi" />4 week<br /><br />
<?php
if($this->type6 == '0'){
?>
<input type="button" value="Unavailable" disabled="disabled" name="button_disable" id="button_disable" />
<?php
} else {
?>
<input type="submit" value="Click here to post" name="typeVI" id="typeVI" onClick="javascript:type6_subs(this);" />
<?php
}
?>
</td>
</tr>
<tr>
<td bgcolor="#c1dff9" width="200" align="center">
Type VII: Featured Listing<br />
<font style="color:#FF0000; font-weight:bold; font-size:10px;">* Book 4-week in a row and get 4th week FREE!</font><br />
<font style="font-size:10px; color:#FF0000; font-weight:bold"><?php if($this->type7 == '0'){ echo 'No free slot'; }?></font>
</td>
<td bgcolor="#c1dff9" width="100" align="center">$50/week</td>
<td bgcolor="#c1dff9" width="200" align="center">Display for New condo for sell only</td>
<td bgcolor="#c1dff9" width="100" align="center"><br />
<input type="radio" value="20" name="type_vii" id="type_vii" checked="checked" />1 week<br />
<input type="radio" value="21" name="type_vii" id="type_vii" />2 week<br />
<input type="radio" value="22" name="type_vii" id="type_vii" />3 week<br />
<input type="radio" value="23" name="type_vii" id="type_vii" />4 week<br /><br />
<?php
if($this->type7 == '0'){
?>
<input type="button" value="Unavailable" disabled="disabled" name="button_disable" id="button_disable" />
<?php
} else {
?>
<input type="submit" value="Click here to post" name="typeVII" id="typeVII" onClick="javascript:type7_subs(this);" />
<?php
}
?>
</td>
</tr>
<?php
}
?>
<input name="option" type="hidden" value="com_ads" />
<input type="hidden" name="view" id="view" value="" />
<input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id;?>" />
<input type="hidden" name="plan_id" id="plan_id" value="" />
<input type="hidden" name="payment_mode" id="payment_mode" value="" />
<input type="hidden" name="ads_type_config" id="ads_type_config" value="" />
<script type="text/javascript">

						function owner_post_ads(submit) {
							var a = "postads";
							document.getElementById('view').value = a;
							var b = "25";
							document.getElementById('plan_id').value = b;
						}
						
						function owner_post_ads1(submit) {
							var a = "postads";
							document.getElementById('view').value = a;
							var b = "1";
							document.getElementById('plan_id').value = b;
						}

						function owner_paypal(submit) {
							var a = "paypal_owner";
							document.getElementById('view').value = a;
							var b = "1";
							document.getElementById('plan_id').value = b;
						}
						
						function per_pay_basis(submit) {
							var a = "credit_management";
							document.getElementById('view').value = a;
							var b = "4";
							document.getElementById('plan_id').value = b;
						}
						
						function quarter_sub(submit) {
							var a = "credit_management";
							document.getElementById('view').value = a;
							var b = "2";
							document.getElementById('plan_id').value = b;
						}
						
						function yearly_sub(submit) {
							var a = "credit_management";
							document.getElementById('view').value = a;
							var b = "3";
							document.getElementById('plan_id').value = b;
						}
						
						function agent_post_ads1(submit) {
							var a = "postads";
							document.getElementById('view').value = a;
							var b = "2";
							document.getElementById('plan_id').value = b;
						}
						
						function agent_post_ads2(submit) {
							var a = "postads";
							document.getElementById('view').value = a;
							var b = "3";
							document.getElementById('plan_id').value = b;
						}
						
						function expert_for_condo(submit) {
							var a = "credit_management";
							document.getElementById('view').value = a;
							var b = "5";
							document.getElementById('plan_id').value = b;
							var c = "1";
							document.getElementById('ads_type_config').value = c;
						}
						
						function recommend_specialist(submit) {
							var a = "credit_management";
							document.getElementById('view').value = a;
							var b = "6";
							document.getElementById('plan_id').value = b;
							var c = "2";
							document.getElementById('ads_type_config').value = c;
						}
						
						function condo_expert(submit) {
							var a = "credit_management";
							document.getElementById('view').value = a;
							var b = "7";
							document.getElementById('plan_id').value = b;
							var c = "3";
							document.getElementById('ads_type_config').value = c;
						}
						
						function type4_subs(submit) {
							var a = "credit_management";
							document.getElementById('view').value = a;
							var b = "type4";
							document.getElementById('plan_id').value = b;
							var c = "4";
							document.getElementById('ads_type_config').value = c;
						}
						
						function type5_subs(submit) {
							var a = "credit_management";
							document.getElementById('view').value = a;
							var b = "type5";
							document.getElementById('plan_id').value = b;
							var c = "5";
							document.getElementById('ads_type_config').value = c;
						}
						
						function type6_subs(submit) {
							var a = "credit_management";
							document.getElementById('view').value = a;
							var b = "type6";
							document.getElementById('plan_id').value = b;
							var c = "6";
							document.getElementById('ads_type_config').value = c;
						}
						
						function type7_subs(submit) {
							var a = "credit_management";
							document.getElementById('view').value = a;
							var b = "type7";
							document.getElementById('plan_id').value = b;
							var c = "7";
							document.getElementById('ads_type_config').value = c;
						}
						
</script>   
</form>
</table>
</div>
</div> <!--End view-->
