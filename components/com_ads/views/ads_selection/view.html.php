<?php
/**
 * @version		$Id: view.html.php 11646 2009-03-01 19:34:56Z ian $
 * @package		Joomla
 * @subpackage	iHouse360 Ads Selection form
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the iHouse360 component
 *
 * @static
 * @package		Joomla
 * @subpackage	Ads Selection form view
 * @since 1.0
 */
class AdsViewAds_selection extends JView
{
	function display($tpl = null)
	{
		global $mainframe;
		
		$user =& JFactory::getUser();
		$user_id = $user->get('id');
		$user_category = $user->get('user_category');
		
		if(!isset($user_id)){
		echo '<span class="cb_result_error">Please login to access to this page.</span>';
		exit;
		}
		
		if(($user_category != "Agent")&&($user_category != "Owner")){
		echo '<span class="cb_result_error">You do not have permission to access to this page.</span>';
		exit;
		}
		
		$db = & JFactory::getDBO();
		
		$query = "SELECT count(*) FROM #__ihouse_ads a, #__cbsubs_subscriptions c WHERE a.subscription_id = c.id AND c.plan_id = '25' AND a.posted_by = c.user_id AND a.posted_by = ".$user_id;
	
		$db->setQuery($query);
		$count_ads = $db->loadResult();
		
		$query = "SELECT count(*) FROM #__cbsubs_subscriptions WHERE plan_id = '1' AND status = 'A' AND user_id = ".$user_id;
	
		$db->setQuery($query);
		$owner_sub = $db->loadResult();
		
		$query = "SELECT count(*) FROM #__ihouse_ads a, #__cbsubs_subscriptions c WHERE a.subscription_id = c.id AND c.plan_id = '1' AND a.posted_by = c.user_id AND a.posted_by = ".$user_id;
	
		$db->setQuery($query);
		$count_paid_ads = $db->loadResult();
		
		$query = "SELECT credit FROM #__ihouse_users_credit WHERE user_id = ".$user_id;
	
		$db->setQuery($query);
		$credit = $db->loadResult();
		
		$query = "SELECT count(*) FROM #__cbsubs_subscriptions WHERE plan_id = '2' AND status = 'A' AND user_id = ".$user_id;
	
		$db->setQuery($query);
		$quarter_sub = $db->loadResult();
		
		$query = "SELECT count(*) FROM #__cbsubs_subscriptions WHERE plan_id = '3' AND status = 'A' AND user_id = ".$user_id;
	
		$db->setQuery($query);
		$yearly_sub = $db->loadResult();
		
		$query = "SELECT count(*) FROM #__ihouse_ads_type_slot WHERE ads_type_config = '1' AND space <> '0'";
	
		$db->setQuery($query);
		$type1 = $db->loadResult();
		
		$query = "SELECT count(*) FROM #__ihouse_ads_type_slot WHERE ads_type_config = '2' AND space <> '0'";
	
		$db->setQuery($query);
		$type2 = $db->loadResult();
		
		$query = "SELECT count(*) FROM #__ihouse_ads_type_slot WHERE ads_type_config = '3' AND space <> '0'";
	
		$db->setQuery($query);
		$type3 = $db->loadResult();
		
		$query = "SELECT count(*) FROM #__ihouse_ads_type_slot WHERE ads_type_config = '4' AND space <> '0'";
	
		$db->setQuery($query);
		$type4 = $db->loadResult();
		
		$query = "SELECT count(*) FROM #__ihouse_ads_type_slot WHERE ads_type_config = '5' AND space <> '0'";
	
		$db->setQuery($query);
		$type5 = $db->loadResult();
		
		$query = "SELECT count(*) FROM #__ihouse_ads_type_slot WHERE ads_type_config = '6' AND space <> '0'";
	
		$db->setQuery($query);
		$type6 = $db->loadResult();
		
		$query = "SELECT count(*) FROM #__ihouse_ads_type_slot WHERE ads_type_config = '7' AND space <> '0'";
	
		$db->setQuery($query);
		$type7 = $db->loadResult();
		
		$this->assignRef('user_category',  $user_category);
		$this->assignRef('count_paid_ads',  $count_paid_ads);
		$this->assignRef('count_ads',  $count_ads);
		$this->assignRef('owner_sub',  $owner_sub);
		$this->assignRef('credit',  $credit);
		$this->assignRef('quarter_sub',  $quarter_sub);
		$this->assignRef('yearly_sub',  $yearly_sub);
		$this->assignRef('type1',  $type1);
		$this->assignRef('type2',  $type2);
		$this->assignRef('type3',  $type3);
		$this->assignRef('type4',  $type4);
		$this->assignRef('type5',  $type5);
		$this->assignRef('type6',  $type6);
		$this->assignRef('type7',  $type7);

		parent::display($tpl);
	}
}
