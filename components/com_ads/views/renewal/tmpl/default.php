﻿<?php defined('_JEXEC') or die; ?>
<?php JHTML::_('behavior.tooltip'); ?>
<?php JHTML::_('script', 'ihouse.js', 'components/com_ads/rokbox/'); ?>
<!--Start View-->
<div style="margin-top:-20px;">

<div id="main-search-header-text" style="padding-left:15px;border-width:2px;">会员中心&nbsp;<span style="font-size:15px;font-weight:100;"><i><?php echo $this->users->user_category ?>&nbsp;Centre</i></span></div>
<script type="text/javascript">
function renewbutton(subs_id) {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();	
	
	var answer = confirm("Do you want to renew this subscription?")

	if(answer) {
		var url = 'index.php?option=com_ads&view=renewSubscription&r=' + unixtime_ms;		
		var req = new Ajax(url, {
	   					data		: 	
									{
										'subs_id' : subs_id
									},
						method		: "get",
		    			onSuccess	: function(data) {
							var obj1 		= Json.evaluate(data);
							var status 		= obj1.status;
							if(status) {
								window.location.reload();
							} else {
								alert('Not Enough Credit');
							}
		   				},
						evalScripts : true
				}).request();		
	}
}
</script>

<style type="text/css">
#tablist {
	border-left:3px solid #FF8200;
	border-bottom:3px solid #FF8200;
	border-right:3px solid #FF8200;
	width:99.1%;
	padding:0;
	margin:0;
	min-height:300px;
	height:auto;
	float:left;
}

#tablist1 {
	width:99.1%;
	padding:0;
	margin:0;
	min-height:300px;
	height:auto;
	float:left;
}

#tablist2 {
	border-left:2px solid #eeeeee;
	border-bottom:2px solid #eeeeee;
	border-right:2px solid #eeeeee;
	width:99.3%;
	padding:0;
	margin:0;
	/*min-height:100px;*/
	height:auto;
	float:left;
}

table#prop_det_info td{
	border:1px solid #999;
}

ul.tab_nav3 {
list-style-image:none;
list-style-type:none;
}

.activeTabRenew {
	padding:0.4em 0.4em;
	font-size:13px;
	background-color:#FC8F0A;
}

.inactiveTabRenew {
	padding:0.4em 0.4em;
	font-size:13px;
	background-color:#908374;
}


</style>

<div style="padding:15px;"><!--start-->

<div id="menu_list_profile_detail">
<ul id="" class="tab_nav5">   
	<li id="tab_li5_account"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=account_info&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Acct. Info</span></a></li>
     <?php
	if(($this->users->user_category == 'Agent')||($this->users->user_category == 'Owner')||($this->users->user_category == 'Individual')){
	?>
	<li id="tab_li5_profile"><a href="<?php echo JURI::root();?>index.php?option=com_user&view=profiler&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Profile</span></a></li>
    <?php
	}
	if(($this->users->user_category == 'Agent')||($this->users->user_category == 'Owner')){
	?>
	<li id="tab_li5_ads" class="tabactive5"><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=ads_management&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Your Ads</span></a></li>
    <?php
	}
	if($this->users->user_category == 'Agent'){
	?>
    <li id="tab_li5_transaction"><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=transaction&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Transactions</span></a></li>
    <li id="tab_li5_top_up" ><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=top_up&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Top-up</span></a></li>
    <?php
	}
	?>
    <li id="tab_li77_specialist"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=favoriteads&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Favorites</span></a></li>
</ul>   
</div>

<div id="tablist"> 
    <div id="tab_profile" class="tabular1" style="display:none;">
    	<div style="padding:15px;">
            <div>
            	&nbsp;
            </div>
        </div>
    </div><!--tabular1-->
    
    <div id="tab_ads" class="tabular1">
     	<div style="padding: 10px;">
            <div>
            	 
<div id="menu_list_ads_listing">
<ul id="" class="tab_nav77">     
	<li id="tab_li77_listing"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=ads_management&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Listing</span></a></li>
	<li id="tab_li77_guru"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=the_guru&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">The Guru</span></a></li>
    <li id="tab_li77_specialist"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=postads&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Place New Ads</span></a></li>
    <li id="tab_li77_specialist" class="tabactive77"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=renewal&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">My Featured Ads Renew</span></a></li>
</ul>   
</div><!--menu_list_profile_detail-->

<div style="float:left;margin:2px 0;color:red;">
<?php if($this->discount > 0.0) : ?>
	<?php echo (1 - $this->discount) * 100 ?>% Discounts have been applied.
<?php endif; ?>
</div>
<div style="clear:both"></div>
<div id="tablist1" style="width:13%;margin-right:5px;"> 
	<div id="tab_listing" class="tabular2">
    	<div style="">
			<div <?php echo ($this->category == 'adtype') ? ' class="activeTabRenew" ' : ' class="inactiveTabRenew" ' ?>>
        	<a href="<?php echo JRoute::_('index.php?option=com_ads&view=renewal&Itemid=126&id=63&cat=adtype'); ?>" style="padding:0.4em 0.4em;color:#fff;text-decoration:none;">Property</a>
        	</div>
    		<div <?php echo ($this->category == 'guru') ? ' class="activeTabRenew" ' : ' class="inactiveTabRenew" ' ?>>
            <a href="<?php echo JRoute::_('index.php?option=com_ads&view=renewal&Itemid=126&id=63&cat=guru'); ?>" style="padding:0.4em 0.4em;color:#fff;text-decoration:none;">The Guru</a>
            </div>
        </div>
	</div>    
</div>


<div id="tablist1" style="width:85%;"> 
<div id="tab_listing" class="tabular2">
	<div style="color:#fa8b11; font-weight:bold; line-height:30px;">
        <div style="background-color:#f0f0f0; border:solid 1px #e0e0e0; float:left; width:100%; height:auto;">
            <?php if($this->category == 'adtype') 	: ?> <div style="float:left; width:21%">Ad Title</div><?php endif; ?>
            <?php if($this->category == 'guru')		: ?> <div style="float:left; width:21%">Condo</div><?php endif; ?>
            <div style="float:left; width:28%">Ad Type</div>
            <div style="float:left; width:9%;">Price</div>
            <div style="float:left; width:14%;">Subscription</div>
            <div style="float:left; width:14%;">Expired</div>
            <div style="float:left; width:12%;text-align:center;">Renewal</div>
        </div>
    </div>
    
    <form action="index.php?option=com_ads" method="post" name="form" id="form">
	<input type="hidden" name="ads_id" value="" />
    <?php
						$k = 0;
						foreach($this->adstypelistings as $r):
						
							$sdate = explode( "-" , $r->subscription_date);
							$syear = $sdate[0];
							$smonth = $sdate[1];
							$sday = substr($sdate[2], 0, 2);	
							
							$edate = explode( "-" , $r->expiry_date);
							$eyear = $edate[0];
							$emonth = $edate[1];
							$eday = substr($edate[2], 0, 2);	
							
							switch($r->plan_id) {
								case 11: // 4
								case 15: // 5
								case 19: // 6
								case 23: // 7
									$rates = $r->rate *0.75;
									break;
								default :
									$rates = $r->rate;
									break;
							} 
	?>
    <div style="border-bottom:solid 1px #e0e0e0; float:left; height:auto; width:100%; line-height:15px; padding-top:5px; padding-bottom:2px;">
    	
        <div style="float:left;width:21%;">
        <?php if($this->category == 'adtype') 	: ?>
        <a href="<?php echo JRoute::_('index.php?option=com_ads&view=adsdetails&id='.$r->ads_id) ?>" style="color:#334D7A;text-decoration:none;"><?php echo $r->ad_title;?></a>
        <?php endif; ?> 
        <?php if($this->category == 'guru')		: ?>
        	<?php if($r->name_en) : ?>
		<a href="<?php echo JRoute::_('index.php?option=com_ihouse&view=ihouse&layout=property_detail&id='.$r->property_id) ?>" style="color:#334D7A;text-decoration:none;"><?php echo $r->name_en?></a>
        	<?php else : ?>
            N/A
            <?php endif; ?>
		<?php endif; ?>  
        </div>
        <div style="float:left; width:28%;"><?php echo $r->name ?></div>
        <div style="float:left; width:9%;">
        <?php if($this->discount > 0.0) : ?>
        <span style="text-decoration:line-through;color:red;">S$<?php echo (int) $r->rate ?></span><br />
        <span>S$<?php echo (int) $rates * $this->discount ?></span>
        <?php else : ?>
        <span>S$<?php echo (int) $rates ?></span>
        <?php endif; ?>
        </div>
        <div style="float:left; width:14%;"><?php echo date("d-m-Y", mktime(0, 0, 0, $smonth, $sday, $syear));?></div>
        <div style="float:left; width:14%;"><?php echo date("d-m-Y", mktime(0, 0, 0, $emonth, $eday, $eyear));?></div>
        <div style="float:left; width:12%;text-align:center;">
        <input type="button" onclick="javascript:renewbutton('<?php echo $r->adtype_subs_id ?>');" value="" class="refreshbutton tooltip" title="Renew">
        </div>
    </div>
    <?php
		endforeach;
	?>
   
    <input type="hidden" name="option" id="option" value="com_ads" />
    <input type="hidden" name="Itemid" id="Itemid" value="126" />
    <input type="hidden" name="view" id="view" value="" />
    <input type="hidden" name="selectid" id="selectid" value="" />
    </form>
</div>

<div id="tab_guru" class="tabular2" style="display:none;">
&nbsp;
</div><!--tab_guru-->

<div id="tab_specialist" class="tabular2" style="display:none;">rr
</div>

                 
</div>   <!--tablist-->              
            </div>
        </div>
    </div><!--tabular1-->
    
  


</div>    




</div><!--End-->



</div>
<!--End View-->
