<?php
/**
 * @version		$Id: view.html.php 11646 2009-03-01 19:34:56Z ian $
 * @package		Joomla
 * @subpackage	iHouse360 Ads Management
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the iHouse360 component
 *
 * @static
 * @package		Joomla
 * @subpackage	 Renewal
 * @since 1.0
 */
class AdsViewRenewal extends JView
{
	function display($tpl = null)
	{
		global $mainframe;
		$db = & JFactory::getDBO();
		
		//$id = JRequest::getVar('id', '', 'get', 'id');
		$user 	=& JFactory::getUser();
		$id = $user->get('id');
		
		$sub	=	JRequest::getVar('cat','adtype');
		
		switch($sub) {
			case 'adtype' 	:
				$this->assignRef('adstypelistings', $this->getAdsTypeListing($id, $sub));
			break;	
			case 'guru'		:
				$this->assignRef('adstypelistings', $this->getAdsTypeListing($id, $sub));
			break;
		}
		
		$this->assignRef('discount', $this->applyDiscount());
		$this->assignRef('users', $user);
		$this->assignRef('category', $sub);
		parent::display($tpl);
	}
	
	function getAdsTypeListing($id = 0, $cat = 'adtype') {
		if(!$id)
			return false;
		
		$db 	  	=& 	JFactory::getDBO();
		
		$join_misc	=	'';
		
		if($cat == 'adtype') {
			$planid = ' t.ads_type_config IN (4,5,6,7) ';
			$join_misc = ' LEFT JOIN #__ihouse_ads AS a ON a.id = t.ads_id ';
		} else {
			$planid = ' t.ads_type_config IN (1,2,3) ';
			$join_misc = ' LEFT JOIN #__ihouse_property AS a ON a.id = t.property_id ';
		}
		
		$query = " SELECT * , t.subscription_id AS adtype_subs_id  FROM #__ihouse_ads_type AS t " 
					.	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = t.subscription_id "
					.	" LEFT JOIN #__cbsubs_plans AS cp ON cp.id = cb.plan_id "
					.	$join_misc
					.	" WHERE t.user_id = '$id' AND cb.status = 'A' "
					.	" AND ".$planid;
					
		$db->setQuery($query);
		$lists = $db->loadObjectList();
		
		return $lists;	
	}
	
	function applyDiscount() {
		$db 	  	=& 	JFactory::getDBO();
		
		$sql = ' SELECT params FROM #__ihouse_promotion WHERE id = 4 LIMIT 1';
			$db->setQuery($sql);
			$param = $db->loadResult();
		
		$discount 	= str_replace('discount=','',$param);

		$gets 	= ( 1 - (float) $discount) ;
		
		return (float) $gets;
	}
	
}
