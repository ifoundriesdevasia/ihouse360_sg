<?php defined('_JEXEC') or die('Restricted access'); ?>
<div class="popupForm" >
	<div id="t">
    	<div id="l">&nbsp;</div>
        <div id="c">&nbsp;</div>
        <div id="r">&nbsp;</div>
    </div>
    <div style="clear:both;"></div>
    <div id="m">
    	<div id="l">&nbsp;</div>
        <div id="c">
        	<div id="popupFormBox">
            	<div id="padding" >
            		<div id="title">
    					Email Form   
                    </div>
                    <div style="clear:both"></div>
                    <form id="emailFormContact" action="<?php echo JRoute::_('index.php?option=com_ads&view=email'); ?>" method="post">
                    <div id="emailform">
                    	<div id="name-box" class="emailFormBox">
                    		<div><label>请输入您的名字/Name</label></div>
	                        <div><input type="text" name="name" value="" /></div>
	                    </div>
	                    <div id="email-box" class="emailFormBox">
	                    	<div><label>请输入您的电子邮箱/Email</label></div>
	                        <div><input type="text" name="email_to_contact" value="" /></div>
	                    </div>
	                    <div id="mobile-box" class="emailFormBox">
	                    	<div><label>请输入您的联系电话/Mobile no.</label></div>
	                        <div><input type="text" name="mobile" value="" /></div>
	                    </div>
	                    <div id="message-box" class="emailFormBox">
	                    	<div><label>请输入您要咨询的问题/Message</label></div>
	                        <div><textarea name="message" id="textarea-emailformbox" maxlength="300"></textarea></div>
	                    </div>
                        <div class="emailFormBox">
                        	<div style="float:left;">
                        		<input type="button" onclick="javascript:submitEmailFormPopup('<?php echo $email ?>');return false;" id="agent_sub" name="agent_sub" class="contactbutton" value="提交">
                            </div>
                            <div style="float:left;margin-left:10px;">
                            	 <input type="button" onclick="javascript:closeEmailFormPopup();" id="agent_sub" name="agent_sub" class="contactbutton" value="取消">
                            </div>    
                        </div>               
                    </div> 
                    <input type="hidden" name="subject" value="Contact Agent Form"  />
                    <input type="hidden" name="email" value="<?php echo $email ?>"  />
                    <input type="hidden" name="ads_id" value="<?php echo $ads_id ?>"  />
                    <input type="hidden" name="uid" value="<?php echo $uid ?>"  />
                    <input type="hidden" name="option" value="com_ads" />
                    <input type="hidden" name="view" value="email"  /> 
                    </form>
                    <div id="ajax-submit-form-email" style="width:500px;text-align:center;padding-top:150px;display:none;">
                        	<div><img src="<?php echo JRoute::_('templates/main/images/ajax-loader-big.gif'); ?>" /></div>
                            <div style="margin-top:10px;color:#525252;font-size:16px;">Submitting</div>
                    </div>
                    <div style="width:500px;text-align:center;padding-top:150px;">
                    	<div id="text-message-sbm"></div>  	
                    </div>
            	</div>
        	</div>
        </div>
        <div id="r">&nbsp;</div>
    </div>
    <div style="clear:both"></div>
    <div id="b">
    	<div id="l">&nbsp;</div>
        <div id="c">&nbsp;</div>
        <div id="r">&nbsp;</div>
    </div>
    <div style="clear:both"></div>
</div>