<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('behavior.tooltip'); ?>

<script type="text/javascript">
	/* Do not remove this */
	var postcode = '<?php echo $this->postcode ?>';
</script>

<?php JHTML::_('script', 'ihouse.js', 'components/com_ihouse/assets/'); ?>
<?php JHTML::_('script', 'postads.js', 'components/com_ads/assets/'); ?>

<div style="float:left; width:100%; background-color:#FFFFFF;">
	<div style="float:left;color:red;margin:10px 15px 0 15px;">
		* This is for Preview Purpose only
	</div>
	<div style="float:left; width:100%; color:#fe8700; font-size:16px; font-weight:bold; border-bottom:solid 2px #fb9014;">
		<div style="padding-left:15px; padding-top:10px; padding-bottom:5px;">
    		<?php echo $this->ad_title ?>
    	</div>
	</div>

	<div style="padding:15px;" >
		<div style="float:left; width:100%; padding:10px 0; color:#5b5b5b; font-size:13px; font-weight:bold;">
			<!--<div style="float:left; height:22px; width:90px;">
            	<img class="icon-img" src="<?php echo JRoute::_('templates/main/images/share.png'); ?>" />&nbsp;分享
            </div>-->
    		<div style="float:left; height:22px; width:80px;">
            	<img class="icon-img1" src="<?php echo JRoute::_('templates/main/images/print.png'); ?>" />&nbsp;打印
            </div>
    		<!--<div style="float:left; height:22px; width:100px;">
            	<img class="icon-img" src="<?php echo JRoute::_('templates/main/images/compare.png'); ?>" />&nbsp;比比看
            </div>
            -->
    		<div style="float:left; height:22px; width:120px;">
            	<img class="icon-img" src="<?php echo JRoute::_('templates/main/images/email.png'); ?>" />&nbsp;电邮给朋友
            </div>
            <!--
    		<div style="float:left; height:22px; width:190px;">
            	<img class="icon-img" src="<?php echo JRoute::_('templates/main/images/pdf.png'); ?>" />&nbsp;导出为PDF
            </div>
            -->
    		<div style="float:left;"><!--浏览次数：3--></div>
		</div>

		<div id="menu_list_property_detail">
			<ul id="" class="tab_nav5">     
				<li id="tab_li5_info" class="tabactive5">
                	<a href="javascript:propertyDetailInfoTab('info')"><span class="chinese_text_menu">基本信息</span></a>
                </li>
				<li id="tab_li5_prop_desc">
                	<a href="javascript:propertyDetailInfoTab('prop_desc')"><span class="chinese_text_menu">物业介绍</span></a>
                </li>
    			<li id="tab_li5_nearest_station" >
                	<a href="javascript:propertyDetailInfoTab('nearest_station')"><span class="chinese_text_menu">附近地铁站</span></a>
                </li>
    			<li id="tab_li5_nearest_school" >
                	<a href="javascript:propertyDetailInfoTab('nearest_school')"><span class="chinese_text_menu">附近学校</span></a>
                </li>
    			<li id="tab_li5_user_rating" >
                	<a href="javascript:propertyDetailInfoTab('user_rating')"><span class="chinese_text_menu">用户评分</span></a>
                </li>
			</ul>   
		</div>

	<div id="tablist"> 

		<div id="tab_info" class="tabular1">
    		<div style="padding:10px 10px 40px 20px;">
    			<div style="float:left;width:100%;">
                	<div id="static-text" style="float:left;">基本信息</div>
                	<div style="padding-left:5px;float:left;">
                    	<span style="font-size:13px;">
                			<i><strong>Basic Information</strong></i>
                        </span>
                	</div>  	
	            </div>
                
                <div style="clear:both"></div>
                	
            	<div style="float:left;width:60%; padding-bottom:30px;">
	                <table id="prop_det_info" cellspacing=0 cellpadding=0 width="100%">
                	<tr style="background-color:#f8f8f8;">
    	            	<td width="40%" id="table-detail-font"><div class="table_space">物业名</div></td>
	                    <td width="60%" class="table-detail-font-value">
                        	<div class="table_space">
                            	<?php if($this->property_type == '公寓' ) : ?>
                           
                        		<span class="color-text1">
                                
								<?php echo ($this->property_name)?stripslashes($this->property_name):'N/A' ?>
                                
                            	</span>
                            	<?php endif; ?>
                            
                            	<?php if($this->property_type == '服务公寓' || $this->property_type == '商业' || $this->property_type == '有地住宅') : ?>
                            
                            	<span class="color-text1">
                                
								<?php echo ($this->project_name)?stripslashes($this->project_name):'N/A' ?>
                                
                            	</span>
                            
                            	<?php endif; ?>
                            	<?php if($this->property_type == '组屋' ) : ?>
                        		<span class="color-text1">
								<?php echo ($this->street_name)?stripslashes($this->street_name):'N/A' ?>
                            	</span>
                            	<?php endif; ?>
                                
                                
                            </div>
	                    </td>
	                </tr>
                    <?php if($this->property_type == '组屋') : ?>
                    <tr>
	                	<td id="table-detail-font"><div class="table_space">大牌</div></td>
	                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1">
						<?php echo ($this->block_house_no)?'Blk '.$this->block_house_no.'&nbsp;':'N/A';?></span></div>
                        </td>
                	</tr>
                    <?php endif; ?>
	                <tr>
	                	<td id="table-detail-font"><div class="table_space">地址</div></td>
	                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo ($this->street_name)?stripslashes($this->street_name).'&nbsp;':''; ?><?php echo ($this->postcode)?$this->postcode.'&nbsp;':''; ?><?php echo stripslashes($this->country) ?></span></div>
                        </td>
                	</tr>
                	<tr style="background-color:#f8f8f8;">
                		<td id="table-detail-font"><div class="table_space">类型</div></td>
                    	<td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo ($this->property_type)?$this->property_type:'N/A'; ?></span></div></td>
                	</tr>
                	<tr>
                	<td id="table-detail-font">
                    <div class="table_space">
                    <?php if($this->ad_type == 'rent') : ?>
                        月租 (新元)
                    <?php elseif($this->ad_type == 'sell') : ?>
                        售价 (新元)
                    <?php endif; ?>
                    </div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo ($this->asking_price)?'S$ '.$this->asking_price:'N/A' ?></span></div></td>
                	</tr>
                	<tr style="background-color:#f8f8f8;">
                    <td id="table-detail-font"><div class="table_space">单价 
					<?php //if($this->row->property_type == '组屋') : ?>
                    	(新元) 
                    <?php //endif; ?>
                    </div></td>
                    <td class="table-detail-font">
                    <div class="table_space">
                    	<span class="color-text1">
                         <?php //if($this->property_type == '公寓') : ?>
							<?php echo '(每平方米)&nbsp;' ?><?php echo ($this->psm_price)?'S$ '.number_format($this->psm_price):'-' ?><br />
                            <?php echo '(每平方尺)&nbsp;' ?><?php echo ($this->psm_price)?'S$ '.number_format($this->psm_price / 10.764):'-' ?>
                         <?php //else: ?>
                         
                         <?php //echo ($this->psm_price)?'S$ '.number_format($this->psm_price):'-' ?>
                            
                         <?php //endif; ?>
                            
                        </span>
                    </div></td>
                    
                	
                    
                	</tr>
               	 	<tr>
                	<td id="table-detail-font"><div class="table_space">面积</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo ($this->size)?$this->size.' 平方尺':'N/A' ?></span></div></td>
                	</tr>
                
                	<!-- Rental Type only for Rent -->
                    <?php if($this->ad_type == 'rent') : ?>
                    <tr>
                    	<td id="table-detail-font"><div class="table_space">出租类型</div></td>
                    	<td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo ($this->rental_type)?$this->rental_type:'N/A'; ?></span></div></td>
                    </tr>
                    <?php endif; ?>
                    
                	<tr>
                	<tr>
                	<td id="table-detail-font"><div class="table_space">楼层</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo ($this->floor_level)?$this->floor_level:'N/A'; ?></span></div></td>
                	</tr>
                	<tr style="background-color:#f8f8f8;">
                	<td id="table-detail-font"><div class="table_space">房间</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo $this->no_of_room; ?><?php echo ($this->no_of_room == 7)?'+':''; ?></span></div></td>
                	</tr> 
                
                	<tr>
                	<td id="table-detail-font"><div class="table_space">厅</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo $this->no_of_hall; ?><?php echo ($this->no_of_hall == 4)?'+':''; ?></span></div></td>
                	</tr>
                	<tr style="background-color:#f8f8f8;">
                	<td id="table-detail-font"><div class="table_space">书房</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo $this->no_study_room; ?><?php echo ($this->no_study_room == 2)?'+':''; ?></span></div></td>
                	</tr>
                	<tr>
                	<td id="table-detail-font"><div class="table_space">卫生间</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo $this->toilet; ?><?php echo ($this->toilet == 5)?'+':''; ?></span></div></td>
                	</tr>
                    <?php if($this->property_type == '公寓') : ?>
                	<tr style="background-color:#f8f8f8;">
                	<td id="table-detail-font"><div class="table_space">邮区</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo ($this->district)?$this->district:'N/A'; ?></span></div></td>
                	</tr>
                    <?php endif; ?>
                    
                     <?php if($this->property_type == '组屋') : ?>
                	<tr style="background-color:#f8f8f8;">
                	<td id="table-detail-font"><div class="table_space">地区</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo ($this->hdb_town_id)?$this->hdb_town_id:'N/A'; ?></span></div></td>
                	</tr>
                    <tr>
                	<td id="table-detail-font"><div class="table_space">户型</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo ($this->flattype)?$this->flattype:'N/A'; ?></span></div></td>
                	</tr>
                    <?php endif; ?>
                    
                    
                	<tr>
                	<td id="table-detail-font"><div class="table_space">邮编</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo $this->postcode ?></span></div></td>
                	</tr>
                	<tr style="background-color:#f8f8f8;">
                	<td id="table-detail-font"><div class="table_space">地契 (年)</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo ($this->tenure)?$this->tenure:'N/A' ?></span></div></td>
                	</tr>
                	<tr>
                	<td id="table-detail-font"><div class="table_space">建筑年份</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo ($this->year_built)?$this->year_built:'N/A' ?></span></div></td>
                	</tr>
                	<tr style="background-color:#f8f8f8;">
                	<td id="table-detail-font"><div class="table_space">装修状况</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo ($this->renovation)?$this->renovation:'N/A' ?></span></div></td>
                	</tr>
                	<tr>
                	<td id="table-detail-font"><div class="table_space">目前状况</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo ($this->status)?$this->status:'N/A' ?></span></div></td>
                	</tr>
                	<tr style="background-color:#f8f8f8;">
                	<td id="table-detail-font"><div class="table_space">更新日期</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo date("Y年m月d日", time()) ?></span></div></td>
                	</tr>
                	</table>    
    			</div>
                
            	<div style="float:right;width:37%;">
					-- Your Contact Profile will be here --
            	</div>
                
            </div>	
      
		</div>
        <div style="clear:both;"></div>   
    	<div id="tab_prop_desc" class="tabular1" style="display:none;">
    		<div style="padding: 10px;">
            	<div style="float:left;border-bottom:2px solid #062284;width:100%;">
            	<div id="static-text" style="float:left;">物业介绍</div>
                <div style="padding-left:5px;float:left;"><span style="font-size:13px;"><i><strong>Property Description</strong></i></span></div>
                </div>
                <div style="clear:both"></div>
                <div style="float:left;margin-top:10px;width:100%;word-wrap:break-word;"><!-- width : 400px -->
            	<?php echo nl2br($this->add_info) ?>
            	</div>
                <div style="clear:both"></div>
        	</div>
    	</div>
    	<div id="tab_nearest_station" class="tabular1" style="display:none;">
    		<div style="padding: 10px;">
            	<div style="float:left;border-bottom:2px solid #062284;width:100%;">
            	<div id="static-text" style="float:left;">附近地铁站</div>
                <div style="padding-left:5px;float:left;"><span style="font-size:13px;"><i><strong>Nearest MRT Station</strong></i></span></div>
                </div>
                <div style="clear:both"></div>
                <div style="margin-top:10px;">
            	--- Nearest MRT ---
            	</div>
        	</div>
    	</div>
    	<div id="tab_nearest_school" class="tabular1" style="display:none;">
    		<div style="padding: 10px;">
            	<div style="float:left;border-bottom:2px solid #062284;width:100%;">
            	<div id="static-text" style="float:left;">附近学校</div>
                <div style="padding-left:5px;float:left;"><span style="font-size:13px;"><i><strong>Nearest School</strong></i></span></div>
                </div>
                <div style="clear:both"></div>
                <div style="margin-top:10px;">
            	--- Nearest School ---
            	</div>
        	</div>
    	</div>
    	<div id="tab_user_rating" class="tabular1" style="display:none;">
    		<div style="padding: 10px;">
            	<div style="float:left;border-bottom:2px solid #062284;width:100%;">
            	<div id="static-text" style="float:left;">用户评分</div>
                <div style="padding-left:5px;float:left;"><span style="font-size:13px;"><i><strong>User Rating</strong></i></span></div>
                </div>
                <div style="clear:both"></div>
                <div style="margin-top:10px;">
            		--- User Rating ---
            	</div>
        	</div>
    	</div>
	</div>

	<div style="color:red;padding-top:10px;">
	Note : These images are for preview purposes only.
	</div>

	<div style="padding-top:15px;">
		<ul class="tab_nav3">   
        	<?php if($this->hasImages) { ?>   
			<li id="tab_li3_image" class="tabactive3"><a href="javascript:propertyDetailTab('image')"><span class="chinese_text_menu">房源图片</span></a></li>
            <?php } ?>
            
            <?php if($this->hasFloorplan) { ?> 
			<li id="tab_li3_floorplan"><a href="javascript:propertyDetailTab('floorplan')"><span class="chinese_text_menu">户型图</span></a></li>
            <?php } ?>
			<li id="tab_li3_googlemap" <?php echo ($this->hasImages)?'':' class="tabactive3" ' ?>><a href="javascript:propertyDetailTab('googlemap')"><span class="chinese_text_menu">Google 地图</span></a></li>
		</ul>
	</div>
	<div id="propertyInfo12wrap" class="borderPropertyTab">
		<div id="propertyInfo12content" style="height:460px;width:680px;">
        	<?php if($this->hasImages) { ?> 
			<div id="property_detail_image" style="margin-bottom:50px;text-align:center;">
    			{modulepos postcode_img_preview}
			</div>                        
			<?php } ?>
            
            <?php if($this->hasFloorplan) { ?> 
			<div id="floor_plan_image" style="display:none;">	
				<?php echo $this->fp1_img; ?>
   				<?php echo $this->fp2_img; ?>
			</div>
			<?php } ?>
			<div id="google_map_image">
				<div id="canvas_map" style="width:659px;height: 460px;"></div> 
			</div>   
		</div>
	</div>
	<div style="clear:both"></div>

	<div style="float:left;margin:10px 10px;color:red;">
	'Save' = save the ad but not publishing ad<br  />
	'Publish' = save and publish the ad<br  />
	<b>**The same reduction of 6 Credits when you saved ads By Per-Pay base Account.</b>
	</div>
	<div style="float:right;margin:10px 10px;">
		<button class="button validate" type="submit" onclick="javascript:gobackads('<?php echo JRoute::_('index.php?option=com_ads&view=gobackads'); ?>');">
		<?php echo JText::_('Back'); ?>
		</button>
		<button class="button validate" type="submit" onclick="javascript:publishads(0);">
			<?php echo JText::_('Save'); ?>
		</button>
		<button class="button validate" type="submit" onclick="javascript:publishads(1);">
			<?php echo JText::_('Publish'); ?>
		</button>
</div>
<div style="clear:both"></div>
</div><!-- style="padding:15px;"> -->
</div><!-- background:#fff -->

</div>
</div>

<form id="publish_ads_form" name="publish_ads_form" method="post" action="<?php echo JRoute::_('index.php?option=com_ads&view=posting_ads'); ?>">
	<input type="hidden" id="publish_ads" name="publish" value="" />
    <input type="hidden" name="project_name" value="<?php echo $this->project_name ?>" />
	<input type="hidden" name="property_id" value="<?php echo $this->property_id ?>" />
    <input type="hidden" name="property_type" value="<?php echo $this->property_type ?>" />
    <input type="hidden" name="property_category" value="<?php echo $this->property_category ?>" />
    <input type="hidden" name="property_name" value="<?php echo $this->property_name ?>" />
    <input type="hidden" name="hdb_town" value="<?php echo $this->hdb_town_id ?>" />
    <input type="hidden" name="flattype" value="<?php echo $this->flattype ?>" />
    <input type="hidden" name="asking_price" value="<?php echo $this->asking_price ?>" />
    <input type="hidden" name="psm_price" value="<?php echo $this->psm_price ?>" />
    <input type="hidden" name="psf_price" value="<?php echo $this->psf_price ?>" />    
    <input type="hidden" name="size" value="<?php echo $this->size ?>" />
    <input type="hidden" name="ad_type" value="<?php echo $this->ad_type ?>" />
    <input type="hidden" name="rental_type" value="<?php echo $this->rental_type ?>" />
    <input type="hidden" name="floor_level" value="<?php echo $this->floor_level ?>" />
    <input type="hidden" name="district" value="<?php echo $this->district ?>" />
    <input type="hidden" name="postcode" value="<?php echo $this->postcode ?>" />
    <input type="hidden" name="tenure" value="<?php echo $this->tenure ?>" />
    <input type="hidden" name="year_built" value="<?php echo $this->year_built ?>" />
    <input type="hidden" name="no_of_hall" value="<?php echo $this->no_of_hall ?>" />
    <input type="hidden" name="no_of_room" value="<?php echo $this->no_of_room ?>" />
    <input type="hidden" name="no_study_room" value="<?php echo $this->no_study_room ?>" />
    <input type="hidden" name="toilet" value="<?php echo $this->toilet ?>" />
    <input type="hidden" name="block_house_no" value="<?php echo $this->block_house_no ?>" />
    <input type="hidden" name="floor_unit_no" value="<?php echo $this->floor_unit_no ?>" />
    <input type="hidden" name="street_name" value="<?php echo $this->street_name ?>" />
    <input type="hidden" name="country" value="<?php echo $this->country ?>" />
    <input type="hidden" name="ad_title" value="<?php echo $this->ad_title ?>" />
    <input type="hidden" name="add_info" value="<?php echo $this->add_info ?>" />
    <input type="hidden" name="renovation" value="<?php echo $this->renovation ?>" />
    <input type="hidden" name="status" value="<?php echo $this->status ?>" />
    <input type="hidden" name="session_id" value="<?php echo $this->session_id; ?>"  />
    
    <?php foreach($this->ihousephotos as $r) : ?>
    <input type="hidden" name="ihousephotos[]" value="<?php echo $r; ?>"  />
    <?php endforeach; ?>
    
    <input type="hidden" name="fp1" value="<?php echo $this->fp1 ?>" />
    <input type="hidden" name="fp2" value="<?php echo $this->fp2; ?>"  />
</form>