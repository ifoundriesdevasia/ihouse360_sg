﻿<?php
/**
 * @version		$Id: view.html.php 11646 2009-03-01 19:34:56Z ian $
 * @package		Joomla
 * @subpackage	iHouse360 Sales posting form
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the iHouse360 component
 *
 * @static
 * @package		Joomla
 * @subpackage	Sales posting form view
 * @since 1.0
 */
class AdsViewPreads extends JView
{
	function display($tpl = null)
	{
		global $mainframe;
		
		$layout = JRequest::getCmd('layout');
		
		$db				= JFactory::getDBO();
		
		$user			=	JFactory::getUser();
		$uid				= $user->id;
		
		
		//$property_id		= JRequest::getVar('property_id');

		$property_type 		= JRequest::getVar('property_type');
		
		
		$property_category 	= JRequest::getVar('property_category');
		$property_id 		= JRequest::getVar('property_name','');
		$project_name		= JRequest::getVar('project_name','');
		
		$flattype			= JRequest::getVar('flattype','');
		
		$asking_price 		= JRequest::getVar('asking_price','');
		
		$psm_price 			= number_format(JRequest::getVar('psm_price',''));
		$psf_price 			= number_format(JRequest::getVar('psf_price',''));
		
		$size 				= JRequest::getVar('size','');
		$ad_type 			= JRequest::getVar('ad_type','');
		$rental_type 		= JRequest::getVar('rental_type','');
		$floor_level 		= JRequest::getVar('floor_level','');
		
		$district 			= JRequest::getVar('district','');
		$postcode 			= JRequest::getVar('postcode','');
		$tenure 			= JRequest::getVar('tenure','');
		$year_built 		= JRequest::getVar('year_built','');
		
		$no_of_hall 		= JRequest::getVar('no_of_hall','');
		$no_of_room 		= JRequest::getVar('no_of_room','');
		$no_study_room 		= JRequest::getVar('no_study_room','');
		$toilet 			= JRequest::getVar('toilet','');
		
		$block_house_no 	= JRequest::getVar('block_house_no','');
		$floor_unit_no 		= JRequest::getVar('floor_unit_no','');
		$street_name 		= JRequest::getVar('street_name','');
		$country 			= JRequest::getVar('country','');
		$ad_title 			= JRequest::getVar('ad_title','');
		$add_info 			= JRequest::getVar('add_info','');
		
		$renovation = JRequest::getVar('renovation','');
		$status		= JRequest::getVar('status','');
		
		$fp1		= JRequest::getVar('fp1','');
		$fp2		= JRequest::getVar('fp2','');
		
		$ihouse_photos_banks = JRequest::getVar('ihousephotos',array());
		
		$this->assignRef('ihousephotos', $ihouse_photos_banks);
		/* HDB */
		
		$hdb_town	= JRequest::getVar('hdb_town','');
		
		$session_id	= JRequest::getVar('session_id','');
		
		$document =& JFactory::getDocument();
		$document->addScript('http://maps.google.com/maps/api/js?sensor=false&region=GB');
		$document->addScriptDeclaration($this->getGoogleMapAPI($postcode));
		
		$query 	= " SELECT name_en,name_ch FROM #__ihouse_property WHERE id = '$property_id ' ";
			$db->setQuery( $query ) ;
			$result = $db->loadObject();
		
		$property_name = $result->name_en;
		if($result->name_ch)
			$property_name .= '&nbsp;' .$result->name_ch;
		
		$psm_price = str_replace(',','', $psm_price);
		$psf_price = str_replace(',','', $psf_price);
		
		$this->assignRef('property_id', $property_id);
		$this->assignRef('property_type', $property_type );
		$this->assignRef('property_category', $property_category );
		$this->assignRef('property_name', $property_name);
		$this->assignRef('project_name', $project_name);
		$this->assignRef('asking_price', $asking_price );
		$this->assignRef('psm_price', $psm_price);
		$this->assignRef('psf_price', $psf_price);
		$this->assignRef('size', $size);
		$this->assignRef('ad_type', $ad_type);
		$this->assignRef('rental_type', $rental_type);
		$this->assignRef('floor_level', $floor_level);
		$this->assignRef('district', $district);
		$this->assignRef('postcode', $postcode);
		$this->assignRef('tenure', $tenure);
		$this->assignRef('year_built', $year_built);
		$this->assignRef('no_of_hall', $no_of_hall);
		$this->assignRef('no_of_room', $no_of_room);
		$this->assignRef('no_study_room', $no_study_room);
		$this->assignRef('toilet', $toilet);
		$this->assignRef('block_house_no', $block_house_no);
		$this->assignRef('floor_unit_no', $floor_unit_no);
		$this->assignRef('street_name', $street_name);
		$this->assignRef('country', $country);
		$this->assignRef('ad_title', $ad_title);
		$this->assignRef('add_info', $add_info);
		$this->assignRef('renovation', $renovation);
		$this->assignRef('status', $status);
		
		$this->assignRef('hdb_town_id', $hdb_town);
		$this->assignRef('flattype', $flattype);
		
		switch($property_type) {
			case 'condo'			: $property_type = '公寓'; break;
			case 'hdb'				: $property_type = '组屋'; break;
			case 'landed'			: $property_type = '有地住宅'; break; 
			case 'commercials'		: $property_type = '商业'; break; // industrial estate
			case 'others'			: $property_type = '其它'; break; // other
			case 'apartments'		: $property_type = '服务公寓'; break; // service apartment
		}
	
		$this->assignRef('property_type',$property_type);
		
		$this->assignRef('session_id', $session_id);
		
		$hasImages = $this->hasImages($ihouse_photos_banks);
		$hasFloorplan = $this->hasFloorplan($fp1, $fp2);
		
		$this->assignRef('hasImages', $hasImages);
		$this->assignRef('hasFloorplan', $hasFloorplan);
		
		$this->assignRef('fp1', $fp1);
		$this->assignRef('fp2', $fp2);
		
		$this->assignRef('fp1_img', $this->getFloorPlan($fp1));
		$this->assignRef('fp2_img', $this->getFloorPlan($fp2));

		parent::display($tpl);
		
	}
	function hasImages($ihouseimage = array()) {
		
		if(!empty($ihouseimage))
			return true;
		
		$db		=& 	JFactory::getDBO();
		
		$session_id	= JRequest::getVar('session_id','');
		
		$query = " SELECT id FROM #__ihouse_ads_image WHERE sess_id = '$session_id' ";
			$db->setQuery( $query );
			$rows = $db->loadObjectList();
		
		if(!empty($rows))
			return true;
			
		return false;
	}
	function hasFloorplan($fp1 , $fp2) {
		if(!empty($fp1))
			return true;
			
		if(!empty($fp2))
			return true;
			
		return false;
	}
	function getPropertyTypeListHTML($selected = '') {
		
		$db		=& 	JFactory::getDBO();

		$query 	= ' SELECT * FROM #__ihouse_property_type WHERE id = 1 OR id = 2 ';
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		$html = '';
		$html .= '<select name="property_type" class="" onchange="javascript:propertyType(this.value);" id="property_type" >';
		$html .= '<option value="">----</option> ';
		foreach($rows as $r):
			$tmp	=	preg_replace('/\s+/','_',strtolower($r->name_en));	
			$html .= '<option value="'.$tmp.'" >'.$r->name_en.'</option>';
		
		endforeach;
       
        $html .= '</select>';
		
		return $html;
	}
	
	function getGoogleMapAPI($postcode){
  		$javascript = "
				var map;
				var geocoder;
		
				function initialize(){
					geocoder = new google.maps.Geocoder();
  					var latlng = new google.maps.LatLng(1.3667,103.8);
  					var options = {
   						zoom: 15,
    					center: latlng,
    					mapTypeId: google.maps.MapTypeId.ROADMAP
  					};
       
  					map = new google.maps.Map(document.getElementById('canvas_map'), options);
					
					
				}
				
				function geocodeGoogleMap() {
					var icon_img = '".JRoute::_('templates/mapsearch/images/building.png')."';	
					
					geocoder.geocode( { 'address' : '".$postcode."'}, function(results, status) {
      					if (status == google.maps.GeocoderStatus.OK) {
        					map.setCenter(results[0].geometry.location);
        					var marker = new google.maps.Marker({
            								map: map, 
            								position: results[0].geometry.location,
											icon : icon_img
        					});
							
      					} else {
							
      					}
    				});
				}
				
				window.addEvent('domready', function(){ 
					initialize();
					setTimeout('geocodeGoogleMap()', 1000);
				});
";
	    return $javascript;
	}
	
	function getFloorPlan($fp) {
		$db		=& 	JFactory::getDBO();
		
		$postcode		=	JRequest::getVar('postcode','');
		
			
		$html = '';
		
		if(!empty($fp)) {
			$tmp 		= 	explode('|', $fp);
			$type_en 	= $tmp[0];
			$name		= $tmp[1];
			
			$floorplan_img = JRoute::_('images/singapore/'.$postcode.'/floorplan/'.$type_en.'/'.$name);
			$html .= '<img src="'.$floorplan_img.'" width="100%" height="100%" />';
		}
		
		return $html;	
	}
}
