﻿<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('behavior.tooltip'); ?>
<?php JHTML::_('script', 'ihouse.js', 'components/com_ads/rokbox/'); ?>

<style type="text/css">
#tablist {
	border-left:3px solid #FF8200;
	border-bottom:3px solid #FF8200;
	border-right:3px solid #FF8200;
	width:99.1%;
	padding:0;
	margin:0;
	min-height:300px;
	height:auto;
	float:left;
}

#tablist1 {
	width:99.1%;
	padding:0;
	margin:0;
	min-height:300px;
	height:auto;
	float:left;
}

#tablist2 {
	border-left:2px solid #eeeeee;
	border-bottom:2px solid #eeeeee;
	border-right:2px solid #eeeeee;
	width:99.3%;
	padding:0;
	margin:0;
	/*min-height:100px;*/
	height:auto;
	float:left;
}

table#prop_det_info td{
	border:1px solid #999;
}

ul.tab_nav3 {
list-style-image:none;
list-style-type:none;
}
</style>

<!--Start View-->
<div style="margin-top:-20px;">

<div id="main-search-header-text" style="padding-left:15px;border-width:2px;">会员中心&nbsp;<span style="font-size:15px;font-weight:100;"><i><?php echo $this->users->user_category ?>&nbsp;Centre</i></span></div>

<div style="padding:15px;"><!--start-->

<div id="menu_list_profile_detail">
<ul id="" class="tab_nav5">     
	<li id="tab_li5_account"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=account_info&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">账号信息</span></a></li>
     <?php
	if(($this->users->user_category == 'Agent')||($this->users->user_category == 'Owner')||($this->users->user_category == 'Individual')){
	?>
	<li id="tab_li5_profile"><a href="<?php echo JURI::root();?>index.php?option=com_user&view=profiler&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">个人资料</span></a></li>
    <?php
	}
	if(($this->users->user_category == 'Agent')||($this->users->user_category == 'Owner')){
	?>
	<li id="tab_li5_ads" class="tabactive5"><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=ads_management&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">房地产广告</span></a></li>
    <?php
	}
	if($this->users->user_category == 'Agent'){
	?>
    <li id="tab_li5_transaction"><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=transaction&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">交易记录</span></a></li>
    <li id="tab_li5_top_up" ><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=top_up&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">充值</span></a></li>
    <?php
	}
	?>
</ul>   
</div>

<div id="tablist">   

<div style="padding: 10px;">
            <div>
            	 
<div id="menu_list_ads_listing">
<ul id="" class="tab_nav77">     
	<li id="tab_li77_listing"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=ads_management&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Listing</span></a></li>
   
	<li id="tab_li77_guru" class="tabactive77"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=the_guru&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">The Guru</span></a></li>
   
    <li id="tab_li77_specialist"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=postads&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Place New Ads</span></a></li>
    
</ul>   
</div><!--menu_list_profile_detail-->


<div id="tablist1"> 
<div id="tab_guru" class="tabular2">

<?php
if($this->page_id == '2'){ // the guru
?>
<form action="index.php?option=com_ads" method="post" name="form2" id="form2">  
    <div id="tab_account" class="tabular1">
    	<div style="padding:10px;">
            <div id="static-text">
                    	<font style="color:#494949;">Subscription to <?php echo $this->rows[0]->name;?></font>
            </div>
            
            <div style="padding-bottom:20px;" id="selection_div">
            <br /><br />You have chosen <?php echo $this->rows[0]->name;?> at $<?php echo round($this->rate);?>.<br />
            All subscriptions are given <?php echo (($this->discount)*100);?>% discount.<br /><br />
            $<?php echo $this->after_discount;?> will be deducted from your credits.
            </div>
            
            <div style="font-weight:bold; float:left; width:660px; padding-top:20px; padding-bottom:20px;">
            Your credits will be deducted upon clicking on the button.
            </div>
            
            <div style="padding-top:10px; padding-bottom:20px; float:left; width:660px;">
                <div style="float:left;">
                    <input type="submit" value="Continue" class="contactbutton" name="agent_pay" id="agent_pay"/>
                </div>
            </div>
        </div>
    </div><!--tabular1-->
    <input type="hidden" name="option" id="option" value="com_ads" />
    <input type="hidden" name="Itemid" id="Itemid" value="126" />
    <input type="hidden" name="view" id="view" value="featured_save" />
    <input type="hidden" name="plan_id" id="plan_id" value="<?php echo $this->plan_id;?>" />
    <input type="hidden" name="property_id" id="property_id" value="<?php echo $this->property_id;?>" />
    <input type="hidden" name="ads_type_config" id="ads_type_config" value="<?php echo $this->ads_type_config;?>" />
</form>
<?php
}
?>


</div><!--tabular2-->

</div><!--tablist1-->

</div>
</div>

</div><!--tablist-->

</div><!--end-->

</div><!--end view-->