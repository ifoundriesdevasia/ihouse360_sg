﻿<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('behavior.tooltip'); ?>

<?php 
	$postcode 	= JRequest::getVar('postcode');
	$id			= JRequest::getVar('id');
?>
<script type="text/javascript">
	/* Do not remove this */
	var postcode = '<?php echo $postcode ?>';
</script>

<?php JHTML::_('script', 'ihouse.js', 'components/com_ihouse/assets/'); ?>
<?php JHTML::_('script', 'rokbox.js', 'components/com_ihouse/assets/rokbox/'); ?>
<?php JHTML::_('script', 'rokbox-config.js', 'components/com_ihouse/assets/rokbox/themes/light/'); ?>
<?php //JHTML::_('script', 'rokbox-style.css', 'components/com_ihouse/assets/rokbox/themes/light/'); ?>
<?php JHTML::stylesheet('rokbox-style.css','components/com_ihouse/assets/rokbox/themes/light/', array('media'=>'all')); ?>

<div style="float:left; width:100%; padding-bottom:15px;background-color:#FFFFFF;">
<div style="float:left; width:100%; color:#fe8700; font-size:16px; font-weight:bold; border-bottom:solid 2px #fb9014;">
	<div style="padding-left:15px; padding-top:10px; padding-bottom:5px;">宏茂桥三房式组屋项目信息 Ang Mo Kio 3 Room Flat Property Information</div>
</div>
</div>


<style type="text/css">

#tablist {
	border-left:3px solid #FF8200;
	border-bottom:3px solid #FF8200;
	border-right:3px solid #FF8200;
	width:99.2%;
	padding:0;
	margin:0;
	min-height:300px;
}
table#prop_det_info td{
	border:1px solid #cfc7c7;
}
</style>
<style type="text/css">
td#table-detail-font {
	color:#45599F;font-size:14px;
}
td.table-detail-font-value {
	font-size:13px;
}

.table_space{
	margin:5px;
	font-weight:bold;
}
</style>

<div id="sub-content-padding" style="background-color:#FFFFFF; padding:15px;"><!--Start view-->

<div style="float:left; width:100%; padding-bottom:20px; color:#5b5b5b; font-size:13px; font-weight:bold;">
	<div style="float:left; height:22px; width:90px;"><img class="icon-img" src="<?php echo JRoute::_('templates/main/images/share.png'); ?>" />&nbsp;分享</div>
    <div style="float:left; height:22px; width:80px;"><img class="icon-img1" src="<?php echo JRoute::_('templates/main/images/print.png'); ?>" />&nbsp;打印</div>
    <div style="float:left; height:22px; width:100px;"><img class="icon-img" src="<?php echo JRoute::_('templates/main/images/compare.png'); ?>" />&nbsp;比比看</div>
    <div style="float:left; height:22px; width:120px;"><img class="icon-img" src="<?php echo JRoute::_('templates/main/images/email.png'); ?>" />&nbsp;电邮给朋友</div>
    <div style="float:left; height:22px; width:190px;"><img class="icon-img" src="<?php echo JRoute::_('templates/main/images/pdf.png'); ?>" />&nbsp;导出为PDF</div>
    <div style="float:left;">浏览次数：3</div>
</div>

<div id="menu_list_property_detail">
<ul id="" class="tab_nav5">     
	<li id="tab_li5_info" class="tabactive5"><a href="javascript:propertyDetailInfoTab('info')"><span class="chinese_text_menu">基本信息</span></a></li>
	<li id="tab_li5_prop_desc"><a href="javascript:propertyDetailInfoTab('prop_desc')"><span class="chinese_text_menu">物业介绍</span></a></li>
    <li id="tab_li5_nearest_station" ><a href="javascript:propertyDetailInfoTab('nearest_station')"><span class="chinese_text_menu">附近地铁站</span></a></li>
    <li id="tab_li5_nearest_school" ><a href="javascript:propertyDetailInfoTab('nearest_school')"><span class="chinese_text_menu">附近学校</span></a></li>
    <li id="tab_li5_user_rating" ><a href="javascript:propertyDetailInfoTab('user_rating')"><span class="chinese_text_menu">用户评分</span></a></li>
</ul>   
</div>

<div id="tablist"> 

	<div id="tab_info" class="tabular1">
    <div style="padding:10px 10px 40px 20px;">
         		<div style="float:left;width:100%;">
                	<div id="static-text" style="float:left;">基本信息</div>
                	<div style="padding-left:5px;float:left;"><span style="font-size:13px;"><i><strong>Basic Information</strong></i></span></div>
                	
                </div>
                
                <div style="clear:both"></div>
                
                <div style="float:left;width:60%; padding-bottom:30px;">
                <table id="prop_det_info" cellspacing=0 cellpadding=0 width="100%">
                <tr style="background-color:#f8f8f8;">
                	<td width="40%" id="table-detail-font"><div class="table_space">物业名</div></td>
                    <td width="60%" class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo $this->data->property_name. " " .$this->data->name_en ?></span></div></td>
                </tr>
                <tr>
                	<td id="table-detail-font"><div class="table_space">地址</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1">Blk <?php echo $this->data->block_no;?><?php echo $this->data->street_name; ?>&nbsp;<?php echo $this->data->postcode ?></span></div></td>
                </tr>
                <tr style="background-color:#f8f8f8;">
                	<td id="table-detail-font"><div class="table_space">类型</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo $this->data->property_type; ?></span></div></td>
                </tr>
                <tr>
                	<td id="table-detail-font"><div class="table_space">售价 (新元)</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo $this->data->ask_price ?></span></div></td>
                </tr>
                <tr style="background-color:#f8f8f8;">
                	<td id="table-detail-font"><div class="table_space">单价 (每平方米新元)</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo $this->data->psm_price ?></span></div></td>
                </tr>
                <tr>
                	<td id="table-detail-font"><div class="table_space">面积 (平方米)</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo $this->data->size ?></span></div></td>
                </tr>
 
                <tr style="background-color:#f8f8f8;">
                	<td id="table-detail-font"><div class="table_space">房间</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo $this->data->no_of_room; ?></span></div></td>
                </tr> 
                
                <tr>
                	<td id="table-detail-font"><div class="table_space">厅</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo $this->data->no_of_hall; ?></span></div></td>
                </tr>
                <tr style="background-color:#f8f8f8;">
                	<td id="table-detail-font"><div class="table_space">书房</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo $this->data->study_room; ?></span></div></td>
                </tr>
                <tr>
                	<td id="table-detail-font"><div class="table_space">卫生间</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo $this->data->toilet; ?></span></div></td>
                </tr>
                <tr style="background-color:#f8f8f8;">
                	<td id="table-detail-font"><div class="table_space">邮区</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo $this->data->property_district_id ?></span></div></td>
                </tr>
                <tr>
                	<td id="table-detail-font"><div class="table_space">邮编</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo $this->data->postcode ?></span></div></td>
                </tr>
                <tr style="background-color:#f8f8f8;">
                	<td id="table-detail-font"><div class="table_space">地契 (年)</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo $this->data->tenure ?></span></div></td>
                </tr>
                <tr>
                	<td id="table-detail-font"><div class="table_space">建筑年份</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo $this->data->top_year ?></span></div></td>
                </tr>
                <tr style="background-color:#f8f8f8;">
                	<td id="table-detail-font"><div class="table_space">楼层</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo $this->data->floor_level; ?></span></div></td>
                </tr>
                <tr>
                	<td id="table-detail-font"><div class="table_space">装修状况</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo $this->data->renovation ?></span></div></td>
                </tr>
                <tr style="background-color:#f8f8f8;">
                	<td id="table-detail-font"><div class="table_space">目前状况</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo $this->data->status_id ?></span></div></td>
                </tr>
                <tr>
                	<td id="table-detail-font"><div class="table_space">更新日期</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo $this->data->posting_date ?></span></div></td>
                </tr>
                
               
                </table>    
    			</div>
                
                <div style="float:right;width:37%;">
                <div> 
            		
            		<div id="static-text1" style="border-bottom:2px solid #062284;">经纪&nbsp;<span style="font-size:13px;"><i><strong>Agent</strong></i></span></div>
                	
                </div>
                <div style="clear:both"></div>
                <div>
					{modulepos ihouse_rentsale_guru}
            	</div>
               <?php /*?> <div style="margin-top:10px;background-color:#fff5e9;">
            		<div style="padding:5px;border:1px #ccc solid; ">Review : Coming Soon
                    <!--
                    	用户评分 : <span id="static-text">75 %</span><br />
                        出售单位 : 20 (查看)<br />     
                        出租单位 : 30 (查看)<br />
                        发表评论 : 10 (查看)<br />
                     -->   
                    </div> 
            	</div><?php */?>
            </div>	
      
                </div>
                <div style="clear:both;"></div>   
            </div>
    </div>
    <div id="tab_prop_desc" class="tabular1" style="display:none;">
    	<div style="padding: 10px;">
            <div>
            	<div style="float:left;border-bottom:2px solid #062284;width:100%;">
            	<div id="static-text" style="float:left;">物业介绍</div>
                <div style="padding-left:5px;float:left;"><span style="font-size:13px;"><i><strong>Property Description</strong></i></span></div>
                </div>
                <div style="clear:both"></div>
                <div style="float:left;margin-top:10px;width:100%;"><!-- width : 400px -->
            	<?php echo $this->data->description ?>
            	</div>
            	<!--<div style="float:right;border-bottom:2px solid #062284;width:37%;">
            		<div id="static-text" style="float:left;">本楼盘置业专家</div>
                	<div style="padding-left:5px;float:left;"><span style="font-size:13px;"><i><strong>The Guru</strong></i></span></div>				{modulepos ihouse_guru}
                </div>
                -->
                <div style="clear:both"></div>
            </div>
        </div>
    </div>
    <div id="tab_nearest_station" class="tabular1" style="display:none;">
    	<div style="padding: 10px;">
            <div>
            	<div style="float:left;border-bottom:2px solid #062284;width:100%;">
            	<div id="static-text" style="float:left;">附近地铁站</div>
                <div style="padding-left:5px;float:left;"><span style="font-size:13px;"><i><strong>Nearest MRT Station</strong></i></span></div>
                </div>
                <div style="clear:both"></div>
                <div style="margin-top:10px;">
            	<?php echo $this->nearby_mrt ?>
            	</div>
            </div>
        </div>
    </div>
    <div id="tab_nearest_school" class="tabular1" style="display:none;">
    	<div style="padding: 10px;">
            <div>
            	<div style="float:left;border-bottom:2px solid #062284;width:100%;">
            	<div id="static-text" style="float:left;">附近学校</div>
                <div style="padding-left:5px;float:left;"><span style="font-size:13px;"><i><strong>Nearest School</strong></i></span></div>
                </div>
                <div style="clear:both"></div>
                <div style="margin-top:10px;">
            	<?php echo $this->nearby_school ?>
            	</div>
            </div>
        </div>
    </div>
    <div id="tab_user_rating" class="tabular1" style="display:none;">
    	<div style="padding: 10px;">
            <div>
            	<div style="float:left;border-bottom:2px solid #062284;width:100%;">
            	<div id="static-text" style="float:left;">用户评分</div>
                <div style="padding-left:5px;float:left;"><span style="font-size:13px;"><i><strong>User Rating</strong></i></span></div>
                </div>
                <div style="clear:both"></div>
                <div style="margin-top:10px;">
            		User Rating : Coming Soon
            	</div>
            </div>
        </div>
    </div>

</div>    


<div style="padding-top:15px;">
<ul class="tab_nav3">     
	<li id="tab_li3_image" class="tabactive3"><a href="javascript:propertyDetailTab('image')"><span class="chinese_text_menu">房源图片</span></a></li>
	<li id="tab_li3_floorplan"><a href="javascript:propertyDetailTab('floorplan')"><span class="chinese_text_menu">户型图</span></a></li>
	<li id="tab_li3_googlemap" ><a href="javascript:propertyDetailTab('googlemap')"><span class="chinese_text_menu">Google 地图</span></a></li>
</ul>
</div>
<div id="propertyInfo12wrap" class="borderPropertyTab">
<div id="propertyInfo12content" style="height:460px;width:657px;">
<div id="property_detail_image" style="margin-bottom:50px;text-align:center;">
	{modulepos postcode_img}
</div>                        

<div id="floor_plan_image" style="display:none;">	
	<?php echo $this->floor_plan_img; ?>
</div>

<div id="google_map_image" style="">
	<div id="canvas_map" style="width:659px;height: 460px;"></div> 
</div>   
</div>
</div>
<?php /*?>{modulepos ihouse_property_highlight}<?php */?>
<div style="clear:both"></div>


<div style="clear:both"></div>
{modulepos ihouse_similar_properties}
<div style="clear:both"></div>
</div><!-- style="padding:15px;"> -->
</div><!-- background:#fff -->

</div>
</div>