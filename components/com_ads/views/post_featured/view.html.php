<?php
/**
 * @version		$Id: view.html.php 11646 2009-03-01 19:34:56Z ian $
 * @package		Joomla
 * @subpackage	iHouse360 Featured Ads Management
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the iHouse360 component
 *
 * @static
 * @package		Joomla
 * @subpackage	 Featured Ads Management view
 * @since 1.0
 */
class AdsViewPost_featured extends JView
{
	function display($tpl = null)
	{
		global $mainframe;
		$db = & JFactory::getDBO();
		
		$ads_id = JRequest::getVar('ads_id', '', 'post', 'ads_id');
		
		$user =& JFactory::getUser();
		$id = $user->get('id');
		
		$query = "SELECT code FROM #__ihouse_district";
		$db->setQuery($query);
		$codes = $db->loadResultArray();
		
		$this->assignRef('users', $this->getInfoOfThisUser($id));
		$this->assignRef('ads', $this->getAds($ads_id));
		$this->assignRef('type4r', $this->gettype4rent());
		$this->assignRef('type4s', $this->gettype4sell());
		$this->assignRef('type4rsub', $this->gettype4rentsubscribe($ads_id));
		$this->assignRef('type4ssub', $this->gettype4sellsubscribe($ads_id));
		$this->assignRef('type5', $this->gettype5slot($ads_id));
		$this->assignRef('type5sub', $this->gettype5slotsubscribe($ads_id));
		$this->assignRef('type6r', $this->gettype6rent());
		$this->assignRef('type6s', $this->gettype6sell());
		$this->assignRef('type6rsub', $this->gettype6rentsubscribe($ads_id));
		$this->assignRef('type6ssub', $this->gettype6sellsubscribe($ads_id));
		$this->assignRef('type7', $this->gettype7slot());
		$this->assignRef('type7sub', $this->gettype7slotsubscribe($ads_id));
		parent::display($tpl);
	}
	
	function getInfoOfThisUser($id = 0) {
		
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT * FROM #__users "
						.	" WHERE id = '$id'" ;
						
			$db->setQuery( $query );
			$list = $db->loadObject();		

		return $list;	
	}
	
	function getAds($ads_id = 0) {
		
		if(!$ads_id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT * FROM #__ihouse_ads "
						.	" WHERE id = '$ads_id'" ;
						
			$db->setQuery( $query );
			$list = $db->loadObject();		
//print_r($list);
		return $list;	
	}
	
	function gettype4rent(){
	
		$db 	  	=& 	JFactory::getDBO();

		$query = "SELECT space FROM #__ihouse_ads_type_slot WHERE ads_type_config = '4' AND property_type = 'rent'";
		$db->setQuery($query);
		$space = $db->loadResult();
		
		return $space;
	
	}//end function
	
	function gettype4sell(){
	
		$db 	  	=& 	JFactory::getDBO();

		$query = "SELECT space FROM #__ihouse_ads_type_slot WHERE ads_type_config = '4' AND property_type = 'sell'";
		$db->setQuery($query);
		$space = $db->loadResult();
		
		return $space;
	
	}//end function
	
	function gettype4rentsubscribe($ads_id = 0){
	
		$db 	  	=& 	JFactory::getDBO();
		$user =& JFactory::getUser();
		$user_id = $user->get('id');
		
		if(!$ads_id)
			return false;

		$query = "SELECT count(*) FROM #__ihouse_ads_type t, #__cbsubs_subscriptions s WHERE t.ads_type_config = '4' AND t.user_id = '".$user_id."' AND t.property_type = 'rent' AND t.ads_id = '".$ads_id."' AND t.subscription_id = s.id AND s.status = 'A'";
		$db->setQuery($query);
		$type4rentsub = $db->loadResult();
		
		return $type4rentsub;
	
	}//end function
	
	function gettype4sellsubscribe($ads_id = 0){
	
		$db 	  	=& 	JFactory::getDBO();
		$user =& JFactory::getUser();
		$user_id = $user->get('id');
		
		if(!$ads_id)
			return false;

		//$query = "SELECT space FROM #__ihouse_ads_type_slot WHERE ads_type_config = '4' AND property_type = 'sell'";
		$query = "SELECT count(*) FROM #__ihouse_ads_type t, #__cbsubs_subscriptions s WHERE t.ads_type_config = '4' AND t.user_id = '".$user_id."' AND t.property_type = 'sell' AND t.ads_id = '".$ads_id."' AND t.subscription_id = s.id AND s.status = 'A'";
		$db->setQuery($query);
		$type4sellsub = $db->loadResult();
		
		return $type4sellsub;
	
	}//end function
	
	function gettype5slot($ads_id = 0){
	
		$db 	  	=& 	JFactory::getDBO();
		
		if(!$ads_id)
			return false;
		
		$query = "SELECT property_id FROM #__ihouse_ads WHERE id = '".$ads_id."'";
		$db->setQuery($query);
		$property_id = $db->loadResult();
	
		$query = "SELECT space FROM #__ihouse_ads_type_slot WHERE ads_type_config = '5' AND property_id = '".$property_id."'";
		$db->setQuery($query);
		$space = $db->loadResult();
		
		return $space;
	
	}//end function
	
	function gettype5slotsubscribe($ads_id = 0){
	
		$db 	  	=& 	JFactory::getDBO();
		$user =& JFactory::getUser();
		$user_id = $user->get('id');
		
		if(!$ads_id)
			return false;
		
		$query = "SELECT property_id FROM #__ihouse_ads WHERE id = '".$ads_id."'";
		$db->setQuery($query);
		$property_id = $db->loadResult();
	
		//$query = "SELECT space FROM #__ihouse_ads_type_slot WHERE ads_type_config = '5' AND property_id = '".$property_id."'";
		$query = "SELECT count(*) FROM #__ihouse_ads_type t, #__cbsubs_subscriptions s WHERE t.ads_type_config = '5' AND t.user_id = '".$user_id."' AND t.property_id = '".$property_id."' AND t.ads_id = '".$ads_id."' AND t.subscription_id = s.id AND s.status = 'A'";
		$db->setQuery($query);
		$type5sub = $db->loadResult();
		
		return $type5sub;
	
	}//end function
	
	function gettype6rent(){
	
		$db 	  	=& 	JFactory::getDBO();

		$query = "SELECT space FROM #__ihouse_ads_type_slot WHERE ads_type_config = '6' AND property_type = 'rent'";
		$db->setQuery($query);
		$space = $db->loadResult();
		
		return $space;
	
	}//end function
	
	function gettype6sell(){
	
		$db 	  	=& 	JFactory::getDBO();

		$query = "SELECT space FROM #__ihouse_ads_type_slot WHERE ads_type_config = '6' AND property_type = 'sell'";
		$db->setQuery($query);
		$space = $db->loadResult();
		
		return $space;
	
	}//end function
	
	function gettype6rentsubscribe($ads_id = 0){
	
		$db 	  	=& 	JFactory::getDBO();
		$user =& JFactory::getUser();
		$user_id = $user->get('id');
		if(!$ads_id)
			return false;

		$query = "SELECT count(*) FROM #__ihouse_ads_type t, #__cbsubs_subscriptions s WHERE t.ads_type_config = '6' AND t.user_id = '".$user_id."' AND t.property_type = 'rent' AND t.ads_id = '".$ads_id."' AND t.subscription_id = s.id AND s.status = 'A'";
		$db->setQuery($query);
		$type6rentsub = $db->loadResult();
		
		return $type6rentsub;
	
	}//end function
	
	function gettype6sellsubscribe($ads_id = 0){
	
		$db 	  	=& 	JFactory::getDBO();
		$user =& JFactory::getUser();
		$user_id = $user->get('id');
		if(!$ads_id)
			return false;

		//$query = "SELECT space FROM #__ihouse_ads_type_slot WHERE ads_type_config = '6' AND property_type = 'sell'";
		$query = "SELECT count(*) FROM #__ihouse_ads_type t, #__cbsubs_subscriptions s WHERE t.ads_type_config = '6' AND t.user_id = '".$user_id."' AND t.property_type = 'sell' AND t.ads_id = '".$ads_id."' AND t.subscription_id = s.id AND s.status = 'A'";
		$db->setQuery($query);
		$type6sellsub = $db->loadResult();
		
		return $type6sellsub;
	
	}//end function
	
	function gettype7slot(){
	
		$db 	  	=& 	JFactory::getDBO();
		

		$query = "SELECT space FROM #__ihouse_ads_type_slot WHERE ads_type_config = '7'";
		$db->setQuery($query);
		$space = $db->loadResult();
		
		return $space;
	
	}//end function
	
	function gettype7slotsubscribe($ads_id = 0){
	
		$db 	  	=& 	JFactory::getDBO();
		$user =& JFactory::getUser();
		$user_id = $user->get('id');
		
		if(!$ads_id)
			return false;

		//$query = "SELECT space FROM #__ihouse_ads_type_slot WHERE ads_type_config = '7'";
		$query = "SELECT count(*) FROM #__ihouse_ads_type t, #__cbsubs_subscriptions s WHERE t.ads_type_config = '7' AND t.user_id = '".$user_id."' AND t.ads_id = '".$ads_id."' AND t.subscription_id = s.id AND s.status = 'A'";
		$db->setQuery($query);
		$type7sub = $db->loadResult();
		
		return $type7sub;
	
	}//end function
	
	/*function getAdsListing($id = 0) {
		if(!$id)
			return false;
		
		$db 	  	=& 	JFactory::getDBO();
		
		$query="SELECT * FROM #__ihouse_ads WHERE posted_by = '".$id."'";
		$db->setQuery($query);
		$list = $db->loadObjectList();
		
		return $list;	
	}
	
	function checkSubscribe($id = 0) {
		if(!$id)
			return false;
		
		$db 	  	=& 	JFactory::getDBO();
		
		$query="SELECT count(*) FROM #__ihouse_ads_type WHERE ads_type_config = '3' AND user_id = '".$id."'";
		$db->setQuery($query);
		$subscribe = $db->loadResult();
		
		return $subscribe;	
	}
	

	function getCSCredit() {
		
		$db 	  	=& 	JFactory::getDBO();
		
		$query="SELECT rate FROM #__cbsubs_plans WHERE id = '7'";
		$db->setQuery($query);
		$rate = $db->loadResult();
		
		return $rate;	
	}

	function getCSSlot() {
		
		$db 	  	=& 	JFactory::getDBO();
		
		$query="SELECT space FROM #__ihouse_ads_type_slot WHERE ads_type_config = '3'";
		$db->setQuery($query);
		$space = $db->loadResult();
		
		return $space;	
	}
	
	
	
	function UserCredits($id = 0) {
		
		if(!$id)
			return false;
			
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT credit FROM #__ihouse_users_credit "
						.	" WHERE user_id = '$id'" ;
						
			$db->setQuery( $query );
			$credit = $db->loadResult();		

		return $credit;	
	}
	
	function GuruCredits() {

		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT rate FROM #__cbsubs_plans "
						.	" WHERE id = '5'" ;
						
			$db->setQuery( $query );
			$guru = $db->loadResult();		

		return $guru;	
	}
	
	function SpecialistCredits() {

		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT rate FROM #__cbsubs_plans "
						.	" WHERE id = '6'" ;
						
			$db->setQuery( $query );
			$specialist = $db->loadResult();		

		return $specialist;	
	}
	
	function CondoSpecialistCredits() {
	
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT rate FROM #__cbsubs_plans "
						.	" WHERE id = '7'" ;
						
			$db->setQuery( $query );
			$cspecialist = $db->loadResult();		
			
		return $cspecialist;	
	}*/
}
