<?php
/**
 * @version		$Id: view.html.php 11646 2009-03-01 19:34:56Z ian $
 * @package		Joomla
 * @subpackage	Credit Management
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Users component
 *
 * @static
 * @package		Joomla
 * @subpackage	Credit Management
 * @since 1.0
 */
class AdsViewCredit_management extends JView
{
	function display($tpl = null)    
	{
		global $mainframe;
		$db = & JFactory::getDBO();
		
		$user_id	= JRequest::getVar('user_id', '', 'post', 'user_id');
		$plan_id	= JRequest::getVar('plan_id', '', 'post', 'plan_id');
		$type_iv	= JRequest::getVar('type_iv', '', 'post', 'type_iv');
		$type_v		= JRequest::getVar('type_v', '', 'post', 'type_v');
		$type_vi	= JRequest::getVar('type_vi', '', 'post', 'type_vi');
		$type_vii	= JRequest::getVar('type_vii', '', 'post', 'type_vii');
		$ads_type_config	= JRequest::getVar('ads_type_config', '', 'post', 'ads_type_config');
		
		$query = "SELECT params FROM #__ihouse_promotion WHERE id = '2'";
	
		$db->setQuery($query);
		$params = $db->loadResult();
		
		$discountone = explode("=", $params);
		$discount = $discountone[1];
		
		if($plan_id == 'type4'){
		$plan_id = $type_iv;
		}
		
		if($plan_id == 'type5'){
		$plan_id = $type_v;
		}
		
		if($plan_id == 'type6'){
		$plan_id = $type_vi;
		}
		
		if($plan_id == 'type7'){
		$plan_id = $type_vii;
		}
		
		$query = "SELECT credit FROM #__ihouse_users_credit WHERE user_id = '".$user_id."'";
	
		$db->setQuery($query);
		$credit = $db->loadResult(); //user's credit
		
		$query = "SELECT rate FROM #__cbsubs_plans WHERE id = '".$plan_id."'";
	
		$db->setQuery($query);
		$sub_credit = $db->loadResult(); //subscription's credit
		
		
		
		$query = "SELECT name FROM #__cbsubs_plans WHERE id = '".$plan_id."'";
	
		$db->setQuery($query);
		$sub_name = $db->loadResult(); //subscription's credit

		
		$this->assignRef('discount'	, 	$discount );
		$this->assignRef('user_id'	, 	$user_id );
		$this->assignRef('sub_credit'	, 	$sub_credit );
		$this->assignRef('credit'	, 	$credit );
		$this->assignRef('sub_name'	, 	$sub_name );
		$this->assignRef('plan_id'	, 	$plan_id );
		$this->assignRef('ads_type_config'	, 	$ads_type_config );
		parent::display($tpl);
	}
	
}
