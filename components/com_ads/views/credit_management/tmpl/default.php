<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('behavior.tooltip'); ?>

<?php
if(($this->plan_id == '11')||($this->plan_id == '15')||($this->plan_id == '19')||($this->plan_id == '23')){
		$sub_credit = ((round($this->sub_credit)) * (1 - ($this->discount)));
		} else {
		$sub_credit = (round($this->sub_credit));
}


$actual_amt = $sub_credit - ($this->credit);
?>

<div><!--Start view-->
<div id="reg_border">
			<div id="reg_header">
			Credits
            </div>
</div>

<div style="padding:20px;">

<div style="padding-bottom:5px;"><b>Credits Balance</b> : <?php echo $this->credit; ?></div>
<div style="padding-bottom:5px;"><b>Ad Type</b> : <?php echo $this->sub_name; ?></div>
<div style="padding-bottom:5px;"><b>Credits Needed</b> : <?php echo $sub_credit; ?></div>


<form method="post" name="ads_form" id="ads_form" action="<?php echo JURI::root(); ?>index.php?option=com_ads">
<div style="padding-bottom:5px; padding-top:10px;">
<?php 
if($this->credit >= $sub_credit){ //enough credits
	if(($this->plan_id == '2')||($this->plan_id == '3')){
?>
<?php echo $sub_credit; ?> credits will be deducted from your credit balance. Click on the button to proceed for deduction.
<br /><br />
<input type="submit" value="Proceed" name="decredit" id="decredit" onClick="javascript:deductcredit(this);" />
<?php
	} else if(($this->plan_id == '7')){
?>
<?php echo $sub_credit; ?> credits will be deducted from your credit balance. Click on the button to proceed for deduction and posting.
<br /><br />
<input type="submit" value="Proceed" name="t_three" id="t_three" onClick="javascript:typethree(this);" />
<?php
	} else if(($this->plan_id == '1')||($this->plan_id == '4')||($this->plan_id == '25')){
?>
<?php echo $sub_credit; ?> credits will be deducted from your credit balance after posting of ads. Click on the button to proceed.
<br /><br />
<input type="submit" value="Proceed" name="postads" id="postads" onClick="javascript:post_ads(this);" />
<?php
	} else {
?>
<?php echo $sub_credit; ?> credits will be deducted from your credit balance after posting of featured ads. Click on the button to proceed.
<br /><br />
<input type="submit" value="Proceed" name="postfeatured" id="postfeatured" onClick="javascript:post_featured(this);" />
<?php
	}
} else {
?>
You do not have enough credits to proceed.<br /><br />
Select the top-up amount and click on Buy to purchase more credits or Cancel to go back to Ad Type Selection.<br /><br />
<select name="top_up_amt" id="top_up_amt">
<option value="">Please Select</option>
<option value="20">$20</option>
<option value="50">$50</option>
<option value="100">$100</option>
<option value="300">$300</option>
<option value="500">$500</option>
<option value="<?php echo $actual_amt; ?>">Actual Amount - $<?php echo $actual_amt; ?></option>
</select><br /><br />
<input type="submit" value="Buy" name="buy_credit" id="buy_credit" onClick="javascript:buycredit(this);" />&nbsp;
<input type="submit" value="Cancel" name="cancel_but" id="cancel_but" onClick="javascript:goback(this);" />
<?php
}
?>
<input name="option" type="hidden" value="com_ads" />
<input type="hidden" name="view" id="view" value="" />
<input type="hidden" name="user_id" id="user_id" value="<?php echo $this->user_id; ?>" />
<input type="hidden" name="plan_id" id="plan_id" value="<?php echo $this->plan_id; ?>" />
<input type="hidden" name="credit" id="credit" value="<?php echo $this->credit; ?>" />
<input type="hidden" name="sub_credit" id="sub_credit" value="<?php echo $sub_credit; ?>" />
<input type="hidden" name="payment_mode" id="payment_mode" value="credits" />
<input type="hidden" name="ads_type_config" id="ads_type_config" value="<?php echo $this->ads_type_config; ?>" />
</div>
<script type="text/javascript">

						function deductcredit(submit) {
							var a = "deduct_credit";
							document.getElementById('view').value = a;
						}
						
						function typethree(submit) {
							var a = "type_three";
							document.getElementById('view').value = a;
						}
						
						function post_featured(submit) {
							var a = "postfeatured";
							document.getElementById('view').value = a;
						}
						
						function post_ads(submit) {
							var a = "postads";
							document.getElementById('view').value = a;
						}
						
						function buycredit(submit) {
							var a = "paymentprocess";
							document.getElementById('view').value = a;
						}
						
						function goback(submit) {
							var a = "ads_selection";
							document.getElementById('view').value = a;
						}
</script>

</form>

</div>
</div><!--End view-->