﻿<?php
/**
 * @version		$Id: view.html.php 11646 2009-03-01 19:34:56Z ian $
 * @package		Joomla
 * @subpackage	iHouse360 Sales posting form
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the iHouse360 component
 *
 * @static
 * @package		Joomla
 * @subpackage	Sales posting form view
 * @since 1.0
 */
class AdsViewPostads extends JView
{
	function display($tpl = null)
	{
		global $mainframe;
		
		$layout = JRequest::getCmd('layout');
		
		$user 			=& 	JFactory::getUser();
		$uid			= $user->id;
		$user 			=& 	JFactory::getUser($uid);
		
		//$this->assignRef('fp1',$this->getFloorPlanHTML('fp1',$row->postcode));
		//$this->assignRef('fp2',$this->getFloorPlanHTML('fp2',$row->postcode));
		$sess_id	=	md5(time());
		
		$this->assignRef('session_ids', $sess_id);
		$this->assignRef('watermark_text', $this->getWatermarkText($user));
		$this->assignRef('property_type',$this->getPropertyTypeListHTML($sess_id, $user));

		parent::display($tpl);
		
	}
	function getWatermarkText($user) {
		$db		=& 	JFactory::getDBO();
		$uid	=	$user->get('id');
		
		$query 	= ' SELECT word FROM #__ihouse_watermark WHERE user_id = '. $uid;
			$db->setQuery( $query ) ;
			$result = $db->loadResult();
		
		return $result;
	}
	function getPropertyTypeListHTML($sess_id, $user) {
		
		$db		=& 	JFactory::getDBO();
		
		$where	=	array();
		if($user->user_category == 'Agent') {
			$where[] = ' id = 1 ';
			$where[] = ' id = 2 ';
			$where[] = ' id = 3 ';
			$where[] = ' id = 4 ';
			$where[] = ' id = 6 ';
		} else if($user->user_category == 'Owner') {
			$where[] = ' id = 1 ';
			$where[] = ' id = 2 ';
		}
		
		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' OR ', $where ) : '' );
		
		$query 	= ' SELECT * FROM #__ihouse_property_type '. $where;
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		$html = '';
		$html .= '<select name="property_type" class="" onchange="javascript:propertyType(this.value,\''.$sess_id.'\');" id="property_type" >';
		$html .= '<option value="">----</option> ';
		foreach($rows as $r):
			$tmp	=	preg_replace('/\s+/','_',strtolower($r->name_en));	
			
			
			$html .= '<option value="'.$tmp.'" >'.$r->label.'</option>';
		endforeach;
       
        $html .= '</select>';
		
		return $html;
	}
}
