﻿<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('behavior.tooltip'); ?>

<?php JHTML::_('script', 'ajaxupload.js', 'components/com_ads/assets/'); ?>
<?php JHTML::_('script', 'postads.js', 'components/com_ads/assets/'); ?>
<?php JHTML::_('script', 'validation_postads.js', 'components/com_ads/assets/'); ?>


<script type="text/javascript" src="<?php echo JURI::base(true).'/components/com_ads/assets/swfobject.js' ?>"></script>
<script type="text/javascript" src="<?php echo JURI::base(true).'/components/com_ads/assets/jquery.uploadify.v2.1.4.js' ?>"></script>

<script type="text/javascript">
window.addEvent('domready', function() {
	$('property_type').setProperty('value','');
});

function adType(value) {
	
	if(value == 'rent') {
		$('rental_type').setProperty('disabled','');
		$('status_text').setProperty('value','待租');
	} else if(value == 'sell'){
		$('rental_type').setProperty('disabled','disabled');
		$('rental_type').setProperty('value','');
		$('status_text').setProperty('value','待售');
	} else {
		$('rental_type').setProperty('disabled','disabled');
		$('rental_type').setProperty('value','');
		$('status_text').setProperty('value','');
	}
	
}

function ajaxUpload(form) {
	$('message_tmp').setHTML('');
	var str = $('imgFile').getProperty('value');
	//document.loadImageForm.submit();
	/* CHECK FILENAME */
	//if(/^[^\!\@\#\$\%\^\*]+(\.[^\!\@\#\$\%\^\*]+)+$/.test(str)) {
		return AIM.submit(form, 
					  {
						'onStart'		:   runThis,
					  	'onComplete' 	: 	loadThis
		});
	//} else {
		//$('message_tmp').setHTML('<span style="color:red;">"!@#$%^*" is restricted. Please change your filename</span>');
	//}
}

function runThis() {
	$('pic_uploading_loader').setStyle('display','block');
}

function loadThis() {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();		
	
	$('pic_uploading_loader').setStyle('display','block');
	
	var url = 'index.php?option=com_ads&view=ajaxLoadAdsTempImage&r=' + unixtime_ms;	
	
	var req = new Ajax(url, {
	   					data		: 	
									{
										'session_id' : '<?php echo $this->session_ids ?>'
									},
						method		: "get",
		    			onSuccess	: function(data) {
						
							var obj1 		= Json.evaluate(data);
							var tmp 		= obj1.html;
							
							$('loadAdsImagesPlace').setHTML(tmp);
							
							$('pic_uploading_loader').setStyle('display','none');
		   				},
						async: true,
						evalScripts : true
				}).request();
}

function watermarkOption(value) {
	$('watermark_rad').setProperty('value', value);
}

function watermarkText(value) {
	$('watermark_textinput').setProperty('value', value);
}

</script>

<script type="text/javascript">

var sess_id = '<?php echo $this->session_ids ?>';
var watermark_use = 1;
var watermark_txt = '<?php echo $this->watermark_text ?>';

jQuery(document).ready(function() {
  jQuery(':radio[name=watermark_radio]').click(function() {
	 if(this.value == 1) {
		 watermark_use = 1;
	 } else {
		 watermark_use = 0;
	 }
	 
	 jQuery("#file_upload").uploadifySettings(
        'scriptData',
        {'watermark_use': watermark_use}
     );

  });								

  jQuery('input[name=watermark_text]').keyup(function() {
	 watermark_txt = this.value;
	 
	 jQuery("#file_upload").uploadifySettings(
        'scriptData',
        {'wmrk_txt': watermark_txt}
     );												  
  });
  
  
  jQuery('#file_upload').uploadify({
    'uploader'  : '<?php echo JURI::base(true).'/components/com_ads/assets/uploadify.swf' ?>',
	'script'	: '<?php echo JURI::base(true).'/uploadify.php' ?>' ,
    'cancelImg' : '<?php echo JURI::base(true).'/components/com_ads/assets/cancel.png' ?>',
    'folder'    : '<?php echo JURI::base(true).'/images/ihouse/ads_images/'.$this->session_ids ?>',
	'scriptData'  : 
		{
			'sess_id':'<?php echo $this->session_ids ?>',
		 	'wmrk_txt': watermark_txt,
			'watermark_use': watermark_use
		},
    'auto'      : true,
	'multi'		: true,
	'width' 	: 60,
	'height' 	: 22,
	'method'	: 'get',
	'sizeLimit'   : 5000000,
	'buttonImg'   : '<?php echo JRoute::_('templates/main/images/btn-upload.png') ?>',
	'buttonText'   : 'Upload',
	'fileExt'		: '*.jpg;*.gif;*.png',
	'fileDesc'		: 'Image Files',
	'removeCompleted' : true,
	'onComplete'  : function(event, ID, fileObj, response, data) {
      				/*if (response !== '1')
                    {
                        alert(response);
                    }
                    else
                    {
                        alert('Filename: ' + fileObj.name + ' was uploaded');
                    }*/
    },
	'onError' : function (event,ID,fileObj,errorObj) {
      //alert(errorObj.type + ' Error: ' + errorObj.info);
    },
	'onAllComplete' : function(event,data) {
      //alert(data.filesUploaded + ' files uploaded successfully!');
	  loadThis();
    }
  });
});

</script>
<style type="text/css">
#tablist {
	border-left:3px solid #FF8200;
	border-bottom:3px solid #FF8200;
	border-right:3px solid #FF8200;
	width:99.2%;
	padding:0;
	margin:0;
	min-height:300px;
	height:auto;
	float:left;
}

#file_uploadQueue{width:100%;}
.uploadifyQueueItem {
	background-color: #F5F5F5;
	border: 2px solid #E5E5E5;
	font: 11px Verdana, Geneva, sans-serif;
	margin-top: 5px;
	padding: 10px;
}
.uploadifyError {
	background-color: #FDE5DD !important;
	border: 2px solid #FBCBBC !important;
}
.uploadifyQueueItem .cancel {
	float: right;
}
.uploadifyQueue .completed {
	background-color: #E5E5E5;
}
.uploadifyProgress {
	background-color: #E5E5E5;
	margin-top: 10px;
	width: 100%;
}
.uploadifyProgressBar {
	background-color: #0099FF;
	height: 3px;
	width: 1px;
}
</style>
<?php
	$user 			=& JFactory::getUser();
	$uid			=	$user->id;
	$user 			=& JFactory::getUser($uid);
	$user_category 	= $user->get('user_category');
?>          	 
<div style="margin-top:-20px;">

<div id="main-search-header-text" style="padding-left:15px;border-width:2px;">
	<?php if($user_category == 'Agent') : ?>经纪中心<?php endif; ?>
    <?php if($user_category == 'Owner') : ?>屋主中心<?php endif;?>
	&nbsp;<span style="font-size:15px;font-weight:100;"><i><?php echo $user_category ?>&nbsp;Centre</i></span>
</div>


<div style="padding:0 10px">


	<div id="menu_list_profile_detail">
		<ul id="" class="tab_nav5">   
			<li id="tab_li5_account"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=account_info&Itemid=126&id=<?php echo $user->id;?>"><span class="chinese_text_menu">Acct. Info</span></a></li>
    <?php
	if(($user->user_category == 'Agent')||($user->user_category == 'Owner')||($user->user_category == 'Individual')){
	?>
			<li id="tab_li5_profile"><a href="<?php echo JURI::root();?>index.php?option=com_user&view=profiler&Itemid=126&id=<?php echo $user->id;?>"><span class="chinese_text_menu">Profile</span></a></li>
    <?php
	}
	if(($user->user_category == 'Agent')||($user->user_category == 'Owner')){
	?>
			<li id="tab_li5_ads" class="tabactive5"><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=ads_management&Itemid=126&id=<?php echo $user->id;?>"><span class="chinese_text_menu">Your Ads</span></a></li>
    <?php
	}
	if($user->user_category == 'Agent'){
	?>
    		<li id="tab_li5_transaction"><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=transaction&Itemid=126&id=<?php echo $user->id;?>"><span class="chinese_text_menu">Transactions</span></a></li>
    		<li id="tab_li5_top_up" ><a href="<?php echo JURI::root();?>index.php?option=com_ads&view=top_up&Itemid=126&id=<?php echo $user->id;?>"><span class="chinese_text_menu">Top-up</span></a></li>
    <?php
	}
	?>
    <li id="tab_li77_specialist"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=favoriteads&Itemid=126&id=<?php echo $this->users->id;?>"><span class="chinese_text_menu">Favorites</span></a></li>
		</ul>   
	</div>

	<div id="tablist">

		<div style="background-color:#fff;width:100%;float:left;margin-top:10px;">
			<div style="margin:10px auto; width:97%;">
			<!--Start-->
    			<div id="menu_list_ads_listing">
					<ul class="tab_nav77" id="">     
						<li  id="tab_li77_listing"><a href="<?php echo JRoute::_('index.php?option=com_ads&amp;view=ads_management&amp;Itemid=126&amp;id='. $user->id); ?>"><span class="chinese_text_menu">Listing</span></a></li>
                        <?php if($user_category == 'Agent') : ?>
    					<li id="tab_li77_guru"><a href="<?php echo JRoute::_('index.php?option=com_ads&amp;view=the_guru&amp;Itemid=126&amp;id='. $user->id); ?>"><span class="chinese_text_menu">The Guru</span></a></li>
                        <?php endif; ?>
   	    				<li id="tab_li77_specialist" class="tabactive77"><a href="<?php echo JRoute::_('index.php?option=com_ads&amp;view=postads&amp;Itemid=126&amp;id='. $user->id); ?>"><span class="chinese_text_menu">Place New Ads</span></a></li>
 <?php
	if($user_category == 'Agent'){
	?>
     <li id="tab_li77_specialist"><a href="<?php echo JURI::root(); ?>index.php?option=com_ads&view=renewal&Itemid=126&id=<?php echo $user->id;?>"><span class="chinese_text_menu">My Featured Ads Renew</span></a></li>
    <?php
	}
	?>    
					</ul>   
				</div><!--menu_list_profile_detail-->
    
	<div style="border-width:2px;" id="main-search-header-text">
		<span style="font-size: 15px; font-weight: 100;"><i>Posting Ads</i></span>
	</div>
	<div id="tabcont-ads" >
		<form name="edit_refresh_ads_form" id="edit_refresh_ads_form" method="post" enctype="multipart/form-data" action="<?php echo JRoute::_('index.php?option=com_ads&view=posting_ads') ?>">
        	<div>
				<label id="property_typemsg" for="property_type">
				Property Type *
			   	</label>
				<div style="padding-top:5px;">
                	<div style="float:left">
					<?php echo $this->property_type ?>
                    </div>
                    <div id="prop_type_loader">
                    	<img src="<?php echo JRoute::_('templates/main/images/ajax-loader.gif'); ?>"  />
                    </div>
                    <div style="clear:both"></div>
				</div>
                
		 		<div id="property_type_table" style="margin:5px 0 5px 0;width:98%;">
				</div>
                
			</div>
            
            <span id="property_id"></span>
            
            <!-- will be ajax -->
            <div id="floorplan_box_postads" style="display:none;width:98%;">
				<div style="background-color:#ff8200;color:#fff;height:37px;">
					<div style="float:left;padding:10px;"><strong>Floor Plan</strong></div>
					<div style="clear:both"></div>
				</div>
                
                <div id="whole_fp_box">
                	<div style="padding:5px">
                	No Floorplan for this particular property
                    </div>
				</div>
                
             
				<div id="test"></div>

				<div style="background-color:#ff8200;color:#fff;height:37px;">
					<div style="float:left;padding:10px;"><strong>Image from iHouse</strong></div>
					<div style="clear:both;"></div>
				</div>

				<div>
            	
					<div id="ihouseImage">
						<i>This will be loaded after you choose <b>PropertyName</b> on Condo</i>
					</div>
                	<div id="ihouseImage_loader" style="float:left;display:none;">
                    	<img src="<?php echo JRoute::_('templates/main/images/ajax-loader.gif'); ?>"  />
                	</div>
					<div style="clear:both"></div>
           		</div>
            </div>    
            
            
            <input type="hidden" name="plan_id" value="<?php echo $this->plan_id ?>" />
			<input type="hidden" name="task" value="" />
			<input type="hidden" name="option" value="com_ads" />
			<input type="hidden" name="session_id" value="<?php echo $this->session_ids ?>" />
    
			<input type="hidden" name="agree1" id="t-agree1" value="0" />
            
			<?php if($user_category == 'Agent') : ?>
			<input type="hidden" name="agree2" id="t-agree2" value="0" />
            <?php endif; ?>
			
			<?php echo JHTML::_( 'form.token' ); ?>
        </form>      
        <div id="upload_box_postads" style="display:none;width:98%;">    
        	<div style="background-color:#ff8200;color:#fff;height:37px;">
				<div style="float:left;padding:10px;"><strong>Current Photos</strong></div>
				<div style="float:right;margin-right:10px;">
					<div style="overflow:hidden;;text-align:center;position:relative;padding:7px 0;">
                    
                    <!--
                        <form name="loadImageForm" enctype="multipart/form-data" method="post" id="loadImageForm" action="<?php echo JRoute::_('index.php?option=com_ads&view=ajaxSave') ?>">
					    	<img src="<?php echo JRoute::_('templates/main/images/btn-upload.png') ?>" />
				    		<input class="imgFileInput" id="imgFile" name="imgFile[]" type="file" onchange="ajaxUpload(this.form);" multiple="multiple" />
                            <input type="hidden" name="session_id" value="<?php echo $this->session_ids ?>" />
                            <input type="hidden" name="watermark_use" id="watermark_rad" value="1"  />
                            <input type="hidden" name="wmrk_txt" id="watermark_textinput" value="<?php echo $this->watermark_text ?>" />
						</form>       
                        -->
				   	</div>
				</div>
				<div style="clear:both"></div>
			</div>
            <div style="margin:5px 0">
            	<div style="position:relative;">
                	<div style="">
            			<input id="file_upload" type="file" name="imgFile[]" />
                    </div>
                    <div style="position:absolute;left:70px;top:3px;"> (Maximum upload file size <= 5MB)
                    </div>
                    <div class="clear"></div>
                </div>    
                <div class="clear"></div>
            	<div style="float:left;"> 
					Watermark <input type="radio" name="watermark_radio" onclick="javascript:watermarkOption(this.value);" value="1" checked="yes" />iHouse Logo
                    <input type="radio" name="watermark_radio" onclick="javascript:watermarkOption(this.value);" value="0" />
                    <input type="text" value="<?php echo $this->watermark_text ?>" maxlength="16" name="watermark_text" />	
                </div>
            	<div style="clear:both"></div>
            </div>
            
            <div style="margin:10px 0;">
            	<div style="float:left;"> 
					Simply click 'Upload' to choose the photos you want to upload. You can upload up to 8 photos.		
                </div>
                <div id="pic_uploading_loader">
                	<div style="float:left;">
                    <img src="<?php echo JRoute::_('templates/main/images/ajax-loader.gif'); ?>"  />
                    </div> 
                    <div style="float:left;margin-left:5px;">
                    Wait! Loading
                    </div>
                    <div style="clear:both"></div>
                </div>
                <div style="clear:both"></div>
                
				<div id="message_tmp">
				</div>
                
				<div id="loadAdsImagesPlace" style="margin:10px 0;">
                	<i>Currently you have not uploaded any photos</i>
				</div>
                
				<div style="clear:both;"></div>
             </div>
         </div>
        <div style="border:2px solid #a50c2a; width:98%;"> 
        	<div style="color:#fff;background-color:#a50c2a;">
            	<div style="padding:2px;">
            		<strong>Important</strong>
                </div>
            </div>
        	<div>      
			<input type="checkbox" name="agree1" value="1" onclick="javascript:agree('agree1');" /><span id="err-agree1">I confirm the listing details and photos describe the property accurately</span><br />
                
			<?php if($user_category == 'Agent') : ?>
               <input type="checkbox" name="agree2" value="1" onclick="javascript:agree('agree2');" />
               	<span id="err-agree2">I confirm I have obtained agreement from the client and/or the client's representing agent to advertise the property</span>
            <?php endif; ?>    
            </div>    
		</div>
			<div id="button_div" style="text-align:center;margin:3px 0;">
        		<button class="button validate" type="submit" onclick="javascript:validateForm('submit','<?php echo JRoute::_('index.php?option=com_ads&view=posting_ads'); ?>');return false;">
					<?php echo JText::_('Submit'); ?>
        		</button>
                <button class="button validate" type="submit" onclick="javascript:validateForm('preview','<?php echo JRoute::_('index.php?option=com_ads&view=preads'); ?>');return false;">
					<?php echo JText::_('Preview'); ?>
        		</button>
			</div>  
		</div><!--End-->
    
    </div>
</div> 
<div style="clear:both"></div>  

			</div>
		</div>

	</div><!-- tablist -->
</div><!-- padding : 10px; -->       

</div><!-- margin : -20px -->     

<div id="popupForm" style="display:none;">
</div>                         

<div id="community-photo-items" class="photo-list-item" style="display:none">
	<div class="container"></div>
</div>  