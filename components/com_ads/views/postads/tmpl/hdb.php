<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('behavior.tooltip'); ?>
<style type="text/css">
.ads_form_border {
	border-bottom:1px dotted #ccc;
	float:left;
	width:100%;
}

.ads_form_0 {
	float:left;margin:10px 0;
}

.ads_form_1 {
	float:left;margin:10px 5px;
}
</style>
<div class="ads_form_border">
	<div class="ads_form_0">
	<label id="hdb_estatemsg" for="property_type">
	HDB Estate *
	</label>
	<div style="padding-top:5px;">
	<?php echo $hdb_town_list; ?>
	</div>           
    </div>
    
    <div class="ads_form_1">
	<label id="ad_typemsg" for="property_type">
	Ad Type*: 
	</label>
	<div style="padding-top:5px;">
	<?php echo $ad_type ?>
	</div>
	</div>
    
    <div class="ads_form_1">
	<label id="property_typemsg" for="property_type">
	Rental Type: 
	</label>
	<div style="padding-top:5px;">
	<?php echo $rental_type ?>
	</div>
	</div>
    
    <div class="ads_form_1">
	<label id="property_typemsg" for="property_type">
	Floor Level
	</label>
	<div style="padding-top:5px;">
	<?php echo $floor_level ?>
	</div>
	</div>
    
    <div class="ads_form_1">
	<label id="flat_typemsg" for="property_type">
	Flat Type* : 
	</label>
	<div style="padding-top:5px;">
	<?php echo $flattype ?>
	</div>
	</div>
    
    
</div>
<div style="clear:both;"></div>
<div class="ads_form_border">
	<div class="ads_form_0">
	<label id="street_name_hdbmsg" for="property_type">
			Street Name *
	</label>
	<div style="padding-top:5px;">
		<?php echo $street_name ?>
	</div>   
    </div>      
</div>
<div style="clear:both;"></div>
<div class="ads_form_border">
	<div class="ads_form_0">
	<label id="property_typemsg" for="property_type">
		Asking Price (SGD)
	</label>
	<div style="padding-top:5px;">
	<?php echo $asking_price ?>
	</div>
    </div>
    
    <div class="ads_form_1">
    	<label id="property_typemsg" for="property_type">
		Size (sqft)
		</label>
		<div style="padding-top:5px;">
		<?php echo $size ?>
		</div>
    </div>
    
    <div class="ads_form_1">
    	<div>
    	<label id="property_typemsg" for="property_type">
		Psm Price:
        </label>
        </div>
        <div style="margin-top:7px;">
        <span style="padding-top:5px;">
        	<?php echo $psm_price ?>
        </span>
        </div>
	</div>
     <div class="ads_form_1"> 
     	<div>       
    	<label id="property_typemsg" for="property_type">
		Psf Price:
        </label>
        </div>
        <div style="margin-top:7px;">
        <span style="padding-top:5px;">
        	<?php echo $psf_price ?>
        </span>
        </div>
    </div>
    
</div>
<div style="clear:both"></div>
<div class="ads_form_border">
	<div class="ads_form_0">
		<label id="property_typemsg" for="property_type">
		# of Hall
		</label>
		<div style="padding-top:5px;">
		<?php echo $no_of_hall; ?>
		</div>
	</div>
	<div class="ads_form_1">
		<label id="property_typemsg" for="property_type">
		# of Room
		</label>
		<div style="padding-top:5px;">
		<?php echo $no_of_room ?>
		</div>
	</div>
	<div class="ads_form_1">
		<label id="property_typemsg" for="property_type">
		Study Room
		</label>
		<div style="padding-top:5px;">
			<?php echo $study_room ?>
		</div>
	</div>
    
	<div class="ads_form_1">
		<label id="property_typemsg" for="property_type">
		Toilet
		</label>
		<div style="padding-top:5px;">
		<?php echo $toilet ?>
		</div>
	</div>

</div>
<div style="clear:both"></div>
<div class="ads_form_border">
	<div class="ads_form_0">
    	<label id="property_typemsg" for="property_type">
		Block/House Number
		</label>
		<div style="padding-top:5px;">
			<?php echo $block_no ?>
		</div>
    </div>
    
    <div class="ads_form_1">
    	<label id="property_typemsg" for="property_type">
		Floor/Unit Number
		</label>
		<div style="padding-top:5px;">
			<?php echo $floor_unit_no ?>                                                          
		</div>
    </div>
    
    <div class="ads_form_1">
    	<label id="property_typemsg" for="property_type">
			Country
		</label>
		<div style="padding-top:5px;">
			<?php echo $country ?>
		</div>
    </div>
    
    <div class="ads_form_1">
    	<label id="tenuremsg" for="property_type">
		Tenure
		</label>
		<div style="padding-top:5px;">
    	<input type="hidden" name="tenure" value="99"  />
		<?php echo '99' ?>
		</div>
    </div>
</div>    
<div style="clear:both"></div>
<div class="ads_form_border">
	<div class="ads_form_0">
    	<label id="postal_codemsg" for="property_type">
		Postal Code*
		</label>
		<div style="padding-top:5px;">
		<?php echo $postcode ?>
		</div>
    </div>
    
    <div class="ads_form_1">
    	<label id="tenuremsg" for="property_type">
		TOP year
		</label>
		<div style="padding-top:5px;">
    	<?php echo $year_built ?>
		</div>
    </div>
</div>
<div style="clear:both"></div>
<div class="ads_form_border">
	<div class="ads_form_0">
    	<label id="ad_titlemsg" for="property_type">
		Ad Title *
		</label>
		<div style="padding-top:5px;">
		<?php echo $ad_title ?>
		</div>
    </div>
    
    <div class="ads_form_1">
    	<label id="property_typemsg" for="property_type">
			Renovation
		</label>
		<div style="padding-top:5px;">
			<?php echo $renovation ?>
		</div>
    </div>
    <div class="ads_form_1">
    	<label id="statusmsg" for="property_type">
			Status
		</label>
		<div style="padding-top:5px;">
			<input type="hidden" id="status_text" name="status" value=""  />
            Pending
		</div>
    </div>
    <!--Chinese input method begin-->
	<div>
		<div style="padding-top:35px;">
			<a href="javascript:void((function(){var e=document.createElement('script');e.setAttribute('src','http://web.pinyin.sogou.com/web_ime/init.js');document.body.appendChild(e);})())">Online Chinese input method</a>
		</div>
    </div>
	<!--Chinese input method end-->
</div>  
<div style="clear:both"></div>
<div class="ads_form_border">
	<div class="ads_form_0">
    	<label id="property_typemsg" for="property_type">
			Description
		</label>
		<div style="padding-top:5px;">
			<?php echo $add_info ?>
		</div>
    </div>
</div>
<div style="clear:both"></div>