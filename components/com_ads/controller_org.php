﻿<?php
/**
 * @version		$Id: controller.php 12538 2009-07-22 17:26:51Z ian $
 * @package		Joomla
 * @subpackage	iHouse360 Ads
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant to the
 * GNU General Public License, and as distributed it includes or is derivative
 * of works licensed under the GNU General Public License or other free or open
 * source software licenses. See COPYRIGHT.php for copyright notices and
 * details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport('joomla.application.component.controller');

/**
 * Ads Component Controller
 *
 * @package		Joomla
 * @subpackage	ihouse360 Ads
 * @since 1.5
 */
class AdsController extends JController
{
	/**
	 * Method to display a view
	 *
	 * @access	public
	 * @since	1.5
	 */
	 function paymentprocess()
	 {
	 	
	 	JRequest::setVar('view','paymentprocess'); 
		parent::display();
	 }
	 
	 function credit_confirmation()
	 {
	 	
	 	JRequest::setVar('view','credit_confirmation'); 
		parent::display();
	 }
	 
	 function paymentsuccess()
	 {
	 	
	 	JRequest::setVar('view','paymentsuccess'); 
		parent::display();
	 }
	 
	 function notavailable()
	 {
	 	JRequest::setVar('view','notavailable'); 
		parent::display();
	 }
	 
	 function deduct_credit()
	 {
	 	global $mainframe;
		
		$user 	=& JFactory::getUser();

		if ( $user->get('guest')) {
			$this->setredirect('index.php?option=com_user&view=login&Itemid=80');
		} else {
		
			$model		= &$this->getModel('credits');

			if($model->deduct_credits()) {
				$this->setRedirect( JRoute::_('index.php?option=com_ads&view=ads_selection', false));
			}
		} 	
	 }
	 
	 /*function type_three()
	 {
	 	$model		= &$this->getModel('credits');

		if($model->type_three()) {
			$this->setRedirect( JRoute::_('index.php?option=com_ads&view=ads_selection', false));
		}
	 }*/

	 /*function delete_transaction()
	 {
		 $model		= &$this->getModel('credits');
		 
		 $user =& JFactory::getUser();
		 $user_id = $user->get('id');

		if(!$model->delete_trans()) {
			$msg = JText::_( 'Error: Deleting User Transaction' );
		} else {
			$msg = JText::_( 'User(s) Transaction Deleted' );
		}
		$link = 'index.php?option=com_ads&view=transaction&id='.$user_id;
		
		$this->setRedirect($link, $msg );
	 }*/
	 
	 function ads_management()
	 {
	 	$user 	=& JFactory::getUser();

		if ( $user->get('guest')) {
			$this->setredirect('index.php?option=com_user&view=login&Itemid=80');
		} else {
			JRequest::setVar('view', 'ads_management');
		}
	
		parent::display();
	 
	 }
	 
	 function post_featured() 
	 {
	 	
		$user 	=& JFactory::getUser();

		if ( $user->get('guest')) {
			$this->setredirect('index.php?option=com_user&view=login&Itemid=80');
		} else {
			JRequest::setVar('view', 'post_featured');
		}
	
		parent::display();
	 }
	 
	 function deleteads()
	 {
	 	global $mainframe;
		$db = & JFactory::getDBO();
		
		$user		=& 	JFactory::getUser();
		$user_id	= 	$user->id;

		if ( $user->get('guest')) {
			$this->setredirect('index.php?option=com_user&view=login&Itemid=80');
		}

		$selectid	= JRequest::getVar('selectid', '', 'post', 'selectid');
		
		$query = "SELECT subscription_id FROM #__ihouse_ads WHERE id = '".$selectid."'";
			$db->setQuery($query);
			$sub_id = $db->loadResult();
		
		$query = "SELECT plan_id FROM #__cbsubs_subscriptions WHERE id = '".$sub_id."' AND status = 'A'";
			$db->setQuery($query);
			$plan_id = $db->loadResult();
		
		$query = "SELECT count(*) FROM #__cbsubs_plans WHERE id = '".$plan_id."' AND (id = '2' OR id = '3')";
			$db->setQuery($query);
			$slot = $db->loadResult();
		
		$query = "DELETE FROM #__ihouse_ads WHERE id = '".$selectid."' AND posted_by = '".$user_id."'";
			$db->setQuery($query);
			$db->query();
		
		if($slot != '0'){
			$sql = "UPDATE #__ihouse_ads_subscription_slot SET slot = slot + 1 WHERE user_id = '".$user_id."'";
				$db->setQuery($sql);
				$db->query();
		}
		
		/* SUBSCRIPTION BECOMES EXPIRED - Based on Ads ID */
		
		$query = 	" SELECT cb.id AS subs_id, cb.plan_id, cb.user_id, t.* FROM #__cbsubs_subscriptions AS cb "
					.	" LEFT JOIN #__ihouse_ads_type AS t ON t.subscription_id = cb.id "
					.	" WHERE cb.status = 'A' "
					.	" AND cb.user_id = '$user_id' "
					.	" AND t.ads_id = '$selectid' ";
						
					
			$db->setQuery( $query );
			$rows = $db->loadObjectList();
		
		foreach($rows as $r) :
		
			$plan_id		=	$r->plan_id;
			$subs_id		=	$r->subs_id;
			$user_id		=	$r->user_id;
			$adstypeconfig 	= 	$r->ads_type_config;
			$propertytype	=	$r->property_type;
			$propertyid		=	$r->property_id;
			
			$query			=	" UPDATE #__cbsubs_subscriptions SET status = 'X' , previous_status = 'A' "
								.	" WHERE id = '$subs_id' AND status = 'A' ";	
						
				$db->setQuery($query);
				$db->query();
			
			switch($adstypeconfig) {
				case 4:
				case 6:
					$query = " UPDATE #__ihouse_ads_type_slot SET space = space + 1 WHERE ads_type_config = '".$adstypeconfig."' AND property_type = '".$propertytype."' ";
						$db->setQuery( $query );
						$db->query();
							
					break;
				case 1:
				case 2:
				case 5:
						
					$query = " UPDATE #__ihouse_ads_type_slot SET space = space + 1 WHERE ads_type_config = '".$adstypeconfig."' AND property_id = '".$propertyid."' ";
						$db->setQuery( $query );
						$db->query();
						
					break;
				case 3:
				case 7:
						
					$query = " UPDATE #__ihouse_ads_type_slot SET space = space + 1 WHERE ads_type_config = '".$adstypeconfig."' ";
						$db->setQuery( $query );
						$db->query();
							
					break;
			}
		endforeach;
		
		/* DELETE PICTURE AND FOLDER */
		$query = " SELECT * FROM #__ihouse_ads_image WHERE ads_id = '$selectid' ";
			$db->setQuery( $query );
			$rows = $db->loadObjectList();
		
		foreach($rows as $r) :
		
			if(is_dir(JRoute::_('images/ihouse/ads_images/'.$r->sess_id))) {
				unlink(JRoute::_('images/ihouse/ads_images/'.$r->sess_id.'/'.$r->name));
				unlink(JRoute::_('images/ihouse/ads_images/'.$r->sess_id.'/med-'.$r->name));
				unlink(JRoute::_('images/ihouse/ads_images/'.$r->sess_id.'/small-'.$r->name));
			}
			
		endforeach;
		
		if(is_dir(JRoute::_('images/ihouse/ads_images/'.$r->sess_id))) 
			rmdir(JRoute::_('images/ihouse/ads_images/'.$r->sess_id));
		
		$query = " DELETE FROM #__ihouse_ads_image WHERE ads_id = '$selectid' ";
			$db->setQuery($query);
			$db->query();
			
		$query = " DELETE FROM #__ihouse_ads_image2 WHERE ads_id = '$selectid' ";
			$db->setQuery($query);
			$db->query();
		
		$message = "You have deleted the ad.";
		$link =  JRoute::_('index.php?option=com_ads&view=ads_management&Itemid=126&id='.$user_id, false);
		$this->setRedirect($link, $message);
		
		//echo $query;
	 
	 
	 }//end function
	 
	 function adssold()
	 {	
	 	global $mainframe;
		$db = & JFactory::getDBO();
		
		
		$user		=& 	JFactory::getUser();
		$user_id	= 	$user->id;

		if ( $user->get('guest')) {
			$this->setredirect('index.php?option=com_user&view=login&Itemid=80');
		}

		$selectid	= JRequest::getVar('selectid', '', 'post', 'selectid');
		
		$query = "SELECT status_id FROM #__ihouse_ads WHERE id = '".$selectid."'";
		$db->setQuery($query);
		$status = $db->loadResult();

		
		if($status == '已售'){
		
			$message = "This ad has already sold.";
			$link =  JRoute::_('index.php?option=com_ads&view=ads_management&Itemid=126&id='.$user_id, false);
			$this->setRedirect($link, $message);
		
		} else {
			
			$sql = "UPDATE #__ihouse_ads set status_id = '已售' WHERE id = '".$selectid."'";
			$db->setQuery($sql);
			$db->query();
			
			$message = "You have updated this ad to SOLD.";
			$link =  JRoute::_('index.php?option=com_ads&view=ads_management&Itemid=126&id='.$user_id, false);
			$this->setRedirect($link, $message);
		
		}
	 
	 }//end function
	 
	 function adsrent()
	 {	
	 	global $mainframe;
		$db = & JFactory::getDBO();
		
		
		$user		=& 	JFactory::getUser();
		$user_id	= 	$user->id;

		if ( $user->get('guest')) {
			$this->setredirect('index.php?option=com_user&view=login&Itemid=80');
		}

		$selectid	= JRequest::getVar('selectid', '', 'post', 'selectid');
		
		$query = "SELECT status_id FROM #__ihouse_ads WHERE id = '".$selectid."'";
		$db->setQuery($query);
		$status = $db->loadResult();

		
		if($status == '已租'){
		
			$message = "This ad has been rented.";
			$link =  JRoute::_('index.php?option=com_ads&view=ads_management&Itemid=126&id='.$user_id, false);
			$this->setRedirect($link, $message);
		
		} else {
			
			$sql = "UPDATE #__ihouse_ads set status_id = '已租' WHERE id = '".$selectid."'";
			$db->setQuery($sql);
			$db->query();
			
			$message = "You have updated this ad to RENT.";
			$link =  JRoute::_('index.php?option=com_ads&view=ads_management&Itemid=126&id='.$user_id, false);
			$this->setRedirect($link, $message);
		
		}
	 
	 }//end function
	 
	  function publishads()
	 {	
	 	global $mainframe;
		$db = & JFactory::getDBO();
		
		
		$user		=& 	JFactory::getUser();
		$user_id	= 	$user->id;

		if ( $user->get('guest')) {
			$this->setredirect('index.php?option=com_user&view=login&Itemid=80');
		}

		$selectid	= JRequest::getVar('selectid', '', 'post', 'selectid');
		
		$query = "SELECT publish FROM #__ihouse_ads WHERE id = '".$selectid."'";
		$db->setQuery($query);
		$publish = $db->loadResult();

		
		if($publish == '1'){
		
			$message = "This ad has been published.";
			$link =  JRoute::_('index.php?option=com_ads&view=ads_management&Itemid=126&id='.$user_id, false);
			$this->setRedirect($link, $message);
		
		} else {
			
			$sql = "UPDATE #__ihouse_ads set publish = '1' WHERE id = '".$selectid."'";
			$db->setQuery($sql);
			$db->query();
			
			$message = "You have successfully published this ad.";
			$link =  JRoute::_('index.php?option=com_ads&view=ads_management&Itemid=126&id='.$user_id, false);
			$this->setRedirect($link, $message);
		
		}
	 
	 }//end function
	 
	 function the_guru()
	 {
	 
	 	$user 	=& JFactory::getUser();

		if ( $user->get('guest')) {
			$this->setredirect('index.php?option=com_user&view=login&Itemid=80');
		} else {
			JRequest::setVar('view', 'the_guru');
		}
	
		parent::display();
	 }//end function
	 
	  
	 function featured_save()
	 {
	 	global $mainframe;
		$db = & JFactory::getDBO();
		
		$user		=& 	JFactory::getUser();
		$user_id	= 	$user->id;

		if ( $user->get('guest')) {
			$this->setredirect('index.php?option=com_user&view=login&Itemid=80');
		}
		
		$model		= &$this->getModel('credits');
		
	 	$plan_id	= JRequest::getVar('plan_id', '', 'post', 'plan_id');
		$property_id	= JRequest::getVar('property_id', '', 'post', 'property_id');
		$ads_type_config	= JRequest::getVar('ads_type_config', '', 'post', 'ads_type_config');
		
		$query = "SELECT credit FROM #__ihouse_users_credit WHERE user_id = '".$user_id."'";
		$db->setQuery($query);
		$balance = $db->loadResult();
		
		$query = "SELECT rate FROM #__cbsubs_plans WHERE id = '".$plan_id."'";
		$db->setQuery($query);
		$rate = $db->loadResult();
		
		$query = "SELECT params FROM #__ihouse_promotion WHERE id = '4'";
		$db->setQuery($query);
		$params = $db->loadResult();
		
		$discountone = explode("=", $params);
		$discount = $discountone[1];
		
		$after_discount = $rate * (1 - ($discount));
		
	 	if($balance >= round($after_discount)){ //enough credits
		
			if($model->featured_save()) {
				$message = "You have successfully subscribed to the featured ads. Thank you.";
				$link =  JRoute::_('index.php?option=com_ads&view=account_info&Itemid=126', false);
				$this->setRedirect($link, $message);
			}
			
		} else {
			
			$actual_amt = round($after_discount) - $balance;
			
			$query = "INSERT INTO #__ihouse_ads_temp (id,user_id,plan_id,ads_type_config,property_id,ads_id,ads_type,actual_amt,temp_type,payment_logid)"
	
				. "\n VALUES ( NULL, '".$user_id."', '".$plan_id."', '".$ads_type_config."', '".$property_id."', '".$ads_id."', '".$ads_type."', '".round($after_discount)."', '1','0')";
				$db->setQuery( $query );
				$db->query();
			$sid = mysql_insert_id();
			
			$message = "You do not have enough credit. Please top up your credit here. The option that you have choosen requires ".$actual_amt." more credits. You have ".$balance." credits currently.";
			$link =  JRoute::_('index.php?option=com_ads&view=top_up&Itemid=126&tempid='.$sid.'&reamt='.$actual_amt.'&id='.$user_id, false);
			$this->setRedirect($link, $message);
		}

	 }//end function
	 
	 function featuredads_save()
	 {	
	 	global $mainframe;
		$db = & JFactory::getDBO();
		
		$user		=& 	JFactory::getUser();
		$user_id	= 	$user->id;

		if ( $user->get('guest')) {
			$this->setredirect('index.php?option=com_user&view=login&Itemid=80');
		}
		
		$model		= &$this->getModel('credits');

		$plan_id	= JRequest::getVar('plan_id', '', 'post', 'plan_id');
		$ads_type_config	= JRequest::getVar('ads_type_config', '', 'post', 'ads_type_config');
		
		$ads_id	= JRequest::getVar('ads_id', '', 'post', 'ads_id');
		
		$query = "SELECT credit FROM #__ihouse_users_credit WHERE user_id = '".$user_id."'";
		$db->setQuery($query);
		$balance = $db->loadResult();
		
		$query = "SELECT rate FROM #__cbsubs_plans WHERE id = '".$plan_id."'";
		$db->setQuery($query);
		$rate = $db->loadResult();
		
		$query = "SELECT params FROM #__ihouse_promotion WHERE id = '4'";
		$db->setQuery($query);
		$params = $db->loadResult();
		
		$discountone = explode("=", $params);
		$discount = $discountone[1];
		
		
		//25% discount for the 4th week
		if(($plan_id == '11')||($plan_id == '15')||($plan_id == '19')||($plan_id == '23')){
		$sub_credit = ((round($rate)) * (1 - 0.25));
		} else {
		$sub_credit = (round($rate));
		}
		
		//80% discount for all category featured ads
		$after_discount = $sub_credit * (1 - ($discount));
		
		
		
	 	if($balance >= $after_discount){ //enough credits
		
			if($model->featuredads_save()) {
				$message = "You have successfully subscribed to the featured ads. Thank you.";
				$link =  JRoute::_('index.php?option=com_ads&view=ads_management&Itemid=126', false);
				$this->setRedirect($link, $message);
			}
			
		} else {
		
			$actual_amt = $after_discount - $balance;
			
			$query = "INSERT INTO #__ihouse_ads_temp (id,user_id,plan_id,ads_type_config,property_id,ads_id,ads_type,actual_amt,temp_type,payment_logid)"
	
				. "\n VALUES ( NULL, '".$user_id."', '".$plan_id."', '".$ads_type_config."', '".$property_id."', '".$ads_id."', '".$ads_type."', '".round($after_discount)."','2','0')";
				$db->setQuery( $query );
				$db->query();
			$sid = mysql_insert_id();
				
			$message = "You do not have enough credit. Please top up your credit here. The option that you have choosen requires ".$actual_amt." more credits. You have ".$balance." credits currently.";
			$link =  JRoute::_('index.php?option=com_ads&view=top_up&Itemid=126&tempid='.$sid.'&reamt='.$actual_amt.'&id='.$user_id, false);
			$this->setRedirect($link, $message);
		}
		
	 }//end function
	 
	 function top_up()
	 {
	 
	 	$user 	=& JFactory::getUser();

		if ( $user->get('guest')) {
			$this->setredirect('index.php?option=com_user&view=login&Itemid=80');
		} else {
			JRequest::setVar('view', 'top_up');
		}
	
		parent::display();
	 }//end function
	 
	 function transaction()
	 {
	 
	 	$user 	=& JFactory::getUser();

		if ( $user->get('guest')) {
			$this->setredirect('index.php?option=com_user&view=login&Itemid=80');
		} else {
			JRequest::setVar('view', 'transaction');
		}
	
		parent::display();
	 }//end function
	 
	 function account_info()
	 {
	 
	 	$user 	=& JFactory::getUser();

		if ( $user->get('guest')) {
			$this->setredirect('index.php?option=com_user&view=login&Itemid=80');
		} else {
			JRequest::setVar('view', 'account_info');
		}
	
		parent::display();
	 }
	 
	 function account_upgrade()
	 {
	 
	 	$user 	=& JFactory::getUser();

		if ( $user->get('guest')) {
			$this->setredirect('index.php?option=com_user&view=login&Itemid=80');
		} else {
			JRequest::setVar('view', 'account_upgrade');
		}
	
		parent::display();
	 }
	 
	 function save_upgrade()
	 {
	 
	 global $mainframe, $option;
	 require_once( JPATH_ROOT.DS.'includes'.DS.'tools.php' );
	 $db = & JFactory::getDBO();
	 $action	= JRequest::getVar('action', '', 'post', 'action');
	 $user_category	= JRequest::getVar('user_category', '', 'post', 'user_category');
	 $user =& JFactory::getUser();
	 $id = $user->get('id');

	if ( $user->get('guest')) {
		$this->setredirect('index.php?option=com_user&view=login&Itemid=80');
	}
	
	/*echo $action;
	echo "asa";
	echo $user_category;*/
	//exit;
	
	 
	 if($user_category == 'Owner'){
	 
	 $query = "UPDATE #__users set user_category = 'Owner' WHERE id = '".$id."'";

			$db->setQuery( $query );
			$db->query();
	 $this->setRedirect('index.php?option=com_user&view=thankupgrade&rid=1&sid='.getrandom().$user->get('id').getrandom().'&Itemid=79', $message);
	 
	 } else {
	 
	 $query = "UPDATE #__users set upgrade_status = '1' WHERE id = '".$id."'";

			$db->setQuery( $query );
			$db->query();
			
	 $this->setRedirect('index.php?option=com_user&view=paymentprocess&Itemid=126&sid='.getrandom().$id.getrandom(), $message);
	 
	 }
	 
	 }
	 
	 	 
	 function subscription()
	 {
	 	global $mainframe, $option;
	 	$user 	=& JFactory::getUser();

		if ( $user->get('guest')) {
			$this->setredirect('index.php?option=com_user&view=login&Itemid=80');
		} else {
			JRequest::setVar('view', 'subscription');
		}
	
		parent::display();	
	 }
	 
	 function save_subscription()
	 {
	 
	 global $mainframe, $option;
	 $db = & JFactory::getDBO();
	 $user =& JFactory::getUser();
	 $user_id = $user->get('id');

	if ( $user->get('guest')) {
		$this->setredirect('index.php?option=com_user&view=login&Itemid=80');
	}
	 
	 $plan_id	= JRequest::getVar('sub_plan', '', 'post', 'sub_plan');
	 
	 $model		= &$this->getModel('credits');
	 
	 $query = "SELECT credit FROM #__ihouse_users_credit WHERE user_id = '".$user_id."'";
	 $db->setQuery($query);
	 $balance = $db->loadResult();
		
	 $query = "SELECT rate FROM #__cbsubs_plans WHERE id = '".$plan_id."'";
	 $db->setQuery($query);
	 $rate = $db->loadResult();
	 
	 $query = "SELECT params FROM #__ihouse_promotion WHERE id = '2'";
	 $db->setQuery($query);
	 $params = $db->loadResult();
		
	 $discountone = explode("=", $params);
	 $discount = $discountone[1];
	 
	 $after_discount = ((round($rate)) * (1 - ($discount)));
		
	 	if($balance >= round($after_discount)){ //enough credits
		
			if($model->sub_plan()) {
				$message = "You have successfully subscribed to this subscription plan. Thank you.";
				$link =  JRoute::_('index.php?option=com_ads&view=account_info&Itemid=126', false);
				$this->setRedirect($link, $message);
			}
			
		} else {
		
			$actual_amt = round($after_discount) - $balance;
			
			$query = "INSERT INTO #__ihouse_ads_temp (id,user_id,plan_id,ads_type_config,property_id,ads_id,ads_type,actual_amt,temp_type,payment_logid)"
	
				. "\n VALUES ( NULL, '".$user_id."', '".$plan_id."', '0', '0', '0', '', '".round($after_discount)."','3','0')";
				$db->setQuery( $query );
				$db->query();
			$sid = mysql_insert_id();
				
			$message = "You do not have enough credit. Please top up your credit here. The option that you have choosen requires ".$actual_amt." more credits. You have ".$balance." credits currently.";
			$link =  JRoute::_('index.php?option=com_ads&view=top_up&Itemid=126&tempid='.$sid.'&reamt='.$actual_amt.'&id='.$user_id, false);
			$this->setRedirect($link, $message);
		}

	 }
	 
	 function confirmation_page()
	 {
	 	global $mainframe;
	 	$user 	=& JFactory::getUser();

		if ( $user->get('guest')) {
			$this->setredirect('index.php?option=com_user&view=login&Itemid=80');
		} else {
			//echo 'dada';
			JRequest::setVar('view', 'confirmation_page');
		}
	
		parent::display();
	 
	 }
	 
	 function guru_confirmation_page()
	 {
	 	$user 	=& JFactory::getUser();

		if ( $user->get('guest')) {
			$this->setredirect('index.php?option=com_user&view=login&Itemid=80');
		} else {
			JRequest::setVar('view', 'guru_confirmation_page');
		}
	
		parent::display();
	 
	 }
	 
	 function featured_confirmation_page()
	 {
	 	$user 	=& JFactory::getUser();

		if ( $user->get('guest')) {
			$this->setredirect('index.php?option=com_user&view=login&Itemid=80');
		} else {
			JRequest::setVar('view', 'featured_confirmation_page');
		}
	
		parent::display();
	 
	 }
	 
	  function adslisting()
	 {
		 JRequest::setVar('view','adslisting'); 
		 parent::display();
	 }
	 
	 function rentadslisting()
	 {
		 JRequest::setVar('view','rentadslisting'); 
		 parent::display();
	 }
	 
	 function emailContactForm() {
		global $mainframe;
		
		$db		=	JFactory::getDBO(); 
		
		$email	=	JRequest::getVar('email','');
		$ads_id =	JRequest::getVar('ads_id','');
		$uid 	=	JRequest::getVar('uid','');
		
		require('views/contact_form/tmpl/default.php');
		$mainframe->close();
		exit;
	}
	
	function email() {
		global $mainframe;
		include_once('assets/json.php');
		
		//make json object...
		
		$json = new Services_JSON();
		
		$model		= &$this->getModel('contact');
		
		header('Content-type: application/json');
		
		$status		=	$model->sendEmail();
		
		echo $json->encode(array("status" => $status	
								 
							));
		$mainframe->close();
		exit;
	}
	 
	 function ajaxLoadAdslisting() 
	{
		global $mainframe;
		$model		= &$this->getModel('adsdetails');
		
		$usercat	= 	JRequest::getVar('user_category', '');
		$id	= 	JRequest::getVar('id', '');
		
		$db		=	JFactory::getDBO(); 
		$obj	=	$model->getAdsObject($id, $usercat);
		
		$model1		= &$this->getModel('pagination');
		
		
		
		$filter					=	JRequest::getVar('filter', '');
		$orderfilter			=	JRequest::getVar('orderfilter', '');

		/* DEFAULT */
		$orderfilter1	=	'ASC';
		$orderfilter2	=	'ASC';
		$orderfilter3	=	'ASC';
		$orderfilter4	=	'ASC';
		
		$image1			= '';
		$image2			= '';
		$image3 		= '';
		$image4			= '';
		
		if($filter == 'a.no_of_room') {
			
			if($orderfilter == 'ASC') {
        		$orderfilter1 = 'DESC';
				$image1 = '<img src="'.JRoute::_('images/sort_asc.png').'" />';
			}else{
				$orderfilter1 = 'ASC';
				$image1 = '<img src="'.JRoute::_('images/sort_desc.png').'" />';
			}
		}
		if($filter == 'a.property_type') {
			
			if($orderfilter == 'ASC') {
        		$orderfilter2 = 'DESC';
				$image2 = '<img src="'.JRoute::_('images/sort_asc.png').'" />';
			}else{
				$orderfilter2 = 'ASC';
				$image2 = '<img src="'.JRoute::_('images/sort_desc.png').'" />';
			}
		} else if($filter == 'a.ask_price') {
			
			if($orderfilter == 'ASC') {
        		$orderfilter3 = 'DESC';
				$image3 = '<img src="'.JRoute::_('images/sort_asc.png').'" />';
			}else{
				$orderfilter3 = 'ASC';
				$image3 = '<img src="'.JRoute::_('images/sort_desc.png').'" />';
			}
		} else if($filter == 'a.posting_date') {
			
			if($orderfilter == 'ASC')  {
        		$orderfilter4 = 'DESC';
				$image4 = '<img src="'.JRoute::_('images/sort_asc.png').'" />';
			} else { 
				$orderfilter4 = 'ASC';
				$image4 = '<img src="'.JRoute::_('images/sort_desc.png').'" />';
			}
		}
		
		$pagination_html	=	$model1->getPaginationAdsListing($obj->total, 5, $usercat, $filter, $orderfilter);
		
		require('views/adslisting_ajax/tmpl/default.php');
		
		$mainframe->close();
		exit;
	}
	 
	 function ajaxLoadRentAdslisting() 
	{
		global $mainframe;
		$model		= &$this->getModel('adsdetails');
		
		$usercat	= 	JRequest::getVar('user_category', '');
		$id	= 	JRequest::getVar('id', '');
		
		$db		=	JFactory::getDBO(); 
		$obj	=	$model->getRentAdsObject($id, $usercat);
		
		$model1		= &$this->getModel('pagination');
		
		
		
		$filter					=	JRequest::getVar('filter', '');
		$orderfilter			=	JRequest::getVar('orderfilter', '');

		/* DEFAULT */
		$orderfilter1	=	'ASC';
		$orderfilter2	=	'ASC';
		$orderfilter3	=	'ASC';
		$orderfilter4	=	'ASC';
		
		$image1			= '';
		$image2			= '';
		$image3 		= '';
		$image4			= '';
		
		if($filter == 'a.no_of_room') {
			
			if($orderfilter == 'ASC') {
        		$orderfilter1 = 'DESC';
				$image1 = '<img src="'.JRoute::_('images/sort_asc.png').'" />';
			}else{
				$orderfilter1 = 'ASC';
				$image1 = '<img src="'.JRoute::_('images/sort_desc.png').'" />';
			}
		}
		if($filter == 'a.property_type') {
			
			if($orderfilter == 'ASC') {
        		$orderfilter2 = 'DESC';
				$image2 = '<img src="'.JRoute::_('images/sort_asc.png').'" />';
			}else{
				$orderfilter2 = 'ASC';
				$image2 = '<img src="'.JRoute::_('images/sort_desc.png').'" />';
			}
		} else if($filter == 'a.ask_price') {
			
			if($orderfilter == 'ASC') {
        		$orderfilter3 = 'DESC';
				$image3 = '<img src="'.JRoute::_('images/sort_asc.png').'" />';
			}else{
				$orderfilter3 = 'ASC';
				$image3 = '<img src="'.JRoute::_('images/sort_desc.png').'" />';
			}
		} else if($filter == 'a.posting_date') {
			
			if($orderfilter == 'ASC')  {
        		$orderfilter4 = 'DESC';
				$image4 = '<img src="'.JRoute::_('images/sort_asc.png').'" />';
			} else { 
				$orderfilter4 = 'ASC';
				$image4 = '<img src="'.JRoute::_('images/sort_desc.png').'" />';
			}
		}
		
		$pagination_html	=	$model1->getPaginationAdsListing($obj->total, 5, $usercat, $filter, $orderfilter);
		
		require('views/rentadslisting_ajax/tmpl/default.php');
		
		$mainframe->close();
		exit;
	}
	
	function postingads()
	 {
	 	$user 	=& JFactory::getUser();

		if ( $user->get('guest')) {
			$this->setredirect('index.php?option=com_user&view=login&Itemid=80');
		} else {
			$this->setRedirect('index.php?option=com_ads&view=postads&Itemid=126');
		}
	
		//parent::display();
	 
	 }
	 

	   /* Me - iFoundries */
	 
	 function postads() {
		global $mainframe;
		 
		JRequest::setVar('view','postads'); 
		
		$user =& JFactory::getUser();
	 	$user_id = $user->get('id');
		 
		$model		= &$this->getModel('ads');

		/* OWNER */
		if($model->checkForOwner()) {
		
		} else {
			$msg	=	'You have reached max limit of posting ads for Owner';
			$this->setRedirect( JRoute::_('index.php?option=com_ads&view=ads_management&Itemid=126&id='.$user_id, false), $msg );
		}
		
		/* AGENT */
		if($model->checkForAgent()) {
			
		} else {
			$msg	=	'Not Enough Credit for Pay Per Basis or Slot is 0 and do not have enough credit. Please Top Up';
			$this->setRedirect( JRoute::_('index.php?option=com_ads&view=ads_management&Itemid=126&id='.$user_id, false), $msg );
		}
		
		parent::display();
	 }
	 
	 function posting_ads() {
		global $mainframe;
		
		$model		= &$this->getModel('ads');
		
		$user =& JFactory::getUser();
	 	$user_id = $user->get('id');
		
		if($model->post_ads()) {
			$msg	=	'Your posting has been added';
			
			$this->setRedirect( JRoute::_('index.php?option=com_ads&view=ads_management&Itemid=126&id='.$user_id, false), $msg );
		} else {
			$msg	=	'Your posting has failed';
			
			$this->setRedirect( JRoute::_('index.php?option=com_ads&view=postads', false), $msg );
			
		}
		
	 }
	 
	 function adsrefresh()
	 {
		global $mainframe;
		
	 	$model		= &$this->getModel('ads');
		
		$ads_id 	= JRequest::getVar('selectid');
		
		$user 		= &JFactory::getUser();
		$uid 		= $user->get('id');
		
		
		
		if($model->refresh($ads_id) && $uid > 0) {
			$msg	=	'Ads has been Refreshed ';
			$this->setRedirect( JRoute::_('index.php?option=com_ads&view=ads_management&Itemid=126&id='.$uid, false), $msg );
		} else {
			$msg	=	'Refreshing Ads has failed ';
			$this->setRedirect( JRoute::_('index.php?option=com_ads&view=ads_management&Itemid=126&id='.$uid, false), $msg );
		}
	}
	
	function ajaxLoadFloorplanPicture() {
		global $mainframe;
		include_once('assets/json.php');
		
		//make json object...
		
		$json = new Services_JSON();
		
		$model		= &$this->getModel('ads');
		
		header('Content-type: application/json');
		$tmp	=	$model->getFloorplanPictureHTML();
		
		
		echo $json->encode(array(	
								 "fp"=> $tmp
							));

		$mainframe->close;
		exit;
	}
	
	function ajaxLoadPropertyId() {
		global $mainframe;
		include_once('assets/json.php');
		
		//make json object...
		
		$json = new Services_JSON();
		
		$model		= &$this->getModel('ads');
		
		header('Content-type: application/json');
		
		$tmp	=	$model->getPropertyInfo();
		
		echo $json->encode(array(	
								 "property_id"=> $tmp->id,
								 "district_id"=> $tmp->district_id,
								 "postcode" => $tmp->postcode,
								 "tenure" => $tmp->tenure,
								 "topyear" => $tmp->year_built,
								 "streetname" => $tmp->address,
								 "has_floorplan" => $tmp->has_floorplan
							));

		$mainframe->close;
		exit;
	}
	 
	function ajaxLoadPropertyName() {
		global $mainframe;
		include_once('assets/json.php');
		
		//make json object...
		
		$json = new Services_JSON();
		
		$model		= &$this->getModel('ads');
		
		header('Content-type: application/json');
		
		$tmp	=	$model->getPropertyName();
		
		$html = '';
		$html .= '<select name="property_name" id="property_name" onchange="javascript:loadPropertyId(this.value);">';
		$html .= '<option value="">----</option>';
		
		foreach($tmp as $t) :
			$html .= '<option value="'.$t->id.'">'.$t->name_en.'</option>';
		endforeach;
		
		$html .= '</select>';
		
		echo $json->encode(array(	
								"select_html" => $html
							));

		$mainframe->close;
		exit;
	} 
	 
	function ajaxLoadFPList() {
		global $mainframe;
		include_once('assets/json.php');
		
		//make json object...
		
		$json = new Services_JSON();
		
		$model		= &$this->getModel('ads');
		
		header('Content-type: application/json');
		
		$html	=	$model->getFPListHTML();
		
		echo $json->encode(array(	
								"fp_select_html" => $html,
								"status" => true
							));

		$mainframe->close;
		exit;
	}
	
	function ajaxLoadImagePhotos() {
		global $mainframe;
		include_once('assets/json.php');
		
		//make json object...
		
		$json = new Services_JSON();
		
		$model		= &$this->getModel('ads');
		
		header('Content-type: application/json');
		
		$html	=	$model->getImagePhotosHTML();
		
		echo $json->encode(array(	
								"img_html" => $html
							));

		$mainframe->close;
		exit;
	}
	
	function ajaxLoadFormPropertyType() {
		global $mainframe;
		
		$user 			=& 	JFactory::getUser();
	 	$user_category 	= 	$user->get('user_category');
		
		$layout = JRequest::getCmd('layout','');
		
		$model		= &$this->getModel('property');
		
		$property_type 	= JRequest::getVar('property_type', '');
		
		$session_id		= JRequest::getVar('session_id', '');
		//echo $layout;
		//$this->assignRef('plan_id', JRequest::getInt('plan_id', 0) );
		
		$asking_price 	= 	$model->getAskingPriceHTML();
		$psm_price		=	$model->getPSMPriceHTML();
		$psf_price		=	$model->getPSFPriceHTML();
		$size			=	$model->getSizeHTML();
		$no_of_room		=	$model->getNoOfRoomListHTML();
		$no_of_hall		=	$model->getNoOfHallListHTML();
		$study_room		=	$model->getNoOfStudyRoomListHTML();
		$toilet			=	$model->getNoOfToiletListHTML();
		
		$floor_level	=	$model->getFloorLevelListHTML();
		$renovation		=	$model->getRenovationListHTML();
		$status			=	$model->getStatusListHTML();
		$block_no		=	$model->getBlockNoHTML();
		$floor_unit_no	=	$model->getFloorUnitNoHTML();
		$street_name	=	$model->getStreetNameHTML('', $property_type);
		$country		=	$model->getCountryHTML();
		$ad_title		=	$model->getAdTitleHTML();
		$add_info		=	$model->getAddInfoHTML();
		
		$postcode		=	$model->getPostcodeHTML('',$property_type);
		
		$ad_type		=	$model->getAdTypeLISTHTML();
		$rental_type	=	$model->getRentalTypeListHTML();
		
		switch($layout) {
			case 'condo' 	:
				$property_category 	= 	$model->getPropertyCategoryListHTML();
				$property_name		=	$model->getPropertyNameHTML($session_id);
				require('views/postads/tmpl/condo.php');
				break;
			case 'hdb'		:
				$hdb_town_list 	= $model->getHDBTownListHTML();
				$year_built		= $model->getTopYearListHTML('', $property_type);
				$flattype		= $model->getFlatTypeListHTML();
				require('views/postads/tmpl/hdb.php');
				break;
			case 'commercials'	:
				$project_name		=	$model->getProjectNameHTML('', $property_type);
				$property_category 	= 	$model->getPropertyCategoryListHTML('', $property_type);
				$topyear 			= 	$model->getTopYearListHTML('', $property_type);
				$property_district 	= 	$model->getPropertyDistrictHTML('', $property_type);
				$tenure				=	$model->getTenureListHTML('', $property_type);
				require('views/postads/tmpl/commercials.php');
				break;
			case 'landed'	:
				$project_name		=	$model->getProjectNameHTML('', $property_type);
				$property_category 	= 	$model->getPropertyCategoryListHTML('', $property_type);
				$topyear 			= 	$model->getTopYearListHTML('', $property_type);
				$property_district 	= 	$model->getPropertyDistrictHTML('', $property_type);
				$tenure				=	$model->getTenureListHTML('', $property_type);
				require('views/postads/tmpl/landed.php');
				break;
			case 'apartments'	:
				$project_name		=	$model->getProjectNameHTML('', $property_type);
				$property_category 	= 	$model->getPropertyCategoryListHTML('', $property_type);
				$topyear 			= 	$model->getTopYearListHTML('', $property_type);
				$property_district 	= 	$model->getPropertyDistrictHTML('', $property_type);
				$tenure				=	$model->getTenureListHTML('', $property_type);
				require('views/postads/tmpl/apartments.php');
				break;	
			default		:
				echo '';
				break;
		}
		$mainframe->close;
		exit;
	}
	
	
	function ajaxSave() {
		global $mainframe;
		//include_once('assets/json.php');
		
		//$json = new Services_JSON();
		
		$model		= &$this->getModel('postads');
		
		//header('Content-type: application/json');
		
		$tmp 		= $model->saveTmpAdsImage();
		
		/*echo $json->encode(array(	
								"html" => $htmls
							));
		*/
		$mainframe->close;
		exit;
	}
	
	function ajaxLoadAdsTempImage() {
		global $mainframe;
		include_once('assets/json.php');
		
		//make json object...
		
		$json = new Services_JSON();
		
		$model		= &$this->getModel('postads');
		
		header('Content-type: application/json');
		
		$htmls	=	$model->loadAdsTempImageHTML();
		
		echo $json->encode(array(	
								"html" => $htmls
							));
		
		$mainframe->close;
		exit;
	}
	
	function ajaxDeleteAdsTempImage() {
		global $mainframe;
		include_once('assets/json.php');
		
		//make json object...
		
		$json = new Services_JSON();
		
		$model		= &$this->getModel('postads');
		
		header('Content-type: application/json');
		
		$htmls	=	$model->deleteAdsTempImageHTML();
		
		echo $json->encode(array(	
								"is_success" 	=> 	$htmls->is_success,
								"name"			=>	$htmls->name
							));

		$mainframe->close;
		exit;
	}
	
	function ajaxUpdatePrimaryPhotos() {
		global $mainframe;
		include_once('assets/json.php');
		
		//make json object...
		
		$json = new Services_JSON();
		
		$model		= &$this->getModel('ads');
		
		header('Content-type: application/json');
		
		$htmls	=	$model->updatePrimaryPhoto();
		
		/*echo $json->encode(array(	
							));
		*/
		$mainframe->close;
		exit;
	}
	
	 function editads() {
		 JRequest::setVar('view','editads'); 
		 parent::display();
	 }
	 
	 function editing_ads() {
		global $mainframe;
		
		$model		= &$this->getModel('ads');
		
		$ads_id	=	JRequest::getVar('ads_id');
		
		$user =& JFactory::getUser();
	 	$user_id = $user->get('id');
		
		if($model->edit_ads($ads_id)) {
			$msg	=	'Your posting has been edited';
			$this->setRedirect(JRoute::_('index.php?option=com_ads&view=ads_management&Itemid=126&id='.$user_id, false), $msg );
		} else {
			$msg	=	'test1';
			$this->setRedirect( JRoute::_('index.php?option=com_ads&view=editads&selectid='.$ads_id, false), $msg );
			
		}
		
	 }
	 
	 function preads() { /* Preview Ads */
		 JRequest::setVar('view','preads'); 
		 parent::display();
	 }
	 
	 function gobackads() {
		 JRequest::setVar('view','gobackads');
		 parent::display();
	 }
	 
	 function adsdetails() {
		 JRequest::setVar('view','adsdetails');
		 parent::display();
	 }	 
	 
	 function renewSubscription() {
		global $mainframe;
		
		include_once('assets/json.php');
		
		//make json object...
		
		$json = new Services_JSON();
		
		$model		= &$this->getModel('renew');
		
		$status	=	$model->renewSubscription();
		
		header('Content-type: application/json');
		
		echo $json->encode(array("status" => $status	
							));
		
		$mainframe->close;
		exit;
	 }
	 
	 function addFavorite() {
		 global $mainframe;
		
		include_once('assets/json.php');
		
		//make json object...
		
		$json = new Services_JSON();
		
		$model		= &$this->getModel('favorite');
		
		$row	=	$model->add();
		
		header('Content-type: application/json');
		
		echo $json->encode(array("status" => $row->status,
								 "fav_id"	=> $row->fav_id
							));
		
		$mainframe->close;
		exit;
	 }
	 
	 function removeFavorite() {
		 global $mainframe;
		
		include_once('assets/json.php');
		
		//make json object...
		
		$json = new Services_JSON();
		
		$model		= &$this->getModel('favorite');
		
		$row	=	$model->remove();
		
		header('Content-type: application/json');
		
		echo $json->encode(array("status" => $row->status,	
								 "aid" => $row->aid,
								 "user_id" => $row->user_id
							));
		
		$mainframe->close;
		exit;
	 }
	 
	 function renewal() 
	 {
	 	
		$user 	=& JFactory::getUser();

		if ( $user->get('guest')) {
			$this->setredirect('index.php?option=com_user&view=login&Itemid=80');
		} else {
			JRequest::setVar('view', 'renewal');
		}
	
		parent::display();
	 }
	 
	 function favoriteads() 
	 {
	 	
		$user 	=& JFactory::getUser();

		if ( $user->get('guest')) {
			$this->setredirect('index.php?option=com_user&view=login&Itemid=80');
		} else {
			JRequest::setVar('view', 'favoriteads');
		}
	
		parent::display();
	 }

	function refreshConfirmation() {
		global $mainframe;
		
		$db		=	JFactory::getDBO(); 
		
		$user	=	JFactory::getUser();
		$uid	=	$user->get('id');
		
		$adsid	=	JRequest::getVar('adsid');
		
		$model		= &$this->getModel('ads');
		
		$query	=	" SELECT * FROM #__ihouse_ads_refresh " 
					.	" WHERE ads_id = '$adsid' LIMIT 1 ";
					
			$db->setQuery( $query );
			$refresh = $db->loadObject();
		
		$query	=	" SELECT * FROM #__ihouse_ads " 
					.	" WHERE id = '$adsid' LIMIT 1 ";
			$db->setQuery( $query );
			$ads = $db->loadObject();
		
		$howmanytimes = $refresh->value / 2;
		
		require('views/refresh_confirmation/tmpl/default.php');
		
		$mainframe->close();
		exit;
	}
}//end class
?>
