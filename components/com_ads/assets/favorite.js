﻿function addFavorite(ads_id, user_id, imgurl_r,imgurl_a) {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();
	
	var url = 'index.php?option=com_ads&view=addFavorite&r=' + unixtime_ms;		
		var req = new Ajax(url, {
	   					data		: 	
									{
										'ads_id' : ads_id,
										'user_id': user_id
									},
						method		: "get",
		    			onSuccess	: function(data) {
							var obj1 		= Json.evaluate(data);
							var status		=	obj1.status;
							var fav_id		=	obj1.fav_id;
							
							if(status) {
								$('favorite_ajax').setHTML('<img class="icon-img" src="' + imgurl_r + '" />&nbsp;<a class="no_decoration" href="javascript:removeFavorite(\'' + fav_id + '\',\'front\',\'' +imgurl_a + '\',\''+imgurl_r+'\')">不喜欢</a>');
							}
		   				},
						evalScripts : true
				}).request();
}

function removeFavorite(fav_id, where_is, imgurl_a,imgurl_r) {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();
	
	var url = 'index.php?option=com_ads&view=removeFavorite&r=' + unixtime_ms;		
		var req = new Ajax(url, {
	   					data		: 	
									{
										'fav_id' : fav_id
									},
						method		: "get",
		    			onSuccess	: function(data) {
							var obj1 		= Json.evaluate(data);
							var aid			= obj1.aid;
							var user_id		= obj1.user_id;
							
							if(where_is == 'front') {
								$('favorite_ajax').setHTML('<img class="icon-img" src="' + imgurl_a + '" />&nbsp;<a class="no_decoration" href="javascript:addFavorite(\'' + aid + '\',\'' + user_id + '\',\''+imgurl_r+'\',\''+imgurl_a+'\')">最爱</a>');
							} 
							else if(where_is == 'back') {
								window.location.reload();
							}
		   				},
						evalScripts : true
				}).request();
}