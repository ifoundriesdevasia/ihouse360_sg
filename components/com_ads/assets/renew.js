﻿function renewUserSubscription(subs_id) {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();
	
	var answer = confirm("Do you want to renew this subscription?")

	if(answer) {
		var url = 'index.php?option=com_ads&view=renewSubscription&r=' + unixtime_ms;		
		var req = new Ajax(url, {
	   					data		: 	
									{
										'subs_id' : subs_id
									},
						method		: "get",
		    			onSuccess	: function(data) {
							var obj1 		= Json.evaluate(data);
							var status 		= obj1.status;
							if(status) {
								window.location.reload();
							} else {
								alert('Not Enough Credit');
							}
		   				},
						evalScripts : true
				}).request();		
	}
}