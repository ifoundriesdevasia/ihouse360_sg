﻿function propertyType(value, session_id) {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();			
	$('prop_type_loader').setStyle('display','block');
	
	switch(value) {
		case 'hdb' :
		case 'condo' :
		default : 
			var url = 'index.php?option=com_ads&view=ajaxLoadFormPropertyType&layout=' + value + '&r=' + unixtime_ms;		
			var req = new Ajax(url, {
	   					data		: 	
									{
										'property_type' : value,
										'session_id'	: session_id
									},
						method		: "get",
		    			onSuccess	: function(data) {
							
							$('property_type_table').setHTML(data);
							
							$('prop_type_loader').setStyle('display','none');
							
							
							$('ihouseImage').setHTML('<i>This will be loaded after you choose <b>PropertyName</b> on Condo</i>');
							
							if(value == 'condo') {
								$('upload_box_postads').setStyle('display','');
								$('floorplan_box_postads').setStyle('display','');
							} else if(value == 'hdb') {
								$('upload_box_postads').setStyle('display','');
								$('floorplan_box_postads').setStyle('display','none');
							} else if(value == 'landed') {
								$('upload_box_postads').setStyle('display','');
								$('floorplan_box_postads').setStyle('display','none');
							} else if(value == 'commercials') {
								$('upload_box_postads').setStyle('display','');
								$('floorplan_box_postads').setStyle('display','none');	
							} else if(value == 'apartments') {
								$('upload_box_postads').setStyle('display','');
								$('floorplan_box_postads').setStyle('display','none');		
							} else {
								$('upload_box_postads').setStyle('display','none');
								$('floorplan_box_postads').setStyle('display','none');
							}
		   				},
						evalScripts : true
				}).request();				 
			break;
			
	}
	
	/*$('fp1_select').setHTML('<select name="fp1"></select>');
	$('fp2_select').setHTML('<select name="fp2"></select>');
	
	$('fp1_img').setHTML('');
	$('fp2_img').setHTML('');
	*/
}


function loadPropertyId(value, session_id) {
		
		/*$('fp1_img').setHTML('');
		$('fp2_img').setHTML('');	
	*/
		$('prop_name_loader').setStyle('display','block');
	
		var foo = new Date; // Generic JS date object
		var unixtime_ms = foo.getTime();			
		
		var url = 'index.php?option=com_ads&view=ajaxLoadPropertyId&r=' + unixtime_ms;		
		var req = new Ajax(url, {
	   					data		: 	
									{
										'id'		:	value
									},
						method		: "get",
		    			onSuccess	: function(data) {
							
							var obj1 		= Json.evaluate(data);
							var property_id	= obj1.property_id;	
							var district 	= obj1.district_id;
							var postcode 	= obj1.postcode;
							var tenure 		= obj1.tenure;
							var topyear 	= obj1.topyear;
							var streetname	= obj1.streetname;
							var has_fp		= obj1.has_floorplan;
							
							var d1 = '<input type="hidden" value="'+district+'" name="district" /><b>' + district+'</b>';
							var d2 = '<input type="hidden" value="'+postcode+'" name="postcode" /><b>' + postcode+'</b>';
							var d3 = '<input type="hidden" value="'+tenure+'" name="tenure" /><b>' + tenure+'</b>';
							var d4 = '<input type="hidden"  value="'+topyear+'" name="year_built" /><b>' + topyear+'</b>';
							
							var d7 = '<input type="hidden" name="property_id" value="'+property_id+'" />';
							
							var d8 = '<div style="padding:10px;">' +
									'Choose the floorplan you want to add in. You can select up to 2 floorplan' +
									'</div>' +
									'<div>' +
									'<div style="float:left;width:49.5%;background-color:#ccc;text-align:center;">' +
										'<div id="fp1_select"><select name="fp1"></select></div>' +
				    					'<div id="fp1_img"></div>' +
			    					'</div>' +
			    					'<div style="float:right;width:49.5%;background-color:#ccc;text-align:center;">' +
				    					'<div id="fp2_select"><select name="fp2"></select></div>' +
				    					'<div id="fp2_img"></div>' +
			    					'</div>' +
        							'<div style="clear:both"></div>' +
								'</div>';
							
							var d9 =  '<input type="hidden" name="street_name" value="'+streetname+'" /><b>'+streetname+'</b>';
							
							$('property_district_id').setHTML(d1);
							$('postcode_id').setHTML(d2);
							$('tenure_id').setHTML(d3);
							$('topyear_id').setHTML(d4);
							
							$('property_id').setHTML(d7);
							$('street_name_id').setHTML(d9);
							
							if(has_fp) {
								$('whole_fp_box').setHTML(d8);
								loadFPList('fp1',postcode);
							} else {
								$('whole_fp_box').setHTML('<div style="padding:5px">No Floorplan for this particular property</div>');
							}

							loadImagePhotos(postcode, session_id);
							
							$('prop_name_loader').setStyle('display','none');
		   				},
						evalScripts : true
				}).request();				 
	
}

function loadFloorPlanPicture(postcode, name, value) {					
		var foo = new Date; // Generic JS date object
		var unixtime_ms = foo.getTime();			
		
		var url = 'index.php?option=com_ads&view=ajaxLoadFloorplanPicture&r=' + unixtime_ms;		
		var req = new Ajax(url, {
	   					data		: 	
									{
										'postcode' 	: 	postcode,
										'value'		:	value
									},
						method		: "get",
		    			onSuccess	: function(data) {
							var obj1 		= Json.evaluate(data);
							var fp_img 		= obj1.fp;
							
							$(name + '_img').setHTML(fp_img);
							
							if(name == 'fp1') {
								if(value == '') {
									$('fp2_select').setHTML('<select name="fp2" id="fp2"></select> ');
								} else {
									loadFPList('fp2',postcode);
								}
								$('fp2_img').setHTML('');
							}
		   				},
						evalScripts : true
				}).request();				 
	
			
}



function loadFPList(name, postcode) {
						
		var foo = new Date; // Generic JS date object
		var unixtime_ms = foo.getTime();			
		
		var url = 'index.php?option=com_ads&view=ajaxLoadFPList&r=' + unixtime_ms;		
		var req = new Ajax(url, {
	   					data		: 	
									{
										'postcode'		:	postcode,
										'name'			:	name
									},
						method		: "get",
		    			onSuccess	: function(data) {
							var obj1 		= Json.evaluate(data);
							var tmp 		= obj1.fp_select_html;
							
							$(name + '_select').setHTML(tmp);
							
		   				},
						evalScripts : true
				}).request();				 
		
}

function loadImagePhotos(postcode, session_id) {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();			
	$('ihouseImage').setHTML('');
	$('ihouseImage_loader').setStyle('display', '');
	
	var url = 'index.php?option=com_ads&view=ajaxLoadImagePhotos&r=' + unixtime_ms;		
	var req = new Ajax(url, {
	   					data		: 	
									{
										'postcode'		:	postcode,
										'session_id'	:	session_id
									},
						method		: "get",
		    			onSuccess	: function(data) {
							var obj1 		= Json.evaluate(data);
							var tmp 		= obj1.img_html;
							$('ihouseImage_loader').setStyle('display', 'none');
							if(tmp) {
								$('ihouseImage').setHTML(tmp);
							} else {
								$('ihouseImage').setHTML('No Pictures Available');
							}
							
		   				},
						evalScripts : true
				}).request();			
	
}


function primaryPhotoAjax(id, sess_id) {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();		
	
	var url = 'index.php?option=com_ads&view=ajaxUpdatePrimaryPhotos&r=' + unixtime_ms;	
	
	var req = new Ajax(url, {
	   					data		: 	
									{
										'id'			:	id,
										'session_id'	:	sess_id
									},
						method		: "get",
		    			onSuccess	: function(data) {
		   				},
						async: true,
						evalScripts : true
			}).request();
}

function deleteTmpAds(id, name, sess_id) {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();			
	
	var url = 'index.php?option=com_ads&view=ajaxDeleteAdsTempImage&r=' + unixtime_ms;	
	
	var req = new Ajax(url, {
	   					data		: 	
									{
										'id' 			: 	id,
										'session_id' 	:	sess_id,
										'name'			:	name
									},
						method		: "get",
		    			onSuccess	: function(data) {
							var obj1 		= Json.evaluate(data);
							var tmp 		= obj1.is_success;
							var name		= obj1.name;
							
							if(tmp) {
								$('file-' + id).setHTML('');
								$('message_tmp').setHTML('Removed Image : ' + name );
							} else {
								$('message_tmp').setHTML('Error: Removing Images');
							}
							
							
		   				},
						async: true,
						evalScripts : true
				}).request();
}

function agree(id) {
	var val = $('t-' + id).getProperty('value');
	if(val == 0) {
		$('t-' + id).setProperty('value', 1);
	} else {
		$('t-' + id).setProperty('value', 0);
	}
}
function stripCommas(value, id) {
	var StartNumber = value;
	var ReplacedNumber = StartNumber.replace(/\,/g,'');
	$(id).setProperty('value', ReplacedNumber);
}

function formatNumber(value, decimal, id) {
     //decimal  - the number of decimals after the digit from 0 to 3
     //-- Returns the passed number as a string in the xxx,xxx.xx format.
	 var StartNumber = value;
	 var StartNumber = StartNumber.replace(/\,/g,'');
	 var ReplacedNumber = StartNumber.replace(/\t\r\n/g,'');
       anynum=ReplacedNumber;
	   
       divider =10;
       switch(decimal){
            case 0:
                divider =1;
                break;
            case 1:
                divider =10;
                break;
            case 2:
                divider =100;
                break;
            default:       //for 3 decimal places
                divider =1000;
        } 

       workNum=Math.abs((Math.round(anynum*divider)/divider)); 

       workStr=""+workNum 

       if (workStr.indexOf(".")==-1){workStr+="."} 

       dStr=workStr.substr(0,workStr.indexOf("."));dNum=dStr-0
       pStr=workStr.substr(workStr.indexOf(".")) 

       while (pStr.length-1< decimal){pStr+="0"} 

       if(pStr =='.') pStr =''; 

       //--- Adds a comma in the thousands place.    
       if (dNum>=1000) {
          dLen=dStr.length
          dStr=parseInt(""+(dNum/1000))+","+dStr.substring(dLen-3,dLen)
       } 

       //-- Adds a comma in the millions place.
       if (dNum>=1000000) {
          dLen=dStr.length
          dStr=parseInt(""+(dNum/1000000))+","+dStr.substring(dLen-7,dLen)
       }
	   
	   //-- Adds a comma in the billions place.
       if (dNum>=1000000000) {
          dLen=dStr.length
          dStr=parseInt(""+(dNum/1000000000))+","+dStr.substring(dLen-11,dLen)
       }
	   
	   //-- Adds a comma in the trillions place.
       if (dNum>=1000000000000) {
          dLen=dStr.length
          dStr=parseInt(""+(dNum/1000000000000))+","+dStr.substring(dLen-15,dLen)
       }
	   
       retval = dStr + pStr
	   
	  
       //-- Put numbers in parentheses if negative.
    if (anynum<0) {retval="("+retval+")";} 

	if(retval == 0) {
		   retval = '';
	}
	  
	  
	$(id).setProperty('value', retval);
}

function isNumberKey(evt)
{
	var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
       return false;
	}
		 
    return true;
}

function calculate_persquare() {
	var askingprice = $('asking_price').getProperty('value');
	var size		= $('size').getProperty('value'); /* psf */
	
	if(askingprice && size) {
		var psm_price 	= $('psm_price');
		var psf_price 	= $('psf_price');
		
		var askprice_int = askingprice.replace(/\,/g,'');
		var size_int = size.replace(/\,/g,'');

		psf_price 		= parseInt(askprice_int / size_int);
		psm_price		= parseInt(psf_price * 10.764);
		
		$('psm_price').setProperty('value',psm_price);
		$('psf_price').setProperty('value',psf_price);
		$('psm_price_txt').setHTML(psm_price);
		$('psf_price_txt').setHTML(psf_price);
		//$('psm_price_t').setProperty('value',psm_price);
		//$('psf_price_t').setProperty('value',psf_price);
		
	}
}




function publishads(publish) {
	$('publish_ads').setProperty('value', publish);
	document.publish_ads_form.submit();
}

function gobackads(actionurl) {
	var forms = $('publish_ads_form');
	forms.setProperty('action', actionurl);
	document.publish_ads_form.submit();
}