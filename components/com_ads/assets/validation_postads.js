﻿function validateForm(postadtype, actionurl) {
	var prop_type 	= $('property_type');
	
	/* Condo */
	var prop_cat	= $('property_category');
	var prop_name	= $('property_name');
	
	/* HDB */
	var hdb_town	= $('hdb_town');
	var street_name_hdb	= $('street_name_hdb');
	
	/* Commercial || Landed*/
	var proj_name	=	$('project_name');
	//var street_name =	$('street_name');
	
	var postal		= $('postcode');
	
	var ad_title 	= $('ad_title');

	var flattype	= $('flattype');
	var ad_type		= $('ad_type');
	var status		= $('status');
	
	var agree1 		= $('t-agree1');
	var agree2 		= $('t-agree2');
	
	var how 		= true;
	var errormsg 	=	'';
	
	$('property_typemsg').setStyle('color','');
	$('err-agree1').setStyle('color','');

	if(prop_type) {
		$('property_typemsg').setStyle('color','');
		var t = prop_type.getProperty('value');
		if(t == '') {
			var errormsg = errormsg + ' - Property Type \n';
			$('property_typemsg').setStyle('color','red');
			how = false;
			
		}
	}
	
	
	if(prop_cat) {
		$('property_categorymsg').setStyle('color','');
		var t = prop_cat.getProperty('value');
		if(t == '') {
			var errormsg = errormsg + ' - Property Category \n';
			$('property_categorymsg').setStyle('color','red');
			how = false;
		}
	}
	
	if(prop_name) {
		$('property_namemsg').setStyle('color','');
		var t = prop_name.getProperty('value');
		if(t == '') {
			var errormsg = errormsg + ' - Property Name \n';
			$('property_namemsg').setStyle('color','red');
			how = false;
		}
	}
	
	if(proj_name) {
		$('project_namemsg').setStyle('color','');
		var t = proj_name.getProperty('value');
		if(t == '') {
			var errormsg = errormsg + ' - Project Name \n';
			$('project_namemsg').setStyle('color','red');
			how = false;
		}
	}
	
	if(hdb_town) {
		$('hdb_estatemsg').setStyle('color','');
		var t = hdb_town.getProperty('value');
		if(t == '') {
			var errormsg = errormsg + ' - HDB Name \n';
			$('hdb_estatemsg').setStyle('color','red');
			how = false;
		}
	}
	
	if(flattype) {
		$('flat_typemsg').setStyle('color','');
		var t = flattype.getProperty('value');
		if(t == '') {
			var errormsg = errormsg + ' - Flat Type \n';
			$('flat_typemsg').setStyle('color','red');
			how = false;
		}
	}
	
	if(postal) {
		$('postal_codemsg').setStyle('color','');
		var t = postal.getProperty('value');
		if(t == '') {
			var errormsg = errormsg + ' - Postal Code \n';
			$('postal_codemsg').setStyle('color','red');
			how = false;
		}
	}
	
	/*if(street_name) {
		$('street_name_msg').setStyle('color','');
		var t = street_name.getProperty('value');
		if(t == '') {
			$('street_name_msg').setStyle('color','red');
			how = false;
		}
	}
	*/
	
	if(street_name_hdb) {
		$('street_name_hdbmsg').setStyle('color','');
		var t = street_name_hdb.getProperty('value');
		if(t == '') {
			var errormsg = errormsg + ' - Street Name HDB \n';
			$('street_name_hdbmsg').setStyle('color','red');
			how = false;
		}
	}
	
	if(ad_title) {
		$('ad_titlemsg').setStyle('color','');
		var t = ad_title.getProperty('value');
		if(t == '') {
			var errormsg = errormsg + ' - Ad Title \n';
			$('ad_titlemsg').setStyle('color','red');
			how = false;
		}
	}
	
	if(ad_type) {
		$('ad_typemsg').setStyle('color','');
		var t = ad_type.getProperty('value');
		if(t == '') {
			var errormsg = errormsg + ' - Ad Type \n';
			$('ad_typemsg').setStyle('color','red');
			how = false;
		}
	}
	
	/*if(status) {
		$('statusmsg').setStyle('color','');
		var t = status.getProperty('value');
		if(t == '') {
			$('statusmsg').setStyle('color','red');
			how = false;
		}
	}
	*/
	if(prop_type == '') {
		$('property_typemsg').setStyle('color','red');
		how = false;
	}
	
	if(agree1) {
		$('err-agree1').setStyle('color','');
		var t = agree1.getProperty('value');
		if(t == 0) {
			var errormsg = errormsg + ' - Agreement 1 \n';
			$('err-agree1').setStyle('color','red');
			how = false;
		}
	}
	
	if(agree2) {
		$('err-agree2').setStyle('color','');
		var t = agree2.getProperty('value');
		if(t == 0) {
			var errormsg = errormsg + ' - Agreement 2 \n';
			$('err-agree2').setStyle('color','red');
			how = false;
		}
	}

	var forms = $('edit_refresh_ads_form');
	forms.setProperty('action', actionurl);
			
	if(how) {
		document.edit_refresh_ads_form.submit();
	} else {
		var html = 'Please fill the following : \n\n';
		html = html + errormsg;
		alert(html);
	}
}