function languagehTab(mode) {
	$$('.tabular4').each(function(el) {						  
		el.setStyle('display','none');
	});
	
	$$('ul.tab_nav222 li').each(function(el){
		el.removeClass('tabactive222');							 
	});
	
	$('tab_li222_' + mode).addClass('tabactive222');

	$('tab_' + mode).setStyle('display','');
}

function languageagentTab(mode) {
	$$('.tabular4').each(function(el) {						  
		el.setStyle('display','none');
	});
	
	$$('ul.tab_nav995 li').each(function(el){
		el.removeClass('tabactive995');							 
	});
	
	$('tab_li995_' + mode).addClass('tabactive995');

	$('tab_' + mode).setStyle('display','');
}

function languagerTab(mode) {
	$$('.tabular4').each(function(el) {						  
		el.setStyle('display','none');
	});
	
	$$('ul.tab_nav999 li').each(function(el){
		el.removeClass('tabactive999');							 
	});
	
	$('tab_li999_' + mode).addClass('tabactive999');

	$('tab_' + mode).setStyle('display','');
}

function languageiTab(mode) {
	$$('.tabular4').each(function(el) {						  
		el.setStyle('display','none');
	});
	
	$$('ul.tab_nav999 li').each(function(el){
		el.removeClass('tabactive999');							 
	});
	
	$('tab_li999_' + mode).addClass('tabactive999');

	$('tab_' + mode).setStyle('display','');
}

function languageaTab(mode) {
	$$('.tabular5').each(function(el) {						  
		el.setStyle('display','none');
	});
	
	$$('ul.tab_nav998 li').each(function(el){
		el.removeClass('tabactive998');							 
	});
	
	$('tab_li998_' + mode).addClass('tabactive998');

	$('tab_' + mode).setStyle('display','');
}

function languagemTab(mode) {
	$$('.tabular6').each(function(el) {						  
		el.setStyle('display','none');
	});
	
	$$('ul.tab_nav997 li').each(function(el){
		el.removeClass('tabactive997');							 
	});
	
	$('tab_li997_' + mode).addClass('tabactive997');

	$('tab_' + mode).setStyle('display','');
}
function languagebTab(mode) {
	$$('.tabular7').each(function(el) {						  
		el.setStyle('display','none');
	});
	
	$$('ul.tab_nav996 li').each(function(el){
		el.removeClass('tabactive996');							 
	});
	
	$('tab_li996_' + mode).addClass('tabactive996');

	$('tab_' + mode).setStyle('display','');
}

function featuredAdsInfoTab(mode) {
	$$('.tabular3').each(function(el) {						  
		el.setStyle('display','none');
	});
	
	$$('ul.tab_nav99 li').each(function(el){
		el.removeClass('tabactive99');							 
	});
	
	$('tab_li99_' + mode).addClass('tabactive99');

	$('tab_' + mode).setStyle('display','');
}

function adsListingInfoTab(mode) {
	$$('.tabular2').each(function(el) {						  
		el.setStyle('display','none');
	});
	
	$$('ul.tab_nav77 li').each(function(el){
		el.removeClass('tabactive77');							 
	});
	
	$('tab_li77_' + mode).addClass('tabactive77');

	$('tab_' + mode).setStyle('display','');
}

function userRegisterInfoTab(mode) {
	$$('.tabular1').each(function(el) {						  
		el.setStyle('display','none');
	});
	
	$$('ul.tab_nav88 li').each(function(el){
		el.removeClass('tabactive88');							 
	});
	
	$('tab_li88_' + mode).addClass('tabactive88');

	$('tab_' + mode).setStyle('display','');
}

function profileDetailInfoTab(mode) {
	$$('.tabular1').each(function(el) {						  
		el.setStyle('display','none');
	});
	
	$$('ul.tab_nav5 li').each(function(el){
		el.removeClass('tabactive5');							 
	});
	
	$('tab_li5_' + mode).addClass('tabactive5');

	$('tab_' + mode).setStyle('display','');
}

function propertyInfoTab(mode) {
	$$('.prop_property_detail').each(function(el) {
		el.setStyle('display','none');

	});
	$('prop_' + mode).setStyle('display','');
}

function propertyDetailTab(mode) {
	$$('ul.tab_nav3 li').each(function(el){
		el.removeClass('tabactive3');							 
	});
	
	switch(mode) {
		case 'image':
			
			$('tab_li3_image').addClass('tabactive3');
			
			$('property_detail_image').setStyle('display','');
			$('floor_plan_image').setStyle('display','none');
			$('google_map_image').setStyle('display','none');
			break;
		case 'floorplan':
			$('tab_li3_floorplan').addClass('tabactive3');
		
			$('floor_plan_image').setStyle('display','');
			$('property_detail_image').setStyle('display','none');
			$('google_map_image').setStyle('display','none');
			break;
		case 'googlemap':
			geocodeGoogleMap();
		
			$('tab_li3_googlemap').addClass('tabactive3');
		
			$('google_map_image').setStyle('display','');
			$('floor_plan_image').setStyle('display','none');
			$('property_detail_image').setStyle('display','none');
			
			
			break;
	}
}

function displayFloorPlanTypeTabs(roomtype) {
	$$('.displFloorPlan').setStyle('display','none');
	$(roomtype + '_' + postcode).setStyle('display', '');
	
	$$('ul.tab_nav4 li').each(function(el){
		el.removeClass('tabactive4');							 
	});
	
	$('bed_' + roomtype).addClass('tabactive4');
	
}

function deletePolicyNo(id) {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();
	
	var url = 'index.php?option=com_insureasia&task=delete_policy_no&r=' + unixtime_ms;	
	var req = new Ajax(url, {
	   			data		: 	
								{
									'item_id' 	: id
								},
				method		: "post",
				onRequest	: function() {
					/*$$('.ajax-loader').setStyle('display', '');*/
				},
	    		onComplete	: function(response) {
					/*$$('.ajax-loader').setStyle('display', 'none');*/
					$('policy_' + id).setHTML('');
					$('ajax_msg').setHTML(response);
	   			},
				evalScripts: true
		}).request();
}

function toggle(id) {
	
	var t = $(id).getStyle('display');

	if(t == '') {
		$(id).setStyle('display', 'none');
	} else {
		$(id).setStyle('display','');
	}
	
}

function movePage(mode) {
	
	switch(mode) {
		case 'prev' :
			$('sliders_pt_lft').setStyle('display','none');
			$('sliders_pt_rgt').setStyle('display','');
			
			$('propertyHighlightDetail').setStyle('left','0');
			break;
		case 'next'	:
			$('sliders_pt_lft').setStyle('display','');
			$('sliders_pt_rgt').setStyle('display','none');
			
			$('propertyHighlightDetail').setStyle('left','-616px');
			break;
	}
	
	
}

function movePage1(mode) {
	
	switch(mode) {
		case 'prev' :
			$('sliders_pt_lft1').setStyle('display','none');
			$('sliders_pt_rgt1').setStyle('display','');
			
			$('propertyHighlightDetail1').setStyle('left','0');
			break;
		case 'next'	:
			$('sliders_pt_lft1').setStyle('display','');
			$('sliders_pt_rgt1').setStyle('display','none');
			
			$('propertyHighlightDetail1').setStyle('left','-616px');
			break;
	}
	
	
}


function movePage2(mode) {
	
	switch(mode) {
		case 'prev' :
			$('sliders_pt_lft2').setStyle('display','none');
			$('sliders_pt_rgt2').setStyle('display','');
			
			$('propertyHighlightDetail2').setStyle('left','0');
			break;
		case 'next'	:
			$('sliders_pt_lft2').setStyle('display','');
			$('sliders_pt_rgt2').setStyle('display','none');
			
			$('propertyHighlightDetail2').setStyle('left','-616px');
			break;
	}
	
	
}

