function emailFormPopup(email, ads_id, uid) {
	
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();			
	//alert(ads_id);
	var url = 'index.php?option=com_ads&view=emailContactForm&r=' + unixtime_ms;		
	var req = new Ajax(url, {
	   					data		: 	
									{
										'email' : email,
										'ads_id': ads_id,
										'uid'	: uid
									},
						method		: "get",
		    			onSuccess	: function(data) {
								$('popupForm').setStyle('display','');
								$('popupForm').setHTML(data);
								
		   				},
						evalScripts : true
				}).request();	
}

function submitEmailFormPopup(urls, email) {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();
	
	var loader = $('ajax-submit-form-email');
	loader.setStyle('display','');
	$('emailform').setStyle('display','none');

	$('emailFormContact').send({
		onComplete	:	function(data) {
			var obj1 		= 	Json.evaluate(data);
			
			loader.setStyle('display','none');
			$('text-message-sbm').setHTML('Thank you for your submission. <a href="javascript:closeEmailFormPopup();">Click to close</a>');
		}
	});

}

function closeEmailFormPopup() {
	$('popupForm').setHTML('');
	$('popupForm').setStyle('display','none');
}