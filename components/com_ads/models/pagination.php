<?php
/**
* @version		$Id: poll.php 10752 2008-08-23 01:53:31Z eddieajau $
* @package		Joomla
* @subpackage	Polls
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class AdsModelPagination extends JModel
{
	/*function __construct()
	{
		global $mainframe;

		parent::__construct();

	}*/
	
	function getPagination($total, $limit) {
		
		$db 			= $this->getDBO();
		
		$page			=	JRequest::getVar('page', 0);
		$property_id	=	JRequest::getVar('property_id', 0);
		
		$how_many		=	$total / $limit; 
		
		$html 			= 	'';
		
		$html .= '<span class="pagination">';
		
		for($i = 0; $i < $how_many; $i++) {
			$html .= '
    			<strong style="padding:0 5px;">';
			
			if($page == $i)
				$html .= '<span id="inactive">';
			else
				$html .= '<a title="'.($i+1).'" href="javascript:getPropertyReview('.$i.','.$property_id.');">';
			
			
			$html .=	($i+1);
			
			
			if($page == $i)
				$html .= '</span>';
			else
				$html .= '</a>';
               
		}
        $html .= '</span>';
			
		return $html;		
			
	}
	
	function getPaginationAdsListing($total, $limit, $usercat, $filter, $orderfilter) {
		$db 			= $this->getDBO();
		
		$page			=	JRequest::getVar('page', 0);
		$filter					=	JRequest::getVar('filter', '');
		$orderfilter			=	JRequest::getVar('orderfilter', '');
		
		$how_many		=	$total / $limit; 
		
		if(!empty($filter) || !empty($orderfilter)) {
			$orderby	= " ORDER BY ".$filter." ".$orderfilter." ";
		}

		$html 			= 	'';
		
		$html .= '<span class="pagination">';
		
		if($page <= 5) {
			$min_page = 0;
			if($how_many >= 10)
				$max_page = 10;
			else
				$max_page = $how_many;
				
		} else if($page >= ($how_many - 5)){
			$min_page = $page - 5;
			$max_page = $how_many;
		} else {
			$min_page = $page - 5;
			$max_page = $page + 5;
		}
		
		for($i = $min_page; $i < $max_page; $i++) {
			$html .= '
    			<strong style="padding:0 5px;">';
			
			if($page == $i)
				$html .= '<span id="inactive">';
			else
				$html .= '<a title="'.($i+1).'" href="javascript:getAds('.$i.',\''.$usercat.'\',\''.$filter.'\',\''.$orderfilter.'\');">';
			
			
			$html .=	($i+1);
			
			
			if($page == $i)
				$html .= '</span>';
			else
				$html .= '</a>';
				
			$html .= '</strong>';	
               
		}
        $html .= '</span>&nbsp;&nbsp;';
		$html .= ($page+1).' of '.ceil($how_many).' pages';	
		return $html;	
	}
}