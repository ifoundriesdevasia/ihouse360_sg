﻿<?php
/**
* @version		$Id: poll.php 10752 2008-08-23 01:53:31Z eddieajau $
* @package		Joomla
* @subpackage	Credits
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class AdsModelRenew extends JModel
{
	function renewSubscription() {
		global $mainframe;
		$db = & JFactory::getDBO();
		
		$user =& JFactory::getUser();
		$user_id = $user->get('id');
		
		$subs_id	=	JRequest::getVar('subs_id');

		/* Check Credit */
		$sql = " SELECT credit FROM #__ihouse_users_credit WHERE user_id = '$user_id' ";
			$db->setQuery($sql);
			$credit = (int) $db->loadResult();
		
		
		$query = " SELECT * FROM #__cbsubs_subscriptions AS cb "
					.	" LEFT JOIN #__cbsubs_plans AS cp ON cp.id = cb.plan_id "
					.	" WHERE cb.id = '$subs_id' LIMIT 1 ";
					
			$db->setQuery( $query );
			$t = $db->loadObject(); 
			
		$validity 	= 	$t->validity;
		$deduct_by	=	$t->rate;
		$planname	=	$t->name;
		$expirydate =	$t->expiry_date;
		$planid 	=	$t->plan_id;
		
		if($planid == '2' || $planid == '3') {
			$sql = ' SELECT params FROM #__ihouse_promotion WHERE id = 2 LIMIT 1';
				$db->setQuery($sql);
				$param = $db->loadResult();
		} else {
			$sql = ' SELECT params FROM #__ihouse_promotion WHERE id = 4 LIMIT 1';
				$db->setQuery($sql);
				$param = $db->loadResult();
		}
		
		$discount 	= str_replace('discount=','',$param);

		switch($planid) { 
			// 4-week will pay only 3 weeks
			case 11: // 4
			case 15: // 5
			case 19: // 6
			case 23: // 7
				$rates = $deduct_by * 0.75;
				break;
			default :
				$rates = $deduct_by;
				break;
		}

		$deduct_by 	= ( 1 - (float) $discount) * (int) $rates ;
		
		if($credit < $deduct_by)
			return false;

		$date 	= explode("-", $validity);
		$year 	= $date[0];
		$month 	= $date[1];
		$day 	= substr($date[2], 0, 2);

		$eH		= date('H', strtotime($expirydate));
		$ei		= date('i', strtotime($expirydate)) + 1;
		$es		= date('s', strtotime($expirydate));
		$em		= date('m', strtotime($expirydate)) + $month;
		$ed		= date('d', strtotime($expirydate)) + $day;
		$eY		= date('Y', strtotime($expirydate)) + $year;
		
		$now		= date('Y-m-d H:i:s', time());
		$expirydate = date("Y-m-d H:i:s", mktime($eH,$ei,$es,$em,$ed,$eY));			

		$sql = " UPDATE #__cbsubs_subscriptions SET subscription_date = '$now', " 
				.	" expiry_date = '$expirydate' WHERE id = '$subs_id' ";
				
			$db->setQuery($sql);
			$db->query();
		
		$sql = " UPDATE #__ihouse_users_credit SET credit = credit - ".$deduct_by." WHERE user_id = '".$user_id."'";
			$db->setQuery($sql);
			$db->query();
		
		$balance = (int) $credit - $deduct_by;
				
		$sql = "INSERT INTO #__users_transaction_log VALUES (NULL, '".$user_id."', 'Renew : ".$planname."', '0', '".$deduct_by."', '".$balance."', CURRENT_TIMESTAMP)";
			$db->setQuery($sql);
			$db->query();
		
		/* 28/11/2012 */
		/* Current Ads  in database with subs_id */
		$sql = " SELECT COUNT(id) FROM #__ihouse_ads WHERE subscription_id = '$subs_id' ";
			$db->setQuery( $sql );
			$total_current_ads = $db->loadResult();
		/* Update Ads Subscription Slots minus Current Ads */
		$total = 100 - (int) $total_current_ads;
		
		$sql = " UPDATE #__ihouse_ads_subscription_slot SET slot = '$total' WHERE user_id = '$user_id' ";
			$db->setQuery( $sql );
			$db->query();
			
		return true;
	}

	
	
	
}
