﻿<?php
/**
* @version		$Id: poll.php 10752 2008-08-23 01:53:31Z eddieajau $
* @package		Joomla
* @subpackage	Credits
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class AdsModelFavorite extends JModel
{
	function add() {
		
		global $mainframe;
		$db = & JFactory::getDBO();
		
		$ads_id		=	JRequest::getVar('ads_id');
		$user_id 	= 	JRequest::getVar('user_id');
		
		$sql = " INSERT INTO #__ihouse_favorites VALUES(NULL, 'ads', '$user_id', '$ads_id') ";
			$db->setQuery($sql);
			$db->query();
		
		$fav_id	=	$db->insertid();
		
		$row	=	new StdClass;
		$row->status	=	true;
		$row->fav_id		=	$fav_id;
		
		return $row;
	}

	function remove() {
		
		global $mainframe;
		$db = & JFactory::getDBO();
		
		$fav_id	=	JRequest::getVar('fav_id');
		
		$sql = " SELECT aid, user_id FROM #__ihouse_favorites WHERE id = '$fav_id' ";
			$db->setQuery($sql);
			$tmp = $db->loadObject();
		
		$sql = " DELETE FROM #__ihouse_favorites WHERE id = '$fav_id' ";
			$db->setQuery($sql);
			$db->query();
		
		$tmp->status = true;
		
		return $tmp;
	}
	
}
