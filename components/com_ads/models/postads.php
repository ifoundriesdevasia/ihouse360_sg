﻿<?php
/**
* @version		$Id: poll.php 10752 2008-08-23 01:53:31Z eddieajau $
* @package		Joomla
* @subpackage	Polls
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class AdsModelPostads extends JModel
{
	function __construct()
	{
		global $mainframe;

		parent::__construct();

	}
		
	function saveTmpAdsImage() 
	{
		jimport('joomla.filesystem.file');
		
		$db = $this->getDBO();
		
		$file 			= 	JRequest::getVar('imgFile','','files','array');
		
		$sess_id		=	JRequest::getVar('session_id');
		$watermark		=	JRequest::getVar('watermark_use'); /* 0 = text, 1 = ihouse logo */

		for($i = 0;$i<count($file['size']);$i++) {
			$name			=	$file['name'][$i];
			$type			=	$file['type'][$i];
			$tmp_name 		=	$file['tmp_name'][$i];
			$size			=	$file['size'][$i];
		
			/* BEFORE PROCESS MUST COUNT - AT MAX 8 */
			$query	=	" SELECT COUNT(id) FROM #__ihouse_ads_image WHERE sess_id = '$sess_id'"; 
				$db->setQuery( $query );
				$count	=	(int) $db->loadResult();
			/* JUST WANT TO MAKE SURE TOTAL UPLOAD NOT > 8 */
			$restriction	=	$count + 1; 
		
			if($restriction > 8) {
				return;
			}
			
  			if ($name) {
				
  				$filename = JFile::makeSafe($name);
        		$extension = $this->_getExtension($filename);
				$extension = strtolower($extension);
				
 				if (($extension != "jpg") && ($extension != "jpeg") && 
					($extension != "png") && ($extension != "gif")) {

  				} else {
   					$size = filesize($tmp_name);
					
 					/*if ($size > (400*1024)) {
 						return false;
					}*/
					
					
 					$uploadedfile = $tmp_name;
					
					if($extension=="jpg" || $extension=="jpeg" )
					{
						$src = imagecreatefromjpeg($uploadedfile);
					} else if($extension=="png") {
						$src = imagecreatefrompng($uploadedfile);
					} else {
						$src = imagecreatefromgif($uploadedfile);	
					}
 
					list($width,$height) = getimagesize($uploadedfile);
					
					if($width > 477 && $height > 358) {
						if($width > $height) {
							$newwidth = 477;
							$newheight = ($height/$width)*$newwidth;
							$tmp = imagecreatetruecolor($newwidth,$newheight);
						} else if($width < $height) {
							$newheight = 358;
							$newwidth = ($width/$height)*$newheight;
							$tmp = imagecreatetruecolor($newwidth,$newheight);
						}
					} else if($width <= 477 && $height <= 358) {
						$tmp 		= imagecreatetruecolor($width,$height);
						$newwidth 	= $width;
						$newheight 	= $height;
					} else if($width > 477) {
						$ratio =  477 / $width;
      					$height = $height * $ratio;
					    $tmp = imagecreatetruecolor(477, $height);
						$newwidth = 477;
						$newheight = $height;
					} else if($height > 358) {
						$ratio =  358 / $height;
						$width	=	$width * $ratio;
						$tmp = imagecreatetruecolor($width, 358);
						$newwidth = $width;
						$newheight = 358;
					} 
 
 					$newwidth1 = 80;
					$newheight1 = ($height/$width)*$newwidth1;
					$tmp1 = imagecreatetruecolor($newwidth1,$newheight1);
					
					$newwidth2 = 156;
					$newheight2 = ($height/$width)*$newwidth2;
					$tmp2 = imagecreatetruecolor($newwidth2,$newheight2);
					
					// For Listing Purpose + Centralize Picture
					$im = imagecreatetruecolor(477, 358);	
					$white = imagecolorallocate($im, 255, 255, 255);
					imagefill($im, 0, 0, $white);

					imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);
					imagecopyresampled($tmp1,$src,0,0,0,0,$newwidth1,$newheight1,$width,$height);
					imagecopyresampled($tmp2,$src,0,0,0,0,$newwidth2,$newheight2,$width,$height);

					imagecopymerge($im, $tmp, ((477-$newwidth)/2), ((358-$newheight)/2), 0,0 , $newwidth, $newheight , 100);
					
					$watermark_file = JRoute::_('templates/mapsearch/images/logo1.gif');
					$watermark_src	=	imagecreatefromgif($watermark_file);
					list($watermark_w, $watermark_h) = getimagesize($watermark_file);

					$newwatermark_w1 = 40;
					$newwatermark_h1 = ($watermark_h/$watermark_w)*$newwatermark_w1;
					$tmp1_wmrk = imagecreatetruecolor($newwatermark_w1,$newwatermark_h1);
					
					$newwatermark_w2 = 80;
					$newwatermark_h2 = ($watermark_h/$watermark_w)*$newwatermark_w2;
					$tmp2_wmrk = imagecreatetruecolor($newwatermark_w2,$newwatermark_h2);
					
					imagecopyresampled($tmp1_wmrk,$watermark_src,0,0,0,0,$newwatermark_w1,$newwatermark_h1,$watermark_w,$watermark_h);
					imagecopyresampled($tmp2_wmrk,$watermark_src,0,0,0,0,$newwatermark_w2,$newwatermark_h2,$watermark_w,$watermark_h);
					
					$placement_x = 477 - $watermark_w;
					$placement_y = 358 - $watermark_h;
					
					$placement_x1 = $newwidth1 - $newwatermark_w1;
					$placement_y1 = $newheight1 - $newwatermark_h1;
					
					$placement_x2 = $newwidth2 - $newwatermark_w2;
					$placement_y2 = $newheight2 - $newwatermark_h2;
					
					imagecopymerge($im, $watermark_src,$placement_x,$placement_y,0,0,$watermark_w, $watermark_h,20); 
					imagecopymerge($tmp1, $tmp1_wmrk,$placement_x1,$placement_y1,0,0,$newwatermark_w1, $newwatermark_h1,10);
					imagecopymerge($tmp2, $tmp2_wmrk,$placement_x2,$placement_y2,0,0,$newwatermark_w2, $newwatermark_h2,10);
					
					if($watermark == '0') {
						
						$watermark_txt		=	JRequest::getVar('wmrk_txt');
						// 255, 140, 0 orange
						$orange 				= 	imagecolorallocate($tmp, 255, 140, 0);
						$orange1 			= 	imagecolorallocate($tmp1, 255, 140, 0);
						$orange2 			= 	imagecolorallocate($tmp2, 255, 140, 0);
						
						$font_text 			= 	JRoute::_('templates/main/font/FZ-CAIYUN.ttf');
						
						$font = 30;
						$font1 = 4;
						$font2 = 10;
						
						$text_width 	= 22 * strlen($watermark_txt);
						$text_width1 	= 3 * strlen($watermark_txt);
						$text_width2 	= 8 * strlen($watermark_txt);
						
						$text_height 	= $font;
						$text_height1 	= $font1;
						$text_height2 	= $font2;
						
						// align it to center
						$xwidth		= ceil((477 - $text_width) / 2);
						$xwidth1	= ceil(($newwidth1 - $text_width1) / 2);
						$xwidth2	= ceil(($newwidth2 - $text_width2) / 2);
						
						$xheight	= ceil((358 - $text_height) / 2);
						$xheight1	= ceil(($newheight1 - $text_height1) / 2);
						$xheight2	= ceil(($newheight2 - $text_height2) / 2);
						
   						imagettftext($im, $font, 0, $xwidth, $xheight, $orange, $font_text, $watermark_txt);
						imagettftext($tmp1, $font1, 0, $xwidth1, $xheight1, $orange1, $font_text, $watermark_txt);
						imagettftext($tmp2, $font2, 0, $xwidth2, $xheight2, $orange2, $font_text, $watermark_txt);
						
						$user		=& JFactory::getUser();	
						$uid		=	$user->get('id');
						
						$query		=	" SELECT COUNT(id) FROM #__ihouse_watermark WHERE user_id = '$uid' ";
							$db->setQuery( $query );
							$exists = $db->loadResult();
							
						if(!empty($exists)) {
							$query		=	" UPDATE #__ihouse_watermark SET word = '$watermark_txt' WHERE user_id = '$uid' ";
							$db->setQuery( $query );
							$db->query();
						} else {
							$query		=	" INSERT INTO #__ihouse_watermark (user_id, word) VALUES('$uid','$watermark_txt') ";
							$db->setQuery( $query );
							$db->query();
							
						}
					}
					
					$tmp_path		= 	JRoute::_('images/ihouse/ads_images');
					$dir			=	$tmp_path.'/'.$sess_id;
					
					if(!is_dir($dir)) 
						mkdir($dir, 0777,true);
					
					$filename	=	preg_replace('/\.'.$extension.'$/','',$filename);
					$flname		= 	preg_replace('/(\s|\.|\'|\#|\@|\!|\%|\&)+/','-',strtolower($filename));
					$flname		=	$flname.'.'.$extension;
					
					$filename 	= $dir."/". $flname;
					$filename2	= $dir."/". "small-".$flname;
					$filename3	= $dir."/". "med-".$flname;
					
					imagejpeg($im,$filename,100);
					imagejpeg($tmp1,$filename2,100);
					imagejpeg($tmp2,$filename3,100);
					
					chmod($filename, 0777);
					chmod($filename2, 0777);
					chmod($filename3, 0777);
					
					imagedestroy($src);
					imagedestroy($im);
					imagedestroy($tmp1);
					imagedestroy($tmp2);	
					
					$query		=	" SELECT COUNT(id) FROM #__ihouse_ads_image WHERE sess_id = '$sess_id' ";
						$db->setQuery( $query );
						$counts = $db->loadResult();
					
					// for image cover 
					if(empty($counts)) {
						$query		=	" INSERT INTO #__ihouse_ads_image VALUES(NULL,'$sess_id',0,'$flname', 1); ";
							$db->setQuery( $query );
							$db->query();	
					} else {
						$query		=	" INSERT INTO #__ihouse_ads_image VALUES(NULL,'$sess_id',0,'$flname', 0); ";
							$db->setQuery( $query );
							$db->query();
					}
					
				}
			} // $name[$i]
		} // for looop
		return;
		//If no errors registred, print the success message	
	}
	
	function loadAdsTempImageHTML() {
		$db = $this->getDBO();
		
		$sess_id		=	JRequest::getVar('session_id');
		
		$query	=	" SELECT * FROM #__ihouse_ads_image WHERE sess_id = '$sess_id' ";
			$db->setQuery( $query );
			$result	=	$db->loadObjectList();
		
		$html = '';
		
		foreach($result as $r) :
			$r->sess_id = $sess_id;
		
			$html .= '<div id="file-'.$r->id.'" style="float:left;margin:1px;">';
			$html .= '<div><img src="'.JRoute::_('images/ihouse/ads_images/'.$r->sess_id.'/'.$r->name).'" width="159px" height="126px" />';
    		$html .= '</div>';
			$html .= '<div style="text-align:center;" ><input type="radio" name="primary_photo" value="'.$r->id.'" onclick="javascript:primaryPhotoAjax(this.value,\''.$r->sess_id.'\')" '.(($r->is_primary)?'checked="yes"':'').' />Cover Photo<br /><input type="image" src="'.JRoute::_('templates/main/images/btn-delete.png').'" alt="Delete now!" onclick="javascript:deleteTmpAds('.$r->id.',\''.$r->name.'\',\''.$r->sess_id.'\');return false;" /></div>';
			$html .= '</div>';	
			
		endforeach;
		
		return $html;
	}
	
	function deleteAdsTempImageHTML() {
		$db = $this->getDBO();
		
		$id			=	JRequest::getVar('id');
		$sess_id	=	JRequest::getVar('session_id');
		$name		=	JRequest::getVar('name');
		
		if(!$id)
			return false;
		
		
		$query	=	" DELETE FROM #__ihouse_ads_image WHERE id = '$id' ";
			$db->setQuery( $query );
			$db->query();
		
		/* Remove from Harddrive */
		
		$fn		= 	explode('.',$name);
		
		$path 	=	JRoute::_('images/ihouse/ads_images/'.$sess_id.'/'.$name);
		$path1 	=	JRoute::_('images/ihouse/ads_images/'.$sess_id.'/small-'.$fn[0].'.'.$fn[1]);
		$path2 	=	JRoute::_('images/ihouse/ads_images/'.$sess_id.'/med-'.$fn[0].'.'.$fn[1]);
		
		unlink($path);
		unlink($path1);
		unlink($path2);

		$row = new stdClass();
		$row->is_success 	= 	true;
		$row->name			=	$name;
		
		return $row;	
	}
	
	function _getExtension($str) {
         $i = strrpos($str,".");
         if (!$i) { return ""; } 

         $l = strlen($str) - $i;
         $ext = substr($str,$i+1,$l);
         return $ext;
 	}
}
