<?php
/**
* @version		$Id: poll.php 10752 2008-08-23 01:53:31Z eddieajau $
* @package		Joomla
* @subpackage	Credits
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class AdsModelCredits extends JModel
{
	
	
	function deduct_credits() {
		global $mainframe;
		$db = & JFactory::getDBO();
		
		$user =& JFactory::getUser();
		$user_id = $user->get('id');
		//$user_id	= JRequest::getVar('user_id', '', 'post', 'user_id');
		$plan_id	= JRequest::getVar('plan_id', '', 'post', 'plan_id');
		$credit		= JRequest::getVar('credit', '', 'post', 'credit');
		$sub_credit	= JRequest::getVar('sub_credit', '', 'post', 'sub_credit');
		
		
		$balance = $credit - $sub_credit;
		
		$query = "SELECT validity FROM #__cbsubs_plans WHERE id = '".$plan_id."'";
	
		$db->setQuery($query);
		$validity = $db->loadResult(); //duration

		$date = explode("-", $validity);
		$year = $date[0];
		$month = $date[1];
		$day = substr($date[2], 0, 2);

		$expirydate = date("Y-m-d H:i:s", mktime(date("H"),date("i")+1,date("s"),date("m")+$month,date("d")+$day,date("Y")+$year));

		
		$sql = "UPDATE #__ihouse_users_credit set credit = '".$balance."' WHERE user_id = '".$user_id."'";
		$db->setQuery($sql);
		$db->query();

		$sql = "INSERT INTO #__users_transaction_log VALUES (NULL, '".$user_id."', 'Subscription', '0', '".$sub_credit."', '".$balance."', CURRENT_TIMESTAMP)";
		$db->setQuery($sql);
		$db->query();
		
		$query = "SELECT id FROM #__cbsubs_subscriptions WHERE status = 'X' AND user_id = '".$user_id."' AND plan_id = '".$plan_id."'";
		$db->setQuery($query);
		$sub_id = $db->loadResult();
		
		if(isset($sub_id)){ //if agent renew
		
		$sql = "UPDATE #__cbsubs_subscriptions set status = 'A', last_renewed_date = CURRENT_TIMESTAMP, expiry_date = '".$expirydate."' WHERE user_id = '".$user_id."' AND id =".$sub_id;
		$db->setQuery($sql);
		$db->query();
		
		$sql = "UPDATE #__ihouse_ads_subscription_slot set slot = '50', start_date = CURRENT_TIMESTAMP, end_time = CURRENT_TIMESTAMP WHERE user_id = '".$user_id."' AND id =".$sub_id;
		$db->setQuery($sql);
		$db->query();
		
		
		
		} else {
		
		$query = "INSERT INTO #__cbsubs_subscriptions (id,status,user_id,plan_id,parent_plan,parent_subscription,replaces_plan,replaces_subscription,subscription_date,last_renewed_date,expiry_date,next_event_date,next_event_id,next_event_params,autorenew_type,autorecurring_type,regular_recurrings_total,regular_recurrings_used,previous_recurrings_used,previous_expiry_date,previous_status,ip_addresses,integrations)"
	
		. "\n VALUES ( NULL, 'A', '".$user_id."', '".$plan_id."', '0', '0', NULL, NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,'".$expirydate."', NULL, '0', '', '0', '0', '0', '1', '0', NULL, 'R', '".$ip."', '')";
		$db->setQuery( $query );
		$db->query();
		$sid = $db->insertid();
		
		$query = "INSERT INTO #__ihouse_ads_subscription_slot(id,user_id,slot,start_date,end_date)"
	
		. "\n VALUES ( NULL, '".$user_id."', '50', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
		$db->setQuery( $query );
		$db->query();
		
		}
		
		return true;
	}
	
	function sub_plan(){
		global $mainframe;
		$db = & JFactory::getDBO();
		
		$user		=& 	JFactory::getUser();
		$user_id	= 	$user->id;
		
		$plan_id	= JRequest::getVar('sub_plan', '', 'post', 'sub_plan');
		
		$aplan_exist	= JRequest::getVar('aplan_exist', '', 'post', 'aplan_exist');
		
		$ip = $_SERVER['REMOTE_ADDR'];
		
		$query = "SELECT rate FROM #__cbsubs_plans WHERE id = '".$plan_id."'";
		$db->setQuery($query);
		$rate = $db->loadResult();
		
		$query = "SELECT params FROM #__ihouse_promotion WHERE id = '2'";
		$db->setQuery($query);
		$params = $db->loadResult();
				
		$discountone = explode("=", $params);
		$discount = $discountone[1];
		
		$after_discount = ((round($rate)) * (1 - ($discount)));

		$sql = "UPDATE #__ihouse_users_credit set credit = credit - '".round($after_discount)."' WHERE user_id = '".$user_id."'";
		$db->setQuery($sql);
		$db->query();
		
		$query = "SELECT validity FROM #__cbsubs_plans WHERE id = '".$plan_id."'";
	
		$db->setQuery($query);
		$validity = $db->loadResult(); //duration

		$date = explode("-", $validity);
		$year = $date[0];
		$month = $date[1];
		$day = substr($date[2], 0, 2);

		// no plan exist especially for quarterly + yearly
		if(!$aplan_exist) {
			$expirydate = date("Y-m-d H:i:s", mktime(date("H"),date("i")+1,date("s"),date("m")+$month,date("d")+$day,date("Y")+$year));
			
			// insert new subscription
			$query = "INSERT INTO #__cbsubs_subscriptions (id,status,user_id,plan_id,parent_plan,parent_subscription,replaces_plan,replaces_subscription,subscription_date,last_renewed_date,expiry_date,next_event_date,next_event_id,next_event_params,autorenew_type,autorecurring_type,regular_recurrings_total,regular_recurrings_used,previous_recurrings_used,previous_expiry_date,previous_status,ip_addresses,integrations)"
	
		. "\n VALUES ( NULL, 'A', '".$user_id."', '".$plan_id."', '0', '0', NULL, NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,'".$expirydate."', NULL, '0', '', '0', '0', '0', '1', '0', NULL, 'R', '".$ip."', '')";
				$db->setQuery( $query );
				$db->query();
				
			$sid = $db->insertid(); // new subs id 
		
			$total_slot = 50; // need to change 50
		
			// if you have pay per basis + active, need to change to current subscription plan
			$count 	= " SELECT COUNT(id) ";
			$select = " SELECT id ";
			$from 	= " FROM #__cbsubs_subscriptions "
					.	" WHERE user_id = '$user_id' AND plan_id = 4 AND status = 'A' ";
			
			$queryslct = $select.$from;
			$querycnt = $count.$from;
			
				$db->setQuery( $queryslct );
				$tmps = $db->loadObjectList();
			
				$db->setQuery( $querycnt );
				$payperbasis_count = $db->loadResult();
			
			// need to apply new subs_id for every payperbasis
			foreach($tmps as $t) :
				$qry = " UPDATE #__ihouse_ads "
						.	" SET subscription_id = '$sid' "
						.	" WHERE subscription_id = ". $t->id ;
					$db->setQuery( $qry );
					$db->query();
			endforeach;
			
			// update the current payperbasis to be expired	(now time), 
			// coz we r going to use current subs plan (Q or Yearly)
			$query = " UPDATE #__cbsubs_subscriptions " 
					.	" SET expiry_date = '".date("Y-m-d H:i:s", time())."' "
					.	" , status = 'X' "
					.	" WHERE user_id = '$user_id' AND plan_id = 4 AND status = 'A' ";
				$db->setQuery( $query );
				$db->query();
		
			// minus the total payperbasis you have 
			$total_slot = $total_slot - $payperbasis_count;
		
			$query = "INSERT INTO #__ihouse_ads_subscription_slot (id,user_id,slot,start_date,end_date)"
						. "\n VALUES ( NULL, '".$user_id."', '$total_slot', CURRENT_TIMESTAMP, '".$expirydate."')";
				$db->setQuery( $query );
				$db->query();
		
			$query = "SELECT credit FROM #__ihouse_users_credit WHERE user_id = '".$user_id."'";
	
				$db->setQuery($query);
				$balance = $db->loadResult();
		
			$query = "SELECT name FROM #__cbsubs_plans WHERE id = '".$plan_id."'";
	
				$db->setQuery($query);
				$plan_name = $db->loadResult();
		
			$sql = "INSERT INTO #__users_transaction_log VALUES (NULL, '".$user_id."', '".$plan_name."', '0', '".round($after_discount)."', '".$balance."', CURRENT_TIMESTAMP)";
				$db->setQuery($sql);
				$db->query();
				
		} else { // if you are Quarterly Subs upgrade to Yearly Subscription (Renew and Upgrade)
			
			$query		=	" SELECT id, expiry_date FROM #__cbsubs_subscriptions "
						.	" WHERE user_id = '$user_id' AND status = 'A' AND (plan_id = '2')" ;
						
				$db->setQuery( $query );
				$row = $db->loadObject();	
			
			$subs_id = $row->id;
			$expirydate = $row->expiry_date;
			
			$eH		= date('H', strtotime($expirydate));
			$ei		= date('i', strtotime($expirydate)) + 1;
			$es		= date('s', strtotime($expirydate));
			$em		= date('m', strtotime($expirydate)) + $month;
			$ed		= date('d', strtotime($expirydate)) + $day;
			$eY		= date('Y', strtotime($expirydate)) + $year;
		
			$now		= date('Y-m-d H:i:s', time());
			$expirydate = date("Y-m-d H:i:s", mktime($eH,$ei,$es,$em,$ed,$eY));			
			
			$query = "UPDATE #__cbsubs_subscriptions SET "
						.	" subscription_date = '$now' "
						.	" , last_renewed_date = '$now' "
						.	" , expiry_date = '$expirydate' "
						.	" , plan_id = '$plan_id' "
						.	" WHERE id = '$subs_id' ";
						
				$db->setQuery( $query );
				$db->query();
			
			$query = "SELECT credit FROM #__ihouse_users_credit WHERE user_id = '".$user_id."'";
	
				$db->setQuery($query);
				$balance = $db->loadResult();
		
			$query = "SELECT name FROM #__cbsubs_plans WHERE id = '".$plan_id."'";
	
				$db->setQuery($query);
				$plan_name = $db->loadResult();
			
			$sql = "INSERT INTO #__users_transaction_log VALUES (NULL, '".$user_id."', ' Renew + Upgrade : ".$plan_name."', '0', '".round($after_discount)."', '".$balance."', CURRENT_TIMESTAMP)";
				$db->setQuery($sql);
				$db->query();
		}
		return true;
	
	
	
	}//end function
	
	function featured_save(){
		global $mainframe;
		$db = & JFactory::getDBO();
		
		$user		=& 	JFactory::getUser();
		$user_id	= 	$user->id;
		
		$plan_id	= JRequest::getVar('plan_id', '', 'post', 'plan_id');
		$ads_type_config	= JRequest::getVar('ads_type_config', '', 'post', 'ads_type_config');
		$property_id	= JRequest::getVar('property_id', '', 'post', 'property_id');
		$ip = $_SERVER['REMOTE_ADDR'];

		
		$query = "SELECT rate FROM #__cbsubs_plans WHERE id = '".$plan_id."'";
		$db->setQuery($query);
		$rate = $db->loadResult();
		
		$query = "SELECT params FROM #__ihouse_promotion WHERE id = '4'";
		$db->setQuery($query);
		$params = $db->loadResult();
		
		$discountone = explode("=", $params);
		$discount = $discountone[1];
		
		$after_discount = $rate * (1 - ($discount));
		
		$sql = "UPDATE #__ihouse_users_credit set credit = credit - '".round($after_discount)."' WHERE user_id = '".$user_id."'";
		$db->setQuery($sql);
		$db->query();
		
		$sql = "UPDATE #__ihouse_ads_type_slot set space = space - 1 WHERE ads_type_config = '".$ads_type_config."' AND property_id  = '".$property_id."'";
		$db->setQuery($sql);
		$db->query();
		
		$query = "SELECT validity FROM #__cbsubs_plans WHERE id = '".$plan_id."'";
		$db->setQuery($query);
		$validity = $db->loadResult(); //duration

		$date = explode("-", $validity);
		$year = $date[0];
		$month = $date[1];
		$day = substr($date[2], 0, 2);

		$expirydate = date("Y-m-d H:i:s", mktime(date("H"),date("i")+1,date("s"),date("m")+$month,date("d")+$day,date("Y")+$year));
		
		$query = "INSERT INTO #__cbsubs_subscriptions (id,status,user_id,plan_id,parent_plan,parent_subscription,replaces_plan,replaces_subscription,subscription_date,last_renewed_date,expiry_date,next_event_date,next_event_id,next_event_params,autorenew_type,autorecurring_type,regular_recurrings_total,regular_recurrings_used,previous_recurrings_used,previous_expiry_date,previous_status,ip_addresses,integrations)"
	
		. "\n VALUES ( NULL, 'A', '".$user_id."', '".$plan_id."', '0', '0', NULL, NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,'".$expirydate."', NULL, '0', '', '0', '0', '0', '1', '0', NULL, 'R', '".$ip."', '')";
		$db->setQuery( $query );
		$db->query();
		$sid = $db->insertid();
		
		$query = "INSERT INTO #__ihouse_ads_type(id,ads_type_config,subscription_id,property_id,ads_id,user_id,property_type,duration,start_date,end_date)"
	
		. "\n VALUES ( NULL, '".$ads_type_config."', '".$sid."', '".$property_id."', '0', '".$user_id."', '', '', CURRENT_TIMESTAMP, '".$expirydate."')";
		$db->setQuery( $query );
		$db->query();
		
		$query = "SELECT credit FROM #__ihouse_users_credit WHERE user_id = '".$user_id."'";
	
		$db->setQuery($query);
		$balance = $db->loadResult();
		
		$query = "SELECT name FROM #__cbsubs_plans WHERE id = '".$plan_id."'";
	
		$db->setQuery($query);
		$plan_name = $db->loadResult();
		
		$sql = "INSERT INTO #__users_transaction_log VALUES (NULL, '".$user_id."', '".$plan_name."', '0', '".round($after_discount)."', '".$balance."', CURRENT_TIMESTAMP)";
		$db->setQuery($sql);
		$db->query();
	
		return true;
	
	}//end function
	
	function featuredads_save(){
		global $mainframe;
		$db = & JFactory::getDBO();
		
		$user		=& 	JFactory::getUser();
		$user_id	= 	$user->id;

		$plan_id	= JRequest::getVar('plan_id', '', 'post', 'plan_id');
		$ads_type_config	= JRequest::getVar('ads_type_config', '', 'post', 'ads_type_config');
		$ads_type	= JRequest::getVar('ads_type', '', 'post', 'ads_type');
		$ads_id	= JRequest::getVar('ads_id', '', 'post', 'ads_id');
		$ip = $_SERVER['REMOTE_ADDR'];
		

		$query = "SELECT rate FROM #__cbsubs_plans WHERE id = '".$plan_id."'";
		$db->setQuery($query);
		$rate = $db->loadResult();
		
		$query = "SELECT name FROM #__cbsubs_plans WHERE id = '".$plan_id."'";
	
		$db->setQuery($query);
		$plan_name = $db->loadResult();
		
		$query = "SELECT params FROM #__ihouse_promotion WHERE id = '4'";
		$db->setQuery($query);
		$params = $db->loadResult();
		
		$discountone = explode("=", $params);
		$discount = $discountone[1];
		
		
		//25% discount for the 4th week
		if(($plan_id == '11')||($plan_id == '15')||($plan_id == '19')||($plan_id == '23')){
		$sub_credit = ((round($rate)) * (1 - 0.25));
		} else {
		$sub_credit = (round($rate));
		}
		
		//80% discount for all category featured ads
		$after_discount = $sub_credit * (1 - ($discount));
		
		$sql = "UPDATE #__ihouse_users_credit set credit = credit - '".round($after_discount)."' WHERE user_id = '".$user_id."'";
		$db->setQuery($sql);
		$db->query();
		
		/*$sql = "UPDATE #__ihouse_ads_type_slot set space = space - 1 WHERE ads_type_config = '".$ads_type_config."' AND property_id  = '".$property_id."'";
		$db->setQuery($sql);
		$db->query();*/
		
		
		if(($ads_type_config == '4')||($ads_type_config == '6')){
		
		$sql = "UPDATE #__ihouse_ads_type_slot set space = space - 1 WHERE ads_type_config = '".$ads_type_config."' AND property_type  = '".$ads_type."'";
		$db->setQuery($sql);
		$db->query();

		} else  if ($ads_type_config == '7'){
		
		$sql = "UPDATE #__ihouse_ads_type_slot set space = space - 1 WHERE ads_type_config = '".$ads_type_config."'";
		$db->setQuery($sql);
		$db->query();
		
		} else if ($ads_type_config == '5'){
		
		$query = "SELECT property_id FROM #__ihouse_ads WHERE id = '".$ads_id."' AND posted_by = '".$user_id."'";
		$db->setQuery($query);
		$property_id = $db->loadResult();
		
		$sql = "UPDATE #__ihouse_ads_type_slot set space = space - 1 WHERE ads_type_config = '".$ads_type_config."' AND property_id = '".$property_id."'";
		$db->setQuery($sql);
		$db->query();
		
		}
		
		$query = "SELECT validity FROM #__cbsubs_plans WHERE id = '".$plan_id."'";
	
		$db->setQuery($query);
		$validity = $db->loadResult(); //duration

		$date = explode("-", $validity);
		$year = $date[0];
		$month = $date[1];
		$day = substr($date[2], 0, 2);

		$expirydate = date("Y-m-d H:i:s", mktime(date("H"),date("i")+1,date("s"),date("m")+$month,date("d")+$day,date("Y")+$year));
		
		$query = "INSERT INTO #__cbsubs_subscriptions (id,status,user_id,plan_id,parent_plan,parent_subscription,replaces_plan,replaces_subscription,subscription_date,last_renewed_date,expiry_date,next_event_date,next_event_id,next_event_params,autorenew_type,autorecurring_type,regular_recurrings_total,regular_recurrings_used,previous_recurrings_used,previous_expiry_date,previous_status,ip_addresses,integrations)"
	
		. "\n VALUES ( NULL, 'A', '".$user_id."', '".$plan_id."', '0', '0', NULL, NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,'".$expirydate."', NULL, '0', '', '0', '0', '0', '1', '0', NULL, 'R', '".$ip."', '')";
		$db->setQuery( $query );
		$db->query();
		$sid = $db->insertid();
		
		$query = "INSERT INTO #__ihouse_ads_type(id,ads_type_config,subscription_id,property_id,ads_id,user_id,property_type,duration,start_date,end_date)"
		. "\n VALUES ( NULL, '".$ads_type_config."', '".$sid."', '".$property_id."', '".$ads_id."', '".$user_id."', '".$ads_type."', '', CURRENT_TIMESTAMP, '".$expirydate."')";
		$db->setQuery( $query );
		$db->query();
		
		$query = "SELECT credit FROM #__ihouse_users_credit WHERE user_id = '".$user_id."'";
		$db->setQuery($query);
		$balance = $db->loadResult();
		
		$sql = "INSERT INTO #__users_transaction_log VALUES (NULL, '".$user_id."', '".$plan_name."', '0', '".round($after_discount)."', '".$balance."', CURRENT_TIMESTAMP)";
		$db->setQuery($sql);
		$db->query();
	
		return true;

	
	}//end function
		
	function save_featured(){
	
		global $mainframe;
		$db = & JFactory::getDBO();
		
		$user		=& 	JFactory::getUser();
		$user_id	= 	$user->id;
		
		$plan_id	= JRequest::getVar('plan_id', '', 'post', 'plan_id');
		$property_id	= JRequest::getVar('property_id', '', 'post', 'property_id');
		$property_type	= JRequest::getVar('property_type', '', 'post', 'property_type');
		$ads_id	= JRequest::getVar('ads_id', '', 'post', 'ads_id');
		$ads_type_config	= JRequest::getVar('ads_type_config', '', 'post', 'ads_type_config');
		$ip = $_SERVER['REMOTE_ADDR'];
		
		$query = "SELECT credit FROM #__ihouse_users_credit WHERE user_id = '".$user_id."'";
	
		$db->setQuery($query);
		$credit = $db->loadResult();
		
		$query = "SELECT rate FROM #__cbsubs_plans WHERE id = '".$plan_id."'";
	
		$db->setQuery($query);
		$rate = $db->loadResult();
		
		$query = "SELECT params FROM #__ihouse_promotion WHERE id = '2'";
	
		$db->setQuery($query);
		$params = $db->loadResult();
		
		$discountone = explode("=", $params);
		$discount = $discountone[1];
		
		if(($plan_id == '11')||($plan_id == '15')||($plan_id == '19')||($plan_id == '23')){
		$sub_credit = ((round($rate)) * (1 - ($discount)));
		} else {
		$sub_credit = (round($rate));
		}
		
		$balance = $credit - $sub_credit;
		
		$query = "SELECT validity FROM #__cbsubs_plans WHERE id = '".$plan_id."'";
	
		$db->setQuery($query);
		$validity = $db->loadResult(); //duration

		$date = explode("-", $validity);
		$year = $date[0];
		$month = $date[1];
		$day = substr($date[2], 0, 2);

		$expirydate = date("Y-m-d H:i:s", mktime(date("H"),date("i")+1,date("s"),date("m")+$month,date("d")+$day,date("Y")+$year));

		
		$sql = "UPDATE #__ihouse_users_credit set credit = '".$balance."' WHERE user_id = '".$user_id."'";
		$db->setQuery($sql);
		$db->query();
		
		$sql = "INSERT INTO #__users_transaction_log VALUES (NULL, '".$user_id."', 'Featured Ads', '0', '".$sub_credit."', '".$balance."', CURRENT_TIMESTAMP)";
		$db->setQuery($sql);
		$db->query();
		
		if(($ads_type_config == '4')||($ads_type_config == '6')){
		
		$query = "SELECT space FROM #__ihouse_ads_type_slot WHERE ads_type_config = '".$ads_type_config."' AND property_type  = '".$property_type."'";
	
		$db->setQuery($query);
		$space = $db->loadResult();
		
		$available_space = $space - 1;
		
		$sql = "UPDATE #__ihouse_ads_type_slot set space = '".$available_space."' WHERE ads_type_config = '".$ads_type_config."' AND property_type  = '".$property_type."'";
		$db->setQuery($sql);
		$db->query();

		} else {
		
		$query = "SELECT space FROM #__ihouse_ads_type_slot WHERE ads_type_config = '".$ads_type_config."' AND property_id  = '".$property_id."'";
	
		$db->setQuery($query);
		$space = $db->loadResult();
		
		$available_space = $space - 1;
		
		$sql = "UPDATE #__ihouse_ads_type_slot set space = '".$available_space."' WHERE ads_type_config = '".$ads_type_config."' AND property_id  = '".$property_id."'";
		$db->setQuery($sql);
		$db->query();
		}
		
		$query = "INSERT INTO #__cbsubs_subscriptions (id,status,user_id,plan_id,parent_plan,parent_subscription,replaces_plan,replaces_subscription,subscription_date,last_renewed_date,expiry_date,next_event_date,next_event_id,next_event_params,autorenew_type,autorecurring_type,regular_recurrings_total,regular_recurrings_used,previous_recurrings_used,previous_expiry_date,previous_status,ip_addresses,integrations)"
	
		. "\n VALUES ( NULL, 'A', '".$user_id."', '".$plan_id."', '0', '0', NULL, NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,'".$expirydate."', NULL, '0', '', '0', '0', '0', '1', '0', NULL, 'R', '".$ip."', '')";
		$db->setQuery( $query );
		$db->query();
		$sid = $db->insertid();
		
		$query = "INSERT INTO #__ihouse_ads_type(id,ads_type_config,subscription_id,property_id,ads_id,user_id,property_type,duration,start_date,end_date)"
	
		. "\n VALUES ( NULL, '".$ads_type_config."', '".$sid."', '".$property_id."', '".$ads_id."', '".$user_id."', '".$property_type."', '', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
		$db->setQuery( $query );
		$db->query();
		
		return true;
	
	}
	
	function type_three(){
	
		global $mainframe;
		$db = & JFactory::getDBO();
		
		$user		=& 	JFactory::getUser();
		$user_id	= 	$user->id;
		
		$plan_id	= JRequest::getVar('plan_id', '', 'post', 'plan_id');
		
		$ads_type_config	= JRequest::getVar('ads_type_config', '', 'post', 'ads_type_config');
		$ip = $_SERVER['REMOTE_ADDR'];
		
		$query = "SELECT credit FROM #__ihouse_users_credit WHERE user_id = '".$user_id."'";
	
		$db->setQuery($query);
		$credit = $db->loadResult();
		
		$query = "SELECT rate FROM #__cbsubs_plans WHERE id = '".$plan_id."'";
	
		$db->setQuery($query);
		$rate = $db->loadResult();
		
		$query = "SELECT params FROM #__ihouse_promotion WHERE id = '2'";
	
		$db->setQuery($query);
		$params = $db->loadResult();
		
		$discountone = explode("=", $params);
		$discount = $discountone[1];
		
		if(($plan_id == '11')||($plan_id == '15')||($plan_id == '19')||($plan_id == '23')){
		$sub_credit = ((round($rate)) * (1 - ($discount)));
		} else {
		$sub_credit = (round($rate));
		}
		
		$balance = $credit - $sub_credit;
		
		$query = "SELECT validity FROM #__cbsubs_plans WHERE id = '".$plan_id."'";
	
		$db->setQuery($query);
		$validity = $db->loadResult(); //duration

		$date = explode("-", $validity);
		$year = $date[0];
		$month = $date[1];
		$day = substr($date[2], 0, 2);

		$expirydate = date("Y-m-d H:i:s", mktime(date("H"),date("i")+1,date("s"),date("m")+$month,date("d")+$day,date("Y")+$year));

		
		$sql = "UPDATE #__ihouse_users_credit set credit = '".$balance."' WHERE user_id = '".$user_id."'";
		$db->setQuery($sql);
		$db->query();
		
		$sql = "INSERT INTO #__users_transaction_log VALUES (NULL, '".$user_id."', 'Featured Ads', '0', '".$sub_credit."', '".$balance."', CURRENT_TIMESTAMP)";
		$db->setQuery($sql);
		$db->query();
		
		$query = "SELECT space FROM #__ihouse_ads_type_slot WHERE ads_type_config = '3'";
	
		$db->setQuery($query);
		$space = $db->loadResult();
		
		$available_space = $space - 1;
		
		$sql = "UPDATE #__ihouse_ads_type_slot set space = '".$available_space."' WHERE ads_type_config = '3'";
		$db->setQuery($sql);
		$db->query();
		
		$query = "INSERT INTO #__cbsubs_subscriptions (id,status,user_id,plan_id,parent_plan,parent_subscription,replaces_plan,replaces_subscription,subscription_date,last_renewed_date,expiry_date,next_event_date,next_event_id,next_event_params,autorenew_type,autorecurring_type,regular_recurrings_total,regular_recurrings_used,previous_recurrings_used,previous_expiry_date,previous_status,ip_addresses,integrations)"
	
		. "\n VALUES ( NULL, 'A', '".$user_id."', '".$plan_id."', '0', '0', NULL, NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,'".$expirydate."', NULL, '0', '', '0', '0', '0', '1', '0', NULL, 'R', '".$ip."', '')";
		$db->setQuery( $query );
		$db->query();
		$sid = $db->insertid();
		
		$query = "INSERT INTO #__ihouse_ads_type(id,ads_type_config,subscription_id,property_id,ads_id,user_id,property_type,duration,start_date,end_date)"
	
		. "\n VALUES ( NULL, '".$ads_type_config."', '".$sid."', '0', '0', '".$user_id."', '', '', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
		$db->setQuery( $query );
		$db->query();
		
		return true;
	
	
	}
	
	/*function delete_trans()
	{
		$db =& JFactory::getDBO();
		$currentUser =& JFactory::getUser();
		$cid 			= JRequest::getVar( 'cid', array(), '', 'array' );
		
		JArrayHelper::toInteger( $cid );
		
		foreach($cid as $id) {

			$query = "DELETE FROM #__users_transaction_log WHERE id = '$id' ";
			
				$db->setQuery( $query );
				$db->query();
			
		}
		return true;
	
	}*/
	
	
	
}
