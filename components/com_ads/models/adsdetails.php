﻿<?php
/**
* @version		$Id: poll.php 10752 2008-08-23 01:53:31Z eddieajau $
* @package		Joomla
* @subpackage	Credits
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class AdsModelAdsdetails extends JModel
{
	
	
	function getRentAdsObject($id, $usercat) {
		global $mainframe;
		$db = & JFactory::getDBO();
		
		$user =& JFactory::getUser();
		$user_id = $user->get('id');
		
		$limitstart =	JRequest::getVar('limitstart', 0);
		$limit		=	JRequest::getVar('limit', 5);
		
		$page			=	JRequest::getVar('page', '');
		$limitstart		=	$page * $limit;
		
		$filter				=	JRequest::getVar('filter', '');
		$orderfilter		=	JRequest::getVar('orderfilter', '');
		
		if(!empty($filter) || !empty($orderfilter)) {
			$orderby	= " ORDER BY ".$filter." ".$orderfilter." ";
		}
		
		$obj	=	new StdClass;
		
		$query = 'SELECT COUNT(a.id)'
		.	' FROM #__ihouse_ads AS a '
		//.	' LEFT JOIN #__ihouse_ads_image AS i ON i.ads_id = a.id '
		.	' LEFT JOIN #__users AS u ON u.id = a.posted_by '
		.	' LEFT JOIN #__cbsubs_subscriptions AS c ON c.user_id = a.posted_by AND c.id = a.subscription_id '
		.   ' WHERE a.ad_type = "rent" AND a.publish = "1" AND c.status = "A" AND a.property_id = "'.$id.'"'
		.	$orderby;
		$db->setQuery( $query );
		$total = $db->loadResult();
		
		$query	=	'SELECT a.*, p.district_id, u.name, u.id AS userid, u.company_name ,u.company_logo, u.chinese_name, u.user_image, u.mobile_contact, u.user_category, u.cea_reg_no,u.email   '
							.	' FROM #__ihouse_ads AS a '
							//.	' LEFT JOIN #__ihouse_ads_image AS i ON i.ads_id = a.id '
							.	' LEFT JOIN #__users AS u ON u.id = a.posted_by '
							.	' LEFT JOIN #__ihouse_property AS p ON p.id = a.property_id '
							.	' LEFT JOIN #__cbsubs_subscriptions AS c ON c.user_id = a.posted_by AND c.id = a.subscription_id '
							.   ' WHERE a.ad_type = "rent" AND a.publish = "1" AND c.status = "A" AND a.property_id = "'.$id.'" AND u.user_category = "'.$usercat.'"'
							.	$orderby;

						
			$db->setQuery( $query , $limitstart, $limit);
			$list = $db->loadObjectList();
		
		$obj->total = 	$total;
		$obj->rows	=	$list;
		
		return $obj;
	}
	
	function getAdsObject($id, $usercat) {
		global $mainframe;
		$db = & JFactory::getDBO();
		
		$user =& JFactory::getUser();
		$user_id = $user->get('id');
		
		$limitstart =	JRequest::getCmd('limitstart', 0);
		$limit		=	JRequest::getCmd('limit', 5);
		
		$page			=	JRequest::getVar('page', '');
		$limitstart		=	$page * $limit;
		
		$filter				=	JRequest::getVar('filter', '');
		$orderfilter		=	JRequest::getVar('orderfilter', '');
		
		if(!empty($filter) || !empty($orderfilter)) {
			$orderby	= " ORDER BY ".$filter." ".$orderfilter." ";
		}
		
		$obj	=	new StdClass;
		
		$query = 'SELECT COUNT(a.id)'
		.	' FROM #__ihouse_ads AS a '
		//.	' LEFT JOIN #__ihouse_ads_image AS i ON i.ads_id = a.id '
		.	' LEFT JOIN #__users AS u ON u.id = a.posted_by '
		.	' LEFT JOIN #__cbsubs_subscriptions AS c ON c.user_id = a.posted_by AND c.id = a.subscription_id '
		.   ' WHERE a.ad_type = "sell" AND a.publish = "1" AND c.status = "A" AND a.property_id = "'.$id.'"'
		.	$orderby;
		$db->setQuery( $query );
		$total = $db->loadResult();
		
		$query	=	'SELECT a.*, p.district_id, u.name, u.id AS userid, u.company_name ,u.company_logo, u.chinese_name, u.user_image,u.mobile_contact, u.user_category, u.cea_reg_no,u.email   '
			.	' FROM #__ihouse_ads AS a '
			//.	' LEFT JOIN #__ihouse_ads_image AS i ON i.ads_id = a.id '
			.	' LEFT JOIN #__users AS u ON u.id = a.posted_by '
			.	' LEFT JOIN #__ihouse_property AS p ON p.id = a.property_id '
			.	' LEFT JOIN #__cbsubs_subscriptions AS c ON c.user_id = a.posted_by AND c.id = a.subscription_id '
			.   ' WHERE a.ad_type = "sell" AND a.publish = "1" AND c.status = "A" AND a.property_id = "'.$id.'" AND u.user_category = "'.$usercat.'"'
			.	$orderby;

						
			$db->setQuery( $query, $limitstart, $limit );
			$list = $db->loadObjectList();
		
		$obj->total = 	$total;
		$obj->rows	=	$list;
		
		return $obj;
	}
	
	
	
	
}
