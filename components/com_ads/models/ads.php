﻿<?php
/**
* @version		$Id: poll.php 10752 2008-08-23 01:53:31Z eddieajau $
* @package		Joomla
* @subpackage	Polls
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class AdsModelAds extends JModel
{
	function __construct()
	{
		global $mainframe;

		parent::__construct();

	}
	
	function post_ads() {
		$db = $this->getDBO();
		
		$user		=& 	JFactory::getUser();
		$uid		= 	$user->id;
		$user		=& 	JFactory::getUser($uid);
		$user_cat	=	$user->user_category;
		
		$publish					=	JRequest::getVar('publish', 1);

		$property_type				=	JRequest::getVar('property_type', '');
		
		$property_id				=	JRequest::getInt('property_id', '');
		
		$property_name				=	'';
		
		if(empty($property_id))
			$property_id				=	JRequest::getVar('property_name', 0); /* id */

		switch($property_type) {
			case 'condo':
			case '公寓':
				
				$property_type = '公寓';
				
				$query = " SELECT name_en FROM #__ihouse_property WHERE id = '$property_id' ";
					$db->setQuery( $query );
					$property_name = $db->loadResult();
					$property_name = str_replace("'","\'",stripslashes($property_name));
					
				break;
			case 'hdb':
			case '组屋':
			
				$property_type = '组屋';
				
				break;	
			case 'landed' :	
			case '有地住宅':
				
				$property_type = '有地住宅';
				
				$property_name = JRequest::getVar('project_name', '');
				$property_name = str_replace("'","\'",stripslashes($property_name));
				
				break;
			case 'commercials' :
			case '商业':
				$property_type = '商业';
				
				$property_name = JRequest::getVar('project_name', '');
				$property_name = str_replace("'","\'",stripslashes($property_name));
				
				break;
			case 'apartments':
			case '服务公寓':
				$property_type = '服务公寓';
				
				$property_name = JRequest::getVar('project_name', '');
				$property_name = str_replace("'","\'",stripslashes($property_name));
				break;
		}
		
		$session_id					=	JRequest::getVar('session_id', '');
		$primary_photo				=	JRequest::getVar('primary_photo', 0);
		
		$hdb_town					=	JRequest::getVar('hdb_town', '');
		$property_category			=	JRequest::getVar('property_category', '');
		
		$ihouse_photos_arr			=	JRequest::getVar('ihousephotos', array());
		
		$flattype					=	JRequest::getVar('flattype', '');
		$asking_price				=	str_replace(',','',JRequest::getVar('asking_price',''));
		$psm_price					=	str_replace(',','',JRequest::getVar('psm_price', ''));
		$size						=	str_replace(',','',JRequest::getVar('size', 0));
		$no_of_room					=	JRequest::getVar('no_of_room', 0);
		$property_district			=	JRequest::getVar('district', '');
		$no_of_hall					=	JRequest::getVar('no_of_hall', 0);
		$postcode					=	JRequest::getVar('postcode', '');
		$no_study_room				=	JRequest::getVar('no_study_room', 0);
		$tenure						=	JRequest::getVar('tenure', '');
		$toilet						=	JRequest::getVar('toilet', 0);
		$top_year					=	JRequest::getInt('year_built', 0);
		$floor_level				=	JRequest::getVar('floor_level', '');
		$block_house_no				=	JRequest::getVar('block_house_no', '');
		$floor_unit_no				=	JRequest::getVar('floor_unit_no', '');
		$street_name				=	JRequest::getVar('street_name', '');
		$street_name	 			= 	str_replace("'","\'",stripslashes($street_name));

		$country					=	JRequest::getVar('country', '');
		$ad_title					=	JRequest::getVar('ad_title', '');
		$ad_title	 				= 	str_replace("'","\'",stripslashes($ad_title));
		$add_info					=	JRequest::getVar('add_info', '');
		$add_info	 				= 	str_replace("'","\'",stripslashes($add_info));
		$renovation					=	JRequest::getVar('renovation', '');
		$status						=	JRequest::getVar('status', '');
		
		$rental_type				=	JRequest::getVar('rental_type','');
		$ad_type					= 	JRequest::getVar('ad_type','');
		
		$subs_status				=	0;
		$plan_id					=	0;
		
		/* GET Subs Id */
		switch($user_cat) {
			case 'Agent':
				$query = " SELECT id,plan_id FROM #__cbsubs_subscriptions WHERE user_id = '$uid' AND status = 'A' AND plan_id != 4 LIMIT 1";
					$db->setQuery( $query );
					$tmp = $db->loadObject();
			
					$subs_id					=	$tmp->id;
					$plan_id					=	$tmp->plan_id;
					
					if(empty($plan_id)) {
						$plan_id = 4; /* Pay Per Basis */
					} else { // in subscription
						
						/* CHECK SLOT */
						$query = " SELECT slot FROM #__ihouse_ads_subscription_slot WHERE user_id = '$uid' LIMIT 1";
							$db->setQuery( $query );
							$slot = $db->loadResult();
						
						/*if($slot == 0) {
							$plan_id = 4;
						}*/
					}
				break;
			case 'Owner':
				$query = " SELECT id,plan_id FROM #__cbsubs_subscriptions WHERE user_id = '$uid' AND status = 'A' LIMIT 1";
					$db->setQuery( $query );
					$tmp = $db->loadObject();
			
					$subs_id					=	$tmp->id;
					$plan_id					=	$tmp->plan_id;
					
					if(empty($plan_id)) {
						$plan_id = 25; /* Pay Per Basis */
					}
				break;
			case 'Individual':
				break;
		}
		
		
		// Load Joomla filesystem layer
		jimport('joomla.filesystem.file');
		
		$query = 	"INSERT INTO #__ihouse_ads VALUES( NULL, "
					. " '$property_id', "								  
					. " '$postcode', "			
					. " '$property_type', "
					. " '$hdb_town', "
					. " '$flattype', "
					. " '$property_category', "
					. " '$property_name', "
					. " '$uid', "
					. " '$asking_price', "
					. " '$psm_price', "
					. " '$size', "
					. " '$rental_type', "
					. " '$no_of_room', "
					. " '$no_of_hall', "
					. " '$no_study_room', "
					. " '$toilet', "
					. " '$property_district', "
					. " '$tenure', "
					. " '$top_year', "
					. " '$block_house_no' , "
					. " '$floor_unit_no', "
					. " '$street_name', "
					. " '$floor_level' , "
					. " '$country', "
					. " '$ad_title', "
					. " '$ad_type', "
					. " '0', "
					. " '$add_info', "
					. " '$renovation', "
					. " '$status', "
					. " '$subs_id', "
					. " '$subs_status', "
					. " '$publish', " /* publish */
					. " '".date("Y-m-d H:i:s", time())."', "
					. " '".date("Y-m-d H:i:s", time())."' "
					. " ); ";

			$db->setQuery( $query );
			$db->query();
		
			$ads_id = $db->insertid();
		
		$fp1			=	JRequest::getVar('fp1', ''); 
		$fp2			=	JRequest::getVar('fp2', '');
		
		if(!empty($fp1)) {
			$tmp 	= 	explode('|', $fp1);
			$fp1	= 	$tmp[0].'/'.$tmp[1];
			
			$query = " INSERT INTO #__ihouse_ads_floorplan VALUES( NULL , "
					.	" '$ads_id', "
					.	" '$fp1', "
					.	" '1' "
					. " ); ";
		
			$db->setQuery( $query );
			$db->query();
		}
		
		if(!empty($fp2)) {
			$tmp 	= 	explode('|', $fp2);
			$fp2	=	$tmp[0].'/'.$tmp[1];
			
			$query = " INSERT INTO #__ihouse_ads_floorplan VALUES( NULL , "
					.	" '$ads_id', "
					.	" '$fp2', "
					.	" '2' "
					. " ); ";
		
			$db->setQuery( $query );
			$db->query();
		}
		
		foreach($ihouse_photos_arr as $ih):
		
			$query = "	INSERT INTO #__ihouse_ads_image2 VALUES( NULL , "
						.	" '$ads_id' , "
						.	" '$ih' "
						. " ); ";
			
				$db->setQuery( $query );
				$db->query();
			
		endforeach;
		
		$query = " UPDATE #__ihouse_ads_image SET ads_id = '$ads_id' WHERE sess_id = '$session_id' ";
		
			$db->setQuery( $query );
			$db->query();
		
		/* Store Session Id and Ads Id */
		
		$query = " INSERT INTO #__ihouse_ads_session VALUES(NULL, '$ads_id','$session_id')";
			$db->setQuery( $query );
			$db->query();
		
		$this->afterSubmit($ads_id, $plan_id);
		
		return true;
	}
	
	function getFloorplanPictureHTML($postcode = '') {
		$postcode				=	JRequest::getVar('postcode',$postcode);
		$brm_and_file			=	JRequest::getVar('value', '');
		
		
		$html = '';
		
		if(!empty($brm_and_file)) {
			$tmp					=	explode('|',$brm_and_file);
		
			$brm					=	$tmp[0];
			$file					=	$tmp[1];
			
			$floorplan_img = JRoute::_('images/singapore/'.$postcode.'/floorplan/'.$brm.'/'.$file);
			$html .= '<img src="'.$floorplan_img.'" width="100%" height="100%" />';
			
		} else {
			$html .= '';
		}
		return $html;
	}
	
	function getPropertyInfo() {
		$db		=& 	JFactory::getDBO();
		
		$id						=	JRequest::getVar('id','');
		
		$query					=	" SELECT p.* FROM #__ihouse_property AS p "
									//.	" LEFT JOIN #__ihouse_property_price AS pp ON pp.postcode = p.postcode " 
									.	" WHERE p.id = '$id' LIMIT 1 ";
									
			$db->setQuery( $query );
		$row = $db->loadObject();
		
		/* CHECK FLOORPLAN IF EXIST OR NOT */
		$exist		= false;
		
		$img_path = JRoute::_('images/singapore/'.$row->postcode.'/floorplan');
		
		$a_img = array();
		
		$exist = false;
		if(!empty($row)) {
			if(file_exists($img_path)) {
				if ($handle = opendir($img_path)) {
					while (false !== ($file = readdir($handle))) {
						if($file == '.' || $file == '..' || $file == 's')
							continue;
						
						$exist = true;
						break;
					}
				}
			}
		}
		
		$row->has_floorplan = $exist;
		
		return $row;
	}
	
	function getPropertyName() {
		$db		=& 	JFactory::getDBO();
		
		$p_type					=	JRequest::getVar('p_type','');
		
		$query					=	" SELECT * FROM #__ihouse_property WHERE property_type = '$p_type' "
									. " ORDER BY name_en ASC ";
			$db->setQuery( $query );
			$row = $db->loadObjectList();
		
		return $row;
	}
	
	function getFPListHTML($postcode = '', $ads_id = 0) {
		
		$postcode		=	JRequest::getVar('postcode',$postcode);
		$name_id		=	JRequest::getVar('name', '');
		
		$db		=& 	JFactory::getDBO();
		
		$query	= "SELECT * FROM #__ihouse_floorplan ";
			$db->setQuery( $query );
			$floorplan = $db->loadObjectList();
		
		$status = false;
		$html = '';
		
		if(!empty($floorplan)) {
			$status = true;
			
			$html .= '<select name="'.$name_id.'" id="'.$name_id.'" onchange="javascript:loadFloorPlanPicture(\''.$postcode.'\',\''.$name_id.'\', this.value);">';
			$html .= '<option value="">---</option>';
		
			$allowed_types = array('png','jpg','jpeg','gif'); // list of filetypes you want to show
			
			foreach($floorplan as $fp) :
				
				$floorplan_path = JRoute::_('images/singapore/'.$postcode.'/floorplan/'.$fp->type_en);
				
				if(file_exists($floorplan_path)) {
					if ($handle = opendir($floorplan_path)) {
						while (false !== ($file = readdir($handle))) {
							if($file == '.' || $file == '..' || $file == 's')
								continue;

							if(in_array(strtolower(substr($file,-3)),$allowed_types))
							{
								$html .= '<option value="'.$fp->type_en.'|'.$file.'">'.$file.'</option>';
							} 
						}
					}
				}
			endforeach;
			
			$html .= '</select>';
		}
		
		return $html;
	}
	
	function getImagePhotosHTML($postcode = '', $ads_id = 0) {
		$postcode		=	JRequest::getVar('postcode',$postcode);
		
		$session_id		=	JRequest::getVar('session_id', $session_id);
		
		$img_path 	= JRoute::_('images/singapore/'.$postcode.'/images');
		
		$rows = array();
		if($ads_id) {
			$db		=& 	JFactory::getDBO();
		
			$query	= "SELECT image FROM #__ihouse_ads_image2 WHERE ads_id = '$ads_id' ";
				$db->setQuery( $query );
				$rows = $db->loadResultArray();
				
		}
		
		
		$html = '';
				
		$allowed_types = array('png','jpg','jpeg','gif'); // list of filetypes you want to show
				
		if(file_exists($img_path)) {
			if ($handle = opendir($img_path)) {
				while (false !== ($file = readdir($handle))) {
					if($file == '.' || $file == '..' || $file == 's')
						continue;

					if(in_array(strtolower(substr($file,-3)),$allowed_types))
					{
						$html .= '<div style="float:left;margin:5px 2px;">';
						$html .= '<div>';
						//$html .= '	<img src="'.$img_url.'/'.$imgfile.'" width="156px" height="129px" />';
						$html .= '	<img src="/image.php?size=156&type=3&path='.$postcode.'/images/'.$file.'" />';
		    			$html .= '</div>';
						$html .= '<div style="text-align:center;">';
						//$html .= '<input type="radio" name="primary_photo" value="'.$file.'" />Cover Photo';
						$html .= '</div>';
						$html .= '<div style="text-align:center;">';
						$html .= '<input type="checkbox" name="ihousephotos[]" value="'.$file.'" '.((in_array($file,$rows))?'checked="yes"':'') . ' />Tick to Display';
						$html .= '</div>';
						$html .= '</div>';
					}
				}
			}
		}
		
		return $html;
	}
	
	
	function afterSubmit($ads_id, $plan_id) {
		$db = $this->getDBO();
		
		
		$user			=& 	JFactory::getUser();
		$user_id		= 	$user->id;
		$user			=& 	JFactory::getUser($user_id);
		$user_id		= 	$user->id;
		
		$query = " SELECT validity,rate FROM #__cbsubs_plans WHERE id = '".$plan_id."' LIMIT 1 ";
	
			$db->setQuery( $query );
			$t = $db->loadObject(); 

		$validity 	= 	$t->validity;
		$deduct_by	=	$t->rate;

		$date = explode("-", $validity);
		$year = $date[0];
		$month = $date[1];
		$day = substr($date[2], 0, 2);

		$expirydate = date("Y-m-d H:i:s", mktime(date("H"),date("i")+1,date("s"),date("m")+$month,date("d")+$day,date("Y")+$year));

		
		switch($plan_id) {
			
			case 4	:
			
				$sql = " UPDATE #__ihouse_users_credit SET credit = credit - ".(int)$deduct_by." WHERE user_id = '".$user_id."'";
					$db->setQuery($sql);
					$db->query();
				
				$sql = " SELECT credit FROM #__ihouse_users_credit WHERE user_id = '$user_id' ";
					$db->setQuery($sql);
					$credit = (int) $db->loadResult();
				
				$balance = $credit;
				
				$sql = "INSERT INTO #__users_transaction_log VALUES (NULL, '".$user_id."', 'Per Pay Basis', '0', '6', '".$balance."', CURRENT_TIMESTAMP)";
					$db->setQuery($sql);
					$db->query();
		
		
				$query = "INSERT INTO #__cbsubs_subscriptions (id,status,user_id,plan_id,parent_plan,parent_subscription,replaces_plan,replaces_subscription,subscription_date,last_renewed_date,expiry_date,next_event_date,next_event_id,next_event_params,autorenew_type,autorecurring_type,regular_recurrings_total,regular_recurrings_used,previous_recurrings_used,previous_expiry_date,previous_status,ip_addresses,integrations)"
	
					. "\n VALUES ( NULL, 'A', '".$user_id."', '".$plan_id."', '0', '0', NULL, NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,'".$expirydate."', NULL, '0', '', '0', '0', '0', '1', '0', NULL, 'R', '".$ip."', '')";
			
					$db->setQuery( $query );
					$db->query();
			
				$sid = $db->insertid();
				break;
				
			case 25 :
			
				$query = "INSERT INTO #__cbsubs_subscriptions (id,status,user_id,plan_id,parent_plan,parent_subscription,replaces_plan,replaces_subscription,subscription_date,last_renewed_date,expiry_date,next_event_date,next_event_id,next_event_params,autorenew_type,autorecurring_type,regular_recurrings_total,regular_recurrings_used,previous_recurrings_used,previous_expiry_date,previous_status,ip_addresses,integrations)"
	
					. "\n VALUES ( NULL, 'A', '".$user_id."', '".$plan_id."', '0', '0', NULL, NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,'".$expirydate."', NULL, '0', '', '0', '0', '0', '1', '0', NULL, 'R', '".$ip."', '')";
			
					$db->setQuery( $query );
					$db->query();
				$sid = $db->insertid();
				break;
		}
		
		switch($plan_id) {
			
			case 4: 	/* Pay Per Basis */
			case 25 : 	/* Basic Package - Owner */	
				$query = "UPDATE #__ihouse_ads SET subscription_id = '".$sid."' WHERE id = '$ads_id' ";

				$db->setQuery( $query );
				$db->query();
				break;
			case 2: /* Quarterly Subs */
			case 3: /* Yearly Subs */
				
				/* CHECK IF IT IS EXIST AND NOT EXPIRED */
				$query = "SELECT id FROM #__cbsubs_subscriptions WHERE user_id = '$user_id' AND plan_id = '$plan_id' ";
					$db->setQuery( $query );
					$sid = $db->loadResult();
				
				if($sid) {
				
					$query = "UPDATE #__ihouse_ads SET subscription_id = '".$sid."' WHERE id = '$ads_id' ";

					$db->setQuery( $query );
					$db->query();
				}
				
				
				/* CHECK SLOT WHETHER IT IS EXIST YET */
				$query = "SELECT id FROM #__ihouse_ads_subscription_slot WHERE user_id = '$user_id' ";
					$db->setQuery( $query );
					$slot = $db->loadResult();
				
				if(!empty($slot)) {
					$query = "UPDATE #__ihouse_ads_subscription_slot SET slot = slot - 1 WHERE user_id = '$user_id' ";
				
					$db->setQuery( $query );
					$db->query();
				} else {
					/* AFTER SUBMIT SO IT IS ALREADY -1 */
					$query = "INSERT INTO #__ihouse_ads_subscription_slot(user_id,slot) VALUES('$user_id','49') "; 
				
					$db->setQuery( $query );
					$db->query();
				}
				break;
				
		}
		
		//return true;
	
	}
	
	function updatePrimaryPhoto() {
		$db = $this->getDBO();
		
		$user			=& 	JFactory::getUser();
		$user_id		= 	$user->id;
		
		$sess_id		=	JRequest::getVar('session_id', '');
		$id				=	JRequest::getVar('id', '');
		
		if(!$id)
			return false;
		/* DEFAULT */
		$query = " UPDATE #__ihouse_ads_image SET is_primary = '0' WHERE sess_id = '$sess_id' ";

			$db->setQuery( $query );
			$db->query();
		
		/* SET PRIMARY PHOTO */
		$query = " UPDATE #__ihouse_ads_image SET is_primary = '1' WHERE id = '$id' ";

			$db->setQuery( $query );
			$db->query();
			
		return true;	
	}
	
	function edit_ads($ads_id = 0) {
		
		if(!$ads_id)
			return false;
	
		$db = $this->getDBO();

		$user		=& 	JFactory::getUser();
		$uid		= 	$user->id;
		$user		=& 	JFactory::getUser($uid);
		$uid		= 	$user->id;
		
		$property_type				=	JRequest::getVar('property_type', '');
		
		switch($property_type) {
			case '公寓' :
				$property_id				=	JRequest::getVar('property_name', ''); /* property id */
		
				$query = " SELECT name_en FROM #__ihouse_property WHERE id = '$property_id' ";
					$db->setQuery( $query );
					$property_name = $db->loadResult();
				break;
				
			case '组屋' :
				break;
			case '服务公寓' :
				$property_name				=	JRequest::getVar('project_name', ''); 
				$property_name = str_replace("'","\'",$property_name);
				break;
			case '商业' :
				$property_name				=	JRequest::getVar('project_name', ''); 
				$property_name = str_replace("'","\'",$property_name);
				break;
			case '有地住宅'	:
				$property_name				=	JRequest::getVar('project_name', ''); 
				$property_name = str_replace("'","\'",$property_name);
				break;
		}
		
		$hdb_town					=	JRequest::getVar('hdb_town', '');
		$property_category			=	JRequest::getVar('property_category', '');
		
		
		$session_id					=	JRequest::getVar('session_id', '');
		$primary_photo				=	JRequest::getVar('primary_photo', 0);
		
		$ihouse_photos_arr			=	JRequest::getVar('ihousephotos', array());
		
		$flattype					=	JRequest::getVar('flattype', '');
		$asking_price				=	str_replace(',','', JRequest::getVar('asking_price',''));
		$psm_price					=	str_replace(',','', JRequest::getVar('psm_price', ''));
		$size						=	str_replace(',','', JRequest::getVar('size', ''));
		$no_of_room					=	JRequest::getVar('no_of_room', 0);
		$property_district			=	JRequest::getVar('district', '');
		$no_of_hall					=	JRequest::getVar('no_of_hall', 0);
		$postcode					=	JRequest::getVar('postcode', '');
		$no_study_room				=	JRequest::getVar('no_study_room', 0);
		$tenure						=	JRequest::getVar('tenure', '');
		$toilet						=	JRequest::getVar('toilet', 0);
		$top_year					=	JRequest::getInt('year_built', 0);
		$floor_level				=	JRequest::getVar('floor_level', '');
		$block_house_no				=	JRequest::getVar('block_house_no', '');
		$floor_unit_no				=	JRequest::getVar('floor_unit_no', '');
		$street_name				=	JRequest::getVar('street_name', '');
		$street_name	 			= 	str_replace("'","\'",$street_name);
		$country					=	JRequest::getVar('country', '');
		$ad_title					=	JRequest::getVar('ad_title', '');
		$ad_title	 				= 	str_replace("'","\'",$ad_title);
		$add_info					=	JRequest::getVar('add_info', '');
		$add_info					=	str_replace("'","\'",$add_info);
		$renovation					=	JRequest::getVar('renovation', '');
		$status						=	JRequest::getVar('status', '');
		
		$rental_type				=	JRequest::getVar('rental_type','');
		$ad_type					= 	JRequest::getVar('ad_type','');
		
		if($status == 'pending') {
			if($ad_type == 'rent')
				$status = '待租';
			
			if($ad_type == 'sell')
				$status = '待售';
		}
		
		$query = 	" UPDATE #__ihouse_ads SET "
					. " property_id = '$property_id', "								  
					. " postcode = '$postcode', "			
					. " property_type = '$property_type', "
					. " hdb_town_id = '$hdb_town', "
					. " flat_type = '$flattype', "
					. " property_category = '$property_category', "
					. " property_name = '$property_name', "
					. " ask_price = '$asking_price', "
					. " psm_price = '$psm_price', "
					. " size = '$size', "
					. " rental_type = '$rental_type', "
					. " no_of_room = '$no_of_room', "
					. " no_of_hall = '$no_of_hall', "
					. " study_room = '$no_study_room', "
					. " toilet = '$toilet', "
					. " property_district_id = '$property_district', "
					. " tenure = '$tenure', "
					. " top_year = '$top_year', "
					. " block_no = '$block_house_no' , "
					. " floor_unit_no = '$floor_unit_no', "
					. " street_name = '$street_name', "
					. " floor_level = '$floor_level' , "
					. " country = '$country', "
					. " ad_title = '$ad_title', "
					. " ad_type = '$ad_type', "	
					. " add_info = '$add_info', "
					. " renovation = '$renovation', "
					. " status_id = '$status' "
					. " WHERE id = '$ads_id' ; ";

			$db->setQuery( $query );
			$db->query();
		
		
		$fp1			=	JRequest::getVar('fp1', ''); 
		$fp2			=	JRequest::getVar('fp2', '');
		
		$query	=	" DELETE FROM #__ihouse_ads_floorplan WHERE ads_id = '$ads_id' ";

			$db->setQuery( $query );
			$db->query();
			
		if(!empty($fp1)) {
			$tmp 	= 	explode('|', $fp1);
			$fp1	= 	$tmp[0].'/'.$tmp[1];
			
			$query = " INSERT INTO #__ihouse_ads_floorplan VALUES( NULL , "
					.	" '$ads_id', "
					.	" '$fp1', "
					.	" '1' "
					. " ); ";
				
			$db->setQuery( $query );
			$db->query();
		}
		
		if(!empty($fp2)) {
			$tmp 	= 	explode('|', $fp2);
			$fp2	=	$tmp[0].'/'.$tmp[1];
			
			$query = " INSERT INTO #__ihouse_ads_floorplan VALUES( NULL , "
					.	" '$ads_id', "
					.	" '$fp2', "
					.	" '2' "
					. " ); ";
		
			$db->setQuery( $query );
			$db->query();
		}

		$query	=	" DELETE FROM #__ihouse_ads_image2 WHERE ads_id = '$ads_id' ";
			$db->setQuery( $query );
			$db->query();

		foreach($ihouse_photos_arr as $ih):
		
			$query = "	INSERT INTO #__ihouse_ads_image2 VALUES( NULL , "
						.	" '$ads_id' , "
						.	" '$ih' "
						. " ); ";
			
				$db->setQuery( $query );
				$db->query();
			
		endforeach;
		
		$query = " UPDATE #__ihouse_ads_image SET ads_id = '$ads_id' WHERE sess_id = '$session_id' ";
		
			$db->setQuery( $query );
			$db->query();
		
		return true;
		
	}
	
	function refresh($ads_id) {
		
		$user 	=& JFactory::getUser();
		$user 	=& JFactory::getUser($user->id);
	 	$uid 	= $user->get('id');
		
		$user_cat = $user->get('user_category');
		
		$db 	= $this->getDBO();
		
		/* SET COUNTER TO 0 */
		/*$query = " UPDATE #__ihouse_popular SET counter = 0 WHERE ads_id = '$ads_id' AND type= 'ads' ";
			$db->setQuery( $query );
			$db->query();
		*/
		/* Agent Subscription - Unlimited Refresh ----*/
		/* CHECK SUBSCRIPTION ID */
		$query = " SELECT subscription_id FROM #__ihouse_ads WHERE id = '$ads_id' LIMIT 1";
			$db->setQuery( $query );
			$subs_id	=	$db->loadResult();
			
		$query = " SELECT plan_id FROM #__cbsubs_subscriptions WHERE id = '$subs_id' LIMIT 1";
			$db->setQuery( $query );
			$plan_id	=	$db->loadResult();
		
		if(empty($plan_id))
			$plan_id = 4;
			
		/* Pay Per Ads - Refresh (Agent) && Simple Owner */
		if($plan_id == 4) {
			
		
			$query = " SELECT * FROM #__ihouse_ads_refresh WHERE ads_id = '$ads_id' LIMIT 1 ";
				$db->setQuery( $query );
				$row	=	$db->loadObject();
		
			if(!empty($row)) { // exist and less than 6 credits caps listing
				
				if($row->value < 6) {
					$query = " SELECT credit FROM #__ihouse_users_credit WHERE user_id = '$uid' LIMIT 1 ";
						$db->setQuery( $query );
						$credit = $db->loadResult();
			
					if($credit < 2) {
						return false;
					}
					
					$query = " UPDATE #__ihouse_ads_refresh SET value = value + 2 WHERE ads_id = '$ads_id' LIMIT 1 ";
						$db->setQuery( $query );
						$db->query();
				
					$balance = $credit - 2;
					
					$query = " UPDATE #__ihouse_users_credit SET credit = '$balance' WHERE user_id = '$uid' LIMIT 1 ";

						$db->setQuery( $query );
						$db->query();
					
					$query = "INSERT INTO #__users_transaction_log VALUES (NULL, '$uid', 'Refresh Ads', '0', '2', '$balance', CURRENT_TIMESTAMP)";
						$db->setQuery( $query );
						$db->query();
				}
				
			} else {	// haven't refresh yet
				
				$query = " SELECT credit FROM #__ihouse_users_credit WHERE user_id = '$uid' LIMIT 1 ";
					$db->setQuery( $query );
					$credit = $db->loadResult();
		
				if($credit < 2) {
					return false;
				}
				$query = "INSERT INTO #__ihouse_ads_refresh VALUES(NULL,'$ads_id', 2); ";
				$db->setQuery( $query );
				$db->query();
				
				$balance = $credit - 2;
				
				$query = " UPDATE #__ihouse_users_credit SET credit = '$balance' WHERE user_id = '$uid' LIMIT 1 ";
					$db->setQuery( $query );
					$db->query();
				
					
				$query = "INSERT INTO #__users_transaction_log VALUES (NULL, '$uid', 'Refresh Ads', '0', '2', '$balance', CURRENT_TIMESTAMP)";
					$db->setQuery( $query );
					$db->query();
			}
		}
		
		$query = " UPDATE #__ihouse_ads SET posting_date = '".date("Y-m-d H:i:s", time())."' WHERE id = '$ads_id' LIMIT 1 ";
			$db->setQuery( $query );
			$db->query();
			
		return true;
	}
	
	
	function checkForOwner() {
		$db = $this->getDBO();
		
		$user =& JFactory::getUser();
	 	$uid 	= $user->get('id');
		$user =& JFactory::getUser($uid);
		$user_cat 	= $user->get('user_category');
		
		if($user_cat == 'Owner') {
			$query = " SELECT COUNT(id) FROM #__ihouse_ads WHERE posted_by = '$uid' ";
				$db->setQuery( $query );
				$c = $db->loadResult();
			
			if($c == 1) {
				
				$query = " SELECT subscription_id FROM #__ihouse_ads WHERE posted_by = '$uid' LIMIT 1 ";
				$db->setQuery( $query );
				$subs_id = $db->loadResult();
				
				$query = " SELECT plan_id FROM #__cbsubs_subscriptions WHERE id = '$subs_id' LIMIT 1";
					$db->setQuery( $query );
				
				$plan_id = $db->loadResult();
				
			} else if($c == 2) {
				$plan_id = 1;
			} else {
				$query = " SELECT id,plan_id FROM #__cbsubs_subscriptions WHERE user_id = '$uid' LIMIT 1";
					$db->setQuery( $query );
					$tmp = $db->loadObject();
				
				$plan_id = $tmp->plan_id;
			}
			
					
			if(empty($plan_id)) { /* No Plan Id at all */
				return true;
			}
				
			if($plan_id == 1) /* Upgraded*/
			{
				if($c == 2) 
					return false;
				else
					return true;
			} 
			else if($plan_id == 25) /* Basic Package */
			{
				if($c == 1) 
					return false;
				else
					return true;
			}
		}
		return true;
	}
	
	function checkForAgent() {
		$db = $this->getDBO();
		
		$user =& JFactory::getUser();
	 	$uid 	= $user->get('id');
		$user =& JFactory::getUser($uid);
		$user_cat 	= $user->get('user_category');
		
		if($user_cat == 'Agent') {
			/* if no subs, plan_id = 4 */
			
			$query = " SELECT credit FROM #__ihouse_users_credit WHERE user_id = '$uid' LIMIT 1";
					$db->setQuery( $query );
			$credit = $db->loadResult();
			
			/* QUARTERLY AND YEARLY SUBSCRIPTION */
			$query = 	" SELECT COUNT(id) "
						.	" FROM #__cbsubs_subscriptions " 
						.	" WHERE user_id = '$uid' AND status = 'A' AND (plan_id = 2 OR plan_id = 3) LIMIT 1";
							
				$db->setQuery( $query );
				$exist = $db->loadResult();	
				
			if(!empty($exist)) {
				/* CHECK SLOT */
				$query = 	" SELECT slot "
						.	" FROM #__ihouse_ads_subscription_slot " 
						.	" WHERE user_id = '$uid' LIMIT 1";
					$db->setQuery( $query );
					$slot = $db->loadResult();	
				
				// if empty slot, send error message
				if($slot == 0) {
					return false;
				}
				return true;
			}
			
			// if less than 6 credits, send error msg
			if($credit < 6) {
				return false;
			}
			
			return true;
		}
		return true;
	}
	
}
