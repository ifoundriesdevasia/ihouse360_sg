<?php
/**
* @version		$Id: poll.php 10752 2008-08-23 01:53:31Z eddieajau $
* @package		Joomla
* @subpackage	Polls
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class AdsModelProperty extends JModel
{
	function __construct()
	{
		global $mainframe;

		parent::__construct();

	}
	
	function getHDBTownListHTML($row = '') {
		$db = $this->getDBO();

		$query 	= ' SELECT * FROM #__ihouse_hdb_town ';
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		$html = '';
		$html .= '<select name="hdb_town" id="hdb_town" >';
		$html .= '<option value="">----</option> ';
		
		foreach($rows as $r):
		
		$selected = '';
		if($r->hdb_town_ch == $row)
			$selected = ' selected="selected" ';
			
		$html .= '<option '.$selected.' value="'.$r->hdb_town_ch.'" >'.$r->hdb_town_en.'</option>';
		
		endforeach;
       
        $html .= '</select>';
		
		return $html;
	}
	
	function getPropertyCategoryListHTML($row = '', $property_type = '') {
		$db		=& 	JFactory::getDBO();

		$query 	= ' SELECT * FROM #__ihouse_ads_property_category ';
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		$html = '';
		$html .= '<select name="property_category" id="property_category">';
		$html .= '<option value="">----</option>';
		foreach($rows as $rw):
			$selected = '';	
			
			if($rw->name_ch == $row) 
				$selected = 'selected="selected"';
			
			if($rw->name_ch == '圣淘沙') {
				if($property_type == 'commercials' || $property_type == 'landed' || $property_type == 'apartments') { 
					$html .= '';
				} else {
					$html .= '<option '.$selected.' value="'.$rw->name_ch.'" >'.$rw->name_en.'</option>';
				}
			} else {
				$html .= '<option '.$selected.' value="'.$rw->name_ch.'" >'.$rw->name_en.'</option>';
			}
			
		
		endforeach;
       
        $html .= '</select>';
		
        return $html;
	}
	
	function getProjectNameHTML($input = '', $property_type) {
		$html = '';
		
		if($property_type == 'commercials' || $property_type == 'landed' || $property_type == 'apartments') {
			$html = '<input type="text" name="project_name" id="project_name" style="width:450px;" value="'.$input.'" />';
		}
		
		return $html;
	}
	function getPropertyNameHTML($session_id = '', $property_id = 0) {
		$db		=& 	JFactory::getDBO();
		
		$query 	= " SELECT * FROM #__ihouse_property WHERE property_type = '公寓' ORDER BY name_en ASC ";
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		$html = '';
		$html .= '<select name="property_name" id="property_name" onchange="javascript:loadPropertyId(this.value, \''.$session_id.'\');">';
		$html .= '<option value="">----</option> ';
       	foreach($rows as $rw):
			
			$selected = '';	
			
			if($rw->id == $property_id) 
				$selected = 'selected="selected"';
			
			$html .= '<option '.$selected.' value="'.$rw->id.'" >'.$rw->name_en.'</option>';
		
		endforeach;
        $html .= '</select>';
		return $html;
	}
	
	
	function getAskingPriceHTML($input = '') {
		if(!empty($input))
			$input 	= str_replace(',','', $input);
		// onfocus="javascript:stripCommas(this.value,\'asking_price\');" onblur="javascript:formatNumber(this.value, 0,\'asking_price\');"
		$html 	= '<input type="text" name="asking_price" id="asking_price" size="20" value="'.(($input)?number_format($input):'').'"  maxlength="15" onkeyup="javascript:formatNumber(this.value, 0,\'asking_price\');calculate_persquare();" onkeypress="return isNumberKey(event);" />';
		
		return $html;
	}
	
	function getPSMPriceHTML($input = '') {
		if(!empty($input))
			$input 	= preg_replace('/,/','', $input);
			
		$html = '<input type="hidden" id="psm_price" name="psm_price" size="5" value="'.$input.'" class="" maxlength="15" readonly="readonly" />';
		$html .= '<strong><span id="psm_price_txt">'.$input.'</span></strong>';
		
		return $html;
	}
	
	function getPSFPriceHTML($input = '') {
		if(!empty($input))
			$input 	= preg_replace('/,/','', number_format($input));
			
		$html = '<input type="hidden" id="psf_price" name="psf_price" size="5" value="'.$input.'" class="" maxlength="50" readonly="readonly" />';
		$html .= '<strong><span id="psf_price_txt">'.$input.'</span></strong>';
		return $html;
	}
	
	function getSizeHTML($input = '') {
		if(!empty($input))
			$input 	= preg_replace('/,/','', $input);
			
		$html = '<input type="text" name="size" id="size" size="20" value="'.(($input)?number_format($input):'').'" class="" maxlength="8"  onkeyup="javascript:formatNumber(this.value, 0,\'size\');calculate_persquare();" onkeypress="return isNumberKey(event);" />';
		
		return $html;
	}
	
	function getNoOfRoomListHTML($row = '') {
		$html = '';
		$html .= '<select name="no_of_room" class="">';
		for($i = 0; $i < 8; $i++) :
			$selected = '';
			if($i == $row)
				$selected = 'selected="selected"';
			
			if($i == 7)
				$html .= '<option '.$selected.' value="'.$i.'" >7+</option>';
			else
				$html .= '<option '.$selected.' value="'.$i.'" >'.$i.'</option>';
		endfor;
		$html .= '</select>';
		
		return $html;
	}

	function getNoOfHallListHTML($row = '') {
		$html = '';
		$html .= '<select name="no_of_hall" class="">';
		for($i = 0; $i < 5; $i++) :
			$selected = '';
			if($i == $row)
				$selected = 'selected="selected"';
				
			if($i == 4)	
				$html .= '<option '.$selected.' value="'.$i.'" >4+</option>';
			else
				$html .= '<option '.$selected.' value="'.$i.'" >'.$i.'</option>';
		endfor;

		$html .= '</select>';
		
		return $html;

	}
	
	function getPostcodeHTML($input = '', $property_type = '' ) {
		$html = '';
		
		if($property_type == 'hdb' || $property_type == 'commercials' || $property_type == 'landed' || $property_type == 'apartments') {
			$html = '<input type="text" name="postcode" id="postcode" size="5" value="'.$input.'" class="" maxlength="6" />';
		
		} else {
			$html = '<input type="hidden" name="postcode" id="postcode" size="5" value="'.$input.'" class="" maxlength="6" />';
			$html .= '<strong>'.$input.'</strong>';
		}
		return $html;
	}
	
	function getNoOfStudyRoomListHTML($row = '') {
		
		
		$html = '';
		$html .= '<select name="no_study_room" class="">';
		for($i = 0; $i < 3; $i++) :
			$selected = '';
			if($i == $row)
				$selected = 'selected="selected"';
			
			if($i == 2)
				$html .= '<option '.$selected.' value="'.$i.'" >2+</option>';
			else
				$html .= '<option '.$selected.' value="'.$i.'" >'.$i.'</option>';
		endfor;
		$html .= '</select>';
		
		return $html;

	}                 
	
	function getNoOfToiletListHTML($row = '') {
		$html = '';
		$html .= '<select name="toilet" class="">';
		
		for($i = 0; $i < 6; $i++) :
			$selected = '';
			if($i == $row)
				$selected = 'selected="selected"';
			
			if($i == 5)
				$html .= '<option '.$selected.' value="'.$i.'" >5+</option>';
			else
				$html .= '<option '.$selected.' value="'.$i.'" >'.$i.'</option>';
		endfor;
		$html .= '</select>';
		
		return $html;
		
	}
	
	function getRentalTypeListHTML($row = '') {
		$html = '';
		$html .= '<select name="rental_type" class="" id="rental_type" >';
		$html .= '<option value="">----</option> ';
		$html .= '<option '.(($row == '整套')?'selected="selected"':'').' value="整套" >Whole Unit</option>';
		$html .= '<option '.(($row == '单间')?'selected="selected"':'').' value="单间" >Room</option>';
		$html .= '<option '.(($row == '搭房')?'selected="selected"':'').' value="搭房" >Share Room</option>';
		$html .= '</select>';
		
		return $html;
	}
	function getPropertyDistrictHTML($input = '', $property_type = '') {
		$html = '';
		if($property_type == 'commercials' || $property_type == 'landed' || $property_type == 'apartments') {
			$db = $this->getDBO();
			
			$query = " SELECT * FROM #__ihouse_district ";
				$db->setQuery( $query );
				$rows = $db->loadObjectList();
			
			$html = '<select name="district">';
			$html .= '<option value="">---</option>';
			foreach($rows as $r) : 
				$selected = '';
				if($r->code == $input)
					$selected = ' selected="selected" ';
			
				$html .= '<option value="'.$r->code.'" '.$selected.'>'.$r->code.' '.$r->district_en.'</option>';
			endforeach;
			$html .= '</select>';
			
		} else {
			$html = '<input type="hidden" name="district" id="property_district" size="5" value="'.$input.'" class="" maxlength="50" />';
			$html .= '<strong>'.$input.'</strong>';
		}
		return $html;
	}
	
	function getTenureListHTML($input = '', $property_type = '') {
		$html = '';
		if($property_type == 'commercials' || $property_type == 'landed' || $property_type == 'apartments') {
			$html = '<input type="text" name="tenure" id="tenure" size="5" value="'.$input.'" class="" maxlength="50" />';
		} else {
			$html = '<input type="hidden" name="tenure" id="tenure" size="5" value="'.$input.'" class="" maxlength="50" />';
			$html .= '<strong>'.$input.'</strong>';
		}
		
		return $html;
	}
	
	function getTopYearListHTML($input = '', $property_type = '') {
		
		if($property_type == 'condo') {
			$html = '<input type="hidden" name="year_built" id="year_built" size="4" value="'.$input.'" class="" maxlength="4" />';
			$html .= '<strong>'.$input.'</strong>';
		} else if($property_type == 'hdb' || $property_type == 'commercials' || $property_type == 'landed' || $property_type == 'apartments') {
			$html = '<input type="text" name="year_built" id="year_built" size="4" value="'.$input.'" class="" maxlength="4" />';
		}
		
		return $html;
		
	}
	
	function getFloorLevelListHTML($row = '') {
		$html = '<select name="floor_level" class="" id="floor_level">';
		$html .= '<option value="">----</option> ';
        $html .= '<option '.(($row == '高')?'selected="selected"':'').'value="高" >High</option>';
        $html .= '<option '.(($row == '中')?'selected="selected"':'').'value="中" >Mid</option>';
        $html .= '<option '.(($row == '低')?'selected="selected"':'').'value="低" >Low</option>';
        $html .= '</select>';
		
		return $html;
	}
	
	function getFlatTypeListHTML($row = '') {
		
		$html = '<select name="flattype" class="" id="flattype">';
		$html .= '<option value="">----</option> ';
        $html .= '<option '.(($row == '1房式')?'selected="selected"':'').'value="1房式" >1房式</option>';
        $html .= '<option '.(($row == '2房式')?'selected="selected"':'').'value="2房式" >2房式</option>';
        $html .= '<option '.(($row == '3房式')?'selected="selected"':'').'value="3房式" >3房式</option>';
		$html .= '<option '.(($row == '4房式')?'selected="selected"':'').'value="4房式" >4房式</option>';
        $html .= '<option '.(($row == '5房式')?'selected="selected"':'').'value="5房式" >5房式</option>';
		$html .= '<option '.(($row == '公寓式')?'selected="selected"':'').'value="公寓式" >公寓式</option>';
        $html .= '</select>';
		
		return $html;
	}
	
	
	function getRenovationListHTML($row = '') {
		
		$html = '<select name="renovation" class="" id="renovation">';
		$html .= '<option value="">----</option> ';
		$html .= '<option '.(($row == '新装修')?'selected="selected"':'').' value="新装修" >Newly Renovated</option>';
        $html .= '<option '.(($row == '普通')?'selected="selected"':'').' value="普通" >Normal</option>';
        $html .= '<option '.(($row == '豪华')?'selected="selected"':'').' value="豪华" >Luxurious</option>';
        $html .= '</select>	';
		
		return $html;
	}
	
	function getStatusListHTML($row = '') { /* at postads, default is pending , changing happen on editads only */
		$user 		=& JFactory::getUser();
		$uid		=	$user->id;
		$user 		=& JFactory::getUser($uid);
		$user_cat 	= $user->get('user_category');
		 
		if($row->ad_type == 'rent') 
			$t = '待租'; 
		if($row->ad_type == 'sell') 
			$t = '待售'; 
		
		$html = '<select name="status" class="" id="status">';
		$html .= '<option '.(($row->status_id == '待租' || $row->status_id == '待售')?'selected="selected"':'').' value="'.$t.'" >Pending</option>';
			
		if($user_cat == 'Agent' && $row->ad_type == 'sell') {
        	$html .= '<option '.(($row->status_id == '已售')?'selected="selected"':'').' value="已售" >Sold</option>';
		} 
		
		if($row->ad_type == 'rent') {
			$html .= '<option '.(($row->status_id == '已租')?'selected="selected"':'').' value="已租" >Rented</option>';
		}

        $html .= '</select>	';
		
		return $html;
	}
	
	function getBlockNoHTML($input = '') {
		$html = '<input type="text" name="block_house_no" id="block_house_no" size="30" value="'.$input.'" class="" maxlength="50" />';
		
		return $html;
	}
	
	function getFloorUnitNoHTML($input = '') {
		$html = '<input type="text" name="floor_unit_no" id="floor_unit_no" size="30" value="'.$input.'" class="" maxlength="50" />';
		
		return $html;
	}
	
	function getStreetNameHTML($input = '', $property_type = '') {
		$html = '';
		
		if($property_type == 'hdb') {
			$db		=& 	JFactory::getDBO();
		
			$query 	= " SELECT street_name FROM #__ihouse_street_name ";
				
				$db->setQuery( $query );
				$rows	=	$db->loadObjectList();
			
			$html .= '<select name="street_name" id="street_name_hdb" >';
			$html .= '	<option value="">----</option>';
			foreach($rows as $r) :
			
				$name	= str_replace("'","\'", $r->street_name);
				
				$selected = '';
				if($r->street_name == $input)
					$selected = ' selected="selected" ';
				
				$html 	.= '	<option '.$selected.' value="'.$name.'">'.$r->street_name.'</option>';
			
			endforeach;
			
			$html .= '</select>';
		} else if($property_type == 'commercials' || $property_type == 'landed' || $property_type == 'apartments') {
			$html .= '<input type="text" name="street_name" id="street_name" style="width:450px;" value="'.$input.'" />';
		} else {
			$html .= '<input type="hidden" name="street_name" id="street_name" value="'.$input.'" /><b>'.$input.'</b>';
		}
		
		return $html;
	}
	function getCountryHTML($input = '') {
		$input = 'Singapore';
		//$html = '<input type="text" name="country" id="country" size="40" value="'.$input.'" class="" maxlength="50" />';
		$html = '<input type="hidden" name="country" id="country" value="'.$input.'" />'. $input;
		
		return $html;
	}
	function getAdTitleHTML($input = '') {
		$html = '<input type="text" name="ad_title" id="ad_title" size="30" value="'.$input.'" class="" maxlength="50" />';
		
		return $html;
	}
	function getAddInfoHTML($input = '') {
		$html = '<textarea name="add_info" id="add_info" cols="75" rows="5" />'.$input.'</textarea>';
		
		return $html;
	}
	
	
	function getAdTypeLISTHTML($row = '') {
		$user 		=& JFactory::getUser();
		$uid		=	$user->id;
		$user 		=& JFactory::getUser($uid);
		$user_cat 	= $user->get('user_category');
		
		$html = '<select name="ad_type" id="ad_type" class="" onchange="javascript:adType(this.value);">';
		$html .= '<option value="">----</option>';
		
		if($user_cat == 'Agent') {
			$html .= '<option '.(($row == 'sell')?'selected="selected"':'').' value="sell" >Sale</option>';
		}
		
		if($user_cat == 'Agent' || $user_cat == 'Owner') {
        	$html .= '<option '.(($row == 'rent')?'selected="selected"':'').' value="rent" >Rent</option>';
		}
        $html .= '</select>	';
		
		return $html;
	}
	
	function getAdsData($id) {
		$db		=& 	JFactory::getDBO();
		
		$query 	= " SELECT * FROM #__ihouse_ads AS a " 
					. " "
					. " WHERE a.id = '$id' ";
			$db->setQuery( $query );
			$row	=	$db->loadObject();
			
		return $row;	
	}
	
	function getFloorPlanHTML($name_id ,$postcode) {
		$db		=& 	JFactory::getDBO();
		
		$query	= "SELECT * FROM #__ihouse_floorplan ";
			$db->setQuery( $query );
			$floorplan = $db->loadObjectList();
		
		$html = '';
		$html .= '<select name="'.$name_id.'" id="'.$name_id.'" onchange="javascript:loadFloorPlanPicture(\''.$name_id.'\', this.value);">';
		$html .= '<option value="">---</option>';
		
		$allowed_types = array('png','jpg','jpeg','gif'); // list of filetypes you want to show
		
		foreach($floorplan as $fp) {
			$floorplan_path = JRoute::_('images/singapore/'.$postcode.'/floorplan/'.$fp->type_en);

			if(file_exists($floorplan_path)) {
				if ($handle = opendir($floorplan_path)) {
					while (false !== ($file = readdir($handle))) {
						if($file == '.' || $file == '..' || $file == 's')
							continue;

						if(in_array(strtolower(substr($file,-3)),$allowed_types))
						{
							$value2	=	$fp->type_en.'|'.$file;
							$html .= '<option '.(($value2==$value)?'selected="selected"':'').' value="'.$value2.'">'.$file.'</option>';
						}
					}
				}
			}
		}
		
		$html .= '</select>';
		
		return $html;
	}
	
}