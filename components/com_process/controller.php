<?php


// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.controller');


class processController extends JController
{
	/**
	 * constructor (registers additional tasks to methods)
	 * @return void
	 */
	function __construct()
	{
		parent::__construct();

		// Register Extra tasks
		//$this->registerTask( 'add'  , 'edit' );
		
		// Set the table directory
		//JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ihouse'.DS.'tables');

	}
	
	function display()
	{
		
		
	}
	
	function generatePopular() {
		global $mainframe;
		
		$model		= &$this->getModel('generate');
	
		$property	=	$model->generateTotalPopular();
		
		$mainframe->close();
		exit;
	}
	/* CROJOB */
	
	function cronjob() {
		global $mainframe;
		
		$model		= &$this->getModel('cron');
	
		$property	=	$model->cronjobExpirySubscription();
		
		$mainframe->close();
		exit;
	}
	
	
	/* CRONJOB - Cleaning Popular Log For This Week */
	
	function cronjob_popular() {
		global $mainframe;
		
		$model		= &$this->getModel('cron');
	
		$cron	=	$model->cronjobPopularThisWeek();
		
		$mainframe->close();
		exit;
	}
	
	function cleanup() {
		$model		= &$this->getModel('cleanup');
	
		$cron	=	$model->cleanupAds();
		
		$mainframe->close();
		exit;
	}
	
	function renew() {
		global $mainframe;
		
		$model		= &$this->getModel('cron');
		
		//$status		=	$model->renewMessages('ads');
		//$status1	=	$model->renewMessages('adtype');
		$status2	=	$model->renewMessages();
		
		$mainframe->close();
		exit;
	}
}