<?php
/**
* @version		$Id: poll.php 10752 2008-08-23 01:53:31Z eddieajau $
* @package		Joomla
* @subpackage	Polls
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class processModelCron1 extends JModel
{
	function __construct()
	{
		global $mainframe;

		parent::__construct();

	}
	
	function cronjobExpirySubscription() {
		
		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		
		$db = $this->getDBO();
		
		$query = 	" SELECT id, plan_id, user_id FROM #__cbsubs_subscriptions "
					.	" WHERE expiry_date <= '".date("Y-m-d H:i:s", time())."' "
					.	" AND status = 'A' ";	
					
			$db->setQuery( $query );
			$rows = $db->loadObjectList();
		
		if($db->getErrorNum()) {
			mail('andreas@ifoundries.com', 'iHouse Cron Job Error : Code 1 ', $this->messageHTML($db->stderr()), $headers);
			exit;
		}

		
		foreach($rows as $r) :
			
			$query = 	"UPDATE #__cbsubs_subscriptions SET status = 'X' , previous_status = 'A' WHERE id = ".$r->id." ";
				$db->setQuery( $query );
				$db->query();
			
			if($db->getErrorNum()) {
				mail('andreas@ifoundries.com', 'iHouse Cron Job Error : Code 2 ', $this->messageHTML($db->stderr()), $headers);
				exit;
			}
			
			switch($r->plan_id) {
				
				/* AGENT */
				
				case 2: /* Quarterly */
				/* SET SLOT TO NORMAL - 50 slots */
				$query = 	"UPDATE #__ihouse_ads_subscription_slot SET slot = 50 WHERE user_id = ".$r->user_id." ";
					$db->setQuery( $query );
					$db->query();
				
				if($db->getErrorNum()) {
					mail('andreas@ifoundries.com', 'iHouse Cron Job Error : Code 3 ', $this->messageHTML($db->stderr()), $headers);
					exit;
				}
					break;
				
				case 3: /* Yearly */
				/* SET SLOT TO NORMAL - 50 slots */
				$query = 	"UPDATE #__ihouse_ads_subscription_slot SET slot = 50 WHERE user_id = ".$r->user_id." ";
					$db->setQuery( $query );
					$db->query();
				
				if($db->getErrorNum()) {
					mail('andreas@ifoundries.com', 'iHouse Cron Job Error : Code 4 ', $this->messageHTML($db->stderr()), $headers);
					exit;
				}
					break;
					
				case 4: /* Per Pay Basis */
					/* Nothing to do */
					break;
				
				case 5: /* Expert for the Condo - Ad Type 1 */ 
					$query = " SELECT ads_type_config, property_id FROM #__ihouse_ads_type WHERE subscription_id = ".$r->id." ";
						$db->setQuery( $query );
						$xs = $db->loadObject();
					
					if($db->getErrorNum()) {
						mail('andreas@ifoundries.com', 'iHouse Cron Job Error : Code 5 ', $this->messageHTML($db->stderr()), $headers);
						exit;
					}
					
					$query = " UPDATE #__ihouse_ads_type_slot SET space = space + 1 WHERE ads_type_config = '".$xs->ads_type_config."' AND property_id = '".$xs->property_id."' ";
						$db->setQuery( $query );
						$db->query();
					
					if($db->getErrorNum()) {
						mail('andreas@ifoundries.com', 'iHouse Cron Job Error : Code 6 ', $this->messageHTML($db->stderr()), $headers);
						exit;
					}
					break;
					
				case 6: /* Recommended Specialist - Ad Type 2 */
					$query = " SELECT ads_type_config, property_id FROM #__ihouse_ads_type WHERE subscription_id = ".$r->id." ";
						$db->setQuery( $query );
						$xs = $db->loadObject();
					
					if($db->getErrorNum()) {
						mail('andreas@ifoundries.com', 'iHouse Cron Job Error : Code 7 ', $this->messageHTML($db->stderr()), $headers);
						exit;
					}
					
					$query = " UPDATE #__ihouse_ads_type_slot SET space = space + 1 WHERE ads_type_config = '".$xs->ads_type_config."' AND property_id = '".$xs->property_id."' ";
						$db->setQuery( $query );
						$db->query();
					
					if($db->getErrorNum()) {
						mail('andreas@ifoundries.com', 'iHouse Cron Job Error : Code 8 ', $this->messageHTML($db->stderr()), $headers);
						exit;
					}
					break;
				
				case 7: /* Condo Expert - Ad Type 3 */  
					$query = " SELECT ads_type_config FROM #__ihouse_ads_type WHERE subscription_id = ".$r->id." ";
						$db->setQuery( $query );
						$xs = $db->loadObject();
			
					if($db->getErrorNum()) {
						mail('andreas@ifoundries.com', 'iHouse Cron Job Error : Code 9 ', $this->messageHTML($db->stderr()), $headers);
						exit;
					}
			
					$query = " UPDATE #__ihouse_ads_type_slot SET space = space + 1 WHERE ads_type_config = '".$xs->ads_type_config."' ";
						$db->setQuery( $query );
						$db->query();
					
					if($db->getErrorNum()) {
						mail('andreas@ifoundries.com', 'iHouse Cron Job Error : Code 10 ', $this->messageHTML($db->stderr()), $headers);
						exit;
					}
					
					break;
					
						/* Featured Listing - Sidebar - Ad Type 4 */
				case 8: 
				case 9:
				case 10:
				case 11:
					$query = " SELECT ads_type_config, ads_id, property_type FROM #__ihouse_ads_type WHERE subscription_id = ".$r->id." ";
						$db->setQuery( $query );
						$xs = $db->loadObject();
					
					if($db->getErrorNum()) {
						mail('andreas@ifoundries.com', 'iHouse Cron Job Error : Code 11 ', $this->messageHTML($db->stderr()), $headers);
						exit;
					}
					
					$query = " UPDATE #__ihouse_ads_type_slot SET space = space + 1 WHERE ads_type_config = '".$xs->ads_type_config."' AND property_type = '".$xs->property_type."' ";
						$db->setQuery( $query );
						$db->query();
					
					if($db->getErrorNum()) {
						mail('andreas@ifoundries.com', 'iHouse Cron Job Error : Code 12 ', $this->messageHTML($db->stderr()), $headers);
						exit;
					}
					break;
						/* Featured Listing - At PropDetail(both sell and rent) - Ad Type 5 */
				case 12:
				case 13:
				case 14:
				case 15:
					$query = " SELECT ads_type_config, ads_id, property_id FROM #__ihouse_ads_type WHERE subscription_id = ".$r->id." ";
						$db->setQuery( $query );
						$xs = $db->loadObject();
					
					if($db->getErrorNum()) {
			mail('andreas@ifoundries.com', 'iHouse Cron Job Error : Code 13 ', $this->messageHTML($db->stderr()), $headers);
			exit;
		}
		
					$query = " UPDATE #__ihouse_ads_type_slot SET space = space + 1 WHERE ads_type_config = '".$xs->ads_type_config."' AND property_id = '".$xs->property_id."' ";
						$db->setQuery( $query );
						$db->query();
						
					if($db->getErrorNum()) {
			mail('andreas@ifoundries.com', 'iHouse Cron Job Error : Code 14 ', $this->messageHTML($db->stderr()), $headers);
			exit;
		}	
					break;
						/* Featured Listing - At SellAdsListing and RentAdsListing - Ad Type 6 */
				case 16:
				case 17:
				case 18:
				case 19:
					$query = " SELECT ads_type_config, ads_id, property_type FROM #__ihouse_ads_type WHERE subscription_id = ".$r->id." ";
						$db->setQuery( $query );
						$xs = $db->loadObject();
					
					if($db->getErrorNum()) {
			mail('andreas@ifoundries.com', 'iHouse Cron Job Error : Code 15 ', $this->messageHTML($db->stderr()), $headers);
			exit;
		}
					
					$query = " UPDATE #__ihouse_ads_type_slot SET space = space + 1 WHERE ads_type_config = '".$xs->ads_type_config."' AND property_type = '".$xs->property_type."' ";
						$db->setQuery( $query );
						$db->query();
					
					if($db->getErrorNum()) {
			mail('andreas@ifoundries.com', 'iHouse Cron Job Error : Code 16 ', $this->messageHTML($db->stderr()), $headers);
			exit;
		}
					break;
						/* Featured Listing - New Condos Sale - Ad Type 7 */
				case 20:
				case 21:
				case 22:
				case 23:
					$query = " SELECT ads_type_config FROM #__ihouse_ads_type WHERE subscription_id = ".$r->id." ";
						$db->setQuery( $query );
						$xs = $db->loadObject();
					
					if($db->getErrorNum()) {
			mail('andreas@ifoundries.com', 'iHouse Cron Job Error : Code 17 ', $this->messageHTML($db->stderr()), $headers);
			exit;
		}
					
					$query = " UPDATE #__ihouse_ads_type_slot SET space = space + 1 WHERE ads_type_config = '".$xs->ads_type_config."' ";
						$db->setQuery( $query );
						$db->query();
					
					if($db->getErrorNum()) {
			mail('andreas@ifoundries.com', 'iHouse Cron Job Error : Code 18 ', $this->messageHTML($db->stderr()), $headers);
			exit;
		}
					break;
				
				/* OWNER */
				
				case 1: /* Owner Refresh Subscription - 1 year only */
				case 25: /* Basic Package */
					/* Nothing to do */
					break;
					
			}
			
		endforeach;
		
		mail('andreas@ifoundries.com', 'iHouse Cron Job Suceess (ExpirySubscription): Completed! ', "Success! Cron Job Expiration on Subscription Checking!", $headers);
		
		return true;
	}
	
	function cronjobPopularThisWeek() {
		
		$db = $this->getDBO();
		
		/*$arr['last_path'] = dirname(substr(dirname(__FILE__),strlen($_SERVER['DOCUMENT_ROOT'])));
		$arr['dir_base'] = "{$_SERVER['DOCUMENT_ROOT']}{$arr['last_path']}";
		$arr['url_base'] = "http://{$_SERVER['HTTP_HOST']}{$arr['last_path']}";
		*/
		
		$query = 	" truncate #__ihouse_popular; ";
			
			$db->setQuery( $query );
			$db->query();
		
		return true;		
			
	}
	
	function renewMessage() {
		
		// To send HTML mail, the Content-type header must be set
		//$headers  = 'MIME-Version: 1.0' . "\r\n";
		//$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		
		$db = $this->getDBO();
		
		$query = 	" SELECT cb.id AS subs_id, cb.plan_id, cb.user_id FROM #__cbsubs_subscriptions AS cb "
					.	" RIGHT JOIN #__ihouse_ads AS a ON a.subscription_id = cb.id "
					.	" WHERE status = 'A' "
					.	" AND user_id = '63' ";	
			echo $query;		
			$db->setQuery( $query );
			$rows = $db->loadObjectList();
		
		if($db->getErrorNum()) {
			//mail('andreas@ifoundries.com', 'iHouse Cron Job Renew Error : Code 1 ', $this->messageHTML($db->stderr()), $headers);
			//exit;
		}
		
		foreach($rows as $r) :
			$plan_id	=	$r->plan_id;
			$subs_id	=	$r->subs_id;
			$user_id	=	$r->user_id;
			
			switch($plan_id) {
				case 1: /* Owner Refresh Subscription */
				case 2: /* Agent Quarterly Subs */
				case 3: /* Agent Yearly Subs */
					$strtotime = date("Y-m-d H:i:s", strtotime('+3 week'));
					break;
				case 4:
				case 5:
				case 6:
				case 7:
				case 25:
					$strtotime = date("Y-m-d H:i:s", strtotime('+1 week'));
					break;
				case 8:
				case 9:
				case 10:
				case 11:
				case 12:
				case 13:
				case 14:
				case 15:
				case 16:
				case 17:
				case 18:
				case 19:
				case 20:
				case 21:
				case 22:
				case 23:
					$strtotime = date("Y-m-d H:i:s", strtotime('+3 days'));
					break;
			}
			
			$query = 	" SELECT cb.id, cb.user_id, cb.plan_id, cp.name , cp.description " 
					.	" FROM #__cbsubs_subscriptions AS cb "
					.	" LEFT JOIN #__cbsubs_plans AS cp ON cp.id = cb.plan_id "
					.	" WHERE cb.expiry_date <= '$strtotime' "
					.	" AND cb.id = '$subs_id' ";	
					
				$db->setQuery( $query );
				$row = $db->loadObject();
				
			echo $query;
			/* SEND EMAIL */
			
			//$status = $this->SendEmailRenewal($row);
			
		endforeach;
		
		return true;
	}
	
	function messageHTML($message) {
		
		$html = '<html><body>';
		
		$html .= $message;
		
		$html .= '</body></html>';
		
		return $html;
	}
	
	function SendEmailRenewal($row) {
		global $mainframe;
		
		
		
		return true;
	}
}