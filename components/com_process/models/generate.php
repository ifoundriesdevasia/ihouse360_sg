<?php
/**
* @version		$Id: poll.php 10752 2008-08-23 01:53:31Z eddieajau $
* @package		Joomla
* @subpackage	Polls
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class processModelGenerate extends JModel
{
	function __construct()
	{
		global $mainframe;

		parent::__construct();

	}
	
	function generateTotalPopular() {
		
		$db = $this->getDBO();
		
		$query = 	" SELECT id FROM #__ihouse_property ";	
					
			$db->setQuery( $query );
			$rows = $db->loadObjectList();
		
		foreach($rows as $r) :
			$property_id = $r->id;
		
			$query = 	" SELECT counter FROM #__ihouse_popular "
						.	" WHERE property_id = '$property_id' AND type = 'property' " 
						.	" LIMIT 1 ";
				
				$db->setQuery( $query );
				$cproperty = (int) $db->loadResult();
		
			$query = 	" SELECT id FROM #__ihouse_ads WHERE property_id = '$property_id' ";
				$db->setQuery( $query );
				$adsid_arr = $db->loadObjectList();
			
			$total = 0;
			$total_cads = 0;
			
			foreach($adsid_arr as $a) :
			
				$query = 	" SELECT counter FROM #__ihouse_popular "
						.	" WHERE ads_id = '$property_id' AND type = 'ads' " 
						.	" LIMIT 1 ";
				
				$db->setQuery( $query );
				$cads	=	(int) $db->loadResult();
				
				$total_cads += $cads;
				
			endforeach;
			
			$total	=	$cproperty + $total_cads;
			
			$query = 	" SELECT id FROM #__ihouse_popular WHERE property_id = '$property_id' AND type = 'total' ";	
					
				$db->setQuery( $query );
				$exists = $db->loadResult();
			
			if(empty($exists)) {
			
				$query	=	"INSERT INTO #__ihouse_popular VALUES(NULL, 'total','$total','$property_id',0)";
			
					$db->setQuery( $query ) ;
					$db->query();
				
				} else {
			
				$query	=	" UPDATE #__ihouse_popular SET counter = '$total' "
						.	" WHERE property_id = '$property_id' AND type = 'total' " ;
					
					$db->setQuery( $query );
					$db->query();
			}
			
		endforeach;

		
		return true;
	}
	
}