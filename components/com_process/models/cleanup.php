<?php
/**
* @version		$Id: poll.php 10752 2008-08-23 01:53:31Z eddieajau $
* @package		Joomla
* @subpackage	Polls
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class processModelCleanupextends JModel
{
	function __construct()
	{
		global $mainframe;

		parent::__construct();

	}
	
	function cleanupAds() {
		
	
		$db = $this->getDBO();
		
		$query = 'truncate ihse_cbsubs_subscriptions;
					truncate ihse_ihouse_ads;
					truncate ihse_ihouse_ads_floorplan;
					truncate ihse_ihouse_ads_image;
					truncate ihse_ihouse_ads_image2;
					truncate ihse_ihouse_ads_refresh;
					truncate ihse_ihouse_ads_session;
					truncate ihse_ihouse_ads_subscription_slot;
					truncate ihse_ihouse_ads_type;

					truncate ihse_ihouse_property_rating;
					truncate ihse_ihouse_property_rating_avg;
					truncate ihse_ihouse_property_review;
					truncate ihse_ihouse_ads_rating;
					truncate ihse_ihouse_ads_rating_avg;

					update ihse_ihouse_ads_type_slot SET space = 1 WHERE ads_type_config = 1;
					update ihse_ihouse_ads_type_slot SET space = 3 WHERE ads_type_config = 2;
					update ihse_ihouse_ads_type_slot SET space = 6 WHERE ads_type_config = 3;
					update ihse_ihouse_ads_type_slot SET space = 50 WHERE ads_type_config = 4;
					update ihse_ihouse_ads_type_slot SET space = 8 WHERE ads_type_config = 5;
					update ihse_ihouse_ads_type_slot SET space = 8 WHERE ads_type_config = 6;
					update ihse_ihouse_ads_type_slot SET space = 50 WHERE ads_type_config = 7;
';					
			$db->setQuery( $query );
			$db->query();
			
			if($db->getErrorNum()) {
				mail('andreas@ifoundries.com', 'Error CleanUp Ads', $this->messageHTML($db->stderr()), $headers);
				exit;
			}
			
			return true;
	}
}