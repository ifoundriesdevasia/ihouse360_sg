function submit_property_review(property_id) {
	$('property_review_loader').setStyle('display','');
	$('userReviewMsg').setHTML('');	
	$('propertyreviewsubmit').send({
		onComplete	:	function(data) {
			var obj1	=	Json.evaluate(data);
			var status 	=	obj1.status;
			
			if(status) {
				getPropertyReview(0,  property_id);
			} else {
				$('userReviewMsg').setHTML('You have already submitted a review');	
			}
			$('property_review_loader').setStyle('display','none');
		}
	});	
}

function getPropertyReview(page, property_id) {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();			
	$('property_review_loader').setStyle('display','');
	
	var url = 'index.php?option=com_ihouse&task=getpropertyreview&r=' + unixtime_ms;		
	var req = new Ajax(url, {
	   					data		: 	
									{
										'page' 			: 	page,
										'property_id' 	:	property_id
									},
						method		: "get",
		    			onSuccess	: function(data) {
								$('userReviewMsg').setHTML('');	
								$('ajax-load-property-review').setHTML(data);
								$('property_review_loader').setStyle('display','none');
		   				},
						evalScripts : true
				}).request();				
}

