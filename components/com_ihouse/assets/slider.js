jQuery.noConflict();
jQuery(function() {			
	jQuery("#slider").slider({
		value:3,
		min: 0,
		max: 10,
		step: 1,
		slide: function(event, ui) {
			jQuery("#amount").val(ui.value);
		}
	});
	jQuery("#amount").val(jQuery("#slider").slider("value"));
});