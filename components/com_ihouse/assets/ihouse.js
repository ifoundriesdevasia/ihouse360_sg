function propertyDetailInfoTab(mode) {
	$$('.tabular1').each(function(el) {						  
		el.setStyle('display','none');
	});
	
	$$('ul.tab_nav5 li').each(function(el){
		el.removeClass('tabactive5');							 
	});
	
	$('tab_li5_' + mode).addClass('tabactive5');

	$('tab_' + mode).setStyle('display','');
}

function propertyInfoTab(mode) {
	$$('.prop_property_detail').each(function(el) {
		el.setStyle('display','none');

	});
	$('prop_' + mode).setStyle('display','');
}

function propertyDetailTab(mode) {
	$$('ul.tab_nav3 li').each(function(el){
		el.removeClass('tabactive3');							 
	});
	
	var propimg = $('property_detail_image');
	var fpimg	= $('floor_plan_image');
	var gmapimg	= $('google_map_image');
	
	switch(mode) {
		case 'image':
			
			$('tab_li3_image').addClass('tabactive3');
			$('propertyInfo12content').setStyle('height','460px');
			
			if(propimg) { propimg.setStyle('display','block'); 	}
			if(fpimg) 	{ fpimg.setStyle('display','none');		}
			if(gmapimg)	{ gmapimg.setStyle('display','none');	}
			break;
		case 'floorplan' :
			$('propertyInfo12content').setStyle('height', 'auto');
			$('tab_li3_floorplan').addClass('tabactive3');
			
			if(propimg) { propimg.setStyle('display','none'); 	}
			if(fpimg) 	{ fpimg.setStyle('display','block');	}
			if(gmapimg)	{ gmapimg.setStyle('display','none');	}
			break;
			
		case 'googlemap' :
			
			$('propertyInfo12content').setStyle('height', 'auto');
			$('tab_li3_googlemap').addClass('tabactive3');
		
			if(propimg) { propimg.setStyle('display','none'); 	}
			if(fpimg) 	{ fpimg.setStyle('display','none');		}
			if(gmapimg)	{ gmapimg.setStyle('display',''); }
			
			break;
	}
}

function displayFloorPlanTypeTabs(roomtype) {
	//$$('.displFloorPlan').setStyle('display','none');
	//$(roomtype + '_' + postcode).setStyle('display', '');
	
	$$('ul.tab_nav4 li').each(function(el){
		el.removeClass('tabactive4');							 
	});
	
	$('bed_' + roomtype).addClass('tabactive4');
	
	// ajax play here
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();			
	$('ajax-floorplan-image-detail').setHTML('<div style="padding:5px;">Loading</div>');
	var url = 'index.php?option=com_ihouse&task=ajaxLoadFloorPlan&r=' + unixtime_ms;		
	var req = new Ajax(url, {
	   			data		: 	
							{
								'roomtype' 			: 	roomtype,
								'postcode' 			:	postcode
							},
				method		: "get",
		    	onSuccess	: function(data) {
					$('ajax-floorplan-image-detail').setHTML(data);
					rokbox.refresh();
		   		},
				evalScripts : true
	}).request();	
	
}

function toggle(id) {
	
	var t = $(id).getStyle('display');

	if(t == '') {
		$(id).setStyle('display', 'none');
	} else {
		$(id).setStyle('display','');
	}
	
}

function movePage(mode) {
	
	switch(mode) {
		case 'prev' :
			$('sliders_pt_lft').setStyle('display','none');
			$('sliders_pt_rgt').setStyle('display','');
			
			$('propertyHighlightDetail').setStyle('left','0');
			break;
		case 'next'	:
			$('sliders_pt_lft').setStyle('display','');
			$('sliders_pt_rgt').setStyle('display','none');
			
			$('propertyHighlightDetail').setStyle('left','-616px');
			break;
	}
	
	
}

function movePage1(mode) {
	
	switch(mode) {
		case 'prev' :
			$('sliders_pt_lft').setStyle('display','none');
			$('sliders_pt_rgt').setStyle('display','');
			
			$('specialistDetail').setStyle('left','0');
			break;
		case 'next'	:
			$('sliders_pt_lft').setStyle('display','');
			$('sliders_pt_rgt').setStyle('display','none');
			
			$('specialistDetail').setStyle('left','-669px');
			break;
	}
	
	
}
