function emailFormPopup(email, ads_id, uid) {
	
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();			
	
	var url = 'index.php?option=com_ihouse&task=emailContactForm&r=' + unixtime_ms;		
	var req = new Ajax(url, {
	   					data		: 	
									{
										'email' : email,
										'ads_id': ads_id,
										'uid'	: uid
									},
						method		: "get",
		    			onSuccess	: function(data) {
								$('popupForm').setStyle('display','');
								$('popupForm').setHTML(data);
								
		   				},
						evalScripts : true
				}).request();	
}

function submitEmailFormPopup(urls, email) {
	
	var how = true;
	
	var namecontact = $('name_contact_popup');
	var emailcontact = $('email_to_contact_popup');
	var mobilecontact = $('mobile_contact_popup');
	var textareamessage = $('textarea-emailformbox');
	
	if(namecontact) {
		var t = namecontact.getProperty('value');
		$('namecontactmsg').setStyle('color','');
		if(t == '') {
			$('namecontactmsg').setStyle('color','red');
			how = false;
		}
	}
	
	if(emailcontact) {
		var t = emailcontact.getProperty('value');
		$('emailtocontactmsg').setStyle('color','');
		if(t == '') {
			$('emailtocontactmsg').setStyle('color','red');
			how = false;
		}
	}
	
	if(mobilecontact) {
		var t = mobilecontact.getProperty('value');
		$('mobilecontactmsg').setStyle('color','');
		if(t == '') {
			$('mobilecontactmsg').setStyle('color','red');
			how = false;
		}
	}
	
	if(textareamessage) {
		var t = textareamessage.getProperty('value');
		$('messageboxmsg').setStyle('color','');
		if(t == '') {
			$('messageboxmsg').setStyle('color','red');
			how = false;
		}
	}
	
	if(how) {
		var foo = new Date; // Generic JS date object
		var unixtime_ms = foo.getTime();
	
		var loader = $('ajax-submit-form-email');
		loader.setStyle('display','');
		$('emailform').setStyle('display','none');
		
		$('emailFormContact').send({
			onComplete	:	function(data) {
				var obj1 		= 	Json.evaluate(data);
			
				loader.setStyle('display','none');
				$('text-message-sbm').setHTML('<img height="1" width="1" style="display:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1022274725/?label=SgQFCOX-k1oQpdm65wM&amp;guid=ON&amp;script=0"/>Thank you for your submission. <a href="javascript:closeEmailFormPopup();">Click to close</a>');
			}
		});
		
	}
}

function closeEmailFormPopup() {
	$('popupForm').setHTML('');
	$('popupForm').setStyle('display','none');
}