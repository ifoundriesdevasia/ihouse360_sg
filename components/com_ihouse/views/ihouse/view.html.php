﻿<?php
/**
* @version		$Id: view.html.php 10711 2008-08-21 10:09:03Z eddieajau $
* @package		Joomla
* @subpackage	Poll
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Poll component
 *
 * @static
 * @package		Joomla
 * @subpackage	Poll
 * @since 1.0
 */
class IhouseViewIhouse extends JView
{
	var $bedroomexist = '';
	
	function display($tpl = null)
	{
		global $mainframe, $option;
		$document =& JFactory::getDocument();
		
		//$document->addScript(JURI::root(true).'/components/com_ihouse/assets/jquery-1.3.2.js');
		//$document->addScript(JURI::root(true).'/components/com_ihouse/assets/jquery-ui-1.7.1.custom.min.js');
		//$document->addScript(JURI::root(true).'/components/com_ihouse/assets/slider.js');
		$document =& JFactory::getDocument();
		$document->addScript( JURI::base(true). '/components/com_ihouse/assets/contacts.js' );
			
		$user		=& 	JFactory::getUser();
		$uid		=	$user->get('id');	
		$gid		=	$user->get('gid');
		
		$layout		= 	JRequest::getCmd('layout', 'default');
		
		$db 	  	=& 	JFactory::getDBO();
		
		$limitstart =	JRequest::getCmd('limitstart', 0);
		$limit		=	JRequest::getCmd('limit', 10);
		
		$sector		=	JRequest::getVar('sector'); /* Sector */

		$list_src	=	JRequest::getCmd('list', ''); /* Latest Real Estate - Homepage */
		
		//$adv_search =	JRequest::getCmd('ads_adv_search', 0);
		
		$global_search_mode = JRequest::getCmd('global_search_mode', 0);
		
		$mapsearch = JRequest::getCmd('mapsearch',0);
		
		$headers 	=	'<strong>物业列表&nbsp;&nbsp;</strong><i>Property Listing</i>'; /* FOR HEADER TITLE */

		$where 		= array();
		$where_or 	= array();
		
		$sentosa 	= '';
		
		$filter				=	JRequest::getVar('filter', '');
		$orderfilter		=	JRequest::getVar('orderfilter', '');
		
		if(!empty($filter) || !empty($orderfilter)) {
			$orderby	= " ORDER BY ".$filter." ".$orderfilter." ";
		} else {
			$orderby = ' ORDER BY a.posting_date DESC ' ;	
		}
		

		/* DEFAULT */
		$orderfilter1	=	'ASC';
		$orderfilter2	=	'ASC';
		$orderfilter3	=	'ASC';
		$orderfilter4	=	'ASC';
		
		$image1			= '';
		$image2			= '';
		$image3 		= '';
		$image4			= '';
		
		if($filter == 'a.no_of_room') {
			
			if($orderfilter == 'ASC') {
        		$orderfilter1 = 'DESC';
				$image1 = '<img src="'.JRoute::_('images/sort_asc.png').'" />';
			}else{
				$orderfilter1 = 'ASC';
				$image1 = '<img src="'.JRoute::_('images/sort_desc.png').'" />';
			}
		}
		if($filter == 'a.property_type') {
			
			if($orderfilter == 'ASC') {
        		$orderfilter2 = 'DESC';
				$image2 = '<img src="'.JRoute::_('images/sort_asc.png').'" />';
			}else{
				$orderfilter2 = 'ASC';
				$image2 = '<img src="'.JRoute::_('images/sort_desc.png').'" />';
			}
		} else if($filter == 'a.ask_price') {
			
			if($orderfilter == 'ASC') {
        		$orderfilter3 = 'DESC';
				$image3 = '<img src="'.JRoute::_('images/sort_asc.png').'" />';
			}else{
				$orderfilter3 = 'ASC';
				$image3 = '<img src="'.JRoute::_('images/sort_desc.png').'" />';
			}
		} else if($filter == 'a.posting_date') {
			
			if($orderfilter == 'ASC')  {
        		$orderfilter4 = 'DESC';
				$image4 = '<img src="'.JRoute::_('images/sort_asc.png').'" />';
			} else { 
				$orderfilter4 = 'ASC';
				$image4 = '<img src="'.JRoute::_('images/sort_desc.png').'" />';
			}
		}
		
		
		/**********************************************/
		/* Latest Real Estate - Homepage Looking Page */
		/**********************************************/
		
		
		if($list_src == 'new') {
			$where[] = ' LOWER(a.property_category) LIKE '.$db->Quote( '%'.$db->getEscaped( '新房', true ).'%', false );
		} else if($list_src == 'second') {
			$where[] = ' LOWER(a.property_category) LIKE '.$db->Quote( '%'.$db->getEscaped( '其它', true ).'%', false );
		} else if($list_src == 'rent') {
			$where[] = ' LOWER(a.ad_type) LIKE '.$db->Quote( '%'.$db->getEscaped( 'rent', true ).'%', false );
		}
		
		
		/******************/
		/* SECTOR SEARCH  */
		/******************/
		
		
		if ($sector) 
		{
			$arr_sector = explode('|',$sector);
			
			foreach($arr_sector as $s) :
				$t = explode(',',$s);
				
				$sct 	= 	$t[0];
				$pl		=	$t[1];
				
				if(!empty($pl)) 
					$sentosa = ' AND property_category = '. $db->Quote( $db->getEscaped( '圣淘沙', true ), false );
			
				$where_or[] = ' UPPER(property_district_id) = '.$db->Quote( $db->getEscaped( $sct, true ), false );
			endforeach;
			
			$where[] = "(".implode( ' OR ', $where_or ).") ". $sentosa;
			
		}
		
		
		/*************************/
		/* ADTYPE - LOOKING PAGE */
		/*************************/
		
		
		$adtype		=	JRequest::getCmd('adtype', '');
		
		
		if($adtype) {
			
			$where[]	=	" t.ads_type_config = '$adtype' ";
			
			
			
			$type		=	JRequest::getCmd('type', ''); /* USUALLY 'RENT' OR 'SELL' */
			
			if($type) { /* ad type config = 4 */
				$where[]	=	" a.ad_type = '$type' ";
			}
		
			if($adtype == 7) { /* New Condos for Sale */
				$where[]	=	" a.ad_type = 'sell'  ";
			}
			
			switch($adtype) {
				case 1:
				case 2:
				case 3:
				case 4:
					$headers = '<strong>精品楼盘</strong>&nbsp;&nbsp;<i>Prime Properties</i>';
				case 5:
				case 6:
					break;
				case 7:
					$headers = '<strong>新公寓出售</strong>&nbsp;&nbsp;<i>New Condo For Sales</i>';
					break;	
			}
			
		}
		
		/*****************************/
		/* GLOBAL SEARCH LOOKUP PAGE */
		/*****************************/
		
		if($global_search_mode) {
			
		}
		
		/*****************************/
		/*    MAP SEARCH RESULT 	 */
		/*****************************/
		if($mapsearch) {
			
		}
		
		$where[]	=	" cb.status = 'A' "; 	/* FIND ACTIVE SUBSCRIPTION */
		$where[]	=	" a.publish = 1 "; 		/* PUBLISH ADS */
		
		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );

		switch($layout) {
			case 'property_detail' 	:
				$id 			= 	JRequest::getCmd('id');
				$postcode 		= 	JRequest::getCmd('postcode');
				
				/* Log IP */
				if($this->hasVisitedByIP('property', $id)) {
					$this->addPopularCounter($id);
				}
				
				$row			= 	$this->getProperty($id);
				
				$document->addScript('https://maps.googleapis.com/maps/api/js?key=AIzaSyDFefam2Po6rvfU_dREQq7fft1w4S1mRhU&libraries=places&region=GB');
				$document->addScriptDeclaration($this->getGoogleMapAPI($row));
				
				
		
				$interestedProp	=	$this->getPropertyInterestedIn();
				$imgChart 		= 	$this->getHistoricalChartImage($row);
				$hasFloorPlan	=	$this->hasFloorPlanImages($postcode);
				
				if($hasFloorPlan)
					$imgFloorPlan 	= 	$this->getFloorPlanImagesHTML($postcode); 
				
				$hasIhouseImages	=	$this->hasIhouseImages($postcode);
				
				//if($hasIHouseImages)
					
				
				$price			=	$this->getPropertyAvgPrice($row);
				$this->assignRef('price', $price);
				
				$guru			=	$this->getPropertyGuru($id);
				
				$this->assignRef('historical_chart_img', $imgChart);
				$this->assignRef('floor_plan_img', $imgFloorPlan);
				$this->assignRef('default_bedroom_type', $this->bedroomexist);
				$this->assignRef('hasFloorPlan', $hasFloorPlan);
				$this->assignRef('hasIhouseImages', $hasIhouseImages);
				$this->assignRef('property_no',	$id);
				$this->assignRef('data', $row);
				$this->assignRef('user_id', $uid);
				$this->assignRef('nearby_mrt', $this->getNearbyStation($row->nearby_mrt));
				$this->assignRef('nearby_school', $this->getNearbySchool($row->nearby_school));
				$this->assignRef('interested_prop', $interestedProp);
				$this->assignRef('fav_id', $this->getFavId($id, $uid));
				$this->assignRef('is_favorited', $this->isFavorited($id, $uid));
				
				$anytitle = '';
				$anytitle .=  $row->name_ch.' '.$row->name_en.' '.$row->postcode;
				
				$document =& JFactory::getDocument();
				$document->setTitle($anytitle ."-".  $document->getTitle());
				$document->setDescription($anytitle."-". $document->getDescription());
				break;
			
			case 'default' 			:
			default 				:
				
				
				
				if($adtype) {
					
		
					$query	=	'SELECT ' 
					    . ' a.*, '
						. ' u.name, u.chinese_name, u.company_name ,u.company_logo ,u.mobile_contact, u.website,u.user_image,u.user_category,u.email,u.cea_reg_no  ' 
						.	' FROM #__ihouse_ads_type AS t ' 
						.	" LEFT JOIN #__ihouse_ads AS a ON a.id = t.ads_id "
						.	" LEFT JOIN #__users AS u ON u.id = a.posted_by "
						.	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = t.subscription_id "
						.	$where
						.	$orderby;
					
					$db->setQuery( $query , $limitstart, $limit);
					$rows		= $db->loadObjectList();
				
					$query	=	'SELECT COUNT(*) FROM #__ihouse_ads_type AS t ' 
						.	" LEFT JOIN #__ihouse_ads AS a ON a.id = t.ads_id "
						.	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = t.subscription_id "
						.	$where
						.	$orderby;
						
						$db->setQuery( $query );
					
					$total			= $db->loadResult();
					
					
				} else if($adv_search) {
					 
					
					$query	=	' SELECT '
						. ' a.*, '
						. ' u.name, u.chinese_name, u.company_name ,u.company_logo ,u.mobile_contact, u.website,u.user_image,u.user_category,u.email,u.cea_reg_no  ' 
							.	' FROM #__ihouse_ads AS a '
							.	' LEFT JOIN #__users AS u ON u.id = a.posted_by '
							.	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = a.subscription_id "
							.	$join_property
							. 	$join_floorplan
							.	$where
							.	' '
							.	$orderby;
					
						$db->setQuery( $query , $limitstart, $limit);
		
					$rows = $db->loadObjectList();
		
					if ($db->getErrorNum())
					{
						echo $db->stderr();
						return false;
					}

					$query	=	'SELECT COUNT(a.id) ' 
							.	' FROM #__ihouse_ads AS a '
							.	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = a.subscription_id "
							.	' '
							.	$where
							.	$orderby;
						$db->setQuery( $query );
						$total = $db->loadResult();


				} else {
				
					
					$query	=	' SELECT ' 
							. 	' a.*,u.name,u.chinese_name,u.company_name,u.company_logo,u.mobile_contact,u.website,u.user_image,u.user_category,u.email,u.cea_reg_no '
							.	' FROM #__ihouse_ads AS a '
							.	' LEFT JOIN #__users AS u ON u.id = a.posted_by '
							.	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = a.subscription_id "
							.	$where
							.	' '
							.	$orderby;

						$db->setQuery( $query , $limitstart, $limit);
						
						$rows = $db->loadObjectList();

					if ($db->getErrorNum())
					{
						echo $db->stderr();
						return false;
					}
					
					/* COUNT TOTAL */
					$query = 'SELECT COUNT(*) '
							. ' FROM #__ihouse_ads AS a '
							. " LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = a.subscription_id "
							. $where;
						
						$db->setQuery( $query );
						
						$total = $db->loadResult();
				}

				jimport('joomla.html.pagination');
				$pagination = new JPagination( $total, $limitstart, $limit );
				
				$pagestotal = ceil($total / $limit);
				$currentpages = ($limitstart / $limit) + 1;
				
				$this->assignRef('pagestotal', 	$pagestotal);
				$this->assignRef('currentpages', $currentpages);
				
				$this->assignRef('items',	$rows);
				$this->assignRef('total', 	$total);
				
				$this->assignRef('user',	JFactory::getUser());
				$this->assignRef('lists',	$lists);
	
				$this->assignRef('pagination',	$pagination);
				
				/* ADTYPE PURPOSE */
				$this->assignRef('adtype', $adtype);
				$this->assignRef('type', $type);/* ad type config = 4 */

				
				$this->assignRef('header_title', $headers);
				
				
				$this->assignRef('sector',$sector);
				$this->assignRef('list_src',$list_src);

				/* Sorting Method */
				$this->assignRef('image1', $image1);
				$this->assignRef('image2', $image2);
				$this->assignRef('image3', $image3);
				$this->assignRef('image4', $image4);
				
				$this->assignRef('orderfilter1', $orderfilter1);
				$this->assignRef('orderfilter2', $orderfilter2);
				$this->assignRef('orderfilter3', $orderfilter3);
				$this->assignRef('orderfilter4', $orderfilter4);
				
				break;
		}
		
		$this->assignRef('db', $db);
		parent::display($tpl);
		
	}
	
	
	function getPropertyHighlight($id = 0) {
		$db 	  		=& 	JFactory::getDBO();
		
		$query			= 	"SELECT a.*, i.img1 FROM #__ihouse_ads_type AS t "
							.	" LEFT JOIN #__ihouse_ads AS a ON a.id = t.ads_id "
							.	" LEFT JOIN #__ihouse_ads_image AS i ON i.ads_id = t.ads_id "
							.	" WHERE t.ads_type_config = 5 "
							.	" AND t.property_id = '$id' "
							.	" LIMIT 8";
			$db->setQuery( $query );
			$lists = $db->loadAssocList();
			
		return $lists;					
	}
	
	function getPropertyGuru($id = 0) {
		$db 	  		=& 	JFactory::getDBO();
		
		$query			= 	"SELECT * FROM #__ihouse_ads_type AS t "
							.	" LEFT JOIN #__ihouse_property AS p ON p.id = t.property_id "
							.	" WHERE t.ads_type_config = 1 "
							.	" AND t.property_id = '$id' "
							.	" LIMIT 1";
		
			$db->setQuery( $query );
			$list = $db->loadObject();
			
		return $list;	
	}
	
	function getPropertyAvgPrice($row) {
		$db 	  		=& 	JFactory::getDBO();
		
		$projectname	=	preg_replace('/(\s|\.|\')+/','-',strtolower($row->name_en));
		
		$query			= 	"SELECT price FROM #__ihouse_property_price "
							.	" WHERE project_name = '$projectname' ";
		
		
			$db->setQuery( $query );
			$list = $db->loadResult();
			
		return $list;	
	}
	
	function getNearbyStation($station) {
		$html = '';
		
		$tmp	=	explode('|', $station);
		
		foreach($tmp as $t) :
			$html .= '<div id="static-text" style="float:left;font-size:12px;">'.$t.'</div>';
			$html .= '<div style="clear:both;"></div>';
		endforeach;
		
		return $html;
	}
	
	function getNearbySchool($school){
		$html = '';
		
		$tmp	=	explode('|', $school);
		
		foreach($tmp as $t) :
			$html .= '<div id="static-text" style="float:left;font-size:12px;">'.$t.'</div>';
			$html .= '<div style="clear:both;"></div>';
		endforeach;
		
		return $html;
	}
	/* Based on Price and RoomType */
	
	function getPropertyInterestedIn() {
		$db 	  		=& 	JFactory::getDBO();
		
		$query			= 	"SELECT p.*,pp.price AS price_psm  FROM #__ihouse_property AS p "
							.	" LEFT JOIN #__ihouse_property_price AS pp ON pp.postcode = p.postcode "
							.	" LIMIT 6";
		
			$db->setQuery( $query );
			$lists = $db->loadObjectList();
		
		return $lists;
			
	}
	function hasFloorPlanImages($postcode = '') {
		
		$hasOne = false;
		
		if(empty($postcode))
			return false;
		
		$imgpath = JRoute::_('images/singapore/'.$postcode.'/floorplan/');
		
		if(file_exists($imgpath)) {
			if ($handle = opendir($imgpath)) {
				while (false !== ($file = readdir($handle))) {
					if($file == '.' || $file == '..' || $file == 's')
						continue;
						
					$hasOne = true;
				}
			}
		}
		
		return $hasOne;
	}
	
	function getFloorPlanImagesHTML($postcode = '') {
		
		$html_tabs = '';
		$html = ''; 
		
		if(empty($postcode))
			return false;
		
		$db 	  		=& 	JFactory::getDBO();
		
		$query	= "SELECT * FROM #__ihouse_floorplan ";
			$db->setQuery( $query );
			$floorplan = $db->loadObjectList();

		$query	= "SELECT floor_plan FROM #__ihouse_property WHERE postcode = ".$postcode;
			$db->setQuery( $query );
			$result = $db->loadResult();
	
		$room_type_ch = explode('|', $result);
	
		$pic = array();
	
		//$default = '1brm'; // display : '';
		
		/* MUST CHANGE -- to accepting TABS */
		
 
		$html_tabs .= '<ul class="tab_nav4">';

		$allowed_types = array('png','jpg','jpeg','gif'); // list of filetypes you want to show
		
		$patterns = array('/\.png/','/\.jpg/','/\.jpeg/','/\.gif/');
		$replacements = array('','','','');

		$default = 0;
		
		
		foreach($floorplan as $fp) :
			$imgpath = JRoute::_('images/singapore/'.$postcode.'/floorplan/'.$fp->type_en.'/');

			if(file_exists($imgpath)) {
				$style_mode = '';
				
				if($default == 0) {
					$style_mode = 'class="tabactive4"';
				}
				$html_tabs .= '<li id="bed_'.$fp->type_en.'" style="margin-left:5px;cursor:pointer;" '.$style_mode.' ><a href="javascript:displayFloorPlanTypeTabs(\''.$fp->type_en.'\')">'.$fp->type_ch.'</a></li>';
				
				$displ_mode = 'style="display:none;" ';

				if($default == 0) {
					$default = 1;
					$displ_mode = 'style=""';
					$this->bedroomexist = $fp->type_en;
				}
				
				//$html .= '<div class="displFloorPlan" id="'.$fp->type_en.'_'.$postcode.'" '.$displ_mode.' >';
				/*
				if ($handle = opendir($imgpath)) {
					while (false !== ($file = readdir($handle))) {
						if($file == '.' || $file == '..' || $file == 's')
							continue;
				
						if(in_array(strtolower(substr($file,-3)),$allowed_types))
						{
							$imgurlfile 	= $imgpath.''.$file;
							$imgurlfile_s 	= $imgpath.'s/'.$file;
						
							$html .= '<div style="float:left;padding:5px;width:95px;white-space:nowrap;">';
							$html .= '<a style="color:#062284" href="'.$imgurlfile.'" rel="rokbox" title="'.$fp->type_en.' '.$postcode.' :: '.$file.'" style="cursor:pointer;">';
							$html .= '<center><img class="fp_img" src="'.$imgurlfile_s.'" width="80px" height="60px;" /><br />';
							$txtfile = preg_replace($patterns, $replacements, $file);
							if(strlen($txtfile) > 12) {
								$txtfile = substr_replace($txtfile,'..',12);
							}
							$html .= $txtfile.'</center>';
							$html .= '</a>';
							$html .= '</div>';
							
							$o++;
						}
						
						if($o >= 70) break;
						
					}
				}
				*/
				
				//$html .= '<div style="clear:both"></div>';
				//$html .= '</div>';
				
			}
		endforeach;

		$html_tabs .= '</ul>';
		
		$html_tabs .= '<div style="clear:both">&nbsp;</div>'.$html;
		
		return $html_tabs;
		
	}
	
	function hasIhouseImages($postcode = '') {
		$hasOne = false;
		
		if(empty($postcode))
			return false;
		
		$imgpath = JRoute::_('images/singapore/'.$postcode.'/images/');
		
		$allowed_types = array('png','jpg','jpeg','gif'); // list of filetypes you want to show
		if(file_exists($imgpath)) {
			if ($handle = opendir($imgpath)) {
				while (false !== ($file = readdir($handle))) {
					if($file == '.' || $file == '..' || $file == 's')
						continue;
				
					if(in_array(strtolower(substr($file,-3)),$allowed_types))
					{
						$hasOne = true;
						break;
					}
				}
			}
		}
		return $hasOne;
	}
	
	function getHistoricalChartImage($row) {
		
		
		$graph_path = JRoute::_('images/ihouse/history_graph/'.$row->postcode.'.jpg');
		$bar_path	= JRoute::_('images/ihouse/history_graph/'.$row->postcode.'_bar.jpg');
		
		$result 	=  	'<img src="'.$graph_path.'" alt="history_graph_'.$row->postcode.'" />';
		$result_bar	=	'<img src="'.$bar_path.'" alt="history_bar_graph_'.$row->postcode.'" />';
		
		/*if(file_exists($graph_path)) {
			return $result;
		}*/
		
		$projectname	=	preg_replace('/(\s|\.|\')+/','-',strtolower($row->name_en));
		
		$db			=& JFactory::getDBO();
		
		$query 		= "SELECT * FROM #__ihouse_history "
						. " WHERE project_name = '$projectname' ";
			
			$db->setQuery( $query );
			
			$lists = $db->loadAssocList(); /* month_year, postcode, avg_psm */

		$data = array();
		$bar_point = array(); /* VOLUME BASED ON PROJECT NAME */
		foreach($lists as $s) :
			$data[strtolower($s['month_year'])] = $s['avg_psm'];
			$bar_point[strtolower($s['month_year'])] = $s['total'];
			
		endforeach;
		
		if(!empty($data)) {
		
			$max_value = max($data);
			$min_value = min($data);
		
		} else {
			
			$max_value = 0;
			$min_value = 0;
		}
		
		if(!empty($bar_point)) {
			$max_tmp_value = max($bar_point);
			$min_tmp_value = min($bar_point);
		} else {
			
			$max_tmp_value = 0;
			$min_tmp_value = 0;
		}

		
		$month_x = array();
		$year_x = array();
		$month_year_x = array();
		
		if(!empty($lists)) {
			
			$x_axis = array();
			$j = 0;
			for($i = 4; $i >= -16; $i--) : /* 21 cols */
				$month_x[] 		= 	date("M",strtotime( strval($i) . " month"));
				$year_x[]		= 	date("Y",strtotime( strval($i) . " month"));
				$month_year_x[]	=	strtolower(date("MY",strtotime( strval($i) . " month")));
				$x_axis[] 		= 	$j*5;
				$j++;
			endfor;
			
			krsort($month_x);
			krsort($year_x);
			krsort($month_year_x);
			
			$psm_y = array();
			$vol_y = array();
			
			foreach($month_year_x as $l) : // e.g. Aug2009
				
				if(!empty($data[$l])) {
					
					$psm_y[] 	= 	$data[$l];
					
				} else {
					
					$psm_y[] = 0;
					
				}
				
				if(!empty($bar_point[$l])) {
					
					$vol_y[] 	= 	$bar_point[$l];
					
				} else {
					
					$vol_y[] = 0;
					
				}
				
			endforeach;
			
			if($max_value <= 10000) {
				$max = 10000;
				$min = 0;
				
				$test = array();
				for($i = 1; $i <= 10; $i++) : /* based on 6000 */
					$test[] = strval($i)."K";
				endfor;
			}
			else 
			{
				$max = 60000;
				$min = 0;
				
				$test = array();
				for($i = 1; $i <= 10; $i++) : /* based on 6000 */
					$test[] = strval(6*$i)."K";
				endfor;
			}
			
			
			/* */
			$y_pt 	= array();
			$xx		= array();
			
			$partly_points = array();
			$o = 0;
			
			/* CALCULATE AVG according Google Chart */
			foreach($psm_y as $y) :
				$value	= (int) ($y - $min) / ($max - $min) * 100 ;
				
				if($value != 0) {
					$y_pt[] =  $value;
					$xx[] = $x_axis[$o];
				} else { // if finds 0, skip it, get a new line
					$x1		=	implode(',', 	$xx);
					$y1		=	implode(',', $y_pt);
					
					if(!empty($x1) && !empty($y1)) {
						if(count($xx) == 1) { // if only 1 point, set the nearest, so the bullet point will appear
							$x1	=	$x1.','.($x1+0.01);
							$y1 =	$y1.','.$y1;
						}
						
						$partly_points[]	=	$x1.'|'.$y1;
					}
					
					$y_pt = array(); // reset to get a new array
					$xx = array();
				}
				
				$o++;
					
			endforeach;
			
			/* VOLUME */
			$min_tmp = 0;
			$max_tmp = $max_tmp_value;
			
			$vol_y_pt 	= array();
			$vol_x_pt	= array();
			if($max_tmp <= 10) {
				$max_tmp 	= 10;
				$gg			= '0|2|4|6|8|10';
			}
			else if($max_tmp <= 50) {
				$max_tmp 	= 50;
				$gg			= '0|10|20|30|40|50';
			}
			else if($max_tmp <= 100) {
				$max_tmp 	= 100;
				$gg			= '0|20|40|60|80|100';
			}
			else if($max_tmp <=150) {
				$max_tmp	= 150;
				$gg			= '0|30|60|90|120|150';
			}
			else if($max_tmp <= 200) {
				$max_tmp	= 200;
				$gg			= '0|40|80|120|160|200';
			}
			
			$vol_partly_points = array();
			$o = 0;
			foreach($vol_y as $y) :
				$value	= (float) ($y - $min_tmp) / ($max_tmp - $min_tmp) * 100 ;
				
					$vol_y_pt[] =  $value;
					$vol_x_pt[] = $x_axis[$o];

					$vol_partly_points[]	=	$value;
				
				$o++;	
			endforeach;
			
			
			
			$bullets		=	array();
			for($i = 0; $i < count($partly_points); $i++) {
				$bullets[] 	= 	"o,0066FF,".$i.",,6";
				//echo print_r($partly_points[$i]);
				//echo count(implode(',',$partly_points[$i]));
			}
			
			$real_points 	= 	implode('|',	$partly_points);
			
			$bar_points		=	implode(',', 	$vol_partly_points);
			
			$month_x 		= 	implode('|', 	$month_x);
			$year_x			=	implode('|',	$year_x);
			

			$value_y = '0|'.implode('|', $test); 

			
			$url = "http://chart.apis.google.com/chart?cht=lxy&chd=t:".$real_points."&chs=640x300";
			$url .= "&chxt=x,y,x,y&chxl=0:|".$month_x."|1:|".$value_y."|2:|".$year_x."|3:|(SGD)&chxp=3,100&chxs=0,ff0000,12,0,lt|1,0000ff,10,1,lt";
			$url .= "&chg=5,10,4,1,0,0&chm=".implode('|', $bullets);
			//echo $url;

			$ch = curl_init();
			curl_setopt( $ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1" );
    		curl_setopt( $ch, CURLOPT_URL, $url );
			curl_setopt( $ch, CURLOPT_HEADER, 0);
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1); 
			
			$content = curl_exec( $ch );
			
			$file = fopen($graph_path, 'w+');

        	fwrite($file, $content);

        	fclose($file); 
		
		
			//echo print_r($bar_points);
			
			$url = "http://chart.apis.google.com/chart?cht=bvs&chd=t:".$bar_points."&chs=640x200";
			$url .= "&chxt=x,y,x,y&chco=4D89F9,C6D9FD&chxl=0:|".$month_x."|1:|".$gg."|2:|".$year_x."|3:|Volume&chxp=3,100";
			$url .= "&chxs=0,ff0000,12,0,lt|1,0000ff,10,1,lt";
			//$url .= "&chg=5,10,4,1,0,0";
			
			$ch = curl_init();
			curl_setopt( $ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1" );
    		curl_setopt( $ch, CURLOPT_URL, $url );
			curl_setopt( $ch, CURLOPT_HEADER, 0);
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1); 
			
			$content = curl_exec( $ch );
			
			$file = fopen($bar_path, 'w+');

        	fwrite($file, $content);

        	fclose($file); 
			
			
			$res	=	new StdClass;
			$res->history_line 	= $result;
			$res->history_bar	= $result_bar;
			return $res;
		}
		
		return false;
			
	}
	
	function getPropertyPhoto($ids) {
		/*$loc_path = 'images/ihouse/postcode/'.$postcode.'/images';
		
		if(is_dir(JRoute::_($loc_path)))
			return "yes";
		
		
		$html = '<img src="'. JRoute::_('images/ihouse/postcode/'.$postcode.'/images/').'" />';
		*/
	}
	
	function getProperty($id = 0) {
		
		if(!$id)
			return false;
		
		$db 	  =& JFactory::getDBO();
		
		$query	=	"SELECT * FROM #__ihouse_property "
					. " WHERE id = '$id' ";
		
			$db->setQuery( $query );
		
		return $db->loadObject();
	}

	function hasVisitedByIP($type, $id) {
		
		$ip = getenv('REMOTE_ADDR');
		
		$db 	  =& JFactory::getDBO();
		
		$params	=	"property_id=".$id;
		
		$query	=	"SELECT id FROM #__ihouse_ip_log "
					. " WHERE ip = '$ip'  "
					. " AND type = '$type' "
					. " AND param LIKE '%".$params."%' ";
		
			$db->setQuery( $query ) ;
			$is_exist = $db->loadResult();		
		
		if($is_exist) 
			return false;
		
		
		
		if($type == 'property') {
			
			$query	=	"INSERT INTO #__ihouse_ip_log VALUES(NULL, '$type','$ip','$params')";
			
				$db->setQuery( $query ) ;
				$db->query();
				
			return true;	
		}
			
	}
	
	function addPopularCounter($id = 0) { // property_id
		if(!$id)
			return false;
		
		$db 	  =& JFactory::getDBO();
		
		$query 	=	" SELECT * FROM #__ihouse_popular WHERE type = 'total' AND property_id = '$id' ";
			$db->setQuery( $query );
			$exists = $db->loadObject();
		
		if(empty($exists)) {
			
			$query	=	"INSERT INTO #__ihouse_popular VALUES(NULL, 'total',1,'$id',0)";
			
				$db->setQuery( $query ) ;
				$db->query();
				
		} else {
			
			$query	=	" UPDATE #__ihouse_popular SET counter = counter + 1 "
					.	" WHERE property_id = '$id' AND type = 'total' " ;
					
				$db->setQuery( $query );
				$db->query();
		}
		
		
		/* FOR COUNTER (PROPERTY DETAIL) - START */
		$query	=	"SELECT id FROM #__ihouse_popular "
					.	" WHERE property_id = '$id' AND type = 'property' LIMIT 1" ;
			
			$db->setQuery( $query ) ;
			$is_exist	=	$db->loadResult();
		
		
		
		if($is_exist) {
			
			$query	=	"UPDATE #__ihouse_popular SET counter = counter + 1"
					.	" WHERE property_id = '$id' AND type = 'property' " ;
			
				$db->setQuery( $query ) ;
				$db->query();
			
		} else {
			
			$query	=	"INSERT INTO #__ihouse_popular VALUES(NULL, 'property',1,'$id',0)";
			
				$db->setQuery( $query ) ;
				$db->query();
				
		}
	}

	function getGoogleMapAPI($row){
		$db		=& 	JFactory::getDBO();
		
		/*$query = " SELECT lat, lng FROM #__ihouse_location_gps WHERE postcode = '$postcode' ";
			$db->setQuery( $query );
			$row = $db->loadObject();
		
		$lat = $row->lat;
		$lng = $row->lng;
		*/
		
		$postcode	=	$row->postcode;
		$address	=	str_replace("'","\'",$row->address);
		$country	=	'Singapore';
		
  		$javascript = "
				var map;
				var geocoder;
		
				function initialize(){
					geocoder = new google.maps.Geocoder();
  					var latlng = new google.maps.LatLng(1.3667,103.8);
  					var options = {
   						zoom: 15,
    					center: latlng,
    					mapTypeId: google.maps.MapTypeId.ROADMAP
  					};
       
  					map = new google.maps.Map(document.getElementById('canvas_map'), options);
					
					
				}
				
				function geocodeGoogleMap() {
					var icon_img = '".JRoute::_('templates/mapsearch/images/building.png')."';
					
					geocoder.geocode( { 'address': '".$country." ".$postcode." ".$address."'}, function(results, status) {
      					if (status == google.maps.GeocoderStatus.OK) {
		  
        					map.setCenter(results[0].geometry.location);
        					var marker = new google.maps.Marker({
            							map: map, 
            							position: results[0].geometry.location,
										icon : icon_img
        					});
      					} 
    				});
					
				}
				
				window.addEvent('load', function(){ 
					initialize();
					setTimeout('geocodeGoogleMap()',1500);
				});
";
	    return $javascript;
	}
	function isFavorited($property_id, $uid) {
		if(!$uid)
			return 0;
		
		$db 	  =& JFactory::getDBO();
		
		$query	=	" SELECT * FROM #__ihouse_favorites "
					.	" WHERE aid = '$property_id' AND user_id = '$uid' AND type = 'property' LIMIT 1" ;
			
			$db->setQuery( $query ) ;
			$row	=	$db->loadObject();
		
		if(!empty($row)) {
			return 2;
		} else {
			return 1;
		}
	}
	
	function getFavId($property_id, $uid) {
		if(!$uid)
			return false;
		
		$db 	  =& JFactory::getDBO();
		
		$query	=	"SELECT * FROM #__ihouse_favorites "
					.	" WHERE aid = '$property_id' AND user_id = '$uid' AND type = 'property' LIMIT 1" ;
			
			$db->setQuery( $query ) ;
			$row	=	$db->loadObject();
		
		return $row->id;
	}
}
?>
