<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('behavior.tooltip'); ?>
<?php JHTML::_('script', 'contacts.js', 'components/com_ihouse/assets/'); ?>  
<?php
	$rows =& $this->items;
?>
<div style="background-color:#fff;width:100%;float:left;">
<div id="search_detail_title">
	<?php echo $this->header_title ?>
</div>
	

<div style="border-bottom:solid 1px #ccc; padding:5px 0 5px 0; float:left; width:100%;">
	<div style="float:left; color:#3a3a3a; width:100px; line-height:20px;">
	<!--<input type="checkbox" />&nbsp;&nbsp;&nbsp;全选 / 不选-->&nbsp;
	</div>
                        
	<div style="float:left; width:300px;">&nbsp;
	<!--<input type="submit" value="比较" class="searchbutton" />&nbsp;-->
	</div>
                        
	<div style="float:left;">
		排序&nbsp;&nbsp;
		<img src="<?php echo JRoute::_('templates/main/images/arrow.png'); ?>" />&nbsp;&nbsp;
        
        <a style="color:#062284;text-decoration:none;" href="<?php echo JURI::base() ?>index.php?option=com_ihouse&sector=<?php echo $this->sector ?>&adtype=<?php echo $this->adtype ?>&type=<?php echo $this->type ?>&list=<?php echo $this->list_src ?>&filter=a.no_of_room&orderfilter=<?php echo $this->orderfilter1 ?>">户型</a><?php echo $this->image1 ?>
        &nbsp;&nbsp; | &nbsp;&nbsp;
        <a style="color:#062284;text-decoration:none;" href="<?php echo JURI::base() ?>index.php?option=com_ihouse&sector=<?php echo $this->sector ?>&adtype=<?php echo $this->adtype ?>&type=<?php echo $this->type ?>&list=<?php echo $this->list_src ?>&filter=a.property_type&orderfilter=<?php echo $this->orderfilter2 ?>">类型</a><?php echo $this->image2 ?>
        &nbsp;&nbsp; | &nbsp;&nbsp;
        
        <a style="color:#062284;text-decoration:none;" href="<?php echo JURI::base() ?>index.php?option=com_ihouse&sector=<?php echo $this->sector ?>&adtype=<?php echo $this->adtype ?>&type=<?php echo $this->type ?>&list=<?php echo $this->list_src ?>&filter=a.ask_price&orderfilter=<?php echo $this->orderfilter3 ?>">价格</a><?php echo $this->image3 ?>
        &nbsp;&nbsp; | &nbsp;&nbsp;
        <a style="color:#062284;text-decoration:none;" href="<?php echo JURI::base() ?>index.php?option=com_ihouse&sector=<?php echo $this->sector ?>&adtype=<?php echo $this->adtype ?>&type=<?php echo $this->type ?>&list=<?php echo $this->list_src ?>&filter=a.posting_date&orderfilter=<?php echo $this->orderfilter4 ?>">更新日期</a><?php echo $this->image4 ?>
    </div>
                        
</div>

<?php if(empty($rows)) : ?>
<div style="border-bottom:1px dotted #ccc;padding:10px 0 10px 0;">
	No Ads
</div>
<?php else : ?>
<?php foreach($rows as $row) : ?>
<div style="border-bottom:1px dotted #ccc;padding:10px 0 10px 0;">
<table style="color:#4D4E50">
<tr style="vertical-align:top;">
	<td width="1%"><!--<input type="checkbox"  />--></td>
	<td width="20%">
    <div>
   	<?php 
		$db = $this->db;
		$query = " SELECT ads_id, sess_id, name FROM #__ihouse_ads_image WHERE ads_id = '".$row->id."' AND is_primary = 1 ";
			$db->setQuery( $query );
			$tmps = $db->loadObject();
		
		if(empty($tmps)) {
			$query = " SELECT image FROM #__ihouse_ads_image2 WHERE ads_id = '".$row->id."' ORDER BY RAND() LIMIT 1 ";
			$db->setQuery( $query );
			$ihouse_img = $db->loadResult();
			
			if(empty($ihouse_img)) {
				$ihouse_img = JRoute::_('templates/main/images/thumb-noimage.jpg');
			}else{
				$ihouse_img = 'image.php?size=156&type=3&path='.$row->postcode.'/images/'.$ihouse_img;
			}
		}
	?>
	<div class="adsimgbg">
    <a href="<?php echo JRoute::_('index.php?option=com_ads&view=adsdetails&id='.$row->id); ?>">
    
    <?php if(!empty($tmps->name)) : ?>
    
    <img src="<?php echo JRoute::_('images/ihouse/ads_images/'.$tmps->sess_id.'/med-'.$tmps->name) ?>" width="156px" height="117px" />
    
    <?php elseif(!empty($ihouse_img)) : ?>
    
    <img style="border:none;" src="<?php echo $ihouse_img ?>" />
	<?php else : ?>
    
    <img width="156px" height="129px" src="<?php echo JRoute::_('templates/main/images/thumb-noimage.jpg') ?>" />
    
    <?php endif; ?>
    </a>
	</div>
    </div>
   
    </td>
	<td width="45%">
    <div id="tabtext-padding1" style="padding:0 10px;font-size:15px;">
    <a href="<?php echo JRoute::_('index.php?option=com_ads&view=adsdetails&id='.$row->id); ?>">
				<?php echo ($row->ad_type == 'sell')?'For Sale - ':'For Rent - ' ?><?php echo $row->ad_title ?>
            </a>
    </div>
    <div style="padding:0 10px;">
							<?php 
							if($row->property_type == '公寓'){
							?>
							<div class="adslistingdetails">
										<div class="l">小区名称</div>
										<div class="m">:</div>
										<div class="r"><a style="color:#062284;text-decoration:underline;" href="<?php echo JRoute::_('index.php?option=com_ihouse&view=ihouse&layout=property_detail&id='.$row->property_id.'&postcode='.$row->postcode); ?>" style="text-decoration:none;"><?php echo ($row->property_name) ?></a></div>
										<div style="clear:both"></div>
							</div>
                            <?php
							} else {
							?>
                            <?php 
							}
							?>
    	<div class="adslistingdetails">
    		<div class="l">地址</div>
        	<div class="m">:</div>
        	<div class="r"><?php echo ($row->street_name)?wordwrap($row->street_name, 25, "\n", true):'N/A'; ?></div>
            <div style="clear:both"></div>
        </div>
        <div class="adslistingdetails">
    		<div class="l">价格</div>
        	<div class="m">:</div>
        	<div class="r"><?php echo ($row->ask_price)?'S$ '.number_format($row->ask_price):'N/A'; ?></div>
            <div style="clear:both"></div>
        </div>
        <div class="adslistingdetails">
    		<div class="l">单价</div>
        	<div class="m">:</div>
        	<div class="r"><?php echo ($row->psm_price)?'S$ '.number_format($row->psm_price / 10.764):'N/A'; ?></div>
            <div style="clear:both"></div>
        </div>
        <div class="adslistingdetails">
    		<div class="l">面积</div>
        	<div class="m">:</div>
        	<div class="r"><?php echo ($row->size)?number_format($row->size).' 平方尺':'N/A'; ?></div>
           	<div style="clear:both"></div>
        </div>
        <div class="adslistingdetails">
    		<div class="l">户型</div>
        	<div class="m">:</div>
        	<div class="r"><?php echo $row->no_of_room ?>房&nbsp;<?php echo $row->no_of_hall ?>厅</div>
            <div style="clear:both"></div>
        </div>		
        <?php if($row->property_type == '公寓') : ?>
        <div class="adslistingdetails">
    		<div class="l">邮区</div>
        	<div class="m">:</div>
        	<div class="r"><?php echo ($row->property_district_id)?$row->property_district_id:'-'; ?></div>
            <div style="clear:both"></div>
        </div>
        <?php endif; ?>            
        <?php if($row->property_type == '组屋') : ?>
        <div class="adslistingdetails">
    		<div class="l">地区</div>
        	<div class="m">:</div>
        	<div class="r"><?php echo ($row->hdb_town_id)?$row->hdb_town_id:'-'; ?></div>
            <div style="clear:both"></div>
        </div>
        <?php endif; ?>
        <div class="adslistingdetails">
    		<div class="l">更新日期</div>
        	<div class="m">:</div>
        	<div class="r"><?php echo date("Y年m月d日", strtotime($row->posting_date)) ?></div>
            <div style="clear:both"></div>
        </div>	
        <div class="adslistingdetails">
    		<div class="l">目前状态</div>
        	<div class="m">:</div>
        	<div class="r"><?php echo $row->status_id ?></div>
            <div style="clear:both"></div>
        </div>		
    </div>    
    </td>
   
    <td>
    	<?php if($row->posted_by > 0) : ?>
    	<table width="100%">
        <tr style="vertical-align:top;">
        	<td width="50%">
            <div style="width:90px;text-align:center;">
           	<?php 
			
			if(empty($row->user_image)) {
				$user_image = JRoute::_('templates/main/images/no_avatar.jpg');
			}else{
				$user_image = 'image.php?size=90&type=1&path='.$row->user_image;
			}
			
			?>
            <?php if($row->user_category == 'Agent'): ?>
            	<a style="text-decoration:none;color:#fff;" href="<?php echo JRoute::_('index.php?option=com_user&view=agent_profile&id='.$row->posted_by.'&Itemid=136'); ?>">
            <?php endif; ?>    
            		<img style="border:none;" src="<?php echo $user_image; ?>" />
            
            <?php if($row->user_category == 'Agent'): ?>        
                </a>
            <?php endif; ?>    
          
            <div style="margin-top:4px;text-align:center;">
		<input type="button" onclick="javascript:emailFormPopup('<?php echo $row->email ?>','<?php echo $row->id ?>','<?php echo $row->posted_by ?>');" id="agent_sub" name="agent_sub" class="contactbutton" value="联系我">
		</div>   
        		</div>
    		</td>
            <td style="vertical-align:middle;">
            	<div style="width:120px;text-align:center;">
            <?php 
			
			if(empty($row->company_logo)) {
				$comp_logo = JRoute::_('templates/main/images/thumb-nologo.jpg');
			}else{
				$comp_logo = 'image.php?size=120&type=1&path='.$row->company_logo;
			}
			
			?>
            
            <?php if($row->user_category == 'Agent'): ?>
            	<img src="<?php echo $comp_logo ?>" />
            <?php endif; ?> 
           	</div>
            </td>
        </tr>
        <tr>    
            <td width="100%" colspan="2">
            <div class="adslistingprofile">
    			<div class="l">姓名</div>
        		<div class="m">:</div>
        		<div class="r">
					<?php echo ($row->chinese_name)?$row->chinese_name:'N/A' ?>
            	</div>
            <div style="clear:both"></div>
            </div>
            <div class="adslistingprofile">
    			<div class="l">Name</div>
        		<div class="m">:</div>
        		<div class="r"><?php echo ($row->name)?$row->name:'N/A' ?>
                </div>
            <div style="clear:both">
            </div>	
            
            <div class="adslistingprofile">
    			<div class="l">电话</div>
        		<div class="m">:</div>
        		<div class="r"><?php echo ($row->mobile_contact)?$row->mobile_contact:'N/A' ?></div>
            <div style="clear:both"></div>
        	</div>
            <?php if($row->user_category == 'Agent'): ?>
  			<div class="adslistingprofile">
    			<div class="l">经纪证号</div>
        		<div class="m">:</div>
        		<div class="r">
                <?php if(!empty($row->cea_reg_no)) : ?>
                	<?php if(preg_match('/^L/', $row->cea_reg_no )): ?>
                		<a href="http://www.cea.gov.sg/cea/app/newimplpublicregister/view.jspa?licNo=<?php echo $row->cea_reg_no ?>">
                    <?php elseif(preg_match('/^R/', $row->cea_reg_no )) : ?>    
                    	<a href='http://www.cea.gov.sg/cea/app/newimplpublicregister/view.jspa?licNo=""&regNo=<?php echo $row->cea_reg_no ?>'>
                    <?php endif; ?>   
						<?php echo $row->cea_reg_no ?>
                    </a>
                <?php else : ?>
                	N/A    
                <?php endif; ?>    
                </div>
            <div style="clear:both"></div>
        	</div>
            <?php endif; ?>	
            </td>
        </tr>
        </table>   
        
		<?php else : ?>
        
        <table width="100%">
        <tr style="vertical-align:top;">
        	<td width="50%">
    			<img src="<?php echo JRoute::_('templates/main/images/no_avatar.jpg') ?>" />
    		</td>
        </tr>
        <tr>
        	<td style="">
            </td>
        </tr>
        </table>
		<?php endif; ?>
    </td>
</tr>
</table>
</div>
<?php endforeach; ?>
<?php endif; ?>
<div style="clear:both"></div>

<div class="pagination_box" style="float:right;">
<?php echo $this->currentpages ?> of <?php echo $this->pagestotal ?> pages
</div>
<div class="pagination_box" style="float:right;">
<?php echo $this->pagination->getPagesLinks() ?>
</div>


</div>


<!-- POPUP -->
<div id="popupForm" style="display:none;">
</div>
