<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('behavior.tooltip');?>

<?php 
	$postcode 	= JRequest::getVar('postcode');
	$id			= JRequest::getVar('id');
?>

<script type="text/javascript">
	/* Do not remove this */
	var postcode = '<?php echo $postcode ?>';
	
</script>

<?php JHTML::_('script', 'ihouse.js', 'components/com_ihouse/assets/'); ?>
<?php JHTML::_('script', 'review.js', 'components/com_ihouse/assets/'); ?>
<?php JHTML::_('script', 'contact.js', 'components/com_ihouse/assets/'); ?>
<?php JHTML::_('script', 'favorite.js', 'components/com_ihouse/assets/') ?>

<?php JHTML::_('script', 'rokbox.js', 'components/com_ihouse/assets/rokbox/'); ?>
<?php JHTML::_('script', 'rokbox-config.js', 'components/com_ihouse/assets/rokbox/themes/light/'); ?>
<?php //JHTML::_('script', 'rokbox-style.css', 'components/com_ihouse/assets/rokbox/themes/light/'); ?>
<?php JHTML::stylesheet('rokbox-style.css','components/com_ihouse/assets/rokbox/themes/light/', array('media'=>'all')); ?>

<script type="text/javascript">
setTimeout('displayFloorPlanTypeTabs("<?php echo $this->default_bedroom_type ?>");',500);
</script>
<div style="margin-top:-10px;">
<!--站内广告开始-->
<div style="width:686px; height:110px; border:1 #ccc; background-color:#FFF; padding-top:5px; padding-left:5px; margin-bottom:5px;"> <a href="http://singapore.ihouse360.com/?option=com_content&amp;view=article&amp;id=182:-vip-&amp;catid=38:market-news"><img src="/images/ihouse360ad.jpg" alt="新加坡置业投资移民服务" title="新加坡置业投资移民服务" border="0" /></a>
</div>
<!--站内广告结束-->
<div id="top_menu_list_property_detail">
<ul id="" class="tab_nav6">     
	<li id="tab_" class="tabactive6">
    	<a href="<?php echo JRoute::_('index.php?option=com_ihouse&layout=property_detail&id='.$id.'&postcode='.$postcode) ?>">
    <span class="chinese_text_menu">公寓详细</span></a></li>
	<li id="tab_"><a href="<?php echo JRoute::_('index.php?option=com_ads&view=adslisting&id='.$id.'&postcode='.$postcode) ?>"><span class="chinese_text_menu">出售</span></a></li>
	<li id="tab_"><a href="<?php echo JRoute::_('index.php?option=com_ads&view=rentadslisting&id='.$id.'&postcode='.$postcode) ?>"><span class="chinese_text_menu">出租</span></a></li>
</ul>
</div>


<div style="background-color:#fff;width:100%;">
	<div id="property_detail_title">
		<strong>新加坡公寓：<?php echo $this->data->name_ch.'&nbsp;&nbsp;'?></strong>
		<i><?php echo $this->data->name_en ?></i>
		<!--<script type="text/javascript" src="/templates/main/js/share.js"></script>-->
	</div>

	<div id="prop_detail_wrapper">  

<style type="css/text">
ul.tab_nav3 {
list-style-image:none;
list-style-type:none;
}
</style>

<div id="menu_supplement_propdetail">
	<div style="float: left; height: 22px; width: 260px;">
		<!-- AddThis Button BEGIN -->
	<div class="addthis_toolbox addthis_default_style ">
	<a href="http://www.addthis.com/bookmark.php?v=250&amp;pubid=xa-4ee1964c49a95c33" class="addthis_button_compact">Share To</a>
	<span class="addthis_separator">|</span>
	<a class="addthis_button_preferred_1"></a>
	<a class="addthis_button_preferred_2"></a>
	<a class="addthis_button_preferred_3"></a>
	<a class="addthis_button_preferred_4"></a>
	<a class="addthis_button_preferred_5"></a>
	<a class="addthis_button_preferred_6"></a>
	<a class="addthis_button_preferred_7"></a>
	<a class="addthis_button_preferred_8"></a>
	</div>
	<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4ee1964c49a95c33"></script>
	<!-- AddThis Button END -->
	</div>
	<!--<div style="float: left; height: 22px; width: 90px;"><img src="<?php echo JRoute::_('templates/main/images/share.png'); ?>" class="icon-img" />&nbsp;<a class="no_decoration" href="javascript:void(0);" onclick="javascript:alert('coming soon');">分享</a></div>-->
    <!--<div style="float: left; height: 22px; width: 80px;">
    	<img src="<?php echo JRoute::_('templates/main/images/print.png'); ?>" class="icon-img1" />
        <a class="no_decoration" onClick="window.print();" href="javascript:void(0);">打印</a>
	</div>-->
    <!--<div style="float: left; height: 22px; width: 100px;"><img src="<?php echo JRoute::_('templates/main/images/compare.png'); ?>" class="icon-img">&nbsp;<a class="no_decoration" href="javascript:void(0);" onclick="javascript:alert('coming soon');">比比看</a></div>-->
    <!--<div style="float: left; height: 22px; width: 120px;">
    <img src="<?php echo JRoute::_('templates/main/images/email.png'); ?>" class="icon-img" />&nbsp;
    <a class="no_decoration" href="mailto:?subject=iHouse360.com -<?php echo $this->data->name_ch.' - '.$this->data->name_en ?>&body=">
		电邮给朋友	
    </a></div>-->
    <?php if($this->is_favorited > 0) : ?>
             
            <div id="favorite_ajax" style="float:left; height:22px; width:120px;">
            	<?php if($this->is_favorited == 1) : ?>
            	<img class="icon-img" src="<?php echo JRoute::_('templates/main/images/icon-fav.png'); ?>" />&nbsp;
               	<a class="no_decoration" href="javascript:addFavorite('<?php echo $this->property_no ?>','<?php echo $this->user_id; ?>','<?php echo JRoute::_('templates/main/images/icon-fav-remove.png') ?>','<?php echo JRoute::_('templates/main/images/icon-fav.png') ?>');">
                最爱
                </a>
                <?php endif; ?>
                <?php if($this->is_favorited == 2) : ?>
            	<img class="icon-img" src="<?php echo JRoute::_('templates/main/images/icon-fav-remove.png'); ?>" />&nbsp;
               	<a class="no_decoration" href="javascript:removeFavorite('<?php echo $this->fav_id; ?>','front','<?php echo JRoute::_('templates/main/images/icon-fav.png') ?>','<?php echo JRoute::_('templates/main/images/icon-fav-remove.png') ?>')">
                不喜欢
                </a>
                <?php endif; ?>
            </div>
    <?php  endif; ?>
	<div style="float: left; height: 22px; width: 120px;">
	<meta property="og:description" content="<?php echo $this->data->description ?>"/>
<script type="text/javascript">
document.write('<iframe src="http://www.facebook.com/plugins/like.php?href='+encodeURIComponent(location.href)+'&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=40" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:40px;" allowTransparency="true"></iframe>');
</script>
	</div>	
    <!--<div style="float: left; height: 22px; width: 190px;"><img src="<?php echo JRoute::_('templates/main/images/pdf.png'); ?>" class="icon-img">&nbsp;<a class="no_decoration" href="javascript:void(0);" onclick="javascript:alert('coming soon');">导出为PDF</a></div>-->
</div>

<div id="menu_list_property_detail">
<ul id="" class="tab_nav5">     
	<li id="tab_li5_info" class="tabactive5"><a href="javascript:propertyDetailInfoTab('info')"><span class="chinese_text_menu">基本信息</span></a></li>
	<li id="tab_li5_prop_desc"><a href="javascript:propertyDetailInfoTab('prop_desc')"><span class="chinese_text_menu">物业介绍</span></a></li>
	<li id="tab_li5_history_chart" ><a href="javascript:propertyDetailInfoTab('history_chart')"><span class="chinese_text_menu">历史均价</span></a></li>
    <li id="tab_li5_nearest_station" ><a href="javascript:propertyDetailInfoTab('nearest_station')"><span class="chinese_text_menu">附近地铁站</span></a></li>
    <li id="tab_li5_nearest_school" ><a href="javascript:propertyDetailInfoTab('nearest_school')"><span class="chinese_text_menu">附近学校</span></a></li>
    <li id="tab_li5_user_rating" ><a href="javascript:propertyDetailInfoTab('user_rating')"><span class="chinese_text_menu">用户评分</span></a></li>
    <li id="tab_li5_property_rating" ><a href="javascript:propertyDetailInfoTab('property_rating')"><span class="chinese_text_menu">发表评论</span></a></li>
</ul>   
</div>

<div id="tablist"> 
	<div id="tab_info" class="tabular1">
    	<div style="padding:10px 10px 40px 20px;">
         	<div style="float:left;width:100%;">
                <div id="static-text" style="float:left;">房产信息</div>
                <div style="padding-left:5px;float:left;">
                    <span style="font-size:13px;"><i><strong>Basic Information</strong></i></span>
                </div>
            </div>
            <div style="clear:both"></div>    
            <div style="float:left;width:60%;margin-bottom:15px;">
                <table id="prop_det_info" cellspacing=0 cellpadding=0 width="100%">
                <tr style="background-color:#f8f8f8;">
                	<td width="40%" id="table-detail-font"><div class="table_space">物业名</div></td>
                    <td width="60%" class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo $this->data->name_ch. " " .$this->data->name_en ?></span></div></td>
                </tr>
                <tr>
                	<td id="table-detail-font"><div class="table_space">物业地址</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo $this->data->address ?>&nbsp;<?php echo $this->data->postcode ?></span></div></td>
                </tr>
                <tr style="background-color:#f8f8f8;">
                	<td id="table-detail-font"><div class="table_space">邮区</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo $this->data->district_id ?></span></div></td>
                </tr>
                
                <tr>
                	<td id="table-detail-font"><div class="table_space">最新成交均价 (SGD)</div></td>
                    <td class="table-detail-font-value"><div class="table_space">
                    <span class="color-text1"> 
					每平方米 SGD$ <?php echo (!empty($this->price))?''.number_format($this->price):'-'; ?><br />
                    每平方尺 SGD$ <?php echo (!empty($this->price_psf))?' '.number_format($this->price_psf):number_format($this->price / 10.764); ?></span>
                    </div>
                    </td>
                </tr>
                <tr style="background-color:#f8f8f8;">    
                	<td id="table-detail-font"><div class="table_space">物业类别</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo ($this->data->property_type)?$this->data->property_type:'N/A'; ?></span></div></td>
                </tr>
                <tr>
                	<td id="table-detail-font"><div class="table_space">土地租用期 (年)</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo ($this->data->tenure)?$this->data->tenure:'N/A' ?></span></div></td>
                </tr>
                <tr style="background-color:#f8f8f8;">
                	<td id="table-detail-font"><div class="table_space">建筑类别</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo ($this->data->property_class)?$this->data->property_class:'N/A'; ?></span></div></td>
                </tr>
                <tr>
                	<td id="table-detail-font"><div class="table_space">封顶年份</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo ($this->data->year_built)?$this->data->year_built:'N/A'; ?></span></div></td>
                </tr>
                <tr style="background-color:#f8f8f8;">
                	<td id="table-detail-font"><div class="table_space">面积</div></td>    
			    	<td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo ($this->data->room_size)?$this->data->room_size:'N/A'; ?></span></div></td>
                </tr>
                <tr>
                	<td id="table-detail-font"><div class="table_space">单位总数</div></td>
                    <td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo ($this->data->units_on_condo)?$this->data->units_on_condo:'N/A' ?></span></div></td>
                </tr>
                <tr style="background-color:#f8f8f8;">    
					<td id="table-detail-font"><div class="table_space">开发商</div></td>
					<td class="table-detail-font-value"><div class="table_space"><span class="color-text1"><?php echo ($this->data->developer)?$this->data->developer:'N/A'; ?></span></div></td>
                </tr>
                </table>    
    		</div>
                
            <div style="float:right;width:37%;">
					{modulepos ihouse_guru}
                	{modulepos ihouse_total_review_and_ads}
            </div>	
      
        </div>
       	<div style="clear:both;"></div>
    </div>
    <div id="tab_prop_desc" class="tabular1" style="display:none;">
    	<div style="padding: 10px;">
            <div>
            	<div style="float:left;width:100%;border-bottom:2px solid #062284;">
            		<div id="static-text" style="float:left;">物业介绍</div>
                	<div style="padding-left:5px;float:left;"><span style="font-size:13px;">
                    	<i><strong>Property Description</strong></i></span>
                    </div>
                </div>
                <div style="clear:both;"></div>
                <div style="margin-top:10px;">
               		<div style="float:left;width:60%;word-wrap:break-word;">
            		<?php echo $this->data->description ?>
            		</div>
                	<div style="float:right;width:37%">
                	{modulepos ihouse_guru}
                	{modulepos ihouse_total_review_and_ads}
                	</div>
                
           	 	</div>
                <div style="clear:both"></div>
            </div>
        </div>
    </div>
    <div id="tab_history_chart" class="tabular1" style="display:none;">
     	<div style="padding: 10px;">
            <div>
            	<div style="float:left;border-bottom:2px solid #062284;width:100%;">
            	<div id="static-text" style="float:left;">历史均价</div>
                <div style="padding-left:5px;float:left;"><span style="font-size:13px;"><i><strong>Historical Chart</strong></i></span></div>
                </div>
                <div style="clear:both"></div>
                <div style="margin-top:10px;">
            	<?php echo $this->historical_chart_img->history_line ?>
                <?php echo $this->historical_chart_img->history_bar; ?>
            	</div>
            </div>
        </div>
    </div>
    <div id="tab_nearest_station" class="tabular1" style="display:none;">
    	<div style="padding: 10px;">
            <div style="float:left;border-bottom:2px solid #062284;width:100%;">
            	<div id="static-text" style="float:left;">附近地铁站</div>
                <div style="padding-left:5px;float:left;"><span style="font-size:13px;"><i><strong>Nearest MRT Station</strong></i></span></div>
            </div>
            <div style="clear:both"></div>
                
            <div style="margin-top:10px;">
               	<div style="float:left;width:60%;">
            		<?php echo $this->nearby_mrt ?>
            	</div>
                <div style="float:right;width:37%">
                	{modulepos ihouse_guru}
                	{modulepos ihouse_total_review_and_ads}
                </div>
                
            </div>
            <div style="clear:both"></div>
        </div>
    </div>
    <div id="tab_nearest_school" class="tabular1" style="display:none;">
    	<div style="padding: 10px;">
            <div>
            	<div style="float:left;border-bottom:2px solid #062284;width:100%;">
            	<div id="static-text" style="float:left;">附近学校</div>
                <div style="padding-left:5px;float:left;"><span style="font-size:13px;"><i><strong>Nearest School</strong></i></span></div>
                </div>
                <div style="clear:both"></div>
                <div style="margin-top:10px;">
               		<div style="float:left;width:60%;">
            		<?php echo $this->nearby_school ?>
            		</div>
                	<div style="float:right;width:37%">
                	{modulepos ihouse_guru}
                	{modulepos ihouse_total_review_and_ads}
                	</div>
                
           	 	</div>
                <div style="clear:both"></div>
            </div>
        </div>
    </div>
    <div id="tab_user_rating" class="tabular1" style="display:none;">
    	<div style="padding: 10px;">
            <div>
            	<div style="float:left;border-bottom:2px solid #062284;width:100%;">
            		<div id="static-text" style="float:left;">用户评分</div>
                	<div style="padding-left:5px;float:left;"><span style="font-size:13px;"><i><strong>User Rating</strong></i></span></div>
                     <div id="property_review_loader1" style="float:left;margin-left:10px;display:none;"><img src="<?php echo JRoute::_('templates/main/images/ajax-loader.gif'); ?>"  /></div>
                </div>
                <div style="clear:both"></div>
                <div id="ajax-review-form-happening" style="margin-top:10px;">
                {modulepos ihouse_property_rating}
            	</div>
            </div>
        </div>
    </div>
    <div id="tab_property_rating" class="tabular1" style="display:none;">
    	<div style="padding: 10px;">
            <div>
            	<div style="float:left;border-bottom:2px solid #062284;width:100%;">
            		<div id="static-text" style="float:left;">发表评论</div>
                	<div style="padding-left:5px;float:left;"><span style="font-size:13px;"><i><strong>Review</strong></i></span></div>
                    <div id="property_review_loader" style="float:left;margin-left:10px;display:none;"><img src="<?php echo JRoute::_('templates/main/images/ajax-loader.gif'); ?>"  /></div>
                </div>
                
                <div style="clear:both"></div>
                <div style="margin-top:10px;">
                {modulepos ihouse_review}
            	</div>
            </div>
        </div>
    </div>

</div>    

<div class="prop_detail_box">
	<ul class="tab_nav3">     
    <?php if($this->hasIhouseImages) { ?>
	<li id="tab_li3_image" class="tabactive3"><a href="javascript:propertyDetailTab('image')"><span class="chinese_text_menu">公寓图片</span></a></li>
    <?php } ?>
    <?php if($this->hasFloorPlan) { ?>
	<li id="tab_li3_floorplan" ><a href="javascript:propertyDetailTab('floorplan')"><span class="chinese_text_menu">户型图</span></a></li>
    <?php } ?>
	<li id="tab_li3_googlemap" <?php echo ($this->hasIhouseImages)?'':' class="tabactive3" ' ?>><a href="javascript:propertyDetailTab('googlemap')"><span class="chinese_text_menu">Google 地图</span></a></li>
	</ul>
</div>

<div id="propertyInfo12wrap" class="borderPropertyTab">
	<div id="propertyInfo12content" style="height:454px;width:657px;">
    	<?php if($this->hasIhouseImages) { ?>
		<div id="property_detail_image" style="margin-bottom:50px;text-align:center;">
			{modulepos postcode_img}
		</div>                        
		<?php } ?>
        
        <?php if($this->hasFloorPlan) { ?>
		<div id="floor_plan_image" style="display:none;">	
			<?php echo $this->floor_plan_img; ?>
            <div id="ajax-floorplan-image-detail"></div>
		</div>
		<?php } ?>
        
		<div id="google_map_image" style="">
			<div id="canvas_map" style="width:659px;height: 460px;"></div> 
		</div>   
	</div>
</div>
<div style="clear:both"></div>
<div class="prop_detail_box3">
{modulepos ihouse_property_highlight}
</div>
<div style="clear:both"></div>
<div class="prop_detail_box2">
{modulepos ihouse_specialist_this_condo}
</div>
<div style="clear:both"></div>
<div class="prop_detail_box2">
{modulepos ihouse_similar_properties}
</div>
<div class="prop_detail_box4">&nbsp;
</div>
</div><!-- style="padding:15px;"> -->
</div><!-- background:#fff -->

</div>