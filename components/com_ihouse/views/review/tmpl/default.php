﻿<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('behavior.tooltip'); ?>
<?php if(!$uid) : ?>
<div style="float:left;color:red;margin-top:-20px;">
You must log in to review
</div>
<div style="clear:both"></div>
<?php endif; ?>
<div style="float:left;border-bottom:1px dotted #ccc;margin:5px 0;width:100%;">
	<?php if(empty($review)): ?>
    	No Review
    <?php else : ?>
	<?php foreach($review as $r) : ?>
    <div style="float:left;">
    	<?php
		  					if(empty($r->user_image)) {
								$user_image = JRoute::_('templates/main/images/no_avatar.jpg');
							} else {
								$user_image = 'image.php?size=90&type=1&path='.$r->user_image;
							}
		  				?>
                        	<a style="text-decoration:none;color:#fff;" href="<?php echo JRoute::_('index.php?option=com_ihouse&view=ihouse&layout=property_detail&id='.$r->property_id.'&postcode='.$r->postcode) ?>">
            		<img style="border:none;" src="<?php echo $user_image; ?>" />
                </a>
    </div>
        
    <div style="float:left;margin-left:5px;width:83%;">
       	<div><span style="color:#062284;font-size:15px;font-weight:bold;"><?php echo $r->title ?></span></div>
        <div>
			<a style="color:#062284;text-decoration:underline;" href="<?php echo JRoute::_('index.php?option=com_ihouse&view=ihouse&layout=property_detail&id='.$r->property_id.'&postcode='.$r->postcode) ?>"><?php echo $r->name_ch.$r->name_en ?></a>
        </div>
        <div><span style="color:#ccc;font-size:13px;">
        <?php 
				if(!empty($r->name) && !empty($r->chinese_name)) {
							$name = '';
							if (!empty($r->name)) {
								$name .= $r->name;
							}
							if (!empty($r->chinese_name)) {
								$name .= '&nbsp;'.$r->chinese_name;
							}
							
							echo $name;
				} else { 
					  		echo 'N/A';
				}
		?>
		<?php echo date('Y/m/d H:i',strtotime($r->created)); ?></span></div>
        <div><span style="font-size:15px"><?php echo $r->message ?></span></div>
    </div>
    <div style="clear:both"></div>
    <?php endforeach; ?>
    
    <?php endif; ?>
</div>    
    
<div style="float:left;border-bottom:1px dotted #ccc;margin:5px 0;width:100%">
    	<div style="float:right;padding-bottom:10px;">
        	<?php echo $pagination_html ?>
        </div>
        <div style="clear:both"></div>
</div>
<div style="clear:both"></div> 