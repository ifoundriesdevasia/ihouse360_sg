﻿<?php
/**
* @version		$Id: view.html.php 10711 2008-08-21 10:09:03Z eddieajau $
* @package		Joomla
* @subpackage	Poll
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Poll component
 *
 * @static
 * @package		Joomla
 * @subpackage	Poll
 * @since 1.0
 */
 
class IhouseViewMapsearch extends JView
{
	function display($tpl = null)
	{
		global $mainframe, $option;
		$document =& JFactory::getDocument();
		
		$user		=& 	JFactory::getUser();
		
		$uid		=	$user->get('id');	
		
		$gid		=	$user->get('gid');
		
		$layout		= 	JRequest::getCmd('layout', 'default');
		
		$db 	  	=& 	JFactory::getDBO();

		//$document->addScript('http://maps.google.com/maps/api/js?sensor=false');
		$document->addScript('https://maps.googleapis.com/maps/api/js?key=AIzaSyAUWlGtel3Nchvh14BSi1aMwi09UtQtw1E&libraries=places&region=SG');
		$document->addScript(JURI::root(true).'/components/com_ihouse/assets/jquery-1.3.2.js');
		$document->addScript(JURI::root(true).'/components/com_ihouse/assets/jquery-ui-1.7.1.custom.min.js');
		$document->addScript(JURI::root(true).'/components/com_ihouse/assets/slider.js');
		
		$select 	= 	JRequest::getInt('select', '');
		
		$document->addScriptDeclaration($this->getGoogleMapAPI($select));
		
		parent::display($tpl);
	}
	
	
	
	function getGoogleMapAPI($select){
  		
		
		$javascript = "
				var map;
				var marker;
				var geocoder;
				var infoWindow;
				var ib;
				
				var markersArray 	= [];
				var mrtArray 		= [];
				var schoolArray		= [];


				var myArray 	= [];
				var myArray2 	= [];
				var myArray3 	= [];
				var myArray4	= [];
				var myArray5	= [];
				var myArray6	= [];
				var myArray7	= [];
				var total_data	= 0;
				
				var marker_reloc;
				
				function initialize() {
					
					geocoder 			= 	new google.maps.Geocoder();
					infoWindow			=	new google.maps.InfoWindow();
  					var lat = 0;
					var lng = 0;
					
					
					var select = ".$select.";
					
						switch(select) {
							case 1:
								lat = '1.3294889';
								lng = '103.7382593';
								break;
							case 2:
								lat = '1.342417';
								lng = '103.776627';
								break;
							case 3:
								lat = '1.3593727';
								lng = '103.8390162';
								break;
							case 4:
								lat = '1.312956';
								lng = '103.838442';
								break;
							case 5:
								lat = '1.302565';
								lng = '103.834694';
								break;
							case 6:
								lat = '1.2494041';
								lng = '103.8303209';
								break;
							case 7:
								lat = '1.2761';
								lng = '103.85467';
								break;
							case 8:
								lat = '1.303306';
								lng = '103.9165274';
								break;
							default:
								lat = '1.29027';
								lng = '103.85201';
								break;
						}
					
					lat = parseFloat(lat);
					lng = parseFloat(lng);
					
					var latlng = new google.maps.LatLng(lat,lng);
  					var	options = {
   								zoom: 13,
    							center: latlng,	
    							mapTypeId: google.maps.MapTypeId.ROADMAP
  					};
						
					
	   
  					map = new google.maps.Map(document.getElementById('big_map'), options);
					
					google.maps.event.addListener(map, 'dragstart', deleteOverlays); 
					google.maps.event.addListener(map, 'idle', googleIdleSearch);
				}

				function googleIdleSearch(type) {
					
					deleteOverlays();
					if(type == 1) {
						$('budget').setProperty('value','');
					} else {
						$('price').setProperty('value','');
						$('price_max').setProperty('value','');
						$('bedroom').setProperty('value','');
					}
					setTimeout('showMarkers()', 1000);
				}


				function showMarkers() {
					
					var t = map.getBounds();
					
					var lat1 = parseFloat(t.getSouthWest().lat());
					var lng1 = parseFloat(t.getSouthWest().lng());
					var lat2 = parseFloat(t.getNorthEast().lat());
					var lng2 = parseFloat(t.getNorthEast().lng());
					
					panningGoogleMap(lat1, lat2, lng1, lng2);
					
				}
				

				// Removes the overlays from the map, but keeps them in the array
				function clearOverlays() {
					
  					if (markersArray) {
    					for (i in markersArray) {
      						markersArray[i].setMap(null);
						}
  					}
				}

				function clearOverlays1(i, intvl) {
					
  					if (markersArray) {
    					for (var x = i; x <= (i+intvl); x++) {
							if(markersArray[x] != null) {
      							markersArray[x].setMap(null);
							}
    					}
  					}
				}

				function showOverlaysCustom(type) {
					
					if(type == 'school') {
						if (schoolArray) {
    						for(i = 0; i < schoolArray.length;i++) {
								if(schoolArray[i] != null) {
      								schoolArray[i].setMap(map);
								}
    						}
  						}
					}

					
				}

				function clearOverlaysCustom(type) {
					
					if(type == 'school') {
						if (schoolArray) {
    						for(i2 = 0; i2 < schoolArray.length;i2++) {
								if(schoolArray[i2] != null) {
      								schoolArray[i2].setMap(null);
								}
    						}
  						}
					}
				}

				
				// Deletes all markers in the array by removing references to them
				
				function deleteOverlays() {
					
					if (markersArray) {
						
    					for(i10 = 0; i10 < markersArray.length;i10++) {
							if(markersArray[i10] != null || markersArray[i10] != '') {
      							markersArray[i10].setMap(null);
							}
    					}
  					}

				}

				
				function getLatLng(option, address, postcode,name_ch,name_en) {
					var icon_img;
					
					if(option == 'school') {
						
						icon_img = '".JRoute::_('templates/mapsearch/images/school3.png')."';
						
					}
					
					if(marker_reloc) {
						marker_reloc.setMap(null); 
					}
					
					geocoder.geocode({ 
										'address': address + ' Singapore ' + postcode
										}, 
							function(results, status) {
     							if (status == google.maps.GeocoderStatus.OK) {
									
									if(option == 'school') {
										marker_reloc = new google.maps.Marker({
            								map: map, 
            								position: results[0].geometry.location,
											title : name_ch + name_en ,
											icon : icon_img
        								});
									} 
									
        							map.setCenter(results[0].geometry.location);
					        		map.setZoom(16);
				      			}
					   		});
				}
				
				
				

				function loadMarkers(starts,intvl, next_action) {
					
					var total_postcode 	= myArray.length;
					
					var limitstart 		= starts + intvl;
					
					if(next_action == 'next') {
						clearOverlays1(0, intvl);
						
						var n1 = starts + intvl;
						var n2 = starts - intvl;
						
						if(limitstart < total_postcode) {
							
							$('load_markers_next').setProperty('href','javascript:loadMarkers('+n1+','+intvl+',\'next\');');
							$('load_markers_next').setHTML('下'+intvl+'个小区');
							
						} else {
							$('load_markers_next').setStyle('display','none');
							$('load_markers_prev').setStyle('display','');
						}
						
						$('load_markers_prev').setProperty('href','javascript:loadMarkers('+n2+','+intvl+',\'prev\');');
						$('load_markers_prev').setHTML('Prev '+intvl+'个小区');
						
					} else if(next_action == 'prev') {
						
						clearOverlays1(0, intvl);
						
						var n1 = starts - intvl;
						var n2 = starts + intvl;
						
						if(starts > 0) {
							$('load_markers_prev').setProperty('href','javascript:loadMarkers('+n1+','+intvl+',\'prev\');');
							$('load_markers_prev').setHTML('Prev '+intvl+'个小区');
						} else {
							$('load_markers_prev').setStyle('display','none');
							$('load_markers_next').setStyle('display','');
							
						}
							
						$('load_markers_next').setProperty('href','javascript:loadMarkers('+n2+','+intvl+',\'next\');');
						$('load_markers_next').setHTML('下'+intvl+'个小区');
						
					} else {
						if(intvl == total_postcode) {
							
						}
						else if(intvl < total_postcode) {
							var n1 = starts + intvl;
							$('load_markers_next').setStyle('display','');
							$('load_markers_next').setProperty('href','javascript:loadMarkers('+n1+','+intvl+',\'next\');');
							$('load_markers_next').setHTML('下'+intvl+'个小区');
						}
					}
					
					markersArray = [];
					
					for(var i = 0; i < intvl; i++)
					{
						if(myArray[starts+i]) {
							var postcode 	= myArray[starts+i];
							var title_ch	= myArray2[starts+i];
							var title_en	= myArray7[starts+i];
							var psm_price	= myArray3[starts+i];
							var lats		= myArray4[starts+i];
							var lngs		= myArray5[starts+i];
							var prop_id		= myArray6[starts+i];
						
							geocodeGoogleMap(postcode, prop_id, title_ch,title_en, psm_price, lats, lngs,'mapsearch',i);
						}
					}

				}
";
	    return $javascript;
	}
	
	function googleMapContent($postcode) {
	}
	
	function grabAllPostcodes() {
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT DISTINCT(postcode) FROM #__ihouse_property "
						.	" LIMIT 50 ";
			$db->setQuery( $query );
			$lists = $db->loadAssocList();
			
		return $lists;	
	}
	
	
	
	
	
	
}
?>
