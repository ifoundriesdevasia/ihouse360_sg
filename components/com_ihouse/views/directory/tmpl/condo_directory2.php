﻿<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('behavior.tooltip'); ?>

<?php
	$rows =& $this->items;
?>

<div id="condo_directory2">
<div>
<form name="adminForm" id="adminForm" action="<?php echo JRoute::_('index.php?option=com_ihouse&view=directory&layout=condo_directory2') ?>">
<table id="condo_directory_2_table" cellspacing="0" cellpadding="0" width="100%">
<tr class="sortable">
	<th align="left" width="21%">
		<?php echo JHTML::_( 'grid.sort', '项目名', 'p.name_en', $this->lists['order_Dir'], $this->lists['order']); ?>
    </th>
    <th align="left" width="21%">
		<?php echo JHTML::_( 'grid.sort', '地址', 'p.address', $this->lists['order_Dir'], $this->lists['order']); ?>
    </th>
    <th width="13%">
		<?php echo JHTML::_( 'grid.sort', '均价(S$)', 'pp.price', $this->lists['order_Dir'], $this->lists['order']); ?>
    </th>
    <th width="13%">
		<?php echo JHTML::_( 'grid.sort', '地契(年)', 'p.tenure', $this->lists['order_Dir'], $this->lists['order']); ?>
    </th>
    <th width="14%">
		<?php echo JHTML::_( 'grid.sort', 'TOP年份', 'p.year_built', $this->lists['order_Dir'], $this->lists['order']); ?>
    </th>
    <th width="18%">
		<?php echo JHTML::_( 'grid.sort', '公众评分(%)', 'r.overall_avg', $this->lists['order_Dir'], $this->lists['order']); ?>
    </th>
</tr>
<?php if(empty($rows)) : ?>
<tr>
	<td colspan="6" style="padding:10px;">No Search Found!!</td> 
</tr>
</table>
<?php else : ?>

<?php foreach($rows as $row) : ?>
<tr>
	<td id="tabtext-padding1">
    	<a href="<?php echo JRoute::_('index.php?option=com_ihouse&layout=property_detail&id='.$row->id.'&postcode='.$row->postcode)?>">
		<?php echo $row->name_en ?><?php echo (!empty($row->name_ch))?'<br />'.$row->name_ch:''?> 
        </a>
    </td>
    <td><?php echo $row->address ?></td>
    <td align="center"><?php echo ($row->price)?number_format($row->price):'N/A'; ?></td>
    <td align="center"><?php echo ($row->tenure)?$row->tenure:'N/A'; ?></td>
    <td align="center"><?php echo ($row->year_built)?$row->year_built:'N/A'; ?></td>
    <td align="center"><?php echo ($row->overall_avg)?sprintf("%01.1f",$row->overall_avg / 5 * 100).'%':'N/A'; ?></td>
</tr>
<?php endforeach; ?>
</table>
<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />

<input type="hidden" name="type" value="<?php echo JRequest::getVar('type','') ?>" />
<input type="hidden" name="year_built" value="<?php echo JRequest::getVar('year_built','') ?>" />
<input type="hidden" name="lease" value="<?php echo JRequest::getVar('lease','') ?>" />
<input type="hidden" name="district_id" value="<?php echo JRequest::getVar('district_id','') ?>" />

<input type="hidden" name="option" value="com_ihouse" />
<input type="hidden" name="view" value="directory" />
<input type="hidden" name="layout" value="condo_directory2" />
</form>
</div>
<div class="pagination_right" style="margin-left:10px;margin-right:10px;">
<?php echo $this->currentpages ?> of <?php echo $this->pagestotal ?> pages
</div>
<div class="pagination_right">
<?php echo $this->pagination->getPagesLinks() ?>
</div>

<?php endif; ?>

</div>