<?php
/**
* @version		$Id: view.html.php 10711 2008-08-21 10:09:03Z eddieajau $
* @package		Joomla
* @subpackage	Poll
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Poll component
 *
 * @static
 * @package		Joomla
 * @subpackage	Poll
 * @since 1.0
 */
class IhouseViewDirectory extends JView
{
	function display($tpl = null)
	{
		global $mainframe, $option;

		$anytitle = '';

        $filter_order     = $mainframe->getUserStateFromRequest( $option.'filter_order', 'filter_order', 'p.name_en', 'cmd' );
        $filter_order_Dir = $mainframe->getUserStateFromRequest( $option.'filter_order_Dir', 'filter_order_Dir', 'ASC', 'word' );

		$user		=& 	JFactory::getUser();
		$uid		=	$user->get('id');	
		$gid		=	$user->get('gid');
		
		if(!$uid) {
			$document =& JFactory::getDocument();
			$document->addScript( JURI::base(true). '/includes/js/joomla.javascript.js' );
		}
		
		$layout		= 	JRequest::getVar('layout', 'condo_directory2');
		
		$db 	  	=& 	JFactory::getDBO();
		
		$limitstart =	JRequest::getVar('limitstart', 0);
		$limit		=	JRequest::getVar('limit', 20);

		$district_id	=	JRequest::getVar('district_id', '');
		$lease			=	JRequest::getVar('lease', '');
		$year_built		=	JRequest::getVar('year_built', '');
		
		$type		=	JRequest::getVar('type','');

		$where 	= array();
		
		switch($type) {
			case 'num' 	:
				$where[] = " (p.name_en REGEXP '^[0-9]' OR p.name_ch REGEXP '^[0-9]') ";
				$anytitle	.=	'0-9';
				break;
			case ''		:
				// nothing to do
				break;
			default		:
				$where[] = " (p.name_en REGEXP '^[".$type."]' OR p.name_ch REGEXP '^[".$type."]') ";
				$anytitle	.=	$type;
				break;
		}
		
		$filters = new stdClass;
		
		if(!empty($district_id) || !empty($lease) || !empty($year_built) ) {
			if(!empty($district_id)) {
				$where[] = " p.district_id = '$district_id' ";
				$anytitle	.=	' - ' . $district_id;
			}
			
			if(!empty($year_built)) {
				switch($year_built) {
					case 1:
						$where[] = " p.year_built <= 1900 ";
						$anytitle	.=	' - Top Year <= 1900 ' ;
						break;
					case 2:
						$where[] = " p.year_built >= 1900 AND p.year_built <= 1999 ";
						$anytitle	.=	' - 1900 <= Top Year <= 1900 ' ;
						break;
					case 3:
						$where[] = " p.year_built > 2000 AND p.year_built <= 2005 ";
						$anytitle	.=	' - 2000 <= Top Year <= 2005 ' ;
						break;
					case 4:
						$where[] = " p.year_built > 2006 AND p.year_built <= 2010 ";
						$anytitle	.=	' - 2006 <= Top Year <= 2010 ' ;
						break;
					case 5:
						$where[] = " p.year_built >= 2010 ";
						$anytitle	.=	' - 2010 <= Top Year' ;
						break;
				}
			}

			if(!empty($lease)) {
				$where[] = " p.tenure = ".$db->Quote( $db->getEscaped($lease, true ), false );	
				$anytitle	.=	' - Tenure ' . $lease;
			}
		}
		
		
		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
		
		
		
		if(!empty($filter_order) && !empty($filter_order_Dir) )
            $orderby = ' ORDER BY '.$filter_order.' '.$filter_order_Dir;
        else 
			$orderby	=	" ORDER BY p.name_en ASC ";

		if($layout == 'condo_directory2') {
			
			$query	=	' SELECT p.*,r.overall_avg ,pp.price ' 
						.	' FROM #__ihouse_property AS p '
						.	" LEFT JOIN #__ihouse_property_slug AS s ON s.project_name = p.name_en "
						.	' LEFT JOIN #__ihouse_property_price AS pp ON pp.project_name = s.slug '
						.	" LEFT JOIN #__ihouse_property_rating_avg AS r ON r.property_id = p.id "
						
						.	$where
						.	' '
						.	$orderby;
		
			$db->setQuery( $query , $limitstart, $limit);
		
			$rows = $db->loadObjectList();
		
			if ($db->getErrorNum())
			{
				echo $db->stderr();
				return false;
			}
		
			$query	=	'SELECT COUNT(p.id) ' 
							.	' FROM #__ihouse_property AS p '
							//.	' LEFT JOIN #__ihouse_property_price AS pp ON pp.postcode = p.postcode '
							.	$where
							.	' ';
				$db->setQuery( $query );
				$total = $db->loadResult();

			jimport('joomla.html.pagination');
			$pagination = new JPagination( $total, $limitstart, $limit );
 
 			$pagestotal = ceil($total / $limit);
			$currentpages = ($limitstart / $limit) + 1;
			
        	/* Get the values from the state object that were inserted in the model's construct function */   
        
        	$lists['order_Dir'] = $filter_order_Dir ;
        	$lists['order']     = $filter_order;
 			$anytitle	.=	' - Filter: '. $filter_order.' '.$filter_order_Dir ;
			
 			$document =& JFactory::getDocument();
			$document->setTitle($document->getTitle()." - ". $anytitle);
 
        	$this->assignRef( 'lists', $lists );
			$this->assignRef('layout', 		$layout);
			$this->assignRef('user',		JFactory::getUser());
			$this->assignRef('items',		$rows);
			$this->assignRef('pagination',	$pagination);
			
			$this->assignRef('pagestotal', 	$pagestotal);
			$this->assignRef('currentpages', $currentpages);
		}
		parent::display($tpl);
	}
}
?>
