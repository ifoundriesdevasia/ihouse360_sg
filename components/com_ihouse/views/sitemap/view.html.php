﻿<?php
/**
* @version		$Id: view.html.php 10711 2008-08-21 10:09:03Z eddieajau $
* @package		Joomla
* @subpackage	Poll
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Poll component
 *
 * @static
 * @package		Joomla
 * @subpackage	Poll
 * @since 1.0
 */
class IhouseViewSitemap extends JView
{
	function display($tpl = null)
	{
		global $mainframe, $option;

		$user		=& 	JFactory::getUser();
		
		$uid		=	$user->get('id');	
		
		$db 	  	=& 	JFactory::getDBO();
		
		$where		=	array();
		
		$this->assignRef('sector', $this->getSectorHTML());
		$this->assignRef('condodirectory', $this->getCondoDirectoryHTML());
		
		$this->assignRef('googlemapnew', $this->getMenuTypeHTML('main-menu-1'));
		$this->assignRef('googlemapsecond', $this->getMenuTypeHTML('main-menu-2'));
		$this->assignRef('googlemaprent', $this->getMenuTypeHTML('main-menu-3'));
		
		$this->assignRef('member', $this->getMenuTypeHTML('ihouse360-sitemap'));
		$this->assignRef('singapore', $this->getMenuTypeHTML('main-menu-5'));
		$this->assignRef('footer', $this->getMenuTypeHTML('footer-menu'));
		$this->assignRef('db', $db);
		parent::display($tpl);
		
	}
	
	function getSectorHTML() {
		$db		=& 	JFactory::getDBO();

		$query 	= 'SELECT * FROM #__ihouse_mapping_sector';
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		$html = '';
		foreach($rows as $r) :
		$html .= '<a class="sitemap-link" href="'.JRoute::_('index.php?option=com_ihouse&sector='.$r->mapping_district).'">';
		$html .= $r->name_ch;
        $html .= '</a>&nbsp;&nbsp;';
		endforeach;
		
		return $html;
	}
	
	function getCondoDirectoryHTML() {
		$html 	= '';
		$urladd	= '';
		$html .= '<a class="sitemap-link" href="'.JRoute::_('index.php?option=com_ihouse&view=directory&layout=condo_directory2'.$urladd.'&Itemid=102') .'">[All]</a>&nbsp;&nbsp;';
		
        $html .= '<a class="sitemap-link" href="'.JRoute::_('index.php?option=com_ihouse&view=directory&layout=condo_directory2&type=num'.$urladd.'&Itemid=102') .'">[0-9]</a>&nbsp;&nbsp;';
                                
		foreach(range('A','Z') as $i)  :
		$html .= '<a '.(($i == $type)?"class='active' ":"").' class="sitemap-link" href="'.JRoute::_('index.php?option=com_ihouse&view=directory&layout=condo_directory2&type='.$i.$urladd.'&Itemid=102').'">['.$i.']</a>&nbsp;&nbsp;'; 
		endforeach;

		return $html;
	}
	
	function getMenuTypeHTML($type) {
		$db		=& 	JFactory::getDBO();

		$query 	= " SELECT * FROM #__menu WHERE menutype = '$type' and published = 1 ";
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
		
		$html = '';
		foreach($rows as $r) :
			$html .= '<a class="sitemap-link" href="'.JRoute::_($r->link.'&Itemid='.$r->id).'">'.$r->name.'</a>&nbsp;&nbsp;';
		endforeach;
		
		return $html;
	}
}
?>
