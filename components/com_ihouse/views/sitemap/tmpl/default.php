﻿<?php defined('_JEXEC') or die('Restricted access'); ?>
<style type="text/css">
.label-sitemap {
	float: left; border-bottom: 2px solid rgb(6, 34, 132); width: 100%;margin-top:20px;
}

a.sitemap-link {
	text-decoration:none;
	color:#525252;
}
</style>
<div class="component-pos" style="float:left;width:100%;">
	<div style="background-color: rgb(255, 255, 255);" id="sub-small-component-border">
    	<div id="sub-small-component">
        	<div id="component-headerpos">
            	<div id="component-logo">
                	<div id="component-title"><span class="chinese_text1"></span>&nbsp;<i>Sitemap</i></div>
                </div>
            </div>
        </div>
        
        <div id="sub-small-component-content">
        	<div style="margin:10px;">
            <div class="label-sitemap">
            	<div style="float: left;" id="static-text">Google地图房源搜索</div>
            </div>
            <div style="clear:both"></div>
        	
			<div style="margin:5px;"><?php echo $this->googlemapnew; ?></div>
			<div style="margin:5px;"><?php echo $this->googlemapsecond; ?></div>
			<div style="margin:5px;"><?php echo $this->googlemaprent; ?></div>
            
            <div class="label-sitemap">
            	<div style="float: left;" id="static-text">新加坡最新房源</div>
            </div>
            <div style="clear:both"></div>
			
			<div style="margin:5px;"><?php echo $this->sector ?></div>
            <div class="label-sitemap">
            	<div style="float: left;" id="static-text">新加坡公寓目录</div>
            </div>
            <div style="clear:both"></div>
    		
			<div style="margin:5px;"><?php echo $this->condodirectory; ?></div>
            
            <div class="label-sitemap">
            	<div style="float: left;" id="static-text">新加坡本地信息</div>
            </div>
            <div style="clear:both"></div>
			
			<div style="margin:5px;"><?php echo $this->singapore ?></div>

            <div class="label-sitemap">
            	<div style="float: left;" id="static-text">Ihouse360会员服务</div>
            </div>
            <div style="clear:both"></div>
			
			<div style="margin:5px;"><?php echo $this->member ?></div>
            <div class="label-sitemap">
            	<div style="float: left;" id="static-text">关于我们</div>
            </div>
			<div style="clear:both"></div>
			<div style="margin:5px;"><?php echo $this->footer ?></div> 
            </div>
            <div style="margin:5px">&nbsp;</div>
        </div>          
	</div>
</div>