<?php defined('_JEXEC') or die('Restricted access'); ?>
<?php if(empty($review)): ?>
    			No Review
   	 		<?php else : ?>
			<?php foreach($review as $r) : ?>
    			<div style="float:left;width:15%;border-right:1px #039 dotted;position:relative;">
               <?php
		  					if(empty($r->user_image)) {
								$user_image = JRoute::_('templates/main/images/no_avatar.jpg');
							} else {
								$user_image = 'image.php?size=90&type=1&path='.$r->user_image;
							}
		  		?>
                        	<a style="text-decoration:none;color:#fff;" href="<?php echo JRoute::_('index.php?option=com_ihouse&view=ihouse&layout=property_detail&id='.$r->property_id.'&postcode='.$r->postcode) ?>">
            		<center><img style="border:none;" src="<?php echo $user_image; ?>" /></center>
                </a>
    			</div>
        
    			<div style="float:left;margin-left:5px;width:80%;word-wrap: break-word;">
       				<div><span style="color:#062284;font-size:15px;font-weight:bold;"><?php echo $r->title ?></span></div>
                     <div>
			<a style="color:#062284;text-decoration:underline;" href="<?php echo JRoute::_('index.php?option=com_ihouse&view=ihouse&layout=property_detail&id='.$r->property_id.'&postcode='.$r->postcode) ?>"><?php echo $r->name_ch.$r->name_en ?></a>
        </div>
        			<div><span style="color:#ccc;font-size:13px;">
        			<?php 
					if(!empty($r->name) && !empty($r->chinese_name)) {
							$name = '';
							if (!empty($r->name)) {
								$name .= $r->name;
							}
							if (!empty($r->chinese_name)) {
								$name .= '&nbsp;'.$r->chinese_name;
							}
							
							echo $name;
					} else { 
					  		echo 'N/A';
					}
					?>
					<?php echo date('Y/m/d H:i',strtotime($r->created)); ?></span>
            		</div>
                    
        			<div id="review-<?php echo $r->id ?>s" style="font-size:15px" class="reviewclass-<?php echo $r->id ?>">
                    <?php if(strlen($r->message) >= 300) : ?>
						<?php echo substr_replace($r->message,'..',400) ?>
                    <?php else : ?>
                    	<?php echo $r->message; ?>    
                    <?php endif; ?>    
                    <div style="position:relative;text-align:right;">[<a style="color:#ff8200;text-decoration:none;font-size:12px;" href="javascript:getReadReviewAllLess('<?php echo $r->id ?>', 1)">Read all</a>]</div>
                    </div>
                    
                    <div id="review-<?php echo $r->id ?>l" style="font-size:15px;display:none;" class="reviewclass-<?php echo $r->id ?>">
                    	<?php echo $r->message; ?>      
                    <div style="position:relative;text-align:right;">[<a style="color:#ff8200;text-decoration:none;font-size:12px;" href="javascript:getReadReviewAllLess('<?php echo $r->id ?>', 0)">Read less</a>]</div>
                    </div>
    			</div>
    			<div style="clear:both"></div>
				<hr size="1" width="99%" />
    		<?php endforeach; ?>
            
            	<div style="float:right;">
                	<?php echo $pagination_html ?>
                </div>
                <div style="clear:both;"></div>
    			<!--<div style="float:right;margin-top:5px;"><a href="#<?php //echo JRoute::_('index.php?option=com_ihouse')?>" style="color:#ff8200;text-decoration:none;">显示更多 ></a></div>-->
    		<?php endif; ?>