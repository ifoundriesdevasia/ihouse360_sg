﻿<?php
/**
* @version		$Id: view.html.php 10711 2008-08-21 10:09:03Z eddieajau $
* @package		Joomla
* @subpackage	Poll
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Poll component
 *
 * @static
 * @package		Joomla
 * @subpackage	Poll
 * @since 1.0
 */
class IhouseViewPropertysearch extends JView
{
	function display($tpl = null)
	{
		global $mainframe, $option;

		$user		=& 	JFactory::getUser();
		
		$uid		=	$user->get('id');	
		
		$gid		=	$user->get('gid');
		
		$layout		= 	JRequest::getCmd('layout', 'default');
		
		$db 	  	=& 	JFactory::getDBO();
		
		$limitstart =	JRequest::getVar('limitstart', 0);
		$limit		=	JRequest::getVar('limit', 10);
		
		$keyword			=	strtolower(JRequest::getVar('global_search_keyword', ''));
		
		
		/* Property Directory */
		$orderby = '';
		
		$whereor	=	array();
		
		if ($keyword != '')
		{
			
			$whereor[] = ' ( LOWER(p.name_en) LIKE '.$db->Quote( '%'.$db->getEscaped( $keyword, true ).'%', false ).' ) ';
			$whereor[] = ' ( LOWER(p.name_ch) LIKE '.$db->Quote( '%'.$db->getEscaped( $keyword, true ).'%', false ).' ) ';
			
			$whereor 		= ( count( $whereor ) ? implode( ' OR ', $whereor ) : '' );
			
			$where[]	=	$whereor;
			
			$orderby2 = ' ORDER BY p.posting_date DESC ';
			
		} else {
			$limits = ' LIMIT 20 ';
			$orderby1 = ' ORDER BY RAND() ' ; // property
			$orderby2 = ' ORDER BY RAND() '; // ads
		}
		
		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
		
		$query	=	'SELECT p.* FROM #__ihouse_property AS p ' 
						.	$where
						.	$orderby1
						.	$limits
					;
			
		$db->setQuery( $query );
		
		$prop_directory 	= $db->loadObjectList();
		$count_pd			=	count($prop_directory);
		
		if ($db->getErrorNum())
		{
			echo $db->stderr();
			return false;
		}
		
		/* Ads Property Listing */
		$where 		= 	array();
		$query		=	'';
		
		$where[] 	= 	' p.publish = 1 ';
		$where[]	=	" cb.status = 'A' ";
		
		if($keyword)
		{
			$where[] = ' ( LOWER(p.property_name) LIKE '.$db->Quote( '%'.$db->getEscaped( $keyword, true ).'%', false )	
						.	' OR LOWER(p.street_name) LIKE '.$db->Quote( '%'.$db->getEscaped( $keyword, true ).'%', false )
						.	' OR LOWER(p.ad_title) LIKE '.$db->Quote( '%'.$db->getEscaped( $keyword, true ).'%', false )
						.	' OR LOWER(p.postcode) LIKE '.$db->Quote( '%'.$db->getEscaped( $keyword, true ).'%', false )
						.	' OR LOWER(p.add_info) LIKE '.$db->Quote( '%'.$db->getEscaped( $keyword, true ).'%', false )
						.	' ) ';
		} 
		
		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
		
		$count		=	' SELECT COUNT(p.id) ';
		$searchlst	=	' SELECT p.*,p.id AS ads_id, u.name, u.chinese_name, u.company_name ,u.company_logo ,u.mobile_contact, u.website,u.user_image,u.user_category ,u.email, u.cea_reg_no ';
		
		$query		=	$count
							.	' FROM #__ihouse_ads AS p '
							.	' LEFT JOIN #__users AS u ON u.id = p.posted_by '
							.	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = p.subscription_id "
							//.	$join_property
							//.	'  LEFT JOIN #__ihouse_ads_image AS i ON i.ads_id = p.id '
							.	$where
							.	$orderby2;
			$db->setQuery( $query );
			
			$count_ads	=	$db->loadResult();
		
		$query		=	$searchlst
							.	' FROM #__ihouse_ads AS p '
							.	' LEFT JOIN #__users AS u ON u.id = p.posted_by '
							.	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = p.subscription_id "
							//.	$join_property
							//.	'  LEFT JOIN #__ihouse_ads_image AS i ON i.ads_id = p.id '
							.	$where
							.	$orderby2;
			
			$db->setQuery( $query , $limitstart, $limit);
			$ads	= $db->loadObjectList();
			
		
	
		/*
		$query	=	'SELECT COUNT(p.id) ' 
							.	' FROM #__ihouse_ads AS p '
							.	' '
							.	$where
							.	' ';
			$db->setQuery( $query );
			$total = $db->loadResult();
		*/
		
		jimport('joomla.html.pagination');
		$pagination = new JPagination( $count_ads, $limitstart, $limit );	
		
		/*
		$this->assignRef('user',		JFactory::getUser());
		$this->assignRef('items',		$rows);
		*/
		$this->assignRef('pagination',	$pagination);
		
		
		$this->assignRef('prop_directory', $prop_directory);
		$this->assignRef('c_prop_directory', $count_pd);
		
		$this->assignRef('items', $ads);
		$this->assignRef('c_items', $count_ads);
		
		$this->assignRef('keyword', $keyword);
		$this->assignRef('db', $db);
		parent::display($tpl);
		
	}
}
?>
