<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php 
$allowed_types = array('png','jpg','jpeg','gif'); // list of filetypes you want to show

// to remove extensions name
$patterns = array('/\.png/','/\.jpg/','/\.jpeg/','/\.gif/');
$replacements = array('','','','');
		
$imgpath = 'images/singapore/'.$postcode.'/floorplan/'.$roomtype.'/';

if ($handle = opendir($imgpath)) {
	while (false !== ($file = readdir($handle))) {
		if($file == '.' || $file == '..' || $file == 's')
			continue;
				
		if(in_array(strtolower(substr($file,-3)),$allowed_types))
		{
			$imgurlfile 	= $imgpath.''.$file;
			$imgurlfile_s 	= $imgpath.'s/'.$file;
?>		
				
<div style="float:left;padding:5px;width:95px;white-space:nowrap;">
	<a style="color:#062284" href="<?php echo $imgurlfile ?>" rel="rokbox" title="<?php echo $roomtype.' '.$postcode.' :: '.$file ?>" style="cursor:pointer;">
<?php         
			$txtfile = preg_replace($patterns, $replacements, $file);
			if(strlen($txtfile) > 12) {
				$txtfile = substr_replace($txtfile,'..',12);
			}
?>			
		<center><img class="fp_img" src="<?php echo $imgurlfile_s ?>" width="80px" height="60px;" /><br />
		<?php echo $txtfile ?>
        </center>
	</a>
</div>

<?php							
		}
						
	}
}
?>