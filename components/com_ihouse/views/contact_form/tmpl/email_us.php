﻿<?php defined('_JEXEC') or die('Restricted access'); ?>
<div class="popupForm" >
	<div id="t">
    	<div id="l">&nbsp;</div>
        <div id="c">&nbsp;</div>
        <div id="r">&nbsp;</div>
    </div>
    <div style="clear:both;"></div>
    <div id="m">
    	<div id="l">&nbsp;</div>
        <div id="c">
        	<div id="popupFormBox">
            	<div id="padding" >
            		<div id="title">
    					Email to Admin Form   
                    </div>
                    <div style="clear:both"></div>
                    <form id="emailFormContact" action="<?php echo JRoute::_('index.php?option=com_ihouse&task=emailNotExistSubmit'); ?>" method="post">
                    <div id="emailform">
	                    <div id="message-box" class="emailFormBox">
	                    	<div><label>Message</label></div>
	                        <div><textarea name="message" id="textarea-emailformbox" maxlength="300"></textarea></div>
	                    </div>
                        <div class="emailFormBox">
                        	<div style="float:left;">
                        		<input type="button" onclick="javascript:submitEmailFormNotExistPopup();return false;" id="agent_sub" name="agent_sub" class="contactbutton" value="Submit">
                            </div>
                            <div style="float:left;margin-left:10px;">
                            	 <input type="button" onclick="javascript:closePopup('popupForm');" id="agent_sub" name="agent_sub" class="contactbutton" value="Cancel">
                            </div>    
                        </div>               
                    </div> 
                    <input type="hidden" name="subject" value="No Exist List Form"  />
                    <input type="hidden" name="email" value="<?php echo $email ?>"  />
                    <input type="hidden" name="uid" value="<?php echo $user->id ?>"  />
                    <input type="hidden" name="option" value="com_ihouse" />
                    <input type="hidden" name="task" value="emailNotExistSubmit"  /> 
                    </form>
                    <div id="ajax-submit-form-email" style="width:500px;text-align:center;padding-top:150px;display:none;">
                        	<div><img src="<?php echo JRoute::_('templates/main/images/ajax-loader-big.gif'); ?>" /></div>
                            <div style="margin-top:10px;color:#525252;font-size:16px;">Submitting</div>
                    </div>
                    <div style="width:500px;text-align:center;padding-top:150px;">
                    	<div id="text-message-sbm"></div>  	
                    </div>
            	</div>
        	</div>
        </div>
        <div id="r">&nbsp;</div>
    </div>
    <div style="clear:both"></div>
    <div id="b">
    	<div id="l">&nbsp;</div>
        <div id="c">&nbsp;</div>
        <div id="r">&nbsp;</div>
    </div>
    <div style="clear:both"></div>
</div>