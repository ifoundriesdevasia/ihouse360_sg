﻿<?php
/**
* @version		$Id: view.html.php 10711 2008-08-21 10:09:03Z eddieajau $
* @package		Joomla
* @subpackage	Poll
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Poll component
 *
 * @static
 * @package		Joomla
 * @subpackage	Poll
 * @since 1.0
 */
class IhouseViewFloorplanlist extends JView
{
	function display($tpl = null)
	{
		global $mainframe, $option;

		$user		=& 	JFactory::getUser();
		
		$uid		=	$user->get('id');	
		
		$gid		=	$user->get('gid');
		
		$layout		= 	JRequest::getCmd('layout', 'default');
		
		$tmp		= 	JRequest::getVar('display_zone_select', '');
		$postcode	=	'';
		$proptype	=	'';
		
		if($tmp) {
			$tmp		=	explode('|', $tmp);
			$postcode	=	$tmp[0];
			$proptype	=	$tmp[1];
		}
		
		$condo		=	$this->loadPropertyHTML($postcode);	
		$dispzone	=	$this->getDisplayZone($postcode, $proptype);
		$fp_img		=	$this->getFloorplanSmallImage();	

	
		
		$this->assignRef('selecthtml', $condo);
		$this->assignRef('selectdisphtml', $dispzone);
		$this->assignRef('fp_imgs', $fp_img);
		$this->assignRef('db', $db);
		
		parent::display($tpl);
		
	}
	
	function loadPropertyHTML($postcode = '') {
		$db 	  	=& 	JFactory::getDBO();
		
		$query		=	" SELECT id, name_en, name_ch, postcode, floor_plan FROM #__ihouse_property ORDER BY name_en ASC ";
			$db->setQuery( $query );
			$result	=	$db->loadObjectList();
		
		$html	=	'<select id="property_floorplan" name="" onchange="javascript:loadFloorplanListProperty(this.value);" >';
		$html 	.= 	'<option value="">---</option>';
		
		
		
		foreach($result as $r):
			
			$selected = '';
			
			if($r->postcode == $postcode)
				$selected = ' selected="selected" ';
			
			$floorplan_path = JRoute::_('images/singapore/'.$r->postcode.'/floorplan/');
				
			if(file_exists($floorplan_path)) :
				$html	.=	'<option value="'.$r->postcode.'" '.$selected.'>'.$r->name_en.$r->name_ch.'</option>';
			endif;
			
		endforeach;

		$html 	.=	'</select>';
		
		return $html;
	}
	
	function getDisplayZone($postcode = '', $proptype = '') {
		global $mainframe;
		
		$db 	  	=& 	JFactory::getDBO();
		
		$query	= "SELECT * FROM #__ihouse_floorplan ";
			$db->setQuery( $query );
			$floorplan = $db->loadObjectList();

		$query	= "SELECT floor_plan FROM #__ihouse_property WHERE postcode = ".$postcode;
			$db->setQuery( $query );
			$result = $db->loadResult();
	
		$html	=	'';
		$html	.= 	'<select id="display_zone_select" name="display_zone_select" onchange="javascript:loadFloorplanImages(this.value);">';
		$html 	.= 	'<option value="">---</option>';
		
		foreach($floorplan as $fp) :
			
			$floorplan_path = JRoute::_('images/singapore/'.$postcode.'/floorplan/'.$fp->type_en);
			
			if(file_exists($floorplan_path)) :
				$selected = '';
				if($fp->type_en == $proptype)
					$selected = ' selected="selected" ';
				
				$html .= '<option value="'.$postcode.'|'.$fp->type_en.'" '.$selected.'>'.$fp->type_ch.'</option>';
			endif;	
			
		endforeach;
		
		$html	.= 	'</select>';
		return $html;		
	}
	
	function getFloorplanSmallImage() {
		$html_tabs = '';
		$html = ''; 
		
		$value			=	JRequest::getVar('display_zone_select', ''); /* postcode | proptype */

		$tmp		=	explode('|', $value);
		$postcode	=	$tmp[0];
		$proptype	=	$tmp[1];
		
		
		if(empty($postcode))
			return false;
		
		$db 	  	=& 	JFactory::getDBO();
		
		$query	= " SELECT floor_plan FROM #__ihouse_property WHERE postcode = ".$postcode;
			$db->setQuery( $query );
			$result = $db->loadResult();
	
		$room_type_ch = str_replace('|',',',$result);
			
		$pic = array();
		
		$floorplan_path = JRoute::_('images/singapore/'.$postcode.'/floorplan/'.$proptype);
		
		$allowed_types = array('png','jpg','jpeg','gif'); // list of filetypes you want to show
		
		$html .= '<div class="displFloorPlan" id="'.$proptype.'_'.$postcode.'" >';
		
		if ($handle = opendir($floorplan_path)) {
			
			while (false !== ($file = readdir($handle))) {
				if($file == '.' || $file == '..' || $file == 's')
					continue;
				
				if(in_array(strtolower(substr($file,-3)),$allowed_types))
				{
					if($i == 0) {
						$html .= '<div style="float:left;">';
					}
						
					$imgurlfile 	= $floorplan_path.'/'.$file;
					$imgurlfile_s 	= 	$floorplan_path.'/s/'.$file;
					
					$html .= '<div style="float:left;padding:5px;width:95px;word-wrap: break-word; ">';
					$html .= '<a style="color:#062284" href="'.$imgurlfile.'" rel="rokbox" onclick="javascript:loadChosenFloorplan(\''.$imgurlfile.'\');" title="'.$proptype.' '.$postcode.' :: '.$file.'" style="cursor:pointer;">';
					$html .= '<center><img class="fp_img" src="'.$imgurlfile_s.'" width="80px" height="60px;" /><br />';
					$html .= $file.'</center>';
					$html .= '</a>';
					$html .= '</div>';
						
					$i++;
					if($i == 6) {
						$i = 0;
						$html .= '</div><div style="clear:both;"></div>';
					}
				}
			}
		}
					
		$html .= '<div style="clear:both"></div>';
		$html .= '</div>';
		
		return $html;
	}
}
?>
