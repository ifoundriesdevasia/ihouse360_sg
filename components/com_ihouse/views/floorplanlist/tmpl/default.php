﻿<?php defined('_JEXEC') or die('Restricted access'); ?>
<?php JHTML::_('behavior.tooltip'); ?>

<?php JHTML::_('script', 'rokbox.js', 'components/com_ihouse/assets/rokbox/'); ?>
<?php JHTML::_('script', 'rokbox-config.js', 'components/com_ihouse/assets/rokbox/themes/light/'); ?>
<?php JHTML::stylesheet('rokbox-style.css','components/com_ihouse/assets/rokbox/themes/light/', array('media'=>'all')); ?>

<script type="text/javascript">
function loadFloorplanListProperty(postcode) {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();
	var url = '<?php echo JRoute::_("index.php?option=com_ihouse&task=loadDisplayZoneFloorplanProperty"); ?>&amp;r=' + unixtime_ms;
	$('loadFloorplanSmallImagesProperty').setHTML('');
	$('load_display_zone_loader').setStyle('display','');
	$('load_display_zone').setHTML('<select></select>');
	var req = new Ajax(url, {
	   			data		: 	
							{
								'postcode' : postcode
							},
				method		: "get",
		    	onSuccess	: function(data) {
					
					var obj1 		= 	Json.evaluate(data);
					var list_html	=	obj1.list_html;
					
					
					$('load_display_zone_loader').setStyle('display','none');
					$('load_display_zone').setHTML(list_html);
				},
				evalScripts: true
	}).request();	
}

function loadFloorplanImages(value) {
	document.loadFloorplanForm.submit();
}

function loadChosenFloorplan(urlimage){
	
}
//$('property_floorplan').setProperty('value','');
</script>

<div style="background-color:#fff;width:100%;">
	<div id="search_detail_title" style="">
		<strong>户型图</strong>&nbsp;&nbsp;<i>Floorplan</i>
	</div>

	<div style="margin:5px 10px;">
		<div>Project Name : </div>
    	<div  style="margin-top:10px;"><?php echo $this->selecthtml ?></div>
    	<div style="clear:both;"></div> 
	</div>

	<div style="margin:5px 10px;">
		<div style="float:left;">Display Zone :  </div>
	    <div id="load_display_zone_loader" style="float:left;margin-left:10px;display:none;"><img src="<?php echo JRoute::_('templates/main/images/ajax-loader.gif') ?>" /></div>
	    <div style="clear:both;"></div>
        
        <form id="loadFloorplanForm" name="loadFloorplanForm"  action="<?php echo JRoute::_('index.php?option=com_ihouse&task=getFloorPlanImages') ?>"> 
	    <div id="load_display_zone" style="margin-top:10px;float:left;"><?php echo $this->selectdisphtml ?></div>
        <input type="hidden" name="option" value="com_ihouse"  />
        <input type="hidden" name="task"	value="getFloorPlanImages"  />
        </form>
        
	    <div style="clear:both;"></div>
	</div>   
	
	<div style="margin:5px 10px;">    
		<div style="float:left;">Floorplan : </div>
		<div id="load_fp_loader" style="float:left;margin-left:10px;display:none;"><img src="<?php echo JRoute::_('templates/main/images/ajax-loader.gif') ?>" /></div>
	    <div style="clear:both;"></div> 
		<div id="loadFloorplanSmallImagesProperty" style="margin-top:5px;float:left;"><?php echo $this->fp_imgs ?></div>
	    <div style="clear:both;"></div>
	</div>

</div>


<!-- POPUP -->
<div id="popupForm" style="display:none;">
</div>    
    