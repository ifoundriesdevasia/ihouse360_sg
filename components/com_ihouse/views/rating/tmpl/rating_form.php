﻿<?php defined('_JEXEC') or die('Restricted access'); ?>
Please fill the user rating form. 
<form name="userRatingForm" id="userRatingForm" action="<?php echo JRoute::_('index.php?option=com_ihouse&task=userratingsubmit'); ?>" method="post">
<div>
    <div class="review-cols2" style="border:none;">
    	<div class="review_padding1">
    	<div class="review_label">交通</div>
		<div style="float:left;margin-left:10px;">
        <input type="radio" name="trans" value="1" />1
        <input type="radio" name="trans" value="2" />2
        <input type="radio" name="trans" value="3" />3
        <input type="radio" name="trans" value="4" />4
        <input type="radio" name="trans" value="5" />5
        </div>
        <div style="clear:both;"></div>
        </div>
        
        <div class="review_padding1">
		<div class="review_label">周边环境</div>
		<div style="float:left;margin-left:10px;">
        <input type="radio" name="environment" value="1" />1
        <input type="radio" name="environment" value="2" />2
        <input type="radio" name="environment" value="3" />3
        <input type="radio" name="environment" value="4" />4
        <input type="radio" name="environment" value="5" />5
        </div>
        <div style="clear:both;"></div>
        </div>
        
        <div class="review_padding1">
		<div class="review_label">餐饮购物</div>
		<div style="float:left;margin-left:10px;">
        <input type="radio" name="design" value="1" />1
        <input type="radio" name="design" value="2" />2
        <input type="radio" name="design" value="3" />3
        <input type="radio" name="design" value="4" />4
        <input type="radio" name="design" value="5" />5
        </div>
        <div style="clear:both;"></div>
        </div>
        
        <div class="review_padding1">
		<div class="review_label">内部设计</div>
		<div style="float:left;margin-left:10px;">
        <input type="radio" name="facility" value="1" />1
        <input type="radio" name="facility" value="2" />2
        <input type="radio" name="facility" value="3" />3
        <input type="radio" name="facility" value="4" />4
        <input type="radio" name="facility" value="5" />5
        </div>
        <div style="clear:both;"></div>
        </div>
        
        <div class="review_padding1">
		<div class="review_label">小区设施</div>
        <div style="float:left;margin-left:10px;">
        <input type="radio" name="developer" value="1" />1
        <input type="radio" name="developer" value="2" />2
        <input type="radio" name="developer" value="3" />3
        <input type="radio" name="developer" value="4" />4
        <input type="radio" name="developer" value="5" />5
        </div>
        
        <div style="clear:both;"></div>
        </div>
    </div>
</div>
<div style="clear:both"></div>
<div class="rating-button">
    <input type="hidden" name="property_id" value="<?php echo $property_id ?>" />
    <input type="hidden" name="option" value="com_ihouse" />
    <input type="hidden" name="task" value="userratingsubmit" />
    <input type="submit" class="enter_button" value="提交" onclick="javascript:submitRatingForm();return false;">
    <input type="submit" class="enter_button" value="Back" onclick="javascript:loadUserRatingCurrent();return false;">
</div>
</form>