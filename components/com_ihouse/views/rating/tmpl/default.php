﻿<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('behavior.tooltip'); ?>
<?php 
	$user 	= 	JFactory::getUser(); 
	$uid	=	$user->id;
?>
<?php if(!$uid) : ?>
<div style="float:left;color:red;margin-top:-10px;">
You must log in to rate
</div>
<div style="clear:both"></div>
<?php endif; ?>

<div style="float:left;">
	<div style="float:left;font-size:15px;">
		网友评分
	</div>
    <div style="float:left;margin-left:10px;">
		<div id="static-text" style="font-size:23px;font-weight:normal;"><?php echo sprintf("%01.1f",$rating->overall_avg / 5 * 100) ?>%</div>
        <div style="clear:both"></div>
        <div style="font-size:14px;">（<?php echo (!empty($rating->total)?$rating->total:'0');?>人参加）</div>
	</div>
</div>
<div style="clear:both"></div>
<div>
	<div class="review-cols1">
    	<div class="review_padding1">
    	<div class="review_label">交通</div>
        <?php  ?>
		<div style="float:left;margin-left:10px;">
        	<div style="background-color:#f2f2f2;height:14px;width:120px;">
            	<div style="width:<?php echo $transport_avg ?>%;background-color:#ff8200;">&nbsp;</div>
            </div>
        </div>
        <div class="review_percent1"><?php echo $transport_avg ?>%</div>
        <div style="clear:both"></div>
        </div>
        
        <div class="review_padding1">
		<div class="review_label">周边环境</div>
        <?php  ?>
		<div style="float:left;margin-left:10px;">
        	<div style="background-color:#f2f2f2;height:14px;width:120px;">
            	<div style="width:<?php echo $env_avg ?>%;background-color:#ff8200;">&nbsp;</div>
            </div>
        </div>
        <div class="review_percent1"><?php echo $env_avg ?>%</div>
        <div style="clear:both"></div>
        </div>
        
        <div class="review_padding1">
		<div class="review_label">餐饮购物</div>
        <?php  ?>
		<div style="float:left;margin-left:10px;">
        	<div style="background-color:#f2f2f2;height:14px;width:120px;">
            	<div style="width:<?php echo $design_avg ?>%;background-color:#ff8200;">&nbsp;</div>
            </div>
        </div>
        <div class="review_percent1"><?php echo $design_avg ?>%</div>
        <div style="clear:both"></div>
        </div>
        
        <div class="review_padding1">
		<div class="review_label">内部设计</div>
		<div style="float:left;margin-left:10px;">
        	<div style="background-color:#f2f2f2;height:14px;width:120px;">
            	<div style="width:<?php echo $fac_avg ?>%;background-color:#ff8200;">&nbsp;</div>
            </div>
        </div>
        <div class="review_percent1"><?php echo $fac_avg ?>%</div>
        <div style="clear:both"></div>
        </div>
        
        <div class="review_padding1">
		<div class="review_label">小区设施</div>
        <div style="float:left;margin-left:10px;">
        	<div style="background-color:#f2f2f2;height:14px;width:120px;">
            	<div style="width:<?php echo $developer_avg ?>%;background-color:#ff8200;">&nbsp;</div>
            </div>
        </div>
        <div class="review_percent1"><?php echo $developer_avg ?>%</div>
        <div style="clear:both"></div>
        </div>
        
    </div>
    <div class="review-cols2">
    	<div class="review_padding1">
    	<div class="review_label">交通</div>
		<div style="float:left;margin-left:10px;">
       	<?php echo $star_transport ?>
        </div>
        <div style="clear:both;"></div>
        </div>
        
        <div class="review_padding1">
		<div class="review_label">周边环境</div>
		<div style="float:left;margin-left:10px;">
        <?php echo $star_env ?>
        </div>
        <div style="clear:both;"></div>
        </div>
        
        <div class="review_padding1">
		<div class="review_label">餐饮购物</div>
		<div style="float:left;margin-left:10px;">
        <?php echo $star_design ?>
        </div>
        <div style="clear:both;"></div>
        </div>
        
        <div class="review_padding1">
		<div class="review_label">内部设计</div>
		<div style="float:left;margin-left:10px;">
        <?php echo $star_fac ?>
        </div>
        <div style="clear:both;"></div>
        </div>
        
        <div class="review_padding1">
		<div class="review_label">小区设施</div>
        <div style="float:left;margin-left:10px;">
        <?php echo $star_developer ?>
        </div>
        <?php if($uid > 0) { ?>
        <div class="review-button" style="">
        	<input type="submit" class="enter_button" value="我来评分" onclick="javascript:loadUserRatingForm();">
        </div>
        <?php } ?>
        <div style="clear:both;"></div>
        </div>
    </div>
    <div style="float:left;margin-left:10px;">
    	<div class="guide_title1">评分准则</div>
        <div style="clear:both;"></div>
        <div class="guide_padding1">
        	<div class="review_guideline">优秀</div>
            <div style="float:left;margin-left:10px;"><img src="<?php echo JURI::root().'templates/main/images/excellent.png'; ?>" /></div>
		</div>
        <div style="clear:both;"></div>
        <div class="guide_padding1">
			<div class="review_guideline">好</div>
            <div style="float:left;margin-left:10px;"><img src="<?php echo JURI::root().'templates/main/images/good.png'; ?>" /></div>
		</div>
        <div style="clear:both;"></div>
        <div class="guide_padding1">
			<div class="review_guideline">平均</div>
            <div style="float:left;margin-left:10px;"><img src="<?php echo JURI::root().'templates/main/images/average.png'; ?>" /></div>
		</div>
        <div style="clear:both;"></div>
        <div class="guide_padding1">
			<div class="review_guideline">一般</div>
            <div style="float:left;margin-left:10px;"><img src="<?php echo JURI::root().'templates/main/images/fair.png'; ?>" /></div> 
		</div>
        <div style="clear:both;"></div>
        <div class="guide_padding1">
			<div class="review_guideline">差劲</div>
        	<div style="float:left;margin-left:10px;"><img src="<?php echo JURI::root().'templates/main/images/poor.png'; ?>" /></div> 
        </div>
        <div style="clear:both;"></div>
    </div>
</div>