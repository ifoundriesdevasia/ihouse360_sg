<?php defined('_JEXEC') or die('Restricted access'); ?>
<?php $db = JFactory::getDBO(); ?>
<div class="tabItem1">
	<div id="tabcont-sub" style="padding-bottom:10px;">
    
    <?php if(empty($rows)) : ?>
	No Ads
	<?php else : ?>
	<?php foreach($rows as $row): ?>
		<div style="width: 100%; float: left;margin:5px 0 0 0;" class="content-border">
		<div id="tabtext-padding1">
                	<a href="<?php echo JRoute::_('index.php?option=com_ads&view=adsdetails&id='.$row->id); ?>">
						<?php echo $row->ad_title; ?>
                    </a>
                </div>
			<div id="tabimg">
    	<?php 
		$query = " SELECT ads_id, sess_id, name FROM #__ihouse_ads_image WHERE ads_id = '".$row->id."' AND is_primary = 1 ";
			$db->setQuery( $query );
			$tmps = $db->loadObject();
			
		if(empty($tmps)) {
			$query = " SELECT image FROM #__ihouse_ads_image2 WHERE ads_id = '".$row->id."' ORDER BY RAND() LIMIT 1 ";
			$db->setQuery( $query );
			$ihouse_img = $db->loadResult();
			
			if(empty($ihouse_img)) {
				$ihouse_img = JRoute::_('templates/main/images/thumb-noimage80.jpg');
			}else{
				$ihouse_img = 'image.php?size=80&type=3&path='.$row->postcode.'/images/'.$ihouse_img;
			}
		}	
		?>
			<div class="sidebaradsimgbg" style="height: 70px;">
     		<a href="<?php echo JRoute::_('index.php?option=com_ads&view=adsdetails&id='.$row->id); ?>">
    		
			<?php if(!empty($tmps->name)) : ?>
    
     		<img src="<?php echo JRoute::_('images/ihouse/ads_images/'.$tmps->sess_id.'/small-'.$tmps->name) ?>" border=0 width="80px" height="60px"/>
    		
			<?php elseif(!empty($ihouse_img)) : ?>
    
    		<img style="border:none;" src="<?php echo $ihouse_img ?>" />
			
			<?php else : ?>
    
    		<img src="<?php echo JRoute::_('templates/main/images/thumb-noimage80.jpg') ?>" border=0 />
    
    		<?php endif; ?>
    		</a>
			</div>
            </div>
            <div id="tabtext-cont1">
            	
                <div id="tabtext-font">
                <!--地址 :&nbsp;<?php echo ($row->street_name)?$row->street_name:'N/A' ?><br />-->
				邮区：<?php echo ($row->property_district_id)?$row->property_district_id:'N/A'; ?><br>
                户型：<?php echo $row->no_of_room ?>房<?php echo $row->no_of_hall ?>厅<br />
                楼层：<?php echo ($row->floor_level)?$row->floor_level.'层':'N/A';?><br />
                价格：<?php echo ($row->ask_price)?'S$ '.number_format($row->ask_price):'N/A' ?>
                </div>
            </div>
                                
        </div>
       	<div style="clear:both;"></div>
	<?php endforeach; ?>
    <?php endif; ?>
    </div>
    <div style="clear:both"></div>
    <div style="float:right;margin-top:5px;">
    	<a class="see_more" href="<?php echo JRoute::_('index.php?option=com_ihouse&adtype=4&task=adtypelisting&type='.$adtype)?>">
        	显示更多 >
    	</a>
    </div>    
</div>