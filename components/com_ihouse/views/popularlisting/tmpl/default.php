﻿<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('behavior.tooltip'); ?>
<?php $db = $this->db ?>

<div style="background-color:#fff;width:100%;float:left;margin-top:10px;">

<div id="search_detail_title">
	<strong>本周热门公寓</strong>&nbsp;&nbsp;<i>Popular Condos This Week</i>
</div>

<div style="padding:10px;">
<table class="table-border-ihouse" width="100%" cellspacing=0 cellpadding=0 >
	<tr align="center" style="">
		<th width="40%" align="left" style="padding:10px;"><span style="color:#FF8200">项目名</span></th>
		<th width="20%"><span style="color:#FF8200">户型</span></th>
		<th width="10%"><span style="color:#FF8200">邮区</span></th>
		<th width="10%"><span style="color:#FF8200">面积</span></th>
    	<th width="10%"><span style="color:#FF8200">价格(SGD)</span></th>
        <th width="10%"><span style="color:#FF8200">浏览人数</span></th>
	</tr>
	<?php foreach($this->items as $p) : ?>
    <tr align="center">
	<td id="tabtext-padding1" align="left" style="padding:10px;">
		<a href="<?php echo JRoute::_('index.php?option=com_ihouse&view=ihouse&layout=property_detail&id='.$p->property_id.'&postcode='.$p->postcode); ?>">
		<?php echo (!empty($p->name_en))?$p->name_en:'&nbsp;' ?>
		<?php echo (!empty($p->name_ch))?'<br />('.$p->name_ch.')' : '' ; ?>
		</a>
    </td>
    <td>
	<?php 
	if(!empty($p->floor_plan)) : 
		$c = explode('|',$p->floor_plan);
		echo implode(',', $c); 
	else :
		echo '-';
	endif;
	?>
    </td>
    <td><?php echo ($p->district_id)?$p->district_id:'-'; ?></td>
    <td><?php echo ($p->room_size)?$p->room_size:'-'; ?></td>
    <td><?php 
	$projectname	=	 preg_replace('/(\s|\.|\')+/','-',strtolower($p->name_en));
	$query = " SELECT price FROM #__ihouse_property_price WHERE project_name = '".$projectname."' ";
			$db->setQuery( $query );
			$price = $db->loadResult();
			echo ($price)?number_format($price):'-';
			?></td>
    <td>
    <?php echo ($p->counter)?$p->counter:'0'; ?>
    </td>        
    </tr>
    <?php endforeach; ?>
</table> 
</div>

<div class="pagination_box" style="float:right;">
<?php if(!$this->top) { ?>
<?php echo $this->currentpages ?> of <?php echo $this->pagestotal ?> pages
<?php } ?>
</div>
<div class="pagination_box" style="float:right;">
<?php echo $this->pagination->getPagesLinks() ?>
</div>

</div>