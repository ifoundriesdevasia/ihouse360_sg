﻿<?php
/**
* @version		$Id: view.html.php 10711 2008-08-21 10:09:03Z eddieajau $
* @package		Joomla
* @subpackage	Poll
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Poll component
 *
 * @static
 * @package		Joomla
 * @subpackage	Poll
 * @since 1.0
 */
class IhouseViewPopularlisting extends JView
{
	function display($tpl = null)
	{
		global $mainframe, $option;

		$user		=& 	JFactory::getUser();
		
		$uid		=	$user->get('id');	
		
		$adtype		=	JRequest::getCmd('adtype', 0);
		
		$layout		= 	JRequest::getCmd('layout', 'default');
		
		$db 	  	=& 	JFactory::getDBO();
		
		$limitstart =	JRequest::getCmd('limitstart', 0);
		$limit		=	JRequest::getCmd('limit', 30);
		
		
		
		$top		=	JRequest::getCmd('top', 0); // Top 'X' List
		
		if($top) {
			$top_limit = ' LIMIT ' . $top;
		} else {
			$sql_limit	=	" LIMIT ".$limitstart.",".$limit." "; 
		}
		$where		=	array();
		
		$where[]	=	" a.type = 'property' ";
		
		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
		
		if($top){
			$total = $top;
		} else {
			$query	=	'SELECT COUNT(*) FROM #__ihouse_popular AS a  ' 
						.	" LEFT JOIN #__ihouse_property AS p ON p.id = a.property_id "
						.	$where
					;
				$db->setQuery( $query );
				$total			= $db->loadResult();
		}
		$orderby	=	' ORDER BY a.counter DESC ';
		
		$query	=	' SELECT * FROM #__ihouse_popular AS a  ' 
						.	" LEFT JOIN #__ihouse_property AS p ON p.id = a.property_id "
						.	$where
						.	$orderby
						.	$sql_limit
						.	$top_limit
					;
			$db->setQuery( $query );
			$rows		= $db->loadObjectList();
		
		if ($db->getErrorNum())
		{
			echo $db->stderr();
			return false;
		}

		jimport('joomla.html.pagination');
		$pagination = new JPagination( $total, $limitstart, $limit);	
	
		$pagestotal = ceil($total / $limit);
		$currentpages = ($limitstart / $limit) + 1;
	
		$this->assignRef('pagestotal', 	$pagestotal);
		$this->assignRef('currentpages', $currentpages);
	
		$this->assignRef('items', $rows);
		$this->assignRef('total', $total);
		
		$this->assignRef('pagination',	$pagination);
		$this->assignRef('top', $top);
		$this->assignRef('db', $db);
		parent::display($tpl);
		
	}
}
?>
