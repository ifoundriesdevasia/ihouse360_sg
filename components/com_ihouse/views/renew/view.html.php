﻿<?php
/**
* @version		$Id: view.html.php 10711 2008-08-21 10:09:03Z eddieajau $
* @package		Joomla
* @subpackage	Poll
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Poll component
 *
 * @static
 * @package		Joomla
 * @subpackage	Poll
 * @since 1.0
 */
class IhouseViewRenew extends JView
{
	function display($tpl = null)
	{
		global $mainframe;

		//$user		=& 	JFactory::getUser();	
		//$uid		=	$user->get('id');	
		
		$subs_id	= 	JRequest::getVar('aid', '');
		$passl		=	JRequest::getVar('passl', '');
		
		$layout		= 	JRequest::getVar('layout');
		
		$db 	  	=& 	JFactory::getDBO();
		
		if($layout == 'default') {
			
			$ads		=	sha1(md5('ads'));
			$adtype		=	sha1(md5('adtype'));
			$account	=	sha1(md5('account'));
	
			$body	=	'You are going to renew ';

			if($passl == $ads) {
				$query = " SELECT a.ad_title, cp.name, cp.rate, cb.user_id FROM #__ihouse_ads AS a "
						.	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = a.subscription_id "
						.	" LEFT JOIN #__cbsubs_plans AS cp ON cp.id = cb.plan_id "
						.	" WHERE a.subscription_id = '$subs_id' "
						.	" LIMIT 1 ";
				
					$db->setQuery( $query );
					$row	=	$db->loadObject();
			
				$userid	=	$row->user_id;
			
				$query	=	" SELECT * FROM #__ihouse_users_credit WHERE user_id = '$userid' ";
					$db->setQuery( $query );
					$users	=	$db->loadObject();
			
				$after_deducted	=	(int) $users->credit - (int) $row->rate;
			
				$body	.=	'"<strong>'.$row->ad_title.'</strong>" as "<strong>'.$row->name.'</strong>" <br /> ';
				$body	.=	'Your account point will be deducted for <strong>' . (int) $row->rate . '</strong> points <br /><br />';
				$body	.=	'Your current point is <strong>'.$users->credit.'</strong> <br />';
				$body	.=	'After deducted is <strong>'.$after_deducted.'</strong> <br />';
			
			} else if($passl == $adtype) {
				
				$query = " SELECT a.ads_type_config,a.ads_id,a.property_id, cp.name, cp.rate, cb.user_id FROM #__ihouse_ads_type AS a "
						.	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = a.subscription_id "
						.	" LEFT JOIN #__cbsubs_plans AS cp ON cp.id = cb.plan_id "
						.	" WHERE a.subscription_id = '$subs_id' "
						.	" LIMIT 1 ";
				
					$db->setQuery( $query );
					$row	=	$db->loadObject();
			
				$userid		=	$row->user_id;
				$adtype		=	(int) $row->ads_type_config;
				$adsid		=	$row->ads_id;
				$propertyid = 	$row->property_id;
				
				$type	=	'';
				
				switch($adtype) {
					case 1:
						$type = 'Type 1: Expert For The Condo';
						
						$query = " SELECT name_en,name_ch FROM #__ihouse_property WHERE id = '$propertyid' ";
							$db->setQuery( $query );
							$tmp	=	$db->loadObject();
							
						$nm	=	$tmp->name_en.$tmp->name_ch;
							
						break;
					
					case 2:
						$type = 'Type 2: Recommended Specialist';
						
						$query = " SELECT name_en,name_ch FROM #__ihouse_property WHERE id = '$propertyid' ";
							$db->setQuery( $query );
							$tmp	=	$db->loadObject();
							
						$nm	=	$tmp->name_en.$tmp->name_ch;
							
						break;
						
					case 3:
						$type = 'Type 3: Condo Expert';
						
						$nm		=	'Condo Directory page';
						break;
						
					case 4:
						$type = 'Type 4: Featured Listing at Side-bar';
						
						$query = " SELECT ad_title FROM #__ihouse_ads WHERE id = '$adsid' ";
							$db->setQuery( $query );
							$tmp	=	$db->loadObject();
							
						$nm	=	$tmp->ad_title;
						break;
						
					case 5:
						$type = 'Type 5: Featured Listing at Specific Condo';
						
						$query = " SELECT ad_title FROM #__ihouse_ads WHERE id = '$adsid' ";
							$db->setQuery( $query );
							$tmp	=	$db->loadObject();
							
						$nm	=	$tmp->ad_title;
						
						break;
						
					case 6:
						$type = 'Type 6: Featured Listing';
						$query = " SELECT ad_title FROM #__ihouse_ads WHERE id = '$adsid' ";
							$db->setQuery( $query );
							$tmp	=	$db->loadObject();
							
						$nm	=	$tmp->ad_title;
						break;
						
					case 7:
						$type = 'Type 7: New Condo for Sell';
						$query = " SELECT ad_title FROM #__ihouse_ads WHERE id = '$adsid' ";
							$db->setQuery( $query );
							$tmp	=	$db->loadObject();
							
						$nm	=	$tmp->ad_title;
						break;
						
				}
				
				$query	=	" SELECT * FROM #__ihouse_users_credit WHERE user_id = '$userid' ";
					$db->setQuery( $query );
					$users	=	$db->loadObject();
			
				$after_deducted	=	(int) $users->credit - (int) $row->rate;
				
				$body	.=	'"<strong>'.$type.'</strong>" on "<strong>'.$nm.'</strong>" <br /> ';
				$body	.=	'Your account point will be deducted for <strong>' . (int) $row->rate . '</strong> points <br /><br />';
				$body	.=	'Your current point is <strong>'.$users->credit.'</strong> <br />';
				$body	.=	'After deducted is <strong>'.$after_deducted.'</strong> <br />';
			
			} else { /*  Account Subscription */

				$query = " SELECT cp.name, cp.rate, cb.user_id " 
						.	" FROM #__cbsubs_subscriptions AS cb "
						.	" LEFT JOIN #__cbsubs_plans AS cp ON cp.id = cb.plan_id "
						.	" WHERE cb.id = '$subs_id' "
						.	" LIMIT 1 ";
				
					$db->setQuery( $query );
					$row	=	$db->loadObject();
			
				$userid		=	$row->user_id;

				$query	=	" SELECT * FROM #__ihouse_users_credit WHERE user_id = '$userid' ";
					$db->setQuery( $query );
					$users	=	$db->loadObject();
			
				$after_deducted	=	(int) $users->credit - (int) $row->rate;
				
				$body	.=	'"<strong>'.$row->name.'</strong>" on your account <br /> ';
				$body	.=	'Your account point will be deducted for <strong>' . (int) $row->rate . '</strong> points <br /><br />';
				$body	.=	'Your current point is <strong>'.$users->credit.'</strong> <br />';
				$body	.=	'After deducted is <strong>'.$after_deducted.'</strong> <br />';
			}
			
			$this->assignRef('body', $body);
			$this->assignRef('subs_id', $subs_id);
			
		} else {
			
		}
		$this->assignRef('db', $db);
		parent::display($tpl);
	}
}
?>
