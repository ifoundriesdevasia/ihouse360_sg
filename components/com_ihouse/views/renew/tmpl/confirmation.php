﻿<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('behavior.tooltip'); ?>
<?php //JHTML::_('script', 'contacts.js', 'components/com_ihouse/assets/'); ?> 

<div class="component-pos" style="float:left;width:100%;">
	<div style="background-color: rgb(255, 255, 255);" id="sub-small-component-border">
    	<div id="sub-small-component">
        	<div id="component-headerpos">
            	<div id="component-logo">
                	<div id="component-title"><span class="chinese_text1"></span>&nbsp;<i>Renewal Confirmation</i></div>
                </div>
            </div>
        </div>
        <div style="clear:both"></div>
        <div id="sub-small-component-content">
        	<div style="margin:10px;font-size:14px;">
            	Dear ihouse360.com Customer, <br  /><br  />
                Your renewal subscription has been extended. <br  />
            
				Please click here to go back to <a href="<?php echo JURI::root() ?>">HOME</a>
            </div>
            
		</div>          
	</div>
</div>
