﻿<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('behavior.tooltip'); ?>
<?php //JHTML::_('script', 'contacts.js', 'components/com_ihouse/assets/'); ?> 

<div class="component-pos" style="float:left;width:100%;">
	<div style="background-color: rgb(255, 255, 255);" id="sub-small-component-border">
    	<div id="sub-small-component">
        	<div id="component-headerpos">
            	<div id="component-logo">
                	<div id="component-title"><span class="chinese_text1"></span>&nbsp;<i>Renewal Subscription</i></div>
                </div>
            </div>
        </div>
        <div style="clear:both"></div>
        <div id="sub-small-component-content">
        	<div style="margin:10px;font-size:14px;">
				<div><?php echo $this->body ?></div>
                <div style="margin:20px 0;text-align:center;">
                	<form name="renewConfirmationForm" action="<?php echo JRoute::_('index.php?option=com_ihouse&task=renewConfirmation') ?>">
            		<input type="submit" class="enter_button" value="Confirm">
                    <input type="hidden" name="option" value="com_ihouse" />
                    <input type="hidden" name="task" value="renew"  />
                    <input type="hidden" name="confirmation" value="1"  />
                    <input type="hidden" name="subs_id" value="<?php echo $this->subs_id ?>"  />
                    </form>
            	</div>
            </div>
            
		</div>          
	</div>
</div>
