﻿<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('behavior.tooltip'); ?>
<?php JHTML::_('script', 'contacts.js', 'components/com_ihouse/assets/'); ?> 

<?php $property_cls = JRequest::getVar('property_cls', 'all') ?>
<script type="text/javascript">
function getAds(page, value, filter, orderfilter, othertype) {
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();			
	
	var myArray = [];
	var arrTmp = '<?php echo serialize(JRequest::getVar('district_sector', array())) ?>';
	var hdbtown = '<?php echo serialize(JRequest::getVar('hdb_town', array())) ?>';
	
	var url = 'index.php?option=com_ihouse&task=ajaxLoadAdsSearch&r=' + unixtime_ms;		
	var req = new Ajax(url, {
	   					data		: 	
									{
										'user_category'		: value,
										'property_cls'		: '<?php echo JRequest::getVar('property_cls', 'all') ?>',
										'ticked_fp'			: '<?php echo JRequest::getVar('with_floorplan', ''); ?>',
										'district_sector' 	: arrTmp,
										'keyword'			: '<?php echo JRequest::getVar('keyword', ''); ?>',
										't_hdb' 			: '<?php echo JRequest::getVar('t_hdb', '') ?>',
										't_condo' 			: '<?php echo JRequest::getVar('t_condo', '') ?>',
										'prices' 			: '<?php echo JRequest::getVar('prices','') ?>',
										'prices_max' 		: '<?php echo JRequest::getVar('prices_max','') ?>',
										'prices_h' 			: '<?php echo JRequest::getVar('prices_h','') ?>',
										'prices_h_max' 		: '<?php echo JRequest::getVar('prices_h_max','') ?>',
										'prices_rent'		: '<?php echo JRequest::getVar('prices_rent','') ?>',
										'prices_rent_max'	: '<?php echo JRequest::getVar('prices_rent_max','') ?>',
										'bedroom' 			: '<?php echo JRequest::getVar('bedroom', '') ?>',
										'bedroom_h' 		: '<?php echo JRequest::getVar('bedroom_h', '') ?>',
										'prop_type' 		: '<?php echo JRequest::getVar('prop_type',2) ?>',
										'amount' 			: '<?php echo JRequest::getVar('amount', 0) ?>',
										'ads_adv_search'	: '<?php echo JRequest::getVar('ads_adv_search') ?>',
										'hdb_town'			: hdbtown,
										'ptype'				: othertype, 
										'from'				: '<?php echo $this->from ?>',
										'property_id'		: '<?php echo $this->property_id ?>',
										'flattype'			: '<?php echo JRequest::getVar('flattype','') ?>',
										'filter'			: filter,
										'orderfilter'		: orderfilter,
										'page'				: page
										
									},
						method		: "get",
		    			onSuccess	: function(data) {
							$('ajax-adssearch-' + value).setHTML(data);
		   				},
						evalScripts : true
				}).request();				
}

function AdsRetrieveSearch(usercat){
	var foo = new Date; // Generic JS date object
	var unixtime_ms = foo.getTime();			
	
	var url = 'index.php?option=com_ihouse&task=loopBackSession&r=' + unixtime_ms;
	var req = new Ajax(url, {
	   					data		: 	
									{
										'usercat' : usercat
									},
						method		: "post",
		    			onSuccess	: function(data) {
							var obj1 		= 	Json.evaluate(data);
							
							var page		=	obj1.page;
							var orderfilter	=	obj1.orderfilter;
							var filter		=	obj1.filter;
							
							$('test123').setHTML(page + ' ' + usercat + ' ' + filter + ' ' + orderfilter);
							getAds(page,usercat,filter,orderfilter,'');
		   				},
						evalScripts : true
				}).request();							
}

AdsRetrieveSearch('owner');
AdsRetrieveSearch('agent');


</script>
<div style="background-color:#fff;width:100%;">

   
<div id="test123" style="display:none;"></div>   
<!--Start-->
<div style="float:left; width:690px;">
<div style="width:690px; float:left; background-color:#FFFFFF; border:solid 1px #e2e9fe;">
<div id="ads-module1">
    <div id="module-headerpos">
    	<div id="module-title">
    	<?php if($property_cls == 'rent') : ?>
            经纪出租房源 Featured Properties by Agent
        <?php else : ?>
             经纪房源 Featured Properties by Agent
        <?php endif; ?>    
        </div>
    </div>
   
</div>

<div style="float:left; width:100%;"><!--Start of 2nd module-->
	<div style="float:left; width:680px; padding-bottom:10px;"> <!--Start of content-->
		<div id="ajax-adssearch-agent">
                    
		</div>
                    
	</div>
</div><!--End of content-->
</div><!--End of 2nd module-->

</div>
</div>
<!--End-->       
</div>
<!--End-->


<!--Start-->
<div style="float:left; width:690px;">
<div style="width:690px; float:left; background-color:#FFFFFF; border:solid 1px #e2e9fe;">

<div id="ads-module1">
    <div id="module-headerpos">
        <div id="module-title">
        <?php if($property_cls == 'rent') : ?>
            屋主出租房源 Featured Properties by Owner
        <?php else : ?>
            屋主房源 Featured Properties by Owner
        <?php endif; ?>  
        </div>
    </div>
   
</div>
<div style="float:left; width:100%;"><!--Start of 2nd module-->
	<div style="float:left; width:680px;padding-bottom:10px;"> <!--Start of content-->
    	<div id="ajax-adssearch-owner">
                    
        </div>
    </div><!--End of content-->
</div><!--End of 2nd module-->

</div>
</div>
<!--End-->        
</div>
<!--End-->


</div>


<!-- POPUP -->
<div id="popupForm" style="display:none;">
</div>