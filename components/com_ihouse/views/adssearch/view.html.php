﻿<?php
/**
* @version		$Id: view.html.php 10711 2008-08-21 10:09:03Z eddieajau $
* @package		Joomla
* @subpackage	Poll
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Poll component
 *
 * @static
 * @package		Joomla
 * @subpackage	Poll
 * @since 1.0
 */
class IhouseViewAdssearch extends JView
{
	function display($tpl = null)
	{
		global $mainframe, $option;
		$document =& JFactory::getDocument();

		//$document->addScript(JURI::root(true).'/components/com_ihouse/assets/jquery-1.3.2.js');
		//$document->addScript(JURI::root(true).'/components/com_ihouse/assets/jquery-ui-1.7.1.custom.min.js');
		//$document->addScript(JURI::root(true).'/components/com_ihouse/assets/slider.js');
			
		$user		=& 	JFactory::getUser();
		$uid		=	$user->get('id');	
		$gid		=	$user->get('gid');
		
		$layout		= 	JRequest::getCmd('layout', 'default');
		
		$db 	  	=& 	JFactory::getDBO();
		
		$limitstart =	JRequest::getCmd('limitstart', 0);
		$limit		=	JRequest::getCmd('limit', 10);
		
		$adv_search =	JRequest::getCmd('ads_adv_search', 0);
		$from		= 	JRequest::getCmd('from', ''); /* ads search MAP , for now */
		if($from)
			$property_id = JRequest::getCmd('id', '');
		//$headers 	=	'<strong>物业列表&nbsp;&nbsp;</strong><i>Property Listing</i>'; /* FOR HEADER TITLE */

		$where 		= array();
		$where_or 	= array();
		
		$this->assignRef('db', $db);
		
		$this->assignRef('adv_search', $adv_search);
		$this->assignRef('from', $from);
		$this->assignRef('property_id', $property_id);
		
		$this->assignRef('owner_page', $owner_page);
		$this->assignRef('agent_page', $agent_page);
		
		parent::display($tpl);
		
	}
	
}
?>
