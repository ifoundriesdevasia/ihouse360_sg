<?php


// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.controller');


class iHouseController extends JController
{
	/**
	 * constructor (registers additional tasks to methods)
	 * @return void
	 */
	function __construct()
	{
		parent::__construct();

		// Register Extra tasks
		//$this->registerTask( 'add'  , 'edit' );
		
		// Set the table directory
		//JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ihouse'.DS.'tables');

	}
	
	function display()
	{
		$viewName	= JRequest::getVar('view', 'ihouse', 'default', 'cmd');

		JRequest::setVar('view',$viewName);
		
		$task	= JRequest::getCmd('task');
		
		$document =& JFactory::getDocument();
		
		$viewType	= $document->getType();
		
		$layout = JRequest::getVar('layout', 'default');
		
		// View caching logic -- simple... are we logged in?
		
		$user = &JFactory::getUser();
		
		$viewcache = JRequest::getVar('viewcache',1,'POST','INT');

		$userid = JRequest::getVar( 'id', 0, 'post', 'int' );


		// preform security checks
		/*if ($user->get('id') == 0 || $userid == 0 || $userid <> $user->get('id')) {
			JError::raiseError( 403, JText::_('Access Forbidden') );
			return;
		}
		*/
		
		
		
		if ($user->get('id')) {
			parent::display(false);
		} else {
			
			parent::display(true);
		}
		
		
	}

	/**
	 * display the edit form
	 * @return void
	 */
	 
	function edit()
	{	
		// start to differentiate user and assign correct layout
		$user		= JFactory::getUser();
		$userid	= $user->get('gid');

		JRequest::setVar( 'view', 'ihouse' );
		JRequest::setVar( 'layout','edit'  );
		JRequest::setVar('hidemainmenu', 1);

		parent::display();
	
		
	}
	
	function search() 
	{
		global $mainframe;
		
		$viewName	= JRequest::getVar('view', 'ihouse', 'default', 'cmd');
		
		JRequest::setVar('view',$viewName);
		
		//JRequest::setVar( 'layout','default'  );	
		JRequest::setVar('hidemainmenu', 1);
		
		parent::display();
	}
	
	function adssearch() 
	{
		global $mainframe;
		
		$viewName	= JRequest::getVar('view', 'adssearch', 'default', 'cmd');
		
		JRequest::setVar('view',$viewName);
		
		$session = JFactory::getSession();
		
		$proptype	=	JRequest::getVar('prop_type');
		
		$current_proptype = $session->get('proptype');
		
		if($current_proptype == $proptype) {
			
			if($session->get('sessionsave')) {
				$sessionsave = $session->get('sessionsave');
				$session->set('sessionsave', 0);
			} else {
				$sessionsave = JRequest::getVar('sessionsave',0);
				$session->set('sessionsave', 1);
			}
		
			/* RESET BACK SESSIONS */
			if(!$sessionsave) {
				$session->set('ads_search_page_owner', 0);
				$session->set('ads_search_orderfilter_owner', 'DESC');
				$session->set('ads_search_filter_owner', 'a.posting_date');
	
				$session->set('ads_search_page_agent', 0);
				$session->set('ads_search_orderfilter_agent', 'DESC');
				$session->set('ads_search_filter_agent', 'a.posting_date');

			}
			
		} else {
			
			$session->set('ads_search_page_owner', 0);
			$session->set('ads_search_orderfilter_owner', 'DESC');
			$session->set('ads_search_filter_owner', 'a.posting_date');
	
			$session->set('ads_search_page_agent', 0);
			$session->set('ads_search_orderfilter_agent', 'DESC');
			$session->set('ads_search_filter_agent', 'a.posting_date');
			
			$session->set('proptype', $proptype);
		}
		
		
		
		//JRequest::setVar( 'layout','default'  );	
		JRequest::setVar('hidemainmenu', 1);
		
		parent::display();
	}
	
	function global_search() { /* Property Search - On Main Menu */
		global $mainframe;
		
		$viewName	= JRequest::getVar('view', 'propertysearch', 'default', 'cmd');
		
		JRequest::setVar('view',$viewName);
		
		//$keyword			=	strtolower(JRequest::getVar('global_search_keyword', ''));
		
		//JRequest::setVar( 'layout','default'  );	
		//JRequest::setVar('hidemainmenu', 1);
		
		parent::display();
	}
	
	function mrtsearch() {
		global $mainframe;
		include_once('assets/json.php');
		
		//make json object...
		
		$json = new Services_JSON();
		
		$viewName	= JRequest::getVar('view', 'mapsearch', 'default', 'cmd');
		
		$model		= &$this->getModel('mapsearch');
		
		header('Content-type: application/json');
		
		$tmp		=	$model->getMRTArray();
		
		
		echo $json->encode(array(	"postcode"	=> 	$tmp['postcode'], 
									"name_en" 	=> 	$tmp['name_en'],				  
									"name_ch"	=>	$tmp['name_ch'], 
									"address" 	=> 	$tmp['address'],
									"lats"		=>	$tmp['lat'], 
									"lngs"		=>	$tmp['lng']
							));
		
		$mainframe->close();
		exit;
	}
	
	function schoolsearch() {
		global $mainframe;
		include_once('assets/json.php');
		
		//make json object...
		
		$json = new Services_JSON();
		
		$viewName	= 	JRequest::getVar('view', 'mapsearch', 'default', 'cmd');
		
		header('Content-type: application/json');
		
		$model		= &$this->getModel('mapsearch');
		
		$tmp		=	$model->getSchoolArray();
		
		
		echo $json->encode(array(	"postcode"=> $tmp['postcode'], 
									"name_en" => $tmp['name_en'],				  
									"name_ch"=>$tmp['name_ch'], 
									"address" => $tmp['address'],
									"lats"=>$tmp['lat'], 
									"lngs"=>$tmp['lng']
							));
		
		$mainframe->close();
		exit;
	}
	function mapsearch() {
		global $mainframe;
		include_once('assets/json.php');
		
		//make json object...
		
		$json = new Services_JSON();
		
		$viewName	= 	JRequest::getVar('view', 'mapsearch', 'default', 'cmd');
		
		JRequest::setVar('view',$viewName);
		
		$model		= &$this->getModel('mapsearch');
		
		header('Content-type: application/json');
		
		$tmp	=	$model->getDataArray();
		
		echo $json->encode(array(	"postcode"=> $tmp['postcode'], 
									"lats"=>$tmp['lat'], 
									"lngs"=>$tmp['lng']
							));
		
		$mainframe->close();
		exit;
	}
	
	function mapsearch1() { /* Panning Search */
		global $mainframe;
		include_once('assets/json.php');
		
		//make json object...
		
		$json = new Services_JSON();
		
		$viewName	= 	JRequest::getVar('view', 'mapsearch', 'default', 'cmd');
		
		$lat1		=	JRequest::getVar('lat1', '');
		$lat2		=	JRequest::getVar('lat2', '');
		$lng1		=	JRequest::getVar('lng1', '');
		$lng2		=	JRequest::getVar('lng2', '');
		
		JRequest::setVar('view',$viewName);
		
		$model		= &$this->getModel('mapsearch');
		
		header('Content-type: application/json');
		
		$tmp	=	$model->getDataArrayByPanning($lat1,$lat2,$lng1,$lng2);
		//$tmp2	=	$model->getAdsArray($tmp['postcode']);
		
		echo $json->encode(array(	"postcode"=> $tmp['postcode'], 
									"property_id" => $tmp['property_id'],				  
									"title_ch"=>$tmp['title_ch'],
									"title_en"=>$tmp['title_en'],
									"psm_price"=>$tmp['psm_price'],
									"lats"=>$tmp['lat'], 
									"lngs"=>$tmp['lng'],
									"address"=>$tmp['address']
							));
		
		$mainframe->close();
		exit;
	}
	function mapsearch_Mode() {
		global $mainframe;
		include_once('assets/json.php');
		
		//make json object...
		
		$json = new Services_JSON();
		
		$viewName	= 	JRequest::getVar('view', 'mapsearch', 'default', 'cmd');
		
		JRequest::setVar('view',$viewName);
		
		$model		= &$this->getModel('mapsearch');
		
		$tmp		=	$model->getOptionMapSearchObj();
				
		header('Content-type: application/json');

		echo $json->encode(array(	"price_html"=> $tmp->price,
								 	"bedroom_html"=> $tmp->bedroom
							));

		$mainframe->close();
		exit;
		
	}
	function reposition_maps() {
		global $mainframe;
		include_once('assets/json.php');
		
		//make json object...
		
		$json = new Services_JSON();
		
		$viewName	= 	JRequest::getVar('view', 'mapsearch', 'default', 'cmd');
		
		$mode		=	JRequest::getVar('opt','');
		$value		=	JRequest::getVar('val',''); // id, or name_ch
		
		JRequest::setVar('view',$viewName);
		
		$model		= &$this->getModel('mapsearch');
		
		header('Content-type: application/json');
		
		$obj	=	$model->getMapPosition($mode, $value);
		//echo $postcode;
		echo $json->encode(array(	"postcode" 	=> $obj->postcode,
								 	"address"	=> $obj->address,
								 	"name_ch"	=> $obj->name_ch,
									"name_en"	=> $obj->name_en
							));
		
		$mainframe->close();
		exit;
	}
	function district_reposition_map() {
		global $mainframe;
		include_once('assets/json.php');
		
		//make json object...
		
		$json = new Services_JSON();
		
		$viewName	= 	JRequest::getVar('view', 'mapsearch', 'default', 'cmd');
		
		$value		=	JRequest::getVar('val',''); // id, or name_ch
		
		JRequest::setVar('view',$viewName);
		
		$model		= &$this->getModel('mapsearch');
		
		header('Content-type: application/json');
		
		$position	=	$model->getDistrictPosition($value);
		//echo $postcode;
		echo $json->encode(array(	"lat" => $position->lat,
								 	"lng" => $position->lng
							));
		
		$mainframe->close();
		exit;
	}
	function autocomplete_globalsearch() {
		global $mainframe;
		include_once('assets/json.php');
		
		//make json object...
		
		$json = new Services_JSON();
		
		//$keyword		=	JRequest::getVar('global_search_keyword','');
		
		$model		= &$this->getModel('search');
		
		header('Content-type: application/json');
		
		$property	=	$model->getProperty();
		
		echo $json->encode(array(	"property" => $property
							));
		
		$mainframe->close();
		exit;
	}
	
	function adtypelisting() {

		JRequest::setVar( 'view', 'ihouse' );
		JRequest::setVar( 'layout','default'  );

		parent::display();
	}
	function popularlisting() {

		JRequest::setVar( 'view', 'popularlisting' );
		JRequest::setVar( 'layout','default'  );

		parent::display();
	}
	
	function adsuserratingoverall() {
		global $mainframe;
		
		$ads_id	=	JRequest::getVar('id');
		
		$model		= &$this->getModel('rating_ads');
		
		$rating		=	$model->getAdsUserRating();
		
		$transport_avg 	= 	0;
		$env_avg		=	0;
		$design_avg		=	0;
		$fac_avg		=	0;
		$developer_avg	=	0;
		
		if(!empty($rating)) {
			
			$transport_avg =  sprintf("%01.1f",$rating->transport_avg / (5*$rating->total) * 100);
			$env_avg =  sprintf("%01.1f",$rating->environment_avg / (5*$rating->total) * 100);
			$design_avg =  sprintf("%01.1f",$rating->design_avg / (5*$rating->total) * 100);
        	$fac_avg =  sprintf("%01.1f",$rating->facility_avg / (5*$rating->total) * 100);		
			$developer_avg =  sprintf("%01.1f",$rating->developer_avg / (5*$rating->total) * 100);
			
		}
		
		$star_transport = $model->getStarAdsUserRating($transport_avg);
		$star_env 		= $model->getStarAdsUserRating($env_avg);
		$star_design 	= $model->getStarAdsUserRating($design_avg);
		$star_fac 		= $model->getStarAdsUserRating($fac_avg);
		$star_developer = $model->getStarAdsUserRating($developer_avg);
		
		require('views/rating_ads/tmpl/default.php');
		
		$mainframe->close();
		exit;
	}
	
	function adsuserratingform() {
		global $mainframe;
		
		$ads_id	=	JRequest::getVar('ads_id');
		
		
		require('views/rating_ads/tmpl/rating_form.php');
		
		$mainframe->close();
		exit;
	}
	
	function adsuserratingsubmit() {
		global $mainframe;
		include_once('assets/json.php');
		
		$json = new Services_JSON();

		header('Content-type: application/json');

		$model		= &$this->getModel('rating_ads');
		
		$rating	=	$model->submitAdsUserRating();
		
		echo $json->encode(array(	"status" => $rating
							));
		
		//require('views/rating/tmpl/rating_form.php');
		
		$mainframe->close();
		exit;
	}
	
	/* PROPERTY USER RATING */
	
	function userratingoverall() {
		global $mainframe;
		
		$property_id	=	JRequest::getVar('id');
		
		$model		= &$this->getModel('rating');
		
		$rating		=	$model->getPropertyUserRating();
		
		$transport_avg 	= 	0;
		$env_avg		=	0;
		$design_avg		=	0;
		$fac_avg		=	0;
		$developer_avg	=	0;
		
		if(!empty($rating)) {
			
			$transport_avg =  sprintf("%01.1f",$rating->transport_avg / (5*$rating->total) * 100);
			$env_avg =  sprintf("%01.1f",$rating->environment_avg / (5*$rating->total) * 100);
			$design_avg =  sprintf("%01.1f",$rating->design_avg / (5*$rating->total) * 100);
        	$fac_avg =  sprintf("%01.1f",$rating->facility_avg / (5*$rating->total) * 100);		
			$developer_avg =  sprintf("%01.1f",$rating->developer_avg / (5*$rating->total) * 100);
			
		}
		
		$star_transport = $model->getStarUserRating($transport_avg);
		$star_env 		= $model->getStarUserRating($env_avg);
		$star_design 	= $model->getStarUserRating($design_avg);
		$star_fac 		= $model->getStarUserRating($fac_avg);
		$star_developer = $model->getStarUserRating($developer_avg);
		
		require('views/rating/tmpl/default.php');
		
		$mainframe->close();
		exit;
	}
	
	function userratingform() {
		global $mainframe;
		
		$property_id	=	JRequest::getVar('id');
		
		require('views/rating/tmpl/rating_form.php');
		
		$mainframe->close();
		exit;
	}
	
	function userratingsubmit() {
		global $mainframe;
		include_once('assets/json.php');
		
		$model		= &$this->getModel('rating');
		
		$rating	=	$model->submitPropertyUserRating();
		
		$json = new Services_JSON();

		header('Content-type: application/json');

		echo $json->encode(array(	"status" => $rating
							));
		
		$mainframe->close();
		exit;
	}
	
	function getpropertyreview() {
		global $mainframe;
		$model		= &$this->getModel('review');
		
		$total		=	$model->totalPropertyReview();
		$review		=	$model->getPropertyReview();
		
		$model		= &$this->getModel('pagination');
		
		$pagination_html	=	$model->getPagination($total, 10);
		
		require('views/review/tmpl/default.php');
		
		$mainframe->close();
		exit;
	}
	
	function propertyreviewsubmit() {
		global $mainframe;
		include_once('assets/json.php');
		
		//make json object...
		
		$json = new Services_JSON();
		
		$model		= &$this->getModel('review');
		
		header('Content-type: application/json');
		
		$property	=	$model->submitPropertyReview();
		
		echo $json->encode(array(	"status" => $property
							));
		
		$mainframe->close();
		exit;
	}
	
	
	function updatestatistictotalpropertydetail() {
		global $mainframe;
		
		$model		= &$this->getModel('stat');
		
		$property_id 	= 	JRequest::getCmd('id');
		$postcode		=	JRequest::getCmd('postcode');
		
		$rating			=	$model->getRatingValuePercentage($property_id);
		$star_html		= 	$model->getReviewStar($rating);

		$total_rent		=	$model->getTotalRent($property_id);
		$total_sell		=	$model->getTotalSell($property_id);
		$total_review	=	$model->getTotalReview($property_id);
		
		require('views/stat/tmpl/default.php');
		
		$mainframe->close();
		exit;
	}
	
	/* AJAX - PRIME PROPERTIES (SELL AND RENT) */
	
	function ajaxLoadPrimeProperties() {
		global $mainframe;
		
		$model		= &$this->getModel('prime_properties');
		
		$adtype	= 	JRequest::getVar('adtype', 'sell');
		
		$rows 	=	''; 
		
		switch($adtype) {
			case 'sell' :
				$rows	=	$model->getSell();
				break;
			case 'rent' :
				$rows	=	$model->getRent();
				break;
		}
		
		require('views/primeproperties/tmpl/default.php');
		
		$mainframe->close();
		exit;
	}
	function loopBackSession() {
		global $mainframe;
		include_once('assets/json.php');
		
		//make json object...
		
		$json = new Services_JSON();
		
		header('Content-type: application/json');
		
		$usercat 	= 	JRequest::getVar('usercat');
		
		$session = JFactory::getSession();
		
		if($usercat == 'owner') {
			$page 			= 	$session->get('ads_search_page_owner');
			$orderfilter 	= 	$session->get('ads_search_orderfilter_owner');
			$filter 		= 	$session->get('ads_search_filter_owner');
			
		} else if($usercat == 'agent') {
			
			$page 			= 	$session->get('ads_search_page_agent');
			$orderfilter 	= 	$session->get('ads_search_orderfilter_agent');
			$filter 		= 	$session->get('ads_search_filter_agent');
			
		}
		
		if(!isset($page))
			$page = 0;
		
		if(!isset($orderfilter))
			$orderfilter = 'DESC';
		
		if(!isset($filter))
			$filter = 'a.posting_date';
			
		echo $json->encode(array(	
								 	"page"			=> 	$page,
									"orderfilter"	=>	$orderfilter,
									"filter"		=>	$filter
							));
		
		$mainframe->close();
		exit;
	}
	function iFrameAdsSearchPage() {
		global $mainframe;
		
		$usercat 		= 	JRequest::getVar('usercat');
		$page			=	JRequest::getInt('page');
		$orderfilter	=	JRequest::getVar('orderfilter');
		$filter			=	JRequest::getVar('filter');
		
		$session = JFactory::getSession();
		
		if($usercat == 'owner') {
			$session->set('ads_search_page_owner', $page);
			$session->set('ads_search_orderfilter_owner', $orderfilter);
			$session->set('ads_search_filter_owner', $filter);
			
			//echo $usercat. ' - ' . $session->get('ads_search_page_owner') . ' - '. $session->get('ads_search_orderfilter_owner') .' - '. $session->get('ads_search_filter_owner');
		}
	
		if($usercat == 'agent') {
			$session->set('ads_search_page_agent', $page);
			$session->set('ads_search_orderfilter_agent', $orderfilter);
			$session->set('ads_search_filter_agent', $filter);
			
			echo $usercat. ' - ' . $session->get('ads_search_page_agent') . ' - '. $session->get('ads_search_orderfilter_agent') .' - '. $session->get('ads_search_filter_agent'). '<br />' ;
		}
		
		$mainframe->close();
		exit;
	}
	function ajaxLoadAdsSearch() 
	{
		global $mainframe;
		$model		= &$this->getModel('adssearch');
		
		$usercat	= 	JRequest::getVar('user_category', '');
		$page		=	JRequest::getVar('page', 0);
		$prop_type	=	JRequest::getVar('prop_type',2);
		$ptype		=	JRequest::getVar('ptype','');
		$db		=	JFactory::getDBO(); 
		
		$obj	=	$model->getAdsObject($usercat);
		
		$model1		= &$this->getModel('pagination');
		
		$pagination_html		=	$model1->getPaginationAdsSearch($obj->total, 5, $usercat);
		
		$filter					=	JRequest::getVar('filter', '');
		$orderfilter			=	JRequest::getVar('orderfilter', '');
		$orderfilter_session	=	$orderfilter;
		
		$session = &JFactory::getSession();
		
		/* DEFAULT */
		
		$orderfilter1	=	'ASC';
		$orderfilter2	=	'ASC';
		$orderfilter3	=	'ASC';
		$orderfilter4	=	'ASC';
		
		$image1			= '';
		$image2			= '';
		$image3 		= '';
		$image4			= '';
		
		if($filter == 'a.no_of_room') {
			
			if($orderfilter == 'ASC') {
        		$orderfilter1 = 'DESC';
				$image1 = '<img src="'.JRoute::_('images/sort_asc.png').'" />';
			}else{
				$orderfilter1 = 'ASC';
				$image1 = '<img src="'.JRoute::_('images/sort_desc.png').'" />';
			}
		}
		else if($filter == 'a.property_type') {
			if($prop_type == 3) {
				
				if($ptype == '有地住宅') {
					if($orderfilter == 'ASC') {
        				$orderfilter1 = 'DESC';
						$image1 = '<img src="'.JRoute::_('images/sort_asc.png').'" />';
					}else{
						$orderfilter1 = 'ASC';
						$image1 = '<img src="'.JRoute::_('images/sort_desc.png').'" />';
					}
				} else if($ptype == '商业') {
					if($orderfilter == 'ASC') {
        				$orderfilter2 = 'DESC';
						$image2 = '<img src="'.JRoute::_('images/sort_asc.png').'" />';
					}else{
						$orderfilter2 = 'ASC';
						$image2 = '<img src="'.JRoute::_('images/sort_desc.png').'" />';
					}
				} else if($ptype == '服务公寓') {
					if($orderfilter == 'ASC') {
        				$orderfilter3 = 'DESC';
						$image3 = '<img src="'.JRoute::_('images/sort_asc.png').'" />';
					}else{
						$orderfilter3 = 'ASC';
						$image3 = '<img src="'.JRoute::_('images/sort_desc.png').'" />';
					}
				}
				
			} else {
	
				if($orderfilter == 'ASC') {
        			$orderfilter2 = 'DESC';
					$image2 = '<img src="'.JRoute::_('images/sort_asc.png').'" />';
				}else{
					$orderfilter2 = 'ASC';
					$image2 = '<img src="'.JRoute::_('images/sort_desc.png').'" />';
				}
			}
		} else if($filter == 'a.ask_price') {
			
			if($orderfilter == 'ASC') {
        		$orderfilter3 = 'DESC';
				$image3 = '<img src="'.JRoute::_('images/sort_asc.png').'" />';
			}else{
				$orderfilter3 = 'ASC';
				$image3 = '<img src="'.JRoute::_('images/sort_desc.png').'" />';
			}
		} else if($filter == 'a.posting_date') {
			
			if($orderfilter == 'ASC')  {
        		$orderfilter4 = 'DESC';
				$image4 = '<img src="'.JRoute::_('images/sort_asc.png').'" />';
			} else { 
				$orderfilter4 = 'ASC';
				$image4 = '<img src="'.JRoute::_('images/sort_desc.png').'" />';
			}
		}
		
		require('views/adssearch_ajax/tmpl/default.php');
		
		$mainframe->close();
		exit;
	}
	
	
	function loadAdsInfoBoxInPropertyMapSearch() {
		global $mainframe;
		include_once('assets/json.php');
		
		//make json object...
		
		$json = new Services_JSON();
		
		$model					= &$this->getModel('mapsearchads');
		$model1					= &$this->getModel('pagination');
		
		header('Content-type: application/json');
		
		$result					=	$model->loadAdsInMapSearchFiltered();
		$pagination 			= 	$model1->getPaginationAdsMapSearch($result->total, 5);
		
		$bedroom				= 	JRequest::getVar('bedroom', '');
		$prices					= 	JRequest::getVar('price', '');
		$prices_max				=	JRequest::getVar('price_max', '');
		$propertytype			=	JRequest::getVar('proptype','');
		$margin_amt_percent		=	JRequest::getVar('amount', 0); 
		$budget					=	JRequest::getVar('budget', 0);
		
		$price_html				=	$model->getPricesAdsListHTML($prices);
		$bedroom_html 			= 	$model->getBedroomAdsListHTML($bedroom);
		$specialist_html 		= 	$model->loadSpecialistsSearchFiltered();
		
		echo $json->encode(array(	"result_html" 		=> $result->result_html,
								 	"pagination"		=> $pagination,
									"price_html"		=> $price_html,
									"bedroom_html"		=> $bedroom_html,
									"specialist_html"	=> $specialist_html,
									"total"				=> $result->total
							));
		
		$mainframe->close();
		exit;
	}
	
	function loadAjaxOptionListMapSearch() {
		global $mainframe;
		include_once('assets/json.php');
		
		//make json object...
		
		$json = new Services_JSON();
		
		$model		= &$this->getModel('mapsearchoption');
		
		header('Content-type: application/json');
		
		$result = $model->loadHTML();
		
		echo $json->encode(array(	"result_html" 	=> $result
							));
		
		$mainframe->close();
		exit;
	}
	
	function emailNotExistForm() {
		global $mainframe;
		
		$db		=	JFactory::getDBO(); 
		
		$user	=	JFactory::getUser();
		$user	=	JFactory::getUser($user->id);
		
		$email	=	'xiaozys@gmail.com';
		//$email		=	'andreas@ifoundries.com';
		
		require('views/contact_form/tmpl/email_us.php');
		$mainframe->close();
		exit;
	}
	
	function emailNotExistSubmit() {
		global $mainframe;
		include_once('assets/json.php');
		
		//make json object...
		
		$json = new Services_JSON();
		
		$model		= &$this->getModel('contact');
		
		header('Content-type: application/json');
		
		$status		=	$model->sendEmailNotExist();
		
		echo $json->encode(array("status" => $status	
								 
							));
		$mainframe->close();
		exit;
	}
	
	function emailContactForm() {
		global $mainframe;
		
		$db		=	JFactory::getDBO(); 
		
		$email	=	JRequest::getVar('email','');
		$ads_id =	JRequest::getVar('ads_id','');
		$uid 	=	JRequest::getVar('uid','');
		
		require('views/contact_form/tmpl/default.php');
		$mainframe->close();
		exit;
	}
	
	function email() {
		global $mainframe;
		include_once('assets/json.php');
		
		//make json object...
		
		$json = new Services_JSON();
		
		$model		= &$this->getModel('contact');
		
		header('Content-type: application/json');
		
		$status		=	$model->sendEmail();
		
		echo $json->encode(array("status" => $status	
								 
							));
		$mainframe->close();
		exit;
	}
	
	/* PICTURE ONLY will be put on images/ihouse/ */
	function getNonLandedPriceIndex() {
		global $mainframe;
		
		$model		= &$this->getModel('nonlandedpriceindex');
		
		$status		=	$model->getImage();
		
		$mainframe->close();
		exit;
	}
	
	function mapsearchListSearch() {
		global $mainframe;
		include_once('assets/json.php');
		
		$json = new Services_JSON();
		
		$model		= &$this->getModel('mapsearchoption');
		
		header('Content-type: application/json');
		
		$list		=	$model->getListHTML();
		
		echo $json->encode(array("list_html" 	=> $list
							));
		
		$mainframe->close();
		exit;
	}
	
	/***********************/
	/* FLOORPLAN LIST PAGE */
	/***********************/
	
	function floorplanlist() {
		global $mainframe;
		
		$viewName	= JRequest::getVar('view', 'floorplanlist', 'default', 'cmd');
		
		JRequest::setVar('view',$viewName);
		
		parent::display();
	}
	
	function loadDisplayZoneFloorplanProperty() {
		global $mainframe;
		include_once('assets/json.php');
		
		$json = new Services_JSON();
		
		$model		= &$this->getModel('floorplanlist');
		
		header('Content-type: application/json');
		
		$list		=	$model->getDisplayZone();
		
		echo $json->encode(array("list_html" 	=> $list
							));
		
		$mainframe->close();
		exit;
	}
	
	/* function loadFloorplanImagesProperty() {
		global $mainframe;
		include_once('assets/json.php');
		
		$json = new Services_JSON();
		
		$model		= &$this->getModel('floorplanlist');
		
		header('Content-type: application/json');
		
		$list		=	$model->getFloorplanSmallImage();
		
		echo $json->encode(array("list_html" 	=> $list
							));
		
		$mainframe->close();
		exit;
	}
	*/
	
	function getFloorPlanImages() {
		global $mainframe;
		
		$viewName	= JRequest::getVar('view', 'floorplanlist', 'default', 'cmd');
		
		JRequest::setVar('view',$viewName);
		parent::display();
	}
	
	function sitemap() {

		JRequest::setVar( 'view', 'sitemap' );
		JRequest::setVar( 'layout','default'  );

		parent::display();
	}
	
	function renew() {
		global $mainframe;
		
		$model		= &$this->getModel('renew');
		
		JRequest::setVar( 'view', 'renew' );
		
		$confirm	=	JRequest::getVar('confirmation',0);
		
		if($confirm == 1) {
			JRequest::setVar( 'layout','confirmation'  );
			
			$status 	=	$model->renewal(); 
			
		} else {
			JRequest::setVar( 'layout','default'  );
			
		}

		parent::display();
	}
	
	 function addFavorite() {
		 global $mainframe;
		
		include_once('assets/json.php');
		
		//make json object...
		
		$json = new Services_JSON();
		
		$model		= &$this->getModel('favorite');
		
		$row	=	$model->add();
		
		header('Content-type: application/json');
		
		echo $json->encode(array("status" => $row->status,
								 "fav_id"	=> $row->fav_id
							));
		
		$mainframe->close;
		exit;
	 }
	 
	 function removeFavorite() {
		 global $mainframe;
		
		include_once('assets/json.php');
		
		//make json object...
		
		$json = new Services_JSON();
		
		$model		= &$this->getModel('favorite');
		
		$row	=	$model->remove();
		
		header('Content-type: application/json');
		
		echo $json->encode(array("status" => $row->status,	
								 "aid" => $row->aid,
								 "user_id" => $row->user_id
							));
		
		$mainframe->close;
		exit;
	 }
	 function ajaxLoadFloorPlan() {
		 global $mainframe;
		
		$db		=	JFactory::getDBO(); 
		
		$postcode	=	JRequest::getVar('postcode','');
		$roomtype	=	JRequest::getVar('roomtype','');
		
		require('views/ajax_floorplan/tmpl/default.php');
		$mainframe->close();
	 }
}