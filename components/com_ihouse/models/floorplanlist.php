<?php
/**
* @version		$Id: poll.php 10752 2008-08-23 01:53:31Z eddieajau $
* @package		Joomla
* @subpackage	Polls
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class IhouseModelFloorplanlist extends JModel
{
	function __construct()
	{
		global $mainframe;

		parent::__construct();

	}
	
	function getDisplayZone() {
		
		$postcode			=	JRequest::getVar('postcode', '');
		
		$db = $this->getDBO();
		
		$query	= "SELECT * FROM #__ihouse_floorplan ";
			$db->setQuery( $query );
			$floorplan = $db->loadObjectList();

		$query	= "SELECT floor_plan FROM #__ihouse_property WHERE postcode = ".$postcode;
			$db->setQuery( $query );
			$result = $db->loadResult();
	
		//$room_type_ch = explode('|', $result);
	
		$html	=	'';
		$html	.= 	'<select id="display_zone_select" name="display_zone_select" onchange="javascript:loadFloorplanImages(this.value);">';
		$html 	.= 	'<option value="">---</option>';
		
		foreach($floorplan as $fp) :
				
			$floorplan_path = JRoute::_('images/singapore/'.$postcode.'/floorplan/'.$fp->type_en);
				
			if(file_exists($floorplan_path)) {
				$html .= '<option value="'.$postcode.'|'.$fp->type_en.'">'.$fp->type_ch.'</option>';
			}
		endforeach;
		
		$html	.= 	'</select>';
		return $html;		
	}
	
	
}