﻿<?php
/**
* @version		$Id: poll.php 10752 2008-08-23 01:53:31Z eddieajau $
* @package		Joomla
* @subpackage	Polls
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class IhouseModelAdssearch extends JModel
{
	function __construct()
	{
		global $mainframe;

		parent::__construct();

	}
	
	function getAdsObject($usercat) {
		
		$db = $this->getDBO();
		
		$limitstart 	=	JRequest::getVar('limitstart', 0);
		$limit			=	JRequest::getVar('limit', 5);
		
		$adv_search 	=	JRequest::getVar('ads_adv_search', 0);
		
		$from			= 	JRequest::getVar('from', '');
		
		
		$where 		= array();
		$where_or 	= array();
		
		$usercat 	= ucfirst($usercat);
		
		/****************************/
		/* ADVANCED ADS SEARCH PAGE */
		/****************************/
		
		$where[]	=	" u.user_category = '$usercat' ";
		
		$page			=	JRequest::getVar('page', '');
		$limitstart		=	$page * $limit;
		
		if($from) {
			$property_id	= 	JRequest::getVar('property_id', '');
			$where[]	=	" a.property_id = '$property_id' ";
		}
		
		if($adv_search) {
			
			$property_class		=	JRequest::getVar('property_cls', 'all');
			$ticked_fp 			= 	JRequest::getVar('ticked_fp', '');
			
			$nearest_station 	= 	JRequest::getVar('nearest_station',''); /* No use */
			$nearest_school 	= 	JRequest::getVar('nearest_school', ''); /* No Use */
		
			$district_sector 	= 	unserialize(JRequest::getVar('district_sector', array()));
		
			$keyword			=	JRequest::getVar('keyword', '');
		
			$ticked_hdb 		= 	JRequest::getVar('t_hdb', '');
			$ticked_condo 		= 	JRequest::getVar('t_condo', '');
		
			$prices 			= 	JRequest::getVar('prices','');/* condo */
			$prices_max 		= 	JRequest::getVar('prices_max','');/* condo */
			
			$prices_h			= 	JRequest::getVar('prices_h','');/* hdb */
			$prices_h_max		= 	JRequest::getVar('prices_h_max','');/* hdb */
			
			$prices_rent		= 	JRequest::getVar('prices_rent','');/* rent ads */
			$prices_rent_max	= 	JRequest::getVar('prices_rent_max','');/* rent ads */
		
			$bedroom			=	JRequest::getVar('bedroom', '');/* condo */
			$bedroom_h			=	JRequest::getVar('bedroom_h', '');/* hdb */
		
			$prop_type			=	JRequest::getVar('prop_type',2); /* 1 or 2 */
		
			$keyword			=	JRequest::getVar('keyword', '');
			
			$ap					=	JRequest::getVar('amount', 0);
			
			$flattype			=	JRequest::getVar('flattype','');
			
			$hdb_town			=	unserialize(JRequest::getVar('hdb_town', array()));
			
			// filter ordering
			$filter				=	JRequest::getVar('filter', '');
			$orderfilter		=	JRequest::getVar('orderfilter', '');
		
			if(!empty($filter) || !empty($orderfilter)) {
				$orderby	= " ORDER BY ".$filter." ".$orderfilter." ";
				
				if($prop_type == 3) {
					$orderby = " ORDER BY ".$filter." ".$orderfilter.", a.posting_date ". $orderfilter;
				}
			}
		
			//echo 'flat : '.$flattype;
			switch($property_class) {
				case 'new'		:
				$where[]	=	" a.ad_type = 'sell' AND a.property_category LIKE ".$db->Quote( '%'.$db->getEscaped('新房', true ).'%', false );
					break;
				case 'second' 	:
				$where[]	=	" a.ad_type = 'sell' AND a.property_category LIKE ".$db->Quote( '%'.$db->getEscaped('其它', true ).'%', false );
					break;
				case 'rent'		:
				$where[]	=	" a.ad_type = 'rent' ";
					break;
				case 'all' 		:
				default 		:
					break;
			}
		
			
		
			/* SET PROPERTY TYPE */
			if($prop_type == 1 || $prop_type == 0)  {
				
				if($prop_type == 1 ) $where[] = " a.property_type = '公寓' ";
				
				if(!empty($district_sector)) { // array()
					$join_property 	= ' LEFT JOIN #__ihouse_property AS p ON p.postcode = a.postcode ';
			
					$arrTmp = array();
					
					foreach($district_sector as $d):
						if(!empty($d))
							$arrTmp[] = " a.property_district_id = '$d' ";
					endforeach;
			
					$arrTmp 		=	( count ( $arrTmp ) ? ' ( '.implode( ' OR ', $arrTmp ).' ) ' : '' ) ;
			
					if(!empty($arrTmp))
						$where[] = $arrTmp;
			
				}
			
			}
			elseif($prop_type == 2) {
				$where[] = " a.property_type = '组屋' ";
				
				if($flattype) {
					$where[]	=	" a.flat_type = '$flattype' ";
				}
				
				if(!empty($hdb_town)) {
					$arrTmp = array();
				
					foreach($hdb_town as $d):
						if(!empty($d))
							$arrTmp[] = " a.hdb_town_id = '$d' ";
					endforeach;
			
					$arrTmp 		=	( count ( $arrTmp ) ? ' ( '.implode( ' OR ', $arrTmp ).' ) ' : '' ) ;
			
					if(!empty($arrTmp))
						$where[] = $arrTmp;
					
				}
			} else if($prop_type == 3) {
				$ptype	=	JRequest::getVar('ptype','有地住宅');
				
				if($ptype == '有地住宅') 
					$where[] = " a.property_type = '有地住宅' ";
				else if($ptype == '商业')
					$where[] = " a.property_type = '商业' ";
				else if($ptype == '服务公寓')
					$where[] = " a.property_type = '服务公寓' ";
				else	
					$where[] = " ( a.property_type = '有地住宅' OR a.property_type = '商业' OR a.property_type = '服务公寓' ) ";
				
				if(!empty($district_sector)) { // array()
			
					$arrTmp = array();
					
					foreach($district_sector as $d):
						if(!empty($d))
							$arrTmp[] = " a.property_district_id = '$d' ";
					endforeach;
			
					$arrTmp 		=	( count ( $arrTmp ) ? ' ( '.implode( ' OR ', $arrTmp ).' ) ' : '' ) ;
			
					if(!empty($arrTmp))
						$where[] = $arrTmp;
			
				}
			}
			 
			$min = 0;
			$max = 0;
			/* SET PRICES */
			if($property_class	== 'rent') {
				$min = $this->setPrice($prices_rent, 'rent');
				$max = $this->setPrice($prices_rent_max, 'rent');
			}
			else if($prop_type == 1 || $prop_type == 0) 
			{
				/* Condo */
				$min = $this->setPrice($prices, 'condo');
				$max = $this->setPrice($prices_max, 'condo');
			} 
			else if($prop_type == 2)  
			{ 
				/* HDB */
				$min = $this->setPrice($prices_h, 'hdb');
				$max = $this->setPrice($prices_h_max, 'hdb');
			}
		
			if($ap && ($min > 0 || $max > 0)) {
				$min_x = $min - ($ap / 100 * $min);
				$max_x = $max + ($ap / 100 * $max);
				
				$where[] = " a.ask_price >= ". $min_x;
				$where[] = " a.ask_price <= ". $max_x;
			}
			
			
			/* SET BEDROOM */
			if($prop_type == 1 || $prop_type == 0) {
				switch($bedroom) {
					case 1: /* 1房 */
						$where[] = " a.no_of_room = '1' ";
						break;
					case 2: /* 2房 */
						$where[] = " a.no_of_room = '2' ";
						break;
					case 3: /* 3房 */
						$where[] = " a.no_of_room = '3' ";
						break;
					case 4: /* 4房 */
						$where[] = " a.no_of_room = '4' ";
						break;
					case 5: /* 5房 */
						$where[] = " a.no_of_room >= 5 ";
						break;
					default:
						break;
				}
			} 
			else if($prop_type == 2) 
			{
				switch($bedroom_h) {
					case 1: /* 1房 */
						$where[] = " a.no_of_room = '1' ";
						break;
					case 2: /* 2房 */
						$where[] = " a.no_of_room = '2' ";
						break;
					case 3: /* 3房 */
						$where[] = " a.no_of_room = '3' ";
						break;
					case 4: /* 4房 */
						$where[] = " a.no_of_room = '4' ";
						break;
					case 5: /* 5房 */
						$where[] = " a.no_of_room = '5' ";
						break;
					case 6: /* 公寓式 */
						$where[] = " a.no_of_room > 5 ";
						break;		
					default:
						break;
				}
			}
		
		
			if ($keyword)
			{
				$where[] = ' ( LOWER(a.ad_title) LIKE '.$db->Quote( '%'.$db->getEscaped( $keyword, true ).'%', false )
						.	' OR LOWER(a.street_name) LIKE '.$db->Quote( '%'.$db->getEscaped( $keyword, true ).'%', false )
						.	' OR LOWER(a.property_name) LIKE '.$db->Quote( '%'.$db->getEscaped( $keyword, true ).'%', false )
						.	' OR LOWER(a.postcode) LIKE '.$db->Quote( '%'.$db->getEscaped( $keyword, true ).'%', false )
						//.	' OR LOWER(p.postcode) LIKE '.$db->Quote( '%'.$db->getEscaped( $keyword, true ).'%', false )
						.	' OR LOWER(a.add_info) LIKE '.$db->Quote( '%'.$db->getEscaped( $keyword, true ).'%', false )
						//.	' OR LOWER(p.nearby_mrt) LIKE '.$db->Quote( '%'.$db->getEscaped( $keyword, true ).'%', false )
						//.	' OR LOWER(p.nearby_school) LIKE '.$db->Quote( '%'.$db->getEscaped( $keyword, true ).'%', false )
						.	' ) ';
			} 
		
			$join_property = ' ';
		
			/*if(!empty($nearest_station)) {
				$join_property 	= '  LEFT JOIN #__ihouse_property AS p ON p.postcode = a.postcode ';
				$where[] 		= ' a.nearby_mrt LIKE '.$db->Quote( '%'.$db->getEscaped( $nearest_station, true ).'%', false );
			}
		
			if(!empty($nearest_school)) {
				$join_property 	= ' LEFT JOIN #__ihouse_property AS p ON p.postcode = a.postcode ';
				$where[] 		= ' a.nearby_school LIKE '.$db->Quote( '%'.$db->getEscaped( $nearest_school, true ).'%', false );
			}
			*/
		
			
		
			$ptype = array();
		
			if($ticked_hdb) {
				$ptype[] = ' a.property_type LIKE '.$db->Quote( '%'.$db->getEscaped('组屋', true ).'%', false );
			}
		
			if($ticked_condo) {
				$ptype[] = ' a.property_type LIKE '.$db->Quote( '%'.$db->getEscaped('公寓', true ).'%', false );
			}
		
			$ptype 		=	( count ( $ptype ) ? ' ( '.implode( ' OR ', $ptype ).' ) ' : '' ) ;
		
			if(!empty($ptype))
				$where[] = $ptype;
		
			$join_img	=	' ';
		
			$join_floorplan = '';
		
			/* FLOORPLAN */
		
			if($ticked_fp) {
				$join_floorplan =  ' LEFT JOIN #__ihouse_ads_floorplan AS fp ON fp.ads_id = a.id ';
				$where[] = " fp.ordering = 1 ";
			}
		}
		
		$where[]	=	" cb.status = 'A' "; 	/* FIND ACTIVE SUBSCRIPTION */
		$where[]	=	" a.publish = 1 "; 		/* PUBLISH ADS */
		
		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );

		$obj	=	new StdClass;
		
		switch($layout) {
			case 'default' 			:
			default 				:
				
				

				if($adv_search || $from) {
					 
					
					$query	=	' SELECT '
						. ' a.*, '
						. ' u.name, u.chinese_name, u.company_name ,u.company_logo ,u.mobile_contact, u.website,u.user_image,u.user_category,u.cea_reg_no,u.email  ' 
							.	' FROM #__ihouse_ads AS a '
							.	' LEFT JOIN #__ihouse_property AS p ON p.id = a.property_id '
							.	' LEFT JOIN #__users AS u ON u.id = a.posted_by '
							.	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = a.subscription_id "
							.	$join_property
							. 	$join_floorplan
							.	$where
							.	' '
							.	$orderby;

						$db->setQuery( $query , $limitstart, $limit);
		
						$rows = $db->loadObjectList();
		
					if ($db->getErrorNum())
					{
						echo $db->stderr();
						return false;
					}

					$query	=	'SELECT COUNT(a.id) ' 
							.	' FROM #__ihouse_ads AS a '
							.	' LEFT JOIN #__ihouse_property AS p ON p.id = a.property_id '
							.	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = a.subscription_id "
							.	' LEFT JOIN #__users AS u ON u.id = a.posted_by '
							.	' '
							.	$where
							.	$orderby;
						
						$db->setQuery( $query );
						
						$total = $db->loadResult();

				} 
					
				$obj->rows	=	$rows;
				$obj->total = 	$total;

				break;
		}
		
		return $obj;		
			
	}
	
	function setPrice($price, $mode = 'rent') 
	{	
		if($mode == 'rent') {
			if($price == 1)  {
				$value		= 500;
			} 
			else if($price == 2) {
				$value		= 1000;
			}
			else if($price == 3) {
				$value		= 1500;
			}
			else if($price == 4) {
				$value		= 2000;
			}
			else if($price == 5) {
				$value		= 2500;
			}
			else if($price == 6) {
				$value		= 3000;
			}
			else if($price == 7) {
				$value		= 3500;
			}
			else if($price == 8) {
				$value		= 4000;
			}
			else if($price == 9) {
				$value		= 5000;
			}
			else if($price == 10) {
				$value		= 6000;
			}
			else if($price == 11) {
				$value		= 7000;
			}
			else if($price == 12) {
				$value		= 8000;
			}
			else if($price == 13) {
				$value		= 9000;
			}
			else if($price == 14) {
				$value		= 10000;
			}
			else if($price == 15) {
				$value		= 12000;
			}
			else if($price == 16) {
				$value		= 15000;
			}
			else if($price == 17) {
				$value		= 20000;
			}
			else if($price == 18) {
				$value		= 30000;
			}
			else if($price == 19) {
				$value		= 40000;
			}
			else if($price == 20) {
				$value		= 50000;
			}
		}
		
		if($mode == 'condo') {
			if($price == 1)  {
				$value		= 500000;
			} 
			else if($price == 2) {
				$value		= 600000;
			}
			else if($price == 3) {
				$value		= 700000;
			}
			else if($price == 4) {
				$value		= 850000;
			}
			else if($price == 5) {
				$value		= 1000000;
			}
			else if($price == 6) {
				$value		= 1200000;
			}
			else if($price == 7) {
				$value		= 1400000;
			}
			else if($price == 8) {
				$value		= 1700000;
			}
			else if($price == 9) {
				$value		= 2000000;
			}
			else if($price == 10) {
				$value		= 2200000;
			}
			else if($price == 11) {
				$value		= 2500000;
			}
			else if($price == 12) {
				$value		= 3000000;
			}
			else if($price == 13) {
				$value		= 3500000;
			}
			else if($price == 14) {
				$value		= 4000000;
			}
			else if($price == 15) {
				$value		= 4500000;
			}
			else if($price == 16) {
				$value		= 5000000;
			}
		}
		
		if($mode == 'hdb') {
			if($price == 1)  {
				$value		= 300000;
			} 
			else if($price == 2) {
				$value		= 350000;
			}
			else if($price == 3) {
				$value		= 400000;
			}
			else if($price == 4) {
				$value		= 450000;
			}
			else if($price == 5) {
				$value		= 500000;
			}
			else if($price == 6) {
				$value		= 600000;
			}
			else if($price == 7) {
				$value		= 700000;
			}
			else if($price == 8) {
				$value		= 800000;
			}
		}
		
		return $value;
	}
	
}