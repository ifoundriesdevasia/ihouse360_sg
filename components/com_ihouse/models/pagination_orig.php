﻿<?php
/**
* @version		$Id: poll.php 10752 2008-08-23 01:53:31Z eddieajau $
* @package		Joomla
* @subpackage	Polls
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class IhouseModelPagination extends JModel
{
	function __construct()
	{
		global $mainframe;

		parent::__construct();

	}
	
	function getPagination($total, $limit) {
		
		$db 			= $this->getDBO();
		
		$page			=	JRequest::getVar('page', 0);
		$property_id	=	JRequest::getVar('property_id', 0);
		
		$how_many		=	$total / $limit; 
		
		
		
		$html 			= 	'';
		
		$html .= '<span class="pagination">';
		
		for($i = 0; $i < $how_many; $i++) {
			$html .= '
    			<strong style="padding:0 5px;">';
			
			if($page == $i)
				$html .= '<span id="inactive">';
			else
				$html .= '<a title="'.($i+1).'" href="javascript:getPropertyReview('.$i.','.$property_id.');">';
			
			
			$html .=	($i+1);
			
			
			if($page == $i)
				$html .= '</span>';
			else
				$html .= '</a>';
               
		}
        $html .= '</span>';
			
		return $html;		
			
	}
	
	function getPaginationAdsSearch($total, $limit, $usercat) {
		$db 			= $this->getDBO();
		
		$page			=	JRequest::getVar('page', 0);
		
		$how_many		=	$total / $limit; 

		$filter				=	JRequest::getVar('filter', '');
		$orderfilter		=	JRequest::getVar('orderfilter', '');
		
		if(!empty($filter) || !empty($orderfilter)) {
			$orderby	= " ORDER BY ".$filter." ".$orderfilter." ";
		}

		$html 			= 	'';
		$html_left		=	'';
		$html_right		=	'';
		
		$html .= '<span class="pagination">';
		
		if($page-10 >= 0) {
			$html_left = '<a href="javascript:getAds(0,\''.$usercat.'\',\''.$filter.'\',\''.$orderfilter.'\');"><<</a>&nbsp;&nbsp;<a href="javascript:getAds('.($page-10).',\''.$usercat.'\',\''.$filter.'\',\''.$orderfilter.'\');"><</a>';
		}
		
		if($page+10 < $how_many) {
		$html_right = '<a href="javascript:getAds('.($page+10).',\''.$usercat.'\',\''.$filter.'\',\''.$orderfilter.'\');">></a>&nbsp;&nbsp;<a href="javascript:getAds('.ceil($how_many-1).',\''.$usercat.'\',\''.$filter.'\',\''.$orderfilter.'\');">>></a>';
		}
		
		if($page == 0)
			$html_left = '';
		
		if($page == ceil($how_many-1))
			$html_right = '';
		
		if($page <= 5) {
			$min_page = 0;
			if($how_many >= 10)
				$max_page = 10;
			else
				$max_page = $how_many;
				
		} else if($page >= ($how_many - 5)){
			$min_page = $page - 5;
			$max_page = $how_many;
		} else {
			$min_page = $page - 5;
			$max_page = $page + 5;
		}
		

		$html .= $html_left;
		for($i = $min_page; $i < $max_page; $i++) {
			$html .= '
    			<strong style="padding:0 5px;">';
			
			
			
			if($page == $i)
				$html .= '<span id="inactive">';
			else
				$html .= '<a title="'.($i+1).'" href="javascript:getAds('.$i.',\''.$usercat.'\',\''.$filter.'\',\''.$orderfilter.'\');">';
			
			
			$html .=	($i+1);
			
			
			if($page == $i)
				$html .= '</span>';
			else
				$html .= '</a>';
				
			$html .= '</strong>';	
            
			
		}
		$html .= $html_right;
        $html .= '</span>&nbsp;&nbsp;';
		$html .= ($page+1).' of '.ceil($how_many).' pages';	
		return $html;	
	}
	
	function getPaginationAdsMapSearch($total, $limit) {
		$db 			= $this->getDBO();
		
		$page			=	JRequest::getVar('page', 0);
		$property_id	=	JRequest::getVar('property_id', 0);
		
		$bedroom				= 	JRequest::getVar('bedroom', '');
		$prices					= 	JRequest::getVar('price', '');
		$prices_max				=	JRequest::getVar('price_max','');
		$propertytype			=	JRequest::getVar('proptype','');
		$margin_amt_percent		=	JRequest::getVar('amount', 0); 
		$budget					=	JRequest::getVar('budget', 0);
		$keyword				= 	JRequest::getVar('keyword', '');
		
		$how_many		=	$total / $limit; 
		
		$totalpage		=	ceil($how_many);
		
		//$filter				=	JRequest::getVar('filter', '');
		//$orderfilter		=	JRequest::getVar('orderfilter', '');
		
		//if(!empty($filter) || !empty($orderfilter)) {
		//	$orderby	= " ORDER BY ".$filter." ".$orderfilter." ";
		//}

		$html 			= 	'';
		
		if($totalpage > 0) {
			$html .= '<span class="pagination">';
		
			if($page > 0) {	
				$html .= '<a class="linkage" href="javascript:map_ads_page(\''.($page-1).'\',\''.$property_id.'\', 0, \''.$bedroom.'\', \''.$prices.'\',\''.$prices_max.'\', \''.$keyword.'\',\''.$propertytype.'\',\''.$budget.'\',\''.$margin_amt_percent.'\');">上一页</a>';
			}
			
			$html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . ($page+1). '/' .$totalpage. '页&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			
			if($page < ($totalpage - 1)) {
				$html .= '<a class="linkage" href="javascript:map_ads_page(\''.($page+1).'\',\''.$property_id.'\', 0, \''.$bedroom.'\', \''.$prices.'\',\''.$prices_max.'\', \''.$keyword.'\' , \''.$propertytype.'\',\''.$budget.'\',\''.$margin_amt_percent.'\');">下一页</a>';
			}
      
        	$html .= '</span>';
		}
		return $html;	
	}
}