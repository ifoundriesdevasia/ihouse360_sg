﻿<?php
/**
* @version		$Id: poll.php 10752 2008-08-23 01:53:31Z eddieajau $
* @package		Joomla
* @subpackage	Polls
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class IhouseModelMapsearchoption extends JModel
{
	function __construct()
	{
		global $mainframe;

		parent::__construct();

	}

	/*
	function loadHTML() {
		$db 			= $this->getDBO();
		
		$category	= JRequest::getVar('category', '');
		$level		= JRequest::getVar('level', 1);
		$type		= JRequest::getVar('type', ''); 
		
		$toggle		= '';
		
		if($level == 2) {
		}
		
		switch($level) {
			case 1:
				switch($category) {
					case 'school' :
						$query	= ' SELECT DISTINCT type FROM #__ihouse_school LIMIT 0 , 30 ';
							$db->setQuery( $query );
							$rows = $db->loadObjectList();
						break;
					case 'station':
						$query	= ' SELECT DISTINCT district FROM #__ihouse_station LIMIT 0 , 30 ';
							$db->setQuery( $query );
							$rows = $db->loadObjectList();
						break;
				}
				break;
			case 2:
				switch($category) {
					case 'school' :
						$toggle	= 'type';
						$query	= " SELECT * FROM #__ihouse_school WHERE type = '$type' ";
							$db->setQuery( $query );
							$rows = $db->loadObjectList();
						break;
					case 'station':
						$toggle = 'district';
						$query	= " SELECT * FROM #__ihouse_station WHERE district = '$type' ";
							$db->setQuery( $query );
							$rows = $db->loadObjectList();
						break;
				}
				break;
		}
		
		$html = '';
		
		foreach($rows as $r) :
			if($category == 'school') {
				if($level == 1) {
				
					
				} else {
					
				}
			} else if($category == 'station' && !empty($r->district)) {
				if($level == 1) {
					$html .= '<div class="paddinglist-map"><a href="#" class="optionlist">D'.$r->district.'</a></div>';	
					$html .= '<div class=""><a href="#" class="optionlist">test</a></div>';
				} else {
				}
			}
		endforeach;
		
		return $html;
	}
	*/
	function getListHTML() {
		$db 			= $this->getDBO();
		
		$type		= JRequest::getVar('type', ''); 
		$value		= JRequest::getVar('value', ''); 
		
		if($type == 'school') {
			$query	= " SELECT * FROM #__ihouse_school WHERE type = '$value' ";
			
				$db->setQuery( $query );
				$rows = $db->loadObjectList();
		} 
		
		if($type == 'station') {
			$query	= " SELECT * FROM #__ihouse_station " 
						. " WHERE code LIKE '%".$value."%' ORDER BY seq ASC ";
						
				$db->setQuery( $query );
				$rows = $db->loadObjectList();
		}
		
		$html = '';
		$html = "<select onchange=\"javascript:repositionGoogleMap('".$type."','".JRoute::_("index.php?option=com_ihouse&task=reposition_maps")."',this.value);return false;\" >";
		$html .= '<option value="" >---</option>';
		
		foreach($rows as $r) :
		
			if($type == 'station') 
				$html .= '<option value="'.$r->name_ch.'" >'.$r->code.' '.$r->name_en.' '.$r->name_ch.'</option>';
			else if($type == 'school')
				$html .= '<option value="'.$r->id.'" >'.$r->name_en.' '.$r->name_ch.'</option>';
				
		endforeach;
		
		$html .= '</select>';
		
		return $html;
	}

}