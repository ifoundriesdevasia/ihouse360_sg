﻿<?php
/**
* @version		$Id: poll.php 10752 2008-08-23 01:53:31Z eddieajau $
* @package		Joomla
* @subpackage	Polls
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class IhouseModelNonlandedpriceindex extends JModel
{
	function __construct()
	{
		global $mainframe;

		parent::__construct();

	}
	
	function getImage() {
		
		$db = $this->getDBO();
		
		$graph_path = JRoute::_('images/ihouse/nonlandedpriceindex.jpg');
		$result 	=  '<img src="'.$graph_path.'" alt="nonlandedpriceindex" />';
		
		/*if(file_exists($graph_path)) {
			return $result;
		}
		*/
		$db			=& JFactory::getDBO();
		
		$query = 	" SELECT * FROM #__ihouse_non_landed_price_index ";
			
				$db->setQuery( $query );
				$data = $db->loadObjectList();
		
		$arrTemp 	= array();
		$timeline	= array();
		
		
		foreach($data as $d) :
			$arrTemp['all'][] = (float) $d->all / 250 * 100;
			$arrTemp['ccr'][] = (float) $d->ccr / 250 * 100;
			$arrTemp['ocr'][] = (float) $d->ocr / 250 * 100;
			$arrTemp['rcr'][] = (float) $d->rcr / 250 * 100;
			$timeline[]	=	$d->timeline;
		endforeach;

		$total_x	=	count($timeline);
		$total_y	=	5;

		$point_x = array();
		$point_y = array();
		
		for($i = 1; $i <= $total_x; $i++) :
			$point_x[] = (float) $i / $total_x * 100;
		endfor;

		$timeline 	= 	implode('|',$timeline);
		//$timeline	=	mb_ereg_replace('/\((.*)\)/u','', $timeline);
		$point_x	=	implode(',',$point_x);
		
		$point_y['all']	=	implode(',',$arrTemp['all']);
		$point_y['ccr']	=	implode(',',$arrTemp['ccr']);
		$point_y['ocr']	=	implode(',',$arrTemp['ocr']);
		$point_y['rcr']	=	implode(',',$arrTemp['rcr']);
		
		
		$url = "http://chart.apis.google.com/chart?chtt=Non+Landed+Index+Price&cht=lc&chd=t:".$point_y['all']."|".$point_y['ccr']."|".$point_y['ocr']."|".$point_y['rcr']."&chs=640x400&chco=FF0000,00FF00,0000FF,000000&chdl=All（全部）|CCR（中央核心区域）|OCR（核心区域以外）|RCR（其它核心区域）&chxt=x,y&chxl=1:|0|50|100|150|200|250|0:|".$timeline."|";
			
			echo $url;
		

			
			$ch = curl_init();
			curl_setopt( $ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1" );
    		curl_setopt( $ch, CURLOPT_URL, $url );
			curl_setopt( $ch, CURLOPT_HEADER, 0);
			curl_setopt( $ch, CURLOPT_ENCODING , "UTF-8");
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1); 
			
			$content = curl_exec( $ch );
			
			$file = fopen($graph_path, 'w+');

        	fwrite($file, $content);

        	fclose($file); 
			
			
		return true;		
			
	}
}