<?php
/**
* @version		$Id: poll.php 10752 2008-08-23 01:53:31Z eddieajau $
* @package		Joomla
* @subpackage	Polls
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class IhouseModelSearch extends JModel
{
	function __construct()
	{
		global $mainframe;

		parent::__construct();

	}
	
	function getProperty() {
		
		$db = $this->getDBO();
		
		$keyword			=	strtolower(JRequest::getVar('keyword', ''));

		if(!$keyword) 
			return false;
		
		$where		=	array();
		$whereor	=	array();
		
		$whereor[] = ' LOWER(name_en) LIKE '.$db->Quote( '%'.$db->getEscaped( $keyword, true ).'%', false ).' ';
		$whereor[] = ' LOWER(name_ch) LIKE '.$db->Quote( '%'.$db->getEscaped( $keyword, true ).'%', false ).' ';
			
		$whereor 		= ( count( $whereor ) ? ' ( '.implode( ' OR ', $whereor ) .' ) ': '' );
		
		$where[]		=	$whereor;
		
		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
		
		$query = 	" SELECT DISTINCT name_ch, name_en FROM #__ihouse_property " 
						.	$where
						.	" ORDER BY name_en ASC "
						.	" LIMIT 15 ";
			
				$db->setQuery( $query );
				$lists = $db->loadObjectList();
		
		$html = '';
			
		foreach($lists as $l):
		
			$html .= '<div name="'.$l->name_en.'" class="property_search" style="padding:2px 5px 2px 5px">'.($l->name_ch?$l->name_ch.' ':'').$l->name_en.'</div>';
				
		endforeach;
			
		return $html;		
			
	}
}