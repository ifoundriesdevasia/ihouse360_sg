<?php
/**
* @version		$Id: poll.php 10752 2008-08-23 01:53:31Z eddieajau $
* @package		Joomla
* @subpackage	Polls
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class IhouseModelContact extends JModel
{
	function __construct()
	{
		global $mainframe;

		parent::__construct();

	}
	
	function SendEmail() {
		global $mainframe;
		
		$db = $this->getDBO();
		$uid		=	JRequest::getVar('uid','');
		$email		=	JRequest::getVar('email','');
		
		$name		=	JRequest::getVar('name','');
		$email_to	=	JRequest::getVar('email_to_contact','');
		$mobile		=	JRequest::getVar('mobile','');
		$message	=	JRequest::getVar('message','');
		$subject	=	JRequest::getVar('subject','');
		
		$ads_id		=	JRequest::getVar('ads_id','');

		$subject 	= 'iHouse360.com - Customer enquiry';

		$MailFrom 	= 	$mainframe->getCfg('mailfrom');
		$FromName 	= 	$mainframe->getCfg('fromname');
		
		if($ads_id) {
			$query		=	" SELECT * FROM #__ihouse_ads "
						.	" WHERE id = '$ads_id' ";		
						
				$db->setQuery($query);
				$ad = $db->loadObject();
		}

		$query		=	" SELECT * FROM #__users "
						.	" WHERE id = '$uid' ";		
						
				$db->setQuery($query);
				$user = $db->loadObject();
		
		$body 		= '';
		
		$body		.=	'<div><img src="cid:logo_id" alt="logo" /></div>';
		$body		.=	'<div>';
		$body		.=	'Dear '.( ($user->salute)? $user->salute.' ' : '' ).$user->name.' ,<br />';
		$body		.=	'<br />'.$name.' has requested you to contact them ';
		
		if($ads_id) {
			$body		.=	'regarding the following listing: ';
			$body		.=	'<br /><br /><a href="'.JURI::root().'index.php?option=com_ads&view=adsdetails&id='.$ad->id.'">'.$ad->ad_title.'</a>('.$ad->property_name.') S$'.number_format($ad->ask_price);
		} else {
			$body		.=	'regarding any enquiries: ';
		}
		
		$body		.=	'<br /><br /><strong>Details on their enquiry are below:</strong><br />';
		
		$body		.=	'<br />Email Address: ';
		$body		.=	'<a href="mailto:'.$email_to.'">'.$email_to.'</a>';

		$body		.=	'<br />Mobile No : ';
		$body		.=	$mobile;
		$body		.=	'<br /><br /><strong>Message : </strong><br />';
		$body		.=	nl2br($message);
		// Prepare email body
		

		$body	.=	"<br /><br />Please remember to mention \"<a href=\"".JURI::root()."\">iHouse360.com</a>\" when you contact the customer";
		$body	.=	"<br /><br />Best regards,";
		$body	.=	"<br />iHouse360.com - iHouse Property";
		$body	.=	'</div>';
		$body 	= "\r\n\r\n".stripslashes($body);

		$mail = JFactory::getMailer();
		
		$mail->isHTML(true);

		$mail->addRecipient( $email );
		$mail->addReplyTo(array($email_to, $name));
		$mail->setSender( array( 'webmaster@ihouse360.com', $FromName ) );
		$mail->setSubject( $FromName.': '.$subject );
		$mail->AddEmbeddedImage( JPATH_SITE.DS.'templates'.DS.'mapsearch'.DS.'images'.DS.'logo_100.png', 'logo_id', 'logo.png', 'base64', 'image/png' );
		$mail->setBody( $body );

		//update#gunmail
		$conf =& JFactory::getConfig();
		if($conf->getValue('config.mailer') == 'smtp'){
			$mail->useSMTP(
			$conf->getValue('config.smtpauth'), 
			$conf->getValue('config.smtphost'), 
			$conf->getValue('config.smtpuser'), 
			$conf->getValue('config.smtppass'), 
			$conf->getValue('config.smtpsecure'), 
			$conf->getValue('config.smtpport')
			);
		}
		//update#end		
		
		
		$sent = $mail->Send();
		
		return true;
	}
	
	function SendEmailNotExist() {
		global $mainframe;
		
		$db = $this->getDBO();
		$uid		=	JRequest::getVar('uid','');
		
		$email		=	JRequest::getVar('email','');
		
		$subject 	= 'iHouse360.com - List Not Exist';

		$MailFrom 	= 	$mainframe->getCfg('mailfrom');
		$FromName 	= 	$mainframe->getCfg('fromname');
		
		$message	=	JRequest::getVar('message','');
		
		$query		=	" SELECT * FROM #__users "
						.	" WHERE id = '$uid' ";		
						
				$db->setQuery($query);
				$user = $db->loadObject();
		
		$body 		= '';
		
		$body		.=	'<div><img src="cid:logo_id" alt="logo" /></div>';
		$body		.=	'<div>';
		$body		.=	'Dear Administrator ,';
		
		$body		.=	'<br /><br />Details on their enquiry are below:<br />';
		
		$body		.=	'<br />Email Address: ';
		$body		.=	'<a href="mailto:'.$user->email.'">'.$user->email.'</a>';

		$body		.=	'<br />Message : <br />';
		$body		.=	nl2br($message);
		// Prepare email body
		
		$body	.=	"<br /><br />Best regards,";
		$body	.=	"<br />iHouse360.com - iHouse Property";
		$body	.=	'</div>';
		$body 	= "\r\n\r\n".stripslashes($body);
		
		$mail = JFactory::getMailer();
		
		$mail->isHTML(true);

		$mail->addRecipient($email);
		$mail->addReplyTo(array($user->email, $user->name));
		$mail->setSender( array( 'webmaster@ihouse360.com', $FromName ) );
		$mail->setSubject( $FromName.': '.$subject );
		$mail->AddEmbeddedImage( JPATH_SITE.DS.'templates'.DS.'mapsearch'.DS.'images'.DS.'logo_100.png', 'logo_id', 'logo.png', 'base64', 'image/png' );
		$mail->setBody( $body );
	
		//update#gunmail
		$conf =& JFactory::getConfig();
		if($conf->getValue('config.mailer') == 'smtp'){
			$mail->useSMTP(
			$conf->getValue('config.smtpauth'), 
			$conf->getValue('config.smtphost'), 
			$conf->getValue('config.smtpuser'), 
			$conf->getValue('config.smtppass'), 
			$conf->getValue('config.smtpsecure'), 
			$conf->getValue('config.smtpport')
			);
		}
		//update#end	

		$sent = $mail->Send();
		
		return true;
	}
}