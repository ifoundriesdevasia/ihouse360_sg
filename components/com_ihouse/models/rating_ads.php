﻿<?php
/**
* @version		$Id: poll.php 10752 2008-08-23 01:53:31Z eddieajau $
* @package		Joomla
* @subpackage	Polls
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class IhouseModelRating_ads extends JModel
{
	function __construct()
	{
		global $mainframe;

		parent::__construct();

	}
	
	function submitAdsUserRating() {
		$db 				= 	$this->getDBO();
		
		$user				= 	JFactory::getUser();
		$userid				= 	$user->get('id');
		
		$ads_id		=	JRequest::getVar('ads_id', 0);
		
		$query = "SELECT user_id FROM #__ihouse_ads_rating WHERE user_id = '$userid' AND ads_id = '$ads_id' ";
			$db->setQuery( $query );
			$uid = $db->loadResult();
		
		if(!empty($uid))
			return false;
		
		$query = "SELECT street_name FROM #__ihouse_ads WHERE id = '$ads_id' LIMIT 1 ";
			$db->setQuery( $query );
			$street_name = $db->loadResult();
		
		$query = "SELECT user_id FROM #__ihouse_ads_rating WHERE user_id = '$userid' AND street_name = '$street_name' ";
			$db->setQuery( $query );
			$uids = $db->loadResult();
			
		if(!empty($uids))
			return false;
		
		$query = "SELECT property_type, street_name FROM #__ihouse_ads WHERE id = '$ads_id' LIMIT 1 ";
			$db->setQuery( $query );
			$row = $db->loadObject(); /* condo or hdb in chinese character */
		
		$where	=	array();
		
		if($row->property_type == '组屋') {
			$street_name		=	$row->street_name;
			$ads_id				=	0;
			$where[]			=	" street_name = '".$row->street_name."' ";
		} else {
			$street_name		=	'';
			$where[]			=	" ads_id = '$ads_id' ";
		}
		
		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
		
		$transportation		=	JRequest::getVar('trans', 0);
		$environment		=	JRequest::getVar('environment', 0);
		$design				=	JRequest::getVar('design', 0);
		$facility			=	JRequest::getVar('facility', 0);
		$developer			=	JRequest::getVar('developer', 0);

		/* INSERTING INTO DB INDIVIDUAL */
		
		$query = "INSERT INTO #__ihouse_ads_rating VALUES(NULL,'$userid','$ads_id','$street_name','$transportation','$environment','$design','$facility','$developer',CURRENT_TIMESTAMP); ";
			
			$db->setQuery( $query );
			$db->query();
		
		$query = "SELECT * FROM #__ihouse_ads_rating_avg " 
					.	$where ;
					
			$db->setQuery( $query );
			$result = $db->loadObject();
		
		if(!empty($result)) {
			
			$total 				= 	$result->total + 1;
			$transport_avg 		= 	$result->transport_avg + (float) $transportation;
			$environment_avg 	= 	$result->environment_avg + (float) $environment;
			$design_avg			=	$result->design_avg + (float) $design;
			$facility_avg		=	$result->facility_avg + (float) $facility;;
			$developer_avg		=	$result->developer_avg + (float) $developer;
			$overall_avg		=	(($transport_avg + $environment_avg + $design_avg + $facility_avg + $developer_avg) / $total) / 5;
			
			$query = "UPDATE #__ihouse_ads_rating_avg  "
						. 	" SET total = '$total', "
						. 	" transport_avg = '$transport_avg', "
						. 	" environment_avg = '$environment_avg', "
						.	" design_avg = '$design_avg', "
						.	" facility_avg = '$facility_avg', "
						.	" developer_avg = '$developer_avg', "
						.	" overall_avg = '$overall_avg' "
						.	$where;
			
				$db->setQuery( $query );
				$db->query();
			
		} else {
			
			$overall_avg	=	($transportation + $environment + $design + $facility + $developer) / 5;	
			
			$query = "INSERT INTO #__ihouse_ads_rating_avg VALUES(NULL,'$ads_id','$street_name','$transportation','$environment','$design','$facility','$developer','$overall_avg',1,CURRENT_TIMESTAMP); ";
			
			$db->setQuery( $query );
			$db->query();
		}
		
		return true;
	}
	
	function getAdsUserRating() {
		$db 				= 	$this->getDBO();
		
		$ads_id	=	JRequest::getVar('ads_id');
		
		$query = "SELECT property_type, street_name FROM #__ihouse_ads WHERE id = '$ads_id' LIMIT 1 ";
			$db->setQuery( $query );
			$row = $db->loadObject(); /* condo or hdb in chinese character */
		
		if($row->property_type == '组屋') {
			$where[]			=	" street_name = '".$row->street_name."' ";
		} else {
			$where[]			=	" ads_id = '$ads_id' ";
		}
		
		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
		
		$query = " SELECT * FROM #__ihouse_ads_rating_avg " 
					.	$where;
			$db->setQuery( $query );
			$result = $db->loadObject();
		
		return $result;
	}

	function getStarAdsUserRating($rating) {
		//global $mainframe;
		
		$star = 0;
		if($rating > 0 && $rating <= 20)
			$star = 1;
		else if($rating > 20 && $rating <= 40)
			$star = 2;
		else if($rating > 40 && $rating <= 60)
			$star = 3;
		else if($rating > 60 && $rating <= 80)
			$star = 4;
		else if($rating > 80 && $rating <= 100)
			$star = 5;	
		
		
		$html = '';
		for($i = 0; $i < $star; $i++) {
			$html .= '<img src="'. JURI::root().'templates/main/images/star.png'. '" />';
		}
		
		for($i = 5; $i > $star; $i--) {
			$html .= '<img src="'. JURI::root().'templates/main/images/star-blank.png'. '" />';
		}

		return $html;
	}
	
}
