<?php
/**
* @version		$Id: poll.php 10752 2008-08-23 01:53:31Z eddieajau $
* @package		Joomla
* @subpackage	Polls
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class IhouseModelPrime_properties extends JModel
{
	function __construct()
	{
		global $mainframe;

		parent::__construct();

	}

	function getRent()
    {
		$db 				= 	$this->getDBO();

		$orderby	= ' ORDER BY a.posting_date DESC '; 

		$query			= 	"SELECT a.* FROM #__ihouse_ads_type AS t "
							.	" LEFT JOIN #__ihouse_ads AS a ON a.id = t.ads_id "
							.	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = t.subscription_id "
							.	" WHERE t.ads_type_config = 4 "
							//.	" AND cb.expiry_date >= '" . date('Y-m-d H:i:s', time()) . "' "
							.	" AND cb.status = 'A' "
							. 	" AND a.ad_type = 'rent' "
							.	" AND a.publish = 1 "
							.	$orderby
							.	" LIMIT 3 ";			
				
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
			
		return $rows;
    }
	
	function getSell() {
		$db 				= 	$this->getDBO();

		$orderby	= ' ORDER BY a.posting_date DESC '; 

		$query			= 	"SELECT a.* FROM #__ihouse_ads_type AS t "
							.	" LEFT JOIN #__ihouse_ads AS a ON a.id = t.ads_id "
							.	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = t.subscription_id "
							.	" WHERE t.ads_type_config = 4 "
							//.	" AND cb.expiry_date >= '" . date('Y-m-d H:i:s', time()) . "' "
							.	" AND cb.status = 'A' "
							. 	" AND a.ad_type = 'sell' "
							.	" AND a.publish = 1 "
							.	$orderby
							.	" LIMIT 3 ";			
				
			$db->setQuery( $query ) ;
			$rows = $db->loadObjectList();
			
		return $rows;
	}
	
}
