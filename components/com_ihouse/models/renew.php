<?php
/**
* @version		$Id: poll.php 10752 2008-08-23 01:53:31Z eddieajau $
* @package		Joomla
* @subpackage	Polls
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class IhouseModelRenew extends JModel
{
	function __construct()
	{
		global $mainframe;

		parent::__construct();

	}
	function renewal() {
		$db = $this->getDBO();
	
		$subs_id	=	JRequest::getVar('subs_id', '');
		
		$query = " SELECT * FROM #__cbsubs_subscriptions WHERE id = '$subs_id' ";
			$db->setQuery( $query );
			$s	=	$db->loadObject();
		
		$planid	=	$s->plan_id;
		$userid	=	$s->user_id;
		
		$query = " SELECT * FROM #__cbsubs_plans WHERE id = '$planid' ";
			$db->setQuery( $query );
			$t	=	$db->loadObject();
			
		$validity 	= 	$t->validity;
		$deduct_by	=	(int) $t->rate;

		$date = explode("-", $validity);
		$year = $date[0];
		$month = $date[1];
		$day = substr($date[2], 0, 2);

		$expirydate = date("Y-m-d H:i:s", mktime(date("H"),date("i")+1,date("s"),date("m")+$month,date("d")+$day,date("Y")+$year));
		$last_renew	=	date("Y-m-d H:i:s", time());
		
		$query = " UPDATE #__cbsubs_subscriptions "
					.	" SET subscription_date = '$last_renew', "
					.	"	last_renewed_date = '$last_renew' , " 
					.	"	expiry_date = '$expirydate' " 
					.	" WHERE id = '$subs_id' ";
			
			$db->setQuery( $query );
			$db->query();
		
		$query = " UPDATE #__ihouse_users_credit SET credit = credit - ". $deduct_by
					. " WHERE user_id = '$userid' ";
					
			$db->setQuery( $query );
			$db->query();
		return true;
	}
	
	
	
}
