﻿<?php
/**
* @version		$Id: poll.php 10752 2008-08-23 01:53:31Z eddieajau $
* @package		Joomla
* @subpackage	Polls
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class IhouseModelMapsearchads extends JModel
{
	function __construct()
	{
		global $mainframe;

		parent::__construct();

	}
	
	function loadAdsInMapSearchFiltered() {
		
		$db 			= $this->getDBO();
		
		$property_id			= 	JRequest::getVar('property_id', 0);
		
		
		$page			= JRequest::getVar('page', 0);
		$limit			= JRequest::getVar('limit', 5);
		$limitstart		= JRequest::getVar('limitstart', $page*$limit);
		
		
		
		$prop_cls		= '';
		
		$where			= $this->buildWheres();
		
		$orderby	=	" ORDER BY a.posting_date DESC ";
		
		$query	=	' SELECT '
						. ' a.* '
							.	' FROM #__ihouse_ads AS a '
							.	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = a.subscription_id "
							//.	$join_property
							//. $join_floorplan
							.	$where
							.	' '
							.	$orderby;
						
						$db->setQuery( $query ,$limitstart, $limit);
		
					$rows = $db->loadObjectList();
		
		$query	=	' SELECT '
						. ' COUNT(*) '
							.	' FROM #__ihouse_ads AS a '
							.	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = a.subscription_id "
							//.	$join_property
							//. $join_floorplan
							.	$where
							.	' '
							.	$orderby;
						$db->setQuery( $query );
				$total	=	$db->loadResult();		

		$html = '';
		
		if(!empty($rows)) : 
			$html .= '<div>';
			$html .= '<div>
						<div style="width:50%;float:left;" class="static-text1">房源名</div>
						<div style="width:12%;float:left;" class="static-text1">户型</div>
						<div style="width:14%;float:left;" class="static-text1">面积(平方尺)</div>
						<div style="width:12%;float:left;" class="static-text1">地契(年)</div>
						<div style="width:12%;float:left;" class="static-text1">价格(S$)</div>
					  </div>
					  <div style="clear:both"></div>
			';		
			
			
			
			$i = 0;
			foreach($rows as $r) :
			
			$background_c = '';
			
			if($i % 2 == 0) {
				$background_c = 'background-color:#ccc;';
			}
			
			$html .= '
				<div style="padding:3px 0;'.$background_c.'" >
					<div style="width:50%;float:left;">
						<a class="linkage1" target="_blank" href="'.JRoute::_('index.php?option=com_ads&view=adsdetails&id='.$r->id.'&Itemid=135').'">'.$r->ad_title.'</a>
					</div>
					<div style="width:12%;float:left;" class="static-text1">'.$r->no_of_room.'房'.$r->no_of_hall.'厅</div>
					<div style="width:14%;float:left;" class="static-text1">
						'.(($r->size)?number_format($r->size):'-').'
					</div>

					<div style="width:12%;float:left;" class="static-text1">
						'.(($r->tenure)?$r->tenure:'-').'
					</div>
					<div style="width:12%;float:left;" class="static-text1">
						'.((number_format($r->ask_price))?number_format($r->ask_price):'-').'
					</div>
					<div style="clear:both"></div>
				 </div>	';
			$i++;	 
			endforeach;
			
			$html .= '</div>';

		else :
			
			$html .= 'No Ads';
			
		endif;
		
		$stuff = new StdClass();
		
		$stuff->result_html = $html;
		$stuff->total		= $total;
		
		return $stuff;
	}
	
	function getPricesAdsListHTML($pri) {
		$db 			= $this->getDBO();
		
		$property_id	= JRequest::getVar('property_id', 0);
		$bedroom		= JRequest::getVar('bedroom', '');
		$pri			= JRequest::getVar('price', '');
		
		$query	=	" SELECT property_type FROM #__ihouse_ads WHERE property_id = '$property_id' ";
					
			$db->setQuery( $query );
		$propertytype = $db->loadResult();
		
		$selected = ' selected="selected" ';
		
		$html = '';
		/*$html .= '<select name="prices" class="searchselect2" id="price" onchange="javascript:map_ads_page(0, \''.$property_id.'\', 0, \''.$bedroom.'\', this.value)">';
		$html .= '	<option value="">总价不限</option>';
		switch($propertytype) {
			case '公寓':
				$html .= '	<option value="1" '.(($pri == 1)? $selected :'').'>总价新币 50万以下</option>';
				$html .= '	<option value="2" '.(($pri == 2)? $selected :'').'>总价新币 60 万</option>';
				$html .= '	<option value="3" '.(($pri == 3)? $selected :'').'>总价新币 70 万</option>';
				$html .= '	<option value="4" '.(($pri == 4)? $selected :'').'>总价新币 85 万</option>';
				$html .= '	<option value="5" '.(($pri == 5)? $selected :'').'>总价新币 100 万</option>';
				$html .= '	<option value="6" '.(($pri == 6)? $selected :'').'>总价新币 120 万</option>';
				$html .= '	<option value="7" '.(($pri == 7)? $selected :'').'>总价新币 140 万</option>';
				$html .= '	<option value="8" '.(($pri == 8)? $selected :'').'>总价新币 170 万</option>';
				$html .= '	<option value="9" '.(($pri == 9)? $selected :'').'>总价新币 200 万</option>';
				$html .= '	<option value="10" '.(($pri == 10)? $selected :'').'>总价新币 220 万</option>';
				$html .= '	<option value="11" '.(($pri == 11)? $selected :'').'>总价新币 250 万</option>';
				$html .= '	<option value="12" '.(($pri == 12)? $selected :'').'>总价新币 300 万</option>';
				$html .= '	<option value="13" '.(($pri == 13)? $selected :'').'>总价新币 350 万</option>';
				$html .= '	<option value="14" '.(($pri == 14)? $selected :'').'>总价新币 400 万</option>';
				$html .= '	<option value="15" '.(($pri == 15)? $selected :'').'>总价新币 450 万</option>';
				$html .= '	<option value="16" '.(($pri == 16)? $selected :'').'>总价新币 500 万以上</option>';
				break;
			case '组屋':
				$html .= '	<option value="1" '.(($pri == 1)? $selected :'').'>总价新币 30 以下</option>';
				$html .= '	<option value="2" '.(($pri == 2)? $selected :'').'>总价新币 35 </option>';
				$html.= '	<option value="3" '.(($pri == 3)? $selected :'').'>总价新币 40 </option>';
				$html .= '	<option value="4" '.(($pri == 4)? $selected :'').'>总价新币 45 </option>';
				$html .= '	<option value="5" '.(($pri == 5)? $selected :'').'>总价新币 50 </option>';
				$html .= '	<option value="6" '.(($pri == 6)? $selected :'').'>总价新币 60 </option>';
				$html .= '	<option value="7" '.(($pri == 7)? $selected :'').'>总价新币 70 </option>';
				$html .= '	<option value="8" '.(($pri == 8)? $selected :'').'>总价新币 80 以上</option>';
				
				break;
		}
		$html .= '</select>';
		*/
		return $html;
	}
	
	function getBedroomAdsListHTML() {
		$db 			= $this->getDBO();
		
		$property_id	= JRequest::getVar('property_id', 0);
		
		$bedroom				= 	JRequest::getVar('bedroom', '');
		$prices					= 	JRequest::getVar('price', '');
		$prices_max				=	JRequest::getVar('price_max','');
		$propertytype			=	JRequest::getVar('proptype','');
		$amount					=	JRequest::getVar('amount', 0); 
		$budget					=	str_replace(',','',JRequest::getVar('budget', 0));
		
		$query	=	" SELECT property_type FROM #__ihouse_ads WHERE property_id = '$property_id' ";
					
			$db->setQuery( $query );
		$propertytype = $db->loadResult();
		
		
		$selected = ' selected="selected" ';
		
		$html = '';
		$html .= '<select name="bedroom" id="bedroom" class="searchselect" onchange="javascript:map_ads_page(0, \''.$property_id.'\', 0, this.value, \''.$prices.'\',\''.$prices_max.'\',\''.$keyword.'\',\''.$propertytype.'\',\''.$budget.'\', \''.$amount.'\')">';
		$html .= '	<option value="">户型不限</option>';
		switch($propertytype) {
			case '公寓':
				$html .= '	<option value="1" '.(($bedroom == 1)? $selected :'').'>1房</option>';
				$html .= '	<option value="2" '.(($bedroom == 2)? $selected :'').'>2房</option>';
				$html .= '	<option value="3" '.(($bedroom == 3)? $selected :'').'>3房</option>';
				$html .= '	<option value="4" '.(($bedroom == 4)? $selected :'').'>4房</option>';
				$html .= '	<option value="5" '.(($bedroom == 5)? $selected :'').'>5房以上</option>';			
				break;
			case '组屋':
				$html .= '	<option value="1" '.(($bedroom == 1)? $selected :'').'>1房</option>';
				$html .= '	<option value="2" '.(($bedroom == 2)? $selected :'').'>2房</option>';
				$html .= '	<option value="3" '.(($bedroom == 3)? $selected :'').'>3房</option>';
				$html .= '	<option value="4" '.(($bedroom == 4)? $selected :'').'>4房</option>';
				$html .= '	<option value="5" '.(($bedroom == 5)? $selected :'').'>5房</option>';
				$html .= '	<option value="6" '.(($bedroom == 6)? $selected :'').'>公寓式</option>';
				break;
		}
		$html .= '</select>';
		
		return $html;
	}
	
	function loadSpecialistsSearchFiltered() {
		
		$db 			= $this->getDBO();
		
		$property_id			= 	JRequest::getVar('property_id', 0);

		$prop_cls		= '';
		
		//$where			= $this->buildWheres();
		
		$query	=	' SELECT '
						. ' u.id, u.name, u.chinese_name '
						.	' FROM #__ihouse_ads_type AS a '
						.	' LEFT JOIN #__users AS u ON u.id = a.user_id '
						.	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = a.subscription_id "
						.	' WHERE a.ads_type_config = 1 '
						.	" AND cb.status = 'A' "
						.	" AND a.property_id = '$property_id' "
						.	' LIMIT 1 ';
						
						$db->setQuery( $query );
		
					$r = $db->loadObject();
		
		$html = '-';
		if(!empty($r)) {
			$html = '<a  class="linkage1" href="'.JRoute::_('index.php?option=com_user&view=agent_profile&id='.$r->id.'&Itemid=136').'">'.$r->name.$r->chinese_name.'</a>&nbsp;&nbsp;';
		}
		
		return $html;
	}
	
	
	/* **********************
		BUILD WHERES
	************************* */
	function buildWheres() {
		$db 					= $this->getDBO();
		$where					=	array();
		
		$property_id			= 	JRequest::getVar('property_id', 0);
		
		$keyword				=	JRequest::getVar('keyword', ''); // either chinese or english
		$bedroom				=	JRequest::getVar('bedroom', '');
		$price					=	JRequest::getVar('price', '');	
		$price_max				=	JRequest::getVar('price_max', '');
		$property_type			=	JRequest::getVar('proptype','');
		$margin_amt_percent		=	JRequest::getVar('amount', 0); 
		$budget					=	str_replace(',','',JRequest::getVar('budget', 0));
		$ap						=	$margin_amt_percent;
		$type					=	JRequest::getVar('type', 'all');
		
		switch($type) {
			case 'new'		:
				$where[]	=	" a.ad_type = 'sell' AND a.property_category = '新房' ";
				break;
			case 'second'	:
				$where[]	=	" a.ad_type = 'sell' AND a.property_category = '其它' "; /* other */
				break;
			case 'rent'		:
				$where[]	=	" a.ad_type = 'rent' ";
				break;
			default			:
				$where[] 	=	" 1 ";
				break;
		}
		
		if($property_type) {
			$where[] = " a.property_type = '$property_type' ";
			if($property_type == '公寓') {
				switch($bedroom) {
					case 1: /* 1房 */
						$where[] = " a.no_of_room = 1 ";
					break;
					case 2: /* 2房 */
						$where[] = " a.no_of_room = 2 ";
					break;
					case 3: /* 3房 */
						$where[] = " a.no_of_room = 3 ";
						break;
					case 4: /* 4房 */
						$where[] = " a.no_of_room = 4 ";
						break;
					case 5: /* 5房以上 */
						$where[] = " a.no_of_room >= 5 ";
					break;
				}
			} else if($property_type == '组屋') {
				switch($bedroom) {
					case 1: /* 1房 */
						$where[] = " a.no_of_room = 1 ";
					break;
					case 2: /* 2房 */
						$where[] = " a.no_of_room = 2 ";
					break;
					case 3: /* 3房 */
						$where[] = " a.no_of_room = 3 ";
						break;
					case 4: /* 4房 */
						$where[] = " a.no_of_room = 4 ";
						break;
					case 5: /* 5房 */
						$where[] = " a.no_of_room = 5 ";
					break;
					case 6: /* 公寓式 */
						$where[] = " a.no_of_room > 5";
				}
			}
		}
		
		if($budget) {
			if($margin_amt_percent > 0) {
				$min = $budget - ($budget * $margin_amt_percent / 100);
				$max = $budget + ($budget * $margin_amt_percent / 100);
				$where[] = " a.ask_price >= '$min' ";
				$where[] = " a.ask_price <= '$max' ";
			}
			else {
				$where[] = " a.ask_price = '$budget' ";
			}
		} else {
			/* SET PRICES */
			if($type	== 'rent') {
				$min = $this->setPrice($price, 'rent');
				$max = $this->setPrice($price_max, 'rent');
			}
			else
			{
				/* Condo */
				if($property_type == '公寓') {
					$min = $this->setPrice($price, 'condo');
					$max = $this->setPrice($price_max, 'condo');
				}
			} 
			
		
			if($ap && ($min > 0 || $max > 0)) {
				$min_x = $min - ($ap / 100 * $min);
				$max_x = $max + ($ap / 100 * $max);
				
				$where[] = " a.ask_price >= ". $min_x;
				$where[] = " a.ask_price <= ". $max_x;
			}
		}
		
		$where[]	=	" cb.status = 'A' "; 	/* FIND ACTIVE SUBSCRIPTION */
		$where[]	=	" a.publish = 1 "; 		/* PUBLISH ADS */
		$where[]	=	" a.property_id = '$property_id' ";
		
		if ($keyword && $keyword != '请输入华文或英文地名、邮编等等。')
		{
			$db = $this->getDBO();
			
			$query = ' SELECT postcode FROM #__ihouse_property WHERE LOWER(name_ch) LIKE '.$db->Quote( '%'.$db->getEscaped( $keyword, true ).'%', false );
				$db->setQuery($query);
				$rows = $db->loadObjectList(); 
			
			$whereor = array();
			$tmpwhere = ' ';
			if(!empty($rows)) {
				
				foreach($rows as $r) :
					$whereor[] = ' LOWER(a.postcode) LIKE '.$db->Quote( '%'.$db->getEscaped( $r->postcode, true ).'%', false ).' ';
				endforeach;
				
				
			}
			$tmpwhere 	= ' ' . ( count( $whereor ) ? ' OR '. implode( ' OR ', $whereor ) : '' ) . ' ' ;
			
			$where[] = ' ( LOWER(a.hdb_town_id) LIKE '.$db->Quote( '%'.$db->getEscaped( $keyword, true ).'%', false )
						.	' OR LOWER(a.property_name) LIKE '.$db->Quote( '%'.$db->getEscaped( $keyword, true ).'%', false )
						.	' OR LOWER(a.street_name) LIKE '.$db->Quote( '%'.$db->getEscaped( $keyword, true ).'%', false )
						.	' OR LOWER(a.postcode) LIKE '.$db->Quote( '%'.$db->getEscaped( $keyword, true ).'%', false )
						.	' OR LOWER(a.add_info) LIKE '.$db->Quote( '%'.$db->getEscaped( $keyword, true ).'%', false )
						.	$tmpwhere
						.	' ) ';
		} 
		
		$where 		= 	( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
		
		return $where;
	}
	
	function setPrice($price, $mode = 'rent') 
	{	
		if($mode == 'rent') {
			if($price == 1)  {
				$value		= 500;
			} 
			else if($price == 2) {
				$value		= 1000;
			}
			else if($price == 3) {
				$value		= 1500;
			}
			else if($price == 4) {
				$value		= 2000;
			}
			else if($price == 5) {
				$value		= 2500;
			}
			else if($price == 6) {
				$value		= 3000;
			}
			else if($price == 7) {
				$value		= 3500;
			}
			else if($price == 8) {
				$value		= 4000;
			}
			else if($price == 9) {
				$value		= 5000;
			}
			else if($price == 10) {
				$value		= 6000;
			}
			else if($price == 11) {
				$value		= 7000;
			}
			else if($price == 12) {
				$value		= 8000;
			}
			else if($price == 13) {
				$value		= 9000;
			}
			else if($price == 14) {
				$value		= 10000;
			}
			else if($price == 15) {
				$value		= 12000;
			}
			else if($price == 16) {
				$value		= 15000;
			}
			else if($price == 17) {
				$value		= 20000;
			}
			else if($price == 18) {
				$value		= 30000;
			}
			else if($price == 19) {
				$value		= 40000;
			}
			else if($price == 20) {
				$value		= 50000;
			}
		}
		
		if($mode == 'condo') {
			if($price == 1)  {
				$value		= 500000;
			} 
			else if($price == 2) {
				$value		= 600000;
			}
			else if($price == 3) {
				$value		= 700000;
			}
			else if($price == 4) {
				$value		= 850000;
			}
			else if($price == 5) {
				$value		= 1000000;
			}
			else if($price == 6) {
				$value		= 1200000;
			}
			else if($price == 7) {
				$value		= 1400000;
			}
			else if($price == 8) {
				$value		= 1700000;
			}
			else if($price == 9) {
				$value		= 2000000;
			}
			else if($price == 10) {
				$value		= 2200000;
			}
			else if($price == 11) {
				$value		= 2500000;
			}
			else if($price == 12) {
				$value		= 3000000;
			}
			else if($price == 13) {
				$value		= 3500000;
			}
			else if($price == 14) {
				$value		= 4000000;
			}
			else if($price == 15) {
				$value		= 4500000;
			}
			else if($price == 16) {
				$value		= 5000000;
			}
		}
		
		if($mode == 'hdb') {
			if($price == 1)  {
				$value		= 300000;
			} 
			else if($price == 2) {
				$value		= 350000;
			}
			else if($price == 3) {
				$value		= 400000;
			}
			else if($price == 4) {
				$value		= 450000;
			}
			else if($price == 5) {
				$value		= 500000;
			}
			else if($price == 6) {
				$value		= 600000;
			}
			else if($price == 7) {
				$value		= 700000;
			}
			else if($price == 8) {
				$value		= 800000;
			}
		}
		
		return $value;
	}
}