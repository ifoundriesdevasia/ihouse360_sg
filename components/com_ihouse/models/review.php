<?php
/**
* @version		$Id: poll.php 10752 2008-08-23 01:53:31Z eddieajau $
* @package		Joomla
* @subpackage	Polls
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class IhouseModelReview extends JModel
{
	function __construct()
	{
		global $mainframe;

		parent::__construct();

	}
	function delete() {
		$db = $this->getDBO();
		$cid			=	JRequest::getVar('cid', array());
		
		
		foreach($cid as $id) {
		
			/* DELETE #__ihouse_property */
			$query = "DELETE FROM #__ihouse_property WHERE id = '$id' ";
			
				$db->setQuery( $query );
				$db->query();
			
		}
		
		return true;
	}
	
	function getPropertyReview($where_at = 'property_detail') {
		$db 			= $this->getDBO();
		
		$property_id	=	JRequest::getVar('property_id', 0);
		
		$page			=	JRequest::getVar('page', 0);
		$limit 			= 	10;
		
		$where			=	array();
		
		if($where_at == 'property_detail') {
			$where[]	=	" r.property_id = '$property_id' ";
		} else {
			
		}
		
		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
		
		$query 			= 	"SELECT r.*,p.name_en,p.name_ch, p.postcode, u.name,u.chinese_name,u.user_image FROM #__ihouse_property_review AS r "
							.	" LEFT JOIN #__users AS u ON u.id = r.user_id " 
							.	" LEFT JOIN #__ihouse_property AS p ON p.id = r.property_id " 
							.	$where
							. 	" ORDER BY r.created DESC "
							.	" LIMIT ".$page*$limit." , 10";
			
				$db->setQuery( $query );
				$rows = $db->loadObjectList();
		
		return $rows;
	}
	
	function totalPropertyReview($where_at = 'property_detail') {
		$db 			= $this->getDBO();
		
		$property_id	=	JRequest::getVar('property_id', 0);
		
		$where			=	array();
		
		if($where_at == 'property_detail') {
			$where[]	=	" r.property_id = '$property_id' ";
		} else {
			
		}
		
		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
		
		$query = 	"SELECT COUNT(*) FROM #__ihouse_property_review AS r "
					.	" LEFT JOIN #__users AS u ON u.id = r.user_id " 
					.	" LEFT JOIN #__ihouse_property AS p ON p.id = r.property_id " 
					.	$where
					.	" ";
		
		
		
				$db->setQuery( $query );
				$result = $db->loadResult();
		
		return $result;
	}
	
	function submitPropertyReview() {
		$db 			= 	$this->getDBO();
		
		$user			= 	JFactory::getUser();
		$userid			= 	$user->get('id');

		$property_id		=	JRequest::getVar('property_id', 0);
		
		$query = "SELECT user_id FROM #__ihouse_property_review WHERE user_id = '$userid' AND property_id = '$property_id' ";
			$db->setQuery( $query );
			$uid = $db->loadResult();
		
		if(!empty($uid))
			return false;
		
		$title			=	JRequest::getVar('rev_title', '');
		$message		=	JRequest::getVar('rev_message', '');
		
		$title			=	str_replace('"','\"', $title);
		$title			=	str_replace("'","\'", $title);
		$message		=	str_replace('"','\"', $message);
		$message		=	str_replace("'","\'", $message);

			$query = "INSERT #__ihouse_property_review VALUES(NULL,'$userid','$property_id','$title','$message',CURRENT_TIMESTAMP); ";
			
				$db->setQuery( $query );
				$db->query();
		
		return true;
	}

	
}
