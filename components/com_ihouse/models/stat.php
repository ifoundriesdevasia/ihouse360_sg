<?php
/**
* @version		$Id: poll.php 10752 2008-08-23 01:53:31Z eddieajau $
* @package		Joomla
* @subpackage	Polls
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class IhouseModelStat extends JModel
{
	function __construct()
	{
		global $mainframe;

		parent::__construct();

	}
	
	function getReviewStar($rating) {
		$db = $this->getDBO();
		
		$star = 0;
		if($rating > 0 && $rating <= 20)
			$star = 1;
		else if($rating > 20 && $rating <= 40)
			$star = 2;
		else if($rating > 40 && $rating <= 60)
			$star = 3;
		else if($rating > 60 && $rating <= 80)
			$star = 4;
		else if($rating > 80 && $rating <= 100)
			$star = 5;	
		
		
		$html = '';
		for($i = 0; $i < $star; $i++) {
			$html .= '<img src="'. JURI::root().'templates/main/images/star.png'. '" />';
		}
		
		for($i = 5; $i > $star; $i--) {
			$html .= '<img src="'. JURI::root().'templates/main/images/star-blank.png'. '" />';
		}

		return $html;
	}
	
	function getRatingValuePercentage($property_id) {
		
		$db = $this->getDBO();
		
		$query = "SELECT overall_avg FROM #__ihouse_property_rating_avg WHERE property_id = '$property_id' ";
			$db->setQuery( $query );
			$result = (float) $db->loadResult();
		
		$rating =  sprintf("%01.1f",$result / 5 * 100);
		
		return $rating;
	}
	
	function getTotalSell($property_id) {
		$db = $this->getDBO();
		
		$query = "SELECT COUNT(a.id) FROM #__ihouse_ads AS a " 
				 .	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = a.subscription_id "
				 .	" WHERE a.property_id = '$property_id' AND a.ad_type = 'sell' AND a.publish = 1 AND cb.status = 'A' ";
			$db->setQuery( $query );
			$result = (int) $db->loadResult();
		
		return $result;
	}
	
	function getTotalRent($property_id) {
		$db = $this->getDBO();
		
		$query = "SELECT COUNT(a.id) FROM #__ihouse_ads AS a " 
				 .	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = a.subscription_id "
				 .	" WHERE a.property_id = '$property_id' AND a.ad_type = 'rent' AND a.publish = 1 AND cb.status = 'A' ";
			$db->setQuery( $query );
			$result = (int) $db->loadResult();
			
		return $result;	
	}
	
	function getTotalReview($property_id) {
		$db = $this->getDBO(); 
		
		$query = "SELECT COUNT(*) FROM #__ihouse_property_review WHERE property_id = '$property_id' ";
			$db->setQuery( $query );
			$result = (int) $db->loadResult();
			
		return $result;	
	}
}