﻿<?php
/**
* @version		$Id: poll.php 10752 2008-08-23 01:53:31Z eddieajau $
* @package		Joomla
* @subpackage	Polls
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class IhouseModelMapsearch extends JModel
{
	function __construct()
	{
		global $mainframe;

		parent::__construct();

	}
	function getMRTArray() {
		$db = $this->getDBO();
		
		$lat1		=	JRequest::getVar('lat1', '');
		$lat2		=	JRequest::getVar('lat2', '');
		$lng1		=	JRequest::getVar('lng1', '');
		$lng2		=	JRequest::getVar('lng2', '');
		
		$query		=	" SELECT DISTINCT (postcode), name_en, name_ch,address, lat, lng FROM #__ihouse_station "
						/*.	" WHERE lat >= '$lat1' " 
						.	" AND lat <= '$lat2' "
						.	" AND lng >= '$lng1' "
						.	" AND lng <= '$lng2' "*/;		
						
		$db->setQuery($query);
		$lists = $db->loadObjectList();
		
		$tmp = array();
		if(!empty($lists)) :
			$i = 0;
			foreach($lists as $l) :
				$tmp['name_en'][$i]		= $l->name_en;
				$tmp['name_ch'][$i] 	= $l->name_ch;
				$tmp['address'][$i] 	= $l->address;
				$tmp['postcode'][$i] 	= $l->postcode;
				$tmp['lat'][$i] 		= $l->lat;
				$tmp['lng'][$i] 		= $l->lng;
				$i++;
			endforeach;
		endif;
		
		return $tmp;
	}
	
	function getSchoolArray() {
		$db = $this->getDBO();
		
		/*$lat1		=	JRequest::getVar('lat1', '');
		$lat2		=	JRequest::getVar('lat2', '');
		$lng1		=	JRequest::getVar('lng1', '');
		$lng2		=	JRequest::getVar('lng2', '');
		*/
		
		$query		=	" SELECT DISTINCT (postcode), name_en, name_ch,address, lat, lng FROM #__ihouse_school "
						/*.	" WHERE lat >= '$lat1' " 
						.	" AND lat <= '$lat2' "
						.	" AND lng >= '$lng1' "
						.	" AND lng <= '$lng2' "*/ ;				
						
		$db->setQuery($query);
		$lists = $db->loadObjectList();
		
		$tmp = array();
		if(!empty($lists)) :
			$i = 0;
			foreach($lists as $l) :
				$tmp['name_en'][$i]		= $l->name_en;
				$tmp['name_ch'][$i] 	= $l->name_ch;
				$tmp['address'][$i] 	= $l->address;
				$tmp['postcode'][$i] 	= $l->postcode;
				$tmp['lat'][$i] 		= $l->lat;
				$tmp['lng'][$i] 		= $l->lng;
				$i++;
			endforeach;
		endif;
		
		return $tmp;
	}
	
	function getDataArray() {
		$db = $this->getDBO();
		
		$where		=	$this->buildWheres();
		
		$orderby	=	" ORDER BY RAND() ";
		
		$query		=	" SELECT DISTINCT (a.postcode), p.id AS property_id, g.lat, g.lng FROM #__ihouse_ads AS a "
						.	" LEFT JOIN #__ihouse_property AS p ON p.postcode = a.postcode "
						.	" LEFT JOIN #__ihouse_location_gps AS g ON g.postcode = a.postcode "
						.	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = a.subscription_id "
						.	$where
						.	$orderby
						.	" " ;			
					
		
		$db->setQuery($query);
		$lists = $db->loadObjectList();
		
		$tmp = array();
		if(!empty($lists)) :
			$i = 0;
			foreach($lists as $l) :
				$tmp['postcode'][$i] 	= $l->postcode;
				$tmp['lat'][$i] 		= $l->lat;
				$tmp['lng'][$i] 		= $l->lng;
				$i++;
			endforeach;
		endif;
		
		return $tmp;
	}
	
	function getDataArrayByPanning($lat1,$lat2,$lng1,$lng2) {
		$db 		= $this->getDBO();
		
		$where		=	$this->buildWheres();
		
		$orderby	=	" ORDER BY RAND() ";
		
		
		$query = 	" SELECT DISTINCT (a.postcode), p.*,g.lat, g.lng FROM #__ihouse_ads AS a " 
					.	" LEFT JOIN #__ihouse_location_gps AS g ON g.postcode = a.postcode "
					.	" LEFT JOIN #__ihouse_property AS p ON p.postcode = a.postcode "
					.	" LEFT JOIN #__cbsubs_subscriptions AS cb ON cb.id = a.subscription_id "
					.	$where
					.	" AND g.lat >= '$lat1' " 
					.	" AND g.lat <= '$lat2' "
					.	" AND g.lng >= '$lng1' "
					.	" AND g.lng <= '$lng2' "
					.	$orderby;
							
			$db->setQuery($query);
			$lists = $db->loadObjectList();
			
		$tmp = array();
		if(!empty($lists)) :
			$i = 0;
			foreach($lists as $l) :
				$tmp['property_id'][$i]	= $l->id;
				$tmp['postcode'][$i] 	= $l->postcode;
				$tmp['lat'][$i] 		= $l->lat;
				$tmp['lng'][$i] 		= $l->lng;
				$tmp['title_en'][$i] 	= $l->name_en;
				$tmp['title_ch'][$i]	= $l->name_ch;
				
				$query	=	"SELECT name_en FROM #__ihouse_property "
					. " WHERE id = '".$l->id."' ";
		
					$db->setQuery( $query );
					$projectname	=	$db->loadResult();
			
				$projectname	=	preg_replace('/(\s|\.|\')+/','-',strtolower($projectname));
		
				$query			= 	" SELECT price FROM #__ihouse_property_price "
							.	" WHERE project_name = '$projectname' ";
					$db->setQuery( $query );
					$avgprice	=	(int) $db->loadResult();
					
				$tmp['psm_price'][$i]	= number_format($avgprice);
				$i++;
			endforeach;
		endif;
		
		return $tmp;
	}
	
	function getMapPosition($mode, $id) {
		$db = $this->getDBO();
		
		$result = '';
		
		switch($mode) {
			case 'school'	:
				$query	=	" SELECT postcode, address, name_en, name_ch FROM #__ihouse_school WHERE id = '$id' LIMIT 1 ";
					$db->setQuery( $query );
					$result = $db->loadObject();
				break;
			case 'station'	:
				$query	=	" SELECT DISTINCT(postcode),address, name_en, name_ch FROM #__ihouse_station WHERE name_ch LIKE '%".$id."%' LIMIT 1";
					$db->setQuery( $query );
					$result = $db->loadObject();
				break;	
			
		}
		return $result;
	}
	
	function getDistrictPosition($value) {
		$db = $this->getDBO();
		
		$query	=	" SELECT lat, lng FROM #__ihouse_district WHERE code = '$value' ";
			$db->setQuery( $query );
			$result = $db->loadObject();
		
		return $result;
	}
	
	
	function getOptionMapSearchObj() {
		
		$prices 	= '';
		$bedroom 	= '';
		$flattype	= '';
		
		$ptype			=	JRequest::getVar('propertytype','');
		$type			=	JRequest::getVar('type','');
		
		switch($ptype) {
			case '公寓' :
				$bedroom .= '<select name="bedroom" id="bedroom" class="searchselect">';
				$bedroom .= '	<option value="">户型不限</option>';
				$bedroom .= '	<option value="1" '.(($bed == 1)? $selected :'').'>1房</option>';
				$bedroom .= '	<option value="2" '.(($bed == 2)? $selected :'').'>2房</option>';
				$bedroom .= '	<option value="3" '.(($bed == 3)? $selected :'').'>3房</option>';
				$bedroom .= '	<option value="4" '.(($bed == 4)? $selected :'').'>4房</option>';
				$bedroom .= '	<option value="5" '.(($bed == 5)? $selected :'').'>5房以上</option>';
				$bedroom .= '</select>';
				
					$prices .= '<select name="price" class="searchselect2" id="price" style="width:132px;">';
					$prices .= '	<option value="">总价下限</option>';
					$prices .= '	<option value="1" '.(($pri == 1)? $selected :'').'>总价新币 50万以下</option>';
					$prices .= '	<option value="2" '.(($pri == 2)? $selected :'').'>总价新币 60 万</option>';
					$prices .= '	<option value="3" '.(($pri == 3)? $selected :'').'>总价新币 70 万</option>';
					$prices .= '	<option value="4" '.(($pri == 4)? $selected :'').'>总价新币 85 万</option>';
					$prices .= '	<option value="5" '.(($pri == 5)? $selected :'').'>总价新币 100 万</option>';
					$prices .= '	<option value="6" '.(($pri == 6)? $selected :'').'>总价新币 120 万</option>';
					$prices .= '	<option value="7" '.(($pri == 7)? $selected :'').'>总价新币 140 万</option>';
					$prices .= '	<option value="8" '.(($pri == 8)? $selected :'').'>总价新币 170 万</option>';
					$prices	.= '	<option value="9" '.(($pri == 9)? $selected :'').'>总价新币 200 万</option>';
					$prices .= '	<option value="10" '.(($pri == 10)? $selected :'').'>总价新币 220 万</option>';
					$prices .= '	<option value="11" '.(($pri == 11)? $selected :'').'>总价新币 250 万</option>';
					$prices .= '	<option value="12" '.(($pri == 12)? $selected :'').'>总价新币 300 万</option>';
					$prices .= '	<option value="13" '.(($pri == 13)? $selected :'').'>总价新币 350 万</option>';
					$prices .= '	<option value="14" '.(($pri == 14)? $selected :'').'>总价新币 400 万</option>';
					$prices .= '	<option value="15" '.(($pri == 15)? $selected :'').'>总价新币 450 万</option>';
					$prices .= '	<option value="16" '.(($pri == 16)? $selected :'').'>总价新币 500 万以上</option>';
					$prices .= '</select>';
				
					$prices .= '<select name="price_max" class="searchselect2" id="price_max" style="width:132px;">';
					$prices .= '	<option value="">总价上限</option>';
					$prices .= '	<option value="1" '.(($pri == 1)? $selected :'').'>总价新币 50万以下</option>';
					$prices .= '	<option value="2" '.(($pri == 2)? $selected :'').'>总价新币 60 万</option>';
					$prices .= '	<option value="3" '.(($pri == 3)? $selected :'').'>总价新币 70 万</option>';
					$prices .= '	<option value="4" '.(($pri == 4)? $selected :'').'>总价新币 85 万</option>';
					$prices .= '	<option value="5" '.(($pri == 5)? $selected :'').'>总价新币 100 万</option>';
					$prices .= '	<option value="6" '.(($pri == 6)? $selected :'').'>总价新币 120 万</option>';
					$prices .= '	<option value="7" '.(($pri == 7)? $selected :'').'>总价新币 140 万</option>';
					$prices .= '	<option value="8" '.(($pri == 8)? $selected :'').'>总价新币 170 万</option>';
					$prices	.= '	<option value="9" '.(($pri == 9)? $selected :'').'>总价新币 200 万</option>';
					$prices .= '	<option value="10" '.(($pri == 10)? $selected :'').'>总价新币 220 万</option>';
					$prices .= '	<option value="11" '.(($pri == 11)? $selected :'').'>总价新币 250 万</option>';
					$prices .= '	<option value="12" '.(($pri == 12)? $selected :'').'>总价新币 300 万</option>';
					$prices .= '	<option value="13" '.(($pri == 13)? $selected :'').'>总价新币 350 万</option>';
					$prices .= '	<option value="14" '.(($pri == 14)? $selected :'').'>总价新币 400 万</option>';
					$prices .= '	<option value="15" '.(($pri == 15)? $selected :'').'>总价新币 450 万</option>';
					$prices .= '	<option value="16" '.(($pri == 16)? $selected :'').'>总价新币 500 万以上</option>';
					$prices .= '</select>';
				break;
			case '组屋':
				$bedroom .= '<select name="bedroom" class="searchselect2" id="bedroom" >';
				$bedroom .= '	<option value="">户型不限</option>';
				$bedroom .= '	<option value="1" '.(($bed1 == 1)? $selected :'').'>1房</option>';
				$bedroom .= '	<option value="2" '.(($bed1 == 2)? $selected :'').'>2房</option>';
				$bedroom .= '	<option value="3" '.(($bed1 == 3)? $selected :'').'>3房</option>';
				$bedroom .= '	<option value="4" '.(($bed1 == 4)? $selected :'').'>4房</option>';
				$bedroom .= '	<option value="5" '.(($bed1 == 5)? $selected :'').'>5房</option>';
				$bedroom .= '	<option value="6" '.(($bed1 == 6)? $selected :'').'>公寓式</option>';
				$bedroom .= '</select>';
				
				$prices .= '<select name="prices_h" class="searchselect2" id="price">';
				$prices .= '	<option value="">总价不限</option>';
				$prices .= '	<option value="1" '.(($pri1 == 1)? $selected :'').'>总价新币 30 以下</option>';
				$prices .= '	<option value="2" '.(($pri1 == 2)? $selected :'').'>总价新币 35 </option>';
				$prices .= '	<option value="3" '.(($pri1 == 3)? $selected :'').'>总价新币 40 </option>';
				$prices .= '	<option value="4" '.(($pri1 == 4)? $selected :'').'>总价新币 45 </option>';
				$prices .= '	<option value="5" '.(($pri1 == 5)? $selected :'').'>总价新币 50 </option>';
				$prices .= '	<option value="6" '.(($pri1 == 6)? $selected :'').'>总价新币 60 </option>';
				$prices .= '	<option value="7" '.(($pri1 == 7)? $selected :'').'>总价新币 70 </option>';
				$prices .= '	<option value="8" '.(($pri1 == 8)? $selected :'').'>总价新币 80 以上</option>';
				$prices .= '</select>';
				
				$flattype .= '<select name="flattype" class="searchselect2" id="flattype_hdb" >';
				$flattype .= '<option value="">户型</option> ';
        		$flattype .= '<option '.(($row == '1房式')?'selected="selected"':'').'value="1房式" >1房式</option>';
        		$flattype .= '<option '.(($row == '2房式')?'selected="selected"':'').'value="2房式" >2房式</option>';
        		$flattype .= '<option '.(($row == '3房式')?'selected="selected"':'').'value="3房式" >3房式</option>';
				$flattype .= '<option '.(($row == '4房式')?'selected="selected"':'').'value="4房式" >4房式</option>';
        		$flattype .= '<option '.(($row == '5房式')?'selected="selected"':'').'value="5房式" >5房式</option>';
				$flattype .= '<option '.(($row == '公寓式')?'selected="selected"':'').'value="公寓式" >公寓式</option>';
        		$flattype .= '</select>';
				break;
		}
		
		
		
		if($type == 'rent') {
			$prices = '';
			$prices .= '<select name="price" class="searchselect2" id="price">';
			$prices .= '	<option value="">总价不限</option>';
			$prices .= '	<option value="1" '.(($pri2 == 1)? $selected :'').'>新元 500 及以下</option>';
			$prices .= '	<option value="2" '.(($pri2 == 2)? $selected :'').'>新元 1000</option>';
			$prices .= '	<option value="3" '.(($pri2 == 3)? $selected :'').'>新元 1500</option>';
			$prices .= '	<option value="4" '.(($pri2 == 4)? $selected :'').'>新元 2000</option>';
			$prices .= '	<option value="5" '.(($pri2 == 5)? $selected :'').'>新元 2500</option>';
			$prices .= '	<option value="6" '.(($pri2 == 6)? $selected :'').'>新元 3000</option>';
			$prices .= '	<option value="7" '.(($pri2 == 7)? $selected :'').'>新元 3500</option>';
			$prices .= '	<option value="8" '.(($pri2 == 8)? $selected :'').'>新元 4000</option>';
			$prices .= '	<option value="9" '.(($pri2 == 9)? $selected :'').'>新元 5000</option>';
			$prices .= '	<option value="10" '.(($pri2 == 10)? $selected :'').'>新元 6000</option>';
			$prices .= '	<option value="11" '.(($pri2 == 11)? $selected :'').'>新元 7000</option>';
			$prices .= '	<option value="12" '.(($pri2 == 12)? $selected :'').'>新元 8000</option>';
			$prices .= '	<option value="13" '.(($pri2 == 13)? $selected :'').'>新元 9000</option>';
			$prices .= '	<option value="14" '.(($pri2 == 14)? $selected :'').'>新元 10000</option>';
			$prices .= '	<option value="15" '.(($pri2 == 15)? $selected :'').'>新元 12000</option>';
			$prices .= '	<option value="16" '.(($pri2 == 16)? $selected :'').'>新元 15000</option>';
			$prices .= '	<option value="17" '.(($pri2 == 17)? $selected :'').'>新元 20000</option>';
			$prices .= '	<option value="18" '.(($pri2 == 18)? $selected :'').'>新元 30000</option>';
			$prices .= '	<option value="19" '.(($pri2 == 19)? $selected :'').'>新元 40000</option>';
			$prices .= '	<option value="20" '.(($pri2 == 20)? $selected :'').'>新元 50000 及以上</option>';
			$prices .= '</select>';
			
			$prices .= '<select name="price_max" class="searchselect2" id="price_max">';
			$prices .= '	<option value="">总价不限</option>';
			$prices .= '	<option value="1" '.(($pri2 == 1)? $selected :'').'>新元 500 及以下</option>';
			$prices .= '	<option value="2" '.(($pri2 == 2)? $selected :'').'>新元 1000</option>';
			$prices .= '	<option value="3" '.(($pri2 == 3)? $selected :'').'>新元 1500</option>';
			$prices .= '	<option value="4" '.(($pri2 == 4)? $selected :'').'>新元 2000</option>';
			$prices .= '	<option value="5" '.(($pri2 == 5)? $selected :'').'>新元 2500</option>';
			$prices .= '	<option value="6" '.(($pri2 == 6)? $selected :'').'>新元 3000</option>';
			$prices .= '	<option value="7" '.(($pri2 == 7)? $selected :'').'>新元 3500</option>';
			$prices .= '	<option value="8" '.(($pri2 == 8)? $selected :'').'>新元 4000</option>';
			$prices .= '	<option value="9" '.(($pri2 == 9)? $selected :'').'>新元 5000</option>';
			$prices .= '	<option value="10" '.(($pri2 == 10)? $selected :'').'>新元 6000</option>';
			$prices .= '	<option value="11" '.(($pri2 == 11)? $selected :'').'>新元 7000</option>';
			$prices .= '	<option value="12" '.(($pri2 == 12)? $selected :'').'>新元 8000</option>';
			$prices .= '	<option value="13" '.(($pri2 == 13)? $selected :'').'>新元 9000</option>';
			$prices .= '	<option value="14" '.(($pri2 == 14)? $selected :'').'>新元 10000</option>';
			$prices .= '	<option value="15" '.(($pri2 == 15)? $selected :'').'>新元 12000</option>';
			$prices .= '	<option value="16" '.(($pri2 == 16)? $selected :'').'>新元 15000</option>';
			$prices .= '	<option value="17" '.(($pri2 == 17)? $selected :'').'>新元 20000</option>';
			$prices .= '	<option value="18" '.(($pri2 == 18)? $selected :'').'>新元 30000</option>';
			$prices .= '	<option value="19" '.(($pri2 == 19)? $selected :'').'>新元 40000</option>';
			$prices .= '	<option value="20" '.(($pri2 == 20)? $selected :'').'>新元 50000 及以上</option>';
			$prices .= '</select>';
					
		}
		
		$formats 			= 	new StdClass;
		$formats->price		= 	$prices;
		$formats->bedroom	=	$bedroom;
		
		return $formats;
		
	}
	
	/* **********************
		BUILD WHERES
	************************* */
	function buildWheres() {
		$db 					= $this->getDBO();
		$where					=	array();
		$keyword				=	JRequest::getVar('keyword', ''); // either chinese or english
		$bedroom				=	JRequest::getVar('bedroom', '');
		$price					=	JRequest::getVar('price', '');	
		$price_max				=	JRequest::getVar('price_max', '');
		$property_type			=	JRequest::getVar('proptype','');
		$margin_amt_percent		=	JRequest::getVar('amount', 0); 
		$budget					=	str_replace(',','',JRequest::getVar('budget', 0));
		$ap						=	$margin_amt_percent;
		$type					=	JRequest::getVar('type', 'all');
		
		switch($type) {
			case 'new'		:
				$where[]	=	" a.ad_type = 'sell' AND a.property_category = '新房' ";
				break;
			case 'second'	:
				$where[]	=	" a.ad_type = 'sell' AND a.property_category = '其它' "; /* other */
				break;
			case 'rent'		:
				$where[]	=	" a.ad_type = 'rent' ";
				break;
			default			:
				$where[] 	=	" 1 ";
				break;
		}
		
		if($property_type) {
			$where[] = " a.property_type = '$property_type' ";
			if($property_type == '公寓') {
				switch($bedroom) {
					case 1: /* 1房 */
						$where[] = " a.no_of_room = 1 ";
					break;
					case 2: /* 2房 */
						$where[] = " a.no_of_room = 2 ";
					break;
					case 3: /* 3房 */
						$where[] = " a.no_of_room = 3 ";
						break;
					case 4: /* 4房 */
						$where[] = " a.no_of_room = 4 ";
						break;
					case 5: /* 5房以上 */
						$where[] = " a.no_of_room >= 5 ";
					break;
				}
			} else if($property_type == '组屋') {
				switch($bedroom) {
					case 1: /* 1房 */
						$where[] = " a.no_of_room = 1 ";
						break;
					case 2: /* 2房 */
						$where[] = " a.no_of_room = 2 ";
						break;
					case 3: /* 3房 */
						$where[] = " a.no_of_room = 3 ";
						break;
					case 4: /* 4房 */
						$where[] = " a.no_of_room = 4 ";
						break;
					case 5: /* 5房 */
						$where[] = " a.no_of_room = 5 ";
						break;
					case 6: /* 公寓式 */
						$where[] = " a.no_of_room > 5";
				}
			}
		}
		
		if($budget) {
			if($margin_amt_percent > 0) {
				$min = $budget - ($budget * $margin_amt_percent / 100);
				$max = $budget + ($budget * $margin_amt_percent / 100);
				$where[] = " a.ask_price >= '$min' ";
				$where[] = " a.ask_price <= '$max' ";
			}
			else {
				$where[] = " a.ask_price = '$budget' ";
			}
		} else {
			/* SET PRICES */
			if($type	== 'rent') {
				$min = $this->setPrice($price, 'rent');
				$max = $this->setPrice($price_max, 'rent');
				//echo $price.' '.$price_max;
			}
			else
			{
				/* Condo */
				if($property_type == '公寓') {
					$min = $this->setPrice($price, 'condo');
					$max = $this->setPrice($price_max, 'condo');
					//echo $price.' '.$price_max;
				}
			} 
			
		
			if($ap && ($min > 0 || $max > 0)) {
				$min_x = $min - ($ap / 100 * $min);
				$max_x = $max + ($ap / 100 * $max);
				
				$where[] = " a.ask_price >= ". $min_x;
				$where[] = " a.ask_price <= ". $max_x;
			}
		}
		/* REMOVE COMMENTS WHEN IN PRODUCTION */
		$where[]	=	" cb.status = 'A' "; 	/* FIND ACTIVE SUBSCRIPTION */
		$where[]	=	" a.publish = 1 "; 		/* PUBLISH ADS */
		
		
		if ($keyword && $keyword != '请输入华文或英文地名、邮编等等。')
		{
			$db = $this->getDBO();
			
			$keyword = strtolower($keyword);
			
			$query = ' SELECT postcode FROM #__ihouse_property WHERE LOWER(name_ch) LIKE '.$db->Quote( '%'.$db->getEscaped( $keyword, true ).'%', false ). ' OR LOWER(name_en) LIKE '.$db->Quote( '%'.$db->getEscaped( $keyword, true ).'%', false );
				$db->setQuery($query);
				$rows = $db->loadObjectList(); 
			
			$whereor = array();
			$tmpwhere = ' ';
			if(!empty($rows)) {
				
				foreach($rows as $r) :
					$whereor[] = ' LOWER(a.postcode) LIKE '.$db->Quote( '%'.$db->getEscaped( $r->postcode, true ).'%', false ).' ';
				endforeach;
				
			}
			else {
				$whereor[] = ' LOWER(a.postcode) LIKE '.$db->Quote( '%'.$db->getEscaped( $keyword, true ).'%', false ).' ';
			}
			
			$tmpwhere 	= ' ' . ( count( $whereor ) ? ' OR '. implode( ' OR ', $whereor ) : '' ) . ' ' ;
			//echo $tmpwhere;
			$where[] = ' ( LOWER(a.hdb_town_id) LIKE '.$db->Quote( '%'.$db->getEscaped( $keyword, true ).'%', false )
						.	' OR LOWER(a.property_name) LIKE '.$db->Quote( '%'.$db->getEscaped( $keyword, true ).'%', false )
						.	' OR LOWER(a.street_name) LIKE '.$db->Quote( '%'.$db->getEscaped( $keyword, true ).'%', false )
						.	' OR LOWER(a.add_info) LIKE '.$db->Quote( '%'.$db->getEscaped( $keyword, true ).'%', false )
						.	$tmpwhere
						.	' ) ';
		} 
		
		$where 		= 	( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
		//echo $where;
		return $where;
	}
	
	function setPrice($price, $mode = 'rent') 
	{	
		if($mode == 'rent') {
			if($price == 1)  {
				$value		= 500;
			} 
			else if($price == 2) {
				$value		= 1000;
			}
			else if($price == 3) {
				$value		= 1500;
			}
			else if($price == 4) {
				$value		= 2000;
			}
			else if($price == 5) {
				$value		= 2500;
			}
			else if($price == 6) {
				$value		= 3000;
			}
			else if($price == 7) {
				$value		= 3500;
			}
			else if($price == 8) {
				$value		= 4000;
			}
			else if($price == 9) {
				$value		= 5000;
			}
			else if($price == 10) {
				$value		= 6000;
			}
			else if($price == 11) {
				$value		= 7000;
			}
			else if($price == 12) {
				$value		= 8000;
			}
			else if($price == 13) {
				$value		= 9000;
			}
			else if($price == 14) {
				$value		= 10000;
			}
			else if($price == 15) {
				$value		= 12000;
			}
			else if($price == 16) {
				$value		= 15000;
			}
			else if($price == 17) {
				$value		= 20000;
			}
			else if($price == 18) {
				$value		= 30000;
			}
			else if($price == 19) {
				$value		= 40000;
			}
			else if($price == 20) {
				$value		= 50000;
			}
		}
		
		if($mode == 'condo') {
			if($price == 1)  {
				$value		= 500000;
			} 
			else if($price == 2) {
				$value		= 600000;
			}
			else if($price == 3) {
				$value		= 700000;
			}
			else if($price == 4) {
				$value		= 850000;
			}
			else if($price == 5) {
				$value		= 1000000;
			}
			else if($price == 6) {
				$value		= 1200000;
			}
			else if($price == 7) {
				$value		= 1400000;
			}
			else if($price == 8) {
				$value		= 1700000;
			}
			else if($price == 9) {
				$value		= 2000000;
			}
			else if($price == 10) {
				$value		= 2200000;
			}
			else if($price == 11) {
				$value		= 2500000;
			}
			else if($price == 12) {
				$value		= 3000000;
			}
			else if($price == 13) {
				$value		= 3500000;
			}
			else if($price == 14) {
				$value		= 4000000;
			}
			else if($price == 15) {
				$value		= 4500000;
			}
			else if($price == 16) {
				$value		= 5000000;
			}
		}
		
		if($mode == 'hdb') {
			if($price == 1)  {
				$value		= 300000;
			} 
			else if($price == 2) {
				$value		= 350000;
			}
			else if($price == 3) {
				$value		= 400000;
			}
			else if($price == 4) {
				$value		= 450000;
			}
			else if($price == 5) {
				$value		= 500000;
			}
			else if($price == 6) {
				$value		= 600000;
			}
			else if($price == 7) {
				$value		= 700000;
			}
			else if($price == 8) {
				$value		= 800000;
			}
		}
		
		return $value;
	}
	
	function formatMoney($number, $cents = 1) { // cents: 0=never, 1=if needed, 2=always
  		if (is_numeric($number)) { // a number
    		if (!$number) { // zero
      			$money = ($cents == 2 ? '0.00' : '0'); // output zero
    		} else { // value
      			if (floor($number) == $number) { // whole number
        			$money = number_format($number, ($cents == 2 ? 2 : 0)); // format
      			} else { // cents
        			$money = number_format(round($number, 2), ($cents == 0 ? 0 : 2)); // format
      			} // integer or decimal
    		} // value
    		return $money;
  		} // numeric
	} // formatMoney
}
