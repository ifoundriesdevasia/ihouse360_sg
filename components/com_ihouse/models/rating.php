<?php
/**
* @version		$Id: poll.php 10752 2008-08-23 01:53:31Z eddieajau $
* @package		Joomla
* @subpackage	Polls
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class IhouseModelRating extends JModel
{
	function __construct()
	{
		global $mainframe;

		parent::__construct();

	}
	
	function submitPropertyUserRating() {
		$db 				= 	$this->getDBO();
		
		$user				= 	JFactory::getUser();
		$userid				= 	$user->get('id');

		$property_id		=	JRequest::getVar('property_id', 0);
		
		$query = "SELECT user_id FROM #__ihouse_property_rating WHERE user_id = '$userid' AND property_id = '$property_id' ";
			$db->setQuery( $query );
			$uid = $db->loadResult();
		
		if(!empty($uid))
			return false;
		
		$transportation		=	JRequest::getVar('trans', 0);
		$environment		=	JRequest::getVar('environment', 0);
		$design				=	JRequest::getVar('design', 0);
		$facility			=	JRequest::getVar('facility', 0);
		$developer			=	JRequest::getVar('developer', 0);

		/* INSERTING INTO DB INDIVIDUAL */
		
		$query = "INSERT INTO #__ihouse_property_rating VALUES(NULL,'$userid','$property_id','$transportation','$environment','$design','$facility','$developer',CURRENT_TIMESTAMP); ";
			
			$db->setQuery( $query );
			$db->query();
		
		$query = "SELECT * FROM #__ihouse_property_rating_avg WHERE property_id = '$property_id' ";
			$db->setQuery( $query );
			$result = $db->loadObject();
		
		if(!empty($result)) {
			
			$total 				= 	$result->total + 1;
			$transport_avg 		= 	$result->transport_avg + (float) $transportation;
			$environment_avg 	= 	$result->environment_avg + (float) $environment;
			$design_avg			=	$result->design_avg + (float) $design;
			$facility_avg		=	$result->facility_avg + (float) $facility;;
			$developer_avg		=	$result->developer_avg + (float) $developer;
			$overall_avg		=	(($transport_avg + $environment_avg + $design_avg + $facility_avg + $developer_avg) / $total) / 5;
			
			$query = "UPDATE #__ihouse_property_rating_avg 
						SET total = '$total', 
						transport_avg = '$transport_avg',
						environment_avg = '$environment_avg',
						design_avg = '$design_avg',
						facility_avg = '$facility_avg',
						developer_avg = '$developer_avg',
						overall_avg = '$overall_avg'
						WHERE property_id = '$property_id' ; ";
			
				$db->setQuery( $query );
				$db->query();
			
		} else {
			
			$overall_avg	=	($transportation + $environment + $design + $facility + $developer) / 5;	
			
			$query = "INSERT INTO #__ihouse_property_rating_avg VALUES(NULL,'$property_id','$transportation','$environment','$design','$facility','$developer','$overall_avg',1,CURRENT_TIMESTAMP); ";
			
			$db->setQuery( $query );
			$db->query();
		}
		
		return true;
	}
	
	function getPropertyUserRating() {
		$db 				= 	$this->getDBO();
		
		$property_id	=	JRequest::getVar('id');
		
		$query = "SELECT * FROM #__ihouse_property_rating_avg WHERE property_id = '$property_id' ";
			$db->setQuery( $query );
			$result = $db->loadObject();
		
		return $result;
	}

	function getStarUserRating($rating) {
		//global $mainframe;
		
		$star = 0;
		if($rating > 0 && $rating <= 20)
			$star = 1;
		else if($rating > 20 && $rating <= 40)
			$star = 2;
		else if($rating > 40 && $rating <= 60)
			$star = 3;
		else if($rating > 60 && $rating <= 80)
			$star = 4;
		else if($rating > 80 && $rating <= 100)
			$star = 5;	
		
		
		$html = '';
		for($i = 0; $i < $star; $i++) {
			$html .= '<img src="'. JURI::root().'templates/main/images/star.png'. '" />';
		}
		
		for($i = 5; $i > $star; $i--) {
			$html .= '<img src="'. JURI::root().'templates/main/images/star-blank.png'. '" />';
		}

		return $html;
	}
	
}
