<?php
/**
* @version		$Id: poll.php 10752 2008-08-23 01:53:31Z eddieajau $
* @package		Joomla
* @subpackage	Polls
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class IhouseModelIhouse extends JModel
{
	function __construct()
	{
		global $mainframe;

		parent::__construct();

	}
	function delete() {
		$db = $this->getDBO();
		$cid			=	JRequest::getVar('cid', array());
		
		
		foreach($cid as $id) {
		
			/* DELETE #__ihouse_property */
			$query = "DELETE FROM #__ihouse_property WHERE id = '$id' ";
			
				$db->setQuery( $query );
				$db->query();
			
		}
		
		return true;
	}
	
	
	function update($id = 0) {
		
		if(!$id)
			return false;
		
		$db = $this->getDBO();

		$user		=& 	JFactory::getUser();
		$uid		= 	$user->id;
		
		$property_id		=	JRequest::getVar('property_id');
		$name_en			=	JRequest::getVar('name_en', '');
		$name_ch			=	JRequest::getVar('name_ch', '');
		$address		=	JRequest::getVar('address', '');
		$postalcode	=	JRequest::getVar('postcode', '');
		$property_type = JRequest::getVar('property_type', '');
		$tenure	=	JRequest::getVar('tenure', '');
		$property_class	=	JRequest::getVar('property_class', '');
		$year_built	=	JRequest::getVar('year_built', '');
		$room_size	=	JRequest::getVar('room_size', '');
		$district	=	JRequest::getVar('district_id', '');
		$condo_units	=	JRequest::getVar('units_on_condo', '');
		$description	=	JRequest::getVar('description', '');
		$nearby_mrt	=	JRequest::getVar('nearby_mrt', '');
		$nearby_school	=	JRequest::getVar('nearby_school', '');
		$developer	=	JRequest::getVar('developer', '');
		$floorplan	=	JRequest::getVar('floor_plan', '');
		
		$query = 	"UPDATE #__ihouse_property "
					. " SET name_en = '$name_en', name_ch = '$name_ch', address = '$address', postcode = '$postalcode', property_type = '$property_type' "
					. " , tenure = '$tenure', property_class = '$property_class', year_built = '$year_built',room_size = '$room_size' "
					. " , district_id = '$district', units_on_condo = '$condo_units', description = '$description' "
					. " , nearby_mrt = '$nearby_mrt', nearby_school = '$nearby_school', developer = '$developer' "
					. " , floor_plan = '$floorplan' "
					. " WHERE id = '$id' ; ";

			$db->setQuery( $query );
			$db->query();
		
		return true;
	}

	
}
