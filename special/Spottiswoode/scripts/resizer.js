var oriWidth=1024;
var oriHeight=670;
var scale=1;
function resizer()
{
	if($(window).width()>$(window).height())
	{
		$("#contents").css({"width":1024,
							"height":670});
		if($(window).height()>$(window).width()*oriHeight/oriWidth)
		{
			
			scale=$(window).width()/oriWidth;
		}
		else
		{
			scale=$(window).height()/oriHeight;
		}
		$("#fp0_desp").css({
			"left":115,
			"top":25
		});
		$("#contents").css({"position":"absolute",
							"-webkit-transform-origin":"0% 0%",
							 "-webkit-transform": "scale("+scale+","+scale+")"
							 
						});
		$("#contents").css({"left":$(window).width()/2-oriWidth*scale/2,
							"top":$(window).height()/2-oriHeight*scale/2
							 
						});
		$("#fp0_desp").css({
			"left":115,
			"top":25
		});
		$("#gap").css({"height":335});
		$("#fp0_mc").css({"left":375,
						"top":90});
		$("#logo").css({"position":"absolute",
						"left":$(window).width()-305,
						"top":$(window).height()-105});
	}
	else
	{
		if($(window).height()>$(window).width()*900/768)
		{
			
			scale=$(window).width()/768;
		}
		else
		{
		
			scale=$(window).height()/900;
		}
		
		$("#contents").css({"width":768,
							"height":900});
		$("#contents").css({"position":"absolute",
							"-webkit-transform-origin":"50% 50%",
							 "-webkit-transform": "scale("+scale+","+scale+")"});
		$("#contents").css({"left":$(window).width()/2-768/2,
							"top":$(window).height()/2-900/2});
		$("#fp0_desp").css({
			"left":105,
			"top":30
		});
		$("#gap").css({"height":540});
		$("#fp0_mc").css({"left":107,
						"top":210});
		$("#logo").css({"position":"absolute",
						"left":$(window).width()-290,
						"top":$(window).height()-125});
	}
	//$("#tester").html($(window).width()+"|"+$(window).height());
	$("#left").css({"position":"absolute",
						"width":97*scale,
						"height":$(window).height()});
	
	$("#popContent").css({
							"width":$(window).width(),
							"height":$(window).height(),
							"z-index":5
						});
}