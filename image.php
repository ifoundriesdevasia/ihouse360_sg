<?php

$filename = $_REQUEST['path'];
$img_width_size = intval($_REQUEST['size']);
$type = intval($_REQUEST['type']);


switch($type) {

	case '1':
		$filename =  'components/com_user/images/'.$filename;
	break;
	
	case '2':
		$filename = 'components/com_user/images/'.$filename;
	break;
	
	case '3':
		$filename = 'images/singapore/'.$filename;
		
	break;
	
	
}//end of switch

if($filename && $type) {
	// Content type
	header('Content-type: image/jpeg');
	// Get new dimensions
	list($width_orig, $height_orig) = getimagesize($filename);

	if($type == 1 || $type == 2) {
		if($width_orig <= 100 && $height_orig <= 100) {
			$image_p = imagecreatetruecolor($width_orig, $height_orig);
			$white = imagecolorallocate($image_p, $colorR, $colorG, $colorB);
			imagefill($image_p, 0, 0, $white);
			
			if(strtolower(end(explode(".", $filename)) ) == 'jpg') {
				// Resample
				//$image_p = imagecreatetruecolor($new_width, $new_height);
				$image = imagecreatefromjpeg($filename);
				imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width_orig, $height_orig, $width_orig, $height_orig);
	
			}else if(strtolower(end(explode(".", $filename)) ) == 'png') {
	
				//$image_p = imagecreatetruecolor($new_width, $new_height);
				$image = imagecreatefrompng($filename);
				imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width_orig, $height_orig, $width_orig, $height_orig);
	
			}else if(strtolower(end(explode(".", $filename)) ) == 'gif') {
	
				//$image_p = imagecreatetruecolor($new_width, $new_height);
				$image = imagecreatefromgif($filename);
				imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width_orig, $height_orig, $width_orig, $height_orig);
	
			}
			// Output
			imagejpeg($image_p, null, 100);
			exit;
		} 
	} 
	
	if($width_orig > $height_orig)
		$ratio= $width_orig/$img_width_size;
	else 
		$ratio=$height_orig/$img_width_size;
	
	$new_width	=	$width_orig/$ratio;
	$new_height	=	$height_orig/$ratio;

	if($new_height < $img_width_size) {
		$blank_height = $img_width_size;
		$image_p = imagecreatetruecolor($new_width, $blank_height);
	} else {
		$blank_height = $new_height;
		$image_p = imagecreatetruecolor($new_width, $blank_height);
	}
	
	if($new_width < $img_width_size) {
		$blank_width = $img_width_size;
		$image_p = imagecreatetruecolor($blank_width, $new_height);
	} else {
		$blank_width = $new_width;
		$image_p = imagecreatetruecolor($blank_width, $new_height);
	}

	/* Andreas */
	$colorR = 255; 
	$colorG = 255; 
	$colorB = 255;

	$white = imagecolorallocate($image_p, $colorR, $colorG, $colorB);
	imagefill($image_p, 0, 0, $white);


	if(strtolower(end(explode(".", $filename)) ) == 'jpg') {
	// Resample
		//$image_p = imagecreatetruecolor($new_width, $new_height);
		$image = imagecreatefromjpeg($filename);
		imagecopyresampled($image_p, $image, (($img_width_size-$new_width)/2), 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
	
	}else if(strtolower(end(explode(".", $filename)) ) == 'png') {
	
		//$image_p = imagecreatetruecolor($new_width, $new_height);
		$image = imagecreatefrompng($filename);
		imagecopyresampled($image_p, $image, (($img_width_size-$new_width)/2), 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
	
	}else if(strtolower(end(explode(".", $filename)) ) == 'gif') {
	
		//$image_p = imagecreatetruecolor($new_width, $new_height);
		$image = imagecreatefromgif($filename);
		imagecopyresampled($image_p, $image, (($img_width_size-$new_width)/2), 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
	
	}
	

	// Output
	imagejpeg($image_p, null, 100);

}//end of if

?>
