<?php
/**
 * Main File
 *
 * @package     Modules Anywhere
 * @version     1.10.0
 *
 * @author      Peter van Westen <peter@nonumber.nl>
 * @link        http://www.nonumber.nl
 * @copyright   Copyright © 2011 NoNumber! All Rights Reserved
 * @license     http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// Ensure this file is being included by a parent file
defined( '_JEXEC' ) or die( 'Restricted access' );

// Import library dependencies
jimport( 'joomla.event.plugin' );

/**
* Plugin that loads modules
*/
class plgSystemModulesAnywhere extends JPlugin
{
	/**
	* Constructor
	*
	* For php4 compatibility we must not use the __constructor as a constructor for
	* plugins because func_get_args ( void ) returns a copy of all passed arguments
	* NOT references. This causes problems with cross-referencing necessary for the
	* observer design pattern.
	*/
	function plgSystemModulesAnywhere( &$subject, $config )
	{
		parent::__construct( $subject, $config );
	}

	function onAfterRoute()
	{
		$this->_pass = 0;

		// return if disabled via url
		if ( JRequest::getCmd( 'disable_modulessanywhere' ) ) { return; }

		// return if current page is a joomfishplus page
		if ( JRequest::getCmd( 'option' ) == 'com_joomfishplus' ) { return; }

		$mainframe =& JFactory::getApplication();

		// load the admin language file
		$lang =& JFactory::getLanguage();
		$lang->load( 'plg_'.$this->_type.'_'.$this->_name, JPATH_ADMINISTRATOR );
		// load English language file as fallback (for undefined stuff in other language file)
		$lang->_load( JPATH_ADMINISTRATOR.DS.'language'.DS.'en-GB'.DS.'en-GB.plg_'.$this->_type.'_'.$this->_name.'.ini', 'plg_'.$this->_type.'_'.$this->_name, 0 );

		// return if NoNumber! Elements plugin is not installed
		jimport( 'joomla.filesystem.file' );
		if ( !JFile::exists( JPATH_PLUGINS.DS.'system'.DS.'nonumberelements.php' ) ) {
			if ( $mainframe->isAdmin() && JRequest::getCmd( 'option' ) !== 'com_login' ) {
				$mainframe->enqueueMessage( '', 'error' );
				$msg = JText::_( 'MA_NONUMBER_ELEMENTS_PLUGIN_NOT_INSTALLED' );
				foreach ( $mainframe->_messageQueue as $m ) {
					if ( $m['message'] == $msg ) {
						$msg = '';
						break;
					}
				}
				$mainframe->enqueueMessage( $msg, 'error' );
			}
			return;
		}

		// return if current page is an administrator page (and not acymailing)
		if ( $mainframe->isAdmin() && JRequest::getCmd( 'option' ) != 'com_acymailing' ) { return; }

		$this->_pass = 1;

		// Load plugin parameters
		require_once JPATH_PLUGINS.DS.'system'.DS.'nonumberelements'.DS.'helpers'.DS.'parameters.php';
		$parameters =& NNParameters::getParameters();
		$params = $parameters->getParams( $this->params->_raw, JPATH_PLUGINS.DS.$this->_type.DS.$this->_name.'.xml' );

		// Include the Helper
		require_once JPATH_PLUGINS.DS.$this->_type.DS.$this->_name.DS.'helper.php';
		$class = get_class( $this ).'Helper';
		$this->helper = new $class ( $params );
	}

	function onPrepareContent( &$article )
	{
		if ( $this->_pass ) {
			$this->helper->replaceInArticles( $article );
		}
	}

	function onAfterDispatch()
	{
		if ( $this->_pass ) {
			$this->helper->replaceInComponents();
		}
	}

	function onAfterRender()
	{
		if ( $this->_pass ) {
			$this->helper->replaceInOtherAreas();
		}
	}
}