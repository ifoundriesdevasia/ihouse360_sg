<?php
/**
 * Main Plugin File
 * Does all the magic!
 *
 * @package     Modules Anywhere
 * @version     1.10.0
 *
 * @author      Peter van Westen <peter@nonumber.nl>
 * @link        http://www.nonumber.nl
 * @copyright   Copyright © 2011 NoNumber! All Rights Reserved
 * @license     http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// Import library dependencies
jimport( 'joomla.event.plugin' );

/**
* Button Plugin that places Editor Buttons
*/
class plgButtonModulesAnywhere extends JPlugin
{
	/**
	* Constructor
	*
	* For php4 compatibility we must not use the __constructor as a constructor for
	* plugins because func_get_args ( void ) returns a copy of all passed arguments
	* NOT references. This causes problems with cross-referencing necessary for the
	* observer design pattern.
	*/
	function plgButtonModulesAnywhere( &$subject, $config )
	{
		parent::__construct( $subject, $config );
	}

	/**
	* Display the button
	*
	* @return array A two element array of ( imageName, textToInsert )
	*/
	function onDisplay( $name )
	{
		jimport( 'joomla.filesystem.file' );

		// return if system plugin is not installed
		if ( !is_file( JPATH_PLUGINS.DS.'system'.DS.'modulesanywhere.php' ) ) {
			return;
		}

		// return if NoNumber! Elements plugin is not installed
		if ( !JFile::exists( JPATH_PLUGINS.DS.'system'.DS.'nonumberelements.php' ) ) {
			return;
		}

		// load the admin language file
		$lang =& JFactory::getLanguage();
		$lang->load( 'plg_'.$this->_type.'_'.$this->_name, JPATH_ADMINISTRATOR );
		// load English language file as fallback (for undefined stuff in other language file)
		$lang->_load( JPATH_ADMINISTRATOR.DS.'language'.DS.'en-GB'.DS.'en-GB.plg_'.$this->_type.'_'.$this->_name.'.ini', 'plg_'.$this->_type.'_'.$this->_name, 0 );

		// Load plugin parameters
		require_once JPATH_PLUGINS.DS.'system'.DS.'nonumberelements'.DS.'helpers'.DS.'parameters.php';
		$parameters =& NNParameters::getParameters();
		$params = $parameters->getParams( $this->params->_raw, JPATH_PLUGINS.DS.$this->_type.DS.$this->_name.'.xml' );

		// Include the Helper
		require_once JPATH_PLUGINS.DS.$this->_type.DS.$this->_name.DS.'helper.php';
		$class = get_class( $this ).'Helper';
		$this->helper = new $class( $params );

		return $this->helper->render( $name );
	}
}