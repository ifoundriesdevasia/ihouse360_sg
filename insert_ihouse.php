<?php

$ftp_server 		= 	"ihouse360.com";
$username 			= 	"ihouse360-temftp";
$password			=	"iHOUSE360";

$data_path			=	"ihouse360_sg";
//$project_path		=	htmlspecialchars("public_html/house360/From PrimeConnect/Project Data");

$transaction_path	=	htmlspecialchars($data_path."/transaction");
//$rental_path		=	$data_path."/Rental";
//$for_homepage_path	=	$data_path."/For Home Page";



// set up a connection or die
$conn_id = ftp_connect($ftp_server) or die("Couldn't connect to $ftp_server"); 

// Open a session to an external ftp site
$login_result = ftp_login ($conn_id, $username, $password); 

////////////////////////////////
//
// Check Open FTP Port
//
////////////////////////////////
if ((!$login_result)) {

	echo "Wrong Username or Password! Please check with administrator"; die;

} else {
	
	echo "Welcome, user. You are connected. <br />";

}

$result = '';


/* /////////////////////////////////////////
 START - FTP : TRANSACTION HISTORICAL DATA
*/ /////////////////////////////////////////

if (ftp_chdir($conn_id, $transaction_path)) {

	echo "Current directory is now: " . ftp_pwd($conn_id) . "<br />";
	
	// get contents of the current directory
	$files = ftp_nlist($conn_id, ".");
	
	foreach($files as $f) :
		
		if($f == '.' || $f == '..')
			continue;
		
		$serverfile = $f;	
		$localfile	= "tmp/".$f;
		
		$trans 		= explode('_',$f);
		$trans2		= explode('.', $trans[1]); // remove .csv extension
		$month_year = $trans2[0];
		$ext		= $trans2[1];
		
		// try to download $server_file and save to $local_file
		if (ftp_get($conn_id, $localfile, $serverfile, FTP_BINARY)) {
			
    		echo "Successfully written to $localfile. Now Process the data... <br />";
		
		} else {
    	
			echo "Oops!! There was a problem <br />";
		
		}
		
		$tmparr = array();
		
		if(file_exists($localfile)) {
			$handle = fopen($localfile, 'rb');
  			$buffer = '';
			$title 	= fgets($handle); 
  			while ( $cline = fgets($handle) )
            {
				// psm, psf, postalcode 
				$cline = rtrim($cline, '\n');
				
				list($project_name, $psm, $psf, $postalcode) = explode(',', $cline);
               	
				$postalcode = rtrim($postalcode, ' ');
				$postalcode = sprintf('%06d', $postalcode);

				$tmparr[$project_name]['postcode'] = $postalcode;
				
				if(is_numeric($psm)) 
					$tmparr[$project_name]['psm'][] = $psm;
				
				if(is_numeric($psf))
					$tmparr[$project_name]['psf'][] = $psf;
				
				
            } 
  			fclose($handle);
			
			foreach($tmparr as $project_name => $tmp) :
				$psm_total = count($tmp['psm']);
				$psf_total = count($tmp['psf']);
				
				$psm_price_total = 0;
				$psf_price_total = 0;
				
				foreach($tmp['psm'] as $psm_price) :
					$psm_price_total += (int) $psm_price;
				endforeach;
					
				foreach($tmp['psf'] as $psf_price) :
					$psf_price_total += (int) $psf_price;
				endforeach;
				
				$avg_psm = ceil($psm_price_total / $psm_total);
				$avg_psf = ceil($psf_price_total / $psf_total);
				
				$postcode	=	$tmp['postcode'];
				
				$result .= $project_name.','.$month_year.','.$avg_psm.','.$avg_psf .','.$postcode.','.$psm_total."|";
				
			endforeach;
			
		} else {
			echo "File does not exist<br />";
		}
		
	endforeach;

} else { 
	echo "Couldn't change directory\n";
}

/* /////////////////////////////////////////
 END - FTP : TRANSACTION HISTORICAL DATA
*/ /////////////////////////////////////////

/* /////////////////////////////////////////
 START - FTP : PROJECT DATA
*/ /////////////////////////////////////////
//projectDataRecursive($conn_id, $project_path);
	
/* /////////////////////////////////////////
 END - FTP : PROJECT DATA
*/ /////////////////////////////////////////	
	
// close this connection
ftp_close($conn_id);

//echo $result;

//$username = "root";
//$password = "";
//$db_name = "ihouse";
$username = "ihouse360webplat";
$password = "primeIHOUSE360";
$hostname = "localhost";	
$db_name = 'ihouse360';

$dbh = mysql_connect($hostname, $username, $password) or die("Unable to connect to MySQL");

print "Welcome, user. Connected to MySQL Server <br>";

$db_selected = mysql_select_db($db_name, $dbh);


/////////////////////////////////////////////////////////
/* START -- TRANSACTION HISTORICAL CHART               */
/////////////////////////////////////////////////////////


$sql = 'truncate ihse_ihouse_history;';
$qry = mysql_query($sql,$dbh);
$sql = 'truncate ihse_ihouse_property_slug;';
$qry = mysql_query($sql,$dbh);

$tmp_arr = array();
$arr2 = array();
$arr_projname = array();

$tmp_arr = explode('|', $result);
foreach($tmp_arr as $tmp) :
	$tmp			= 	trim($tmp);
	
	if(empty($tmp))
		continue;
		
	$t				=	explode(',',$tmp);
	
	$monthyear		=	$t[1];
	$avg_psm		=	$t[2];
	$avg_psf		=	$t[3];
	$postcode		=	$t[4];
	$project_name	=	$t[0];
	$totals			=	$t[5];
	
	if(!in_array($monthyear, $arr2))
		$arr2[strtotime($monthyear)]	=	$monthyear;
	
	$project_name_slug = preg_replace('/(\s|\.|\')+/','-',strtolower($project_name));
	
	$sql = "INSERT INTO ihse_ihouse_history VALUES(NULL,'$project_name_slug','$postcode','$monthyear','$avg_psm','$avg_psf','$totals') ";
	mysql_query($sql, $dbh);
	
	if(!in_array($project_name, $arr_projname)) {
		$arr_projname[] = $project_name;
	}
	
endforeach;

krsort($arr2);

$sql	=	"SELECT name_en FROM ihse_ihouse_property ";
$result = mysql_query($sql, $dbh);
	
if (!$result) {
   	echo "Could not successfully run query ($sql) from DB: " . mysql_error();
   	exit;
}
	
while ($row = mysql_fetch_assoc($result)) {   	
	$project_name = $row["name_en"];
	
	$project_name_slug = preg_replace('/(\s|\.|\')+/','-',strtolower($project_name));
	
	$sql = "INSERT INTO ihse_ihouse_property_slug VALUES(NULL,'$project_name','$project_name_slug') ";
	mysql_query($sql, $dbh);
}



//echo print_r($arr2);

$zipcode_arr = array();

$sql = 'truncate ihse_ihouse_property_price;';

$qry = mysql_query($sql,$dbh);

$arrr = array();
foreach($arr2 as $t) :

	$zp 	= 	'';
	$sql	=	"SELECT * FROM ihse_ihouse_history WHERE month_year = '$t' ";
	$result = 	mysql_query($sql, $dbh);
	
	if (!$result) {
    	echo "Could not successfully run query ($sql) from DB: " . mysql_error();
    	exit;
	}
	
	while ($row = mysql_fetch_assoc($result)) {
		$project_name = $row["project_name"];
		if(!in_array($project_name,$arrr)) {
			array_push($arrr, $project_name);
		}
	}
	
endforeach;

foreach($arrr as $project_name) :
	$sql	=	"SELECT avg_psm, avg_psf FROM ihse_ihouse_history WHERE project_name = '$project_name' ORDER BY str_to_date(month_year, \"%b%Y\") DESC LIMIT 1 ";
	$result1 = mysql_query($sql, $dbh);
	
	$row = mysql_fetch_row($result1);
	
	$avg_psm	=	$row[0];
	$avg_psf	=	$row[1];
	
	if (!$result1) {
		
    	echo "Could not successfully run query ($sql) from DB: " . mysql_error();
    	exit;
		
	}
	
	$sql = "INSERT INTO ihse_ihouse_property_price VALUES(NULL,'$project_name','$avg_psm','$avg_psf') ";
	mysql_query($sql, $dbh);
	
endforeach;
mysql_free_result($result);

/////////////////////////////////////////////////////////
/* END -- TRANSACTION HISTORICAL CHART               */
/////////////////////////////////////////////////////////



mysql_close($dbh);

?>

<?php

function projectDataRecursive($conn_id, $project_path) {
	
	if (ftp_chdir($conn_id, $project_path)) {
		echo "Current directory is now: " . ftp_pwd($conn_id) . "<br />";
	
		// get contents of the current directory
		$files = ftp_nlist($conn_id, ".");
	
		foreach($files as $f) :
		
			if($f == '.' || $f == '..')
				continue;
			
			
			//echo $project_path_1;
			
			//if(is_dir($project_path_1)) {
			
			if(!preg_match('/\.csv$/', $f)) {
				$project_path_1 = $project_path.'/'.$f;
				echo $project_path_1."<br />";
				projectDataRecursive($conn_id, '/'.$project_path_1);
			}
			//}
			
			if(!preg_match('/\.csv$/', $f))
				continue;
		
			//echo $project_path_1."<br />";
			$serverfile = $project_path_1;	
			$localfile	= "tmp/ProjectData/".$f;
		
			// try to download $server_file and save to $local_file
			if (ftp_get($conn_id, $localfile, $serverfile, FTP_BINARY)) {
			
    			echo "Successfully written to $localfile. Now Process the data... <br />";
		
			} else {
    	
				echo "Oops!! There was a problem. Cannot write to $localfile<br />";
		
			}
		endforeach;
	}
}

?>